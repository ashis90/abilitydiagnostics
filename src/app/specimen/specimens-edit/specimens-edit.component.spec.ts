import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecimensEditComponent } from './specimens-edit.component';

describe('SpecimensEditComponent', () => {
  let component: SpecimensEditComponent;
  let fixture: ComponentFixture<SpecimensEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecimensEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecimensEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
