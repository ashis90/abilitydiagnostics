import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecimenAddComponent } from './specimen-add.component';

describe('SpecimenAddComponent', () => {
  let component: SpecimenAddComponent;
  let fixture: ComponentFixture<SpecimenAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecimenAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecimenAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
