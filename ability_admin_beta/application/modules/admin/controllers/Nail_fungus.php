<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class  Nail_fungus extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->session_checked($is_active_session = 1);

     }

 public function index() {
 	$this->session_checked($is_active_session = 1);
    $conditions = " ( `service_status` <>'Deleted') AND ( `service_type` = 'Nail')";
  	$select_fields = '*';
    $is_multy_result = 0;
    $serviceList = $this->BlankModel->getTableData('services', $conditions, $select_fields, $is_multy_result);
    common_viewloader('Nail_Fungus/nail_fungus_mgr', array('serviceList' => $serviceList)); 
 }
 
 function nailFungusEdit(){
	if(isset($_REQUEST['service_id'])){
	$service_id = $_REQUEST['service_id'];
	$conditions = " ( `service_id` = '".$service_id."')";
    $select_fields = '*';
    $is_multy_result = 1;
	
	$service = $this->BlankModel->getTableData('services', $conditions, $select_fields, $is_multy_result);
   	common_viewloader('Nail_Fungus/nail_fungus_mgredit', array('service' => $service)); 	
	}
	
}
 
function nailFungusEditing()
{
    if ($this->input->post()) {
		$data = $this->input->post();
    	$service_id = $data['service_id'];
    	$this->form_validation->set_rules('service_title','Service Title','required');
		$this->form_validation->set_rules('service_desc','Service Description','required');
		if ($this->form_validation->run() == FALSE) {
			
			$title = '';
			if (empty($_FILES['service_image']['name'])) {
				$title .= "<p>Please Select Image</p>";
			}
			if (form_error('service_title')) {
				$title .= "<p>Please Enter Nail Fungus Title</p>";
			}
			if (form_error('service_desc')) {
				$title .= "<p>Please Enter Nail Fungus Description</p>";
			}
			$this->session->set_flashdata('Error',$title);
			$this->serviceEdit($service_id);
		} else{
			$data = array();
			$data = $this->input->post();

			$select_fields = '*';
			$is_multy_result = 1;
			$get_img_conditions = " ( `service_id` = '".$data['service_id']."' AND `service_status` = 'Active')";
			
			$img = $this->BlankModel->getTableData('services', $get_img_conditions, $select_fields, $is_multy_result);

			// print_r($img);
			// exit();

			$redirect_url = base_url().'admin/nail_fungus/';
			
			if($_FILES['service_image']['name']==''){
			   $image_name = $img['service_image'];
			}
			else{
				$service_uploads  = image_uploads('Nail',$thumb_Size_width = '500', $thumb_Size_hight = '300', 'service_image');
				
				if(is_array($service_uploads))
				{
						$image_name = $service_uploads['file_name'];
				}
				
				if(!is_array($service_uploads))
				{
					$this->session->set_flashdata('Err', $service_uploads);
					header('location:'.base_url().'admin/nail_fungus/');	
					exit;    
				}
			}
			 
			 $conditions = " ( `service_id` = '".$data['service_id']."' )";
			 $service_data = array('service_title' => $data['service_title'],
			 				'service_desc' => $data['service_desc'],
			 				'service_status' => $data['service_status'],
			 				// 'service_link' => $data['service_link'],
			 				'service_image' => $image_name);

			 
			if($_FILES['service_image']['name']==''){
			   $service_data = array('service_title' => $data['service_title'],
			 				'service_desc' => $data['service_desc'],
			 				'service_status' => $data['service_status'],
			 		);
			}
			
			 $programsedit = $this->BlankModel->editTableData('services', $service_data, $conditions);
			 $this->session->set_flashdata('succ', 'Service Updated successfully');
			 header('location:'.$redirect_url);
			 exit;
		}
	} else{
		$this->session->set_flashdata('Err','Submission Failed');
		header('location:'.base_url().'admin/nail_fungus');
	}
		   
}

    public function NailAdd() {

    	if ($this->input->post('Submit')) {
    		// $this->form_validation->set_rules('service_image','Service Image','required');
    		$this->form_validation->set_rules('service_title','Service Title','required');
    		$this->form_validation->set_rules('service_desc','Service Description','required');
    		if ($this->form_validation->run() == FALSE) {
    			
    			$title = '';
    			// if (empty($_FILES['service_image']['name'])) {
    			// 	$title .= "<p>Please Select Image</p>";
    			// }
    			if (form_error('service_title')) {
    				$title .= "<p>Please Enter Nail Fungus Title</p>";
    			}
    			if (form_error('service_desc')) {
    				$title .= "<p>Please Enter Nail Fungus Description</p>";
    			}
				$this->session->set_flashdata('Error',$title);
				$this->index();
    		} else{
		        $redirect_url = base_url().'admin/nail_fungus/';
		        $data = array();
			    $data = $this->input->post();
		        //check whether submit button was clicked or not
		        // if ($this->input->post('Submit')) {

		    	$service_uploads  = image_uploads('Nail',$thumb_Size_width = '500', $thumb_Size_hight = '300', 'service_image');
		       
		        if (is_array($service_uploads)) {
		        	
		              //save the file info in the database
		   
		            $image_name = $service_uploads['file_name'];
		        } else{
		        	$image_name = '';
		        }  
			        $service_data = array('service_title' => $data['service_title'],
			        				'service_desc' => $data['service_desc'],
			        				'service_status' => 'Active',
			        				// 'service_link' => $data['service_link'] ,
			        				'service_image' => $image_name,
			        				'service_type' => 'Nail');
			     		
					$service_data = $this->BlankModel->addTableData('services',$service_data);
					$this->session->set_flashdata('succ', 'New Service added successfull.');
					header('location:'.$redirect_url);	
					exit;    
				// }  
			
			   // }
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/nail_fungus');
		}
	}

	public function deleteNailFungus($did = ''){
		$select_fields = '*';
		$is_multy_result = 1;
		$get_img_conditions = " ( `service_id` = '".$did."')";
		
		$img = $this->BlankModel->getTableData('services', $get_img_conditions, $select_fields, $is_multy_result);
		$image = $img['service_image'];

		$path = FCPATH.'assets/uploads/Nail/';
		$value = $path.$image;
		unlink($value);
		// echo "<pre>";
		// print_r($img);
		// echo "<br>";
		// print_r($value);
		// exit();



		$delete_nailfungus = $this->BlankModel->delete_id('wp_abd_services','service_id',$did);
		if ($delete_nailfungus) {
			$this->session->set_flashdata('delete','Successfully Deleted');
			header('location:'.base_url().'admin/nail_fungus');
		}
	}

	public function deleteServiceImage(){
		$service_id = $this->input->post('service_id');
		$service_image = $this->input->post('service_image');
		// echo $service_image;
		$path = FCPATH.'assets\uploads\Nail/';

		$value = $path.$service_image;
		unlink($value);

		$data = array('service_image'=> '');
		$conditions = array('service_id'=> $service_id, 'service_image'=> $service_image);
		$delete_service_image = $this->BlankModel->editTableData('services', $data, $conditions);
		echo $delete_service_image;
	}

}
?>