import { Component, OnInit, Renderer2 } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import {PcrService } from '../pcr.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'cm-assign-well',
  templateUrl: './assign-well.component.html',
  styleUrls: ['./assign-well.component.css']
})
export class AssignWellComponent implements OnInit {

  searchForm: FormGroup;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  public data : any;
  specimens_list:any = [];
  show =  false;
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private renderer: Renderer2,
    private toastr: ToastrService
  ) 
  { 
    this.searchForm = new FormGroup({        
      "barcode" : new FormControl(),
      "assessioning_num" : new FormControl(), 
     });
  }

  ngOnInit() {
  }
  onSubmit()
  {
    if(this.searchForm.status == "VALID")
    {   
        this.spinner.show();
        this.show = true;
        this.sendData = {"barcode":this.searchForm.value.barcode, "assessioning_num":this.searchForm.value.assessioning_num, "physician":this.searchForm.value.physician_id  };
        this.itemsService.serachDeliveryToExtraction(this.sendData).subscribe( data=> {
            this.specimens_list = data;
            if(this.specimens_list.status === '1')
            {
            this.spinner.hide();
            this.data  =  this.specimens_list.specimen_results;
            }
            if(this.specimens_list.status === '0')
            {
              this.no_data_status= true;
              this.no_data="No Data Found.";
            }
         });
    }
    else
    {

    }
  }

  assignStage(specimen_id:any){
    let params;
    params = {id:specimen_id}
    this.spinner.show();
        this.itemsService.assignInStage(params).subscribe( data=> {
          if(data['status']==='1'){
            this.spinner.hide();
            this.toastr.info('Data Successfully updated', 'Success', {
              timeOut: 3000
            });
            this.route.navigateByUrl('/pcr-stages');  
          }
      });
  }
  
  unassignStage(specimen_id:any){
    let params:any;
    params = {id:specimen_id}
    this.spinner.show();
      this.itemsService.unassignInStage(params).subscribe( data=> {
        if(data['status']==='1'){
          this.spinner.hide();
          this.toastr.info('Data Successfully updated', 'Success', {
            timeOut: 3000
          });
          this.route.navigateByUrl('/pcr-stages');
        }
      });
  }

  resetForm(){
    this.searchForm.reset();
  }


}
