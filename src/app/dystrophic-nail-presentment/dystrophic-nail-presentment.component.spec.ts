import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DystrophicNailPresentmentComponent } from './dystrophic-nail-presentment.component';

describe('DystrophicNailPresentmentComponent', () => {
  let component: DystrophicNailPresentmentComponent;
  let fixture: ComponentFixture<DystrophicNailPresentmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DystrophicNailPresentmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DystrophicNailPresentmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
