import { Component, OnInit, Renderer2 } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import {PcrService } from '../pcr.service';
import { ProcessStageModalComponent } from '../pcr/process-stage-modal/process-stage-modal.component';

@Component({
  selector: 'cm-delivery-ext',
  templateUrl: './delivery-ext.component.html',
  styleUrls: ['./delivery-ext.component.css']
})
export class DeliveryExtComponent implements OnInit {

  public data : any;
  specimens_list:any = [];
  specimen_data:any = [];
  physicians_list:any = [];
  searchForm: FormGroup;
  extractionProcessData: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  physicians_list_data:any = [];
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  pcr_text = false;
  xvalue;
  yvalue;
  zvalue;
  color;
  pieces;
  cassettes;
  nail;
  skin;
  sub;
  histo_chk;
  pcr_chk;
  histo_val;
  pcr_val;
  pass:any;
  test_type;
  specimen_id;
  stageid;
  xval;
  yval;
  zval;
  show = false;
  filter :boolean = false;
  spe_count:any;
  serch_arr:any;
  load_more_data:any=0;
  modalRef: BsModalRef;
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private renderer: Renderer2,
  ) 
  { 
    this.searchForm = new FormGroup({        
      "assessioning_num" : new FormControl('',Validators.required), 
     });

     this.extractionProcessData = new FormGroup({
      "pass" : new FormControl("",[Validators.required]),
      "xvalue" : new FormControl("",[Validators.required]),
      "yvalue" : new FormControl("",[Validators.required]),
      "zvalue" : new FormControl("",[Validators.required]),
      "color" : new FormControl("",[Validators.required]),
      "pieces" : new FormControl("",[Validators.required]),
      "tissue" : new FormControl(),
      "pcr" : new FormControl(),
      "pcr_val" : new FormControl()
     });
  }

  ngOnInit() {
    this.getdeliverytoextarction();  
  }
  getdeliverytoextarction() {
    let params = {'load':0};
    this.spinner.show();
    this.itemsService.deliveryToExtraction(params).subscribe(data => {          
    this.specimens_list = data;
      if(this.specimens_list.status == '1')
      {
        this.spinner.hide();
        this.no_data_status = false;
        this.data  =  this.specimens_list.specimen_results;
        this.serch_arr =  this.specimens_list.specimen_results;
        this.test_type = this.specimens_list.test_types;
        this.physicians_list = this.specimens_list.physicians_data;
        this.spe_count = this.specimens_list.specimen_count;
        this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
        this.physicians_list.map((val) => this.allIds.push ( val.id));
        this.physicians_list.map((data) => this.filterVal.push ( data));
      }
      if(this.specimens_list.status == '0')
      {
        this.spinner.hide();
        this.no_data_status= true;
        this.no_data="No Data Found.";
      }
               
    });
  }
  show_pcr(event)
   {
    this.filter = !this.filter;
   }

  
  extractionDisplayFieldCss(field: string) {
    return {
      'has-error': this.extractionIsFieldValid(field),
      'has-feedback': this.extractionIsFieldValid(field)
    };
  }

  extractionIsFieldValid(field: string) {
    return !this.extractionProcessData.get(field).valid && (this.extractionProcessData.get(field).touched || this.extractionProcessData.get(field).dirty);
  }


  onSubmit()
  {
      if(this.searchForm.status == "VALID")
      {   
          this.spinner.show();
          this.load_status=false;
          this.sendData = {"assessioning_num":this.searchForm.value.assessioning_num, 
        };
          this.itemsService.serachsingleStage(this.sendData).subscribe( data=> {
              this.spinner.hide();
              this.postsArray = data;
              
              if(this.postsArray.status === '1')
              {     
                    this.show = true;
                    this.postsArray = data;
                    if(this.postsArray.stage_det.pass_fail)
                    {
                      this.pass=this.postsArray.stage_det.pass_fail;
                    }
                    else
                    {
                      this.pass="";
                    }
                    this.xvalue= this.postsArray.first;
                    this.yvalue= this.postsArray.second;
                    this.zvalue= this.postsArray.third;
                    this.color= this.postsArray.color;
                    this.cassettes= this.postsArray.cassettes;
                    if(this.postsArray.stage_det === 'Nil' || this.postsArray.stage_det.pieces == "")
                    {
                      this.pieces= '1';
                    }
                    else
                    {
                      this.pieces= this.postsArray.stage_det.pieces;
                    }
                    this.nail= this.postsArray.nail;
                    this.pcr_chk= this.postsArray.pcr_chk;
                    if(this.postsArray.stage_det.pcr)
                    {
                      this.pcr_val = this.postsArray.stage_det.pcr;
                    }
                    else
                    {
                      this.pcr_val = "";
                    }
                    this.specimen_id=  this.postsArray.spe_details.id;
                    this.stageid= 1;
                    this.xval=this.xvalue; 
                    this.yval= this.yvalue;
                    this.zval= this.zvalue;
                    this.acc_no= this.postsArray.spe_details.assessioning_num;
                    
              }
              if(this.postsArray.status === '0')
              {
                  this.show = false;
                  this.data ="";
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
              }
           });
      }
      else
      {
        this.validateAllFormFields(this.searchForm);
      }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  extractionDataSubmit(specimen_id,stageid){
    //console.log(this.extractionProcessData);
    if(this.extractionProcessData.status == "VALID")
    {   
        this.spinner.show();
        let serializedForm = this.extractionProcessData.value;
        let user_id = localStorage.userid;
        let formObj = {...serializedForm,user_id,specimen_id}; // {name: '', description: ''}
    
        this.itemsService.submitExtractionProcessStage(formObj).subscribe(data => { 
          this.spinner.hide();        
          this.route.navigateByUrl('/pcr-stages');
        });
    }
    else

    {
      this.validateAllFormFields(this.extractionProcessData);
    }
}

validateAllFormFields(formGroup: FormGroup) {         

  Object.keys(formGroup.controls).forEach(field => {  

    const control = formGroup.get(field);             

    if (control instanceof FormControl) {             

      control.markAsTouched({ onlySelf: true });

    } else if (control instanceof FormGroup) {        

      this.validateAllFormFields(control);            

    }

  });

}
load_more(){
  this.load_more_data = this.load_more_data + 10;
  this.spinner.show();
  let params = {'load':this.load_more_data};
  this.itemsService.loadMorePCR(params).subscribe(data => {
  this.spinner.hide();  
  this.specimens_list = data;
    if(this.specimens_list){
      this.specimens_list.specimen_results.forEach(element => {
        this.data.push(element);
      });
      this.spe_count = this.data?this.data.length:'';
    }
  });    
}

  resetForm(){
    this.searchForm.reset();
  }
  

}
