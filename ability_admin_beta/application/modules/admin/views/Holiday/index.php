<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Holidays Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Holidays Management</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <?php
            if ($this->session->flashdata('success')) {
            $message = $this->session->flashdata('success');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?>
          </div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
            <?php
            if ($this->session->flashdata('update')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('update');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
            <?php
            if ($this->session->flashdata('delete')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('delete');
              echo ($message);
              ?>
          </div>
          <?php
            }
          ?>

          <div class="box-body">
            <div class="svt">
              <div class="table-responsive">
                <a href="<?php echo base_url('admin/holiday/add_holiday');?>" class="btn btn-primary">Add Holiday</a> <br><br><br>
                <table class="table table-bordered table-hover" id="basicDataTable">
                  <thead>
                    <tr>
                      <th width="8%">Sr No</th>
                      <th width="15%">Title</th>
                      <th width="15%">Start Date</th>
                      <th width="15%">End Date</th>
                      <th width="15%">Status</th>                      
                      <th width="12%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($holidayList): ?>
                    <?php
                      $count= 1;  
                      foreach($holidayList as $holiday):
                        $start_date = date('Y-m-d',strtotime($holiday['start_date']));
                        $end_date = date('Y-m-d',strtotime($holiday['end_date']));
                        $datetime1 = new DateTime($start_date);
                        $datetime2 = new DateTime($end_date);
                        $interval = $datetime1->diff($datetime2);
                        $num_of_months = $interval->format('%m');
                        $num_of_years = $interval->format('%y');
                        $num_of_days = $interval->format('%d') + ($num_of_months * 30) + ($num_of_years * 365);
         
                      ?>                 
                    <tr class="odd gradeX">
                      <td style="display: none"><?php echo $holiday['start_date'];?></td>
                      <td><?php echo $count;?> </td>
                      <td><?php echo $holiday['title'];?> </td>
                      <td><?php echo $holiday['start_date'];?> </td>
                      <td><?php echo $holiday['end_date'];?> </td>
                      <?php
                      if (($holiday['is_active']) == 1) { ?>
                        <td><?php echo 'Active';?> </td>
                      <?php } else{ ?>

                          <td><?php echo 'Inactive';?> </td>
                      <?php  } ?>
                      <!-- <td>
                        <a href="<?php   ?>"></a>
                      </td> -->
                      <td>
                        <a href="<?php echo base_url('holiday-update/'.$holiday['id']); ?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                        <a href="<?php echo base_url('holiday-delete/'.$holiday['id']);  ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                      </td> 
                    </tr>
                    <?php 
                      $count++;  
                      endforeach ;                 
                      endif;
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>
