<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Page Add
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Page List</li>
    </ol>
  </section>
     <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
              <h4 class="title">Page List </h4>
          </div>
        <form action="<?php echo base_url().'admin/cms/pageAdding/';?>" method="POST" name="program_frm" id="program_frm" enctype='multipart/form-data'>
			 <div class="form-group">
                <label for="exampleTextarea"> Page Title :</label>
                <input type="text" id="site_page_title" name="site_page_title" value="" required/>        
             </div>
          

          <div class="form-group">
            <label for="exampleTextarea"> Description:</label>
            <textarea id="site_page_desc" name="site_page_desc" rows="10" cols="80" placeholder="Enter Description"></textarea>            
          </div>
          
          <!-- <div class="form-group">
            <label for="exampleTextarea"> Video Upload :</label>
           <input type="file" class="form-control-file" id="logo" aria-describedby="fileHelp" name="site_video">
         
          </div> -->
         
            <div class="text-center">
           <a href="<?php echo base_url();?>admin/cms/" class="btn btn-warning">  Back</a>
           <input type="submit" name="Submit" value="Submit">
         <?php /*?> <button type="submit" class="btn btn-primary ban-sbmt">Submit</button><?php */?>
         </div>
        </form>
        </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
