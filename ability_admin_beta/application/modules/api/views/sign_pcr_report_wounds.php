		
		
		<p style="padding-top:1px;"></p>

<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4; border-top:none;font-family:Arial, Helvetica, sans-serif;font-size: 12px;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;font-size: 12px;">
    <h4 style="font:700 12px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Wound Molecular Report</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 0 0;">
      <div style="width:31.3333%; float:left; margin:0; padding:1% 0 1% 1%;">
        <div style="max-width:80%; margin:0 auto;">
          <img src="{logo}" alt = "ability_logo"/>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:0;">
        <div class="comny_tag">
          <h4 style="font:700 15px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;">Ability Diagnostics, LLC</h4>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">858 South Auto Mall Road, Suite 102</p>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">American Fork, UT 84003</p>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:1%;">
        <div style="font:400 12px/14px Arial, Helvetica, sans-serif;color: #444;">
          <p style="margin-bottom:0;font-size: 12px;">Date of Report</p>
          <p style="margin:0;font-size: 12px;"><span style="display:block;">{date}</span></p>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="border-bottom:1px dotted #8db3e2; text-align:center;color:#444; padding:0; margin:0;">
      <div style=" width:31.3333%; float:left; margin:0; padding:0.5% 1% 0 1%;">
        <div class="n_phn_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">801-899-3828</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:0.5% 1% 0 1%">
        <div class="fax_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">801-855-7548</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:0.5% 1% 0 1%;">
        <div class="assct_no">
          <h6 style="font:700 12px/14px Arial, Helvetica, sans-serif; color:#444;margin:0;">Accessioning Number</h6>
          <p style="margin-top:5px;color:#444;font:normal 12px/14px Arial, Helvetica, sans-serif;">{acc_num}</p>
        </div>
      </div>
    </div>
  </div>
  <div style="padding:3px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
		<p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif; color:#000;">Patient Information:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;text-transform:capitalize;">{p_name}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_phone}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">DOB:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_dob}</div>
        </p>
        <p style="display:block;margin:0 0 5px">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Collection Date:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_col_date}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Date Received:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_rec_date}</div>
        </p>
      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
		<div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif;">Referring Physician:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;">{phy_name}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Address:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_address}</div>
          </p>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_phone}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_fax}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
   <div class="clearfix"></div>             
              </div>           
            
                
                
                
                
                
                
<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4;border-top:none;">        
                
 <div style="padding: 0 13px;padding-bottom:10px;"> 
             
<div style="width: 100%;border-bottom: 2px solid #000;margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;margin-bottom: 5px;margin-top: 2px;">PCR Results for (Site Indicator):</div>
     
     <div style="width: 100%">
     	<table width="100%" style="border: 1px solid #000;" cellpadding="0" cellspacing="0">
     	<thead>
     	<tr>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Skin and Soft Tissue Pathogens</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Result</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Bacterial Load*</td>
     	</tr>	
     		
     	</thead>
     	<tbody>
     	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Acinetobacter baumannii </td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Medium</td>
     	
     	</tr>
     	
     	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Bacteriodes fragilis</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>	

	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Escherichia coli</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Klebsiella pneumoniae </td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Low</td>
     	
     	</tr>
     		<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Proteus mirabilis </td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">High</td>
     	
     	</tr>
     		<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Pseudomonas aeruginosa</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
     		<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Serratia marcescens</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
     		<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Staphylococcus aureus</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
     		
     		
     	</tbody>
     		
     	</table>
     	
     </div>

 

 

<div style="font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;margin: 5px 0 2px;"><span style="font-weight: bold;">Normal Skin Flora:</span> The organisms listed below may be detected in or on normal skin. A High bacterial load may be associated with an infection but may also only represent colonization. Use clinical judgement when interpreting these results.</div>

<div style="width: 100%">
     	<table width="100%" style="border: 1px solid #000;" cellpadding="0" cellspacing="0">
     	<thead>
     	<tr>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Normal Skin Flora</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Result</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Bacterial Load*</td>
     	</tr>	
     		
     	</thead>
     	<tbody>
     	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Anaerococcus spp. </td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">High</td>
     	
     	</tr>
     	
     	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Enterococcus faecalis</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>	

	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Enterococcus faecium</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Staphylococcus epidermidis </td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">No Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
     		<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Streptococcus agalactiae</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">No Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>

     		
     		
     	</tbody>
     		
     	</table>
     	
     </div>
<div style="font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;margin: 5px 0 2px;"><span style="font-weight: bold;">Resistance Markers:</span> The resistance markers listed below may not be associated with pathogens tested on this panel.</div>


<div style="width: 100%">
     	<table width="100%" style="border: 1px solid #000;" cellpadding="0" cellspacing="0">
     	<thead>
     	<tr>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Resistance Genes</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Result</td>
     	<td style="background: #5999d6;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size: 12px;color: #fff;text-align: center;padding: 2px;border: 1px solid #000;">Resistance Load*</td>
     	</tr>	
     		
     	</thead>
     	<tbody>
     	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">mec A</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">High</td>
     	
     	</tr>
     	
     	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">KPC</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>	

	<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">OXA-23</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Not Detected</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
	<tr>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">van A</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">No Detected</td>
     	<td style="background: #fff;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;"></td>
     	
     	</tr>
     		<tr>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">van B</td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">Detected </td>
     	<td style="background: #ddebf6;font-family:Arial, Helvetica, sans-serif;font-weight: normal;font-size: 12px;color: #000;text-align: center;padding: 2px;border: 1px solid #000;">High</td>
     	
     	</tr>

     		
     		
     	</tbody>
     		
     	</table>
     	
     </div>


<div style="font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;margin: 5px 0 0;">*Low, Medium, and High load quantification for bacteria and resistance genes corresponds to sample loads of &lt;10^5, 10^5 to
10^7, and &gt;10^7 respectively.</div>


     
       <div style="width:100%; float:left; margin: 0 0 0;">
            <div><span style="border-bottom: 2px solid #000;margin-bottom:0;font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;">Molecular Interpretation:</span></div>
       <div style="margin-top: 2px;"><span style="margin:0;margin-top:0;font-size:11px;font-family:Arial, Helvetica, sans-serif;">
       	A “Detected” result indicates amplification of DNA from the specified organism.<br>
A “Not Detected” result indicates the absence of amplification of DNA from the specified organism.<br>
Clinical false positives and false negatives occur. A clinical false positive occurs when organisms are detected in the specimen,
but are not causing infection or pathogenic change in the patient. A clinical false negative occurs when an organism is present
in the wound and causing clinical disease, but amplification does not occur.
       	
       	
       </span>
       </div>
     </div>
     
     
        
<div style="margin-bottom:0;font-family:Arial, Helvetica, sans-serif;font-size: 12px; font-weight: normal;margin-top: 5px;">Report verified and autogenerated on 03/01/2019 at 11:07</div>

					<div style="margin-top:2px; "><h2 style="margin-bottom:1px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;margin-top: 0;"><span style="border-bottom: 2px solid #000;margin-top: 0;">Test Method Validation:</span></h2></div>
					<div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
					<p style="margin:0;margin-top:3px;font-size:11px;">The test method used to generate the above results is real-time quantitative polymerase chain reaction (QPCR). Analytical and clinical
validation studies compliant with CLIA and the College of American Pathologists(CAP) were performed at Ability Diagnostics, LLC. The performance characteristics for this assay are available upon request.</p>
					
					</div>
            
            
            
				<div><h2 style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;margin-top: 0;"><span style="border-bottom: 2px solid #000;margin-top: 0;">Lab Developed Test (LDT) Disclaimer:</span></h2></div>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
					<p style="margin:0;margin-top:2px;font-size:11px;">This test was developed and its performance characteristics determined by Ability Diagnostics, LLC. It has not been cleared or approved by the
FDA; however, the FDA has determined that such clearance or approval is not necessary. The laboratory is regulated and certified under CLIA as qualified to perform high-complexity testing. This test is used for clinical purposes. It should not be regarded as investigational or for research.</p>
				</div>
				
				
                </div> 
				
			
		</div>  