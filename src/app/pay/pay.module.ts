import { NgModule } from '@angular/core';
import { PayRoutingModule } from './pay-routing.module';
import { CommonModule } from '@angular/common';
import { NavbarModule } from '../core/navbar/navbar.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, PayRoutingModule, NavbarModule],
  declarations: [PayRoutingModule.components]
})
export class PayModule { }
