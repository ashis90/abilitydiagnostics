import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  postsArray: any = [];
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/contactus_inner_banner.jpg';
  contactForm: FormGroup;
  sendData: any;
  constructor(private http: HttpClient, public itemsService: FrontendService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, private fb: FormBuilder, private toastr: ToastrService) {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      message: ['', [Validators.required]],
    });
  }

  ngOnInit() {

    // let shand = document.getElementsByClassName('middle_hdr') as HTMLCollectionOf<HTMLElement>;

    // shand[0].style.display = "none";
    // let shand2 = document.getElementsByClassName('top_hdr') as HTMLCollectionOf<HTMLElement>;

    // shand2[0].style.display = "block"; 
    // let shand3 = document.getElementsByClassName('hdr_btm') as HTMLCollectionOf<HTMLElement>;

    // shand3[0].style.display = "block";    

    // let shand4 = document.getElementsByClassName('labdoctor_hdr') as HTMLCollectionOf<HTMLElement>;
    // shand4[0].style.display = "none";

    // let shand6 = document.getElementsByClassName('data_entry_hdr') as HTMLCollectionOf<HTMLElement>;                    
    // shand6[0].style.display = "none";

    this.getcontact();
  }
  getcontact() {
    this.itemsService.makeContactRequest().subscribe(data => this.postsArray = data);

  }

  onSubmit() {
    if (this.contactForm.status == "VALID") {
      this.sendData = { name: this.contactForm.value.name, email: this.contactForm.value.email, subject: this.contactForm.value.subject, message: this.contactForm.value.message };
      this.itemsService.send_mail(this.sendData).subscribe(data => {
        this.postsArray = data;
        if (this.postsArray.status == 1) {
          this.toastr.info('Mail Sent successfully', 'Success', {
            timeOut: 3000
          });
          this.contactForm.reset();
        }

      });
    }
    else {
      this.validateAllFormFields(this.contactForm);
    }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.contactForm.get(field).valid && (this.contactForm.get(field).touched || this.contactForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


}
