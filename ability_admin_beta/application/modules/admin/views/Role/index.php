<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Role Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Role Management</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <?php
            if ($this->session->flashdata('success')) {
            $message = $this->session->flashdata('success');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?>
          </div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
            <?php
            if ($this->session->flashdata('update')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('update');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
            <?php
            if ($this->session->flashdata('delete')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('delete');
              echo ($message);
              ?>
          </div>
          <?php
            }
          ?>
          <div class="box-body">
            <div class="svt">
              <div class="table-responsive">
                <a href="<?php echo base_url('admin/role/add_role');?>" class="btn btn-primary">Add new Role</a> <br><br><br>
                <table class="table table-bordered table-hover" id="basicDataTable">
                  <thead>
                    <tr>
                      <th width="8%">Sr No</th>
                      <th width="15%">Role Name</th>
                      <!-- <th width="15%">Short Name</th> -->
                      
                      <th width="12%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($roleList): ?>
                    <?php
                      $count= 1;  
                      foreach($roleList as $user):
                      ?>                 
                    <tr class="odd gradeX">
                      <td><?php echo $count;?> </td>
                      <td><?php echo $user['role_name'];?> </td>
                      <!-- <td>
                        <a href="<?php   ?>"></a>
                      </td> -->
                      <td>
                        <a href="<?php echo base_url('role-update/'.$user['role_id']); ?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                        <a href="<?php echo base_url('role-delete/'.$user['role_id']);  ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                      </td> 
                    </tr>
                    <?php 
                      $count++;  
                      endforeach ;                 
                      endif;
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>
