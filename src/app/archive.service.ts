import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArchiveService {
  headers: Headers = new Headers;
  options: any;
  constructor(private http:HttpClient) { 
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept','application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl+'Archive/'
  
  getAllPendingSpecimen()
  {
    return this.http.get(this.url+'get_pending_specimens', this.options);
   
  }

  searchSpecimen(data)
  {
    return this.http.post(this.url+'search_specimen',JSON.stringify(data), this.options);
   
  }
  // get_fungal_pathology_report(data)
  // {
  //   return this.http.post(this.url+'get_nail_fungal_pathology_report',JSON.stringify(data), this.options);
   
  // }

  // getNailMacroCode(data){
  //   return this.http.post(this.url+'get_nail_macro_code',JSON.stringify(data), this.options);
  // }

  // addReportData(data){
  //   return this.http.post(this.url+'add_report_data',data, this.options);
  // }

  // get_pending_report_details(data){
  //   return this.http.post(this.url+'get_pending_report_details',JSON.stringify(data), this.options);
  // }
}
