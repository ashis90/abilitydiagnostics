<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
?>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
       Report Shortcode Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report Shortcode Management</li>
      
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <a href="<?php echo base_url().'admin/report/add_short'; ?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add</a><br/>
              <h3 class="box-title">Report Shortcode List</h3>
              
            </div>     
          <div class="box-body">
             <div id="alert-success" style="display: none;" class="alert alert-success" role="alert" ></div>
         <div class="svt">
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="basicDataTable">
            <thead>
              <tr>
                <th class="sort-alpha">Sr No</th>
                <th class="sort-alpha" width="10%">Short Codes</th>   
                <th class="sort-alpha" width="15%">Diagnosis</th>         
                <th class="sort-alpha" width="25%">Description / Micro Description</th>
                <th class="sort-alpha" width="25%">Comments</th>            
                <th class="sort-alpha" width="10%">Actions</th>      
              </tr>
            </thead>
            <tbody>
            <?php $i=1; if($report_details){ 
              foreach($report_details as $report){
              ?>             
              <tr class="odd gradeX" id="data-<?php echo $report['id'];?>">
                    <td><?php echo $i;?></td>                
                    <td><?php echo $report['sc'];?></td>
                    <td><?php echo $report['diagnosis'];?></td>
                    <td><?php echo short_description($report['text'],100);?></td>
                    <td><?php echo short_description($report['comments'],100);?></td>  
                    <td><div><a href="<?php echo base_url().'admin/report/reportEdit?id='.$report['id'];?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></div>
                    </td>                   
              </tr>
              <?php $i++; } } ?>             
            </tbody>
           </table>
           <a href="<?php echo base_url('admin/report/report_shortcode_list_export');?>" target="_blank" class="btn btn-primary">Report Shortcode list Export</a>
    		 </div>
         </div>
           </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
 </div>
  