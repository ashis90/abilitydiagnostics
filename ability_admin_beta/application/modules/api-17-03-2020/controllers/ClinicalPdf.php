<?php

if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
include_once APPPATH.'libraries/FusionCharts.php';

class ClinicalPdf extends MY_Controller 
{
function __construct() {
    parent::__construct();
   date_default_timezone_set('MST7MDT');
}

function index()
{}

function generate_bar_graph($phy_id,$from_date='',$to_date='')
{

    if($phy_id && $from_date && $to_date)
    {
        $conditions = " ( `physician_id` = '".$phy_id."' AND create_date BETWEEN '" .$from_date." 00:00:00' AND '".$to_date." 23:59:59')";
    }
    else if($phy_id && !($from_date) && !($to_date))
    {
        $conditions = " ( `physician_id` = '".$phy_id."')";
    }
   
    $select_fields = 'id';
    $is_multy_result = 0;
    $result  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
    
    $val=array();
    if(!empty($result))
    {
        foreach ($result as $id) {
            $spe_id = $id['id'];
            $sql1   = "SELECT `gross_description` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$spe_id."'";
            $gross_description=$this->BlankModel->customquery($sql1);

	     if($gross_description > 0){
		 foreach($gross_description as $description){		
			 $str = $description['gross_description'];		
			 $x_postion = strpos($str,"x");	 
		 if(!empty($x_postion)){
		 	 $get_data_from = $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
		     $size_data     = substr($str, $get_data_from, 12);
		 }
		 else{	
			 $postion = strpos($str, "*"); 	
			 if(!empty($postion)){
			 	$get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			    $size_data = substr($str, $get_data_from, 10);
			 }
	    	}	
			preg_match_all('!\d+!', $size_data, $matches);
	    	$generate_gross = '';
			for($i=0; $i<1; $i++){
				$generate_gross .= $matches[0][$i];
			}	
			$first_num = substr($generate_gross,0,2);	
			$num =  filter_var($first_num, FILTER_SANITIZE_NUMBER_INT);
			array_push($val,$num);	
			}
	      } 
	    }      
    }

    $first  = 0;
    $second = 0;
    $third  = 0; 
    $fourth = 0;
    $fifth  = 0; 
    $sixth  = 0;
    $seventh= 0;
    for ($i=0; $i<count($val); $i++)
    {
        if($val[$i] == 0 || $val[$i] == 1 || $val[$i] ==2 || $val[$i] == 3 || $val[$i] == 4 || $val[$i] == 5)
        {
           $first++; 
        }
         if($val[$i] == 6 || $val[$i] == 7 || $val[$i] ==8 || $val[$i] == 9 || $val[$i] == 10)
        {
           $second++; 
        }
         if($val[$i] == 11 || $val[$i] == 12 || $val[$i] ==13 || $val[$i] == 14 || $val[$i] == 15)
        {
           $third++; 
        }
         if($val[$i] == 16 || $val[$i] == 17 || $val[$i] ==18 || $val[$i] == 19 || $val[$i] == 20)
        {
           $fourth++; 
        }
         if($val[$i] == 21 || $val[$i] == 22 || $val[$i] ==23 || $val[$i] == 24 || $val[$i] == 25)
        {
           $fifth++; 
        }
         if($val[$i] == 26 || $val[$i] == 27 || $val[$i] == 28 || $val[$i] == 29 || $val[$i] == 30)
        {
           $sixth++; 
        }
        if($val[$i] >= 31 )
        {
           $seventh++; 
        }
    }
   
    $this->load->view('chart',array('first' => $first,'second' => $second,'third' => $third,'fourth' => $fourth,'fifth' => $fifth,'sixth' => $sixth, 'seventh' => $seventh));
}

}
    


?>