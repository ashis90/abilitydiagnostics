import { sandboxOf } from 'angular-playground';
import { CancelledComponent } from './cancelled.component';

export default sandboxOf(CancelledComponent)
  .add('Cancelled Component', {
    template: `<cm-cancelled></cm-cancelled>`
  });
