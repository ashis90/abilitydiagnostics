import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-data-analysis',
  templateUrl: './data-analysis.component.html',
  styleUrls: ['./data-analysis.component.css']
})
export class DataAnalysisComponent implements OnInit {
  arrayBuffer:any;
  file:File;
  excelData: any[][];
  postArray:any;
  analyze_data:any;
  unique_data:any;
  data:any;
  barcode:any;
  analyze_data_count:any;
  loading:boolean;
  constructor(public itemsService:PcrService,private toastr: ToastrService,private spinner: NgxSpinnerService) { 
    this.excelData=[];
  }

  ngOnInit() {
      
  }

  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }

  Upload() {
    if(this.file){
      this.spinner.show();
    let fileReader = new FileReader();
    let fileData : any;
    fileReader.readAsArrayBuffer(this.file);
      fileReader.onload = (e) => {
          this.arrayBuffer = fileReader.result;
          var data = new Uint8Array(this.arrayBuffer);
          var arr = new Array();
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, {type:"binary"});
          var first_sheet_name = workbook.SheetNames[0];
          var worksheet = workbook.Sheets[first_sheet_name];
          fileData = XLSX.utils.sheet_to_json(worksheet,{raw:true});
       
          this.itemsService.dataAnalysisImportData(fileData).subscribe(data => {
            this.spinner.hide();
            this.postArray = data;
            this.data = this.postArray.analyze_data;
            this.unique_data = this.postArray.unique_data;
            this.barcode = this.postArray.barcode['well_position'];
            this.analyze_data_count = this.postArray.analyze_data_count;
            console.log(this.barcode);
            if(data['status']==='1'){
              
              this.toastr.info('Data Successfully imported', 'Success', {
                timeOut: 3000
              });
            }
        }); 
         
      }
    }else{
      this.toastr.info('Please choose a file.', 'Error', {
        timeOut: 3000
      });
    }
      
}

}
