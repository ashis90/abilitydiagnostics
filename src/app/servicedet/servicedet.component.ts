import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-servicedet',
  templateUrl: './servicedet.component.html',
  styleUrls: ['./servicedet.component.css']
})
export class ServicedetComponent implements OnInit {

  postsArray:any = [] ;
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl+'assets/frontend/main_images/inner_banner.jpg';
  constructor(private http:HttpClient, public itemsService:FrontendService,public route:Router,public activatedRoute : ActivatedRoute) { }

  ngOnInit() 
  {
    // let shand = document.getElementsByClassName('middle_hdr') as HTMLCollectionOf<HTMLElement>;
                     
    // shand[0].style.display = "none";
    // let shand2 = document.getElementsByClassName('top_hdr') as HTMLCollectionOf<HTMLElement>;

    // shand2[0].style.display = "block"; 
    // let shand3 = document.getElementsByClassName('hdr_btm') as HTMLCollectionOf<HTMLElement>;

    // shand3[0].style.display = "block";    
    
    // let shand4 = document.getElementsByClassName('labdoctor_hdr') as HTMLCollectionOf<HTMLElement>;
    // shand4[0].style.display = "none";

    // let shand6 = document.getElementsByClassName('data_entry_hdr') as HTMLCollectionOf<HTMLElement>;                    
    // shand6[0].style.display = "none";
    
    var id=this.activatedRoute.snapshot.paramMap.get('id');
    this.itemsService.makeServiceDetailsRequest(id).subscribe(data => this.postsArray = data[0]);
  }

}
