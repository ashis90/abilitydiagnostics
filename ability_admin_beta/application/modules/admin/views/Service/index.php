<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Banner Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Banners</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title abourt">Banners for <?php echo $bannerList['text'];?> Page</h4>
          </div>    
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?></div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
          <div class="box-body"> 
           <div class="svt">          
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="">
                <thead>
                  <tr>
                    <th width="8%">Sr No</th>
                    <th width="15%">Image</th>                    
                    <th width="25%">Action</th>
                  </tr>
                </thead>
                <tbody> 
                <?php if($bannerList): ?>
                  <?php
                    $count= 1;                     
                    ?>                 
                  <tr class="odd gradeX">
                    <td><?php echo $count;?> </td>                    
                    <td>
                      <a href="<?php echo base_url().'assets/uploads/Banners/'.$bannerList['image'];?>" class="fancy imag">
                        <img src = "<?php echo base_url().'assets/uploads/thumb/'.$bannerList['image'];?>" alt="<?php echo $bannerList['image'];?>"  id="fancyLaunch" class="images"></a>
                    </td>                    
                    <td>
                      <a href="<?php echo base_url().'admin/banner/bannerEdit?banner_id='.$bannerList['id'];?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>                      
                    </td>
                  </tr>                
                 <?php 
                    $count++;                   
                   endif;?>
                  
                </tbody>
              </table>
            </div>
</div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
