<?php
class Holiday extends MY_Controller{
	
	function __construct(){
		parent::__construct();
	$this->session_checked($is_active_session = 1);
	$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('date');
	}

	function index(){
		$sql_holiday = "SELECT * FROM `wp_abd_holidays` ORDER BY `id` ASC";
		$query = $this->db->query($sql_holiday);
		$resList = $query->result_array();	

		common_viewloader('Holiday/index', array('holidayList' => $resList));
	}

	function add_holiday(){
		$this->session_checked($is_active_session = 1);
		common_viewloader('Holiday/addNewholiday');
	}

	function createNewHoliday(){

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('end_date', 'End Date', 'required');
			if ($this->form_validation->run() == FALSE) {
				common_viewloader('Holiday/addNewholiday');
			} else{

				$data = $this->input->post();
				$create_date = date("Y-m-d h:i:sa");
				$start_date = date('Y-m-d',strtotime($data['start_date']));
				$end_date = date('Y-m-d',strtotime($data['end_date']));
				if (($start_date) > ($end_date)) {
					$this->session->set_flashdata('sdate','Submission Failed.. End Date should be greater than Start Date');
					common_viewloader('Holiday/addNewholiday');

					// header('location:'.base_url().'admin/holiday/');
				} else{
					$is_active = 1;
					$table_data = array('title'=>$data['title'],'start_date'=>$start_date,'end_date'=>$end_date,'is_active'=>$is_active, 'create_date'=>$create_date);

					$insert_holiday = $this->BlankModel->addTableData('wp_abd_holidays',$table_data);
					if ($insert_holiday) {
						$this->session->set_flashdata('success','Successfully added Holiday');
						header('location:'.base_url().'admin/holiday/');
					}
				}
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/holiday/');
		}
	}

	function holidayDelete($did=''){
		$delete_holiday = $this->BlankModel->delete_id('wp_abd_holidays','id',$did);
		if ($delete_holiday) {
			$this->session->set_flashdata('delete','Successfully Deleted');
			header('location:'.base_url().'admin/holiday');
		}
	}

	function holidayEdit($eid='') {
	   	$conditions = "(`id` = '".$eid."')";
	   	$select_fields = '*';
	   	$is_multy_result = 1;
	   	$holiday_data = $this->BlankModel->getTableData('wp_abd_holidays', $conditions, $select_fields, $is_multy_result );

	   	common_viewloader('Holiday/holiday_edit',array('holidayData'=>$holiday_data));
	}

	function holidayEditing(){

		if ($this->input->post()) {
			$data = $this->input->post();
			$holiday_id = $data['id'];

			$this->form_validation->set_rules('title','Title','required');
			$this->form_validation->set_rules('start_date','Start Date','required');
			$this->form_validation->set_rules('end_date','Start Date','required');
			if ($this->form_validation->run() == FALSE) {

				// redirect('holiday-update/'.$holiday_id);
				// common_viewloader('Holiday/holiday_edit');
				$this->holidayEdit($holiday_id);
			} else{

				$data = $this->input->post();
				$holiday_id = $data['id'];
				$conditions = "(`id` = '".$holiday_id."')";

				$create_date = date("Y-m-d h:i:sa");
				$start_date = date('Y-m-d',strtotime($data['start_date']));
				$end_date = date('Y-m-d',strtotime($data['end_date']));

				if (($start_date) > ($end_date)) {
					$this->session->set_flashdata('sdate','Submission Failed.. End Date should be greater than Start Date');
					$this->holidayEdit($holiday_id);
				} else{

					$update_holiday_data = array('id' => $holiday_id, 'title' => $data['title'], 
						'start_date' => $start_date, 'end_date' => $end_date, 'is_active' => $data['is_active'],
						'create_date'=>$create_date);

					$update_holiday = $this->BlankModel->editTableData('wp_abd_holidays',$update_holiday_data,
									$conditions);
					if ($update_holiday) {
						$this->session->set_flashdata('update','Successfully Updated');
						header('location:'.base_url().'admin/holiday');
					}
				}
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/holiday/');
		}
	}

}

?>