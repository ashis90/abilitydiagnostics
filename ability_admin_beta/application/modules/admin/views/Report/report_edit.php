<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
 /* if($service['service_status']=="Active"){
      $redirect_url=base_url().'admin/report/';
  }else{
      $redirect_url=base_url().'admin/report/report_list/'.$_REQUEST['id'];
  }  */
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Report Short Code Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Report Short Code List</li>
    </ol>
  </section>
     <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Report Short code</h4>
			<div id="loaderImg" style="display:none;"><img src="<?php echo base_url('assets/frontend/image/loder.gif');?>" alt="loder" width="50px" height="50px"></div>
          </div>
        <form action="<?php echo base_url().'admin/report/reportEditing/';?>" method="POST" enctype="multipart/form-data" name="report_frm" id="report_frm">
         
          <input type="hidden" value="<?php echo $_REQUEST['id'];?>" name="id" />
           
        <div class="form-group">
  			  <label for="exampleInputFile">Short Code</label>
  				<input type="text" id="sc" name="sc" placeholder="Enter Short Code" value="<?php echo $report['sc'];?>">
           <?php echo form_error('sc','<div class="text-danger">','</div>');  ?>
  			</div>
		
        <div class="form-group">
          <label for="exampleTextarea"> Diagnosis Text: </label>
          <input type="text" id="diagnosis" name="diagnosis" placeholder="Enter Short Code" value="<?php echo $report['diagnosis'];?>">
          <?php echo form_error('diagnosis','<div class="text-danger">','</div>');  ?>
            <?php /*?><textarea class="tnytextarea" name="diagnosis" id="diagnosis" rows="10" cols="5"><?php echo $report['diagnosis'];?></textarea><?php */?>
        </div>
        	
        <div class="form-group">
          <label for="exampleTextarea"> Shortcode Description: </label>
          <textarea class="tnytextarea" id="text" name="text" rows="10" cols="80" placeholder="Shortcode Description"><?php echo $report['text'];?></textarea>
          <?php echo form_error('text','<div class="text-danger">','</div>');  ?>
        </div>
          
        <div class="form-group">
          <label for="exampleTextarea"> Comments: </label>
          <textarea class="tnytextarea" id="comments" name="comments" rows="10" cols="80" placeholder="Shortcode Description"><?php echo $report['comments'];?></textarea>
          <?php echo form_error('comments','<div class="text-danger">','</div>');  ?>
        </div>
        <div class="form-group">
          <label for="exampleTextarea"> Color: </label>
          <input type="text" id="color" name="color" placeholder="Enter Short Code" value="<?php echo $report['color'];?>">
          <?php echo form_error('color','<div class="text-danger">','</div>');  ?>
        </div>
        <button type="submit" class="btn btn-primary ban-sbmt">Submit</button>
         
      </form>
        </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
  
    <!-- /.row -->
  </section>
</div>
