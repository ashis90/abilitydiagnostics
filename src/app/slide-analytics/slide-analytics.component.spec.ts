import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideAnalyticsComponent } from './slide-analytics.component';

describe('SlideAnalyticsComponent', () => {
  let component: SlideAnalyticsComponent;
  let fixture: ComponentFixture<SlideAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
