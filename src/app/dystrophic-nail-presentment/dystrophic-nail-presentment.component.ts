import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-dystrophic-nail-presentment',
  templateUrl: './dystrophic-nail-presentment.component.html',
  styleUrls: ['./dystrophic-nail-presentment.component.css']
})
export class DystrophicNailPresentmentComponent implements OnInit {

  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/inner_banner.jpg';
  postsArray: any = [];
  constructor(
    private http: HttpClient,
    public itemsService: FrontendService,
    public route: Router,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.spinner.show();
    this.itemsService.ourScienceBaisedService().subscribe(data => {
      this.postsArray = data
      this.spinner.hide();
    });
  }

}
