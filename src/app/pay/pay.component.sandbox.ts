import { sandboxOf } from 'angular-playground';
import { PayComponent } from './pay.component';

export default sandboxOf(PayComponent)
  .add('Pay Component', {
    template: `<cm-pay></cm-pay>`
  });
