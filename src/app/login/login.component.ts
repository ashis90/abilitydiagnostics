import { Component, OnInit } from "@angular/core";
import { UserserviceService } from "../userservice.service";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { Injectable } from "@angular/core";
import { AuthService } from "../core/services/auth.service";
import { ValidationService } from "../core/services/validation.service";
import { IUserLogin } from "../shared/interfaces";
import {
  GrowlerService,
  GrowlerMessageType
} from "../core/growler/growler.service";
import { LoggerService } from "../core/services/logger.service";
import { environment } from "../../environments/environment";

@Component({
  selector: "cm-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
@Injectable()
export class LoginComponent {
  loginForm: FormGroup;
  errorMessage = true;
  sendData;
  message = false;
  wrongmsg = true;
  postsArray: any = [];
  loginSuccess: boolean = false;
  captchaErrorMessage: any = true;
  captcha_test: boolean = false;
  //siteKey: any = "6Lc7164UAAAAAKZ6D5R9aXdD1xA6QDujfeB9ZZ9F"; //For http://abilitydiagnostics.com
   siteKey: any = "6LfO3KsUAAAAAKlsSwHOK3JaKmuR5ru_RZX-TWOH"; //For localhost
  //siteKey: any = '6LdL4KkUAAAAAGYqBQ0_aXJ-V3fU3PGG6TUWWKRe'; //For http://deve.sisworknew.com/ability_new/
  public home_ban =
    environment.assetsUrl + "assets/frontend/main_images/inner_banner.jpg";
  constructor(
    private router: Router,
    private authService: AuthService,
    private growler: GrowlerService,
    private logger: LoggerService,
    public itemsService: UserserviceService
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      recaptcha: new FormControl(null, [Validators.required])
    });
  }
  handleSuccess(e) {
    if (e) {
      this.captcha_test = true;
    }
  }
  onSubmit() {
    if (this.loginForm.status == "VALID") {
      this.sendData = {
        username: this.loginForm.value.username,
        password: this.loginForm.value.password
      };
      this.itemsService.login(this.sendData).subscribe(data => {
        this.postsArray = data;
        if (this.postsArray.status == "1") {
          this.message = true;
          localStorage.setItem("loggedIn", "true");
          localStorage.setItem("userToken", this.postsArray.details.token);
          localStorage.setItem("userid", this.postsArray.details.id);
          localStorage.setItem("user_role", this.postsArray.details.user_role);
          localStorage.setItem(
            "specimen_stages",
            this.postsArray.details.specimen_stages
          );
          if (this.postsArray.details.user_role === "data_entry_oparator") {
            if (this.postsArray.details.acc_cntrl === "add") {
              localStorage.setItem(
                "acc_cntrl",
                this.postsArray.details.acc_cntrl
              );
            }
            if (this.postsArray.details.acc_cntrl_qc === "check") {
              localStorage.setItem(
                "acc_cntrl_qc",
                this.postsArray.details.acc_cntrl_qc
              );
            }
          }
          if (this.message == true) {
            this.loginSuccess = true;
            this.router.navigate(["/home"]);
          }
        } else {
          this.wrongmsg = false;
          this.loginSuccess = false;
        }
      });
    } else {
      if (
        this.loginForm.value.username == "" ||
        this.loginForm.value.password == ""
      ) {
        this.errorMessage = false;
      } else if (this.loginForm.value.recaptcha == null && !this.captcha_test) {
        this.captchaErrorMessage = false;
      }
    }
  }

  get isLoggedIn() {
    return this.loginSuccess;
  }

  ngOnInit() {
    if (localStorage.loggedIn === "true") {
      this.router.navigate(["/home"]);
    }
  }
}
