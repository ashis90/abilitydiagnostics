import { Component, OnInit, Renderer2 } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'cm-stages',
    templateUrl: './stages.component.html',
    styleUrls: ['./stages.component.css']
})
export class StagesComponent implements OnInit {
  public url = environment.baseUrl;
    datePickerConfig:Partial<BsDatepickerConfig>;
    postsArray:any = [] ;
    sendData;
    physicianArray:any = [] ;
    searchForm: FormGroup;
    public data : any;
    public phy_data : any;
    load_status:boolean = true;
    public loading = false;
    first_msg= false;
    second_msg= false;
    third_msg= false;
    fourth_msg= false;
    histo_modal=false;
    pcr_modal=false;
    acc_no=false;
    modalRef: BsModalRef;
    load_more_data:any=0;
    spe_count:any;
    specimen_stages:any = [];
    st1;
    st2;
    st3;
    st4;
    load_btn = true;
    tatResponse:any;
    tatDetails:any;
    tatMsg:any;
    stageTATCount:any;
    param_list:string;
    constructor(private http:HttpClient, public itemsService:SpecimenService,public route:Router,private spinner: NgxSpinnerService,private modalService: BsModalService,
      public analyticsService:AnlyticsService,
      private renderer: Renderer2) 
    { 
        this.searchForm = new FormGroup({
      
            "acc_no" : new FormControl(),
            "fromdate" : new FormControl(),
            "todate" : new FormControl(), 
            "sort_data" : new FormControl(), 
            "bar_code" : new FormControl(), 
          });
        this.datePickerConfig = Object.assign({},{
        containerClass:'theme-blue',
        dateInputFormat:'MM/DD/YYYY'
        
        });

        this.specimen_stages = localStorage.specimen_stages;

        if(this.specimen_stages){
          this.st1 = this.specimen_stages.indexOf('1') !== -1;
          this.st2 = this.specimen_stages.indexOf('2') !== -1;
          this.st3 = this.specimen_stages.indexOf('3') !== -1;
          this.st4 = this.specimen_stages.indexOf('4') !== -1;
        }

    }

    ngOnInit() {
        if(this.load_status==true)
        {
            this.load_btn = true; 
            this.getstagedetails();
            this.getStageTATDetails()
        }
    }
    resetForm(){
        this.load_btn = true; 
        this.searchForm.reset();
        this.getstagedetails();
      }
    getstagedetails()
    {
      let params = {'load':0};
      this.spinner.show();
      this.itemsService.makeSpecimenStageRequest(params).subscribe( data=> {
      this.spinner.hide();
      this.postsArray = data;
      this.data  =  this.postsArray.specimens_detail;
      this.spe_count = this.data?this.data.length:'';
      this.param_list = this.data.map(x => x['assessioning_num']).join();
      } );
      
    }

    getStageTATDetails()
    {
      this.spinner.show();
      this.itemsService.getStageTATDetails().subscribe( data=> {
      this.spinner.hide();
      this.tatResponse = data;
      if(this.tatResponse.status == '1'){
        this.tatDetails  =  this.tatResponse.specimen_stages_details;
        this.stageTATCount = this.tatResponse.note_count;
        console.log(this.stageTATCount)
      }else{
        this.tatMsg = this.tatResponse.message;
      }
     
      });
      
    }

    onSubmit()
    {
        if(this.searchForm.status == "VALID")
        {   
            this.load_btn = false; 
            this.spinner.show();
            this.load_status=false;
            this.sendData = { "accessioning_number":this.searchForm.value.acc_no,"barcode":this.searchForm.value.bar_code,"fromdate":this.convertDate(this.searchForm.value.fromdate),"todate":this.convertDate(this.searchForm.value.todate), //"sort_data":this.searchForm.value.sort_data 
          };
            this.itemsService.search_specimen_stages(this.sendData).subscribe( data=> {
                this.spinner.hide();
                this.postsArray = data;
                this.data  =  this.postsArray.specimens_detail;
                this.spe_count = this.data?this.data.length:'';
             });
        }
        else
        {
 
        }
    }
    load_last_five()
    {
      this.load_btn = false; 
      this.spinner.show();
      this.itemsService.makelastFiveSpecimenStage().subscribe( data=> {
      this.spinner.hide();
      this.postsArray = data;
      this.data  =  this.postsArray.specimens_detail;
      this.spe_count = this.data?this.data.length:'';
    });
      
    }

    convertDate(str: Date) {
        if(!str){
          var newDate = '';
        }else{
          
          var date = new Date(str),
            mnth = ("0" + (date.getMonth()+1)).slice(-2),
            day  = ("0" + date.getDate()).slice(-2),
            newDate = [ date.getFullYear(), mnth, day  ].join("-");
        }
        return newDate;
      }

      displaytrackmodal(specimen_id:any){
        this.spinner.show();
        this.sendData = { "acc_no":specimen_id, 'dataType': 'general'};
        this.analyticsService.specimen_track(this.sendData).subscribe(data => {
          this.postsArray = data;
          this.spinner.hide();
          if(this.postsArray.histo_status=='1')
          {
            this.acc_no=true;
            this.histo_modal =true;
          }
          else
          {
            this.histo_modal =false;
          }
          if(this.postsArray.pcr_status=='1')
          {
            this.acc_no=true;
            this.pcr_modal =true;
          }
          else
          {
            this.pcr_modal =false;
          }
          if(this.postsArray.status=='0')
          {
             this.acc_no=false;
             this.pcr_modal =false;
             this.histo_modal =false;
          }
          this.modalRef = this.modalService.show(TrackModalComponent,  {
            initialState: {
              title: 'track',
              acc_no:  this.acc_no,
              postsArray: this.postsArray,
              histo_modal: this.histo_modal,
              pcr_modal: this.pcr_modal
            }
      
          });
      
        });
      
      
      }

      load_more(){
        this.load_more_data = this.load_more_data + 10;
        this.spinner.show();
        let params = {'load':this.load_more_data};
          this.itemsService.loadMoreRequest(params).subscribe(data => {
            console.log(data);
          this.spinner.hide();  
          this.postsArray = data;
          if(this.postsArray){
            this.postsArray.specimens_detail.forEach(element => {
              this.data.push(element);
            });
            this.spe_count = this.data?this.data.length:'';
          }
        });    
      }

}
