import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PcrService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http:HttpClient) { 
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept','application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl+'Pcr/'
  
  deliveryToExtraction(data)
  {
    return this.http.post(this.url+'delivery_to_extraction',JSON.stringify(data), this.options);
   
  }
  serachDeliveryToExtraction(data)
  {
    return this.http.post(this.url+'search_delivery_to_extraction',JSON.stringify(data), this.options);
   
  }
  serachsingleStage(data)
  {
    return this.http.post(this.url+'search_single_pcr',JSON.stringify(data), this.options);
   
  }
  extractionLoadAllRequest()
  {
    return this.http.get(this.url+'load_all_delivery_to_extraction', this.options); 
  }
  deliveryToPcr()
  {
    return this.http.get(this.url+'delivery_to_pcr', this.options);
   
  }
  serachDeliveryToPcr(data)
  {
    return this.http.post(this.url+'search_delivery_to_pcr',JSON.stringify(data), this.options);
   
  }
  pcrLoadAllRequest()
  {
    return this.http.get(this.url+'load_all_delivery_to_pcr', this.options); 
  }

  assignWellStage()
  {
    return this.http.get(this.url+'well_assign_stage', this.options);
   
  }
  serachAssignWellStage(data)
  {
    return this.http.post(this.url+'search_well_assign_stage',JSON.stringify(data), this.options);
   
  }
  wellAssignLoadAllRequest()
  {
    return this.http.get(this.url+'load_all_well_assign_stage', this.options); 
  }

  assignInStage(data)
  {
    return this.http.post(this.url+'assign_in_stage',JSON.stringify(data), this.options);
   
  }
  unassignInStage(data)
  {
    return this.http.post(this.url+'unassign_in_stage',JSON.stringify(data), this.options);
   
  }
  dataAnalysisImportData(data)
  {
    return this.http.post(this.url+'data_analysis_import_data',data, this.options);
   
  }

  batchDataReview()
  {
    return this.http.get(this.url+'batch_data_review_data', this.options);
   
  }

  passBatchReview(data)
  {
    return this.http.post(this.url+'pass_batch_review',JSON.stringify(data), this.options);
   
  }

  submitFailBatch(data)
  {
    return this.http.post(this.url+'fail_rq_submit',JSON.stringify(data), this.options);
   
  }
  submitReviewProcessStage(data)
  {
    return this.http.post(this.url+'review_process_stage',JSON.stringify(data), this.options);
   
  }
  getBatchHistory(data)
  {
    return this.http.post(this.url+'batch_history_details',JSON.stringify(data), this.options);
   
  }

  submitExtractionProcessStage(data)
  {
    return this.http.post(this.url+'extraction_process_stage',JSON.stringify(data), this.options);
   
  }

  makeSpecimenNext(id,stageid)
  {
    return this.http.post(this.url+'pcr_next',JSON.stringify({id,stageid}), this.options);
  }

  submitPcrProcessStage(data)
  {
    return this.http.post(this.url+'pcr_process_stage',JSON.stringify(data), this.options);
   
  }

  reportData()
  {
    return this.http.get(this.url+'get_report_generate', this.options);
   
  }

  reportDataById(data)
  {
    return this.http.post(this.url+'get_report_generate_by_id',JSON.stringify(data), this.options);
   
  }
  getBatchDelete()
  {
    return this.http.get(this.url+'get_batch_delete', this.options);
   
  }
  batchDelete(data)
  {
    return this.http.post(this.url+'batch_delete',JSON.stringify(data), this.options);
   
  }
  pcrReportView(data)
  {
    return this.http.post(this.url+'view_pcr_report',JSON.stringify(data), this.options);
   
  }
  getSubmitedReports(data) {
    return this.http.post(this.url + 'view_submited_report', JSON.stringify(data), this.options);
  }


  getArchiveDocument()
  {
    return this.http.get(this.url+'view_archive_document', this.options);
  }

  uploadArchiveFile(data)
  {
    return this.http.post(this.url+'upload_archive_file',data, this.options);
  }
  deleteArchive(data)
  {
    return this.http.post(this.url+'delete_archive_file',JSON.stringify(data), this.options);
  }

  requeuefail(data)
  {
    return this.http.post(this.url+'requeue_fail',JSON.stringify(data), this.options);
  }
  loadMorePCR(data)
  {
    return this.http.post(this.url+'delivery_to_extraction',JSON.stringify(data), this.options);
  }
  uploadWoundsReport(data)
  {
    return this.http.post(this.url+'upload_wounds_report',data, this.options);
  }
  unlinkGeneratedPdf(data)
  {
    return this.http.post(this.url+'unlink_generated_pdf',JSON.stringify(data), this.options);
  }
  sendEmailFax(data)
  {
    return this.http.post(this.url+'send_email_fax',JSON.stringify(data), this.options);
   
  }

  pcrSpecimenTatNotification() {
    return this.http.get(this.url + 'pcrSpecimenTatNotification', this.options);

  }
  search_pcr_submitted_reports(data) {
    return this.http.post(this.url + 'view_submited_report', JSON.stringify(data), this.options);

  }
}
