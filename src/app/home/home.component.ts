import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  postsArray: any = [];
  loggedIn: boolean = true;
  serviceDetails: any;
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/banner-1.jpg';
  public home_sknd_blk = environment.assetsUrl + 'assets/frontend/main_images/service_bg.jpg';
  constructor(private http: HttpClient, public itemsService: FrontendService, public route: Router) { }

  ngOnInit() {
    if (localStorage.loggedIn === 'true') {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }

    this.getuser();
    this.servicePageDetails();

  }
  getuser() {
    this.itemsService.makeHttpGetRequest().subscribe(data => this.postsArray = data);

  }

 
  servicePageDetails() {
    this.itemsService.makeServiceRequest().subscribe(data => {
      this.serviceDetails = data
    });

  }
}
