
<div style="padding:20px;width:100%; margin:0 auto;display:block; font-size:12px;font-family: "Lato",Georgia,serif;">
  <div class="add_physicin">
         <div class="col-md-6" style="width:100%;float:left; margin-bottom-10px;">
		    <div style="width:100%;margin:0 auto;text-align:center;">
		    <img style="max-width:15%; margin:0 auto;" src="{weil_pic}" alt = "weil_pic"/>
		    <img style="max-width:20%; margin:0 auto;" src="{logo}" alt = "ability_logo"/>
		 	<p style="margin:0; font-size:10px;">858 South Auto Mall Drive, Suite 102 American Fork, Utah 84003 (808) 899-3828</p></div>
		  	
          </div>
          <div class="form-group" style="margin:0 0 10px;padding-left:5%;">
              <div style="width:49%;padding-right:1%;float:left;">Date Collected :</div>
			  <div style="width:49%;padding-left:1%;float:left;">Time Collected :</div>
		  	</div>
          <div class="box" style="border:1px solid #000; width:69%;float:left;">
				<div class="form-group" style="padding:5px;background:#cfe1f4;"><div style="float:left;width:30%">Patient Information</div></div>
		  		
		  		<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:49%;float:left;">Last Name : *</div><div style="width:49%;float:left;">First Name : *</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:70%;float:left;">Street address :</div><div style="width:30%;float:left;">Apt :</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:35%;float:left;">City :</div><div style="width:35%;float:left;">State :</div><div style="width:30%;float:left;">Zip :</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Phone :</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:25%;float:left;">Date of Birth</div><div style="width:20%;float:left;border-left:1px solid #000;padding-left:2%;">Sex</div><div style="width:20%;float:left;border-left:1px solid #000;padding-left:2%;">Age</div><div style="width:25%;float:left;border-left:1px solid #000;padding-left:2%;">Patient ID</div></div>
		  	</div>
		  	
		  <div class="col-md-6" style="width:30%;float:right; font-size:11px;">
		  	<div class="box" style="border:1px solid #000;margin:0 0;width:100%"><h4 style="font-weight:normal;background:#cfe1f4;padding:5px;margin:0;font-size:12px;">Physician Information</h4></div>
			<div class="box" style="border:1px solid #000;">
				<div class="form-group" style="padding:5px"><div style="width:100%;float:left;">NIA #</div></div>
				<div class="form-group" style="padding:5px"><div style="width:100%;float:left;">Clinic</div></div>
				<div class="form-group" style="padding:5px;"><div style="width:70%;float:left;">Street</div><div style="width:30%;float:left;">Suite</div></div>
				<div class="form-group" style="padding:5px"><div style="width:35%;float:left;">City :</div><div style="width:35%;float:left;">State :</div><div style="width:30%;float:left;">Zip :</div></div>
				<div class="form-group" style="padding:5px;"><div style="width:100%;float:left;">Ordering Physician</div></div>
				<div class="form-group" style="padding:5px;"><div style="width:100%;float:left;">Phone</div></div>
				<div class="form-group" style="padding:5px;"><div style="width:100%;float:left;">Fax</div></div>
			</div>
		  </div>
		  <div class="clear" style="clear:both"></div>
		 <h2 style="background:#cfe1f4;font-size:12px;font-weight:normal;margin:10px 0;padding:5px;border:1px solid #000;">Primary Billing/Insurance Information <strong>(Attach a copy of insurance card - both sides)</strong></h2>
		  <div class="col-md-6" style="width:49%;float:left;">
		  	<div class="box" style="border:1px solid #000;">
		  		<h4 style="margin:5px 0;font-weight:normal;text-align:center;">SUBSCRIBER PRIMARY INSURANCE</h4>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="float:left;width:35%;">Name/Relationship</div><div style="float:left; width:20%"><input type="checkbox" name="bill[]" id="bill_insurence">Self</div><div style="float:left;width:20%"><input type="checkbox" name="bill[]" id="bill_patient">Spouse</div><div style="float:left;width:22%"><input type="checkbox" name="bill[]" id="bill_patient">Dependent</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Insurance Name</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Address</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:35%;float:left;">City :</div><div style="width:35%;float:left;">State :</div><div style="width:30%;float:left;">Zip :</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:59%;float:left;">Employer Name</div><div style="width:40%;float:left;">Member ID#</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Group/Contract #</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:50%;float:left;">Subscriber DOB</div><div style="width:9%;float:left;">Sex</div><div style="float:left; width:20%"><input type="checkbox" name="bill[]" id="bill_insurence">Male</div><div style="float:left;width:20%"><input type="checkbox" name="bill[]" id="bill_patient">Female</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:49%;float:left;">Medicare ID#</div><div style="width:48%;float:left;border-left:1px solid #000;padding-left:2%;">Medicaid ID#</div></div>
		  	</div>
		  </div>
		  <div class="col-md-6" style="width:49%;float:right;">
		  	<div class="box" style="border:1px solid #000;">
		  		<h4 style="margin:5px 0;font-weight:normal;text-align:center;">SUBSCRIBER SECONDARY INSURANCE</h4>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="float:left;width:35%;">Name/Relationship</div><div style="float:left; width:20%"><input type="checkbox" name="bill[]" id="bill_insurence">Self</div><div style="float:left;width:20%"><input type="checkbox" name="bill[]" id="bill_patient">Spouse</div><div style="float:left;width:22%"><input type="checkbox" name="bill[]" id="bill_patient">Dependent</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Insurance Name</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Address</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:35%;float:left;">City :</div><div style="width:35%;float:left;">State :</div><div style="width:30%;float:left;">Zip :</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:59%;float:left;">Employer Name</div><div style="width:40%;float:left;">Member ID#</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:100%;float:left;">Group/Contract #</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:50%;float:left;">Subscriber DOB</div><div style="width:9%;float:left;">Sex</div><div style="float:left; width:20%"><input type="checkbox" name="bill[]" id="bill_insurence">Male</div><div style="float:left;width:20%"><input type="checkbox" name="bill[]" id="bill_patient">Female</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;border-top:1px solid #000;"><div style="width:49%;float:left;">Medicare ID#</div><div style="width:48%;float:left;border-left:1px solid #000;padding-left:2%;">Medicaid ID#</div></div>
		  	</div>
		  </div>
		<h2 style="background:#cfe1f4;font-size:12px;font-weight:normal;margin:10px 0 20px;padding:5px;border:1px solid #000;text-align:center">Additional Clinical Information/DIAGNOSIS CODES (If clinical image is available please print and attach)</h2>
		<h2 style="background:#cfe1f4;font-size:12px;font-weight:normal;margin:10px 0;padding:5px;border:1px solid #000;">Clinical Information</h2>
		 <div class="box" style="border:1px solid #000;">
		 	<div class="col-md-4" style="width:39%;float:left;border-right:1px solid #000;padding:10px 0;">
				<div class="form-group" style="padding:4px 0 4px 5px;"><div style="float:left;width:35%;">Specimen #1</div><div style="float:left; width:30%"><input type="checkbox" name="bill[]" id="bill_insurence">Right</div><div style="float:left;width:30%"><input type="checkbox" name="bill[]" id="bill_patient">Left</div><div style="float:left;width:30%;padding-left:35%;"><input type="checkbox" name="bill[]" id="bill_patient">Biopsy</div><div style="float:left;width:30%"><input type="checkbox" name="bill[]" id="bill_patient">Excision</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;">Nail Unity (Histopathology. Choose stain(s) (Optional)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Higher Sensitivity and melanin screen(PAS/GMSFM) (Dematiaceous fungi/Melanoma</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Higher Sensitivity (PAS/GMS)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Routine (PAS)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;margin:10px 0 0;">FUNGAL SPECIATION (Typical added to avobe stains)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">PCR</div>
			</div>
			
			<div class="col-md-4" style="width:39%;float:left;padding:10px 0;">
				<div class="form-group" style="padding:4px 0 4px 5px;"><div style="float:left;width:35%;">Specimen #2</div><div style="float:left; width:30%"><input type="checkbox" name="bill[]" id="bill_insurence">Right</div><div style="float:left;width:30%"><input type="checkbox" name="bill[]" id="bill_patient">Left</div><div style="float:left;width:30%;padding-left:35%;"><input type="checkbox" name="bill[]" id="bill_patient">Biopsy</div><div style="float:left;width:30%"><input type="checkbox" name="bill[]" id="bill_patient">Excision</div></div>
				<div class="form-group" style="padding:4px 0 4px 5px;">Nail Unity (Histopathology. Choose stain(s) (Optional)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Higher Sensitivity and melanin screen(PAS/GMSFM) (Dematiaceous fungi/Melanoma</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Higher Sensitivity (PAS/GMS)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">Routine (PAS)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;margin:10px 0 0;">FUNGAL SPECIATION (Typical added to avobe stains)</div>
				<div class="form-group" style="padding:4px 0 4px 5px;"><input type="checkbox" name="nail_unit[]" class="form-control3" id="nail_unit_1">PCR</div>
			</div>
		</div>
		<h2 style="background:#cfe1f4;font-size:12px;font-weight:normal;margin:10px 0;padding:5px;border:1px solid #000;">Signature Required &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The requested test(s) is/are medically indicated for patient management</h2>
		<h2 style="background:#fffb00;font-size:12px;font-weight:normal;margin:10px 0;padding:5px;border:1px solid #000;">
			<div class="col-md-6" style="width:49%;float:left;"><div style="width:70%;float:left">Physician Signature</div><div style="width:30%;float:left">Date</div></div>
			<div class="col-md-6" style="width:49%;float:left;border-left:1px solid #000;padding-left:1%;"><div style="width:70%;float:left">Patient Signature</div><div style="width:30%;float:left">Date</div></div>
		</h2>    
         

  </div>		 
</div>		 