import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { map, startWith } from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

export interface StateGroup {
  ids: Number[];
  names: string[];
}


@Component({
  selector: 'cm-specimens-edit',
  templateUrl: './specimens-edit.component.html',
  styleUrls: ['./specimens-edit.component.css']
})
export class SpecimensEditComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  public url = environment.baseUrl + 'PdfGenerate/'
  public data: any;
  list: any = [];
  physicians_list: any = [];
  physicians_list_data: any = [];
  add_specimen: any = [];
  addSpecimen: FormGroup;
  public loading = false;
  sendData;
  specimenData: any = [];
  specimenInfo: any = [];
  postsArray: any = [];
  load_status: boolean = true;
  assn_no: any;
  // variables
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]//test
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  bsValue: Date = new Date();
  relations: Array<String> = ['Self', 'Spouse', 'Dependent'];
  // insurance_type: Array<String> = ['Commercial','Federal'];
  clinical_specimen: Array<String> = ['Right', 'Left', 'Biopsy', 'Excision'];
  bill: Array<String> = ['Insurance', 'Patient'];
  billData: any = [1, 2];
  nailUnit: Array<String> = ['Combination Testing (PCR and Histopathology - choose stains below)', 'PAS/GMS/FM - Higher Sensitivity and Melanin screen', 'PAS/GMS - Higher Sensitivity', 'Reflex to PCR for identification', 'PCR Only', 'Reflex to Histopathology to confirm presence of disease'];
  nailUnitData: any = [5, 1, 2, 6, 4, 7];
  //selectedClinicalSpecimen: any[][];
  selectedCheckBoxBill: any = [];
  //selectedNailUnit: any[][];
  billErrors: boolean = true;
  billValidCheck: boolean = false;
  relationValidCheck: boolean = false;
  nailUnitValidchk: boolean = false;
  nailUnitValid: boolean = false;
  nailUnitValidMsg: string = "Please check atlest one nail unit.";
  formData: any;
  specimenStatus: any;
  editType: any;
  typesArr: any;
  partners_company: any;
  partners_specimen_details: any;
  // variables
  test_type:string;
  process_done:string;
  partners_company_name:string;
  test_type_list:any;
  constructor(private http: HttpClient,
    public itemsService: SpecimenService,
    public route: Router,
    private fb: FormBuilder,
    private _ActivatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {

    this.addSpecimen = this.fb.group({
      user_id: [''],
      collection_date: ['', Validators.required],
      date_received: ['', Validators.required],
      p_lastname: ['', Validators.required],
      p_firstname: ['', Validators.required],
      specimen_id: [''],
      format: [''],
      bill: this.addControlsToArray(this.bill),
      billValidCheck: [this.billValidCheck, Validators.requiredTrue],
      patient_address: ['', Validators.required],
      apt: [''],
      patient_city: ['', Validators.required],
      patient_state: ['', Validators.required],
      patient_zip: ['', [Validators.required]],
      patient_phn: [''],
      patient_dob: ['', Validators.required],
      patient_age: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      patient_sex: ['', Validators.required],
      patient_id: [''],
      priSubscriberRelationValidCheck: [this.relationValidCheck, Validators.requiredTrue],
      pri_subscriber_relation: this.addControlsToArray(this.relations),
      pri_insurance_name: ['', Validators.required],
      pri_address: ['', Validators.required],
      pri_city: ['', Validators.required],
      pri_state: ['', Validators.required],
      pri_zip: ['N/A', [Validators.required]],
      insurance_type: ['', Validators.required],
      pri_employee_name: [''],
      pri_member_id: ['', Validators.required],
      pri_subscriber_dob: ['', Validators.required],
      pri_group_contact: ['', Validators.required],
      pri_sex: ['', Validators.required],
      pri_medicare_id: [''],
      pri_madicaid_id: [''],

      sec_subscriber_relation: this.addControlsToArray(this.relations),
      sec_insurance_name: [''],
      sec_address: [''],
      sec_city: [''],
      sec_state: [''],
      sec_zip: [''],
      sec_employee_name: [''],
      sec_member_id: [''],
      sec_subscriber_dob: [''],
      sec_group_contact: [''],
      sec_sex: [''],
      sec_medicare_id: [''],
      sec_medicaid_id: [''],

      physician_accepct: ['1'],
      time: ['PM'],
      wp_submit: ['Update Specimen'],
      physician_id: ['', Validators.required],
      physician_name: ['', Validators.required],
      test_type: [''],
      notes: [''],
      partners_company: [''],
      external_id: [''],
      specimen: this.fb.array([
        this.addSpecimenFormGroup(),
      ]),
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'MM/DD/YYYY'

    });
  }



  ngOnInit() {
    this.addSpecimen.controls['user_id'].setValue(localStorage.userid);
    this.getAllTestTypes();
    this.getPhysicians(); //Getting physician details
    this.setFormValue();



    /*........Calling physician autocomplete.........*/
    this.filteredOptions = this.addSpecimen.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value.toLowerCase()))
    );
    /*........End of calling physician autocomplete.........*/

  } //End of ngOnInit

  getAllTestTypes() {
    this.itemsService.getAllTestTypes().subscribe(data => {
      let postData = data;
      this.typesArr = postData['test_types'];
    });
  }

  /*..................Assigning Edit Data...................*/
  setFormValue() {
    this.addSpecimen.controls['user_id'].setValue(localStorage.userid);
    const id = +this._ActivatedRoute.snapshot.paramMap.get('id');
    this.editType = this._ActivatedRoute.snapshot.paramMap.get('type');
    if (this.editType === 'qcCheck') {
      this.addSpecimen.controls['wp_submit'].setValue('Update & Save');
      this.itemsService.makeSpecimenViewRequest(id).subscribe(data => {
        this.specimenData = data;
        this.specimenInfo = this.specimenData.spe_det[0];
        if (this.specimenInfo['lab_id'] == localStorage.userid) {
          this.route.navigateByUrl('/restricted');
        }

      });
    }
    this.spinner.show();
    let params = { 'id': id, 'dataType': "general" }
    this.itemsService.makeSpecimenViewRequest(params).subscribe(data => {
      this.specimenData = data;
      console.log(this.specimenData )
      this.specimenInfo = this.specimenData.spe_det[0];
      let physiciansList = this.specimenData.phy_det[0];
      this.test_type_list = this.specimenData.test_types;
      this.assn_no = this.specimenInfo['assessioning_num'];
      let bill = this.specimenData.bill;
      let subscriber = this.specimenData.subscriber;
      let subscriberSec = this.specimenData.subscriber_sec;
      let insurance_type = this.specimenData.insurance_type;
      let clinicalSpecimen = this.specimenData.clinical_specimen;
      let nailUnit = this.specimenData.nail_unit;
      this.physicians_list_data = this.specimenData.phy_det[0];
      this.addSpecimen.controls['specimen_id'].setValue(id);
      if (!this.assn_no) { 
        this.addSpecimen.controls['test_type'].setValue(this.specimenInfo['test_type']);
      }

      if(this.specimenInfo['test_type']){

        this.test_type_list.forEach(elem=>{
          if( elem.type_code === this.specimenInfo['test_type']){
            this.test_type = elem.type_name;
          }
        })
        if(this.specimenInfo['test_type'] === 'W'){
          this.process_done = this.specimenInfo['process_done'];
        }

        
      }
      this.partners_company = this.specimenInfo['partners_company'];

      if(this.partners_company){
        this.partners_company_name = this.specimenInfo['partners_company_name'];
        console.log(this.partners_company_name)
      }

      this.partners_specimen_details = this.specimenData.partners_specimen_details;
      if (this.partners_specimen_details) {
        this.addSpecimen.controls['external_id'].setValue(this.partners_specimen_details.external_id);
      }
      this.addSpecimen.controls['collection_date'].setValue(this.specimenInfo['collection_date']);
      this.addSpecimen.controls['date_received'].setValue(this.specimenInfo['date_received']);
      this.addSpecimen.controls['patient_dob'].setValue(this.specimenInfo['patient_dob']);
      this.addSpecimen.controls['p_lastname'].setValue(this.specimenInfo['p_lastname']);
      this.addSpecimen.controls['p_firstname'].setValue(this.specimenInfo['p_firstname']);
      this.addSpecimen.controls['patient_address'].setValue(this.specimenInfo['patient_address']);
      this.addSpecimen.controls['apt'].setValue(this.specimenInfo['apt']);
      this.addSpecimen.controls['patient_city'].setValue(this.specimenInfo['patient_city']);
      this.addSpecimen.controls['patient_state'].setValue(this.specimenInfo['patient_state']);
      this.addSpecimen.controls['patient_zip'].setValue(this.specimenInfo['patient_zip']);
      this.addSpecimen.controls['patient_phn'].setValue(this.specimenInfo['patient_phn']);
      this.addSpecimen.controls['patient_id'].setValue(this.specimenInfo['patient_id']);
      this.addSpecimen.controls['patient_sex'].setValue(this.specimenInfo['patient_sex']);
      this.addSpecimen.controls['pri_insurance_name'].setValue(this.specimenInfo['pri_insurance_name']);
      this.addSpecimen.controls['pri_address'].setValue(this.specimenInfo['pri_address']);
      this.addSpecimen.controls['pri_city'].setValue(this.specimenInfo['pri_city']);
      this.addSpecimen.controls['pri_state'].setValue(this.specimenInfo['pri_state']);
      if (!this.specimenInfo['pri_zip']) {
        this.addSpecimen.controls['pri_zip'].setValue('N/A');
      }
      else {
        this.addSpecimen.controls['pri_zip'].setValue(this.specimenInfo['pri_zip']);
      }
      this.addSpecimen.controls['pri_employee_name'].setValue(this.specimenInfo['pri_employee_name']);
      this.addSpecimen.controls['pri_member_id'].setValue(this.specimenInfo['pri_member_id']);
      this.addSpecimen.controls['pri_subscriber_dob'].setValue(this.specimenInfo['pri_subscriber_dob']);
      this.addSpecimen.controls['pri_group_contact'].setValue(this.specimenInfo['pri_group_contact']);
      this.addSpecimen.controls['pri_sex'].setValue(this.specimenInfo['pri_sex']);
      this.addSpecimen.controls['pri_medicare_id'].setValue(this.specimenInfo['pri_medicare_id']);
      this.addSpecimen.controls['pri_madicaid_id'].setValue(this.specimenInfo['pri_madicaid_id']);
      this.addSpecimen.controls['sec_insurance_name'].setValue(this.specimenInfo['sec_insurance_name']);
      this.addSpecimen.controls['sec_address'].setValue(this.specimenInfo['sec_address']);
      this.addSpecimen.controls['sec_city'].setValue(this.specimenInfo['sec_city']);
      this.addSpecimen.controls['sec_state'].setValue(this.specimenInfo['sec_state']);
      this.addSpecimen.controls['sec_zip'].setValue(this.specimenInfo['sec_zip']);
      this.addSpecimen.controls['sec_employee_name'].setValue(this.specimenInfo['sec_employee_name']);
      this.addSpecimen.controls['sec_member_id'].setValue(this.specimenInfo['sec_member_id']);
      this.addSpecimen.controls['sec_subscriber_dob'].setValue(this.specimenInfo['sec_subscriber_dob']);
      this.addSpecimen.controls['sec_group_contact'].setValue(this.specimenInfo['sec_group_contact']);
      this.addSpecimen.controls['sec_sex'].setValue(this.specimenInfo['sec_sex']);
      this.addSpecimen.controls['sec_medicare_id'].setValue(this.specimenInfo['sec_medicare_id']);
      this.addSpecimen.controls['sec_medicaid_id'].setValue(this.specimenInfo['sec_medicaid_id']);
      this.addSpecimen.controls['physician_accepct'].setValue(this.specimenInfo['physician_accepct']);
      this.addSpecimen.controls['time'].setValue(this.specimenInfo['collection_time'].substr(0, 5));
      this.addSpecimen.controls['format'].setValue(this.specimenInfo['collection_time'].substr(-2));
      this.addSpecimen.controls['physician_name'].setValue(physiciansList['name']);
      this.addSpecimen.controls['physician_id'].setValue(this.specimenInfo['physician_id']);
      this.addSpecimen.controls['partners_company'].setValue(this.specimenInfo['partners_company']);
      this.addSpecimen.controls['insurance_type'].setValue(this.specimenInfo['ins_type']);
      console.log(this.specimenInfo['insurance_type']);
      this.addSpecimen.controls['bill']['controls'].forEach((element, indx) => {
        element.setValue(bill[indx]);
      });
      this.addSpecimen.controls['pri_subscriber_relation']['controls'].forEach((element, indx) => {
        element.setValue(subscriber[indx]);
      });
      this.addSpecimen.controls['sec_subscriber_relation']['controls'].forEach((element, indx) => {
        element.setValue(subscriberSec[indx]);
      });

      this.addSpecimen.controls['specimen']['controls'].forEach(element => {
        element.controls['assessioning_num'].setValue(this.specimenInfo['assessioning_num']);
        element.controls['clinicalSpecimenValidCheck'].setValue(this.checkArray(nailUnit));
        element.controls['site_indicator'].setValue(this.specimenInfo['site_indicator']);
        element.controls['nail_unit'].controls.forEach((element, indx) => {
          element.setValue(nailUnit[indx]);
        });
        element.controls['clinical_specimen'].controls.forEach((element, indx) => {
          element.setValue(clinicalSpecimen[indx]);
        });
      });
      this.addSpecimen.controls['billValidCheck'].setValue(this.checkArray(bill));
      this.addSpecimen.controls['priSubscriberRelationValidCheck'].setValue(this.checkArray(subscriber));
      this.specimenStatus = this.specimenInfo['status'];
      if (this.partners_company) {
        this.addSpecimen.controls['billValidCheck'].setErrors(null);
        this.addSpecimen.controls['insurance_type'].setErrors(null);
        this.addSpecimen.controls['physician_id'].setErrors(null);
      }
      this.spinner.hide();
    });

  }
  /*..................End of assigning Edit Data...................*/

  /*..............Get physician function.............*/
  getPhysicians() {
    this.spinner.show();
    this.itemsService.physicians_list().subscribe(data => {
      this.list = data;
      this.physicians_list = this.list.physicians_data;
      this.physicians_list.map((option) => this.allNames.push(option.physician_name));
      this.physicians_list.map((val) => this.allIds.push(val.id));
      this.physicians_list.map((data) => this.filterVal.push(data));
      this.spinner.hide();
    });
    this.stateGroups = [{
      ids: this.allIds,
      names: this.allNames
    }];
  }
  /*..............End of get physician function.............*/

  /*..............Specimen add more and delete button.............*/
  add_specimen_data(): void {
    (<FormArray>this.addSpecimen.get('specimen')).push(this.addSpecimenFormGroup());
  }
  delete_specimen_data(spIndex: number): void {
    (<FormArray>this.addSpecimen.get('specimen')).removeAt(spIndex);
  }
  /*..............End of specimen add more and delete button.............*/

  /*..............Add more Specimen function.............*/
  addSpecimenFormGroup(): FormGroup {
    return this.fb.group({
      assessioning_num: [''],
      clinicalSpecimenValidCheck: [this.nailUnitValidchk, Validators.requiredTrue],
      clinical_specimen: this.addControlsToArray(this.clinical_specimen),
      nail_unit: this.addControlsToArray(this.nailUnit),
      site_indicator: [''],
    });
  }
  /*..............End of add more Specimen function.............*/

  /*..............Form submit function.............*/
  onSubmit() {
    this.nailUnitValidMsg = "Please check atlest one nail unit.";
    console.log(this.addSpecimen);
    if (this.addSpecimen.status == "VALID") {
      this.loading = true;
      let fmData = Object.assign({}, this.addSpecimen.value);
      fmData = Object.assign(fmData, { collection_date: this.convertDate(fmData.collection_date) });
      fmData = Object.assign(fmData, { date_received: this.convertDate(fmData.date_received) });
      fmData = Object.assign(fmData, { patient_dob: this.convertDate(fmData.patient_dob) });
      fmData = Object.assign(fmData, { pri_subscriber_dob: this.convertDate(fmData.pri_subscriber_dob) });
      fmData = Object.assign(fmData, { sec_subscriber_dob: this.convertDate(fmData.sec_subscriber_dob) });
      fmData = Object.assign(fmData, { patient_phn: this.phone_format(fmData.patient_phn) });
      fmData = Object.assign(fmData, { bill: fmData.bill.map((v, i) => v ? this.billData[i] : null) });
      fmData = Object.assign(fmData, { pri_subscriber_relation: fmData.pri_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      fmData = Object.assign(fmData, { sec_subscriber_relation: fmData.sec_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      // fmData = Object.assign(fmData,{insurance_type:fmData.insurance_type.map((v,i)=>v?this.insurance_type[i]:null)});

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { clinical_specimen: elem.clinical_specimen.map((el, j) => el ? this.clinical_specimen[j] : null) });

        });
      });

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { nail_unit: elem.nail_unit.map((el, j) => el ? this.nailUnitData[j] : null) });

        });
      });

      let formObj = { ...fmData }; // {name: '', description: ''}
      let serializedForm = JSON.stringify(formObj);
      this.itemsService.edit_specimen(serializedForm).subscribe(data => {
        console.log(data);
        this.setFormValue();
        if (data.hasOwnProperty("external_id_err")) {
          this.toastr.info(data['external_id_err'], 'Error', {
            timeOut: 3000
          });
        }
        if (data['update_success'] == '1') {
          this.toastr.info('Data Successfully updated', 'Success', {
            timeOut: 3000
          });
          if (data['qrCode'] != '') {
            data['qrCode'].forEach((element, ind) => {
              this.print(data['assessioning_num'][ind], element, data['base_url']);
              this.print(data['assessioning_num'][ind], element, data['base_url']);
            });
          }

          this.route.navigateByUrl(data['returnUrl']);
        }
      });
    }
    else {
      this.toastr.info('Please fill all required fields.', 'Error', {
        timeOut: 3000
      });
      let invalidFields = [].slice.call(document.getElementsByClassName('ng-invalid'));
      invalidFields[1].focus();
      this.validateAllFormFields(this.addSpecimen);
    }
  }
  /*..............End of form submit function.............*/

  /*..............Autocomplete custom filter function.............*/
  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];
      return this.searchData;
    }
    return this.stateGroups;
  }
  /*..............End of autocomplete custom filter function.............*/

  /*..............Form validation functions.............*/
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.addSpecimen.get(field).valid && (this.addSpecimen.get(field).touched || this.addSpecimen.get(field).dirty);
  }

  isNailUnitValid(firstField: any) {
    return !firstField.valid && (this.addSpecimen.get('wp_submit').touched || this.addSpecimen.get('specimen').touched);
  }
  /*..............End of Form validation functions.............*/

  /*..............Form validation functions.............*/

  /*..............Checkbox add and checkbox control data functions.............*/
  addControlsToArray(data: any) {
    const arr = data.map(elem => {
      return this.fb.control(false);
    });
    console.log(arr);
    return this.fb.array(arr);
  }

  get specimenArray() {
    return <FormArray>this.addSpecimen.get('specimen');
  }
  get relationArray() {
    return <FormArray>this.addSpecimen.get('pri_subscriber_relation');
  }

  get relationArraySec() {
    return <FormArray>this.addSpecimen.get('sec_subscriber_relation');
  }

  // get getInsuranceTypeArr(){

  //   return <FormArray>this.addSpecimen.get('insurance_type');
  // }

  get getBillArr() {

    return <FormArray>this.addSpecimen.get('bill');
  }

  get getClinicalSpecimenArr() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    let spcFrmGrp: any = [];
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      <FormArray>spcFrmGrp.push(<FormArray>innerGroup.controls['clinical_specimen']);
    }
    return spcFrmGrp;
  }

  get getNailUnitArr() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    let spcFrmGrp: any = [];
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      <FormArray>spcFrmGrp.push(<FormArray>innerGroup.controls['nail_unit']);
    }
    return spcFrmGrp;
  }
  /*..............End of checkbox add and checkbox control data functions.............*/

  /*..............Checkbox validate functions.............*/
  getBillVal() {
    let count = 0;
    this.getBillArr.controls.forEach((elm, i) => {
      elm.value ? count++ : '';
    });
    this.billValidCheck = count > 0 ? true : false;
    if (this.partners_company) {
      this.addSpecimen.controls['billValidCheck'].setValue(true);
    } else {
      this.addSpecimen.controls['billValidCheck'].setValue(this.billValidCheck);
    }

  }

  getSubscriberVal() {
    let count = 0;
    let patient_dob;
    let patient_sex;
    this.relationArray.controls.forEach((elm, i) => {
      elm.value ? count++ : '';

      if (elm.value && i === 0) {
        patient_dob = this.addSpecimen.controls['patient_dob'].value;
        patient_sex = this.addSpecimen.get('patient_sex').value;
        this.addSpecimen.controls['pri_subscriber_dob'].setValue(patient_dob);
        this.addSpecimen.controls['pri_sex'].setValue(patient_sex);
      }
    });
    this.relationValidCheck = count > 0 ? true : false;
    this.addSpecimen.controls['priSubscriberRelationValidCheck'].setValue(this.relationValidCheck);
  }

  getNailUnitVal() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      let spcFrmGrp = <FormArray>innerGroup.controls['nail_unit'];
      let st1: boolean = false;
      let st2: boolean = false;
      let st3: boolean = false;
      let st4: boolean = false;
      let st5: boolean = false;
      spcFrmGrp.controls.forEach((eml, ind) => {
        if ((ind === 0 && eml.value)) {
          st1 = true;
        }
        if ((ind === 3 && eml.value)) {
          st2 = true;
        }
        if (ind === 5 && eml.value) {
          st4 = true;
        }
        if (ind === 4 && eml.value) {
          st5 = true;
        }

        if ((ind === 1 && eml.value) || (ind === 2 && eml.value)) {
          st3 = true;
        }

      });
      if (!st1 && !st2 && !st4 && !st3 && !st5) {
        this.nailUnitValid = false;
        this.nailUnitValidMsg = "Please check atlest one nail unit.";
        console.log(this.nailUnitValid);
      } else if ((st1 || st2 || st4) && !st3) {
        this.nailUnitValid = false;
        this.nailUnitValidMsg = "Please check at least one between PAS/GMS/FM or PAS/GMS.";
      } else {
        this.nailUnitValid = true;
        this.nailUnitValidMsg = "";
      }

      innerGroup.controls['clinicalSpecimenValidCheck'].setValue(this.nailUnitValid);
      console.log('s1: ' + st1 + ' s2: ' + st2 + ' s3: ' + st3 + ' s4: ' + st4 + ' s5: ' + st5);

      console.log(spcFrm);
    }
  }
  /*..............End of checkbox validate functions.............*/

  /*..............Convert date function.............*/
  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [mnth, day, date.getFullYear()].join("/");
    }
    return newDate;
  }
  /*..............End of convert date function.............*/

  /*..............Calculate age function.............*/
  onChangeCalculateAge(value: Date) {
    if (value) {
      var today = new Date();
      var diff = Math.abs(today.getTime() - value.getTime());
      var diffYears = Math.floor(diff / (1000 * 3600 * 24 * 365));
      this.addSpecimen.controls['patient_age'].setValue(diffYears);
    }
  }
  /*..............End of calculate age function.............*/

  /*..............Display physician details function.............*/
  callSomeFunction(phy_id: any) {
    this.addSpecimen.controls['physician_id'].setValue(phy_id);

    this.itemsService.getPhisicianDetails(phy_id).subscribe(data => {
      this.postsArray = data;
      if (this.postsArray.status === '1') {
        this.physicians_list_data = this.postsArray.phy_det[0];
      } else {
        this.physicians_list_data = '';
      }
    });
  }
  /*..............End of display physician details function.............*/

  /*..............Print window qrcode function.............*/
  print(assessioning_num: any, qrname: any, qr_path: any): void {
    let printContents, popupWin;
    //printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">
    <div id="${assessioning_num}_1;?>" class="print_qr_code">
    <img src='${qr_path}assets/uploads/qr_images/${qrname}'>
    <p style='font-size:15px; margin-top: -12px;'><strong>${assessioning_num}</strong></p>
    </div>
    </body>
      </html>`
    );
    popupWin.document.close();
  }
  genarateAddSpecimenPdf() {
    this.itemsService.makeSpecimenPdf().subscribe(data => {
      window.open(data['base_url'] + data['pdf_name'], "_blank");
    });
  }
  /*..............End of print window qrcode function.............*/

  /*..............Array empty check function.............*/
  checkArray(my_arr) {
    var count = 0;
    for (var i = 0; i < my_arr.length; i++) {
      if (my_arr[i] !== "")
        count++;
    }
    return (count) > 0 ? true : false;
  }
  /*..............End of array empty check function.............*/

  qcCheckList() {
    // let specimen_id = this.addSpecimen.get('specimen_id').value
    // this.itemsService.sendToLab(specimen_id).subscribe(data => {
    //   if(data['status'] == '1'){
    //     this.toastr.info('Data Successfully updated', 'Success', {
    //       timeOut: 3000
    //     });
    //   this.route.navigateByUrl('qc-confirmation');
    //     // this.print(data['assessioning_num'],data['qrCode'],data['base_url']);
    //     // this.print(data['assessioning_num'],data['qrCode'],data['base_url']);
    //     // 
    //   }
    // });
    this.nailUnitValidMsg = "Please check atlest one nail unit.";
    console.log(this.addSpecimen);
    if (this.addSpecimen.status == "VALID") {
      this.loading = true;
      let fmData = Object.assign({}, this.addSpecimen.value);
      fmData = Object.assign(fmData, { collection_date: this.convertDate(fmData.collection_date) });
      fmData = Object.assign(fmData, { date_received: this.convertDate(fmData.date_received) });
      fmData = Object.assign(fmData, { patient_dob: this.convertDate(fmData.patient_dob) });
      fmData = Object.assign(fmData, { pri_subscriber_dob: this.convertDate(fmData.pri_subscriber_dob) });
      fmData = Object.assign(fmData, { sec_subscriber_dob: this.convertDate(fmData.sec_subscriber_dob) });
      fmData = Object.assign(fmData, { patient_phn: this.phone_format(fmData.patient_phn) });
      fmData = Object.assign(fmData, { bill: fmData.bill.map((v, i) => v ? this.billData[i] : null) });
      fmData = Object.assign(fmData, { pri_subscriber_relation: fmData.pri_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      fmData = Object.assign(fmData, { sec_subscriber_relation: fmData.sec_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      // fmData = Object.assign(fmData,{insurance_type:fmData.insurance_type.map((v,i)=>v?this.insurance_type[i]:null)});

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { clinical_specimen: elem.clinical_specimen.map((el, j) => el ? this.clinical_specimen[j] : null) });

        });
      });

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { nail_unit: elem.nail_unit.map((el, j) => el ? this.nailUnitData[j] : null) });

        });
      });

      let formObj = { ...fmData }; // {name: '', description: ''}
      let serializedForm = JSON.stringify(formObj);
      this.itemsService.sendToLab(serializedForm).subscribe(data => {
        console.log(data);
        this.setFormValue();
        if (data.hasOwnProperty("external_id_err")) {
          this.toastr.info(data['external_id_err'], 'Error', {
            timeOut: 3000
          });
        }
        if (data['update_success'] == '1') {
          this.toastr.info('Data Successfully updated', 'Success', {
            timeOut: 3000
          });
          if (data['qrCode'] != '') {
            data['qrCode'].forEach((element, ind) => {
              this.print(data['assessioning_num'][ind], element, data['base_url']);
              this.print(data['assessioning_num'][ind], element, data['base_url']);
            });
          }
          this.route.navigateByUrl('/qc-confirmation');
        }
      });
    }
    else {
      this.toastr.info('Please fill all required fields.', 'Error', {
        timeOut: 3000
      });
      let invalidFields = [].slice.call(document.getElementsByClassName('ng-invalid'));
      invalidFields[1].focus();
      this.validateAllFormFields(this.addSpecimen);
    }
  }

  //Custom Validators----------

  ageRangeValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && (isNaN(control.value) || control.value < 18 || control.value > 45)) {
      return { 'ageRange': true };
    }
    return null;
  }

  nailUnitValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && (isNaN(control.value) || control.value < 18 || control.value > 45)) {
      console.log(control.value);
      return { 'nailUnit': true };
    }
    return null;
  }

  onEnter(evt: any) {
    this.physicians_list_data = '';
    this.addSpecimen.controls['physician_id'].setValue('');
  }
  phone_format(phone) {
    var city, number;
    let ph_no = phone;
    var value = ph_no.toString().trim().replace(/^\+/, '');
    city = value.slice(0, 3);
    number = value.slice(3);
    number = number.slice(0, 3) + '-' + number.slice(3);
    number = city + "-" + number.trim();
    return number;
  }

  emptyCheck(e) {
    if (!e.target.value) {
      this.addSpecimen.controls['external_id'].setErrors({ required: true });
    } else {
      this.addSpecimen.controls['external_id'].setErrors(null);
    }
  }
}
