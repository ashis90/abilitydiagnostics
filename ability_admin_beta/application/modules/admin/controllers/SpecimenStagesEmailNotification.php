<?php if (!defined('BASEPATH')) EXIT("No direct script access allowed");
class SpecimenStagesEmailNotification extends MY_Controller 
{
     function __construct() 
     {
        parent::__construct();
        date_default_timezone_set('MST7MDT');
     }
     function index()
     {}
 
   /**
   * 
   * Get HISTO Avg time
   */
  
    function specimenStagesTatEmailNotification()
    {  
      
    $mail_format = "<div class='cont_wrapperInner' style='border:4px solid #34b6e6;width:850px;padding:10px;'>
	<p style='text-align: center'><img src='https://www.abilitydiagnostics.com/abadmin/assets/frontend/main_images/logo.png' alt='Ability Diagonostics' align='middle'></p>
	
    <p style='font:normal 13px/18px Arial, Helvetica, sans-serif; font-weight:bold; border:none;'>Hi, Admin <br></p>
	<p style='font:normal 13px/18px Arial, Helvetica, sans-serif; margin-bottom:0px; color:#000000;'> Below is a list of the Specimens where Specimen go beyond their given Turn Around Time(TAT).</p>";
	
   
    /**
	* 
	* @var Current Date and Time
	* 
	*/
   
    $current_datetime     = date('Y-m-d H:i:s');  
    $current_date         = date('Y-m-d'); 
	$current_timestamp    = strtotime($current_datetime);  
   
    /**
	* 
	* @var HISTO Specimen
	* 
	*/
	
    $details = array(); 
    $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
    (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` >= DATE(NOW()) - INTERVAL 7 DAY AND `id` NOT IN 
    (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
    (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `id` DESC");
   
    if($specimen_results)
    {
    	
 	$mail_format .="<table width='100%' border='0' cellpadding='2' cellspacing='2' style='background:none; border:none;margin-top:25px;'>
	
	<tr>
	<td colspan=8>
	<h3 style='color:#000000;'>Below is a list of the HISTO Specimens Stages and Reports Details.</h3>
	</td>
	</tr>
	<tr>
	<th width='15%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Accessioning No.</strong></th>
	<th width='15%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Delivery to Lab</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Gross</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Microtomy</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>QC Check </strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Pending Specimen List</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Pending Reports</strong></th>
	</tr>";
	
	/**
	* 
	* Ability HISTO Process
	* 
	*/

    foreach ($specimen_results as $specimen_data)
    {    	
        $specimen_create_date  = strtotime($specimen_data['create_date']);  
        $specimen_date         = date('Y-m-d', $specimen_create_date);
		
		/**
		* 
		* @var Weekend Checking
		* 
		*/
		
		$weekend_days = Array ();

		for($i=$specimen_create_date; $i<=strtotime($current_date); $i=$i+86400) {

		if(date("w",$i) == 6) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		if(date("w",$i) == 0) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		}


		/**
		* 
		* @var Holiday List 
		* 
		*/
		$holiday_sql = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' AND `start_date` BETWEEN '".$specimen_date."' AND '".$current_date."'";
        $holiday_range = $this->BlankModel->customquery($holiday_sql);  
       
       
        $holiday_count = 0;
        if($holiday_range){
		
		foreach($holiday_range as $holidays ){
          $holiday_list[] = getDatesFromRange($holidays['start_date'], $holidays['end_date']);
        }
                
        $holidays_list = $this->array_flatten($holiday_list);
	
		if($holidays_list){
			
			foreach($holidays_list as $holiday){		
		    $holiday_data = check_in_range($specimen_date, $current_date, $holiday);  
		    
		    if($holiday_data == '1'){
			   $holiday_count += $holiday_data;
			 }
		    }
		   }
	    }
	
	    $total_exclude_days = count($weekend_days) + $holiday_count;
	    $total_seconds_calculation   = floor(abs($current_timestamp - $specimen_create_date));  
	    $exclude_second = 0;
	    if($total_exclude_days>0){
		   $exclude_second = $total_exclude_days*86400; 
		}
	 
	    $seconds_calculation   = ($total_seconds_calculation - $exclude_second);   		
    	/**
		* 
		* @var Delivary to Lab 24 hrs.
		* 
		*/
       
        $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
      
         $stage1 = '';      
        //Check Stage Completed or Not.
        if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
        {              
           $color1 = 'Green';                    
        }
        else
        {              
          if($seconds_calculation > 86400) // 24 hrs.
          {
            $color1 = 'Red';
            $stage1 = 'Delivary to Lab';        
          }
          else
          {
           $color1 = 'Green';                
          } 
        }
        
        /**
		* 
		* @var Gross Description 8hrs.
		* 
		*/
        
        $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
          $stage2 = ''; 
           //Check Stage Completed or Not.
          if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
          {
            $color2 = 'Green';    
          }
          else
          {                  
           if($seconds_calculation > 28800)  // 8 hrs.
           {
            $color2 = 'Red'; 
            $stage2 = 'Gross Description';                
           }
           else
           {
            $color2 = 'Green';            
           }
          }

			/**
			* 
			* @var Microtomy  24 hrs.>86400) // 24 hrs.
			* 
			*/
          $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
         if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
          {
              $color3 = 'Green';    
          }  
          else
          {                 
            if($seconds_calculation > 86400) // 24 hrs.
            {
               $color3 = 'Red';
            }
            else 
            {
               $color3 = 'Green';
            } 
              
          }

          /**
		  * 
		  * @var QC Check  6 hrs.
		  * 
		  */
		  
          $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
          if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
          {
                $color4 = 'Green';    
          }
          else
          {
            if($seconds_calculation > 21600) // 6 hrs.
            {
                $color4 = 'Red';
            }
            else 
            {
                $color4 = 'Green';
            } 
          }

           /**
		   * 
		   * @var Pending Specimen List 48 hrs.
		   * 
		   */

            $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
            if(!empty($report_sql[0]['create_date']))
            {
                 $color5 = 'Green';  
                 $pending_specimen_status =  'Report Generated';                  
            }
            else
            {             
              if($seconds_calculation > 172800) // 48 hrs.
              {
                 $color5 = 'Red';
                 $pending_specimen_status =  'Report Not Generated';  
              }
              else
              {
                 $color5 = 'Green';
                 $pending_specimen_status =  'Not Pass TAT';  
              }
            }

			/**
			* 
			* @var Pending Report 48 hrs.
			* 
			*/

             $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."' AND `status` = 'Active' ");
            if(!empty($report_sql[0]['create_date']))
            {
               $color6 = 'Green';
               $pending_report_status =  'Report Submitted';    
            }
            else
            {                     
              if($seconds_calculation > 172800) // 48 hrs.
              {
                 $color6 = 'Red';
                 $pending_report_status =  'Report Not Submitted';    
              }
              else
              {
                 $color6 = 'Green';
                 $pending_report_status =  'Not Pass TAT';    
              }
            }

        $specimen_information =  array( 'accessioning_num' => $specimen_data['assessioning_num'], 
        								'specimen_id' => $specimen_data['id'],
						                'color1' => $color1, 
						                'color2' => $color2,
						                'color3' => $color3, 
						                'color4' => $color4,						                 
						                'color5' => $color5,
						                'pending_specimen_status' => $pending_specimen_status, 
						                'color6' => $color6,
						                'pending_report_status' => $pending_report_status
						                );
        array_push($details, $specimen_information); 
     }
    
    if(!empty($details))
    {	 
        $data_row = '0'; 	
	  foreach($details as $specimnen)
	  {	   	
	   if (in_array("Red", $specimnen))
       {
       $data_row = '1'; 
       $mail_format.= "<tr>
		<td valign='center'>
			<div style='text-align: center;'>".$specimnen['accessioning_num']."</div>
		</td>
	    
	    <td valign='center'>
			<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/1' target='_blank' title='Delivery To Lab' style='color: ".$specimnen['color1'].";'>".$specimnen['color1']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/2' target='_blank' title='Gross' style='color: ".$specimnen['color2'].";'>".$specimnen['color2']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/3' target='_blank' title='Microtomy' style='color: ".$specimnen['color3'].";'>".$specimnen['color3']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/4' target='_blank' title='QC Check' style='color: ".$specimnen['color4'].";'>".$specimnen['color4']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/add-specimen-report/".$specimnen['specimen_id']."' target='_blank' title='Pending Specimen' style='color: ".$specimnen['color5'].";'>".$specimnen['pending_specimen_status']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;' ><a href='javascript:void(0);' title='Pending Reports' style='color: ".$specimnen['color6'].";'> ".$specimnen['pending_report_status']."</a></div>
		</td>		
		</tr>"; 
        }
      
      }        
	  if($data_row != '1')
	   {
	    $mail_format.= "<tr>
			<td colspan=12>
				<div style='text-align: center;'>No HISTO Stages and Reports are Pending.</div>
			</td>
			</tr>";
	   } 
	 }    
  
   }
   
 
    /**
	*
	* 
	*  PCR Specimen
	* 
	* 
	*/

    $pcr_details = array();
    $pcr_specimen_results = $this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
    FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
    AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
    AND wp_abd_specimen.create_date >= DATE(NOW()) - INTERVAL 7 DAY ORDER BY `wp_abd_specimen`.`id` DESC");
   
    if($pcr_specimen_results)
    {

    $mail_format .="<tr>
    <td colspan=6>	
	   <h3 style='color:#000000;'>Below is a list of the PCR Specimens Stages and Reports Details.</h3>
	</td>
	</tr>
	
	<tr>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Accessioning No.</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Delivery to Extraction</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong> Assign Well Stages</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Data Analysis</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Report Generated </strong></th>
	</tr>";
    
    foreach ($pcr_specimen_results as $specimen_data)
    {       
     
        $specimen_create_date  = strtotime($specimen_data['create_date']);  
        $specimen_date         = date('Y-m-d', $specimen_create_date);
		
		/**
		* 
		* @var Weekend Checking
		* 
		*/
		
		$weekend_days = Array ();

		for($i=$specimen_create_date; $i<=strtotime($current_date); $i=$i+86400) {

		if(date("w",$i) == 6) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		if(date("w",$i) == 0) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		}


		/**
		* 
		* @var Holiday List 
		* 
		*/
		$holiday_sql = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' AND `start_date` BETWEEN '".$specimen_date."' AND '".$current_date."'";
        $holiday_range = $this->BlankModel->customquery($holiday_sql);  
       
       
        $holiday_count = 0;
        if($holiday_range){
		
		foreach($holiday_range as $holidays ){
          $holiday_list[] = getDatesFromRange($holidays['start_date'], $holidays['end_date']);
        }
                
        $holidays_list = $this->array_flatten($holiday_list);
	
		if($holidays_list){
			
			foreach($holidays_list as $holiday){		
		    $holiday_data = check_in_range($specimen_date, $current_date, $holiday);  
		    
		    if($holiday_data == '1'){
			   $holiday_count += $holiday_data;
			 }
		    }
		   }
	    }
	
	    $total_exclude_days = count($weekend_days) + $holiday_count;
	    $total_seconds_calculation   = floor(abs($current_timestamp - $specimen_create_date));  
	    $exclude_second = 0;
	    if($total_exclude_days>0){
		   $exclude_second = $total_exclude_days*86400; 
		}
	 
	    $pcr_seconds_calculation   = ($total_seconds_calculation - $exclude_second);   		
      
           
        /**
		* 
		* @var Delivery to Extraction
		* 
		*/
		
        $pcr_specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
        if(!empty($pcr_specimen_stage_results_stage1[0]['specimen_timestamp']))
        {
          $pcr_color1 = 'Green';   
        }
        else
        {
         if($pcr_seconds_calculation > 86400) // 24 hrs.
         {
          $pcr_color1 = 'Red';
         }
         else
         {
          $pcr_color1 = 'Green';   
         }
        }
        
        /**
		* 
		* @var Assign Well Stages
		* 
		*/
						
        $pcr_specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
      
         if(!empty($pcr_specimen_stage_results_stage2[0]['specimen_timestamp']))
          {
            $pcr_color2 = 'Green';
          }
          else
          {
           if($pcr_seconds_calculation > 86400) // 24 hrs
           {
            $pcr_color2 = 'Red';
           }
           else
           {
            $pcr_color2 = 'Green';
           }
          }
       
         /**
		  * 
		  * @var Data Analysis 72 hrs.
		  * 
		  */
		  
         $pcr_specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
        
          if(!empty($pcr_specimen_stage_results_stage3[0]['specimen_timestamp']))
          {
            $pcr_color3 = 'Green';
            $data_analysis_status = 'Process Done';    
          }  
          else
          {     
           if($pcr_seconds_calculation > 259600) // 72 hrs.
           {
            $pcr_color3 = 'Red';
            $data_analysis_status = 'Pending';
            
           }
           else 
           {
            $pcr_color3 = 'Green'; 
            $data_analysis_status = 'Not Pass TAT';
           } 
          
          }

		/**
		* 
		* @var Report Generated
		* 
		*/

        $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
            
            if(!empty($pcr_report_sql[0]['create_date']))
            {
               $pcr_color4 = 'Green';
               $pending_report_status =  'Report Submitted';    
            }
            else
            {                     
              if($pcr_seconds_calculation > 144000) // 4 hrs.
              {
                $pcr_color4 = 'Red';
                $pending_report_status =  'Report Not Generated';    
              }
              else
              {
                $pcr_color4 = 'Green';
                $pending_report_status =  'Not Pass TAT';    
              }
            }

         $pcr_specimen_information =  array( 
                                        'accessioning_num' => $specimen_data['assessioning_num'], 
        								'specimen_id'      => $specimen_data['id'],
						                'pcr_color1' => $pcr_color1, 
						                'pcr_color2' => $pcr_color2,
						                'pcr_color3' => $pcr_color3, 
						                'data_analysis_status'=> $data_analysis_status,
						                'pcr_color4' => $pcr_color4,
						                'pending_report_status' => $pending_report_status
						                );
        array_push($pcr_details, $pcr_specimen_information); 
            
       }
	   
     if(!empty($pcr_details))
     {	
       $data_row = '0';  	
	   foreach($pcr_details as $pcr_specimnen)
	   {
	 	if (in_array("Red", $pcr_specimnen))
        {
       $data_row = '1';  
       $mail_format.= "<tr>
		<td valign='center'>
			<div style='text-align: center;'>".$pcr_specimnen['accessioning_num']."</div>
		</td>
	    
	    <td valign='center'>
			<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/delivery-to-extraction-stage/".$pcr_specimnen['specimen_id']."/1' target='_blank' title='Delivery To Extraction ' style='color: ".$pcr_specimnen['pcr_color1'].";'>".$pcr_specimnen['pcr_color1']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/assign-well-stage' target='_blank' title='Assign Well Stage' style='color: ".$pcr_specimnen['pcr_color2'].";'>".$pcr_specimnen['pcr_color2']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/data-analysis/' target='_blank' title='Data Analysis Stage' style='color: ".$pcr_specimnen['pcr_color3'].";'>".$pcr_specimnen['data_analysis_status']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/report-generate' target='_blank' title='Report Generate' style='color: ".$pcr_specimnen['pcr_color4'].";'>".$pcr_specimnen['pending_report_status']."</a></div>
		</td>
		</tr>"; 
        }
      	   
	   }        
	 
	   if($data_row != '1')
	   {
	    $mail_format.= "<tr>
				<td colspan=12>
					<div style='text-align: center;'>No PCR Stages and Reports are Pending.</div>
				</td>
				</tr>";
	   } 
	  } 	   
	     
    }

   $mail_format.= "</table>
	<br>	
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Red'>Red:</span> Accessioning Number go Beyond Their TAT.</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Red'>Report Not Generated:</span> The Report Not Generated.</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Red'>Report Not Submitted:</span> The Report hasn't Submitted to the Physician Portal.</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Green'>Green:</span> Accessioning Number Process may be completed or it has time to process.</span> <br>

	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Green'>Not Pass TAT:</span> Accessioning Number has time to process.</span> <br>
	<br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Thanks,</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Support Team </span><br>
	<a href='mailto:support@abilitydiagnostics.com' target='_blank' style='font:normal 13px/18px Arial, Helvetica, sans-serif;color:#000000;'>support@abilitydiagnostics.com</a> 
	
	</div>";
	
	echo $mail_format;
	
	    $from = "support@abilitydiagnostics.com";
		  
		$config['mailtype'] = 'html';
		$config['charset']  = 'UTF-8';
		$config['newline']  = "\r\n";
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset']  = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

        $this->load->library('email',$config);
        $this->email->from($from, 'Ability Diagnostics');
        $this->email->to('nate@abilitydiagnostics.com');
       
        $this->email->subject('Specimens TAT Notification');
        $this->email->message($mail_format);
        $admin = $this->email->send(); 
     
    }

}       

?>