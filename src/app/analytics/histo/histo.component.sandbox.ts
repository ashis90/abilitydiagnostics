import { sandboxOf } from 'angular-playground';
import { HistoComponent } from './histo.component';

export default sandboxOf(HistoComponent)
  .add('Histo Component', {
    template: `<cm-histo></cm-histo>`
  });
