import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingSpecimensListComponent } from './pending-specimens-list.component';

describe('PendingSpecimensListComponent', () => {
  let component: PendingSpecimensListComponent;
  let fixture: ComponentFixture<PendingSpecimensListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingSpecimensListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingSpecimensListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
