import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryToExtractionComponent } from './delivery-to-extraction.component';

describe('DeliveryToExtractionComponent', () => {
  let component: DeliveryToExtractionComponent;
  let fixture: ComponentFixture<DeliveryToExtractionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryToExtractionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryToExtractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
