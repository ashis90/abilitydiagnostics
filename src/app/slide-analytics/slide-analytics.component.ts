import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-slide-analytics',
  templateUrl: './slide-analytics.component.html',
  styleUrls: ['./slide-analytics.component.css']
})
export class SlideAnalyticsComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  data;
  no_data;
  from_date: any;
  to_date: any;
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote; 
    this.searchForm = new FormGroup({

      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)

    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.spinner.show();
      this.itemsService.get_slide_analytics().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
      });
    }

  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), 'to_date': this.convertDate(this.searchForm.value.to_date), "dataType": this.searchForm.value.dataType };
      this.itemsService.search_slide(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }


  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }


  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
}
