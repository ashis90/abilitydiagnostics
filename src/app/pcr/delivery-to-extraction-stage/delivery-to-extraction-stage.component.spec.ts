import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryToExtractionStageComponent } from './delivery-to-extraction-stage.component';

describe('DeliveryToExtractionStageComponent', () => {
  let component: DeliveryToExtractionStageComponent;
  let fixture: ComponentFixture<DeliveryToExtractionStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryToExtractionStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryToExtractionStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
