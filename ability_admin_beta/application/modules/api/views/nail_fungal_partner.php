<p style="padding-top:1px;"></p>
<div style="width:96%; margin:15px auto;display:block;border:1px solid #4285f4; border-top:none;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;">
    <h4 style="font:700 16px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Nail Fungal Pathology Report</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 0 0;">
      <div style="width:31.3333%; float:left; margin:0; padding:1% 0 1% 1%;overflow:hidden">
        <div style="float:left; position:relative; width:100%;">
          <img src="{logo}" alt = "ability_logo" style="width: auto; height:80px; display:flex;"/>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:0;">
        <div class="comny_tag">
          <h4 style="font:700 18px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;">{partner_company}</h4>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">{address}</p>
         </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:1%;">
        <div style="font:400 12px/14px Arial, Helvetica, sans-serif;color: #444;">
          <p style="margin-bottom:0;">Date of Report</p>
          <p style="margin:0;"><span style="display:block;">{date}</span></p>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="border-bottom:1px dotted #8db3e2; text-align:center;color:#444; padding:0; margin:0;">
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%;">
        <div class="n_phn_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phone}</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%">
        <div class="fax_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{fax}</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%;">
        <div class="assct_no">
          <h6 style="font:700 13px/14px Arial, Helvetica, sans-serif; color:#000;margin:0;">Accessioning Number</h6>
          <p style="margin-top:0;color:#000;font:bold 13px/14px Arial, Helvetica, sans-serif;">{assessioning_num}</p>
        </div>
      </div>
    </div>
  </div>
  <div style="padding:5px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
		<p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 14px Arial, Helvetica, sans-serif; color:#000;">Patient Information:</div>
        <div style="width:50%;float:left;color:#00;font:bold 14px Arial, Helvetica, sans-serif;margin:0 0 0px;">{patient_name}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_phone}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">DOB:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_dob}</div>
        </p>
        <p style="display:block;margin:0 0 5px">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Collection Date:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_col}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Date Received:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_recive}</div>
        </p>
      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
		<div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 14px Arial, Helvetica, sans-serif;">Referring Physician:</div>
        <div style="width:50%;float:left;color:#00;font:bold 14px Arial, Helvetica, sans-serif;margin:0 0 0px;">{physician_name}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Address:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_addresss}</div>
          </p>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_mobile}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
        <div style="margin:0 0 6px 0;">
          <p style="display:block;margin:0 0 5px">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_fax}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>
  <div class="patholgy_rpt">
    <div class="tp_hdng" style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:5px 8px; text-align:center; font:700 22px Arial, Helvetica, sans-serif;color:#000;">
      <h2 style="font:700 16px Arial, Helvetica, sans-serif; color:#fff;margin:0;">{report_name}</h2>
    </div>
    <div style="padding:10px;">
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label> Clinical History: </label>
        </div>
        <div style=" width:38%; padding:0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          {clinical_history}
        </div>
        <div style=" width:38%; padding:0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          {clinicial_info}
        </div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Gross Description:</label>
        </div>
        <div style=" width:76%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif">
          {gross_desc}
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="margin:0 0 5px 0; padding:3px 2px;">
        <div style="width:20%; border:4px solid {diagnostic_color};  background:white; padding:2px 0 2px 2px;float:left; margin:0 0 0 -5px; color:#000;font:400 16px Arial,Helvetica,sans-serif; ">
          <label style=""><strong >Diagnosis: </strong></label>
        </div>
        <div style=" width:76%; float:left; margin:0 0 0 2%; font:400 16px Arial,Helvetica,sans-serif; color:#000; border:4px solid {diagnostic_color};  background:white; padding:2px 0 2px 2px;">
          <strong style="padding-left:2%;">{diagnostic}</strong>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style="width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Micro Description:</label>
        </div>
        <div style=" width:76%; float:left; margin:0;color:#444; font:400 12px/14px Arial,Helvetica,sans-serif; ">
          {gross_micro_description}
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Stains:</label>
        </div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_fst}</div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_sec}</div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_thr}</div>
		    <div style="clear:both;"></div>
      </div>
	    {nail_fungal_img}
		{addendum}
			<div style="margin:0 0 5px 0;">
		        <div style="width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
		          <label>Comments:</label>
		        </div>
		        <div style="width:76%; float:left; margin:0;color:#444; font:400 12px/14px Arial,Helvetica,sans-serif; ">
		          {stains_comments}
				  {comments}
		        </div>  
				 <div style="clear:both;"></div>
		      </div>
			</div>
	  <div style=" width:31.3333%; float:left; margin:0; padding:1%;">
       <p style="color:#444; margin:0; padding:0; font:100 10px/14px Arial, Helvetica, sans-serif; float:right;">
        {lab_doc_desc} <br/>
       
        <img style="height:70px; width:128px;" src={assigned_sign_img} alt="laboratory doctor sign" />
      </p>
	   {report_generate_datetime}
	   </div>
     <div style="clear:both;"></div>
    </div>
  </div>			