<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');
class Reports extends MY_Controller {

    function __construct() {
		parent::__construct();
		$this->load->library('m_pdf');
		$this->load->library('efax');
		date_default_timezone_set('MST7MDT');		 
    }

function index(){
		echo $filePath = realpath($_SERVER['DOCUMENT_ROOT']);
}
   /**
	* /
	*  @ get_physicians_active_list
	*
	*/
 function get_physicians_active_list(){
 	    $physicians_data = array();
        $physicians_det = array();
        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
       
        foreach($physician_details as $key => $physician){        
			$physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['ID'];        
	        array_push($physicians_data, $physicians_det);
        }         
        echo json_encode(array('physicians_data' => $physicians_data ));
   }
    
    
   /**
   * /
   * 
   * @ Pending_specimens_list
   */   
	
    function pending_specimens_list(){		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
   /* 
    $pending_specimen_sql = "SELECT `specimen`.`assessioning_num`, `specimen`.`id`, `specimen`.`physician_id`, `specimen`.`p_firstname` ,`p_lastname`, `specimen`.`collection_date`,`specimen`.`patient_address`,`specimen`.`patient_city`, `specimen`.`patient_zip` ,`specimen`.`patient_state`, `specimen`.`qc_check`, `specimen`.`modify_date` FROM (SELECT `assessioning_num`, `id`, `physician_id`, `p_firstname` ,`p_lastname`, `collection_date`,`patient_address`,`patient_city`, `patient_zip` ,`patient_state`, `qc_check`, `modify_date`  FROM `wp_abd_specimen` 
						 WHERE `status` = '0' AND `qc_check` = '0' AND `physician_accepct` = '0' 
						 AND `create_date` > '2017-03-27 23:59:59' 
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report`)
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' GROUP BY `specimen_id`) )specimen 
						 INNER JOIN ( SELECT * FROM `wp_abd_clinical_info`
						 WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
					     ORDER BY `specimen`.`id` DESC  LIMIT 0,100";*/


 $pending_specimen_sql = "SELECT `specimen`.`assessioning_num`, `specimen`.`id`, `specimen`.`physician_id`, `specimen`.`p_firstname` ,`p_lastname`, `specimen`.`collection_date`,`specimen`.`patient_address`, `specimen`.`modify_date`, `specimen`.`partners_company` 
    FROM (SELECT `assessioning_num`, `id`, `physician_id`, `p_firstname` ,`p_lastname`, `collection_date`, `patient_address`, `modify_date`,`partners_company` FROM `wp_abd_specimen` 
						 WHERE `status` = '0' AND `qc_check` = '0' AND `physician_accepct` = '0' 
						 AND `create_date` > '2017-03-27 23:59:59' 
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report`)
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' GROUP BY `specimen_id`) )specimen 
						 INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `wp_abd_clinical_info`
						 WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
					     ORDER BY `specimen`.`id` DESC  LIMIT 0,50";


	$pending_specimen_results = $this->BlankModel->customquery($pending_specimen_sql);

	$merged_specimen_results = array_merge($specimen_results, $pending_specimen_results);

	for($i= 0; $i< count($merged_specimen_results);$i++)
	{
		if($merged_specimen_results[$i]['partners_company']){
			$sp_id = $merged_specimen_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta_value( $merged_specimen_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta_value( $merged_specimen_results[$i]['physician_id'], 'last_name');
			$physician_name = $physician_first_name.' '.$physician_last_name;
		}
		//$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;
		$specimen_count_data = $this->BlankModel->customquery("SELECT count(*) as count FROM `wp_abd_specimen_stage_details` where specimen_id ='".$merged_specimen_results[$i]['id']."'");
			if($specimen_count_data[0]['count']>=4)
			{
				$merged_specimen_results[$i]['count']= "done";
			}
			else
			{
				$merged_specimen_results[$i]['count']= "pending";
			}
		
	    }
	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta_value( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta_value( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	  }

     if($merged_specimen_results){	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	      
     }

    function all_pending_specimens_list(){		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
    
    $pending_specimen_sql = "SELECT `specimen`.`assessioning_num`, `specimen`.`id`, `specimen`.`physician_id`, `specimen`.`p_firstname` ,`p_lastname`, `specimen`.`collection_date`,`specimen`.`patient_address`,`specimen`.`patient_city`, `specimen`.`patient_zip` ,`specimen`.`patient_state`, `specimen`.`qc_check`, `specimen`.`modify_date` FROM (SELECT `assessioning_num`, `id`, `physician_id`, `p_firstname` ,`p_lastname`, `collection_date`,`patient_address`,`patient_city`, `patient_zip` ,`patient_state`, `qc_check`, `modify_date`  FROM `wp_abd_specimen` 
						 WHERE `status` = '0' AND `qc_check` = '0' AND `physician_accepct` = '0' 
						 AND `create_date` > '2017-03-27 23:59:59' 
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report`)
						 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' GROUP BY `specimen_id`) )specimen 
						 INNER JOIN ( SELECT * FROM `wp_abd_clinical_info`
						 WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%') )clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
					     ORDER BY `specimen`.`id` DESC";

	$pending_specimen_results = $this->BlankModel->customquery($pending_specimen_sql);

	$merged_specimen_results = array_merge($specimen_results, $pending_specimen_results);

	for($i= 0; $i< count($merged_specimen_results);$i++){
		$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;
		$specimen_count_data = $this->BlankModel->customquery("SELECT count(*) as count FROM `wp_abd_specimen_stage_details` where specimen_id ='".$merged_specimen_results[$i]['id']."'");
			if($specimen_count_data[0]['count']>=4)
			{
				$merged_specimen_results[$i]['count']= "done";
			}
			else
			{
				$merged_specimen_results[$i]['count']= "pending";
			}
	
	    }
	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
      
     }

    
    function search_pending_specimens_list()
    {	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data))
		{
			if($data['physician']!=""){
				$search=" AND `physician_id`='".$data['physician']."'";	
			 }
			 if($data['date']!=""){
				$collection_date =$data['date'];
				$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
			 }
			 if($data['fname']!=""){
				 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
			 }
			 if($data['lname']!=""){
				 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
			 }
			 if($data['barcode']!=""){
				$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
				
		 
			 }
			 if($data['assessioning_num']!=""){
				$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
			 }
			 $specimen_details = array();
			 $physicians_data = array();
			 $physicians_det = array();
			 $merged_specimen_results = array();
			 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
		  
			 $physician_ids = $this->BlankModel->customquery($physician_sql);
		   
			 foreach($physician_ids as $key => $physician){
		   
				$physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
				$physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
				$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				 $physicians_det['id'] = $physician['physician_id'];
			
				 array_push($physicians_data, $physicians_det);
				}
			 
			$pending_specimen_sql = "SELECT `specimen`.`assessioning_num`, `specimen`.`id`, `specimen`.`physician_id`, `specimen`.`p_firstname` ,`p_lastname`, `specimen`.`collection_date`,`specimen`.`patient_address`,`specimen`.`patient_city`, `specimen`.`patient_zip` ,`specimen`.`patient_state`, `specimen`.`qc_check` FROM (SELECT `assessioning_num`, `id`, `physician_id`, `p_firstname` ,`p_lastname`, `collection_date`,`patient_address`,`patient_city`, `patient_zip` ,`patient_state`, `qc_check`  FROM `wp_abd_specimen` 
							 WHERE `status` = '0' AND `qc_check` = '0' AND `physician_accepct` = '0' 
							 AND `create_date` > '2017-03-27 23:59:59' ".$search."
							 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report`)
							 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' GROUP BY `specimen_id`) )specimen 
							 INNER JOIN ( SELECT * FROM `wp_abd_clinical_info`
							 WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%') )clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` ORDER BY `specimen`.`id` DESC";
			$merged_specimen_results = $this->BlankModel->customquery($pending_specimen_sql);	

			for($i= 0; $i< count($merged_specimen_results);$i++)
		{
			$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
			
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['physician_name'] = $physician_name;
			$merged_specimen_results[$i]['physician_name'] = $physician_name;
			$specimen_count_data = $this->BlankModel->customquery("SELECT count(*) as count FROM `wp_abd_specimen_stage_details` where specimen_id ='".$merged_specimen_results[$i]['id']."'");
				if($specimen_count_data[0]['count']>=4)
				{
					$merged_specimen_results[$i]['count']= "done";
				}
				else
				{
					$merged_specimen_results[$i]['count']= "pending";
				}
			
			}
			
			if($merged_specimen_results){
		   
				die( json_encode(array('status' =>'1','specimen_count' => count($merged_specimen_results), "specimens_detail" => $merged_specimen_results , "physicians_data" => $physicians_data )));
				}
				
				else{
					 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
				}	
		}
		
	}


//////////// pending reports ///////////////
 
    function pending_reports()
    {
    		
    $specimen_results = array();
    $merged_specimen_results = array();      
    $pending_reports_sql = "SELECT `id`,`nail_funagl_id`, `dor`, `physician_id`, `p_lastname`, `p_firstname`, `assessioning_num`, `nail_pdf`, `ins_type`, `qc_check`, `ins`,`partners_company`
	FROM 
	(SELECT `nail_funagl_id`, `specimen_id`, `status`, `nail_pdf`, `dor`, `ins` FROM `wp_abd_nail_pathology_report` )nail_patho
	INNER JOIN 
	(SELECT `id`, `lab_id`, `patient_id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`, `patient_phn`, `patient_dob`, `barcode_number`, `ins_type`, `qc_check`,`partners_company` FROM `wp_abd_specimen` WHERE `status` = '0') specimen 
	ON
	`specimen`.`id` = `nail_patho`.`specimen_id`
	AND `nail_patho`.`status` ='Inactive' AND `nail_patho`.`ins` ='3' ORDER BY `nail_patho`.`nail_funagl_id` DESC";
	$pending_reports_results = $this->BlankModel->customquery($pending_reports_sql);

	for($i= 0; $i< count($pending_reports_results); $i++){
		if($pending_reports_results[$i]['partners_company']){
			$sp_id = $pending_reports_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta( $pending_reports_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $pending_reports_results[$i]['physician_id'], 'last_name');		
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
		$pending_reports_results[$i]['physician_name'] = $physician_name;	
		$pending_reports_results[$i]['nail_pdf'] = $pending_reports_results[$i]['nail_pdf'];	
		$pending_reports_results[$i]['report_date'] = date('Y-m-d', strtotime($pending_reports_results[$i]['dor']));

		$pcr_det_sql = "SELECT `report_pdf_name` FROM wp_abd_generated_pcr_reports WHERE accessioning_num = '".$pending_reports_results[$i]['assessioning_num']."'";
		$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
		if(!empty($pcr_results))
		{
			$pending_reports_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
		}
		else
		{
			$pending_reports_results[$i]['report_pdf_name']  = "";
		}
	}

    if($pending_reports_results){
	   die( json_encode(array( "status" => "1", "specimen_results" => $pending_reports_results,"specimen_count" => count($pending_reports_results) )));
	   }
	else{
	   die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
		 	
    }

    function search_pending_reports()
      {	
		$search = "";
		$pending_report_detail = array();
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search.=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['report_date']!=""){
		   $report_date = date('Y-m-d', strtotime($data['report_date']));
		 	
			$search.=" AND `nail_patho`.`dor` LIKE '%".$report_date."%'";		
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }

	  $pending_reports_sql = "SELECT `id`,`nail_funagl_id`, `dor`, `physician_id`, `p_lastname`, `p_firstname`, `assessioning_num`, `nail_pdf`, `ins_type`, `qc_check`, `ins`
			FROM 
			(SELECT `nail_funagl_id`,`specimen_id`,`status`,`nail_pdf`,`dor`,`ins` FROM `wp_abd_nail_pathology_report` )nail_patho
			INNER JOIN 
			(SELECT `id`,`lab_id`,`patient_id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id`,`patient_phn`,`patient_dob`,`barcode_number`,`ins_type`,`qc_check` FROM `wp_abd_specimen` WHERE `status` = '0') specimen 
			ON
			`specimen`.`id` = `nail_patho`.`specimen_id`
			AND `nail_patho`.`status` ='Inactive' AND `nail_patho`.`ins` ='3'".$search." ORDER BY `nail_patho`.`nail_funagl_id` DESC";
		
		 $pending_reports_results = $this->BlankModel->customquery($pending_reports_sql);
		 if($pending_reports_results)
		 {		 
		    $num_rows_count = 0;
		    foreach($pending_reports_results as $specimen){
			 	$report_date = date('Y-m-d', strtotime($specimen['dor']));
				$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');		 
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];					
				$pcr_det_sql = "SELECT `report_pdf_name` FROM wp_abd_generated_pcr_reports WHERE accessioning_num = '".$specimen['assessioning_num']."'";
				$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
				if(!empty($pcr_results))
				{
					$pdf_report = $pcr_results[0]['report_pdf_name'];
				}
				else
				{
					$pdf_report = "";
				}
				
				$pending_report_results =  array( 'id' => $specimen['id'], 
												  'assessioning_num' => $specimen['assessioning_num'], 
												  'physician_id'     => $specimen['physician_id'], 
												  'p_lastname'       => $specimen['p_lastname'], 
												  'p_firstname'      => $specimen['p_firstname'], 
												  'ins_type'         => $specimen['ins_type'], 
												  'report_date'      => $report_date, 
												  'physician_name'   => $physician_name,
												  'nail_funagl_id'   => $specimen['nail_funagl_id'],
												  'nail_pdf'   		 => $specimen['nail_pdf'],
												  'report_pdf_name'  => $pdf_report
												);
				$num_rows_count++;	
			    array_push($pending_report_detail, $pending_report_results);
		     }		 
		  die( json_encode(array('status' =>'1','specimen_count' => $num_rows_count, "pending_report_detail" => $pending_report_detail )));		 
		 }
		 else
		 {
		  die( json_encode(array('status'=>'0')));
		 }
	}

	function pending_report_view_data(){
		$clinical_specimen_det = array();
		$specimen_information = array();		
		$data = json_decode(file_get_contents('php://input'), true);
		
		/**
		* @var /Pending Report
		*/
		$pending_report_sql = "SELECT * FROM `wp_abd_nail_pathology_report` WHERE `nail_funagl_id` = '".$data."' AND `status` != 'Active'";
		$pending_report_view_details = $this->BlankModel->customquery($pending_report_sql);		
		/**
		* @var /clinical_info
		*/
		$clinical_information_sql = "SELECT clinical_specimen FROM `wp_abd_clinical_info` WHERE specimen_id = '".$pending_report_view_details[0]['specimen_id']."'";
	 	$clinical_information = $this->BlankModel->customquery($clinical_information_sql);		
	    $clinical_specimen_fst = $clinical_information[0]['clinical_specimen'];
	     if($clinical_specimen_fst){
		  $clinical_specimen_fst = rtrim($clinical_specimen_fst, ',');	       
	      $clinical_specimen_det = str_replace(',', ' ',$clinical_specimen_fst);
	    }	   
	    /**
	    * @var /Specimen Details
	    */
	     $specimen_sql = "SELECT `date_received`,`physician_id`,`assessioning_num`,`site_indicator`  FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id = '".$pending_report_view_details[0]['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
	     $specimen_information_result =  $this->BlankModel->customquery($specimen_sql);		
	   
	     $specimen_information['timestamp']       =  $specimen_information_result[0]['date_received'];
	     $specimen_information['physician_id']    =  $specimen_information_result[0]['physician_id'];
	     $specimen_information['accessioning_num']=  $specimen_information_result[0]['assessioning_num'];
	     $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
	   
	    /**
		* @var /GrossDescription
		*/
	     $specimen_stage_details_sql = "SELECT `addtional_desc`, `comments` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$pending_report_view_details[0]['specimen_id']."' AND`stage_id` = '2' ";
	    
	     $specimen_stage_details = $this->BlankModel->customquery($specimen_stage_details_sql);	     
	     $gross_description = $specimen_stage_details[0]['addtional_desc'];
	    
	    /**
		* @var Diagnostic_short_code
		*/
		
	     $diagnostic_short_code_sql = "SELECT `color`, `diagnosis`,`text`, `sc` FROM `wp_abd_nail_macro_codes` where `sc` = '".$pending_report_view_details[0]['diagnostic_short_code']."'";
		 $diagnosis = $this->BlankModel->customquery($diagnostic_short_code_sql);	  
	    
	    
	    if($pending_report_view_details){
			 die( json_encode(array("status" =>'1', "pending_report_view_details" => $pending_report_view_details[0], "clinician_location" => $clinical_specimen_det, "specimen_information" => $specimen_information, "gross_description" => $gross_description, "diagnostic_short_code" => $diagnosis[0] )));	
			 }
		else{
			 die( json_encode(array('status' =>'0')));		 
		    }
	
	}


//////////// submitted reports ///////////////
 
    public function submitted_reports()
    {
    		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
		$tbl1=SPECIMEN_MAIN;
		$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
		$tbl3=GENERATE_PCR_REPORT_MAIN;
		$tbl4=CLINICAL_INFO_MAIN;
		$data = json_decode(file_get_contents('php://input'), true);
	
		$dataType = $data['dataType'];

		if($dataType == 'general'){
			$tbl1=SPECIMEN_MAIN;
			$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
			$tbl3=GENERATE_PCR_REPORT_MAIN;
			$tbl4=CLINICAL_INFO_MAIN;
		}else if($dataType == 'archive'){
			$tbl1=SPECIMEN_ARCHIVE;
			$tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
			$tbl3=GENERATE_PCR_REPORT_ARCHIVE;
			$tbl4=CLINICAL_INFO_ARCHIVE;
		}
    
    $submitted_reports = "SELECT `nail_patho`.`nail_funagl_id`,`nail_patho`.`amend_note`, `nail_patho`.`nail_pdf`, `nail_patho`.`specimen_id`, `nail_patho`.`dor`, `specimen`.`assessioning_num`, `specimen`.`p_lastname`, `specimen`.`p_firstname`,`specimen`.`id`, `specimen`.`physician_id`,`specimen`.`qc_check`,`specimen`.`partners_company` FROM (SELECT * FROM ".$tbl2." ) nail_patho
	INNER JOIN (SELECT `id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`,`qc_check`,`partners_company` FROM ".$tbl1." WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' ) specimen
	ON
	`specimen`.`id` = `nail_patho`.`specimen_id` AND `nail_patho`.`status` ='Active' ORDER BY `nail_patho`.`dor` DESC limit 0,100";

	$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);

	$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

	for($i= 0; $i< count($merged_specimen_results);$i++)
	{
		if($merged_specimen_results[$i]['partners_company']){
			$sp_id = $merged_specimen_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
			
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;

		$pcr_det_sql = "SELECT `report_pdf_name` FROM $tbl3 WHERE accessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
		if(!empty($pcr_results))
		{
			$merged_specimen_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
		}
		else
		{
			$merged_specimen_results[$i]['report_pdf_name']  = "";
		}
		$nail_unit_sql = "SELECT `nail_unit` FROM $tbl4 WHERE assessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$nail_unit_results = $this->BlankModel->customquery($nail_unit_sql);
		if(!empty($nail_unit_results))
		{
			$nail_unit = explode(",",$nail_unit_results[0]['nail_unit']);
			if(in_array("4", $nail_unit) || in_array("5", $nail_unit))
			{
				$nail_unit="true";
			}
			else
			{
				$nail_unit="false";
			}
			$merged_specimen_results[$i]['nail_unit'] = $nail_unit;
		}
		else
		{
			$merged_specimen_results[$i]['nail_unit'] ="";
		}
		
		
	}

	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
    }


     public function load_more_submitted_reports()
    {
	 $data = json_decode(file_get_contents('php://input'), true);
    if(!empty($data)){
        $start = $data['load'];	
    $specimen_results = array();
    $merged_specimen_results = array();
		$physicians_data = array();
		$submitted_reports_results='';
		$tbl1=SPECIMEN_MAIN;
		$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
		$tbl3=GENERATE_PCR_REPORT_MAIN;
		$tbl4=CLINICAL_INFO_MAIN;

		$dataType = $data['dataType'];

		if($dataType == 'general'){
			$tbl1=SPECIMEN_MAIN;
			$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
			$tbl3=GENERATE_PCR_REPORT_MAIN;
			$tbl4=CLINICAL_INFO_MAIN;
		}else if($dataType == 'archive'){
			$tbl1=SPECIMEN_ARCHIVE;
			$tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
			$tbl3=GENERATE_PCR_REPORT_ARCHIVE;
			$tbl4=CLINICAL_INFO_ARCHIVE;
		}
    
    $submitted_reports = "SELECT `nail_patho`.`nail_funagl_id`,`nail_patho`.`amend_note`, `nail_patho`.`nail_pdf`, `nail_patho`.`specimen_id`, `nail_patho`.`dor`, `specimen`.`assessioning_num`, `specimen`.`p_lastname`, `specimen`.`p_firstname`,`specimen`.`id`, `specimen`.`physician_id`,`specimen`.`qc_check`,`specimen`.`partners_company` FROM (SELECT * FROM $tbl2 ) nail_patho
	INNER JOIN (SELECT `id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`,`qc_check`,`partners_company` FROM $tbl1 WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' ) specimen
	ON
	`specimen`.`id` = `nail_patho`.`specimen_id` AND `nail_patho`.`status` ='Active' ORDER BY `nail_patho`.`dor` DESC limit $start,100";

	$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);

	$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

	for($i= 0; $i< count($merged_specimen_results);$i++)
	{
		if($merged_specimen_results[$i]['partners_company']){
			$sp_id = $merged_specimen_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
			
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;

		$pcr_det_sql = "SELECT `report_pdf_name` FROM $tbl3 WHERE accessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
		if(!empty($pcr_results))
		{
			$merged_specimen_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
		}
		else
		{
			$merged_specimen_results[$i]['report_pdf_name']  = "";
		}
		$nail_unit_sql = "SELECT `nail_unit` FROM $tbl4 WHERE assessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$nail_unit_results = $this->BlankModel->customquery($nail_unit_sql);
		if(!empty($nail_unit_results))
		{
			$nail_unit = explode(",",$nail_unit_results[0]['nail_unit']);
			if(in_array("4", $nail_unit) || in_array("5", $nail_unit))
			{
				$nail_unit="true";
			}
			else
			{
				$nail_unit="false";
			}
			$merged_specimen_results[$i]['nail_unit'] = $nail_unit;
		}
		else
		{
			$merged_specimen_results[$i]['nail_unit'] ="";
		}
		
		
	}

	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
		}
    }



    function all_submitted_reports()
 {		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
    
    $submitted_reports = "SELECT `nail_patho`.`nail_funagl_id`,`nail_patho`.`amend_note`, `nail_patho`.`nail_pdf`, `nail_patho`.`specimen_id`, `nail_patho`.`dor`, `specimen`.`assessioning_num`, `specimen`.`p_lastname`, `specimen`.`p_firstname`,`specimen`.`id`, `specimen`.`physician_id`,`specimen`.`qc_check` FROM (SELECT * FROM `wp_abd_nail_pathology_report` ) nail_patho
	INNER JOIN (SELECT `id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`,`qc_check` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' ) specimen
	ON `specimen`.`id` = `nail_patho`.`specimen_id` AND `nail_patho`.`status` ='Active' ORDER BY `nail_patho`.`dor` DESC";

	$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);



	$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

	for($i= 0; $i< count($merged_specimen_results);$i++){
		$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name');
		$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;

		$pcr_det_sql = "SELECT `report_pdf_name` FROM wp_abd_generated_pcr_reports WHERE accessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
		if(!empty($pcr_results))
		{
			$merged_specimen_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
		}
		else
		{
			$merged_specimen_results[$i]['report_pdf_name']  = "";
		}
		$nail_unit_sql = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE assessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$nail_unit_results = $this->BlankModel->customquery($nail_unit_sql);
		if(!empty($nail_unit_results))
		{
			$nail_unit = explode(",",$nail_unit_results[0]['nail_unit']);
		    if(in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("6", $nail_unit) || in_array("7", $nail_unit))
			{
				$nail_unit="true";
			}
			else
			{
				$nail_unit="false";
			}
			$merged_specimen_results[$i]['nail_unit'] = $nail_unit;
		}
		else
		{
			$merged_specimen_results[$i]['nail_unit'] ="";
		}
	
	    }

	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
      
     }



     public function search_submitted_reports()
    {	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
	
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =date('Y-m-d',strtotime($data['date']));
			$search.=" AND (`nail_patho`.`dor` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND `assessioning_num` LIKE '%".$data['barcode']."%'";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }
	
			$specimen_results = array();
			$merged_specimen_results = array();
			$physicians_data = array();
			$tbl1=SPECIMEN_MAIN;
			$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
			$tbl3=GENERATE_PCR_REPORT_MAIN;
			$tbl4=CLINICAL_INFO_MAIN;
	
			$dataType = $data['dataType'];
	
			if($dataType == 'general'){
				$tbl1=SPECIMEN_MAIN;
				$tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
				$tbl3=GENERATE_PCR_REPORT_MAIN;
				$tbl4=CLINICAL_INFO_MAIN;
			}else if($dataType == 'archive'){
				$tbl1=SPECIMEN_ARCHIVE;
				$tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
				$tbl3=GENERATE_PCR_REPORT_ARCHIVE;
				$tbl4=CLINICAL_INFO_ARCHIVE;
			}
			
			$submitted_reports = "SELECT `nail_patho`.`nail_funagl_id`, `nail_patho`.`nail_pdf`, `nail_patho`.`specimen_id`, `nail_patho`.`dor`, `specimen`.`assessioning_num`, `specimen`.`p_lastname`, `specimen`.`p_firstname`, `nail_patho`.`amend_note`, `specimen`.`id`, `specimen`.`physician_id`,`specimen`.`qc_check`,`specimen`.`partners_company` FROM (SELECT * FROM $tbl2 )nail_patho
			INNER JOIN (SELECT `id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`,`qc_check`,`partners_company` FROM $tbl1 WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' ) specimen
			ON
			specimen.id = nail_patho.specimen_id ".$search." AND nail_patho.status ='Active' ORDER BY specimen.assessioning_num ";

			$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);

			$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

			for($i= 0; $i< count($merged_specimen_results);$i++)
			{
			if($merged_specimen_results[$i]['partners_company']){
				$sp_id = $merged_specimen_results[$i]['id'];
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
				$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
				
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			}
				$physicians_det['physician_name'] = $physician_name;
				$merged_specimen_results[$i]['physician_name'] = $physician_name;

				$pcr_det_sql = "SELECT `report_pdf_name` FROM $tbl3 WHERE accessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
				$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
				if(!empty($pcr_results))
				{
					$merged_specimen_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
				}
				else
				{
					$merged_specimen_results[$i]['report_pdf_name']  = "";
				}
				$nail_unit_sql = "SELECT `nail_unit` FROM $tbl4 WHERE assessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
				$nail_unit_results = $this->BlankModel->customquery($nail_unit_sql);
				if(!empty($nail_unit_results))
				{
					$nail_unit = explode(",",$nail_unit_results[0]['nail_unit']);
					if(in_array("4", $nail_unit) || in_array("5", $nail_unit))
					{
					   $nail_unit="true";
					}
					else
					{
						$nail_unit="false";
					}

					//$nail_unit=in_array("4", $nail_unit);
					$merged_specimen_results[$i]['nail_unit'] = $nail_unit;
				}
				else
				{
					$merged_specimen_results[$i]['nail_unit'] ="";
				}
				
				
			}

		if($merged_specimen_results){
		
			die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'specimen_count'=>count($merged_specimen_results) )));
			}
			
			else{
				die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
			}	
	}	

	public function fivedays_reports ()
    {
    		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
    
    $submitted_reports = "SELECT `nail_patho`.`nail_funagl_id`, `nail_patho`.`nail_pdf`, `nail_patho`.`specimen_id`, `nail_patho`.`dor`, `specimen`.`assessioning_num`, `specimen`.`p_lastname`, `specimen`.`p_firstname`, `nail_patho`.`amend_note`, `specimen`.`id`, `specimen`.`physician_id`,`specimen`.`qc_check`,`specimen`.`partners_company` FROM (SELECT * FROM `wp_abd_nail_pathology_report` )nail_patho
	INNER JOIN (SELECT `id`, `p_lastname`, `p_firstname`, `assessioning_num`, `physician_id`,`qc_check`,`partners_company` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' ) specimen
	ON
	`specimen`.`id` = `nail_patho`.`specimen_id` AND `nail_patho`.`status` ='Active' AND create_date >= DATE(NOW()) - INTERVAL 5 DAY";

	$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);

	$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

	for($i= 0; $i< count($merged_specimen_results);$i++){
		if($merged_specimen_results[$i]['partners_company']){
			$sp_id = $merged_specimen_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
			
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;

		$pcr_det_sql = "SELECT `report_pdf_name` FROM wp_abd_generated_pcr_reports WHERE accessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$pcr_results = $this->BlankModel->customquery($pcr_det_sql);
		if(!empty($pcr_results))
		{
			$merged_specimen_results[$i]['report_pdf_name']  = $pcr_results[0]['report_pdf_name'];
		}
		else
		{
			$merged_specimen_results[$i]['report_pdf_name']  = "";
		}
		$nail_unit_sql = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE assessioning_num = '".$merged_specimen_results[$i]['assessioning_num']."'";
		$nail_unit_results = $this->BlankModel->customquery($nail_unit_sql);
		if(!empty($nail_unit_results))
		{
			$nail_unit = explode(",",$nail_unit_results[0]['nail_unit']);
			if(in_array("4", $nail_unit) || in_array("5", $nail_unit))
			{
				$nail_unit="true";
			}
			else
			{
				$nail_unit="false";
			}

			//$nail_unit=in_array("4", $nail_unit);
			$merged_specimen_results[$i]['nail_unit'] = $nail_unit;
		}
		else
		{
			$merged_specimen_results[$i]['nail_unit'] ="";
		}
	
	    }

	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found",'specimen_count'=>'0' )));
		 }	
    }



//////////// amended reports ///////////////
 
    public function amended_reports()
    {
    		
    $specimen_results = array();
    $merged_specimen_results = array();
    $physicians_data = array();
    
    $submitted_reports = "SELECT * FROM (SELECT * FROM `wp_abd_nail_pathology_report` WHERE `amend_note` !='')nail_patho
  INNER JOIN (SELECT `id`,`lab_id`,`patient_id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id`,`patient_phn`,`patient_dob`,`qc_check`,`collection_date`,`partners_company` FROM `wp_abd_specimen` WHERE status = 0)specimen
  ON
  specimen.id = nail_patho.specimen_id ORDER BY nail_patho.nail_funagl_id DESC ";

	$submitted_reports_results = $this->BlankModel->customquery($submitted_reports);

	$merged_specimen_results = array_merge($specimen_results, $submitted_reports_results);

	for($i= 0; $i< count($merged_specimen_results);$i++){
		if($merged_specimen_results[$i]['partners_company']){
			$sp_id = $merged_specimen_results[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
		$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;
	
	    }

	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
    }



     public function search_amended_reports()
    {	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }
		 $specimen_details = array();
		 $physicians_data = array();
		 $physicians_det = array();
		 $specimen_results = array();
		 $merged_specimen_results = array();
		 
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			$physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		 	$physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		//  $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59'".$search." )";
		//  $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
		//  $is_multy_result = 0;
			 
		//  $specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);

		$amended_reports = "SELECT * FROM (SELECT * FROM `wp_abd_nail_pathology_report` WHERE `amend_note` !='')nail_patho
  INNER JOIN (SELECT `id`,`lab_id`,`patient_id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id`,`patient_phn`,`patient_dob`,`qc_check`,`collection_date`,`partners_company` FROM `wp_abd_specimen` WHERE status = 0)specimen
  ON
  specimen.id = nail_patho.specimen_id".$search." ORDER BY nail_patho.nail_funagl_id DESC ";

		$specimens_details_det = $this->BlankModel->customquery($amended_reports);	 
		 if($specimens_details_det)
		 {
		 
		//  $num_rows_count = 0;
		//  foreach($specimens_details as $specimen){
		// 	$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
		// 	$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');		 
		// 	$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];		 
		// 	$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name,
		// );

		// 	$num_rows_count++;	
		//    array_push($specimen_details, $specimen_information);
		//  }

		$specimen_details = array_merge($specimen_results, $specimens_details_det);

		for($i= 0; $i< count($specimen_details);$i++){
		if($specimen_details[$i]['partners_company']){
			$sp_id = $specimen_details[$i]['id'];
			$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
			$physician_name = $partners_specimen_details['physician_name'];
		}else{
			$physician_first_name = get_user_meta( $specimen_details[$i]['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $specimen_details[$i]['physician_id'], 'last_name');
			
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		}
			$physicians_det['physician_name'] = $physician_name;
			$specimen_details[$i]['physician_name'] = $physician_name;
		
			}
		 
		 die( json_encode(array('status' =>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
	}

	/////// view amended report ///////

	function amended_report_details()
	{
		date_default_timezone_set('MST7MDT');
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$sid= $data['id'];
		$addendum_val = array();
		$pending_report_sql = "SELECT * FROM `wp_abd_nail_pathology_report` WHERE `nail_funagl_id` = '".$sid."'";
		$pending_report_view_details = $this->BlankModel->customquery($pending_report_sql);	
		if($pending_report_view_details[0]['modify_date'] < '2018-06-12 00:00:00') 
		{
			$addendum_val = $pending_report_view_details[0]['addendum'];
			$old_date = true;
			$new_date = "";
		}
		else
		{
			$addendum_ser_val = unserialize($pending_report_view_details[0]['addendum_desc']);
			if(!empty($addendum_ser_val[0]))
			{
				$addendum_val = $addendum_ser_val;
				$new_date = true;
				$old_date = "";
			}
			elseif(!empty($pending_report_view_details[0]['addendum']))
			{ 
				$new_date = true;
				$old_date = "";
				$nail_addendum = $pending_report_view_details[0]['addendum'];
				$addendum_codes = explode(",",$nail_addendum);
				foreach($addendum_codes as $addendum_code)
				{	
					$addendum_desc = get_stains_desc($addendum_code);
					$text_info =  array( 'text' => $addendum_desc);
					array_push($addendum_val, $text_info);
				}
			}
		}
		$clinical_information_sql = "SELECT clinical_specimen FROM `wp_abd_clinical_info` WHERE specimen_id = '".$pending_report_view_details[0]['specimen_id']."'";
	 	$clinical_information = $this->BlankModel->customquery($clinical_information_sql);		
	    $clinical_specimen_fst = $clinical_information[0]['clinical_specimen'];
	     if($clinical_specimen_fst){
		  $clinical_specimen_fst = rtrim($clinical_specimen_fst, ',');	       
	      $clinical_specimen_det = str_replace(',', '  ',$clinical_specimen_fst);
	    }	   
	    
	     $specimen_sql = "SELECT * FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id = '".$pending_report_view_details[0]['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
		 $specimen_information_result =  $this->BlankModel->customquery($specimen_sql);	
	  
	     $specimen_information['timestamp']       =  $specimen_information_result[0]['date_received'];
	     $specimen_information['physician_id']    =  $specimen_information_result[0]['physician_id'];
	     $specimen_information['accessioning_num']=  $specimen_information_result[0]['assessioning_num'];
	     $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 $specimen_information['patient_name']  =  $specimen_information_result[0]['p_firstname']." ".$specimen_information_result[0]['p_lastname']; 
		 $specimen_information['patient_phn']  =  $specimen_information_result[0]['patient_phn']; 
		 $specimen_information['patient_dob']  =  $specimen_information_result[0]['patient_dob']; 
		 $specimen_information['collection_date']  =  $specimen_information_result[0]['collection_date']; 
		 $specimen_information['date_received']  =  $specimen_information_result[0]['date_received'];
		 $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 
		$first_name = get_user_meta( $specimen_information_result[0]['physician_id'], 'first_name');				
		$last_name  = get_user_meta( $specimen_information_result[0]['physician_id'], 'last_name');
		$mobile  = get_user_meta( $specimen_information_result[0]['physician_id'], '_mobile');
		$address  = get_user_meta( $specimen_information_result[0]['physician_id'], '_address');
		$fax  = get_user_meta( $specimen_information_result[0]['physician_id'], 'fax');

		$fname = $first_name['meta_value'];
		$lname = $last_name['meta_value'];
		$mob  = $mobile['meta_value'];
		$add = $address['meta_value'];	
		$fax = $fax['meta_value'];

		$physicain_info= array('name'=> $fname." ".$lname, 'mob'=> $mob, 'add'=> $add, 'fax'=>$fax);

	     $specimen_stage_details_sql = "SELECT `addtional_desc`, `comments` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$pending_report_view_details[0]['specimen_id']."' AND`stage_id` = '2' ";
	    
	     $specimen_stage_details = $this->BlankModel->customquery($specimen_stage_details_sql);	     
	     $gross_description = $specimen_stage_details[0]['addtional_desc'];

	     $diagnostic_short_code_sql = "SELECT `color`, `diagnosis`,`text`, `sc` FROM `wp_abd_nail_macro_codes` where `sc` = '".$pending_report_view_details[0]['diagnostic_short_code']."'";
		 $diagnosis = $this->BlankModel->customquery($diagnostic_short_code_sql);	
		 
		 if(!empty($pending_report_view_details[0]['stains']))
		 {
			$stains = explode(",",$pending_report_view_details[0]['stains']);
			if(!empty($stains[0]))
			{
			  $stv1= $stains[0];
			}
			else
			{
			   $stv1= "";
			}
			if(!empty($stains[1]))
			{
			  $stv2= $stains[1];
			}
			else
			{
				$stv2= "";
			}
			if(!empty($stains[2]))
			{
			  $stv3= $stains[2];
			}
			else
			{
				$stv3= "";
			}
			if(!empty($stains[0]))
			{
			   $Stains_desc1 = get_stains_desc($stains[0]);
			}
			else
			{
			   $Stains_desc1 = "";
			}
			if(!empty($stains[1]))
			{
			   $Stains_desc2 = get_stains_desc($stains[1]);
			}
			else
			{
			   $Stains_desc2 = "";
			}
			if(!empty($stains[2]))
			{
			   $Stains_desc3 = get_stains_desc($stains[2]);
			}
			else
			{
			   $Stains_desc3 = "" ;
			}
		 }
		
		if($pending_report_view_details)
		{
			 die( json_encode(array("status" =>'1', "amended_report_view_details" => $pending_report_view_details[0], 
			 "clinician_location" => $clinical_specimen_det, "specimen_information" => $specimen_information, 
			 "gross_description" => $gross_description, "diagnostic_short_code" => $diagnosis[0],'physician_info'=>$physicain_info,
			 "st1val"=>$stv1, "st2val"=>$stv2, "st3val"=>$stv3,"st1"=>$Stains_desc1,"st2"=>$Stains_desc2,"st3"=>$Stains_desc3,"addendum_val"=>$addendum_val,
			"old_date"=>$old_date,"new_date"=>$new_date)));	
	    }
		else
		{
			 die( json_encode(array('status' =>'0')));		 
		}
		}
		
	}

   /**
    * 
    * Submitted Reports Edit
    * 
    */

	function submitted_reports_edit() {
		date_default_timezone_set('MST7MDT');
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data))
		{
			
			$pending_report_sql = "SELECT * FROM `wp_abd_nail_pathology_report` WHERE `nail_funagl_id` = '".$data['nail_id']."'";
			$pending_report_view_details = $this->BlankModel->customquery($pending_report_sql);		
			if($pending_report_view_details[0]['modify_date'] < '2018-06-12 00:00:00') 
			{
				$addendum_val = $pending_report_view_details[0]['addendum'];
				$addendum_pdf_content = "";
				if(!empty($pending_report_view_details[0]['addendum']))
				{
				$addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
				}
				$addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$pending_report_view_details[0]['addendum']."</div>";
				$addendum_pdf_content .="<div style='clear:both;'></div></div>";
			}
			else
			{
				$addendum_ser_val = unserialize($pending_report_view_details[0]['addendum_desc']);
				if(!empty($addendum_ser_val[0]))
				{
					$addendum_val = $addendum_ser_val;
					$addendum_pdf_content = "";
					if(!empty($addendum_val[0]) || !empty($addendum_val[1]) || !empty($addendum_val[2]))
					{
					$addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
					}
					foreach($addendum_val as $value)
					{
					$addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
					}
					$addendum_pdf_content .="<div style='clear:both;'></div></div>";
					
				}
				elseif(!empty($pending_report_view_details[0]['addendum']))
				{ 
					$nail_addendum = $pending_report_view_details[0]['addendum'];
					$addendum_codes = explode(",",$nail_addendum);
					foreach($addendum_codes as $addendum_code)
					{	
						$addendum_desc = get_stains_desc($addendum_code);
						$text_info =  array( 'text' => $addendum_desc);
						array_push($addendum_val, $text_info);
					}
					$addendum_pdf_content = "";
					if(!empty($addendum_val[0]) || !empty($addendum_val[1]) || !empty($addendum_val[2]))
					{
					$addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
					}
					foreach($addendum_val as $value)
					{
					$addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
					}
					$addendum_pdf_content .="<div style='clear:both;'></div></div>";
				}
			}
			$clinical_information_sql = "SELECT clinical_specimen FROM `wp_abd_clinical_info` WHERE specimen_id = '".$pending_report_view_details[0]['specimen_id']."'";
			$clinical_information = $this->BlankModel->customquery($clinical_information_sql);		
			$clinical_specimen_fst = $clinical_information[0]['clinical_specimen'];
			if($clinical_specimen_fst){
			$clinical_information_div = "";
			$clinical_specimen_fst = rtrim($clinical_specimen_fst, ',');	       
			$clinical_specimen_det = explode(",",$clinical_specimen_fst);
			$clinical_information_div .= '<div style="float:left; padding:0; width:62px; margin:0;"><strong>Location: </strong></div>';
			foreach ($clinical_specimen_det as $specimen)
			{ 
				$clinical_information_div.= '<div class="inr-spn" style="float:left; padding:0; margin:0 10px 0 0;">'.$specimen .'</div>'; 
			}
			}	   
	    
	     $specimen_sql = "SELECT * FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id = '".$pending_report_view_details[0]['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
		 $specimen_information_result =  $this->BlankModel->customquery($specimen_sql);	



		$nail_macro_sql = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '" . $pending_report_view_details[0]['diagnostic_short_code'] . "'";
      	$nail_macro     = $this->db->query($nail_macro_sql)->row_array();
      	$comment = nl2br($nail_macro['comments']);

	     $diagnostic_short_code_sql = "SELECT `color`, `diagnosis`,`text`, `sc` FROM `wp_abd_nail_macro_codes` where `sc` = '".$pending_report_view_details[0]['diagnostic_short_code']."'";
		 $diagnosis = $this->BlankModel->customquery($diagnostic_short_code_sql);	

		 $specimen_stage_details_sql = "SELECT `addtional_desc` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$pending_report_view_details[0]['specimen_id']."' AND`stage_id` = '2' ";
	    
		 $specimen_stage_details = $this->BlankModel->customquery($specimen_stage_details_sql);	 
	   $gross_description = $specimen_stage_details[0]['addtional_desc'];
		
		 if(!empty($pending_report_view_details[0]['stains']))
		 {
			$stains = explode(",",$pending_report_view_details[0]['stains']);
			if(!empty($stains[0]))
			{
			  $stv1= $stains[0];
			}
			else
			{
			   $stv1= "";
			}
			if(!empty($stains[1]))
			{
			  $stv2= $stains[1];
			}
			else
			{
				$stv2= "";
			}
			if(!empty($stains[2]))
			{
			  $stv3= $stains[2];
			}
			else
			{
				$stv3= "";
			}
			if(!empty($stains[0]))
			{
			   $Stains_desc1 = get_stains_desc($stains[0]);
			}
			else
			{
			   $Stains_desc1 = "";
			}
			if(!empty($stains[1]))
			{
			   $Stains_desc2 = get_stains_desc($stains[1]);
			}
			else
			{
			   $Stains_desc2 = "";
			}
			if(!empty($stains[2]))
			{
			   $Stains_desc3 = get_stains_desc($stains[2]);
			}
			else
			{
			   $Stains_desc3 = "" ;
			}
		 }
		 $stains_comments = '';
		 for($num = 0; $num < 3; $num++)
		 {
			$Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$stains[$num]."'";
			$Stain_shortcode = $this->BlankModel->customquery($Stain_short);
			$stains_comments.= $Stain_shortcode[0]['comments'];
			$stains_comments.=  '<div style= "margin:2px;"></div>';
		}
		 if($pending_report_view_details[0]['images'])
		 {
			$report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
			<label>&nbsp;</label></div>
			<div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
			<img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$pending_report_view_details[0]['images']." alt='Nail Fungal Image'/>
			</div>
			<div style='clear:both;'></div></div>";
		 }
		 
		 $color_array = array( 'green' => '#96faa2', 'red' => '#e53131', 'yellow' => '#f1ec2d');
	     $diagnostic_color = ($diagnosis[0]['color'] != '') ? $color_array[$diagnosis[0]['color']] : $color_array['yellow']; 
		 $assigned_partner_id =  $specimen_information_result[0]['physician_id']?get_user_meta($specimen_information_result[0]['physician_id'], '_assign_partner',true):'';
		 $assigned_partner = $assigned_partner_id['meta_value'];
if(!empty($specimen_information_result[0]['partners_company']) && $data['amend_note'] != '***' && $data['amend_note'] != ""){
			$amend_response = 1;
			$prtnr_id = $specimen_information_result[0]['partners_company'];
			$spe_id = $specimen_information_result[0]['id'];
			$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
			$partners_company_data = $this->db->query($partners_company_sql)->row_array();
			$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
			$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();
			$amend_note_pdf = "<div style='background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:2px; text-align:center; font:700 12px Arial, Helvetica, sans-serif;color:#000;'>
			<p style='font:700 16px Arial, Helvetica, sans-serif; color:#fff; margin:0; text-align: center;'>Amended Report</p>
			</div>";
			$logo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
			$pdfHeaderfile       = $this->load->view('amened_nail_fungal_nextgen','',true);
			
		 }else if(!empty($specimen_information_result[0]['partners_company'])){
			$prtnr_id = $specimen_information_result[0]['partners_company'];
			$spe_id = $specimen_information_result[0]['id'];
			$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
			$partners_company_data = $this->db->query($partners_company_sql)->row_array();
	
			$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
			$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();
	
			$logo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
			$pdfHeaderfile = $this->load->view('nail_fungal_nextgen','',true);
		 
       	}else if(!empty($assigned_partner) && $data['amend_note'] != '***' && $data['amend_note'] != ""){
			$amend_response = 1;
			$pdfHeaderfile       = $this->load->view('amened_nail_fungal_partner','',true);
			$logo_img            = get_user_meta($assigned_partner,'company_logo',true);
			$partner_address     = get_user_meta($assigned_partner, '_address',true);
			$partner_phn         = get_user_meta($assigned_partner, '_mobile', true);
			$partner_fax         = get_user_meta($assigned_partner, 'fax',     true);
			$partner_company     = get_user_meta($assigned_partner, 'first_name',true);
			$logo = $logo_img['meta_value'];
			$paddress = $partner_address['meta_value'];
			$pph = $partner_phn['meta_value'];
			$pfax = $partner_fax['meta_value'];
			$pcompany = $partner_company['meta_value'];
			$partner_footer_data = "Powered by Ability Diagnostics, LLC.  Phone: (801) 899-3828";
			$amend_note_pdf = "<div style='background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:2px; text-align:center; font:700 12px Arial, Helvetica, sans-serif;color:#000;'>
			<p style='font:700 16px Arial, Helvetica, sans-serif; color:#fff; margin:0; text-align: center;'>Amended Report</p>
			</div>";
		 }
			elseif($data['amend_note'] != '***' && $data['amend_note'] != ""){
				$amend_response = 1;
				$pdfHeaderfile = $this->load->view('amened_nail_fungal','',true);
				$logo = base_url()."assets/frontend/images/logo.png";
				$amend_note_pdf = "<div style='background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:2px; text-align:center; font:700 12px Arial, Helvetica, sans-serif;color:#000;'>
				<p style='font:700 16px Arial, Helvetica, sans-serif; color:#fff; margin:0; text-align: center;'>Amended Report</p>
				</div>";
			}
	
			elseif(!empty($assigned_partner ))
			{
		
				$pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
				$logo_img            = get_user_meta($assigned_partner,'company_logo',true);
				$partner_address     = get_user_meta($assigned_partner, '_address',true);
				$partner_phn         = get_user_meta($assigned_partner, '_mobile', true);
				$partner_fax         = get_user_meta($assigned_partner, 'fax',     true);
				$partner_company     = get_user_meta($assigned_partner, 'first_name',true);
				$logo = $logo_img['meta_value'];
				$paddress = $partner_address['meta_value'];
				$pph = $partner_phn['meta_value'];
				$pfax = $partner_fax['meta_value'];
				$pcompany = $partner_company['meta_value'];
			}
	
			else{
				
				$pdfHeaderfile = $this->load->view('nail_fungal','',true);
	
				$logo = base_url()."assets/frontend/images/logo.png";
			}
			$labdoc = $data['labdoc'];
			if(!empty($specimen_information_result[0]['partners_company'])){
					$name = $partners_specimen_data['physician_name'];
					$splite_name = $this->split_name($name);
					$fname = $splite_name[0];
					$lname = $splite_name[1];
					$add = $partners_specimen_data['physician_address'];
					$mob = $partners_specimen_data['physician_phone'];
					$fax = '';	 
			}else{
					$first_name = get_user_meta( $specimen_information_result[0]['physician_id'], 'first_name');				
					$last_name  = get_user_meta( $specimen_information_result[0]['physician_id'], 'last_name');
					$mobile  = get_user_meta( $specimen_information_result[0]['physician_id'], '_mobile');
					$address  = get_user_meta( $specimen_information_result[0]['physician_id'], '_address');
					$fax  = get_user_meta( $specimen_information_result[0]['physician_id'], 'fax');

					$fname = $first_name['meta_value'];
					$lname = $last_name['meta_value'];
					$mob  = $mobile['meta_value'];
					$add = $address['meta_value'];	
					$fax = $fax['meta_value'];
			}
			
			if($labdoc == '75770')
			{
				$logoddd_img = base_url()."assets/frontend/images/sign_img_jared.png";
				$lab_doc_desc = "Jared Szymanski, DO, FCAP";
			}
			else
			{
				$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
				$lab_doc_desc = "David R. Bolick, MD, FCAP";
			
			}
			$report_generate_datetime = '<div style="width:97%; float:left; color:#444; font:400 10px/14px Arial,Helvetica,sans-serif; ">Report signed on <span style="display:block;">'.date("m/d/Y").'</span> at <span style="display:block;">'.date("H:i").'</span></div>';
			$replace_in_pdf = array(
				"{Amended_Report}"     => $amend_note_pdf,
				"{logo}" 			   => $logo,
				"{report_name}"        => 'Pathology Report',
				"{date}" 			   => date('m/d/Y', strtotime($pending_report_view_details[0]['dor'])),
				"{patient_name}"	   => $specimen_information_result[0]['p_firstname']." ".$specimen_information_result[0]['p_lastname'],
				"{patient_phone}"	   => $specimen_information_result[0]['patient_phn'],
				"{patient_dob}"		   => $specimen_information_result[0]['patient_dob'],
				"{patient_col}"		   => $specimen_information_result[0]['collection_date'],
				"{assessioning_num}"   => $specimen_information_result[0]['assessioning_num'],
				"{patient_recive}"	   => date('m/d/Y', strtotime($specimen_information_result[0]['date_received'])),
				
				"{physician_name}"	   => (!empty($specimen_information_result[0]['partners_company']))?$partners_specimen_data['physician_name']:$fname." ".$lname,
				"{physician_addresss}" => (!empty($specimen_information_result[0]['partners_company']))?$partners_specimen_data['physician_phone']:$add,
				"{physician_mobile}"   => (!empty($specimen_information_result[0]['partners_company']))?$partners_specimen_data['physician_address']:$mob,
				"{physician_fax}"	   => (!empty($specimen_information_result[0]['partners_company']))?'':$fax,
				"{clinical_history}"   => $pending_report_view_details[0]['clinical_history'],
				"{clinicial_info}"	   => $clinical_information_div,
				"{site_indicator}"     => $specimen_information_result[0]['site_indicator'],
				"{diagnostic}"		   => $diagnosis[0]['diagnosis'],
				"{Stains_desc_fst}"	   => nl2br($Stains_desc1),
				"{Stains_desc_sec}"	   => nl2br($Stains_desc2),
				"{Stains_desc_thr}"    => nl2br($Stains_desc3),
				"{nail_fungal_img}"	   => $report_img,
				"{addendum}"		   => $addendum_pdf_content,
				"{gross_desc}"		   => $gross_description,
				"{note}"               => $data['amend_note'],
				"{lab_doc_desc}"	   => $lab_doc_desc,
				"{sign_img}"		   => $logoddd_img,
				"{diagnostic_color}"   => $diagnostic_color,
				"{gross_micro_description}" => $diagnosis[0]['text'],
				"{stains_comments}"	   => nl2br($stains_comments),
				"{comments}"		   => nl2br($comment),
				"{time}"			   => date("H:i", strtotime($pending_report_view_details[0]['dor'])),
				"{fax}"                => $pfax,
				"{address}"            => nl2br($paddress),
				"{phone}"              => $pph,
				"{partner_company}"    => $pcompany,
				"{partner_footer_data}"=> "",
				"{assigned_sign_img}"  => $logoddd_img,
				"{report_generate_datetime}" => $report_generate_datetime
		
			);

			if(!empty($specimen_information_result[0]['partners_company'])){
				$replace_in_pdf = array_merge($replace_in_pdf,array(
				"{partner_name}"	=>$partners_company_data['partner_name'],
				"{address_line1}"	=>$partners_company_data['partner_address1'],
				"{address_line2}"	=>$partners_company_data['partner_address2'],
				"{partner_phone}"	=>$partners_company_data['phone_no'],
				"{partner_fax}"		=>$partners_company_data['fax_no'],
				"{partner_cli}"		=>$partners_company_data['clia_no'],
				"{partner_lab_doc}"	=>str_ireplace('<p>','',$partners_company_data['lab_director']),
				"{patient_id}"		=>$partners_specimen_data['external_id'],
				));
			}
			$pdfTotalBody= $pdfHeaderfile;
			foreach($replace_in_pdf as $find => $replace)
			{
				$pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
			}

			$body = $pdfTotalBody;
			$message = $body;
			$mpdf = new mPDF('','A4','','', 1, 1, 0, 0, 0, 0); 
			$mpdf->WriteHTML($message);
			$p_Name = preg_replace('/\s+/', '_', $specimen_information_result[0]['p_lastname']);
			$accessioning_num =$specimen_information_result[0]['assessioning_num'];

			if(!empty($specimen_information_result[0]['partners_company'])){
		  	$l_name = $lname;
		  	$ability_pdf_string = $p_Name.'_'.$l_name.'_'.$accessioning_num.".pdf";
			}else{
				$l_name = get_user_meta($specimen_information_result[0]['physician_id'],'last_name',true );
				$ability_pdf_string = $p_Name.'_'.$l_name['meta_value'].'_'.$accessioning_num.".pdf";
			}
			//$ability_pdf_string = $p_Name.'_'.$lname.'_'.$specimen_information_result[0]['assessioning_num'].".pdf";
			$ability_pdf = str_replace(' ', '', $ability_pdf_string);
			$mpdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");

			if($amend_response == 1)
			{
				$up_data = array('amend_note'=>$data['amend_note'],'dor'=>$pending_report_view_details[0]['dor'],'nail_pdf'=>$ability_pdf,'signature_id'=>$labdoc);
				$conditions1 = " ( `nail_funagl_id` = '".$data['nail_id']."')";	
				$usersedit = $this->BlankModel->editTableData('nail_pathology_report', $up_data, $conditions1);
			}
			else
			{
				$up_data = array('dor'=>$pending_report_view_details[0]['dor'],'nail_pdf'=>$ability_pdf,'signature_id'=>$labdoc);
				$conditions1 = " ( `nail_funagl_id` = '".$data['nail_id']."')";	
				$usersedit = $this->BlankModel->editTableData('nail_pathology_report', $up_data, $conditions1);	
			}
			if($pdfTotalBody)
			{
				die( json_encode(array("status" =>'1', "amended_report_view_details" => $pending_report_view_details[0], 
				"clinician_location" => $clinical_specimen_det, "specimen_information" => $specimen_information_result[0], 
				"gross_description" => $gross_description, "diagnostic_short_code" => $diagnosis[0],'physician_info'=>"",
				"st1val"=>$stv1, "st2val"=>$stv2, "st3val"=>$stv3,"st1"=>$Stains_desc1,"st2"=>$Stains_desc2,"st3"=>$Stains_desc3,"addendum_val"=>$addendum_val,
				"old_date"=>"","new_date"=>"")));	
			}
			else
			{
				die( json_encode(array('status' =>'0')));		 
			}

		}
	}

   /**
    * 
    * Submitted Reports Edit End
    * 
    */	
	
	/**
	* 
	* Send Report
	* 
	*/
	function send_pending_report(){
	date_default_timezone_set('MST7MDT');
	$is_send = '';
	$data = json_decode(file_get_contents('php://input'), true);
	if(!empty($data)){
	  $sender_id              = $data['user_id'];
	  $nail_funagl_id         = $data['report_id'];	 
	  $nail_results           = $this->BlankModel->getReportDetails($nail_funagl_id);
	  $lab_id                 = $nail_results['lab_id'];
	  $specimen_id            = $nail_results['specimen_id'];	
	  $clinical_information   = $this->clinical_function_html($specimen_id);	 
	  $datetime               = date('m/d/Y');
	  $accessioning_num       = $nail_results['assessioning_num'];
	  

	if($nail_results['images']){
	
	$report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
	<label>&nbsp;</label></div>
	<div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
	<img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$nail_results['images']."  alt='No Image'/>
	</div>
	<div style='clear:both;'></div></div>";
	}

  $color_array = array( 'green' => '#96faa2', 'red' => '#e53131', 'yellow' => '#f1ec2d');
  $ser_value = serialize($nail_results['addendum']);
  if(!empty($nail_results['addendum'])) {
  $addendum_value = unserialize($ser_value);  
  $add_text = explode(",", $addendum_value);
  $addendum_pdf_content = "";
  if(!empty($nail_results['addendum'][0]) || !empty($nail_results['addendum'][1]) || !empty($nail_results['addendum'][2])) {
  $addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
  }
  foreach($add_text as $value){
  $text_sql = "SELECT `text` FROM `wp_abd_nail_macro_codes` where sc = '" . $value. "'";
  $text     = $this->db->query($text_sql)->row_array();

  $addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$text['text']."</div>";
  }
  $addendum_pdf_content .="<div style='clear:both;'></div></div>";
  }
  $physician_id = $nail_results['physician_id'];

  $assigned_partner =  get_user_meta_value($physician_id, '_assign_partner',true);
  if(!empty($nail_results['partners_company'])){
	$prtnr_id = $nail_results['partners_company'];
	$spe_id   = $nail_results['id'];
	$partners_company_sql   = "SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '".$prtnr_id."'";
	$partners_company_data  = $this->db->query($partners_company_sql)->row_array();
	$partners_specimen_sql  = "SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '".$spe_id."'";
	$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();
	$logo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
	$pdfHeaderfile = $this->load->view('nail_fungal_nextgen','',true);
	
    }
    else if($assigned_partner){
    $pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
   	$logo_img            = get_user_meta_value($assigned_partner,'company_logo',true);
    $logo                = $logo_img['url'];
    $partner_address     = get_user_meta_value($assigned_partner, '_address',true);
    $partner_phn         = get_user_meta_value($assigned_partner, '_mobile', true);
    $partner_fax         = get_user_meta_value($assigned_partner, 'fax',     true);
    $partner_company     = get_user_meta_value($assigned_partner, 'first_name',true);
    //$partner_footer_data = "Powered by Ability Diagnostics, LLC.  Phone: (801) 899-3828";
  }
  else{
    $pdfHeaderfile = $this->load->view('nail_fungal','',true);
	$logo = base_url()."assets/frontend/images/logo.png";
  }
  

  $nail_macro_sql = "SELECT * FROM `wp_abd_nail_macro_codes` where sc = '" . $nail_results['diagnostic_short_code']. "'";
  $nail_macro     = $this->db->query($nail_macro_sql)->row_array();
  
  $comments = $nail_macro['comments'];
  $Stain_shortcodes = explode(",",$nail_results['stains']);
  $Stains_desc_fst = get_stains_desc($Stain_shortcodes[0]); 
  $Stains_desc_sec = get_stains_desc($Stain_shortcodes[1]); 
  $Stains_desc_thr = get_stains_desc($Stain_shortcodes[2]); 
  $stains_comments = "";
  $diagnostic_color = ($nail_macro['color'] != '') ? $color_array[$nail_macro['color']] : $color_array['yellow'];

  for($num = 0; $num < 3; $num++){
    $Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$Stain_shortcodes[$num]."'";
    $Stain_shortcode = $this->db->query($Stain_short)->row_array();
    $stains_comments.= $Stain_shortcode['comments'];
    $stains_comments.=  '<div style= "margin:2px;"></div>';
  }

  $labdoc = $nail_results['signature_id'];
	if($labdoc == '75770')
	{
		$logoddd_img = base_url()."assets/frontend/images/sign_img_jared.png";
		$lab_doc_desc = "Jared Szymanski, DO, FCAP";
	}
	else
	{
		$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
		$lab_doc_desc = "David R. Bolick, MD, FCAP";
		
	}
	if(!empty($nail_results['partners_company'])){
		$name = $partners_specimen_data['physician_name'];
		$splite_name = $this->split_name($name);
		$name = $partners_specimen_data['physician_name'];
		$fname = $splite_name[0];
		$lname = $splite_name[1];
		$add = $partners_specimen_data['physician_address'];
		$mob = $partners_specimen_data['physician_phone'];
		$fax = '';
	}
	else{
		$fname = get_user_meta_value($physician_id, 'first_name', true);
		$lname = get_user_meta_value($physician_id, 'last_name', true );
		$name  = $fname.' '.$lname;
		$add   = get_user_meta_value($physician_id, '_address', true);
		$mob   = get_user_meta_value($physician_id, '_mobile', true);
		$fax   = get_user_meta_value($physician_id, 'fax', true);
	}
   $report_generate_datetime = '<div style="width:97%; float:left; color:#444; font:400 10px/14px Arial,Helvetica,sans-serif; ">Report signed on <span style="display:block;">'.date("m/d/Y").'</span> at <span style="display:block;">'.date("H:i").'</span></div>';
   $pdfTotalBody   = $pdfHeaderfile;  

  
  $replace_in_pdf = array(
    "{logo}"         => $logo,
    "{date}"         => date('m/d/Y'),
    "{report_name}"        => 'Pathology Report',
    "{patient_name}"       => $nail_results['p_firstname']." ".$nail_results['p_lastname'],
    "{patient_phone}"      => $nail_results['patient_phn'],
    "{patient_dob}"        => $nail_results['patient_dob'],
    "{patient_col}"        => $nail_results['collection_date'],
    "{assessioning_num}"   => $nail_results['assessioning_num'],
    "{patient_recive}"     => date('m/d/Y', strtotime($nail_results['create_date'])),    
    "{physician_name}"	   => (!empty($nail_results['partners_company']))?$partners_specimen_data['physician_name']:$name,
    "{physician_addresss}" => (!empty($nail_results['partners_company']))?$partners_specimen_data['physician_phone']:$add,
    "{physician_mobile}"   => (!empty($nail_results['partners_company']))?$partners_specimen_data['physician_address']:$mob,
    "{physician_fax}"	   => (!empty($nail_results['partners_company']))?'':$fax,
    "{clinical_history}"   => $nail_results['clinical_history'],
    "{clinicial_info}"     => $clinical_information,
    "{site_indicator}"     => $nail_results['site_indicator'],
    "{diagnostic}"         => $nail_macro['diagnosis'],
    "{Stains_desc_fst}"    => nl2br($Stains_desc_fst),
    "{Stains_desc_sec}"    => nl2br($Stains_desc_sec),
    "{Stains_desc_thr}"    => nl2br($Stains_desc_thr),
    "{addendum}"           => $addendum_pdf_content,
    "{gross_desc}"         => $nail_results['gross_description'],
    "{nail_fungal_img}"    => $report_img,
    "{lab_doc_desc}"       => $lab_doc_desc,
    "{sign_img}"           => $logoddd_img,
    "{diagnostic_color}"   => $diagnostic_color,
    "{gross_micro_description}" => $nail_macro['text'],
    "{stains_comments}"         => nl2br($stains_comments),
    "{comments}"                => nl2br($comments),
    "{time}"                    => date("H:i"),
    "{fax}"                => $partner_fax,
    "{address}"            => nl2br($partner_address),
    "{phone}"              => $partner_phn,
    "{partner_company}"    => $partner_company,
    "{partner_footer_data}"=> "",
    "{assigned_sign_img}"  => $logoddd_img,
    "{report_generate_datetime}" => $report_generate_datetime
    );

  	if(!empty($nail_results['partners_company'])){
		$replace_in_pdf = array_merge($replace_in_pdf,array(
		"{partner_name}"	=> $partners_company_data['partner_name'],
		"{address_line1}"	=> $partners_company_data['partner_address1'],
		"{address_line2}"	=> $partners_company_data['partner_address2'],
		"{partner_phone}"	=> $partners_company_data['phone_no'],
		"{partner_fax}"		=> $partners_company_data['fax_no'],
		"{partner_cli}"		=> $partners_company_data['clia_no'],
		"{partner_lab_doc}"	=> str_ireplace('<p>','',$partners_company_data['lab_director']),
		"{patient_id}"		=> $partners_specimen_data['external_id'],
		));
	}
	 
  foreach($replace_in_pdf as $find => $replace){
  $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
  }
 
  $message = $pdfTotalBody;
	$mpdf       = new mPDF('', 'A4', '', '', 1, 1, 0, 0, 0, 0); 
	$mpdf->WriteHTML($pdfTotalBody);
	$p_Name = preg_replace('/\s+/', '_', $nail_results['p_lastname']);	
	if(!empty($nail_results['partners_company'])){
		$physician_lname = $lname;
		$ability_pdf_string = $p_Name.'_'.$physician_lname.'_'.$accessioning_num.".pdf";
	}
	else{
		$physician_lname = get_user_meta_value($physician_id, 'last_name', true );
		$ability_pdf_string = $p_Name.'_'.$physician_lname.'_'.$accessioning_num.".pdf";
	}

  
  $ability_pdf = str_replace(' ', '', $ability_pdf_string);
  $mpdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");
  $pdf_link = base_url().'assets/uploads/histo_report_pdf/'.$ability_pdf; 

 //  // Ending nail report

  $this->db->where('nail_funagl_id',$nail_funagl_id);
   $updateData = $this->db->update('wp_abd_nail_pathology_report', array('status'        => "Active",
  		'nail_pdf'      => $ability_pdf,
		'dor'           => date('Y-m-d H:i:s'),
		'modify_date'   => date('Y-m-d H:i:s'), 
    		'report_sender' => $sender_id ));


   $pathology_sql = "SELECT * FROM (SELECT `nail_funagl_id`,`specimen_id`,`status`,`nail_pdf` FROM wp_abd_nail_pathology_report WHERE nail_funagl_id = '".$nail_funagl_id."' )nail_patho
  INNER JOIN (SELECT `id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id` FROM wp_abd_specimen WHERE status = 0) specimen
  ON
  specimen.id = nail_patho.specimen_id
  AND nail_patho.status ='Active'";
  $pathology_results = $this->db->query($pathology_sql)->row_array();	 
	if(!empty($nail_results['partners_company'])){
		$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
						  array(
								'physician_id'    => 0,
								'partners_company_id'=>$nail_results['partners_company'],
								'report_pdf_link' => $pdf_link,
								'report_name'     => $ability_pdf,
								'report_id'       => $pathology_results['nail_funagl_id'],
								'create_date'     => date('Y-m-d H:i:s')								
							));
	}
	else{
		$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
						  array(
								'physician_id'    => $pathology_results['physician_id'],
								'report_pdf_link' => $pdf_link,
								'report_name'     => $ability_pdf,
								'report_id'       => $pathology_results['nail_funagl_id'],
								'create_date'     => date('Y-m-d H:i:s')								
							));
	}
	if(!empty($nail_results['partners_company'])){
		$partners_company_sql = "SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '".$nail_results["partners_company"]."'";
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();
		$report_recive_ways = $partners_company_data['report_recive_way'];	
		$selected_report_recive_way = explode(',', $report_recive_ways);
	}
	else{
		$report_recive_ways = get_user_meta_value($pathology_results['physician_id'], 'report_recive_way', true);	
		$selected_report_recive_way = explode(',', $report_recive_ways);
	}

  if($report_recive_ways){
  foreach($selected_report_recive_way as $selected_report_recive){
 
  if($selected_report_recive == 'Fax'){
  $fax_status = 'Yes';
  } 
  elseif($selected_report_recive == 'None'){
  $fax_status = 'No';
  
    }
   }
  }
  else{
  $fax_status = 'Yes';
  }

  if($fax_status === 'Yes'){
  
	if(empty($nail_results['partners_company'])){
		$fax_number = get_user_meta_value($nail_results['physician_id'],'fax', true);
		$fax_no     = str_replace(".","-",$fax_number);
		$name       = 'Dr '.get_user_meta_value($nail_results['physician_id'], 'first_name',  true)." ".get_user_meta_value($nail_results['physician_id'], 'last_name',  true);
		$mobile = $mob;
		$logo   = base_url()."assets/frontend/images/logo.png";
	}
	else{
		$prtnr_id = $nail_results['partners_company'];
		$partners_company_sql  = 'SELECT * FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();
		$fax_no = str_replace(".","-",$partners_company_data['fax_no']);
		$name   = $partners_company_data['partner_name'];
		$mobile = $partners_company_data['phone_no'];
		$logo   = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
	}

  
	$report_pdf_link = $pdf_link;
	$pdfHeaderfile   = $this->load->view('print_fax_cover_pdf','',true);
	$pdfTotalBody    =  $pdfHeaderfile;
  
  	$replace_in_pdf = array(
			"{logo}"          => $logo,
			"{date}"          => date('m/d/Y'),
			"{physician_name}"  => $name,
			"{physician_mobile}"=> $mobile,
			"{physician_fax}"   => $fax_no
		);
	foreach($replace_in_pdf as $find => $replace):
	$pdfHeaderfile = str_replace($find, $replace, $pdfHeaderfile);
	endforeach;


   $mpdf    = new mPDF('','A4','','', 1, 1, 0, 0, 0, 0); 
   $mpdf->WriteHTML($pdfHeaderfile);

  
if(!empty($nail_results['partners_company'])){
		$p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
		$l_name = $lname;
		$fax_cover = $p_Name.'_'.$l_name.'_'.$accessioning_num.".pdf";
	}else{
		$p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
		$fax_cover = $p_Name.'_'.get_user_meta_value($pathology_results['physician_id'],'last_name',true ).'_'.$pathology_results['assessioning_num'].'.pdf';
	}
  $fax_cover_pdf = str_replace(' ', '', $fax_cover);
  
  //$mpdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$fax_cover_pdf,"F");
  $mpdf->Output(FCPATH."assets/uploads/fax_cover_photo/".$fax_cover_pdf,"F");
  $fax_cover_pdf_link = base_url().'assets/uploads/fax_cover_photo/'.$fax_cover_pdf;

  header("Content-Type: text/html");
	$efax = new eFax(false);
	$efax->set_account_id("8014694891");
	$efax->set_user_name("antaresps");
	$efax->set_user_password("antaresps");
	$efax->add_recipient($name, "Ability Diagnostics", $fax_no);
	$fax_cover = file_get_contents($fax_cover_pdf_link);
	$efax->add_file("pdf", $fax_cover);
	$report_pdf  = file_get_contents($report_pdf_link);
	$efax->add_file("pdf", $report_pdf);
	$efax->set_outbound_url("https://secure.efaxdeveloper.com/EFax_WebFax.serv");
	$efax->set_fax_id("Fax #" . rand());
	$efax->add_disposition_email("Trajan King", "trajanking@gmail.com");
	$efax->add_disposition_email("Blake Clouse", "blake@abilitydiagnostics.com");
	$efax->set_disposition_level(eFax::RESPOND_ERROR | eFax::RESPOND_SUCCESS);
	$efax->set_disposition_method("EMAIL");
	$efax->set_duplicate_id(true);
	$efax->set_fax_header("@DATE @TIME Ability Diagnostics");
	$efax->set_priority("NORMAL");
	$efax->set_resolution("STANDARD");
	$efax->set_self_busy(true);
  
  try{
	$result  = $efax->send($efax->message());
	$result  = true;
	$is_send = $result;
	if($result){
		unlink('assets/uploads/fax_cover_photo/'.$fax_cover_pdf); 
	  	$this->db->where('report_id',$pathology_results['nail_funagl_id']);
	    $updateData = $this->db->update('wp_abd_auto_mail', array('fax_cover_pdf_link' => $fax_cover_pdf_link));
	}
   }
   catch(eFaxException $e){
   }
  
  }
  
     $anthr_fax_number = get_user_meta_value($pathology_results['physician_id'],'anthr_fax_number', true);  
  if($anthr_fax_number !=""){
  
	$another_fax_no = str_replace(".","-",$anthr_fax_number);

	$report_pdf_link = $pdf_link;
	$name = 'Dr '.get_user_meta_value($pathology_results['physician_id'], 'first_name',  true)." ".get_user_meta_value($pathology_results['physician_id'], 'last_name',  true);
	$pdfHeaderfile = $this->load->view('print_fax_cover_pdf','',true);
	$pdfTotalBody   =  $pdfHeaderfile;
  
    $logo = get_custom_logo_url();
    $replace_in_pdf = array(
						  	"{logo}" 		    => $logo,
						  	"{date}" 		    => date('m/d/Y'),
						  	"{physician_name}"  => get_user_meta_value($pathology_results['physician_id'], 'first_name',true)." ".get_user_meta_value($pathology_results['physician_id'],'last_name',true),
						  	"{physician_mobile}"=> get_user_meta_value($pathology_results['physician_id'], '_mobile',true),
						  	"{physician_fax}"   => $another_fax_no
			 				 );
  foreach($replace_in_pdf as $find => $replace){
  	 $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
  }
 

  $mpdf    = new mPDF('','A4','','', 1, 1, 0, 0, 0, 0); 
  $mpdf->WriteHTML($pdfTotalBody);

  
  $p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
  $fax_cover = $p_Name.'_'.get_user_meta_value($pathology_results['physician_id'],'last_name',true ).'_'.$pathology_results['assessioning_num'].'.pdf';
  $fax_cover_pdf = str_replace(' ', '', $fax_cover);
  
  //$mpdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$fax_cover_pdf,"F");
  $mpdf->Output(FCPATH."assets/uploads/fax_cover_photo/".$fax_cover_pdf,"F");
  $fax_cover_pdf_link = base_url().'assets/uploads/fax_cover_photo/'.$fax_cover_pdf;
  header("Content-Type: text/html");
  $efax = new eFax(false);
  $efax->set_account_id("8014694891");
  $efax->set_user_name("antaresps");
  $efax->set_user_password("antaresps");
  $efax->add_recipient($name, "Ability Diagnostics", $another_fax_no);
  $fax_cover = file_get_contents($fax_cover_pdf_link);
  $efax->add_file("pdf", $fax_cover);
  $report_pdf  = file_get_contents($report_pdf_link);
  $efax->add_file("pdf", $report_pdf);
  $efax->set_outbound_url("https://secure.efaxdeveloper.com/EFax_WebFax.serv");
  $efax->set_fax_id("Fax #" . rand());
  $efax->add_disposition_email("Trajan King", "trajanking@gmail.com");
  $efax->add_disposition_email("Blake Clouse", "blake@abilitydiagnostics.com");
  $efax->set_disposition_level(eFax::RESPOND_ERROR | eFax::RESPOND_SUCCESS);
  $efax->set_disposition_method("EMAIL");
  $efax->set_duplicate_id(true);
  $efax->set_fax_header("@DATE @TIME Ability Diagnostics");
  $efax->set_priority("NORMAL");
  $efax->set_resolution("STANDARD");
  $efax->set_self_busy(true);
  
	try{
		$result = $efax->send($efax->message());
	  $result  = true;
      $is_send = $result;
		unlink('assets/uploads/fax_cover_photo/'.$fax_cover_pdf); 
	}
		catch(eFaxException $e){
	}
	}

	if($updateData){
		die( json_encode(array('status' =>'1')));		 
	}
	else{
		die( json_encode(array('status' =>'0')));		
	}
	 
    }
  
   }

	/**
	* 
	* Send Report End
	*/
	function clinical_function_html($specimen_id)
	{
	$specimen_det      = "SELECT * FROM wp_abd_clinical_info WHERE specimen_id ='".$specimen_id."'";
	$specimen_results  = $this->db->query($specimen_det)->row_array();
	
	$clinical_information  = '';
	$clinical_specimen_fst = $specimen_results['clinical_specimen'];
	$clinical_specimen     = explode(",",$clinical_specimen_fst);
	$clinical_information .= '<div style="float:left; padding:0; width:62px; margin:0;"><strong>Location: </strong></div>';
	foreach ($clinical_specimen as $specimen){ $clinical_information.= '<div class="inr-spn" style="float:left; padding:0; margin:0 10px 0 0;">'.$specimen .'</div>'; }
	return $clinical_information; 
	}

	function clinical_function($specimen_id)
	{
		$specimen_det      = "SELECT * FROM wp_abd_clinical_info WHERE specimen_id ='".$specimen_id."'";
		$specimen_results  = $this->db->query($specimen_det)->row_array();
		
		$clinical_information  = array();
		$clinical_specimen_fst = $specimen_results['clinical_specimen'];
		$clinical_specimen     = explode(",",$clinical_specimen_fst);
		// $clinical_information .= '<div style="float:left; padding:0; width:62px; margin:0;"><strong>Location: </strong></div>';
		foreach ($clinical_specimen as $specimen){
			if($specimen != '')
		 	array_push($clinical_information, $specimen);
		}
		return $clinical_information; 
	}

	function get_nail_fungal_pathology_report(){
		$data = json_decode(file_get_contents('php://input'), true);
		date_default_timezone_set('MST7MDT');
		$partners_arr='';
		if(!empty($data)){
			$specimen_id = $data['id'];
			if(isset($data['nail_funagl_id'])){
				$nail_funagl_id = $data['nail_funagl_id'];
				$nail_report       		= "SELECT * FROM wp_abd_nail_pathology_report WHERE nail_funagl_id = '".$nail_funagl_id."'";
				$nail_results      		= $this->db->query($nail_report)->row_array();
				$nail_results['dor'] 	= date('m/d/Y', strtotime($nail_results['dor']));
			}else{
				$nail_results = array('dor'=>date('m/d/Y'));
			}
			
			$specimen_det      		= "SELECT * FROM wp_abd_specimen WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id ='".$specimen_id."' ORDER BY `id` DESC LIMIT 1";
			$specimen_results  		= $this->db->query($specimen_det)->row_array();
			if(!empty($specimen_results['partners_company'])){
				$partners_specimen_sql = "SELECT * FROM wp_abd_partners_specimen WHERE specimen_id ='".$specimen_id."' ORDER BY `id` DESC LIMIT 1";
				$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();

				$partners_arr= array('name'=>$partners_specimen_data['physician_name'],'add'=>$partners_specimen_data['physician_address'],'mob'=>$partners_specimen_data['physician_phone']);
			}
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);

			$fname 		= get_user_meta( $specimen_results['physician_id'], 'first_name',true );
			$lname 		= get_user_meta( $specimen_results['physician_id'],'last_name',true );
			$address 	= get_user_meta($specimen_results['physician_id'], '_address',true);
			$mob 		= get_user_meta($specimen_results['physician_id'], '_mobile',true);
			$fax 		= get_user_meta($specimen_results['physician_id'], 'fax',true);
			$name = $fname['meta_value'].' '.$lname['meta_value'];
			$clinical_information   = $this->clinical_function($specimen_id);

			$physician_info = array('name'=>$name,'add'=>$address['meta_value'],'mob'=>$mob['meta_value'],'fax'=>$fax['meta_value']);

			$specimen_stage_details = "SELECT `addtional_desc`,`comments`,`total_desc` FROM wp_abd_specimen_stage_details WHERE `specimen_id`= '".$specimen_id."' AND`stage_id`=2";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();

			if(!empty($specimen_results)){
				die( json_encode(array('status'=>'1','nail_results'=>$nail_results,'specimen_results'=>$specimen_results,'datetime'=>$datetime,'physician_info'=>$physician_info,'clinical_information'=>$clinical_information,'GrossDescription'=>$GrossDescription,'partners_specimen_data'=>$partners_arr)));
			}else{
				die( json_encode(array('status'=>'0')));
			}

		}
		
	}

	function get_nail_macro_code(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = $data['sort_code'];
			$name = $data['name'];
			$result = $this->db->query("SELECT * FROM `wp_abd_nail_macro_codes` WHERE sc LIKE '" . $info . "'")->row_array();

			if(!empty($result)){
				die( json_encode(array('status'=>'1','marcro_data'=>$result,'name'=>$name)));
			}else{
				die( json_encode(array('status'=>'0','name'=>$name)));
			}
			
		}
	}

    /**
	* 
	* Add Report 
	*/
	
	 function add_report_data(){
		date_default_timezone_set('MST7MDT');
	 	if($this->input->post()){
		 	$addendum_code = array();
		 	$addendum_text = array();
		 	$name = $this->input->post('diagnostic_text');
		 	$id = $this->input->post('id');
		 	$addendum = json_decode($this->input->post('addendum'));
		 	$clinical_history = $this->input->post('clinical_history');
		 	$diagnostic_color = $this->input->post('diagnostic_color');
		 	$diagnostic_short_code = $this->input->post('diagnostic_short_code');
		 	$diagnostic_text = $this->input->post('diagnostic_text');
		 	$gross_description = $this->input->post('gross_description');
		 	$gross_micro_desc_text = $this->input->post('gross_micro_desc_text');
		 	$stains_addendum_first = $this->input->post('stains_addendum_first');
		 	$stains_addendum_sec = $this->input->post('stains_addendum_sec');
		 	$stains_addendum_third = $this->input->post('stains_addendum_third');
		 	$stains_first = $this->input->post('stains_first');
		 	$stains_sec = $this->input->post('stains_sec');
		 	$stains_third = $this->input->post('stains_third');
			$user_id = $this->input->post('user_id');
			$labdoc = $this->input->post('labdoc');
		 	

		 	$specimen_det      		= "SELECT * FROM wp_abd_specimen WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id ='".$id."' GROUP BY `id`";
			$specimen_results  		= $this->db->query($specimen_det)->row_array();
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);
			$clinical_information   = $this->clinical_function_html($id);
			$specimen_stage_details = "SELECT `addtional_desc`, `comments`, `total_desc` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id`= '".$id."' AND`stage_id`= '2'";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();
			$nail_report = "SELECT `nail_funagl_id` FROM wp_abd_nail_pathology_report WHERE specimen_id = '".$id."'";
	  		$nail_results = $this->db->query($nail_report)->row_array();
	  
		  if(!empty($nail_results)){
		     $reports_already_exits = "Report is already generated for this Specimen whose accessioning number is ".$specimen_results['assessioning_num'];
		  	die( json_encode(array('status'=>'0','reports_already_exits'=>$reports_already_exits)));
		  }
		  else
		  {

		  	if(!empty($addendum)){
		 		foreach ($addendum as $value) {
			 		array_push($addendum_code, $value->addendum_code);
			 		array_push($addendum_text, $value->addendum_text);
			 	}
		 	}
		 	
		 	$ser_value = serialize($addendum_text);

		 	if($_FILES && $_FILES['file_data']['name']){
				$config['upload_path'] = 'assets/uploads/nail_fungal';
				$config['allowed_types'] = 'jpeg|jpg|png';
				$config['max_size'] = 5000000;
				$new_name = time().'_'.$_FILES["file_data"]['name'];
				$config['file_name'] = $new_name;

				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('file_data')){
					die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
				}else{
					$uploadData = $this->upload->data();
					$fileName = $uploadData['file_name'];
				}
			}

		  if($diagnostic_color == "green"){
		  	$diagnostic_color = "#96faa2";
		  }
		  elseif($diagnostic_color == "red"){
		  	$diagnostic_color = "#e53131";
		  }
		  else{
		  	$diagnostic_color = "#f1ec2d";
		  }
		  $filename_new = $fileName;
		  
		  $array_val = count(array_filter($addendum_text));
		  if(0 < $array_val)
		  {
		  $addendum_value = unserialize($ser_value); 	
		  $addendum_pdf_content = "";
		  $addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
		  
		  foreach($addendum_value as $value)
		  {
		  $addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
		  }
		  $addendum_pdf_content .="<div style='clear:both;'></div></div>"; 
		  }
	  
	 
		  if($filename_new){
		  $report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <label>&nbsp;</label></div>
		  <div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$filename_new." alt='No Image'/>
		  </div>
		  <div style='clear:both;'></div></div>";
		  }

		  $Stain_shortcodes = array();
		  array_push($Stain_shortcodes, $stains_first);
		  array_push($Stain_shortcodes, $stains_sec);
		  array_push($Stain_shortcodes, $stains_third);
		  $Stains_desc = $this->get_stains_desc($stains_first); 
		  $Stains_desc_one = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_sec); 
		  $Stains_desc_sec = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_third); 
		  $Stains_desc_thr = $Stains_desc['text'];
		  $stains_comments = "";
	  
	  for($num = 0; $num < 3; $num++){
	  	$Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$Stain_shortcodes[$num]."'";
	  	$Stain_shortcode = $this->db->query($Stain_short)->row_array();
	  	$stains_comments.= $Stain_shortcode['comments'];
	  	$stains_comments.=  '<div style= "margin:2px;"></div>';
	  }
	  $nail_macro_sql = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '" . $diagnostic_short_code . "'";
	  $nail_macro     = $this->db->query($nail_macro_sql)->row_array();
	  $comments = nl2br($nail_macro['comments']);
	  
	  $physician_id = "'".$specimen_results['physician_id']."'";
	  
	  $assigned_partner_meta =  get_user_meta($physician_id, '_assign_partner',true);
	  
	  $assigned_partner = $assigned_partner_meta['meta_value'];
	  
if(!empty($specimen_results['partners_company'])){
		$prtnr_id = $specimen_results['partners_company'];
		$spe_id = $specimen_results['id'];
		$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();

		$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
		$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();

		$logo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
		$pdfHeaderfile = $this->load->view('nail_fungal_nextgen','',true);
	
	}else if(!empty($assigned_partner)){
	  	$pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
		$logo            		 = get_user_meta_value($assigned_partner,'company_logo',true);
		$partner_address     = get_user_meta_value($assigned_partner, '_address',true);
		$partner_phn         = get_user_meta_value($assigned_partner, '_mobile', true);
		$partner_fax         = get_user_meta_value($assigned_partner, 'fax',     true);
		$partner_company     = get_user_meta_value($assigned_partner, 'first_name',true);
		//$logo = $logo_img['meta_value'];
		//$paddress = $partner_address['meta_value'];
		//$pph = $partner_phn['meta_value'];
		//$pfax = $partner_fax['meta_value'];
		//$pcompany = $partner_company['meta_value'];
	  }
	  else{
	  	$pdfHeaderfile = $this->load->view('nail_fungal','',true);
		$logo = base_url()."assets/frontend/images/logo.png";
	  }
	  
	  if($labdoc == '75770')
	  {
				$logoddd_img = base_url()."assets/frontend/images/sign_img_jared.png";
				$lab_doc_desc = "Jared Szymanski, DO, FCAP";
			}
			else
			{
				$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
				$lab_doc_desc = "David R. Bolick, MD, FCAP";
	
			}
	if(!empty($specimen_results['partners_company'])){
		$name = $partners_specimen_data['physician_name'];
		$splite_name = $this->split_name($name);
		$name = $partners_specimen_data['physician_name'];
	  	$fname = $splite_name[0];
	  	$lname = $splite_name[1];
	  	$add = $partners_specimen_data['physician_address'];
	  	$mob = $partners_specimen_data['physician_phone'];
	  	$fax = '';
	}else{
		$fname = get_user_meta_value($physician_id, 'first_name', true);
		$lname = get_user_meta_value($physician_id, 'last_name', true );
		$name  = $fname.' '.$lname;
		$add   = get_user_meta_value($physician_id, '_address', true);
		$mob   = get_user_meta_value($physician_id, '_mobile', true);
		$fax   = get_user_meta_value($physician_id, 'fax', true);

	}


	   $pdfTotalBody   = $pdfHeaderfile;  	  
	   $replace_in_pdf = array(
	  	"{logo}" 			   => $logo,
	  	"{date}" 			   => '___',
	  	"{report_name}"        => 'Preliminary Report',
	  	"{patient_name}"	   => $specimen_results['p_firstname']." ".$specimen_results['p_lastname'],
	  	"{patient_phone}"	   => $specimen_results['patient_phn'],
	  	"{patient_dob}"		   => $specimen_results['patient_dob'],
	  	"{patient_col}"		   => $specimen_results['collection_date'],
	  	"{assessioning_num}"   => $specimen_results['assessioning_num'],
	  	"{patient_recive}"	   => date('m/d/Y', strtotime($datetime['0'])),
	  	
	    "{physician_name}"	   => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_name']:$name,
	  	"{physician_addresss}" => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_phone']:$add,
	  	"{physician_mobile}"   => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_address']:$mob,
	  	"{physician_fax}"	   => (!empty($specimen_results['partners_company']))?'':$fax,
	  	"{clinical_history}"   => $clinical_history,
	  	"{site_indicator}"     => $specimen_results['site_indicator'],
	  	"{clinicial_info}"	   => $clinical_information,
	  	"{diagnostic}"		   => $diagnostic_text,
	  	"{Stains_desc_fst}"	   => nl2br($Stains_desc_one),
	  	"{Stains_desc_sec}"	   => nl2br($Stains_desc_sec),
	  	"{Stains_desc_thr}"    => nl2br($Stains_desc_thr),
	  	"{addendum}"		   => $addendum_pdf_content,
	  	"{gross_desc}"		   => $gross_description,
	  	"{nail_fungal_img}"	   => $report_img,
	  	"{lab_doc_desc}"	   => '',
	  	"{sign_img}"		   => '',
	  	"{diagnostic_color}"   => $diagnostic_color,
	  	"{gross_micro_description}"=> $gross_micro_desc_text,
	  	"{stains_comments}"	   => nl2br($stains_comments),
	  	"{comments}"		   => nl2br($comments),
	  	"{time}"			   => '___',
	  	"{fax}"                => $partner_fax,
	  	"{address}"            => nl2br($partner_address),
	  	"{phone}"              => $partner_phn,
	  	"{partner_company}"    => $partner_company,
	  	"{report_generate_datetime}" => '',
	  	"{partner_footer_data}"=> "",
	  	"{assigned_sign_img}"  => "",

	  );
	  //die( json_encode(array('status'=>'test')));
		if(!empty($specimen_results['partners_company'])){
	  	$replace_in_pdf = array_merge($replace_in_pdf,array(
	  		"{partner_name}"	=>$partners_company_data['partner_name'],
			"{address_line1}"	=>$partners_company_data['partner_address1'],
			"{address_line2}"	=>$partners_company_data['partner_address2'],
			"{partner_phone}"	=>$partners_company_data['phone_no'],
			"{partner_fax}"		=>$partners_company_data['fax_no'],
			"{partner_cli}"		=>$partners_company_data['clia_no'],
			"{partner_lab_doc}"	=>str_ireplace('<p>','',$partners_company_data['lab_director']),
			"{patient_id}"		=>$partners_specimen_data['external_id'],
	  	));
	  }
	  foreach($replace_in_pdf as $find => $replace){
	  $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
	  }

	  $archivePdf = new mPDF('', 'A4', '', '', 1, 1, 0, 0, 0, 0); 
	  $archivePdf->SetWatermarkText('PRELIMINARY');
	  $archivePdf->showWatermarkText = true;
	  $archivePdf->WriteHTML($pdfTotalBody,2);
	  $accessioning_num = $specimen_results['assessioning_num'];
	  
	  $p_Name = preg_replace('/\s+/', '_', $specimen_results['p_lastname']);
		if(!empty($specimen_results['partners_company'])){
		  	$l_name = $lname;
		  	$ability_pdf_string = $p_Name.'_'.$l_name.'_'.$accessioning_num.".pdf";
		}else{
			$l_name = get_user_meta($physician_id,'last_name',true );
			$ability_pdf_string = $p_Name.'_'.$l_name['meta_value'].'_'.$accessioning_num.".pdf";
		}
	  
	  $ability_pdf = str_replace(' ', '', $ability_pdf_string);
	  $archivePdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");
	  $current_date = date('Y-m-d H:i:s');
	 // die( json_encode(array('status'=>$ability_pdf)));
	  $insert_arr = array(
	  		'lab_id'               =>  $user_id,
	  		'specimen_id'          =>  $specimen_results['id'],
	  		'clinical_history'     =>  $clinical_history,
	  		'diagnostic_short_code'=>  $diagnostic_short_code,
	  		'addendum'             =>  implode(",",$addendum_code),
	      'addendum_desc'        =>  $ser_value,
	  		'gross_description'    =>  $gross_description,
	      'total_description'    =>  $GrossDescription['total_desc'],
	  		'stains'               =>  join(",",$Stain_shortcodes),
	  		'images'               =>  $filename_new,
	  		'dor'                  =>  $current_date,
	  		'nail_pdf'             =>  $ability_pdf,
	  		'signature_id'         =>  $labdoc,
	  		'create_date'          =>  $current_date,
	  		'modify_date'          =>  $current_date
	  	);
	  $insert_nail_fungal = $this->db->insert('wp_abd_nail_pathology_report',$insert_arr);

	  	die( json_encode(array('status'=>'1')));
		 }
	}
	
	}

	/**
	* 
	* Add Report End
	*/
	function get_stains_desc($info){
		 $result =  $this->db->query("select `text` from `wp_abd_nail_macro_codes` where sc = '".$info."'" )->row_array();  
		 return $result;
	}

	function get_pending_report_details(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$sid= $data['id'];
		$addendum_val = array();
		$pending_report_sql = "SELECT * FROM `wp_abd_nail_pathology_report` WHERE `nail_funagl_id` = '".$sid."'";
		$pending_report_view_details = $this->BlankModel->customquery($pending_report_sql);	
		$pending_report_view_details[0]['dor'] 	= date('m/d/Y', strtotime($pending_report_view_details[0]['dor']));
		$pending_report_view_details[0]['addendum'] 	= explode(',',$pending_report_view_details[0]['addendum']);
		if($pending_report_view_details[0]['modify_date'] < '2018-06-12 00:00:00') 
		{
			$addendum_val = $pending_report_view_details[0]['addendum'];
			$old_date = true;
			$new_date = "";
		}
		else
		{
				 
			$addendum_ser_val = unserialize($pending_report_view_details[0]['addendum_desc']);
			if(!empty($addendum_ser_val[0]))
			{
				$addendum_val = $addendum_ser_val;
				$new_date = true;
				$old_date = "";
			}
			else if(!empty($pending_report_view_details[0]['addendum']))
			{ 
				$new_date = true;
				$old_date = "";
				$nail_addendum = $pending_report_view_details[0]['addendum'];
				$addendum_codes = explode(",",$nail_addendum);
				foreach($addendum_codes as $addendum_code)
				{	
					$addendum_desc = $this->get_stains_desc($addendum_code);
					$text_info =  array( 'text' => $addendum_desc);
					array_push($addendum_val, $text_info);
				}
			}
		}
		$clinical_information_sql = "SELECT clinical_specimen FROM `wp_abd_clinical_info` WHERE specimen_id = '".$pending_report_view_details[0]['specimen_id']."'";
	 	$clinical_information = $this->BlankModel->customquery($clinical_information_sql);		
	    $clinical_specimen_fst = $clinical_information[0]['clinical_specimen'];
	     if($clinical_specimen_fst){
		  $clinical_specimen_fst = rtrim($clinical_specimen_fst, ',');	       
	      $clinical_specimen_det = str_replace(',', '  ',$clinical_specimen_fst);
	    }	   
	    
	     $specimen_sql = "SELECT * FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id = '".$pending_report_view_details[0]['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
		 $specimen_information_result =  $this->BlankModel->customquery($specimen_sql);	
	  
	     $specimen_information['timestamp']       =  $specimen_information_result[0]['date_received'];
	     $specimen_information['physician_id']    =  $specimen_information_result[0]['physician_id'];
	     $specimen_information['accessioning_num']=  $specimen_information_result[0]['assessioning_num'];
	     $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 $specimen_information['patient_name']  =  $specimen_information_result[0]['p_firstname']." ".$specimen_information_result[0]['p_lastname']; 
		 $specimen_information['patient_phn']  =  $specimen_information_result[0]['patient_phn']; 
		 $specimen_information['patient_dob']  =  $specimen_information_result[0]['patient_dob']; 
		 $specimen_information['collection_date']  =  $specimen_information_result[0]['collection_date']; 
		 $specimen_information['date_received']  =  $specimen_information_result[0]['date_received'];
		 $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 
		 $fname    = get_user_meta_value($specimen_information_result[0]['physician_id'], 'first_name');				
		 $lname    = get_user_meta_value($specimen_information_result[0]['physician_id'], 'last_name');
		 $mob      = get_user_meta_value($specimen_information_result[0]['physician_id'], '_mobile');
		 $add      = get_user_meta_value($specimen_information_result[0]['physician_id'], '_address');
		 $fax      = get_user_meta_value($specimen_information_result[0]['physician_id'], 'fax');


		$physicain_info= array('name'=> $fname." ".$lname, 'mob'=> $mob, 'add'=> $add, 'fax'=>$fax);

	     $specimen_stage_details_sql = "SELECT `addtional_desc`, `comments` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$pending_report_view_details[0]['specimen_id']."' AND`stage_id` = '2' ";
	    
	     $specimen_stage_details = $this->BlankModel->customquery($specimen_stage_details_sql);	     
	     $gross_description = $specimen_stage_details[0]['addtional_desc'];

	     $diagnostic_short_code_sql = "SELECT `color`, `diagnosis`,`text`, `sc` FROM `wp_abd_nail_macro_codes` where `sc` = '".$pending_report_view_details[0]['diagnostic_short_code']."'";
		 $diagnosis = $this->BlankModel->customquery($diagnostic_short_code_sql);	
		 
		 if(!empty($pending_report_view_details[0]['stains']))
		 {
			$stains = explode(",",$pending_report_view_details[0]['stains']);
			if(!empty($stains[0]))
			{
			  $stv1= $stains[0];
			}
			else
			{
			   $stv1= "";
			}
			if(!empty($stains[1]))
			{
			  $stv2= $stains[1];
			}
			else
			{
				$stv2= "";
			}
			if(!empty($stains[2]))
			{
			  $stv3= $stains[2];
			}
			else
			{
				$stv3= "";
			}
			if(!empty($stains[0]))
			{
			   $Stains_desc1 = $this->get_stains_desc($stains[0]);
			}
			else
			{
			   $Stains_desc1 = "";
			}
			if(!empty($stains[1]))
			{
			   $Stains_desc2 = $this->get_stains_desc($stains[1]);
			}
			else
			{
			   $Stains_desc2 = "";
			}
			if(!empty($stains[2]))
			{
			   $Stains_desc3 = $this->get_stains_desc($stains[2]);
			}
			else
			{
			   $Stains_desc3 = "" ;
			}
		 }
		
		if($pending_report_view_details)
		{
			 die( json_encode(array("status" =>'1', "pending_report_view_details" => $pending_report_view_details[0], 
			 "clinician_location" => $clinical_specimen_det, "specimen_results" => $specimen_information, 
			 "gross_description" => $gross_description, "diagnostic_short_code" => $diagnosis[0],'physician_info'=>$physicain_info,
			 "st1val"=>$stv1, "st2val"=>$stv2, "st3val"=>$stv3,"st1"=>$Stains_desc1['text'],"st2"=>$Stains_desc2['text'],"st3"=>$Stains_desc3['text'],"addendum_val"=>$addendum_val,
			"old_date"=>$old_date,"new_date"=>$new_date)));	
	    }
		else
		{
			 die( json_encode(array('status' =>'0')));		 
		}
		}
	}

    /**
	* Edit Pending Report 	
	*/
	function edit_report_data(){
	 	if($this->input->post()){
		
		 	$addendum_code = array();
		 	$addendum_text = array();
		 	$name = $this->input->post('diagnostic_text');
		 	$id = $this->input->post('id');
		 	$addendum = json_decode($this->input->post('addendum'));
		 	$clinical_history = $this->input->post('clinical_history');
		 	$diagnostic_color = $this->input->post('diagnostic_color');
		 	$diagnostic_short_code = $this->input->post('diagnostic_short_code');
		 	$old_addendum_text = $this->input->post('old_addendum_text');
		 	$diagnostic_text = $this->input->post('diagnostic_text');
		 	$gross_description = $this->input->post('gross_description');
		 	$gross_micro_desc_text = $this->input->post('gross_micro_desc_text');
		 	$stains_addendum_first = $this->input->post('stains_addendum_first');
		 	$stains_addendum_sec = $this->input->post('stains_addendum_sec');
		 	$stains_addendum_third = $this->input->post('stains_addendum_third');
		 	$stains_first = $this->input->post('stains_first');
		 	$stains_sec = $this->input->post('stains_sec');
		 	$stains_third = $this->input->post('stains_third');
			$user_id = $this->input->post('user_id');
			$labdoc = $this->input->post('labdoc');
		 	
			$specimen_results  		= $this->BlankModel->getReportDetails($id);
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);
			$clinical_information   = $this->clinical_function_html($specimen_results['specimen_id']);
			$specimen_stage_details = "SELECT `addtional_desc`, `comments`, `total_desc` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id`= '".$specimen_results['specimen_id']."' AND`stage_id` = '2'";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();

		  	if(!empty($addendum)){
		 		foreach ($addendum as $value) {
			 		array_push($addendum_code, $value->addendum_code);
			 		array_push($addendum_text, $value->addendum_text);
			 	}
		 	}

		 	if(!empty($old_addendum_text)){
		 		array_push($addendum_text, $old_addendum_text);
		 	}
		 	
		 	$ser_value = serialize($addendum_text);

		 	if($_FILES && $_FILES['file_data']['name']){
				unlink('assets/uploads/nail_fungal/'.$specimen_results['images']); 
				$config['upload_path'] = 'assets/uploads/nail_fungal';
				$config['allowed_types'] = 'jpeg|jpg|png';
				$config['max_size'] = 5000000;
				$new_name = time().'_'.$_FILES["file_data"]['name'];
				$config['file_name'] = $new_name;

				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('file_data')){
					die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
				}else{
					$uploadData = $this->upload->data();
					$fileName = $uploadData['file_name'];
				}
			}
			else
			{
				$fileName = $specimen_results['images'];
			}
		
		  if($diagnostic_color == "green"){
		  	$diagnostic_color = "#96faa2";
		  }
		  elseif($diagnostic_color == "red"){
		  	$diagnostic_color = "#e53131";
		  }
		  else{
		  	$diagnostic_color = "#f1ec2d";
		  }
		  $filename_new = $fileName;
		
		  $array_val = count(array_filter($addendum_text));
		  if(0 < $array_val)
		  {
		  $addendum_value = unserialize($ser_value); 	
		  $addendum_pdf_content = "";
		  $addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
		  
		  foreach($addendum_value as $value)
		  {
		  $addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
		  }
		  $addendum_pdf_content .="<div style='clear:both;'></div></div>"; 
		  }
	  
	 
		  if($filename_new){
		  $report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <label>&nbsp;</label></div>
		  <div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$filename_new." alt='No Image'/>
		  </div>
		  <div style='clear:both;'></div></div>";
		  }
		
		  $Stain_shortcodes = array();
		  array_push($Stain_shortcodes, $stains_first);
		  array_push($Stain_shortcodes, $stains_sec);
		  array_push($Stain_shortcodes, $stains_third);
		  $Stains_desc = $this->get_stains_desc($stains_first); 
		  $Stains_desc_one = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_sec); 
		  $Stains_desc_sec = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_third); 
		  $Stains_desc_thr = $Stains_desc['text'];
		  $stains_comments = "";
	  
	  for($num = 0; $num < 3; $num++){
	  	$Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$Stain_shortcodes[$num]."'";
	  	$Stain_shortcode = $this->db->query($Stain_short)->row_array();
	  	$stains_comments.= $Stain_shortcode['comments'];
	  	$stains_comments.=  '<div style= "margin:2px;"></div>';
	  }
	  $nail_macro_sql = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '" . $diagnostic_short_code . "'";
	  $nail_macro     = $this->db->query($nail_macro_sql)->row_array();
	  $comments = nl2br($nail_macro['comments']);
	  
	  $physician_id = "'".$specimen_results['physician_id']."'";
	  
	  $assigned_partner_meta =  get_user_meta_value($physician_id, '_assign_partner',true);
	  
	  $assigned_partner = $assigned_partner_meta;
	  
if(!empty($specimen_results['partners_company'])){
		$prtnr_id = $specimen_results['partners_company'];
		$spe_id = $specimen_results['id'];
		$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();

		$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
		$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();

		$logo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
		$pdfHeaderfile = $this->load->view('nail_fungal_nextgen','',true);
		
	 	}else if(!empty($assigned_partner)){
	  	$pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
		$logo    = get_user_meta_value($assigned_partner,'company_logo',true);
		$partner_address     = get_user_meta_value($assigned_partner, '_address',true);
		$partner_phn         = get_user_meta_value($assigned_partner, '_mobile', true);
		$partner_fax         = get_user_meta_value($assigned_partner, 'fax',     true);
		$partner_company     = get_user_meta_value($assigned_partner, 'first_name',true);

	  }
	  else{
	  	$pdfHeaderfile = $this->load->view('nail_fungal','',true);
		$logo = base_url()."assets/frontend/images/logo.png";
	  }
	  

		if($labdoc == '75770')
			{
				$logoddd_img = base_url()."assets/frontend/images/sign_img_jared.png";
				$lab_doc_desc = "Jared Szymanski, DO, FCAP";
			}
			else
			{
				$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
				$lab_doc_desc = "David R. Bolick, MD, FCAP";
				
			}

			if(!empty($specimen_results['partners_company'])){
				$name = $partners_specimen_data['physician_name'];
				$splite_name = $this->split_name($name);
		
					$fname = $splite_name[0];
					$lname = $splite_name[1];
					$add = $partners_specimen_data['physician_address'];
					$mob = $partners_specimen_data['physician_phone'];
					$fax = '';
			}else{
			$fname = get_user_meta_value($physician_id, 'first_name', true);
			$lname = get_user_meta_value($physician_id, 'last_name', true );
			$name  = $fname.' '.$lname;
			$add   = get_user_meta_value($physician_id, '_address', true);
			$mob   = get_user_meta_value($physician_id, '_mobile', true);
			$fax   = get_user_meta_value($physician_id, 'fax', true);
			}

	  $pdfTotalBody   = $pdfHeaderfile;
	  $replace_in_pdf = array(
	  	"{logo}" 			   => $logo,
	  	"{date}" 			   => "___",
	  	"{report_name}"        => "Preliminary Report",
	  	"{patient_name}"	   => $specimen_results['p_firstname']." ".$specimen_results['p_lastname'],
	  	"{patient_phone}"	   => $specimen_results['patient_phn'],
	  	"{patient_dob}"		   => $specimen_results['patient_dob'],
	  	"{patient_col}"		   => $specimen_results['collection_date'],
	  	"{assessioning_num}"   => $specimen_results['assessioning_num'],
	  	"{patient_recive}"	   => date('m/d/Y', strtotime($datetime['0'])),
	  	
		"{physician_name}"	   => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_name']:$name,
	  	"{physician_addresss}" => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_phone']:$add,
	  	"{physician_mobile}"   => (!empty($specimen_results['partners_company']))?$partners_specimen_data['physician_address']:$mob,
	  	"{physician_fax}"	   => (!empty($specimen_results['partners_company']))?'':$fax,			
	
	  	"{clinical_history}"   => $clinical_history,
	  	"{site_indicator}"     => $specimen_results['site_indicator'],
	  	"{clinicial_info}"	   => $clinical_information,
	  	"{diagnostic}"		   => $diagnostic_text,
	  	"{Stains_desc_fst}"	   => nl2br($Stains_desc_one),
	  	"{Stains_desc_sec}"	   => nl2br($Stains_desc_sec),
	  	"{Stains_desc_thr}"    => nl2br($Stains_desc_thr),
	  	"{addendum}"		   => $addendum_pdf_content,
	  	"{gross_desc}"		   => $gross_description,
	  	"{nail_fungal_img}"	   => $report_img,
	  	"{lab_doc_desc}"	   => "",
	  	"{sign_img}"		   => "",
	  	"{diagnostic_color}"   => $diagnostic_color,
	  	"{gross_micro_description}"=> $gross_micro_desc_text,
	  	"{stains_comments}"	   => nl2br($stains_comments),
	  	"{comments}"		   => nl2br($comments),
	  	"{time}"			   => "___",
	  	"{fax}"                => $pfax,
	  	"{address}"            => nl2br($paddress),
	  	"{phone}"              => $pph,
	  	"{partner_company}"    => $pcompany,
	  	"{partner_footer_data}"=> "",
	  	"{assigned_sign_img}"  => "",
	  	"{report_generate_datetime}" => "",

	  );
if(!empty($specimen_results['partners_company'])){
	  	$replace_in_pdf = array_merge($replace_in_pdf,array(
	  		"{partner_name}"	=>$partners_company_data['partner_name'],
			"{address_line1}"	=>$partners_company_data['partner_address1'],
			"{address_line2}"	=>$partners_company_data['partner_address2'],
			"{partner_phone}"	=>$partners_company_data['phone_no'],
			"{partner_fax}"		=>$partners_company_data['fax_no'],
			"{partner_cli}"		=>$partners_company_data['clia_no'],
			"{partner_lab_doc}"	=>str_ireplace('<p>','',$partners_company_data['lab_director']),
			"{patient_id}"		=>$partners_specimen_data['external_id'],
	  	));
	  }
	  //die( json_encode(array('status'=>'test')));
	  foreach($replace_in_pdf as $find => $replace){
	  $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
	  }

	  $archivePdf = new mPDF('','A4','','', 1, 1, 0, 0, 0, 0); 
	  $archivePdf->SetWatermarkText('PRELIMINARY');
	  $archivePdf->showWatermarkText = true;
	  $archivePdf->WriteHTML($pdfTotalBody,2);
	  $accessioning_num = $specimen_results['assessioning_num'];
	  
	  $p_Name = preg_replace('/\s+/', '_', $specimen_results['p_lastname']);
		if(!empty($specimen_results['partners_company'])){
		  	$l_name = $lname;
		  	$ability_pdf_string = $p_Name.'_'.$l_name.'_'.$accessioning_num.".pdf";
		}else{
			$l_name = get_user_meta_value($physician_id,'last_name',true );
		  $ability_pdf_string = $p_Name.'_'.$l_name.'_'.$accessioning_num.".pdf";
		}
	  $ability_pdf = str_replace(' ', '', $ability_pdf_string);
	  $archivePdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");
	  $current_date = date('Y-m-d H:i:s');

	 
	  		$insert_arr['lab_id'] =  $user_id;
	  		$insert_arr['clinical_history'] =  $clinical_history;
	  		$insert_arr['diagnostic_short_code']= $diagnostic_short_code;
	  		$insert_arr['addendum'] =  implode(",",$addendum_code);
	      	$insert_arr['addendum_desc']  =  $ser_value;
	  		$insert_arr['gross_description'] =  $gross_description;
	      	$insert_arr['total_description'] =  $GrossDescription['total_desc'];
	  		$insert_arr['stains'] =  join(",",$Stain_shortcodes);
	  		if(!empty($filename_new)){
	  			$insert_arr['images'] =  $filename_new;
	  		}

	  		$insert_arr['nail_pdf']  =  $ability_pdf;
	  		$insert_arr['signature_id'] =  $labdoc;
	  		$insert_arr['modify_date'] =  $current_date;

	  $this->db->where('nail_funagl_id',$id);
	  $insert_nail_fungal = $this->db->update('wp_abd_nail_pathology_report',$insert_arr);

	  if($this->db->affected_rows() > 0){
	  	die( json_encode(array('status'=>'1')));
	  }else{
	  	die( json_encode(array('status'=>'0')));
	  }
	  	
		 }else{
		 	die( json_encode(array('status'=>'0')));
		 }
	
	}

    /**
	* Edit Pending Report End
	*/
	function split_name($name) {
	    $name = trim($name);
	    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
	    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
	    return array($first_name, $last_name);
	}
		
 }
