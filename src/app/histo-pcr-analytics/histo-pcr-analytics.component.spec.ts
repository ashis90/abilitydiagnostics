import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoPcrAnalyticsComponent } from './histo-pcr-analytics.component';

describe('HistoPcrAnalyticsComponent', () => {
  let component: HistoPcrAnalyticsComponent;
  let fixture: ComponentFixture<HistoPcrAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoPcrAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoPcrAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
