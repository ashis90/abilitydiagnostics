<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');
class Pcr extends MY_Controller {

    function __construct() {
        parent::__construct();
       $this->load->library('m_pdf');
	   $this->load->library('efax');
	   date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {
	  
    }
   /**
   * /
   * 
   * @List Delivery-to-extraction
   */   
	
    function delivery_to_extraction(){		
	$data = json_decode(file_get_contents('php://input'), true);
	
	if(!empty($data))
	{
	$start = $data['load'];
			
    $merged_specimen_results = array();
    $physicians_data = array();
    $physicians_det_new = array();
    $assgn_arr = array();
    $import_arr = array();
	$specimen_det ="SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`test_type`
	FROM `wp_abd_specimen`
	INNER JOIN `wp_abd_clinical_info`
	ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id`
	WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%7%')
	AND `wp_abd_specimen`.`status` = '0'
	AND `wp_abd_specimen`.`qc_check` = '0'
	AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' AND `wp_abd_specimen`.`assessioning_num` NOT IN(SELECT DISTINCT accessioning_num FROM `wp_abd_import_data` WHERE `wp_abd_import_data`.`report_pdf_name` <>'') 
	ORDER BY `wp_abd_specimen`.`id` DESC LIMIT 0,10";
	
	$specimen_results = $this->BlankModel->customquery($specimen_det);

    /**
	* 
	* @var Refelx Code
	* 
	*/

    /*
    **
    *****@@@@ Not Required Now. Open it when Refelx System is open.
    **
    */

	/*$sql_query = "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`test_type`
	FROM `wp_abd_specimen`
	JOIN `wp_abd_clinical_info`
	ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
	JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
	WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%6%')
	AND `wp_abd_specimen`.`status` = '0'
	AND `wp_abd_specimen`.`qc_check` = '0'
	AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
	AND (`wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '665%' OR `wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '668%')";

	$new_specimen_results = $this->BlankModel->customquery($sql_query);
	
	*/

	/*$merged_specimen_results = array_merge($specimen_results, $new_specimen_results);*/

    $merged_specimen_results = 	$specimen_results;
	$conditions2 = " (`stage_id` = '1' AND `status` = 'Complete')";
	$select_fields = 'specimen_id';
	$is_multy_result = 0;
	$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions2, $select_fields, $is_multy_result);

	if(!empty($pcr_stage_det)){
		for($n=0;$n<count($pcr_stage_det);$n++){				
			array_push($assgn_arr, $pcr_stage_det[$n]['specimen_id']);
		}
	}	

	$this->db->select('accessioning_num');
	$this->db->order_by('id','DESC');
	$specimen_import_det  = $this->db->get('import_data')->result_array();	

	for($i= 0; $i< count($merged_specimen_results);$i++){
		
		if($merged_specimen_results[$i]['test_type'] != 'W'){
			if(in_array($merged_specimen_results[$i]['id'],$assgn_arr))
			{
				$assign_button = 'yes';
			}
			else
			{
				$assign_button = 'Previous stage is not complete';
			}
		}
		else{
			$sp_id = $merged_specimen_results[$i]['id'];
			$this->db->where('specimen_id',$sp_id);
			$stg= $this->db->get('wp_abd_assign_stage');
			if($stg->num_rows() > 0){
				$assign_button = 'yes';
			}
			else{
				$insert_assign_stage = $this->db->insert('wp_abd_assign_stage', 
				array( 
					'lab_tech' => $data['user_id'], 
					'specimen_id' => $sp_id, 
					'create_date'=> date("Y-m-d, H:i:s"),
					'modify_date'=> date("Y-m-d, H:i:s")
				));
				$assign_button = 'yes';
			}
		}

		if(!empty($specimen_import_det)){
			for($m=0;$m<count($specimen_import_det);$m++){
				array_push($import_arr, $specimen_import_det[$m]['accessioning_num']);
			}
		}

		if($merged_specimen_results[$i]['test_type'] != 'W'){
			if(in_array($merged_specimen_results[$i]['assessioning_num'], $import_arr))
			{
				$upload_data = "Process done";
			}
			else
			{
				$upload_data = "Process not done";
			}
		}else{
			$upload_data = "Process done";
		}	


/*		$physician_first_name = get_user_meta_value( $merged_specimen_results[$i]['physician_id'], 'first_name', TRUE );
		$physician_last_name  = get_user_meta_value( $merged_specimen_results[$i]['physician_id'], 'last_name', TRUE);
		
		$physician_name = $physician_first_name.' '.$physician_last_name;
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;*/
		
		
		
		$merged_specimen_results[$i]['assign_button'] = $assign_button;
		$merged_specimen_results[$i]['upload_data'] = $upload_data;

		$this->db->order_by('id','DESC');
		$assign_data  = $this->db->get('wp_abd_assign_stage')->result_array();

		if(!empty($assign_data)){
			for($k=0;$k<count($assign_data);$k++){
				if($merged_specimen_results[$i]['id'] == $assign_data[$k]['specimen_id']){

					$merged_specimen_results[$i]['assign_status'] = $assign_data[$k]['status'];
				}
				
			}			
		}
	}
	
	/**
	* 
	* @var Physician Details
	* 
	*/
 
	     $physician_details = get_users_by_role_n_status($role = 'physician', $select_data = 'ID', $status = 'Active');	 
	  if($physician_details){ 	
	    foreach($physician_details as $key => $physician){
	   	 $physician_first_name = get_user_meta_value( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta_value( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det_new['id'] = $physician['ID'];	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	  }
	  

/*	$this->db->where('type_status','Active');
	$test_types = $this->db->get('wp_abd_test_types')->result_array();
	
	, 'test_types' => $test_types */ 
   
     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results, 'physicians_data' => $physicians_data, 'specimen_count' => count($merged_specimen_results) )));
		 }		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }      
		}
	}


	/**
	* 
	* 
	* @ Search PCR Stages Data
	*/
	
	function search_delivery_to_extraction(){		
	$data = json_decode(file_get_contents('php://input'), true);
	
	if(!empty($data))
	{
		$merged_specimen_results = array();
	    $physicians_data = array();
	    $physicians_det_new = array();
	    $assgn_arr = array();
	    $import_arr = array();
	    $search = "";
	     if($data['physician'] != "" ){
			$search=" AND `wp_abd_specimen`.`physician_id`='".$data['physician']."'";	
		 }
		 if($data['barcode'] != ""  ){
			$search.= " AND (`wp_abd_specimen`.`barcode_number` LIKE '%".$data['barcode']."%' OR `wp_abd_specimen`.`assessioning_num` LIKE '%".$data['barcode']."%')";
		 }
		 if($data['assessioning_num'] != "" ){
			$search.= " AND (`wp_abd_specimen`.`assessioning_num` LIKE '%".$data['assessioning_num']."%')";	
		 }
		 if($data['test_type'] != "" ){
			$search.= " AND `wp_abd_specimen`.`test_type` ='".$data['test_type']."'";	
		 }
		 if($data['fromdate'] != "" && $data['todate'] != "" ){
			$search.= " AND (`wp_abd_specimen`.`accessioning_cur_date` BETWEEN '".date("Y-m-d",strtotime($data['fromdate']))." 00:00:01' AND '".date("Y-m-d",strtotime($data['todate']))." 23:59:59')";	
		 }
			
	        $specimen_det ="
			SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`test_type`
			FROM `wp_abd_specimen`
			INNER JOIN `wp_abd_clinical_info`
			ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
			WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%7%')
			AND `wp_abd_specimen`.`status` = '0'
			AND `wp_abd_specimen`.`qc_check` = '0'
			AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
			".$search." AND `wp_abd_specimen`.`assessioning_num` 
			NOT IN(SELECT DISTINCT `accessioning_num` FROM `wp_abd_import_data` WHERE `wp_abd_import_data`.`report_generate` = 'yes') 
			ORDER BY `wp_abd_specimen`.`id` DESC";

			$specimen_results = $this->BlankModel->customquery($specimen_det);

			/**
			* 
			* @var Refelx Code
			* 
			*/

		    /*
		    **
		    *****@@@@ Not Required Now. Open it when Refelx System is open.
		    **
		    */
		    /*
			$sql_query = "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`qc_check`
			FROM `wp_abd_specimen`
			JOIN `wp_abd_clinical_info`
			ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
			JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
			WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%6%')
			AND `wp_abd_specimen`.`status` = '0'
			AND `wp_abd_specimen`.`qc_check` = '0'
			".$search." 
			AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
			AND (`wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '665%' OR `wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '668%')";
			
			$new_specimen_results = $this->BlankModel->customquery($sql_query);*/			
			/*$merged_specimen_results = array_merge($specimen_results, $new_specimen_results);*/
			
			$merged_specimen_results = $specimen_results;
			
			$conditions2 = " (`stage_id` = '1' AND `status` = 'Complete')";
			$select_fields = 'specimen_id';
			$is_multy_result = 0;
			$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions2, $select_fields, $is_multy_result);

			if(!empty($pcr_stage_det)){
				for($n = 0; $n<count($pcr_stage_det); $n++){						
					array_push($assgn_arr, $pcr_stage_det[$n]['specimen_id']);				
					}
			}

			/**
			* 
			* @var wp_abd_import_data Query
			* 
			*/	
			

            $this->db->select('accessioning_num');             
			$this->db->order_by('id', 'DESC');
			$specimen_import_det = $this->db->get('wp_abd_import_data')->result_array();

			/**
			* 
			* @var wp_abd_assign_stage Query
			* 
			*/

			$this->db->order_by('id','DESC');
			$assign_data = $this->db->get('wp_abd_assign_stage')->result_array();
							
			if(!empty($specimen_import_det)){
				for( $m=0; $m<count($specimen_import_det); $m++ ){
				array_push($import_arr, $specimen_import_det[$m]['accessioning_num']);
				}
			}

    	for($i= 0; $i< count($merged_specimen_results); $i++){
		
			if($merged_specimen_results[$i]['test_type'] != 'W'){
				if(in_array($merged_specimen_results[$i]['id'], $assgn_arr))
				{
					$assign_button = 'yes';
				}
				else
				{
					$assign_button = 'Previous stage is not complete';
				}
			}
			else{
				/*$sp_id = $merged_specimen_results[$i]['id'];
				$this->db->where('specimen_id', $sp_id);
				$stg = $this->db->get('wp_abd_assign_stage');
				if($stg->num_rows() > 0){
					$assign_button = 'yes';
				}
				else{
					$insert_assign_stage = $this->db->insert('wp_abd_assign_stage', 
					array( 
						'lab_tech'    => $data['user_id'], 
						'specimen_id' => $sp_id, 
						'create_date' => date("Y-m-d, H:i:s"),
						'modify_date' => date("Y-m-d, H:i:s")
					));
					
				}*/
				
				$assign_button = 'yes';
			}
			
			if($merged_specimen_results[$i]['test_type'] != 'W' ){
				if(in_array($merged_specimen_results[$i]['assessioning_num'], $import_arr)){
					$upload_data = "Process done";
				}
				else{
					$upload_data = "Process not done";
				}
			}
			else{
				    $upload_data = "Process done";
			}	

		/*$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;*/
		
		$merged_specimen_results[$i]['assign_button'] = $assign_button;
		$merged_specimen_results[$i]['upload_data']   = $upload_data;

		if(!empty($assign_data)){
			for( $k=0; $k<count($assign_data); $k++ ){
				if($merged_specimen_results[$i]['id'] == $assign_data[$k]['specimen_id']){
				   $merged_specimen_results[$i]['assign_status'] = $assign_data[$k]['status'];
				}				
			 }			  
		  }
	  
	   }

	     $physician_details = get_users_by_role_n_status($role = 'physician', $select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta_value( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta_value( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		
		 }	
	    }

/*	$this->db->where('type_status','Active');
	$test_types = $this->db->get('wp_abd_test_types')->result_array();*/
   
     if($merged_specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $merged_specimen_results, 'physicians_data' => $physicians_data, 'specimen_count' => count($merged_specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }	
      
		}
	}


 //     function search_delivery_to_extraction()
	// {	
	// 	$search = "";
	// 	$assgn_arr = array();
 //    	$import_arr = array();
	// 	$data = json_decode(file_get_contents('php://input'), true);
	// 	if($data['physician']!=""){
	// 		$search=" AND `wp_abd_specimen`.`physician_id`='".$data['physician']."'";	
	// 	 }
		 
	// 	 if($data['barcode']!=""){
	// 		$search.=" AND (`wp_abd_specimen`.`barcode_number` = '".$data['barcode']."' OR `wp_abd_specimen`.`assessioning_num` ='".$data['barcode']."')";
	// 	 }
	// 	 if($data['assessioning_num']!=""){
	// 		$search.=" AND `wp_abd_specimen`.`assessioning_num` ='".$data['assessioning_num']."'";	
	// 	 }
	// 	 if($data['test_type']!=""){
	// 		$search.=" AND `wp_abd_specimen`.`test_type` ='".$data['test_type']."'";	
	// 	 }
	// 	 if($data['fromdate']!="" && $data['todate']!=""){
	// 		$search.=" AND (`wp_abd_specimen`.`accessioning_cur_date` BETWEEN '".date("Y-m-d",strtotime($data['fromdate']))." 00:00:01' AND '".date("Y-m-d",strtotime($data['todate']))." 23:59:59')";	
	// 	 }
	// 	 $specimen_det ="
	// 		SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`qc_check`
	// 		FROM `wp_abd_specimen`
	// 		INNER JOIN `wp_abd_clinical_info`
	// 		ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
	// 		WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%7%')
	// 		AND `wp_abd_specimen`.`status` = '0'
	// 		AND `wp_abd_specimen`.`qc_check` = '0'
	// 		AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
	// 		".$search." AND `wp_abd_specimen`.`assessioning_num` NOT IN(SELECT DISTINCT accessioning_num FROM `wp_abd_import_data` WHERE `wp_abd_import_data`.`report_pdf_name` <>'') ORDER BY `id` DESC";

	// 		//die( json_encode(array('status'=>$specimen_det)));
			
	// 		$specimen_results = $this->BlankModel->customquery($specimen_det);
	// 		if(empty($specimen_results))
	// 		{
	// 			$sql_query = "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`qc_check`
	// 		FROM `wp_abd_specimen`
	// 		JOIN `wp_abd_clinical_info`
	// 		ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
	// 		JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
	// 		WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%6%')
	// 		AND `wp_abd_specimen`.`status` = '0'
	// 		AND `wp_abd_specimen`.`qc_check` = '0'
	// 		".$search." 
	// 		AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
	// 		AND (`wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '665%' OR `wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '668%')";
			
	// 		$specimen_results = $this->BlankModel->customquery($sql_query);
			
	// 		}

	// 		$conditions2 = " (`stage_id` = '1' AND `status` = 'Complete')";
	// 		$select_fields = 'specimen_id';
	// 		$is_multy_result = 0;
	// 		$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions2, $select_fields, $is_multy_result);

	// 		if(!empty($pcr_stage_det)){
	// 			for($n=0;$n<count($pcr_stage_det);$n++){
						
	// 				array_push($assgn_arr, $pcr_stage_det[$n]['specimen_id']);
	// 			}
	// 		}

	// 		$this->db->order_by('id','DESC');
	// 		$assign_data  = $this->db->get('wp_abd_assign_stage')->result_array();

	// 		$this->db->select('accessioning_num');
	// 		$this->db->order_by('id','DESC');
	// 		$specimen_import_det  = $this->db->get('import_data')->result_array();

	// 		if(!empty($specimen_import_det)){
	// 			for($m=0;$m<count($specimen_import_det);$m++){
	// 				array_push($import_arr, $specimen_import_det[$m]['accessioning_num']);
	// 			}
	// 		}

	// 	for($i= 0; $i< count($specimen_results);$i++){

			
	// 		if(in_array($specimen_results[$i]['id'],$assgn_arr))
	// 		{
	// 			$assign_button = 'yes';
	// 		}
	// 		else
	// 		{
	// 			$assign_button = 'Previous stage is not complete';
	// 		}

	// 		if(in_array($specimen_results[$i]['assessioning_num'], $import_arr))
	// 		{
	// 			$upload_data = "Process done";
	// 		}
	// 		else
	// 		{
	// 			$upload_data = "Process not done";
	// 		}

	// 	$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
	// 	$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
	// 	$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	// 	$physicians_det['physician_name'] = $physician_name;
	// 	$specimen_results[$i]['physician_name'] = $physician_name;
	// 	$specimen_results[$i]['assign_button'] = $assign_button;
	// 	$specimen_results[$i]['upload_data'] = $upload_data;


	// 	if(!empty($assign_data)){
	// 		for($k=0;$k<count($assign_data);$k++){
	// 			if($specimen_results[$i]['id'] == $assign_data[$k]['specimen_id']){

	// 				$specimen_results[$i]['assign_status'] = $assign_data[$k]['status'];
	// 			}
				
	// 		}
			
	// 	}
	// }
	// 	 if($specimen_results){
	// 	 die( json_encode(array('status' =>'1', "specimen_results" =>$specimen_results,'specimen_count'=>count($specimen_results))));
		 
	// 	 }
	// 	 else
	// 	 {
	// 		 die( json_encode(array('status'=>'0')));
	// 	 }
	// } 


	function load_all_delivery_to_extraction(){
		$assgn_arr = array();
    	$import_arr = array();
		 $specimen_det ="
			SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`qc_check`
			FROM `wp_abd_specimen`
			INNER JOIN `wp_abd_clinical_info`
			ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
			WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit` LIKE '%7%')
			AND `wp_abd_specimen`.`status` = '0'
			AND `wp_abd_specimen`.`qc_check` = '0'
			AND `wp_abd_specimen`.`physician_accepct` = '0'
			AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' AND `wp_abd_specimen`.`assessioning_num` NOT IN(SELECT DISTINCT accessioning_num FROM `wp_abd_import_data`) ORDER BY `id` DESC";

			$specimen_results = $this->BlankModel->customquery($specimen_det);

			$sql_query = "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`qc_check`
			FROM `wp_abd_specimen`
			JOIN `wp_abd_clinical_info`
			ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
			JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
			WHERE (`wp_abd_clinical_info`.`nail_unit` LIKE '%6%')
			AND `wp_abd_specimen`.`status` = '0'
			AND `wp_abd_specimen`.`qc_check` = '0'
			AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
			AND (`wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '665%' OR `wp_abd_nail_pathology_report`.`diagnostic_short_code` LIKE '668%')";

			$new_specimen_results = $this->BlankModel->customquery($sql_query);

			$merged_specimen_results = array_merge($specimen_results,$new_specimen_results);

			$conditions2 = " (`stage_id` = '1' AND `status` = 'Complete')";
			$select_fields = 'specimen_id';
			$is_multy_result = 0;
			$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions2, $select_fields, $is_multy_result);

			if(!empty($pcr_stage_det)){
				for($n=0;$n<count($pcr_stage_det);$n++){
						
					array_push($assgn_arr, $pcr_stage_det[$n]['specimen_id']);
				}
			}
			$this->db->order_by('id','DESC');
			$assign_data  = $this->db->get('wp_abd_assign_stage')->result_array();

			$this->db->select('accessioning_num');
			$this->db->order_by('id','DESC');
			$specimen_import_det  = $this->db->get('wp_abd_import_data')->result_array();

			if(!empty($specimen_import_det)){
				for($m=0;$m<count($specimen_import_det);$m++){
					array_push($import_arr, $specimen_import_det[$m]['accessioning_num']);
				}
			}

	for($i= 0; $i< count($merged_specimen_results);$i++){

		
		if(in_array($merged_specimen_results[$i]['id'],$assgn_arr))
		{
			$assign_button = 'yes';
		}
		else
		{
			$assign_button = 'Previous stage is not complete';
		}

		if(in_array($merged_specimen_results[$i]['assessioning_num'], $import_arr))
		{
			$upload_data = "Process done";
		}
		else
		{
			$upload_data = "Process not done";
		}
		$physician_first_name = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $merged_specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['physician_name'] = $physician_name;
		$merged_specimen_results[$i]['assign_button'] = $assign_button;
		$merged_specimen_results[$i]['upload_data'] = $upload_data;


		if(!empty($assign_data)){
			for($k=0;$k<count($assign_data);$k++){
				if($merged_specimen_results[$i]['id'] == $assign_data[$k]['specimen_id']){

					$merged_specimen_results[$i]['assign_status'] = $assign_data[$k]['status'];
				}
				
			}
			
		}
		

		
	}
		 if($specimen_results){
		 die( json_encode(array('status' =>'1', "specimen_results" =>$merged_specimen_results,'specimen_count'=>count($merged_specimen_results))));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
	}


	function delivery_to_pcr(){
    $physicians_data = array();
    $physicians_det_new = array();
	$specimen_sql ="SELECT `id`, `assessioning_num`,`physician_id`,`qc_check` FROM `wp_abd_specimen` WHERE status = '0'  AND `qc_check`  = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` where `pass_fail` = '1' AND `specimen_id` NOT IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2' AND `stage_id` != '3'  GROUP BY `specimen_id`)) ORDER BY `id` DESC LIMIT 10";

	$specimen_results = $this->BlankModel->customquery($specimen_sql);

	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}

	function search_delivery_to_pcr(){
    $physicians_data = array();
    $physicians_det_new = array();
    $search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `wp_abd_specimen`.`physician_id`='".$data['physician']."'";	
		 }
		 
		 if($data['barcode']!=""){
			$search.=" AND (`wp_abd_specimen`.`barcode_number` = '".$data['barcode']."' OR `wp_abd_specimen`.`assessioning_num` ='".$data['barcode']."')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `wp_abd_specimen`.`assessioning_num` ='".$data['assessioning_num']."'";	
		 }
	$specimen_sql ="SELECT `id`, `assessioning_num`,`physician_id`,`qc_check` FROM `wp_abd_specimen` WHERE status = '0'  AND `qc_check`  = '0' ".$search." AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` where `pass_fail` = '1' AND `specimen_id` NOT IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2' AND `stage_id` != '3'  GROUP BY `specimen_id`)) ORDER BY `id` DESC";

	$specimen_results = $this->BlankModel->customquery($specimen_sql);

	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}


	function load_all_delivery_to_pcr(){
		$physicians_data = array();
    	$physicians_det_new = array();
		$specimen_sql ="SELECT `id`, `assessioning_num`,`physician_id`,`qc_check` FROM `wp_abd_specimen` WHERE status = '0'  AND `qc_check`  = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` where `pass_fail` = '1' AND `specimen_id` NOT IN (SELECT `specimen_id` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2' AND `stage_id` != '3'  GROUP BY `specimen_id`)) ORDER BY `id` DESC";

	$specimen_results = $this->BlankModel->customquery($specimen_sql);

	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}


	function well_assign_stage(){
    $physicians_data = array();
    $physicians_det_new = array();
	$specimen_sql ="SELECT `wp_abd_specimen`.`id`, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`physician_id`,`wp_abd_specimen`.`qc_check`,`wp_abd_assign_stage`.`status`,`wp_abd_assign_stage`.`id` as 'stage_id' FROM `wp_abd_specimen` INNER JOIN `wp_abd_assign_stage` ON `wp_abd_specimen`.`id`= `wp_abd_assign_stage`.`specimen_id` WHERE `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`qc_check`  = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' ORDER BY `wp_abd_specimen`.`id` DESC LIMIT 10";
	$specimen_results = $this->BlankModel->customquery($specimen_sql);
	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}

	function search_well_assign_stage(){
    $physicians_data = array();
    $physicians_det_new = array();
    $search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `wp_abd_specimen`.`physician_id`='".$data['physician']."'";	
		 }
		 
		 if($data['barcode']!=""){
			$search.=" AND (`wp_abd_specimen`.`barcode_number` = '".$data['barcode']."' OR `wp_abd_specimen`.`assessioning_num` ='".$data['barcode']."')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `wp_abd_specimen`.`assessioning_num` ='".$data['assessioning_num']."'";	
		 }
	$specimen_sql ="SELECT `wp_abd_specimen`.`id`, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`physician_id`,`wp_abd_specimen`.`qc_check`,`wp_abd_assign_stage`.`status`,`wp_abd_assign_stage`.`id` as 'stage_id' FROM `wp_abd_specimen` INNER JOIN `wp_abd_assign_stage` ON `wp_abd_specimen`.`id`= `wp_abd_assign_stage`.`specimen_id` WHERE `wp_abd_specimen`.`status` = '0' ".$search." AND `wp_abd_specimen`.`qc_check`  = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' ORDER BY `wp_abd_specimen`.`id` DESC";

	$specimen_results = $this->BlankModel->customquery($specimen_sql);

	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}

	function load_all_well_assign_stage(){
    $physicians_data = array();
    $physicians_det_new = array();
	$specimen_sql ="SELECT `wp_abd_specimen`.`id`, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`physician_id`,`wp_abd_specimen`.`qc_check`,`wp_abd_assign_stage`.`status`,`wp_abd_assign_stage`.`id` as 'stage_id' FROM `wp_abd_specimen` INNER JOIN `wp_abd_assign_stage` ON `wp_abd_specimen`.`id`= `wp_abd_assign_stage`.`specimen_id` WHERE `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`qc_check`  = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' ORDER BY `wp_abd_specimen`.`id` DESC";

	$specimen_results = $this->BlankModel->customquery($specimen_sql);

	for($i= 0; $i< count($specimen_results);$i++){
		$physician_first_name = get_user_meta( $specimen_results[$i]['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen_results[$i]['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$physicians_det['physician_name'] = $physician_name;
		$specimen_results[$i]['physician_name'] = $physician_name;
	}

	 
	     $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	  if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det_new['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det_new['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det_new);
		}
	}

     if($specimen_results){
	   
	     die( json_encode(array( "status" => "1", "specimen_results" => $specimen_results,'physicians_data'=>$physicians_data,'specimen_count'=>count($specimen_results) )));
		 }
		 
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Specimen Found" )));
		 }
	}


	function assign_in_stage(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$this->db->where_in('specimen_id',$data);
			$this->db->update('wp_abd_assign_stage',array('status'=>'Assign'));
			if($this->db->affected_rows() > 0){
				die( json_encode(array( "status" => "1" )));
			}else{
				die( json_encode(array( "status" => "0" )));
			}
		}
			
	}

	function unassign_in_stage(){
		$data = json_decode(file_get_contents('php://input'), true);
		//die( json_encode(array( "status" => $data )));
		if(!empty($data)){
			$this->db->where_in('specimen_id',$data);
			$this->db->update('wp_abd_assign_stage',array('status'=>'Unassign'));
			if($this->db->affected_rows() > 0){
				die( json_encode(array( "status" => "1" )));
			}else{
				die( json_encode(array( "status" => "0" )));
			}
		}
			
	}

	function data_analysis_import_data()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$insertExcel=array(array());
		$lastInsertId = '';
		$uniqueData = array();
		$analyzeData = array(array());
		$testData = $data;
		$len = count($testData);
		for($i=0;$i<$len;$i++){
			if($i===0){
               $j=0;
               $well_val ='';
              foreach ($data[$i] as $key => $value) {
                if($j===1){
                	$well_val = $key;
                }
                $j++;
              }  
              $insertExcel[$i]['well_position'] = $well_val;
            }else{
                $insertExcel[$i]['well_position'] = $data[$i][$well_val]?$data[$i][$well_val]:'';
				$insertExcel[$i]['accessioning_num'] = $data[$i]['__EMPTY']?$data[$i]['__EMPTY']:'';
				$insertExcel[$i]['target_name'] = $data[$i]['__EMPTY_1']?$data[$i]['__EMPTY_1']:'';
				$insertExcel[$i]['rox'] = $data[$i]['__EMPTY_8']?$data[$i]['__EMPTY_8']:'';
				$insertExcel[$i]['ct_sd'] = $data[$i]['__EMPTY_9']?$data[$i]['__EMPTY_9']:'';
				$insertExcel[$i]['amp_score'] = $data[$i]['__EMPTY_10']?$data[$i]['__EMPTY_10']:'';
				$insertExcel[$i]['cq'] = $data[$i]['__EMPTY_11']?$data[$i]['__EMPTY_11']:'';
				$insertExcel[$i]['ntc'] = $data[$i]['__EMPTY_12']?$data[$i]['__EMPTY_12']:'';
				$insertExcel[$i]['filter_out'] = $data[$i]['__EMPTY_13']?$data[$i]['__EMPTY_13']:'';
				$insertExcel[$i]['pass_fail_status'] = $data[$i]['__EMPTY_14']?$data[$i]['__EMPTY_14']:'';
				$insertExcel[$i]['positive_negtaive'] = $data[$i]['__EMPTY_15']?$data[$i]['__EMPTY_15']:'';
            }
		}
		$this->db->query( 'DELETE FROM `wp_abd_import_data_analysis`');
		for($i=0;$i<count($insertExcel);$i++){
			$insData = $insertExcel[$i];
			$this->db->insert('wp_abd_import_data_analysis',$insData);
			$lastInsertId = $this->db->insert_id();
			
		}
		if($lastInsertId){

			$pcr_report_sql = "SELECT * FROM `wp_abd_import_data_analysis` WHERE `accessioning_num` != '' GROUP BY `accessioning_num`,`target_name`";
			$analyze_datas  = $this->BlankModel->customquery($pcr_report_sql);
			$count_data= count($analyze_datas);
			$pcr_report_batch_number_sql =  $this->db->query("SELECT `well_position` FROM `wp_abd_import_data_analysis` ORDER BY `id` ASC LIMIT 1")->row_array();

			$target = "SELECT DISTINCT `target_name`,`accessioning_num` FROM `wp_abd_import_data_analysis`";
						$target_sql  = $this->BlankModel->customquery($target);
						 $batch_sql = "SELECT `well_position` FROM `wp_abd_import_data_analysis` ORDER BY `id` ASC LIMIT 1";
						$batch_data = $this->db->query($batch_sql)->row_array();
						foreach($target_sql as $t)
						{
						$remain_data = "SELECT `well_position`,`pass_fail_status`,`positive_negtaive` FROM `wp_abd_import_data_analysis` where `accessioning_num`= '".$t['accessioning_num']."' AND `target_name`= '".$t['target_name']."'";
						$remain_data_sql  = $this->BlankModel->customquery($remain_data);
						
						 $rox = "SELECT `rox`,`ct_sd`,`amp_score`,`cq` FROM `wp_abd_import_data_analysis` where `accessioning_num`= '".$t['accessioning_num']."' AND `target_name`= '".$t['target_name']."'";
						 $rox_sql  = $this->db->query($rox)->result_array();
						
					 	$search_data_oo = $this->searchForId('no', $rox_sql,'rox');
						$rox_data = ($search_data_oo === 'yes'?"yes":"no");
						$search_data_oo = $this->searchForId('no', $rox_sql,'ct_sd');
						$ct_data = ($search_data_oo === 'yes'?"yes":"no");
						$search_data_oo = $this->searchForId('no', $rox_sql,'amp_score');
						$amp_score_data = ($search_data_oo === 'yes'?"yes":"no");
						$search_data_oo = $this->searchForId('no', $rox_sql,'cq');
						$cq_data = ($search_data_oo === 'yes'?"yes":"no");
					

						$chk_pcr = "SELECT `nail_unit` FROM `wp_abd_clinical_info` where `assessioning_num`= '".$t['accessioning_num']."'";
						$chk_pcr_sql  = $this->db->query($chk_pcr)->row_array();
						$exp_arr = explode(',',$chk_pcr_sql['nail_unit']);
						if(in_array("4", $exp_arr) || in_array("5", $exp_arr) || in_array("7", $exp_arr) || in_array("6", $exp_arr))
						{
						$insert = $this->db->insert('wp_abd_import_data', 
										array( 'accessioning_num' => trim($t['accessioning_num']), 'target_name' => trim($t['target_name']),'rox' => trim($rox_data),'ct_sd' => trim($ct_data),'amp_score' => trim($amp_score_data),'cq' => trim($cq_data), 'well_position' => trim($remain_data_sql[0]['well_position']), 'pass_fail_status' => trim($remain_data_sql[0]['pass_fail_status']), 'positive_negtaive' => trim($remain_data_sql[0]['positive_negtaive']), 'batch_no' => trim($batch_data['well_position'])));

						 }else{
						 	$skip_data[]= $t['accessioning_num'];
							$uniq_data = array_unique($skip_data);
						 }
						}

						if(!empty($uniq_data)) 
						{
							foreach($uniq_data as $value)
							{
								array_push($uniqueData, $value);
							}
						}

					for($i=0;$i<count($analyze_datas);$i++){
						$analyzeData[$i]['id'] = $analyze_datas[$i]['id'];
						$analyzeData[$i]['well_position'] = $analyze_datas[$i]['well_position'];
						$analyzeData[$i]['accessioning_num'] = $analyze_datas[$i]['accessioning_num'];
						$analyzeData[$i]['target_name'] = $analyze_datas[$i]['target_name'];
						$analyzeData[$i]['rox'] = $analyze_datas[$i]['rox'];
						$analyzeData[$i]['ct_sd'] = $analyze_datas[$i]['ct_sd'];
						$analyzeData[$i]['amp_score'] = $analyze_datas[$i]['amp_score'];
						$analyzeData[$i]['cq'] = $analyze_datas[$i]['cq'];
						$analyzeData[$i]['ntc'] = $analyze_datas[$i]['ntc'];
						$analyzeData[$i]['filter_out'] = $analyze_datas[$i]['filter_out'];
						$analyzeData[$i]['pass_fail_status'] = $analyze_datas[$i]['pass_fail_status'];
						$analyzeData[$i]['positive_negtaive'] = $analyze_datas[$i]['positive_negtaive'];
					}

			die( json_encode(array( "status" => '1','analyze_data'=>$analyzeData,'unique_data'=>$uniqueData,'barcode'=>$pcr_report_batch_number_sql,'analyze_data_count'=>$count_data )));
			}
			else{
				die( json_encode(array( "status" => "0" )));
			}
		}

	} 

	function batch_data_review_data(){
		$batchReview = array();
		$distinct_batch_no = "SELECT DISTINCT `batch_no` FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `manual_pass_fail`= '2'";	
		$batch_data = $this->BlankModel->customquery($distinct_batch_no);
		$specimen_count='';
		if(!empty($batch_data)) 
		{
			foreach($batch_data as $data){
				$pass_fail_ntc = "SELECT `pass_fail_status` FROM `wp_abd_import_data` WHERE `batch_no` = '".$data['batch_no']."' AND `accessioning_num`='NTC' AND `manual_pass_fail`= '2'";	
				$nct_data = $this->db->query($pass_fail_ntc)->result_array();
				if(empty($nct_data)){ $nct_data=array();}
				$search_data_ntc = $this->searchForId('no', $nct_data,'pass_fail_status');	

				$pass_fail_ptc = "SELECT `pass_fail_status` FROM `wp_abd_import_data` WHERE `batch_no` = '".$data['batch_no']."' AND `accessioning_num`='PTC' AND `manual_pass_fail`= '2'";	
				$pct_data = $this->db->query($pass_fail_ptc)->result_array();
				if(empty($pct_data)){ $pct_data=array();}
				$search_data_ptc = $this->searchForId('no', $pct_data,'pass_fail_status');	
				
				if($search_data_ntc==='yes' || $search_data_ptc==='yes')
				{
					array_push($batchReview, $data['batch_no']);
				}
			}

			$pcr_report_sql = "SELECT * FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `pass_fail_status` = 'no' AND `manual_pass_fail`= '2' AND `accessioning_num` NOT IN('NTC','PTC') ORDER BY `id` ASC";
			$fail_specimens = $this->db->query($pcr_report_sql)->result_array();
			if(!empty($fail_specimens)){
				$specimen_count = count($fail_specimens);
			}
			if(!empty($fail_specimens)){
				if(!empty($batchReview)){
					die( json_encode(array( "status" => "1",'fail_specimens'=>$fail_specimens,'batch_review'=>$batchReview,'specimen_count'=>$specimen_count )));
				}else{
					die( json_encode(array( "status" => "0" )));
				}
				
				
			}else{
				die( json_encode(array( "status" => "0" )));
			}	


		}else{
			die( json_encode(array( "status" => "0" )));
		}
	}


	function pass_batch_review(){
		$data = json_decode(file_get_contents('php://input'), true);
		//die( json_encode(array( "status" => $data['batch_no'] )));
		if(!empty($data['batch_no']) && !empty($data['user_id'])){
			$this->db->where('batch_no',$data['batch_no']);
			$upd_pass = $this->db->update('wp_abd_import_data',array('pass_fail_status' => 'yes', 'manual_pass_fail' => '1'));
			$affected_rows = $this->db->affected_rows();
			$insert = $this->db->insert('wp_abd_batch_pass_fail_history', 
						array( 'batch_no' => $data['batch_no'], 'action' => 'Pass','created_date' => date('Y-m-d h:i:s'), 'user_id' => $data['user_id'], 'comment' => 'Pass'));
			if($affected_rows > 0){
				die( json_encode(array( "status" => "1" )));
			}else{
				die( json_encode(array( "status" => "0" )));
			}
		}
	}

	function fail_rq_submit(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$fetch_specimen_sql = "SELECT DISTINCT `accessioning_num` FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `batch_no` = '".$data['batch_no']."'";
			$fetch_specimen = $this->db->query($fetch_specimen_sql)->result_array();
			if($data['lists']=='1')
			{
			foreach($fetch_specimen as $key => $value) 
			{
				$fetch_specimen_id = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` = '".$value['accessioning_num']."'";
				$specimen_data = $this->db->query($fetch_specimen_id)->row_array();

				$stage_1_sql = "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` = '".$specimen_data['id']."'";
				$stage_1_data = $this->db->query($stage_1_sql)->result_array();
				$assign_sql = "SELECT * FROM `wp_abd_assign_stage` WHERE `specimen_id` = '".$specimen_data['id']."'";
				$assign_data = $this->db->query($assign_sql)->result_array();
				
				if(!empty($stage_1_data))
				{ 
					$this->db->query("DELETE FROM `wp_abd_pcr_stage_details` WHERE specimen_id = '".$specimen_data['id']."'"); 
				}
				
				if(!empty($assign_data))
				{
					$this->db->query("DELETE FROM `wp_abd_assign_stage` WHERE specimen_id = '".$specimen_data['id']."'");
				}
				
			}
			}
			$this->db->query("DELETE FROM `wp_abd_import_data` WHERE batch_no = '".$data['batch_no']."'");
			$insert = $this->db->insert('wp_abd_batch_pass_fail_history', 
											array( 'batch_no' => $data['batch_no'], 'action' => 'Fail','created_date' => date('Y-m-d H:i:s'), 'user_id' => $data['user_id'], 'comment' => $data['comment']));
			$lastInsertId = $this->db->insert_id();

			if($lastInsertId){
				die( json_encode(array( "status" => "1" )));
			}else{
				die( json_encode(array( "status" => "0" )));
			}
		}else{
			die( json_encode(array( "status" => "0" )));
		}

	}

	function review_process_stage(){
		$data = json_decode(file_get_contents('php://input'), true);
		//die( json_encode(array( "status" => $data )));
		if(!empty($data)){
			$speciman_sql = "SELECT * FROM `wp_abd_import_data` WHERE `id` = '".$data['specimen_id']."'";
			$specimen_ori_id = $this->db->query($speciman_sql)->row_array();
			$speciman = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` = '".$specimen_ori_id['accessioning_num']."'";
			$specimen_id_upt = $this->db->query($speciman)->row_array();
			 
			if($data['report'] == '1'){
				$this->db->where('id',$data['specimen_id']);
				$updt = $this->db->update('wp_abd_import_data',
				array('pass_fail_status' => 'yes', 'manual_pass_fail' => $data['report']));
				die( json_encode(array( "status" => "1" )));	
				}else if($data['report'] == '0')
				{

				if($data['lists']=='0')
				{	
				$sql_insert = $this->db->insert( 'wp_abd_pcr_stage_details', 
				array( 
					'stage_id' => '1', 
					'pass_fail' => '0', 
					'lab_tech' => $data['user_id'], 
					'specimen_id' => $specimen_id_upt['id'], 
					'status' => 'Complete',
					'specimen_timestamp'=> date("Y-m-d, H:i:s")
				));	
				
				}
				if($data['lists']=='1')
				{	
					$stage_1_sql = "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` = '".$specimen_id_upt['id']."'";
					$stage_1_data = $this->db->query($stage_1_sql)->result_array();
					$assign_sql = "SELECT * FROM `wp_abd_assign_stage` WHERE `specimen_id` = '".$specimen_id_upt['id']."'";
					$assign_data = $this->db->query($assign_sql)->result_array();
					
					if(!empty($stage_1_data))
					{ 
						$this->db->query("DELETE FROM `wp_abd_pcr_stage_details` WHERE specimen_id = '".$specimen_id_upt['id']."'"); 
					}
					
					if(!empty($assign_data))
					{
						$this->db->query("DELETE FROM `wp_abd_assign_stage` WHERE specimen_id = '".$specimen_id_upt['id']."'");
					}
					
				}
				if($data['lists']=='2')
				{	
					$stage_2_sql = "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` = '".$specimen_id_upt['id']."' AND stage_id = '2'";
					$stage_2_data = $this->db->query($stage_2_sql)->result_array();
					
					if(!empty($stage_2_data))
					{
						$this->db->query("DELETE FROM `wp_abd_pcr_stage_details` WHERE specimen_id = '".$specimen_id_upt['id']."' AND `stage_id` = '2'");
					}
					$stage_1_sql = "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` = '".$specimen_id_upt['id']."' AND stage_id = '1'";
					$stage_1_data = $this->db->query($stage_1_sql)->result_array();
					if(empty($stage_1_data))
					{
						$sql_insert = $this->db->insert( 'wp_abd_pcr_stage_details', 
						array( 
							'stage_id' => '1', 
							'pass_fail' => '1', 
							'lab_tech' => $data['user_id'], 
							'specimen_id' => $specimen_id_upt['id'], 
							'status' => 'Complete',
							'specimen_timestamp'=> date("Y-m-d, H:i:s")
						));	
					}
					
				}
				if($data['lists']=='3')
				{
					$sql_insert =  $this->db->insert( 'wp_abd_assign_stage', 
					array( 
						'lab_tech' => $data['user_id'], 
						'specimen_id' => $specimen_id_upt['id'], 
						'status' => 'Assign',
						'create_date'=> date("Y-m-d, H:i:s"),
						'modify_date'=> date("Y-m-d, H:i:s")
					));
				}
				
				$check_accessioning_no_exits = $this->db->query("SELECT * FROM `wp_abd_import_data` WHERE `accessioning_num` = '".$data['accessioning_no']."' AND `pass_fail_status` = 'no' AND `manual_pass_fail`= '2'");
				
				if($check_accessioning_no_exits){
					foreach($check_accessioning_no_exits as  $update_accessioning){
						$this->db->where('accessioning_num',$data['accessioning_no']);
						$this->db->where('pass_fail_status','no');
						$updt = $this->db->update('wp_abd_import_data',
				    	array('manual_pass_fail' => '0'));
					}
				}
				else{
					$this->db->where('id',$data['specimen_id']);
					$updt = $this->db->update('wp_abd_import_data', array('manual_pass_fail' => $data['report']));
				    } 

				    die( json_encode(array( "status" => "1" )));
				}else{
				die( json_encode(array( "status" => "0" )));
			}
			}else{
				die( json_encode(array( "status" => "0" )));
			}
	}

	function extraction_process_stage(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){

			$sp_id = $data['specimen_id'];
			$xval = $data['xvalue'];
			$yval = $data['yvalue'];
			$zval = $data['zvalue'];
			$color = $data['color'];
			$pieces = $data['pieces'];
			$tissue = $data['tissue'];
			$cassettes = $data['cassettes']; 
			$pcr_val =  $data['pcr_val'];

			$stage_results = $this->BlankModel->customquery("SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id`='".$sp_id."' AND `stage_id`='1'");
			
			if($pieces==1)
			{
			$word = $pieces." piece";
			$piece_word = "";
			}
			else if($pieces==2 || $pieces==3 || $pieces=="multiple")
			{
			$word = $pieces." pieces";
			$piece_word = " in aggregate";
			}

			// if($cassettes>1)
			// {
			// $pcr_merged_value ="The specimen consists of ".$word." of ".$color." ".$tissue." measuring ".$xval."x".$yval."x".$zval."mm".$piece_word.". The specimen is submitted ".$pcr_val."% in ".$cassettes." cassettes.";
			// }
			// else
			// {
			// $pcr_merged_value ="The specimen consists of ".$word." of ".$color." ".$tissue." measuring ".$xval."x".$yval."x".$zval."mm".$piece_word.". The specimen is submitted ".$pcr_val."% in ".$cassettes." cassette.";
			// }
			$pcr_merged_value ="The specimen consists of ".$word." of ".$color." ".$tissue." measuring ".$xval."x".$yval."x".$zval."mm".$piece_word.". The specimen is submitted ".$pcr_val."%.";	
			$tot_val = $xval."x".$yval."x".$zval."mm ".$color." aggregate ";

			if(!empty($stage_results)){
				
				$this->db->where('specimen_id', $sp_id);
				$this->db->where('stage_id','1');
				$sql_insert = $this->db->update( 'wp_abd_pcr_stage_details', 
				array( 
					'stage_id' => '1', 
					'pass_fail' => $data['pass'], 
					'lab_tech' => $data['user_id'], 
					'addtional_desc' => $pcr_merged_value, 
					'specimen_id' => $sp_id, 
					'status' => 'Complete',
					'specimen_timestamp'=> date("Y-m-d, H:i:s"),
					'total_desc' => $tot_val,
					'pcr' => $pcr_val,
					'pieces' => $pieces
				));

				$this->db->where('specimen_id', $sp_id);
				$assgn_table_data = $this->db->get('wp_abd_assign_stage');

				//die( json_encode(array( "status" => $sp_id)));

				if($assgn_table_data->num_rows() > 0){
					
				}else{
					$insert_assign_stage = $this->db->insert( 'wp_abd_assign_stage', 
					array( 
						'lab_tech' => $data['user_id'], 
						'specimen_id' => $sp_id, 
						'create_date'=> date("Y-m-d, H:i:s"),
						'modify_date'=> date("Y-m-d, H:i:s")
					));
				}

			}
			else
			{
				$sql_insert = $this->db->insert( 'wp_abd_pcr_stage_details', 
				array( 
					'stage_id' => '1', 
					'pass_fail' => $data['pass'], 
					'lab_tech' => $data['user_id'], 
					'addtional_desc' => $pcr_merged_value, 
					'specimen_id' => $sp_id, 
					'status' => 'Complete',
					'specimen_timestamp'=> date("Y-m-d, H:i:s"),
					'total_desc' => $tot_val,
					'pcr' => $pcr_val,
					'pieces' => $pieces
				));

				$insert_assign_stage = $this->db->insert( 'wp_abd_assign_stage', 
				array( 
					'lab_tech' => $data['user_id'], 
					'specimen_id' => $sp_id, 
					'create_date'=> date("Y-m-d, H:i:s"),
					'modify_date'=> date("Y-m-d, H:i:s")
				));


			}
			if($sql_insert || $this->db->affected_rows() > 0){
				die( json_encode(array( "status" => "1")));
			}else{
				die( json_encode(array( "status" => "0")));
			}
		}
	}

	function pcr_process_stage(){
		$data = json_decode(file_get_contents('php://input'), true);
		//die( json_encode(array( "status" => $data )));
		if(!empty($data)){
			$sql_insert = $this->db->insert( 'wp_abd_pcr_stage_details', 
		array( 
			'stage_id' => '2', 
			'pass_fail' => $data['report'], 
			'lab_tech' => $data['user_id'], 
			'addtional_desc' => $data['addtional_desc'], 
			'specimen_id' => $data['specimen_id'], 
			'status' => 'Complete',
			'specimen_timestamp'=> date("Y-m-d, H:i:s")
		));
		$sql_insert_id = $this->db->insert_id();
		if($sql_insert_id && $data['report'] == '1'){
			
		$insert_assign_stage = $this->db->insert( 'wp_abd_assign_stage', 
		array( 
			'lab_tech' => $data['user_id'], 
			'specimen_id' => $data['specimen_id'], 
			'create_date'=> date("Y-m-d, H:i:s"),
			'modify_date'=> date("Y-m-d, H:i:s")
		));
		}		
		$lastInsertId = $this->db->insert_id();
			if($lastInsertId){
				die( json_encode(array( "status" => "1")));
			}else{
				die( json_encode(array( "status" => "0")));
			}
		}
	}

	function batch_history_details(){
		$data = json_decode(file_get_contents('php://input'), true);
			if(!empty($data)){
				$batchData = array(array());
				$batch_record = "SELECT * FROM `wp_abd_batch_pass_fail_history`";	
				$batch_record_data = $this->db->query($batch_record)->result_array();
				$i=0;
				foreach($batch_record_data as $data) 
				{
					
					$fname = get_user_meta($data['user_id'], 'first_name', true);
					$lname = get_user_meta($data['user_id'], 'last_name', true);
					$batchData[$i]['batch_num'] = $data['batch_no'];
					$batchData[$i]['action'] = $data['action'];
					$batchData[$i]['action_by'] = $fname['meta_value']." ".$lname['meta_value'];
					$batchData[$i]['comment'] = $data['comment'];
					$batchData[$i]['date'] = date('Y-m-d',strtotime($data['created_date']));

					$i++;
				}
			if(!empty($batchData)){
				die( json_encode(array( "status" => "1",'batch_history'=>$batchData )));
			}else{
				die( json_encode(array( "status" => "0" )));
			}
		}
	}

	function get_report_generate(){
		$final_sql= "SELECT DISTINCT `accessioning_num` FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `accessioning_num` NOT IN('NTC','PTC') AND `report_generate`='no'";
		$final_data = $this->db->query($final_sql)->result_array();

		if(!empty($final_data)){
			die( json_encode(array( "status" => "1","report_specimen"=>$final_data,'specimen_count'=>count($final_data) )));
		}else{
			die( json_encode(array( "status" => '0' )));
		}
		
	}

	function get_report_generate_by_id(){
		$data = json_decode(file_get_contents('php://input'), true);
		$search = '';
		if(!empty($data)){
			$id = $data['id'];
			$search = "AND `accessioning_num` = '".$id."'";
		}
			$accessioning_num_type = substr(strrchr($id, '-'), 1);
			if($accessioning_num_type != 'W'){
				$final_sql= "SELECT DISTINCT `accessioning_num` FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `accessioning_num` NOT IN('NTC','PTC') AND `report_generate`='no' $search";
				$final_data = $this->db->query($final_sql)->result_array();
			}else{
				
				// $this->db->where('assessioning_num',$id);
				// $spe_data = $this->db->get('wp_abd_specimen')->row_array();
				// $final_data = array(array('accessioning_num'=>$id,'process_done'=>$spe_data['process_done']));

				$spe_data = $this->db->query("SELECT spe.*,gpr.`report_pdf_name` FROM `wp_abd_specimen` spe LEFT JOIN `wp_abd_generated_pcr_reports` gpr ON spe.`assessioning_num` = gpr.`accessioning_num` WHERE spe.`assessioning_num` ='".$id."'")->row_array();
				$final_data = array(array('accessioning_num'=>$id,'process_done'=>$spe_data['process_done'],'report_pdf_name'=>$spe_data['report_pdf_name']));
			}

			if(!empty($final_data)){
				die( json_encode(array( "status" => "1","report_specimen"=>$final_data,'specimen_count'=>count($final_data) )));
			}else{
				die( json_encode(array( "status" => '0' )));
			}
		
	}

	function get_batch_delete(){
		//$final_sql= "SELECT * FROM `wp_abd_batch_pass_fail_history` GROUP BY `batch_no`";
		$final_sql= "SELECT DISTINCT `batch_no` FROM `wp_abd_import_data`";
		$final_data = $this->db->query($final_sql)->result_array();

		if(!empty($final_data)){
			die( json_encode(array( "status" => '1', 'batch_delete'=>$final_data, 'specimen_count'=>count($final_data))));
		}else{
			die( json_encode(array( "status" => '0' )));
		}
	}

	function batch_delete(){
		$data = json_decode(file_get_contents('php://input'), true);
			if(!empty($data)){
				 $this->db->query("DELETE FROM `wp_abd_import_data` where `batch_no` = '".$data['batch_no']."'");
 
  				$this->db->query("DELETE FROM `wp_abd_batch_pass_fail_history` where `batch_no` = '".$data['batch_no']."'");
  			if($this->db->affected_rows() > 0){
  				die( json_encode(array( "status" => '1' )));
  			}else{
  				die( json_encode(array( "status" => '0' )));
  			}

			}
	}

	function view_pcr_report(){
		$data = json_decode(file_get_contents('php://input'), true);
		$sendCount =0;
		if(!empty($data)){
			$reportListArr = array();
		foreach($data as $assessioning_num){
		$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
		$pathology_results = $this->db->query($specimen_data)->row_array();
	
		$report_recive_ways = get_user_meta($pathology_results['physician_id'], 'report_recive_way', true);	
		$selected_report_recive_way = explode(',', $report_recive_ways['meta_value']);
		
		if($report_recive_ways){
			foreach($selected_report_recive_way as $selected_report_recive){
				if($selected_report_recive == 'Fax'){
					$fax = 'yes';
				} 
				elseif($selected_report_recive == 'None'){
					$fax = 'No';
		
				}
			}
		}
		else{
			$fax = 'yes';
		}
		  			
  		/// Report Generate ///

		date_default_timezone_set('MST7MDT');
if(!empty($pathology_results['partners_company'])){
			$prtnr_id = $pathology_results['partners_company'];
			$spe_id = $pathology_results['id'];
			$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
			$partners_company_data = $this->db->query($partners_company_sql)->row_array();
			$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
			$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();
			$pdfLogo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
			$filecontent = $this->load->view('sign_pcr_report_nextgen','',true);
		}else{
			$pdfLogo = base_url()."assets/frontend/images/logo.png";
			$filecontent = $this->load->view('sign_pcr_report','',true);
		}
		$pdfTotalBody = $filecontent;
		$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
		$specimen_det = $this->db->query($specimen_data)->row_array();

		$clinical_data = "SELECT * FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
		$clinical_det = $this->db->query($clinical_data)->row_array();

		$additional_desc_sql= "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` LIKE '".$specimen_det['id']."' AND `stage_id`='1'";
		$additional_desc = $this->db->query($additional_desc_sql)->row_array();
		
		$pat_name= $specimen_det['p_firstname']." ".$specimen_det['p_lastname'];
		$physician_fname = get_user_meta($specimen_det['physician_id'], 'first_name', true);
		$physician_lname = get_user_meta($specimen_det['physician_id'], 'last_name', true);
		$physician_name = $physician_fname['meta_value']." ".$physician_lname['meta_value'];
		$physician_phone = get_user_meta($specimen_det['physician_id'], '_mobile', true);
		$physician_address = get_user_meta($specimen_det['physician_id'], '_address', true);
		$physician_fax = get_user_meta($specimen_det['physician_id'], 'fax', true);
		
		$import_data = "SELECT DISTINCT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` LIKE '".$assessioning_num."'  AND `target_name` NOT IN ('Xeno_Ac00010014_a1','C. albicans_ Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Ac00010014_a1','Fn04646233_s1','Fn04646221_s1','C. albicans_Fn04646233_s1') order by `positive_negtaive` desc";
		$report_det = $this->db->query($import_data)->result_array();
		$table_data = "";
		
		$table_data.= "<table style='width:70%; border: 1px solid black; border-collapse: collapse;padding: 4px;font-size: 11px;'>";
					 
		foreach($report_det as $report_taxon){
			$name = explode("_",$report_taxon['target_name']);	
			if(count($name)==1) 
			{
				$target_name = $name[0]; 
			}
			else if(count($name)==2)	
			{
				$target_name = $name[1];
			}
			else if(count($name)==3)
			{
				$target_name = trim($name[1])."_".trim($name[2]);
			}
			if($report_taxon['target_name'] == "Pa04230908_s1" || $report_taxon['target_name'] == "MRSA_Pa04230908_s1" )
			{

				$target_name = "Pa04230908_s1";
			}
			if($report_taxon['target_name'] == "AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_APAACDX"){

					$target_name = "APAACDX";
			}	

			$taxon_data = "SELECT `taxon` FROM `wp_abd_asses_genes` WHERE `assay_name` LIKE '".$target_name."'";
			$taxon_det = $this->db->query($taxon_data)->row_array();
			if($report_taxon['positive_negtaive']=="positive")
			{
			$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px; color: red;';
			}
			else
			{
			$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;';
			}
			$table_data.= "<tr>
			<td style='font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;'>".$taxon_det['taxon']."</td>
			<td style='".$sty."'>".$report_taxon['positive_negtaive']."</td>
			</tr>";
		}

		$val = explode(",",$clinical_det['clinical_specimen']);
			$new_val = array();
			foreach($val as $k=>$v)
			{ 
				if(!empty($v))	
				{	
					array_push($new_val,$v);
				}
			}
		$tot_val = implode(",",$new_val);

		$table_data.= "</table>";
		$find=array();
		$find[]="{logo}";
		$find[]="{signature}";
		$find[]="{date}";
		$find[]="{acc_num}";
		$find[]="{p_name}";
		$find[]="{p_phone}";
		$find[]="{p_dob}";
		$find[]="{p_col_date}";
		$find[]="{p_rec_date}";
		$find[]="{phy_name}";
		$find[]="{phy_phone}";
		$find[]="{phy_address}";
		$find[]="{phy_fax}";
		$find[]="{table_data}";
		$find[]="{addi_desc}";
		$find[]="{site_indicator}";
		$find[]="{location}";
		$find[]="{date_generate}";
		$find[]="{time_report}";
		if(!empty($specimen_det['partners_company'])){
			$find[]="{partner_name}";
			$find[]="{address_line1}";
			$find[]="{address_line2}";
			$find[]="{partner_phone}";
			$find[]="{partner_fax}";
			$find[]="{partner_cli}";
			$find[]="{partner_lab_doc}";
			$find[]="{patient_id}";
		}
		$replace=array();
		$replace[] = $pdfLogo;
		$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
		$replace[] = date('m-d-Y');
		$replace[] = $assessioning_num;
		$replace[] = $pat_name;
		$replace[] = $specimen_det['patient_phn'];
		$replace[] = $specimen_det['patient_dob'];
		$replace[] = $specimen_det['collection_date'];
		$replace[] = $specimen_det['date_received'];
		if(!empty($specimen_det['partners_company'])){
				$replace[] = $partners_specimen_data['physician_name'];
				$replace[] = $partners_specimen_data['physician_phone'];
				$replace[] = $partners_specimen_data['physician_address'];
				$replace[] = '';
			}else{
				$replace[] = $physician_name;
				$replace[] = $physician_phone['meta_value'];
				$replace[] = $physician_address['meta_value'];
				$replace[] = $physician_fax['meta_value'];
			}
		$replace[] = $table_data;
		$replace[] = $additional_desc['addtional_desc'];
		$replace[] = $specimen_det['site_indicator'];
		$replace[] = $tot_val;
		$replace[] = date('m/d/Y');
		$replace[] = date("H:i");
		if(!empty($specimen_det['partners_company'])){
				$replace[] = $partners_company_data['partner_name'];
				$replace[] = $partners_company_data['partner_address1'];
				$replace[] = $partners_company_data['partner_address2'];
				$replace[] = $partners_company_data['phone_no'];
				$replace[] = $partners_company_data['fax_no'];
				$replace[] = $partners_company_data['clia_no'];
				$replace[] = str_ireplace('<p>','',$partners_company_data['lab_director']);
				$replace[] = $partners_specimen_data['external_id'];
			}
		for($data_int=0;$data_int<count($find);$data_int++){ 
		$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
		}
		$body = $pdfTotalBody;
		$message = $body;
		$mpdf = new mPDF('','A4','','', 0, 0, 0, 0, 0, 0); 
		$mpdf->WriteHTML($message);
		$ability_pdf = "pcr_report_".$assessioning_num.".pdf";
		//$mpdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$ability_pdf,"F");
		$mpdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$ability_pdf,"F");
		
		$pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$ability_pdf;
		$report_pdf_link = $pdf_link;
	//	if($fax == 'yes'){ 	
		$fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
		$fax_no = str_replace(".","-",$fax_number['meta_value']);
		$fname = get_user_meta($pathology_results['physician_id'], 'first_name',  true);
		$lname = get_user_meta($pathology_results['physician_id'], 'last_name',  true);
		$mobile = get_user_meta($pathology_results['physician_id'], '_mobile',true);
		$name = 'Dr '.$fname['meta_value']." ".$lname['meta_value'];

if(!empty($specimen_det['partners_company'])){
			$sendArr = array(
				'accessioning_num'=>$assessioning_num,
				'phy_name'=>'Dr '.$partners_specimen_data['physician_name'],
				'fax_number'=>'',
				'mobile'=>$partners_specimen_data['physician_phone'],
				'report_pdf_link'=>$report_pdf_link,
				'ability_pdf'=>$ability_pdf
			);
			
		}else{
			$sendArr = array(
				'accessioning_num'=>$assessioning_num,
				'phy_name'=>$name,
				'fax_number'=>$fax_number['meta_value'],
				'mobile'=>$mobile['meta_value'],
				'report_pdf_link'=>$report_pdf_link,
				'ability_pdf'=>$ability_pdf
			);
		}
		array_push($reportListArr,$sendArr);

		//}
		
		// $this->db->where('accessioning_num',$assessioning_num);
		// $updateData = $this->db->update('wp_abd_import_data',array('report_generate' => 'yes','report_pdf_name'=> $ability_pdf));

		// $pcr_sql_insert = $this->db->insert('wp_abd_generated_pcr_reports',
		//  		array(
		//  			'accessioning_num'    => $assessioning_num,
		//  			'report_pdf_name' => $ability_pdf,
		//  			'created_date'     => date('Y-m-d H:i:s')								
		//  		));

		//  	$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
		// 	array(
		// 		'physician_id'    => $specimen_det['physician_id'],
		// 		'report_pdf_link' => $report_pdf_link,
		// 		'report_name'     => $ability_pdf,
		// 		'create_date'     => date('Y-m-d H:i:s')								
		// 	));

		// if($fax == 'yes'){ 	
		// $fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
		// $fax_no = str_replace(".","-",$fax_number['meta_value']);
		// $fname = get_user_meta($pathology_results['physician_id'], 'first_name',  true);
		// $lname = get_user_meta($pathology_results['physician_id'], 'last_name',  true);
		// $mobile = get_user_meta($pathology_results['physician_id'], '_mobile',true);
		// $name = 'Dr '.$fname['meta_value']." ".$lname['meta_value'];
		// $pdfHeaderfile = $this->load->view('print_fax_cover_pdf','',true);
		// $pdfTotalBody   =  $pdfHeaderfile;

		// $logo = base_url()."assets/frontend/images/logo.png";
		// $replace_in_pdf = array(
		// 	"{logo}"          => $logo,
		// 	"{date}"          => date('m/d/Y'),
		// 	"{physician_name}"  => $name,
		// 	"{physician_mobile}"=> $mobile['meta_value'],
		// 	"{physician_fax}"   => $fax_no
		// );
		// foreach($replace_in_pdf as $find => $replace):
		// $pdfHeaderfile = str_replace($find, $replace, $pdfHeaderfile);
		// endforeach;
		// $coverbody    = $pdfHeaderfile;
		// $newmessage = $coverbody;
		// $faxCoverPdf = new m_pdf();
		// $faxCoverPdf->pdf->WriteHTML($newmessage);
		
		// $p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
		// $fax_cover = $p_Name.'_'.$lname['meta_value'].'_'.$pathology_results['assessioning_num'].'.pdf';
		// $fax_cover_pdf = str_replace(' ', '', $fax_cover);

		// $faxCoverPdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$fax_cover_pdf,"F");
		// //$faxCoverPdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$fax_cover_pdf,"F");
		// $fax_cover_pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$fax_cover_pdf;
	
		// $status = $this->sendEfax($name,$fax_no,$fax_cover_pdf_link,$report_pdf_link);
		// if($status){
		// 	$sendCount++;
		// 	$this->db->where('report_name', $ability_pdf);
		// 	$updateData = $this->db->update('wp_abd_auto_mail', array('fax_cover_pdf_link' => $fax_cover_pdf_link));     
		// }
		// $anthr_fax_number = get_user_meta($pathology_results['physician_id'],'anthr_fax_number', true);
		// if($anthr_fax_number['meta_value'] !=""){
		// 	$another_fax_no = str_replace(".","-",$anthr_fax_number);
		// 	$anotherStatus = $this->sendEfax($name,$another_fax_no,$fax_cover_pdf_link,$report_pdf_link);
		// }
		// }	

		}
		
	// if($sendCount > 0){
	// 	die( json_encode(array( "status" => '1' )));
	// }else{
	// 	die( json_encode(array( "status" => '0' )));
	// }
	die( json_encode(array( "status" => '1','generated_pdf'=>$reportListArr )));
	}
}

function send_email_fax(){
	$data = json_decode(file_get_contents('php://input'), true);
	//die( json_encode(array( "status" => $data )));
	$sendCount =0;
	$dataList = $data['dataList'];
	$user_id = $data['user_id'];

	if(!empty($dataList)){
		$mailMsg = '<table>
		<tr>
		  <th>Accessioning Number</th>
		  <th>Physician Name</th>
		  <th>Fax Number</th>
		  <th>Report Link</th>
		</tr>';
		$reportListArr = array();
	foreach($dataList as $dt){
		$assessioning_num = $dt['accessioning_num'];
		$report_pdf_link = $dt['report_pdf_link'];
		$ability_pdf = $dt['ability_pdf'];
	$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
	$pathology_results = $this->db->query($specimen_data)->row_array();

if(!empty($pathology_results['partners_company'])){
		$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$pathology_results["partners_company"].'';
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();

		$report_recive_ways = $partners_company_data['report_recive_way'];	
		$selected_report_recive_way = explode(',', $report_recive_ways);
	}else{
		$report_recive_ways = get_user_meta($pathology_results['physician_id'], 'report_recive_way', true);	
		$selected_report_recive_way = explode(',', $report_recive_ways['meta_value']);
	}

	
	if($report_recive_ways){
		foreach($selected_report_recive_way as $selected_report_recive){
			if($selected_report_recive == 'Fax'){
				$fax = 'yes';
			} 
			elseif($selected_report_recive == 'None'){
				$fax = 'No';
	
			}
		}
	}
	else{
		$fax = 'yes';
	}
				  
	  /// Report Generate ///

	date_default_timezone_set('MST7MDT');
	$filecontent = $this->load->view('sign_pcr_report','',true);
	$pdfTotalBody = $filecontent;
	$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
	$specimen_det = $this->db->query($specimen_data)->row_array();

	$clinical_data = "SELECT * FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '".$assessioning_num."'";
	$clinical_det = $this->db->query($clinical_data)->row_array();

	$additional_desc_sql= "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` LIKE '".$specimen_det['id']."' AND `stage_id`='1'";
	$additional_desc = $this->db->query($additional_desc_sql)->row_array();
	
	$pat_name= $specimen_det['p_firstname']." ".$specimen_det['p_lastname'];
	$physician_fname = get_user_meta($specimen_det['physician_id'], 'first_name', true);
	$physician_lname = get_user_meta($specimen_det['physician_id'], 'last_name', true);
	$physician_name = $physician_fname['meta_value']." ".$physician_lname['meta_value'];
	$physician_phone = get_user_meta($specimen_det['physician_id'], '_mobile', true);
	$physician_address = get_user_meta($specimen_det['physician_id'], '_address', true);
	$physician_fax = get_user_meta($specimen_det['physician_id'], 'fax', true);
	
	$import_data = "SELECT DISTINCT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` LIKE '".$assessioning_num."'  AND `target_name` NOT IN ('Xeno_Ac00010014_a1','C. albicans_ Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Ac00010014_a1','Fn04646233_s1','Fn04646221_s1','C. albicans_Fn04646233_s1') order by `positive_negtaive` desc";
	$report_det = $this->db->query($import_data)->result_array();
	// $table_data = "";
	
	// $table_data.= "<table style='width:70%; border: 1px solid black; border-collapse: collapse;padding: 4px;font-size: 11px;'>";
				 
	// foreach($report_det as $report_taxon){
	// 	$name = explode("_",$report_taxon['target_name']);	
	// 	if(count($name)==1) 
	// 	{
	// 		$target_name = $name[0]; 
	// 	}
	// 	else if(count($name)==2)	
	// 	{
	// 		$target_name = $name[1];
	// 	}
	// 	else if(count($name)==3)
	// 	{
	// 		$target_name = trim($name[1])."_".trim($name[2]);
	// 	}
	// 	if($report_taxon['target_name'] == "Pa04230908_s1" || $report_taxon['target_name'] == "MRSA_Pa04230908_s1" )
	// 	{

	// 		$target_name = "Pa04230908_s1";
	// 	}
	// 	if($report_taxon['target_name'] == "AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_APAACDX"){

	// 			$target_name = "APAACDX";
	// 	}	

	// 	$taxon_data = "SELECT `taxon` FROM `wp_abd_asses_genes` WHERE `assay_name` LIKE '".$target_name."'";
	// 	$taxon_det = $this->db->query($taxon_data)->row_array();
	// 	if($report_taxon['positive_negtaive']=="positive")
	// 	{
	// 	$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px; color: red;';
	// 	}
	// 	else
	// 	{
	// 	$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;';
	// 	}
	// 	$table_data.= "<tr>
	// 	<td style='font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;'>".$taxon_det['taxon']."</td>
	// 	<td style='".$sty."'>".$report_taxon['positive_negtaive']."</td>
	// 	</tr>";
	// }

	// $val = explode(",",$clinical_det['clinical_specimen']);
	// 	$new_val = array();
	// 	foreach($val as $k=>$v)
	// 	{ 
	// 		if(!empty($v))	
	// 		{	
	// 			array_push($new_val,$v);
	// 		}
	// 	}
	// $tot_val = implode(",",$new_val);

	// $table_data.= "</table>";
	// $find=array();
	// $find[]="{logo}";
	// $find[]="{signature}";
	// $find[]="{date}";
	// $find[]="{acc_num}";
	// $find[]="{p_name}";
	// $find[]="{p_phone}";
	// $find[]="{p_dob}";
	// $find[]="{p_col_date}";
	// $find[]="{p_rec_date}";
	// $find[]="{phy_name}";
	// $find[]="{phy_phone}";
	// $find[]="{phy_address}";
	// $find[]="{phy_fax}";
	// $find[]="{table_data}";
	// $find[]="{addi_desc}";
	// $find[]="{site_indicator}";
	// $find[]="{location}";
	// $find[]="{date_generate}";
	// $find[]="{time_report}";
	// $replace=array();
	// $replace[] = base_url()."assets/frontend/images/logo.png";
	// $replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
	// $replace[] = date('m-d-Y');
	// $replace[] = $assessioning_num;
	// $replace[] = $pat_name;
	// $replace[] = $specimen_det['patient_phn'];
	// $replace[] = $specimen_det['patient_dob'];
	// $replace[] = $specimen_det['collection_date'];
	// $replace[] = $specimen_det['date_received'];
	// $replace[] = $physician_name;
	// $replace[] = $physician_phone['meta_value'];
	// $replace[] = $physician_address['meta_value'];
	// $replace[] = $physician_fax['meta_value'];
	// $replace[] = $table_data;
	// $replace[] = $additional_desc['addtional_desc'];
	// $replace[] = $specimen_det['site_indicator'];
	// $replace[] = $tot_val;
	// $replace[] = date('m/d/Y');
	// $replace[] = date("H:i");
	// for($data_int=0;$data_int<count($find);$data_int++){ 
	// $pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
	// }
	// $body = $pdfTotalBody;
	// $message = $body;
	// $mpdf = new mPDF('','A4','','', 0, 0, 0, 0, 0, 0); 
	// $mpdf->WriteHTML($message);
	// $ability_pdf = "pcr_report_".$assessioning_num.".pdf";
	// $mpdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$ability_pdf,"F");
	//$mpdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$ability_pdf,"F");
	
	// $pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$ability_pdf;
	// $report_pdf_link = $pdf_link;
	// if($fax == 'yes'){ 	
	// $fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
	// $fax_no = str_replace(".","-",$fax_number['meta_value']);
	// $fname = get_user_meta($pathology_results['physician_id'], 'first_name',  true);
	// $lname = get_user_meta($pathology_results['physician_id'], 'last_name',  true);
	// $mobile = get_user_meta($pathology_results['physician_id'], '_mobile',true);
	// $name = 'Dr '.$fname['meta_value']." ".$lname['meta_value'];

	// $sendArr = array(
	// 	'accessioning_num'=>$assessioning_num,
	// 	'phy_name'=>$name,
	// 	'fax_number'=>$fax_number['meta_value'],
	// 	'mobile'=>$mobile['meta_value'],
	// 	'report_pdf_link'=>$report_pdf_link,
	// 	'ability_pdf'=>$ability_pdf
	// );
	// array_push($reportListArr,$sendArr);

	// }
	
	$this->db->where('accessioning_num',$assessioning_num);
	$updateData = $this->db->update('wp_abd_import_data',array('report_generate' => 'yes','report_pdf_name'=> $ability_pdf));

	$pcr_sql_insert = $this->db->insert('wp_abd_generated_pcr_reports',
	 		array(
	 			'accessioning_num'    => $assessioning_num,
	 			'report_pdf_name' => $ability_pdf,
	 			'sender_id'=>$user_id,
	 			'created_date'     => date('Y-m-d H:i:s')								
	 		));

if(!empty($pathology_results['partners_company'])){
		$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
		array(
			'physician_id'    => 0,
			'partners_company_id'    => $pathology_results['partners_company'],
			'report_pdf_link' => $report_pdf_link,
			'report_name'     => $ability_pdf,
			'create_date'     => date('Y-m-d H:i:s')								
		));
	}else{
		$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
		array(
			'physician_id'    => $specimen_det['physician_id'],
			'report_pdf_link' => $report_pdf_link,
			'report_name'     => $ability_pdf,
			'create_date'     => date('Y-m-d H:i:s')								
		));
	}

	if($fax == 'yes'){ 	
	if(empty($pathology_results['partners_company'])){
		$fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
		$fax_no = str_replace(".","-",$fax_number['meta_value']);
		$fname = get_user_meta($pathology_results['physician_id'], 'first_name',  true);
		$lname = get_user_meta($pathology_results['physician_id'], 'last_name',  true);
		$mobile = get_user_meta($pathology_results['physician_id'], '_mobile',true);
		$mob = $mobile['meta_value'];
		$name = 'Dr '.$fname['meta_value']." ".$lname['meta_value'];
		$logo = base_url()."assets/frontend/images/logo.png";
	}else{
		//$fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
		$prtnr_id = $pathology_results['partners_company'];
		$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
		$partners_company_data = $this->db->query($partners_company_sql)->row_array();
		$fax_no = str_replace(".","-",$partners_company_data['fax_no']);
		$name = $partners_company_data['partner_name'];
		$mob = $partners_company_data['phone_no'];
		$logo =  base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
	}
	$pdfHeaderfile = $this->load->view('print_fax_cover_pdf','',true);
	$pdfTotalBody   =  $pdfHeaderfile;

	$replace_in_pdf = array(
		"{logo}"          => $logo,
		"{date}"          => date('m/d/Y'),
		"{physician_name}"  => $name,
		"{physician_mobile}"=> $mob,
		"{physician_fax}"   => $fax_no
	);
	foreach($replace_in_pdf as $find => $replace):
	$pdfHeaderfile = str_replace($find, $replace, $pdfHeaderfile);
	endforeach;
	$coverbody    = $pdfHeaderfile;
	$newmessage = $coverbody;
	$faxCoverPdf = new m_pdf();
	$faxCoverPdf->pdf->WriteHTML($newmessage);
	
	$p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
	$fax_cover = $p_Name.'_'.$lname['meta_value'].'_'.$pathology_results['assessioning_num'].'.pdf';
	$fax_cover_pdf = str_replace(' ', '', $fax_cover);

	//$faxCoverPdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$fax_cover_pdf,"F");
	$faxCoverPdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$fax_cover_pdf,"F");
	$fax_cover_pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$fax_cover_pdf;

	$status = $this->sendEfax($name,$fax_no,$fax_cover_pdf_link,$report_pdf_link);
	if($status){
		$sendCount++;
		$this->db->where('report_name', $ability_pdf);
		$updateData = $this->db->update('wp_abd_auto_mail', array('fax_cover_pdf_link' => $fax_cover_pdf_link));     
	}
	$anthr_fax_number = get_user_meta($pathology_results['physician_id'],'anthr_fax_number', true);
	if($anthr_fax_number['meta_value'] !=""){
		$another_fax_no = str_replace(".","-",$anthr_fax_number);
		$anotherStatus = $this->sendEfax($name,$another_fax_no,$fax_cover_pdf_link,$report_pdf_link);
	}

	$mailMsg .='<tr>
    <td>'.$assessioning_num.'</td>
    <td>'.$name.'</td>
	<td>'.$fax_no.'</td>
	<td><a href="'.$report_pdf_link.'">View Report</a></td>
  </tr>';
	}	

	}
	$mailMsg .='</table>';
if($sendCount > 0){
	$this->load->library('email');

	$this->email->from('support@abilitydiagnostics.com', 'Ability Diagnostics');
	$this->email->to('trajanking@gmail.com');
	$this->email->bcc('ashis@sleekinfosolutions.com');
	$this->email->subject('PCR Report Generate Details');
	$this->email->message($mailMsg);
	$this->email->set_mailtype('html');
	$this->email->send();
	die( json_encode(array( "status" => '1')));
}else{
	die( json_encode(array( "status" => '0' )));
}
}
}

function unlink_generated_pdf(){
	$data = json_decode(file_get_contents('php://input'), true);
	if(!empty($data)){
		for($i=0;$i<count($data);$i++){
			$path_to_file = FCPATH."assets/uploads/pcr_report_pdf/".$data[$i]['ability_pdf'];
			unlink($path_to_file);
		}
		die( json_encode(array( "status" => '1' )));
	}
	
}


function sendEfax($name,$fax_no,$fax_cover_pdf_link,$report_pdf_link){
	header("Content-Type: text/html");
	$efax = new eFax(false);
	$efax->set_account_id("8014694891");
	$efax->set_user_name("antaresps");
	$efax->set_user_password("antaresps");
	$efax->add_recipient($name, "Ability Diagnostics", $fax_no);
	$fax_cover = file_get_contents($fax_cover_pdf_link);
	$efax->add_file("pdf", $fax_cover);
	$report_pdf  = file_get_contents($report_pdf_link);
	$efax->add_file("pdf", $report_pdf);
	$efax->set_outbound_url("https://secure.efaxdeveloper.com/EFax_WebFax.serv");
	$efax->set_fax_id("Fax #" . rand());
	$efax->add_disposition_email("Trajan King", "trajanking@gmail.com");
	$efax->add_disposition_email("Blake Clouse", "blake@abilitydiagnostics.com");
	$efax->set_disposition_level(eFax::RESPOND_ERROR | eFax::RESPOND_SUCCESS);
	$efax->set_disposition_method("EMAIL");
	$efax->set_duplicate_id(true);
	$efax->set_fax_header("@DATE @TIME Ability Diagnostics");
	$efax->set_priority("NORMAL");
	$efax->set_resolution("STANDARD");
	$efax->set_self_busy(true);
	try{
		$result = $efax->send($efax->message());
		if($result){
		return true;
		}else{
		return false;
		}
		}
		catch(eFaxException $e){
		return false;
		}
}

	function view_submited_report(){
		$submited_report_data = $this->db->query("SELECT * FROM `wp_abd_generated_pcr_reports` ORDER BY `accessioning_num` DESC")->result_array();
		if(!empty($submited_report_data)){
			die( json_encode(array( "status" => '1', 'submited_report_data'=>$submited_report_data,'specimen_count'=>count($submited_report_data))));
		}else{
			die( json_encode(array( "status" => '0' )));
		}

	}

	function view_archive_document(){
		$archive_doc_data = $this->db->query("SELECT * FROM `wp_abd_doc_log` WHERE `status` = 'Active' ORDER BY `id` DESC")->result_array();
		if(!empty($archive_doc_data)){
			die( json_encode(array( "status" => '1', 'archive_doc_data'=>$archive_doc_data,'specimen_count'=>count($archive_doc_data))));
		}else{
			die( json_encode(array( "status" => '0' )));
		}

	}

	function upload_archive_file(){
		//$data = json_decode(file_get_contents('php://input'), true);
		$name = $this->input->post('name');
		$fileName = NULL;

		if($_FILES && $_FILES['file_data']['name']){
			$config['upload_path'] = 'assets/uploads/archive_files';
			$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|eds';
			$config['max_size'] = 5000000;

			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file_data')){
				die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
			}else{
				$uploadData = $this->upload->data();
				$fileName = $uploadData['file_name'];
				$insert = $this->db->insert('wp_abd_doc_log',array( 'file_name' => $fileName,'created_date' => date('Y-m-d h:i:s')));
				$lastInsertId = $this->db->insert_id();
				die( json_encode(array( "status" => '1')));
			}

		}

	}

	function delete_archive_file(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$file_id = $data['file_id'];
 		 $archive_doc_data = $this->db->query("DELETE FROM `wp_abd_doc_log` WHERE `id` = $file_id");
		if($this->db->affected_rows() > 0){
			die( json_encode(array( "status" => '1')));
		}else{
			die( json_encode(array( "status" => '0' )));
		}
		}

	}

	function searchForId($id, $array,$data) {
       foreach ($array as $key => $val) {
           if ($val[$data] === $id) {
               return $key;
           }
       }
       return 'yes';
	}

	function pcr_next()
    {
        $time_det= array(); 
        $data = json_decode(file_get_contents('php://input'), true);
        
        $arr_val[0]= "";
        $arr_val[1]= "";
        $num = "";
        $color_val[1] = "tan";
        $sub = "";
        $cas_val = "1";
        $nail = "nail";
        $histo_chk = "";
        $pcr_chk = "";

        $id = "'".$data['id']."'";
        $stageid = "'".$data['stageid']."'";

        $conditions = " ( `id` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_data  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
	
        $conditions1 = " (`specime_stage_id` = ".$stageid.")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_data  = $this->BlankModel->getTableData('specimen_ids', $conditions1, $select_fields, $is_multy_result);
	
		$conditions3 = " (`specimen_id`= ".$id." AND `stage_id` = '1')";
		$select_fields = '*';
		$is_multy_result = 1;
		$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions3, $select_fields, $is_multy_result);
		
		if(!empty($pcr_stage_det))
		{
			$specimen_stage_det = $pcr_stage_det;
		}
	
		else
		{
			$specimen_stage_det="Nil";
		}



        if(!empty($pcr_stage_det))
        {
                $ori_value = substr($specimen_stage_det['total_desc'],0,10);
                $dimen_value = substr($specimen_stage_det['total_desc'],11);
                $arr_val = explode("x",$ori_value);
                $color_val = explode(" ",$specimen_stage_det['total_desc']);
                if(!empty($arr_val) && !empty($ori_value))
                {
                 $third_val = substr($arr_val[2],0,2);
                 $num =  filter_var($third_val, FILTER_SANITIZE_NUMBER_INT);
                }
                else
                {
                    $arr_val[0]= "";
                    $arr_val[1]= "";
                    $color_val[1]= "";
                }
        
                $desc = explode(" ",$specimen_stage_det['addtional_desc']);
                $new_cas_val = substr($specimen_stage_det['addtional_desc'],-13);
                $cas_val=filter_var($new_cas_val, FILTER_SANITIZE_NUMBER_INT);
                if(in_array('nail', $desc) || empty($specimen_stage_det['addtional_desc'])) 
                { 
                  $nail = "nail"; 
                }
                else if(in_array('skin', $desc))
                {
                  $nail = "skin"; 
                }
                else
                {
                  $nail = "nail"; 
                } 
                 if(!empty($specimen_stage_det['submitted']))
                 {
                     $sub = 1;
                 }
                 else
                 {
                     $sub = 0;
                 }
                 if($specimen_stage_det['histo']!="")
                 {
                     $histo_chk = 1;
                 }
                 else
                 {
                     $histo_chk = 0;
                 }
                 if($specimen_stage_det['pcr']!="")
                 {
                     $pcr_chk = 1;
                 }
                 else
                 {
                     $pcr_chk = 0 ;
                 }
           

		}

        // if(empty($specimen_stage_det))
        // {
        //     $specimen_stage_det ="Nil";
        // }
        $specimen_stages_sql = "SELECT * FROM (SELECT * FROM `wp_abd_specimen_stage_details`)stage_det
                   INNER JOIN (SELECT * FROM `wp_abd_specimen_ids`)stages ON `stage_det`.`stage_id` =  `stages`.`specime_stage_id` 
                    AND `stage_det`.`specimen_id`=".$id." ORDER BY `stage_det`.`stage_id` ASC"; 
		$specimens_details = $this->BlankModel->customquery($specimen_stages_sql);
        foreach ($specimens_details as $stage_det)
        {
            $physician_first_name = get_user_meta($stage_det['lab_tech'], 'first_name' );
            $physician_last_name  = get_user_meta($stage_det['lab_tech'], 'last_name');
            $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
            if($stage_det['pass_fail']=='1')
            {
                $val = 'ticked';
            }
            else{
                $val = 'crossed';
            }

            $specimen_information = array('id'=>$stage_det['specimen_id'],'stage_name'=>$stage_det['stage_name'],'specimen_timestamp'=>$stage_det['specimen_timestamp'],'comments'=>$stage_det['comments'],'addtional_desc'=>$stage_det['addtional_desc'],'physician_name'=>$physician_name,'pass_fail' => $val);
            array_push($time_det, $specimen_information);
        }

        $conditions3 = " ( `specimen_id` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$clinical_data  = $this->BlankModel->getTableData('clinical_info', $conditions3, $select_fields, $is_multy_result);
        $clinical_specimen     = explode(",",$clinical_data['clinical_specimen']);
      
        if($specimen_data)
		{
            echo json_encode(array('status'=>'1','spe_details'=>$specimen_data,'stage_data'=>$specimen_stage_data,'stage_det'=>$specimen_stage_det,'stage_time'=>$time_det,'clinical_info'=>$clinical_specimen,
            'first'=>$arr_val[0],'second'=>$arr_val[1],'third'=>$num,'color'=> $color_val[1],'cassettes'=>$cas_val,'nail'=>$nail,
        'sub'=>$sub,'histo_chk'=>$histo_chk,'pcr_chk'=>$pcr_chk));
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }

	}
	
	function search_single_pcr()
	{
		$time_det= array(); 
        $data = json_decode(file_get_contents('php://input'), true);
        
        $arr_val[0]= "";
        $arr_val[1]= "";
        $num = "";
        $color_val[1] = "tan";
        $sub = "";
        $cas_val = "1";
        $nail = "nail";
        $histo_chk = "";
        $pcr_chk = "";

		if(!empty($data['barcode']))
		{
			$id = "'".$data['barcode']."'";
        	$stageid = "'1'";
        	$acc_id = $data['barcode'];
		}
		else
		{
			$id = "'".$data['assessioning_num']."'";
        	$stageid = "'1'";
        	$acc_id = $data['assessioning_num'];
		}
        $accessioning_num_type = substr(strrchr($acc_id, "-"), 1);

	if($accessioning_num_type != 'W'){
				        
        $conditions = " ( `assessioning_num` = ".$id.")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_data  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		
        $conditions1 = " (`specime_stage_id` = ".$stageid.")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_data  = $this->BlankModel->getTableData('specimen_ids', $conditions1, $select_fields, $is_multy_result);
		
		if(!empty($specimen_data))
		{
			$conditions3 = " (`specimen_id`= ".$specimen_data['id']." AND `stage_id` = '1')";
			$select_fields = '*';
			$is_multy_result = 1;
			$pcr_stage_det  = $this->BlankModel->getTableData('pcr_stage_details', $conditions3, $select_fields, $is_multy_result);
		}
		
		if(!empty($pcr_stage_det))
		{
			$specimen_stage_det = $pcr_stage_det;
		}
	
		else
		{
			$specimen_stage_det="Nil";
		}

        if(!empty($pcr_stage_det))
        {
                $ori_value = substr($specimen_stage_det['total_desc'],0,10);
                $dimen_value = substr($specimen_stage_det['total_desc'],11);
                $arr_val = explode("x",$ori_value);
                $color_val = explode(" ",$specimen_stage_det['total_desc']);
                if(!empty($arr_val) && !empty($ori_value))
                {
                 $third_val = substr($arr_val[2],0,2);
                 $num =  filter_var($third_val, FILTER_SANITIZE_NUMBER_INT);
                }
                else
                {
                    $arr_val[0]= "";
                    $arr_val[1]= "";
                    $color_val[1]= "";
                }
        
                $desc = explode(" ",$specimen_stage_det['addtional_desc']);
                $new_cas_val = substr($specimen_stage_det['addtional_desc'],-13);
                $cas_val=filter_var($new_cas_val, FILTER_SANITIZE_NUMBER_INT);
                if(in_array('nail', $desc) || empty($specimen_stage_det['addtional_desc'])) 
                { 
                  $nail = "nail"; 
                }
                else if(in_array('skin', $desc))
                {
                  $nail = "skin"; 
                }
                else
                {
                  $nail = "nail"; 
                } 
                 if(!empty($specimen_stage_det['submitted']))
                 {
                     $sub = 1;
                 }
                 else
                 {
                     $sub = 0;
                 }
                 if($specimen_stage_det['histo']!="")
                 {
                     $histo_chk = 1;
                 }
                 else
                 {
                     $histo_chk = 0;
                 }
                 if($specimen_stage_det['pcr']!="")
                 {
                     $pcr_chk = 1;
                 }
                 else
                 {
                     $pcr_chk = 0 ;
                 }
           

		}
		if(!empty($specimen_data))
		{
			$specimen_stages_sql = "SELECT * FROM (SELECT * FROM `wp_abd_specimen_stage_details`)stage_det
			INNER JOIN (SELECT * FROM `wp_abd_specimen_ids`)stages ON `stage_det`.`stage_id` =  `stages`.`specime_stage_id` 
			 AND `stage_det`.`specimen_id`=".$specimen_data['id']." ORDER BY `stage_det`.`stage_id` ASC"; 
 			$specimens_details = $this->BlankModel->customquery($specimen_stages_sql);
		}
       
        foreach ($specimens_details as $stage_det)
        {
            $physician_first_name = get_user_meta($stage_det['lab_tech'], 'first_name' );
            $physician_last_name  = get_user_meta($stage_det['lab_tech'], 'last_name');
            $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
            if($stage_det['pass_fail']=='1')
            {
                $val = 'ticked';
            }
            else{
                $val = 'crossed';
            }

            $specimen_information = array('id'=>$stage_det['specimen_id'],'stage_name'=>$stage_det['stage_name'],'specimen_timestamp'=>$stage_det['specimen_timestamp'],'comments'=>$stage_det['comments'],'addtional_desc'=>$stage_det['addtional_desc'],'physician_name'=>$physician_name,'pass_fail' => $val);
            array_push($time_det, $specimen_information);
        }

        $conditions3 = " ( `assessioning_num` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$clinical_data  = $this->BlankModel->getTableData('clinical_info', $conditions3, $select_fields, $is_multy_result);
        $clinical_specimen     = explode(",",$clinical_data['clinical_specimen']);
      
        if($specimen_data)
		{
            echo json_encode(array('status'=>'1','spe_details'=>$specimen_data,'stage_data'=>$specimen_stage_data,'stage_det'=>$specimen_stage_det,'stage_time'=>$time_det,'clinical_info'=>$clinical_specimen,
            'first'=>$arr_val[0],'second'=>$arr_val[1],'third'=>$num,'color'=> $color_val[1],'cassettes'=>$cas_val,'nail'=>$nail,
        'sub'=>$sub,'histo_chk'=>$histo_chk,'pcr_chk'=>$pcr_chk));
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }else
        {
            echo json_encode(array('status'=>'0'));
        }
	}
	
	// function requeue_fail()
	// {
	// 	$data = json_decode(file_get_contents('php://input'), true);
	// 	if(!empty($data))
	// 	{
	// 		if($data['pass']==1)
	// 		{
	// 			$this->db->delete('wp_abd_import_data', array('accessioning_num' => $data['acc']));  
	// 			$spe_sql = "SELECT `id` from `wp_abd_specimen` where `assessioning_num` = '".$data['acc']."'";
	// 			$sp_sql = $this->BlankModel->customquery($spe_sql);
	// 			$this->db->where('specimen_id',$sp_sql[0]['id']);
	// 			$upd_pass = $this->db->update('wp_abd_assign_stage',array('status' => 'Unassign'));
				
	// 			$this->db->where('specimen_id',$sp_sql[0]['id']);
	// 			$this->db->where('stage_id','1');
	// 			$upd_pass = $this->db->update('wp_abd_pcr_stage_details',array('comments' => $data['comment']));
				
	// 			if($upd_pass)
	// 			{
	// 				echo json_encode(array('status'=>'1'));
	// 			}
	// 			else
	// 			{
	// 				echo json_encode(array('status'=>'0'));
	// 			}
	// 		}
	// 		if($data['pass']==0)
	// 		{	
	// 			$this->db->where('accessioning_num',$data['acc']);
	// 			$updateData = $this->db->update('wp_abd_import_data',array('report_generate' => 'yes','report_pdf_name'=>""));
	// 			$insert = $this->db->insert('wp_abd_generated_pcr_reports', 
	// 									array('accessioning_num' => $data['acc'],'cancelled_reason'=> $data['comment'],'created_date'=> date('Y-m-d h:i:s')));
	// 			if($insert)
	// 			{
	// 				echo json_encode(array('status'=>'1'));
	// 			}
	// 			else
	// 			{
	// 				echo json_encode(array('status'=>'0'));
	// 			}						
	// 		}
			

	// 	}
	// }

	// function requeue_fail()
	// {
	// 	$data = json_decode(file_get_contents('php://input'), true);
	// 	if(!empty($data))
	// 	{
	// 		if($data['pass']==1)
	// 		{
	// 			$this->db->delete('wp_abd_import_data', array('accessioning_num' => $data['acc']));  
	// 			$spe_sql = "SELECT `id` from `wp_abd_specimen` where `assessioning_num` = '".$data['acc']."'";
	// 			$sp_sql = $this->BlankModel->customquery($spe_sql);
	// 			$this->db->where('specimen_id',$sp_sql[0]['id']);
	// 			$upd_pass = $this->db->update('wp_abd_assign_stage',array('status' => 'Unassign'));
				
	// 			$this->db->where('specimen_id',$sp_sql[0]['id']);
	// 			$this->db->where('stage_id','1');
	// 			$upd_pass = $this->db->update('wp_abd_pcr_stage_details',array('comments' => $data['comment']));
				
	// 			if($upd_pass)
	// 			{
	// 				echo json_encode(array('status'=>'1'));
	// 			}
	// 			else
	// 			{
	// 				echo json_encode(array('status'=>'0'));
	// 			}
	// 		}
	// 		if($data['pass']==0)
	// 		{	

	// 		$accessioning_num = $data['acc'];
	// 		/// Report Generate ///
	// 	  	date_default_timezone_set('MST7MDT');	
	// 	  	//$accessioning_num_type = substr(strrchr($accessioning_num, '-'), 1);
		 
	// 		$filecontent = $this->load->view('sign_pcr_report_cancel','',true);
		 
	// 		$pdfTotalBody = $filecontent;
	// 		$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
	// 		$specimen_det = $this->db->query($specimen_data)->row_array();

	// 		$clinical_data = "SELECT * FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
	// 		$clinical_det = $this->db->query($clinical_data)->row_array();

	// 		$additional_desc_sql= "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` LIKE '".$specimen_det['id']."' AND `stage_id`='1'";
	// 		$additional_desc = $this->db->query($additional_desc_sql)->row_array();
			
	// 		$pat_name= $specimen_det['p_firstname']." ".$specimen_det['p_lastname'];
	// 		$physician_fname = get_user_meta($specimen_det['physician_id'], 'first_name', true);
	// 		$physician_lname = get_user_meta($specimen_det['physician_id'], 'last_name', true);
	// 		$physician_name = $physician_fname['meta_value']." ".$physician_lname['meta_value'];
	// 		$physician_phone = get_user_meta($specimen_det['physician_id'], '_mobile', true);
	// 		$physician_address = get_user_meta($specimen_det['physician_id'], '_address', true);
	// 		$physician_fax = get_user_meta($specimen_det['physician_id'], 'fax', true);
			
	// 		$import_data = "SELECT DISTINCT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` LIKE '".$accessioning_num."'  AND `target_name` NOT IN ('Xeno_Ac00010014_a1','C. albicans_ Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Ac00010014_a1','Fn04646233_s1','Fn04646221_s1','C. albicans_Fn04646233_s1') order by `positive_negtaive` desc";
	// 		$report_det = $this->db->query($import_data)->result_array();
	// 		$table_data = "";
			
	// 		$table_data.= "<table style='width:70%; border: 1px solid black; border-collapse: collapse;padding: 4px;font-size: 11px;'>";
			
	// 		foreach($report_det as $report_taxon)
	// 		{
	// 			$name = explode("_",$report_taxon['target_name']);	
	// 			if(count($name)==1) 
	// 			{
	// 				$target_name = $name[0]; 
	// 			}
	// 			else if(count($name)==2)	
	// 			{
	// 				$target_name = $name[1];
	// 			}
	// 			else if(count($name)==3)
	// 			{
	// 				$target_name = trim($name[1])."_".trim($name[2]);
	// 			}
	// 			if($report_taxon['target_name'] == "Pa04230908_s1" || $report_taxon['target_name'] == "MRSA_Pa04230908_s1" )
	// 			{

	// 				$target_name = "Pa04230908_s1";
	// 			}


	// 			if($report_taxon['target_name'] == "AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_APAACDX"){

	// 					$target_name = "APAACDX";
	// 			}	

	// 			$taxon_data = "SELECT `taxon` FROM `wp_abd_asses_genes` WHERE `assay_name` LIKE '".$target_name."'";
	// 			$taxon_det = $this->db->query($taxon_data)->row_array();
	// 			if($report_taxon['positive_negtaive']=="positive")
	// 			{
	// 			$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px; color: red;';
	// 			}
	// 			else
	// 			{
	// 			$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;';
	// 			}
	// 			$table_data.= "<tr>
	// 			<td style='font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;'>".$taxon_det['taxon']."</td>
	// 			<td style='".$sty."'>".$report_taxon['positive_negtaive']."</td>
	// 		</tr>";
	// 		}
	// 		$val = explode(",",$clinical_det['clinical_specimen']);
	// 		$new_val = array();
	// 		foreach($val as $k=>$v)
	// 		{ 
	// 			if(!empty($v))	
	// 			{	
	// 				array_push($new_val,$v);
	// 			}
	// 		}
	// 		$tot_val = implode(",",$new_val);
	// 		$table_data.= "</table>";
	// 		$find=array();
	// 		$find[]="{logo}";
	// 		$find[]="{signature}";
	// 		$find[]="{date}";
	// 		$find[]="{acc_num}";
	// 		$find[]="{p_name}";
	// 		$find[]="{p_phone}";
	// 		$find[]="{p_dob}";
	// 		$find[]="{p_col_date}";
	// 		$find[]="{p_rec_date}";
	// 		$find[]="{phy_name}";
	// 		$find[]="{phy_phone}";
	// 		$find[]="{phy_address}";
	// 		$find[]="{phy_fax}";
	// 		$find[]="{table_data}";
	// 		$find[]="{addi_desc}";
	// 		$find[]="{site_indicator}";
	// 		$find[]="{location}";
	// 		$find[]="{date_generate}";
	// 		$find[]="{time_report}";
	// 		$find[]="{cancel_reason}";
	// 		$replace=array();
	// 		$replace[] = base_url()."assets/frontend/images/logo.png";
	// 		$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
	// 		$replace[] = date('m-d-Y');
	// 		$replace[] = $accessioning_num;
	// 		$replace[] = $pat_name;
	// 		$replace[] = $specimen_det['patient_phn'];
	// 		$replace[] = $specimen_det['patient_dob'];
	// 		$replace[] = $specimen_det['collection_date'];
	// 		$replace[] = $specimen_det['date_received'];
	// 		$replace[] = $physician_name;
	// 		$replace[] = $physician_phone['meta_value'];
	// 		$replace[] = $physician_address['meta_value'];
	// 		$replace[] = $physician_fax['meta_value'];
	// 		$replace[] = $table_data;
	// 		$replace[] = $additional_desc['addtional_desc'];
	// 		$replace[] = $specimen_det['site_indicator'];
	// 		$replace[] = $tot_val;
	// 		$replace[] = date('m/d/Y');
	// 		$replace[] = date("H:i");
	// 		$replace[] = $data['comment'];
	// 		for($data_int=0;$data_int<count($find);$data_int++){ 
	// 			$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
	// 		}
	// 		$body = $pdfTotalBody;
	// 		$message = $body;
	// 		$this->m_pdf->pdf->WriteHTML($message);
	// 		$ability_pdf = "pcr_report_".$accessioning_num.".pdf";
	// 		//$this->m_pdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$ability_pdf,"F");
	// 		$this->m_pdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$ability_pdf,"F");
	// 		$this->db->where('accessioning_num',$data['acc']);
	// 		$import_tbl = $this->db->get('wp_abd_import_data');

	// 		if($import_tbl->num_rows() > 0){
	// 			$this->db->where('accessioning_num',$data['acc']);
	// 			$updateData = $this->db->update('wp_abd_import_data',array('report_generate' => 'yes','report_pdf_name'=>$ability_pdf));
	// 		}else{
	// 			$updateData = $this->db->insert('wp_abd_import_data',array('accessioning_num'=>$data['acc'],'report_generate' => 'yes','report_pdf_name'=>$ability_pdf));
	// 		}

				
	// 			$insert = $this->db->insert('wp_abd_generated_pcr_reports', 
	// 									array('accessioning_num' => $data['acc'],'report_pdf_name'=>$ability_pdf,'cancelled_reason'=> $data['comment'],'internal_comment'=> $data['internal_comment'],'created_date'=> date('Y-m-d h:i:s')));
	// 			if($insert)
	// 			{
	// 				echo json_encode(array('status'=>'1'));
	// 			}
	// 			else
	// 			{
	// 				echo json_encode(array('status'=>'0'));
	// 			}						
	// 		}
			

	// 	}
	// }

	function requeue_fail()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data))
		{
			if($data['pass']==1)
			{
				$this->db->delete('wp_abd_import_data', array('accessioning_num' => $data['acc']));  
				$spe_sql = "SELECT `id` from `wp_abd_specimen` where `assessioning_num` = '".$data['acc']."'";
				$sp_sql = $this->BlankModel->customquery($spe_sql);
				$this->db->where('specimen_id',$sp_sql[0]['id']);
				$upd_pass = $this->db->update('wp_abd_assign_stage',array('status' => 'Unassign'));
				
				$this->db->where('specimen_id',$sp_sql[0]['id']);
				$this->db->where('stage_id','1');
				$upd_pass = $this->db->update('wp_abd_pcr_stage_details',array('comments' => $data['comment']));
				
				if($upd_pass)
				{
					echo json_encode(array('status'=>'1'));
				}
				else
				{
					echo json_encode(array('status'=>'0'));
				}
			}
			if($data['pass']==0)
			{	

			$accessioning_num = $data['acc'];
			/// Report Generate ///
		  	date_default_timezone_set('MST7MDT');	
		  	//$accessioning_num_type = substr(strrchr($accessioning_num, '-'), 1);
		 
			$filecontent = $this->load->view('sign_pcr_report_cancel','',true);
		 
			$pdfTotalBody = $filecontent;
			$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
			$specimen_det = $this->db->query($specimen_data)->row_array();

			$clinical_data = "SELECT * FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
			$clinical_det = $this->db->query($clinical_data)->row_array();

			$additional_desc_sql= "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` LIKE '".$specimen_det['id']."' AND `stage_id`='1'";
			$additional_desc = $this->db->query($additional_desc_sql)->row_array();
			
			$pat_name= $specimen_det['p_firstname']." ".$specimen_det['p_lastname'];
			$physician_fname = get_user_meta($specimen_det['physician_id'], 'first_name', true);
			$physician_lname = get_user_meta($specimen_det['physician_id'], 'last_name', true);
			$physician_name = $physician_fname['meta_value']." ".$physician_lname['meta_value'];
			$physician_phone = get_user_meta($specimen_det['physician_id'], '_mobile', true);
			$physician_address = get_user_meta($specimen_det['physician_id'], '_address', true);
			$physician_fax = get_user_meta($specimen_det['physician_id'], 'fax', true);
			
			$import_data = "SELECT DISTINCT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` LIKE '".$accessioning_num."'  AND `target_name` NOT IN ('Xeno_Ac00010014_a1','C. albicans_ Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Ac00010014_a1','Fn04646233_s1','Fn04646221_s1','C. albicans_Fn04646233_s1') order by `positive_negtaive` desc";
			$report_det = $this->db->query($import_data)->result_array();
			$table_data = "";
			
			$table_data.= "<table style='width:70%; border: 1px solid black; border-collapse: collapse;padding: 4px;font-size: 11px;'>";
			
			foreach($report_det as $report_taxon)
			{
				$name = explode("_",$report_taxon['target_name']);	
				if(count($name)==1) 
				{
					$target_name = $name[0]; 
				}
				else if(count($name)==2)	
				{
					$target_name = $name[1];
				}
				else if(count($name)==3)
				{
					$target_name = trim($name[1])."_".trim($name[2]);
				}
				if($report_taxon['target_name'] == "Pa04230908_s1" || $report_taxon['target_name'] == "MRSA_Pa04230908_s1" )
				{

					$target_name = "Pa04230908_s1";
				}


				if($report_taxon['target_name'] == "AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_APAACDX"){

						$target_name = "APAACDX";
				}	

				$taxon_data = "SELECT `taxon` FROM `wp_abd_asses_genes` WHERE `assay_name` LIKE '".$target_name."'";
				$taxon_det = $this->db->query($taxon_data)->row_array();
				if($report_taxon['positive_negtaive']=="positive")
				{
				$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px; color: red;';
				}
				else
				{
				$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;';
				}
				$table_data.= "<tr>
				<td style='font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;'>".$taxon_det['taxon']."</td>
				<td style='".$sty."'>".$report_taxon['positive_negtaive']."</td>
			</tr>";
			}
			$val = explode(",",$clinical_det['clinical_specimen']);
			$new_val = array();
			foreach($val as $k=>$v)
			{ 
				if(!empty($v))	
				{	
					array_push($new_val,$v);
				}
			}
			$tot_val = implode(",",$new_val);
			$table_data.= "</table>";
			$find=array();
			$find[]="{logo}";
			$find[]="{signature}";
			$find[]="{date}";
			$find[]="{acc_num}";
			$find[]="{p_name}";
			$find[]="{p_phone}";
			$find[]="{p_dob}";
			$find[]="{p_col_date}";
			$find[]="{p_rec_date}";
			$find[]="{phy_name}";
			$find[]="{phy_phone}";
			$find[]="{phy_address}";
			$find[]="{phy_fax}";
			$find[]="{table_data}";
			$find[]="{addi_desc}";
			$find[]="{site_indicator}";
			$find[]="{location}";
			$find[]="{date_generate}";
			$find[]="{time_report}";
			$find[]="{cancel_reason}";
			$find[]="{report_heading}";
			$replace=array();
			$replace[] = base_url()."assets/frontend/images/logo.png";
			$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
			$replace[] = date('m-d-Y');
			$replace[] = $accessioning_num;
			$replace[] = $pat_name;
			$replace[] = $specimen_det['patient_phn'];
			$replace[] = $specimen_det['patient_dob'];
			$replace[] = $specimen_det['collection_date'];
			$replace[] = $specimen_det['date_received'];
			$replace[] = $physician_name;
			$replace[] = $physician_phone['meta_value'];
			$replace[] = $physician_address['meta_value'];
			$replace[] = $physician_fax['meta_value'];
			$replace[] = $table_data;
			$replace[] = $additional_desc['addtional_desc'];
			$replace[] = $specimen_det['site_indicator'];
			$replace[] = $tot_val;
			$replace[] = date('m/d/Y');
			$replace[] = date("H:i");
			$replace[] = $data['comment'];
			if($specimen_det["test_type"] == 'W'){
				$replace[] = "Wound Molecular Report";
			}else if($specimen_det["test_type"] == 'NF'){
				$replace[] = "Nail Fungal Molecular Report";
			}
			for($data_int=0;$data_int<count($find);$data_int++){ 
				$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
			}
			$body = $pdfTotalBody;
			$message = $body;
			$this->m_pdf->pdf->WriteHTML($message);
			$ability_pdf = "pcr_report_".$accessioning_num.".pdf";
			//$this->m_pdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$ability_pdf,"F");
			$this->m_pdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$ability_pdf,"F");
			$this->db->where('accessioning_num',$data['acc']);
			$import_tbl = $this->db->get('wp_abd_import_data');
			if($data['view_pdf']=='no')
			{	
			if($import_tbl->num_rows() > 0){
				$this->db->where('accessioning_num',$data['acc']);
				$updateData = $this->db->update('wp_abd_import_data',array('report_generate' => 'yes','report_pdf_name'=>$ability_pdf));
			}else{
				$updateData = $this->db->insert('wp_abd_import_data',array('accessioning_num'=>$data['acc'],'report_generate' => 'yes','report_pdf_name'=>$ability_pdf));
			}

				
				$insert = $this->db->insert('wp_abd_generated_pcr_reports', 
										array('accessioning_num' => $data['acc'],'report_pdf_name'=>$ability_pdf,'cancelled_reason'=> $data['comment'],'internal_comment'=> $data['internal_comment'],'created_date'=> date('Y-m-d h:i:s')));
				if($insert)
				{
					echo json_encode(array('status'=>'1'));
				}
				else
				{
					echo json_encode(array('status'=>'0'));
				}
			}else if($data['view_pdf']=='yes'){
				echo json_encode(array('status'=>'2','cancel_pdf'=>$ability_pdf));
			}						
			}
			

		}
	}

	function upload_wounds_report(){
		//$data = json_decode(file_get_contents('php://input'), true);
		$name = $this->input->post('name');
		$accessioning_num = $this->input->post('accessioning_num');
		$fileName = NULL;

		if($_FILES && $_FILES['file_data']['name']){
			$config['upload_path'] = 'assets/uploads/pcr_report_pdf';
			$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|eds';
			$config['max_size'] = 5000000;

			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file_data')){
				die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
			}else{
				$uploadData = $this->upload->data();
				$fileName = $uploadData['file_name'];
				$this->db->where('accessioning_num',$accessioning_num);
				$res_report = $this->db->get('wp_abd_generated_pcr_reports');
				if($res_report->num_rows() > 0){
					$report_data = $res_report->row_array();
					$this->db->where('accessioning_num',$accessioning_num);
					$insert = $this->db->update('wp_abd_generated_pcr_reports',array('report_pdf_name' => $fileName,'created_date' => date('Y-m-d h:i:s')));
				}else{
					$insert = $this->db->insert('wp_abd_generated_pcr_reports',array( 'accessioning_num'=>$accessioning_num,'report_pdf_name' => $fileName,'created_date' => date('Y-m-d h:i:s')));
					$lastInsertId = $this->db->insert_id();
				}
				$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
				$pathology_results = $this->db->query($specimen_data)->row_array();

				$report_recive_ways = get_user_meta($pathology_results['physician_id'], 'report_recive_way', true);	
				$selected_report_recive_way = explode(',', $report_recive_ways['meta_value']);
				
				if($report_recive_ways){
					foreach($selected_report_recive_way as $selected_report_recive){
						if($selected_report_recive == 'Fax'){
							$fax = 'yes';
						} 
						elseif($selected_report_recive == 'None'){
							$fax = 'No';
				
						}
					}
				}
				else{
					$fax = 'yes';
				}
				if($fax == 'yes'){ 	
					date_default_timezone_set('MST7MDT');
					$ability_pdf = $fileName;
					$report_pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$fileName;
					$fax_number = get_user_meta($pathology_results['physician_id'],'fax', true);
					$fax_no = str_replace(".","-",$fax_number['meta_value']);
					$fname = get_user_meta($pathology_results['physician_id'], 'first_name',  true);
					$lname = get_user_meta($pathology_results['physician_id'], 'last_name',  true);
					$mobile = get_user_meta($pathology_results['physician_id'], '_mobile',true);
					$name = 'Dr '.$fname['meta_value']." ".$lname['meta_value'];
					$pdfHeaderfile = $this->load->view('print_fax_cover_pdf','',true);
					$pdfTotalBody   =  $pdfHeaderfile;
				
					$logo = base_url()."assets/frontend/images/logo.png";
					$replace_in_pdf = array(
						"{logo}"          => $logo,
						"{date}"          => date('m/d/Y'),
						"{physician_name}"  => $name,
						"{physician_mobile}"=> $mobile['meta_value'],
						"{physician_fax}"   => $fax_no
					);
					foreach($replace_in_pdf as $find => $replace):
					$pdfHeaderfile = str_replace($find, $replace, $pdfHeaderfile);
					endforeach;
					$coverbody    = $pdfHeaderfile;
					$newmessage = $coverbody;
					$faxCoverPdf = new m_pdf();
					$faxCoverPdf->pdf->WriteHTML($newmessage);
					
					$p_Name = preg_replace('/\s+/', '_', $pathology_results['p_lastname']);
					$fax_cover = $p_Name.'_'.$lname['meta_value'].'_'.$pathology_results['assessioning_num'].'.pdf';
					$fax_cover_pdf = str_replace(' ', '', $fax_cover);
				
					//$faxCoverPdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$fax_cover_pdf,"F");
					$faxCoverPdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$fax_cover_pdf,"F");
					$fax_cover_pdf_link = base_url().'assets/uploads/pcr_report_pdf/'.$fax_cover_pdf;
					//die( json_encode(array( "status" => $fax_cover_pdf_link)));
					$status = $this->sendEfax($name,$fax_no,$fax_cover_pdf_link,$report_pdf_link);
					
					if($status){
						$auto_mail_sql_insert = $this->db->insert('wp_abd_auto_mail',
						array(
							'physician_id'    => $pathology_results['physician_id'],
							'report_pdf_link' => $report_pdf_link,
							'report_name'     => $ability_pdf,
							'fax_cover_pdf_link' => $fax_cover_pdf_link,
							'create_date'     => date('Y-m-d H:i:s')								
						));
						// $this->db->where('report_name', $ability_pdf);
						// $updateData = $this->db->update('wp_abd_auto_mail', array('fax_cover_pdf_link' => $fax_cover_pdf_link));     
					}
					$anthr_fax_number = get_user_meta($pathology_results['physician_id'],'anthr_fax_number', true);
					if($anthr_fax_number['meta_value'] !=""){
						$another_fax_no = str_replace(".","-",$anthr_fax_number);
						$anotherStatus = $this->sendEfax($name,$another_fax_no,$fax_cover_pdf_link,$report_pdf_link);
					}
					}	
				
				/// Report Generate ///

				
				
			 }

		}

	}
 
 }
