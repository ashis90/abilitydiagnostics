import { sandboxOf } from 'angular-playground';
import { ShortcodeComponent } from './shortcode.component';

export default sandboxOf(ShortcodeComponent)
  .add('Shortcode Component', {
    template: `<cm-shortcode></cm-shortcode>`
  });
