<!doctype html>
<html amp>
<head>
<title>Ability Diagnostics</title>
<link rel="icon" href="<?php echo base_url('assets/favicon.ico'); ?>" type="image/gif" sizes="16x16">
<style>
.top_hdr {
    background: #fefefe;
    background: -moz-linear-gradient(top, #fefefe 0, #eee 100%);
    background: -webkit-linear-gradient(top, #fefefe 0, #eee 100%);
    background: linear-gradient(to bottom, #fefefe 0, #eee 100%);
    filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#fefefe', endColorstr='#eeeeee', GradientType=0);
    box-shadow: 0 1px 3px 2px #dfdede;
    box-sizing: border-box;
    min-height: 55px;
    padding: 12px;
}
.container {
    width: 1170px;
    max-width: 100%;
    margin: 0 auto;
}
.custom-logo-link {
    display: block;
}
.top_hdr ul {
    text-align: right;
    line-height: 0;
}
.main_mnu ul li:first-child, .top_hdr ul li:first-child {
    margin: 0;
}

.scl_cntct, .top_hdr ul li, .top_hdr ul li .sub-menu:hover li ul li {
    position: relative;
}
.top_hdr ul li {
    display: inline-block;
    vertical-align: top;
    margin: 0 0 0 12px;
    line-height: 0;
}
li {
    list-style: none;
}
.top_hdr ul li a {
    color: #777;
    font: 400 15px/30px Lato;
}
.top_hdr ul li:first-child a:before {
    content: '\f023';
}
.top_hdr ul li:first-child a:before, .top_hdr ul li:last-child a:before, .top_hdr ul li:nth-child(2) a:before, .top_hdr ul li:nth-child(3) a:before {
    font-family: FontAwesome;
    margin: 0 6px 0 0;
}
.top_hdr ul li:last-child a:before {
    content: '\f08b';
}

.top_hdr ul li:nth-child(2) a:before {
    content: '\f0f2';
}
.top_hdr ul li:first-child a:before, .top_hdr ul li:last-child a:before, .top_hdr ul li:nth-child(2) a:before, .top_hdr ul li:nth-child(3) a:before {
    font-family: FontAwesome;
    margin: 0 6px 0 0;
}
img {
    height: auto;
}

embed, iframe, img, object, video {
    vertical-align: middle;
    max-width: 100%;
}
#wpcrlResetPasswordSection h3, .bg-primary, .content-area h1, .fv-form h3, .woocommerce h2, .woocommerce-checkout h3 {
    color: #444 !important;
    font-weight: 400;
    font-size: 44px;
    font-family: "Yanone Kaffeesatz";
    margin: 0 0 20px;
    padding: 0 0 4px;
    text-transform: capitalize;
    background:none !important;
}
#wpcrlResetPasswordSection h3:after, .bg-primary:after, .content-area h1:after, .fv-form h3:after, .woocommerce h2:after, .woocommerce-checkout h3:after {
    display: block;
    margin: 5px 0 0;
    padding: 0;
    width: 9%;
    content: '';
    height: 3px;
    background: #34b6e6;
}	

#registration-form .form-group, #title, .add_physicin .col-md-1.no_padding {
    text-align: left;
}

.form-group, .view_form-group {
    width: 100%;
    margin: 15px auto;
    display: inline-block;
}
label {
    width: 20%;
    font: 600 13px Lato;
    color: #5c5c5c;
}

.filter_value, label {
    float: left;
    text-align: left;
}
input[type=text], input[type=email], input[type=url], input[type=password], input[type=tel], input[type=number], input[type=search], textarea {
    background: #f7f7f7;
    background-image: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 0));
    border: 1px solid #d1d1d1;
    border-radius: 2px;
    color: #999;
    padding: 8px;
    font-size: 12px!important;
}

.form-control {
    width: 70% !important;
}
.form-control {
    font-size: 1rem;
}
.form-control, .form-control1, .page-id-160 .form-control, .speciman_top .form-control {
    display: inline-block;
    vertical-align: middle;
}

.no_padding, .no_padding1, .no_padding2 {
    padding-right: 50px !important;
    padding-left: 0 !important;
}
.form-group label span {
    color: red;
}
#btnForgotPassword, #place_order, .btn, .button, .reset_button, .single_add_to_cart_button, .wc-proceed-to-checkout .button, input[type=reset], input[type=submit] {
    width: auto;
    background: #34b6e6!important;
    font-weight: 300 !important;
    font-family: "Yanone Kaffeesatz"!important;
    font-size: 18px;
    border-radius: 2px;
    border: 0;
    padding: 4px 15px!important;
    color: #fff!important;
    text-transform: uppercase;
    display: inline-block;
    vertical-align: top;
    height: 33px;
}
.clear {
    clear: both;
}

.clear, .n_clear {
    clear: both;
}
.footer_btm {
    background: #525252;
    color: #fff;
    padding: 30px 0;
}

.ftr_contact_info p {
    margin: 0;
    padding: 0;
    color: #b5b5b5;
    font: 400 16px Lato;
}
#text-7 p a {
    color: #b5b5b5;
    clear: both;
}
.survey_button {
    margin: 0 auto;
}

button, button[disabled]:focus, button[disabled]:hover, input[type=button], input[type=button][disabled]:focus, input[type=button][disabled]:hover, input[type=reset], input[type=reset][disabled]:focus, input[type=reset][disabled]:hover, input[type=submit], input[type=submit][disabled]:focus, input[type=submit][disabled]:hover {
    background: #1a1a1a;
    border: 0;
    border-radius: 2px;
    color: #fff !important;
    font-family: Lato, "Helvetica Neue", sans-serif;
    font-weight: 700;
    letter-spacing: .046875em;
    line-height: 1;
    padding:.30em .875em .60em !important;
    text-transform: uppercase;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.fa-ticket:before {
    content: "\f145";
}
.comment-content>:last-child, .entry-content>:last-child, .entry-summary>:last-child, .page-content>:last-child, .textwidget>:last-child, body:not(.search-results) .entry-summary>:last-child {
    margin-bottom: 0;
}

address, p {
    margin: 0 0 1.75em;
}
#text-7, #text-6 {
    display: inline-block;
    margin-right: 18px;
}
#text-6 p {
    clear: both;
    padding-top: 12px;
}
.ftr_tgline h2 {
    color: #525252;
    font-weight: 400;
    font-family: "Yanone Kaffeesatz";
    font-size: 30px;
    line-height: 30px;
    margin: 0;
    padding: 0;
}
.ftr_tgline {
    padding: 36px 0;
}
.scl_cntct, .top_hdr ul li, .top_hdr ul li .sub-menu:hover li ul li {
    position: relative;
}
.footer_top .contact_us {
    padding: 40px 0 0;
}
.footer_top .contact_us a {
    background:url('cntct_btn.png');
    font: 400 22px/44px Lato;
    text-transform: uppercase;
    color: #fff;
    display: inline-block;
    min-height: 50px;
    padding: 0;
    min-width: 228px;
    text-align: center;
}
.footer_top {
    background: url('ftr_tp_bg.jpg');
    overflow: hidden;
}

.inner_banner {
    min-height: 350px;
    background-size: cover!important;
    margin-bottom: 30px;
}

/**mycss**/
.main_mnu {
    padding: 10px 0 0;
    text-align: right;
}
.hdr_btm {
    padding: 10px 0 10px;
    margin: 0;
    display: block;
}
.main_mnu ul li {
    margin: 0 0 0 15px;
    padding: 0;
    display: inline-block;
    vertical-align: top;
}
.main_mnu ul li:first-child{
    margin:0;
}
.main_mnu ul li a{
    color: #777;
    font: 400 20px/30px Lato;
    text-decoration: none;
    cursor: pointer;
}	
.top_nav_menu ul li:first-child a:before {
    content: '\f023';
}
.top_nav_menu ul li:last-child a:before {
    content: '\f08b';
}
.top_nav_menu ul li:last-child a{
  color: #34b6e6;
}
.top_nav_menu {
    padding-top: 5px;
}
.footer-txt {
    margin-top: 5px;
}
.ftr_contact_info p.cpy_right {
    margin: 0;
    padding: 0;
    color: #b5b5b5;
    font: 400 16px Lato;
}
.survey_button {
    background: #1a1a1a;
    border: 0;
    border-radius: 2px;
    color: #fff;
    font-family: Lato,"Helvetica Neue",sans-serif;
    font-weight: 700;
    letter-spacing: .046875em;
    line-height: 1;
    padding: .84375em .875em .78125em !important;
    text-transform: uppercase;
}
.footer_top .contact_us {
    padding: 40px 0 0;
}
.footer_top .contact_us a {
    background: url(cntct_btn.bdea638….png) no-repeat;
    font: 400 22px/44px Lato;
    text-transform: uppercase;
    color: #fff;
    display: inline-block;
    min-height: 50px;
    padding: 0;
    min-width: 228px;
    text-align: center;
}
.footer_top {
    background: url(ftr_tp_bg.51ac1f1….jpg) center top no-repeat;
    overflow: hidden;
}
input[type="submit"].updtntm {
    line-height: 27px;
    width: auto ;
    background: #34b6e6;
    font-weight: 300;
    font-size: 17px;
    font-family: "Yanone Kaffeesatz";
    border-radius: 2px ;
    border: 0 ;
    padding: 6px 15px;
    color: #fff;
    text-transform: uppercase;
    display: inline-block ;
    vertical-align: top;
    height: 33px ;
    letter-spacing: 0;
}
#registration-form textarea {
    background: #f7f7f7;
    border-radius: 2px;
}
#registration-form select {
    font-size: 10px;
    color: #999;
}
.error{
    color:#FF0000;
    width: 100%;
}
.form-group {
    position: relative;
}
.form-group label.error {
    position: absolute;
    bottom: -23px;
    left: 101px;
}
#menu-main-menu {
    margin-left: 0px;
    padding-left: 0;
}
.top_hdr ul {
    margin: 0;
}
.main_mnu ul li a {
    color: #777;
    font: 400 19px/30px Lato;
    text-decoration: none;
    cursor: pointer;
}
</style>
</head>

<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
<div class="top_hdr">
            	<div class="container">
                	<div class="row">
                      <div class="col-md-3 col-sm-4">
                        	<div class="logo"><a href="<?php echo SITEURL; ?>" class="custom-logo-link" rel="home" itemprop="url"><img width="371" height="58" src="<?php echo base_url('assets/frontend/main_images/logo.png'); ?>" class="custom-logo" alt="Ability Diagnostics" itemprop="logo" srcset="<?php echo base_url('assets/frontend/main_images/logo.png'); ?> 371w, <?php echo base_url('assets/frontend/main_images/logo-300x47'); ?>.png 300w" sizes="(max-width: 371px) 85vw, 371px"></a></div>
                      </div> 
	                  <div class="col-md-9 right_top_menu">
	                    <div class="top_nav_menu">
	                    	<div class="menu-top-header-menu-container"><ul id="menu-top-header-menu" class="top_menu"><li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="<?php echo SITEURL.'login'; ?>">Portal Login</a></li>
<li id="menu-item-461" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-461"><a href="<?php echo base_url('pay'); ?>">Make a Payment</a></li>
</ul></div>						    						</div>
						</div>
                     </div>
            	 </div>
                </div>
                <div class="hdr_btm">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-4"></div>
          <div class="col-md-5 col-sm-8">
            <div class="main_mnu">
              <div class="menu-main-menu-container">
                <ul id="menu-main-menu" class="primary-menu">
                  <li
                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item">
                    <a href="<?php echo SITEURL.'home'; ?>">Home</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page">
                    <a href="<?php echo SITEURL.'about'; ?>">About</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page">

                    <a href="<?php echo SITEURL.'services'; ?>">Nail Fungus</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page">

                    <a href="<?php echo SITEURL.'wound-care'; ?>">Wound Care</a></li>

                  <li class="menu-item menu-item-type-post_type menu-item-object-page">

                    <a href="<?php echo SITEURL.'contact'; ?>">Contact</a></li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div id="content" class="site-content">
  <div class="inner_banner" style="background: url('<?php echo base_url('assets/frontend/main_images/inner_banner.jpg'); ?>') no-repeat;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="slider_cntent"></div>
        </div>
      </div>
    </div>
  </div>
<div class="content-wrap">
   <div class="container">

   <div id="primary" class="content-area col-md-12">
     
      <div id="registration-form" class="form">
    
		 <form name="pay" method="post" action="<?php echo base_url(); ?>pay" id="pay" >
         <div class="bg-primary heading">Pay Online</div>
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>First Name :<span>*</span></label>
               <input type="text" name="First_Name" id="CustRefID1"  value="" placeholder="First Name" class="form-control"/>
             </div>
          </div>
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Last Name :<span>*</span></label>
              <input type="text" name="Last_Name" id="CustRefID2"  class="form-control" value="" placeholder="Last Name"/>
            </div>
          </div>   
          
            <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Email :<span>*</span></label>
              <input type="email" name="Email" class="form-control" id="Email" value="" placeholder="Email">
            </div>
          </div>
        
        <div class="col-md-6 no_padding">
            <div class="form-group">
            <label>State : </label>
            <select id="State" name="State" class="form-control">
	            <option value="">-Select-</option>
				<option value="AK">AK</option>
				<option value="AL">AL</option>
				<option value="AR">AR</option>
				<option value="AZ">AZ</option>
				<option value="CA">CA</option>
				<option value="CO">CO</option>
				<option value="CT">CT</option>
				<option value="DC">DC</option>
				<option value="DE">DE</option>
				<option value="FL">FL</option>
				<option value="GA">GA</option>
				<option value="HI">HI</option>
				<option value="IA">IA</option>
				<option value="ID">ID</option>
				<option value="IL">IL</option>
				<option value="IN">IN</option>
				<option value="KS">KS</option>
				<option value="KY">KY</option>
				<option value="LA">LA</option>
				<option value="MA">MA</option>
				<option value="MD">MD</option>
				<option value="ME">ME</option>
				<option value="MI">MI</option>
				<option value="MN">MN</option>
				<option value="MO">MO</option>
				<option value="MS">MS</option>
				<option value="MT">MT</option>
				<option value="NC">NC</option>
				<option value="ND">ND</option>
				<option value="NE">NE</option>
				<option value="NH">NH</option>
				<option value="NJ">NJ</option>
				<option value="NM">NM</option>
				<option value="NV">NV</option>
				<option value="NY">NY</option>
				<option value="OH">OH</option>
				<option value="OK">OK</option>
				<option value="OR">OR</option>
				<option value="PA">PA</option>
				<option value="RI">RI</option>
				<option value="SC">SC</option>
				<option value="SD">SD</option>
				<option value="TN">TN</option>
				<option value="TX">TX</option>
				<option value="UT">UT</option>
				<option value="VA">VA</option>
				<option value="VT">VT</option>
				<option value="WA">WA</option>
				<option value="WI">WI</option>
				<option value="WV">WV</option>
				<option value="WY">WY</option>
			</select>
            </div>
          </div>  
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Address : </label>
              <textarea name="Address1" class="form-control" id="Address1" placeholder="Address"></textarea>
            </div>
          </div>   
          
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Address 2 : </label>
              <textarea name="Address2" class="form-control" id="Address2" placeholder="Address 2"></textarea>
            </div>
          </div>
            
          
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>City : </label>
              <input type="text" name="City" class="form-control" id="City" value="" placeholder="City">
            </div>
          </div>    
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Zip :<span>*</span></label>
              <input type="text" name="Zip" class="form-control" id="Zip" value="" placeholder="Zip">
            </div>
          </div>  
          
          <div class="col-md-6 no_padding">
            <div class="form-group">
              <label>Phone Number :</label>
              <input type="text" name="PhoneNumber" class="form-control" id="mobile" value="" placeholder="PhoneNumber">
            </div>
          </div> 
          <div class="clear">&nbsp;</div>
           <div class="col-md-2 no_padding1" align="left" style="margin-top:25px;">
             <input type="submit" class="updtntm" name="submit" value="Update Details"/>
            </div> 
          <div class="clear">&nbsp;</div>
        </form>
  	  </div>   
    </div>
  </div>
</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">

  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
  <script src="https://use.fontawesome.com/9aef6104e3.js"></script> 
  <script type="text/javascript">
    $(document).ready(function(){
      $("#pay").validate({
        rules: {
            First_Name: {
                required: true,
            },
            Last_Name: {
                required: true,
            },
            Email: {
                required: true,
                email:true
            },
            Zip: {
                required: true,
                minlength: 5,
                number: true,
            },
            agree: "required"
        },
        messages: {
            First_Name: "Please enter first name.",
            Last_Name: "Please enter last name.",
            Email: "Please enter a valid email.",
            Zip: "Please enter a valid zip.",
            agree: "Please accept our policy"
        },
    });
    });
  </script>
<div class="footer_top">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-9">
            	<section id="text-2" class="ftr_tgline widget_text"><h2 class="widget-title" style="display:none;">Control Drug</h2>			
            	<div class="textwidget"><h2>Laboratory services in Onychomycosis (Nail Fungus) and Wound Care.</h2></div>
		</section>
            </div>
            <div class="col-sm-3">
            	<div class="scl_cntct">
                	<div class="contact_us">
                        <a href="<?php echo SITEURL.'contact'; ?>">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>  
<div class="footer_btm">

  	<div class="container">

    	<div class="row">
        
        	<div class="col-sm-3">
                  <section id="text-5" class="ftr_left widget_text">			<div class="textwidget"><!-- BEGIN LUXSCI HIPAA SEAL CODE --><a target="_blank" href="https://luxsci.com/perl/public/hipaa_seal.pl?verify=16664&amp;auth=$1$r933jtKV$8QCHzdEG3tioh04MHVGh//&amp;type=g&amp;d=23755&amp;c=Ability%20Diagnostics"><img src="<?php echo base_url(); ?>assets/frontend/main_images/Compliant.jpg" width="120" height="90" border="0" alt="LuxSci helps ensure HIPAA-Compliance for email and web services." oncontextmenu="alert('Copying Prohibited by Law - Copyright held by Lux Scientiae, Incorporated'); return false;"></a>
<div class="footer-txt"><a target="_blank" style="font-size: 10px; text-decoration: none; color:#888; font-family: Arial, Helvetica,sans-serif" href="http://luxsci.com">Secure Internet Services
CLIA and CAP certified.</a></div><!-- END LUXSCI HIPAA SEAL CODE --></div>
		</section>            
            </div>

        	<div class="col-sm-6">

            	<section id="text-3" class="ftr_contact_info widget_text"><h2 class="widget-title" style="display:none;">Contact Info &amp; Copyright</h2>			<div class="textwidget"><p>858 South Auto Mall Road, Suite 102, American Fork, UT 84003</p>
<p>Phone: 801-899-3828<br>
Fax: 801-855-7548</p>
<p class="cpy_right">Copyright © 2018 Ability Diagnostics - All Rights Reserved</p>
</div>
		</section>
            </div>

            <div class="col-sm-3">

            	<section id="widget_sp_image-2" class="ftr_logo widget_sp_image"><h2 class="widget-title" style="display:none;">logo</h2><img width="292" height="46" alt="logo" class="attachment-full alignright" style="max-width: 100%;" src="<?php echo base_url('assets/frontend/main_images/ftr_logo.png'); ?>"></section><section id="text-7" class="ftr_logo widget_text">			<div class="textwidget"><p><a href="https://abilitydiagnostics.com/privacy-policy/" rel="noopener">Privacy Policy</a></p>
</div>
		</section><section id="text-6" class="ftr_logo widget_text">			<div class="textwidget"><p><a href="<?php echo SITEURL.'survey'; ?>" rel="noopener"><button class="survey_button" type="button"><i class="fa fa-ticket" aria-hidden="true"></i> Survey</button></a></p>
</div>
		</section>				
            </div>

        </div>

    </div>

  </div> 
 </footer>

 </body>
</html>