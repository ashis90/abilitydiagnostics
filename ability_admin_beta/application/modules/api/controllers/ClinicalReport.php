<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class ClinicalReport extends MY_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->library('m_pdf');
       date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {    	    	

	$str = 'The specimen consists of multiple pieces of yellow/brown nail measuring 20x15x3mm in aggregate.50% in 1 cassette.';
	preg_match_all('!\d+!', $str, $matches);
$gross = '';
	for($i=0; $i<3; $i++){
		
		$gross .= $matches[0][$i].'*';
		
	 }
    echo rtrim( $gross, '*');
    }
    
         
    function get_clinical_data(){   
        $histo = "No Histo Report Generated";
        $pcr = "No PCR Report Generated";
        $pcr_data = "N/A";
        $agg = "N/A";
        $fungus = "N/A";
        $yeast = "N/A";
        $bacteria = "N/A";
        $rese = "N/A";
        $pcr_code="N/A";
        $details = array();
        $data = json_decode(file_get_contents('php://input'), true); 
        $search="";
        $user_id = intval($data['physician_id']);
        $tbl1=SPECIMEN_MAIN;
        $tbl2=CLINICAL_INFO_MAIN;
        $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
        $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl5=GENERATE_PCR_REPORT_MAIN;
        $tbl6=ASSIGN_STAGE_MAIN;
        $tbl7=IMPORT_DATA_MAIN;
        if($data['dataType'] == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=CLINICAL_INFO_MAIN;
            $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
            $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl5=GENERATE_PCR_REPORT_MAIN;
            $tbl6=ASSIGN_STAGE_MAIN;
            $tbl7=IMPORT_DATA_MAIN;
        }else if($data['dataType'] == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=CLINICAL_INFO_ARCHIVE;
            $tbl3=SPECIMEN_STAGE_DETAILS_ARCHIVE;
            $tbl4=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl5=GENERATE_PCR_REPORT_ARCHIVE;
            $tbl6=ASSIGN_STAGE_ARCHIVE;
            $tbl7=IMPORT_DATA_ARCHIVE;
        }
        if(count(array_filter($data)) != 0){
        if(($data['from_date']) && ($data['to_date'])) 
        {
            $search .= "create_date BETWEEN '" .$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' AND";
        }
          

         
          $new_pathology_sql = "SELECT `id`,`lab_id`,`patient_id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id`,`patient_phn`,`patient_dob` FROM $tbl1 WHERE status = 0 AND `physician_accepct` = '0' AND `qc_check` = '0' AND ".$search." `physician_id`='".$data['physician_id']."' AND (`id` IN (SELECT `specimen_id` from $tbl4) OR `assessioning_num` IN (SELECT `accessioning_num` from $tbl5))";
          $new_pathology_results = $this->BlankModel->customquery($new_pathology_sql);
          $total_count    = count($new_pathology_results);


          if($data['physician_id'])
          {
            $fname    = get_user_meta_value( $user_id, 'first_name');				
            $lname    = get_user_meta_value( $user_id, 'last_name');
            $add      = get_user_meta_value( $user_id, '_address');          
            $phy_name = $fname." ".$lname;         
          }
          else
          {
            $fname    = "";				
            $lname    = "";
            $add      = "";
            $phy_name = "";
          }
          if($new_pathology_results)
          {
          	 
            foreach($new_pathology_results as $report)
            {
               
                $pname       = $report['p_firstname'].' '.$report['p_lastname']; 
                $acc_no      = $report['assessioning_num'];
                $report_date = "SELECT `dor`,`gross_description`,`diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."'";
                $report_results  = $this->BlankModel->customquery($report_date);
                
                $pcr_report_date = "SELECT `created_date` FROM $tbl5 WHERE `accessioning_num`='".$report['assessioning_num']."'";
                $pcr_report_results = $this->BlankModel->customquery($pcr_report_date);
                
                $gross = "";
                if(!empty($report_results) || !empty($pcr_report_results))
                {
                  if(!empty($report_results)) 
                  {
                      $histo = "Histo: ".date('m-d-Y',strtotime($report_results[0]['dor'])); 
                  }
                  else 
                  { 
                      $histo = "No Histo Report Generated";
                  }
                  if(!empty($pcr_report_results)) 
                  {
                      $pcr = "PCR: ".date('m-d-Y',strtotime($pcr_report_results[0]['created_date'])); 
                  }
                  else 
                  { 
                      $pcr =  "No PCR Report Generated";
                  }
                 
                  if(!empty($report_results)){
					$gross_description = $report_results[0]['gross_description'];
					if($gross_description){			
					$str = $gross_description; 
					$x_postion = strpos($str,"x");	 
					if(!empty($x_postion)){
					$get_data_from = $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
					$size_data     = substr($str, $get_data_from, 10);
					}
					else{	
					$postion = strpos($str, "*"); 	
					if(!empty($postion)){
					$get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
					$size_data = substr($str, $get_data_from, 10);
					}
					}						
					preg_match_all('!\d+!', $size_data, $matches);
					$generate_gross = '';
					for($i=0; $i<3; $i++){
					$generate_gross .= $matches[0][$i].'x';
					}
					$gross = rtrim( $generate_gross, 'x')." mm"; 				
					}                  
                  }
                  
                  else{
                      $gross = "";
                  }
                  if(!empty($report_results)){
                    $pos = strpos($report_results[0]['gross_description'], "Aggregate");
                    if ($pos != false){
                        $agg = "Aggregate";
                    }
                    else{
                        $agg="";
                    } 
                  }
                  else{
                      $agg="";
                  }

                  $fungus_sql = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '665%'";
                  $fungus_results = $this->BlankModel->customquery($fungus_sql);

                  $fungus_sql_660 = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '660%'";
                  $fungus_results_660 = $this->BlankModel->customquery($fungus_sql_660);

                  $fungus_sql_661 = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '661%'";
                  $fungus_results_661 = $this->BlankModel->customquery($fungus_sql_661);

                  $fungus_sql_667 = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '667%'";
                  $fungus_results_667 = $this->BlankModel->customquery($fungus_sql_667);

                  $fungus_sql_530 = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '530%'";
                  $fungus_results_530 = $this->BlankModel->customquery($fungus_sql_530);

                  $fungus_sql_987 = "SELECT `diagnostic_short_code` FROM $tbl4 WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '987%'";
                  $fungus_results_987 = $this->BlankModel->customquery($fungus_sql_987);

                  if(!empty($fungus_results)){ 
                    $fungus = "Present"; 
                  }
                  else if(!empty($fungus_results_660) || !empty($fungus_results_661) || !empty($fungus_results_667) || !empty($fungus_results_530) || !empty($fungus_results_987)){
                    $fungus = " "; 
                  }
                  else{
                    $fungus = "N/A"; 
                  }
                  if(!empty($report_results[0]['diagnostic_short_code'])){
                    if($report_results[0]['diagnostic_short_code'] == 668){
                        $yeast = "Present"; 
                    }
                    else if(!empty($fungus_results_660) || !empty($fungus_results_661) || !empty($fungus_results_667) || !empty($fungus_results_530) || !empty($fungus_results_987)){
                        $yeast = " "; 
                    }
                    else{
                        $yeast = "N/A"; 
                    }
                    if($report_results[0]['diagnostic_short_code'] == 667 || $report_results[0]['diagnostic_short_code'] == 667.5 || $report_results[0]['diagnostic_short_code'] == 'CBI' || $report_results[0]['diagnostic_short_code'] == 'CBIF' || $report_results[0]['diagnostic_short_code'] == 'epb' || $report_results[0]['diagnostic_short_code'] == 'bpp' || $report_results[0]['diagnostic_short_code'] == 7.5 || $report_results[0]['diagnostic_short_code'] == 'TI'){
                      $bacteria = "Infection";
                    }
                      
                  else if($report_results[0]['diagnostic_short_code'] == 667.1 || $report_results[0]['diagnostic_short_code'] == 667.2 || $report_results[0]['diagnostic_short_code'] == 7.1){
                      $bacteria = "Colonization";
                    }
                    else{
                      $bacteria = " ";
                    }

                  }
                
                  $search_sql = "SELECT `target_name`,`positive_negtaive` FROM $tbl7 WHERE `accessioning_num`='".$report['assessioning_num']."'";
                  $search_sql_results = $this->BlankModel->customquery($search_sql);

                  $import_sql = "SELECT `target_name`,`positive_negtaive` FROM $tbl7 WHERE `positive_negtaive`='positive' AND `accessioning_num`='".$report['assessioning_num']."' AND `target_name` NOT IN('Pa04230908_s1','MRSA_Pa04230908_s1','mecA_Pa04230908_s1','Xeno_Ac00010014_a1','Ac00010014_a1','C. albicans_ Fn04646233_s1','Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Fn04646221_s1')";
                  $import_results = $this->BlankModel->customquery($import_sql);
                    if(!empty($import_results) && !empty($search_sql_results)){
                        $pcr_code = "";	
                    foreach($import_results as $value){
                        $name = explode("_",$value['target_name']);  
                    if(count($name)==1){
                        $target_name = $name[0]; 
                    }
                    else if(count($name)==2){
                        $target_name = $name[0]; 
                    }
                    else if(count($name)==3){
                        $target_name = trim($name[0]);
                    } 
                    $pcr_code .= $target_name.'<br/>';
                    }
                    }
                    else if(empty($import_results) && !empty($search_sql_results)){
                        $pcr_code = " ";
                    }
                    else{
                        $pcr_code = "N/A";
                    }
                    $gene_sql = "SELECT `target_name`,`positive_negtaive` FROM $tbl7 WHERE `accessioning_num`='".$report['assessioning_num']."' AND `target_name` IN('Pa04230908_s1','MRSA_Pa04230908_s1','mecA_Pa04230908_s1')";
                    $gene_results = $this->BlankModel->customquery($gene_sql);
                    if(!empty($gene_results)){
                    if($gene_results[0]['positive_negtaive'] == 'positive')
                    {
                      $rese = "Detected";
                    }
                    else if($gene_results[0]['positive_negtaive'] == 'negative'){
                      $rese =" ";
                    }
                    else{
                        $rese = "N/A";
                    } 
                    }
                    else{
                        $rese="N/A";
                    }    
                }
                
                $information = array('patient_name'=>$pname,'acc_no'=>$acc_no,'hdor'=>$histo,'pdor'=>$pcr,
                'size'=>$gross,'pos'=>$agg,'fungus'=>$fungus,'yeast'=>$yeast,'bacteria'=>$bacteria,'pcr'=>$pcr_code,'rese'=>$rese);      
                array_push($details,$information);
            }
              
          }
           if(!empty($information)){
          die(json_encode(array('status' => '1', 'phy_id' => $data['physician_id'], 'count' => $total_count, 'name' => $phy_name,'address' => $add, 'from_date' => $data['from_date'], 'to_date' => $data['to_date'], 'details' => $details)));
          }
          else{
            die(json_encode(array('status' => '0')));
          }
        }
        else{
          die(json_encode(array('status' => '0')));
        }
   }

    function clinic_report_generate(){
        $fname     = $this->input->post('fname');
        $file_data = $this->input->post('file_data');
        $from_date = $this->input->post('from_date');
        $to_date   = $this->input->post('to_date');
        $physician_id = $this->input->post('physician_id');
        $comment      = $this->input->post('comment');
           
        if($_FILES && $_FILES['file_data']['name'])
        {
			$config['upload_path'] = 'assets/uploads/clinical_image';
			$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|jpg|jpeg|png';
			$config['max_size'] = 5000000;

			$this->load->library('upload',$config);
            if(!$this->upload->do_upload('file_data'))
            {
				die( json_encode(array( "status" => '0', 'message' => $this->upload->display_errors())));
            }
            else
            {
				$uploadData = $this->upload->data();
				$fileName = $uploadData['file_name'];
				$this->db->where('id', '1');
                $updateData = $this->db->update('wp_abd_clinic_comment', array('physician_id' => $physician_id,'comment'=>$comment,'diagnostic_image'=>$fileName));
                
                // PDF generate //

                if(($from_date) && ($to_date)) 
                {
                    $search .= "create_date BETWEEN '" .$from_date." 00:00:00' AND '".$to_date." 23:59:59' AND";
                }
  
                $new_pathology_sql = "SELECT `id`,`lab_id`,`patient_id`,`p_lastname`,`p_firstname`,`assessioning_num`,`physician_id`,`patient_phn`,`patient_dob` FROM `wp_abd_specimen` WHERE status = 0 AND `physician_accepct` = '0' AND `qc_check` = '0' AND ".$search." `physician_id`='".$physician_id."' AND (`id` IN (SELECT `specimen_id` from `wp_abd_nail_pathology_report`) OR `assessioning_num` IN (SELECT `accessioning_num` from `wp_abd_generated_pcr_reports`))";
               
                $new_pathology_results = $this->BlankModel->customquery($new_pathology_sql);
                $total_count    = count($new_pathology_results);
            
                $user_id = $physician_id;	
                if($user_id)
                {
                    $first_name = get_user_meta( $user_id, 'first_name');				
                    $last_name  = get_user_meta( $user_id, 'last_name');
                    $address    = get_user_meta( $user_id, '_address');
                    $fname      = $first_name['meta_value'];
                    $lname      = $last_name['meta_value'];
                    $phy_name   = $fname." ".$lname;
                    $add        = $address['meta_value'];
                }
                else
                {
                    $fname = "";				
                    $lname  = "";
                    $add  = "";
                    $phy_name ="";
                }
                date_default_timezone_set('MST7MDT');
                $pdfHeaderfile = $this->load->view('clinical_report','',true);
                $pdfTotalBody = $pdfHeaderfile;
                $table_data = "";
            $table_data.= "<table  cellspacing='0' width='100%' border-spacing='1' style='font-family:Arial, Helvetica, sans-serif;border: 1px solid #d1d1d1;'>
            <thead>
                <tr style='background: rgb(141,179,225);'>
                    <th width='14%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Patient Name</th>
                    <th width='22%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Report Date</th>
                    <th width='13%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Accessioning Number</th>
                    <th width='15%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Specimen Size</th>
                    <th width='20%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Histo DX</th>
                    <th width='10%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>PCR</th>
                    <th width='8%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Methicillin Resistance</th>
                </tr>
                <tr>
                    <th width='14%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'></th>
                    <th width='22%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;text-align:center;'></th>
                    <th width='13%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'></th>
                    <th width='10%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'></th>
                    <th width='22%' style='padding: 0px;font-size: 11px;font-weight:normal;border-left:1px solid #d1d1d1;border-right:1px solid #d1d1d1;'>
                    <table style='margin:0;padding:0;'>
                        <tr>
                            <td style='width:30%;float:left;font-weight: 400;font-size:11px;border-bottom: none;text-align:center;'>Fungus</td>
                            <td style='width:40%;float:left;font-weight: 400;font-size:11px;border-bottom: none;border-left:1px solid #d1d1d1;text-align:center;'>Yeast</td>
                            <td style='width:30%;float:left;font-weight: 400;font-size:11px;border-bottom: none;border-left:1px solid #d1d1d1;text-align:center;'>Bacteria</td>
                        </tr>    
                    </table>
                    </th>
                    <th width='10%'></th>
                    <th width='8%'></th>
                </tr>
            </thead>
            <tfoot>
                <tr style='background: rgb(141,179,225);'>
                    <th width='14%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Patient Name</th>
                    <th width='14%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Report Date</th>
                    <th width='13%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Accessioning Number</th>
                    <th width='10%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Specimen Size</th>
                     <th width='25%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Histo DX</th>
                     <th width='12%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>PCR</th>
                    <th width='10%' style='padding: 8px;color: #fff;font-size: 11px;font-weight:normal;'>Methicillin Resistance</th>
                </tr>
               
            </tfoot>
            <tbody>";

            if($new_pathology_results) 
            {
                foreach($new_pathology_results as $report)
                { 

                    $pname=  $report['p_firstname'].' '.$report['p_lastname']; 
                    $acc_no = $report['assessioning_num'];
                    $report_date = "SELECT `dor`,`gross_description`,`diagnostic_short_code` FROM wp_abd_nail_pathology_report WHERE `specimen_id`='".$report['id']."'";
                    $report_results = $this->BlankModel->customquery($report_date);
                    $pcr_report_date = "SELECT `created_date` FROM wp_abd_generated_pcr_reports WHERE `accessioning_num`='".$report['assessioning_num']."'";
                    $pcr_report_results = $this->BlankModel->customquery($pcr_report_date);
                    $gross = "";
                    $histo = "";
                    $pcr = "";
                    $size = "";
                    $agre = "";
                    if(!empty($report_results) || !empty($pcr_report_results))
                    {
                    if(!empty($report_results)) 
                    {
                        $histo = "Histo: ".date('m-d-Y',strtotime($report_results[0]['dor'])).'<br>'; 
                    }
                    else 
                    { 
                        $histo = "No Histo Report Generated".'<br>'; 
                    }
                    if(!empty($pcr_report_results)) 
                    {
                        $pcr = "PCR: ".date('m-d-Y',strtotime($pcr_report_results[0]['created_date'])); 
                    }
                    else 
                    { 
                        $pcr =  "No PCR Report Generated";
                    }
                   /* if(!empty($report_results))
                    {
                        
                    preg_match_all('!\d+!', $report_results[0]['gross_description'], $matches);
					$generate_gross = '';
					for($i=0; $i<3; $i++){
					$generate_gross .= $matches[0][$i].'*';
					}
					$size = rtrim( $generate_gross, '*')." mm"; 	
                        
                    }
                    */                    
                    if(!empty($report_results)){
						$gross_description = $report_results[0]['gross_description'];
						if($gross_description){			
						$str = $gross_description; 
						$x_postion = strpos($str,"x");	 
						if(!empty($x_postion)){
						$get_data_from = $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
						$size_data     = substr($str, $get_data_from, 10);
						}
						else{	
						$postion = strpos($str, "*"); 	
						if(!empty($postion)){
						$get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
						$size_data = substr($str, $get_data_from, 10);
						}
						}						
						preg_match_all('!\d+!', $size_data, $matches);
						$generate_gross = '';
						for($i=0; $i<3; $i++){
						$generate_gross .= $matches[0][$i].'x';
						}
						$size = rtrim( $generate_gross, 'x')." mm"; 				
						}                  
                     } 
                    else{
                        $size = "";
                     }
                    if(!empty($report_results)){
                        $pos = strpos($report_results[0]['gross_description'], "Aggregate");
                        if ($pos != false){
                            $aggre = "Aggregate";
                        }
                        else{
                            $aggre = "";
                        } 
                    }
                    else{
                        $aggre = "";
                    }

                    $fungus_sql = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '665%'";
                    $fungus_results = $this->BlankModel->customquery($fungus_sql);

                    $fungus_sql_660 = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '660%'";
                    $fungus_results_660 = $this->BlankModel->customquery($fungus_sql_660);

                    $fungus_sql_661 = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '661%'";
                    $fungus_results_661 = $this->BlankModel->customquery($fungus_sql_661);

                    $fungus_sql_667 = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '667%'";
                    $fungus_results_667 = $this->BlankModel->customquery($fungus_sql_667);

                    $fungus_sql_530 = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '530%'";
                    $fungus_results_530 = $this->BlankModel->customquery($fungus_sql_530);

                    $fungus_sql_987 = "SELECT `diagnostic_short_code` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`='".$report['id']."' AND `diagnostic_short_code` LIKE '987%'";
                    $fungus_results_987 = $this->BlankModel->customquery($fungus_sql_987);

                    if(!empty($fungus_results)) 
                    { 
                        $first_data = "Present"; 
                    }
                    else if(!empty($fungus_results_660) || !empty($fungus_results_661) || !empty($fungus_results_667) || !empty($fungus_results_530) || !empty($fungus_results_987))
                    {
                        $first_data = " "; 
                    }
                    else
                    {
                        $first_data = "N/A"; 
                    }
                    if(!empty($report_results[0]['diagnostic_short_code']))
                    {
                        if($report_results[0]['diagnostic_short_code'] == 668) 
                        {
                            $second_data = "Present"; 
                        }
                        else if(!empty($fungus_results_660) || !empty($fungus_results_661) || !empty($fungus_results_667) || !empty($fungus_results_530) || !empty($fungus_results_987))
                        {
                            $second_data = " "; 
                        }
                        else
                        {
                            $second_data = "N/A"; 
                        }
                        if($report_results[0]['diagnostic_short_code'] == 667 || $report_results[0]['diagnostic_short_code'] == 667.5 || $report_results[0]['diagnostic_short_code'] == 'CBI' || $report_results[0]['diagnostic_short_code'] == 'CBIF' || $report_results[0]['diagnostic_short_code'] == 'epb' || $report_results[0]['diagnostic_short_code'] == 'bpp' || $report_results[0]['diagnostic_short_code'] == 7.5 || $report_results[0]['diagnostic_short_code'] == 'TI')

                        {
                        $third_data = "Infection";
                        }
                        
                    else if($report_results[0]['diagnostic_short_code'] == 667.1 || $report_results[0]['diagnostic_short_code'] == 667.2 || $report_results[0]['diagnostic_short_code'] == 7.1)
                        {
                        $third_data = "Colonization";
                        }
                        else
                        {
                        $third_data = " ";
                        }

                    }
                    
                    $search_sql = "SELECT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num`='".$report['assessioning_num']."'";
                    $search_sql_results = $this->BlankModel->customquery($search_sql);

                    $import_sql = "SELECT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `positive_negtaive`='positive' AND `accessioning_num`='".$report['assessioning_num']."' AND `target_name` NOT IN('Pa04230908_s1','MRSA_Pa04230908_s1','mecA_Pa04230908_s1','Xeno_Ac00010014_a1','Ac00010014_a1','C. albicans_ Fn04646233_s1','Fn04646233_s1','C. parapsilosis_Fn04646221_s1','Fn04646221_s1')";
                    $import_results = $this->BlankModel->customquery($import_sql); 
                        if(!empty($import_results) && !empty($search_sql_results))
                        {
                        $new_value = "";	
                        foreach($import_results as $value)
                        {
                        $tname = explode("_",$value['target_name']);
                        if(count($tname)==1) 
                        {
                            $target_name = $tname[0]; 
                        }
                        else if(count($tname)==2)  
                        {
                            $target_name = $tname[0]; 
                        }
                        else if(count($tname)==3)
                        {
                            $target_name = trim($tname[0]);
                        } 
                         $new_value.= $target_name.'<br/>';
                        }
                        }
                        else if(empty($import_results) && !empty($search_sql_results))
                        {
                            $new_value = " ";
                        }
                        else 
                        {
                            $new_value = "N/A";
                        }
                        
                        $gene_sql = "SELECT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num`='".$report['assessioning_num']."' AND `target_name` IN('Pa04230908_s1','MRSA_Pa04230908_s1','mecA_Pa04230908_s1')";
                        $gene_results = $this->BlankModel->customquery($gene_sql);
                        if(!empty($gene_results))
                        {
                        if($gene_results[0]['positive_negtaive'] == 'positive')
                        {
                        $gene = "Detected";
                        }
                        else if($gene_results[0]['positive_negtaive'] == 'negative')
                        {
                        $gene =" ";
                        }
                        else
                        {
                            $gene = "N/A";
                        } 
                        }
                        else
                        {
                            $gene="N/A";
                        }
        
                    }

          
                    $table_data.= "<tr>
                    
                
                    <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$report['p_firstname'].' '.$report['p_lastname']."
                     
                    </td>
                     <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$histo.$pcr."
                  
                    </td>  
                   <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$report['assessioning_num']."
                   
                    </td>

                    <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$size.$aggre."
                        
                      </td>

                    <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>
                     <table style='width100%;'>
                        <tr>
                            <td style='font-size:11px;padding-left:25px;padding-right:35px;float:left;text-align:center;'>".$first_data."</td>
                            <td style='font-size:11px;padding-right:35px;text-align:center;%;float:left;text-align:center;'>".$second_data."</td>
                            <td style='font-size:11px;padding-right:20px;float:left;text-align:center;'>".$third_data."</td>
                        </tr>    
                    </table>
    
                    </td>
                    <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$new_value."</td>
                     <td valign='middle' style='border: 1px solid #d1d1d1;font-size:11px;'>".$gene."</td></tr>";
                }        
            } 
            
            $table_data.= " </tbody></table>";  
            $find=array();
            $find[]="{logo}";
            $find[]="{signature}";
            $find[]="{date}";
            $find[]="{pname}";
            $find[]="{paddress}";
            $find[]="{daterange}";
            $find[]="{count}";
            $find[]="{table_data}";
            $find[]="{comment}";
            $find[]="{diagnosis}";
            $find[]="{dimension}";
            $replace=array();
            $replace[] = base_url()."assets/frontend/images/logo.png";
            $replace[] = base_url()."assets/frontend/images/davidbolick_sign.jpg";
            $replace[] = date('m-d-Y');
            $replace[] = $phy_name;
            $replace[] = $add;
            if(($from_date) && ($to_date))
            {
            $replace[] = date('m-d-Y',strtotime($from_date))." to ".date('m-d-Y',strtotime($to_date));
            }
            else
            {
                $replace[] = "  ";
            }
            $replace[] = $total_count;
            $replace[] = $table_data;
            $replace[] = $comment;
            $replace[] = base_url()."assets/uploads/clinical_image/".$fileName;
            $replace[] = base_url()."assets/frontend/images/diagnostic.jpg";
            for($data_int=0;$data_int<count($find);$data_int++){ 
                $pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
            }
          
            $message = $pdfTotalBody;
            $mpdf = new mPDF('','A4','','', 1, 2, 4, 4, 0, 0); 
            $mpdf->WriteHTML(utf8_encode($message));
            $clinical_pdf = $fname."_".$lname."_Clinician_Report.pdf";
            $mpdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$clinical_pdf,"F");

            $pdf_url = base_url("assets/uploads/pcr_report_pdf/").$clinical_pdf;

            die(json_encode(array("status"=>'1','phy_id'=>$physician_id,'count'=>$total_count,'name'=>$phy_name,'address'=>$add,'from_date'=>$from_date,'to_date'=>$to_date,'pdf_url'=>$pdf_url)));
			}

		}
         
    }
 
}

?>