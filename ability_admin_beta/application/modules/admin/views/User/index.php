<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");



//   foreach($roleList as $rolval) { 
//   echo "<pre>";
//   print_r($rolval['role_shortname']);
// }

//   exit();


  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Users Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Users Management</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?>
          </div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>


            <?php
            if ($this->session->flashdata('user')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('user');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>


          <div class="lidot" id="roleChoose">
            <ul>
              <?php foreach($roleList as $rolval) { 
                 // echo "<pre>";
                 // print_r($rolval);
                 // exit();  
                  ?>
              <li>
                <a class="roles" href="<?php echo site_url('admin/user/index/').$rolval['role_shortname']; ?>">
                  <p id="shows"><?php echo trim(ucwords(str_replace('_', ' ', $rolval['role_name'])))?></p>
                 
                </a>
              </li>
              <?php } ?>

              <?php
                $all = 'all';
              ?>
              <li>
                <a href="<?php echo site_url('admin/user/index/').$all; ?>">
                  All
                  <p></p>
                </a>
              </li>

            </ul>
          </div>


          <div class="box-body">
            <div class="svt">
              <div class="table-responsive">
                <a href="<?php echo base_url('admin/user/addUser');?>" class="btn btn-primary">Add new user</a> <br><br><br>
                <table class="table table-bordered table-hover" id="basicDataTable">
                  <thead>
                    <tr>
                      <th width="8%">Sr No</th>
                      <th width="15%">User Name</th>
                      <th width="15%">Name</th>
                      <th width="25%">Email</th>
                      <th width="25%">Role</th>
                      <th width="25%">Last Login</th>


                      <!-- <div style="display: none;" id="partners_show"><th width="25%">Company</th></div> -->

                      
                          <th width="25%">Partners Company</th>
                          

                      <th width="15%">Email Send Status</th>
                      <th width="12%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($userList): ?>
                    <?php
                      $count= 1;  
                      foreach($userList as $user):
                        // echo get_role($user['role']);
                        // exit();
                      ?>                 
                    <tr class="odd gradeX">
                      <td><?php echo $count;?> </td>
                      <td><?php echo $user['nickname'];?> </td>
                      <td><?php echo $user['firstname']." ".$user['lastname'];?> </td>
                      <td><?php echo $user['user_email'];?> </td>
                      <td>
                      <?php
                        foreach($roleList as $rolval) {  
                          if (($rolval['role_shortname']) == (get_role($user['role']))){ 
                           echo $rolval['role_name'];
                          } 
                        } 
                      ?>
                      </td>

                      <td   <?php $last_login_td = get_user_meta_value($user['ID'],'when_last_login'); ?>
                      data-order="<?php echo $last_login_td; ?>"> 
                      <!-- <td>   -->
                      <?php 
                      $last_login = get_user_meta_value($user['ID'],'when_last_login');
                      if(is_numeric($last_login) && $last_login!=0) 
                      { 
                        echo date('Y-m-d h:i', $last_login); 
                      } 
                      else if(!is_numeric($last_login) && !empty($last_login) && $last_login!=0)
                      {
                        echo date('Y-m-d h:i', strtotime($last_login));
                      }
                      else if(is_numeric($last_login) && $last_login==0)
                      {
                        echo "Never";
                      }
                      else if(!is_numeric($last_login) && empty($last_login))
                      { 
                        echo "Never"; 
                      } 
                      ?>
                      <a onclick="viewUserLog(<?php echo $user['ID'];?>)" class="btn btn-warning edit-btn log-btn">View Log</a>
                      
                      </td>


                      <td>
                        <?php 
                          $partners_company = get_user_meta_value($user['ID'],'partners_company');
                          foreach($partnerList as $partnerval) {  
                            if (($partnerval['partner_id']) == $partners_company){ 
                             echo $partnerval['partner_name'];
                            } 
                          } 
                        ?> 
                      </td>


                      <td>
                      <?php 
                           $email_sent_status = get_user_meta_value($user['ID'],'email_status');
                           if($email_sent_status){ 
                           ?>
                           <div class="title_aa" id="mail_send_status_<?php echo $user['ID'];?>" style="color: #12ed38; font: 400 18px Roboto Condensed,sans-serif;"><?php echo ucfirst($email_sent_status); ?></div>
                          <?php  } else {
                          ?>
                          <div class="title_aa" id="mail_send_status_<?php echo $user['ID'];?>" style="color: #f90000; font: 400 18px Roboto Condensed,sans-serif;"><span class="glyphicon glyphicon-remove"></span></div>
                        <?php }  ?>
                      </td> 
                      <!-- <td></td> -->
                      <td>
                        <a href="<?php echo base_url().'admin/user/userEdit?user_id='.$user['ID'];?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                        <a href="javascript:void(0);" class="btn btn-warning"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>

                        <a href="javascript:void(0)" onclick="javascript:sendMail('<?php echo $user['ID'];?>')" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Send Password</a>

                      </td>
                    </tr>
                    <?php 
                      $count++;  
                      endforeach ;                 
                      endif;
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>


    <!-- Modal -->
    <div class="modal fade" id="logModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Log</h4>
        </div>
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="basicDataTable">
              <thead>
                <tr>
                  <th width="8%">Sr No</th>
                  <th width="15%">User Name</th>
                  <th width="15%">Name</th>
                  <th width="25%">Date</th>
                  <th width="25%">Time</th>
                  <th width="15%">Login From</th>
                </tr>
              </thead>
              <tbody id="log_tbl_row">                
              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>

<script type="text/javascript">

//$(document).ready(function(){
//  $('.log-btn').click(function(){
//
//    var uid = $(this).attr('data-id');
//    alert(uid);
//    $.ajax({
//      url:'<?php echo base_url("admin/user/getUserLogDetails"); ?>',
//      data:{'uid':uid},
//      type: 'POST',
//      dataType:'JSON',
//      success:function(data){
//        //var dt = JSON.parse(data);
//        if(data.status){
//          $('#logModal #log_tbl_row').html(data.html);
//          $('#logModal').modal('show');
//        }
//      },
//      error:function(){
//        alert('failed');
//      }
//
//    });
//
//
//});
//});
    
function viewUserLog(uid)
{
    $.ajax({
      url:'<?php echo base_url("admin/user/getUserLogDetails"); ?>',
      data:{'uid':uid},
      type: 'POST',
      dataType:'JSON',
      success:function(data){
        //var dt = JSON.parse(data);
        if(data.status){
          $('#logModal #log_tbl_row').html(data.html);
          $('#logModal').modal('show');
        }
      },
      error:function(){
        alert('failed');
      }

    });
}
</script>
