import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabAnalyticsComponent } from './lab-analytics.component';

describe('LabAnalyticsComponent', () => {
  let component: LabAnalyticsComponent;
  let fixture: ComponentFixture<LabAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
