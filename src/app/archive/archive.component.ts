import { Component, OnInit } from '@angular/core';
import {ArchiveService } from '../archive.service';
import { SpecimenService } from '../specimen.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';

export interface StateGroup {
  ids: Number[];
  names: string[];
}
@Component({
  selector: 'cm-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {
  datePickerConfig:Partial<BsDatepickerConfig>;
  public data : any;
  list:any = [];
  specimens_list:any = [];
  specimen_data:any = [];
  searchForm: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  physicians_list:any = [];
  physicians_list_data:any = [];

  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  public stateGroups: StateGroup[] = [{
      ids: [],
      names: []
    }]//test
  public searchData: StateGroup[] = [{
      ids: [],
      names: []
    }]
  constructor(
    private http:HttpClient, 
    public itemsService:SpecimenService,
    public archiveService:ArchiveService,
    public route:Router,
    private spinner: NgxSpinnerService
  ) { 
    this.searchForm = new FormGroup({        
      "barcode" : new FormControl(''),
      "physician_name" : new FormControl(''), 
      "physician_id" : new FormControl(''),  
      "assessioning_num" : new FormControl(''),
      "collection_date" : new FormControl(''),
      "p_firstname" : new FormControl(''),
      "p_lastname"  : new FormControl('') 
     });

     this.datePickerConfig = Object.assign({},{
      containerClass:'theme-blue',
      dateInputFormat:'MM/DD/YYYY'
    
    });
  }

  ngOnInit() {
    this.loading = true;
    this.getPhysicians();
    this.getAllPendingSpecimen();
    this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value?value.toLowerCase():''))
    );
  }

  resetForm(){
    this.searchForm.reset();
    this.getAllPendingSpecimen();
  }

  onSubmit()
   {
       if(this.searchForm.status == "VALID")
       {   
           this.spinner.show();
           this.load_status=false;
           this.no_data_status= false;
           this.sendData = { "collection_date":this.convertDate(this.searchForm.value.collection_date),"barcode":this.searchForm.value.barcode,"p_firstname":this.searchForm.value.p_firstname,"p_lastname":this.searchForm.value.p_lastname, "assessioning_num":this.searchForm.value.assessioning_num, "physician":this.searchForm.value.physician_id  };
           this.archiveService.searchSpecimen(this.sendData).subscribe( data=> {
               this.loading = false;
               this.specimens_list = data;
               this.data  =  this.specimens_list.pending_specimens;
                if(this.specimens_list.status ==='1'){
                  this.spinner.hide();
                }
                if(this.specimens_list.status ==='0'){
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
                  this.spinner.hide();
                }
            });
       }
       else
       {

       }
   }

  getAllPendingSpecimen() {
    this.spinner.show();
    this.archiveService.getAllPendingSpecimen().subscribe(data => {          
    this.specimens_list = data;
    this.no_data_status= false;
    if(this.specimens_list.status == '1')
    {
      this.loading = false;
      this.data = this.specimens_list.pending_specimens;
      this.spinner.hide();
    }
    if(this.postsArray.status == '0')
    {
      this.no_data_status= true;
      this.no_data="No Data Found.";
      this.spinner.hide();
    }
               
    });

  }

  getPhysicians()
    {
      this.spinner.show();
      this.itemsService.physicians_list().subscribe(data => {
          
        this.list = data;
        this.physicians_list = this.list.physicians_data;    
        if(this.list['status'] === '1'){
          this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
          this.physicians_list.map((val) => this.allIds.push ( val.id));
          this.physicians_list.map((data) => this.filterVal.push ( data));
          this.spinner.hide();
        }

        if(this.list['status'] === '0'){
          this.spinner.hide();
        }
        
      
      });
      
      this.stateGroups = [{
        ids:this.allIds,
        names:this.allNames
      }];
      
    }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount : number = this.filterVal.length;
        this.options = [];
        this.ids=[];
        for(let i=0;i<itemCount;i++){
            if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ){
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids:this.ids,
          names:this.options
        }];
        
        return this.searchData;
    }
    
    return this.stateGroups;
}

callSomeFunction(phy_id :any){
  this.searchForm.controls['physician_id'].setValue(phy_id);
}

convertDate(str: Date) {
  if(!str){
    var newDate = '';
  }else{
    
    var date = new Date(str),
      mnth = ("0" + (date.getMonth()+1)).slice(-2),
      day  = ("0" + date.getDate()).slice(-2),
      newDate = [ mnth, day, date.getFullYear() ].join("/");
  }
  return newDate;
}

}
