import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSpecimenReportRoutingModule } from './add-specimen-report-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorDisplayModule } from '../../field-error-display/field-error-display.module';

@NgModule({
  imports:      [ CommonModule,AddSpecimenReportRoutingModule,NavbarModule,FormsModule,
    ReactiveFormsModule,FieldErrorDisplayModule ],
  declarations: [ AddSpecimenReportRoutingModule.components ]
})
export class AddSpecimenReportModule { }
