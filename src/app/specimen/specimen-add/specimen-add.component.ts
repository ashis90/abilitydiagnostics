import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { map, startWith } from 'rxjs/operators'; //add this
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { element } from '@angular/core/src/render3';
import { ToastrService } from 'ngx-toastr';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-specimen-add',
  templateUrl: './specimen-add.component.html',
  styleUrls: ['./specimen-add.component.css']
})
export class SpecimenAddComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  public url = environment.baseUrl + 'PdfGenerate/'
  public data: any;
  list: any = [];
  physicians_list: any = [];
  physicians_list_data: any = [];
  add_specimen: any = [];
  addSpecimen: FormGroup;
  public loading = false;
  sendData;
  postsArray: any = [];
  load_status: boolean = true;
  bsValue: Date = new Date();
  // variables
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]//test
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  showProcess: boolean = false;
  relations: Array<String> = ['Self', 'Spouse', 'Dependent'];
  insurance_type: Array<String> = ['Commercial', 'Federal'];
  clinical_specimen: Array<String> = ['Right', 'Left', 'Biopsy', 'Excision'];
  bill: Array<String> = ['Insurance', 'Patient'];
  billData: any = [1, 2];
  nailUnit: Array<String> = ['Combination Testing (PCR and Histopathology - choose stains below)', 'PAS/GMS/FM - Higher Sensitivity and Melanin screen', 'PAS/GMS - Higher Sensitivity', 'Reflex to PCR for identification', 'PCR Only', 'Reflex to Histopathology to confirm presence of disease'];
  nailUnitData: any = [5, 1, 2, 6, 4, 7];
  selectedCheckBoxValue: any = [];
  //selectedClinicalSpecimen: any[][];
  selectedCheckBoxBill: any = [];
  //selectedNailUnit: any[][];
  billErrors: boolean = true;
  billValidCheck: boolean = false;
  nailUnitValidchk: boolean = false;
  nailUnitValid: boolean = false;
  relationValidCheck: boolean = false;
  nailUnitValidMsg: string = "Please check atlest one nail unit.";
  formData: any;
  typesArr: any;
  nailCheck: boolean = true;
  public value: string = "359884123321";
  public mask: string = "(999) 000-00-00-00";
  // variables
  constructor(private http: HttpClient,
    public itemsService: SpecimenService,
    public route: Router,
    private fb: FormBuilder,
    private _ActivatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
  ) {

    this.addSpecimen = this.fb.group({
      user_id: [''],
      collection_date: ['', Validators.required],
      date_received: [this.bsValue, Validators.required],
      p_lastname: ['', Validators.required],
      p_firstname: ['', Validators.required],
      specimen_count: [''],
      format: ['PM'],
      bill: this.addControlsToArray(this.bill),
      billValidCheck: [this.billValidCheck, Validators.requiredTrue],
      patient_address: ['', Validators.required],
      apt: [''],
      patient_city: ['', Validators.required],
      patient_state: ['', Validators.required],
      patient_zip: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(5)]],
      patient_phn: [''],
      patient_dob: ['', Validators.required],
      patient_age: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.min(1)]],
      patient_sex: ['', Validators.required],
      patient_id: [''],
      priSubscriberRelationValidCheck: [this.relationValidCheck, Validators.requiredTrue],
      pri_subscriber_relation: this.addControlsToArray(this.relations),
      pri_insurance_name: ['', Validators.required],
      pri_address: ['', Validators.required],
      pri_city: ['', Validators.required],
      pri_state: ['', Validators.required],
      pri_zip: ['N/A', [Validators.required]],
      insurance_type: ['', Validators.required],
      pri_employee_name: [''],
      pri_member_id: ['', Validators.required],
      pri_subscriber_dob: ['', Validators.required],
      pri_group_contact: ['', Validators.required],
      pri_sex: ['', Validators.required],
      pri_medicare_id: [''],
      pri_madicaid_id: [''],
      clinical_info: ['1'],
      sec_subscriber_relation: this.addControlsToArray(this.relations),
      sec_insurance_name: [''],
      sec_address: [''],
      sec_city: [''],
      sec_state: [''],
      sec_zip: [''],
      sec_employee_name: [''],
      sec_member_id: [''],
      sec_subscriber_dob: [''],
      sec_group_contact: [''],
      sec_sex: [''],
      sec_medicare_id: [''],
      sec_medicaid_id: [''],

      physician_accepct: ['1'],
      time: [''],
      wp_submit: ['Add Specimen'],
      physician_id: ['', Validators.required],
      physician_name: ['', Validators.required],
      test_type: ['', Validators.required],
      process_done: [''],
      specimen: this.fb.array([
        this.addSkillFormGroup(),
      ]),
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'MM/DD/YYYY'

    });
  }

  ngOnInit() {
    this.addSpecimen.controls['user_id'].setValue(localStorage.userid);
    this.spinner.show();
    this.getPhysicians();
    this.getAllTestTypes();
    this.filteredOptions = this.addSpecimen.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value.toLowerCase()))
    );
    //console.log(this.bsValue);
  }

  getAllTestTypes() {
    this.itemsService.getAllTestTypes().subscribe(data => {
      let postData = data;
      this.typesArr = postData['test_types'];
    });
  }

  add_specimen_data(): void {
    (<FormArray>this.addSpecimen.get('specimen')).push(this.addSkillFormGroup());
  }

  delete_specimen_data(spIndex: number): void {
    (<FormArray>this.addSpecimen.get('specimen')).removeAt(spIndex);
  }


  addSkillFormGroup(): FormGroup {
    return this.fb.group({
      assessioning_num: [''],
      clinicalSpecimenValidCheck: [this.nailUnitValidchk, Validators.requiredTrue],
      clinical_specimen: this.addControlsToArray(this.clinical_specimen),
      nail_unit: this.addControlsToArray(this.nailUnit),
      site_indicator: [''],
    });
  }
  getPhysicians() {
    this.itemsService.physicians_list().subscribe(data => {

      this.list = data;
      this.physicians_list = this.list.physicians_data;

      this.physicians_list.map((option) => this.allNames.push(option.physician_name));
      this.physicians_list.map((val) => this.allIds.push(val.id));
      this.physicians_list.map((data) => this.filterVal.push(data));
      if (this.list['status'] === '1') {
        this.spinner.hide();
      }

    });

    this.stateGroups = [{
      ids: this.allIds,
      names: this.allNames
    }];

  }

  hasClinicalInfo(e) {

    this.nailCheck = e.target.checked;
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    const innerGroup: FormGroup = spcFrm.controls[0] as FormGroup;
    this.getNailUnitVal();
    if (this.nailCheck && !this.nailUnitValid) {
      innerGroup.controls['clinicalSpecimenValidCheck'].setErrors({ required: true });
    } else {
      innerGroup.controls['clinicalSpecimenValidCheck'].setErrors(null);
    }

  }
  onSubmit() {
    //console.log(this.addSpecimen);
    this.getNailUnitVal();
    if (this.addSpecimen.status == "VALID") {
      //this.spinner.show();      
      let fmData = Object.assign({}, this.addSpecimen.value);
      fmData = Object.assign(fmData, { collection_date: this.convertDate(fmData.collection_date) });
      fmData = Object.assign(fmData, { date_received: this.convertDate(fmData.date_received) });
      fmData = Object.assign(fmData, { patient_dob: this.convertDate(fmData.patient_dob) });
      fmData = Object.assign(fmData, { pri_subscriber_dob: this.convertDate(fmData.pri_subscriber_dob) });
      fmData = Object.assign(fmData, { sec_subscriber_dob: this.convertDate(fmData.sec_subscriber_dob) });
      fmData = Object.assign(fmData, { patient_phn: this.phone_format(fmData.patient_phn) });
      fmData = Object.assign(fmData, { bill: fmData.bill.map((v, i) => v ? this.billData[i] : null) });
      fmData = Object.assign(fmData, { pri_subscriber_relation: fmData.pri_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      fmData = Object.assign(fmData, { sec_subscriber_relation: fmData.sec_subscriber_relation.map((v, i) => v ? this.relations[i] : null) });
      //fmData = Object.assign(fmData,{insurance_type:fmData.insurance_type.map((v,i)=>v?this.insurance_type[i]:null)});

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { clinical_specimen: elem.clinical_specimen.map((el, j) => el ? this.clinical_specimen[j] : null) });

        });
      });

      fmData.specimen.forEach((elem, inx) => {
        Object.keys(elem).forEach(field => {
          elem = Object.assign(elem, { nail_unit: elem.nail_unit.map((el, j) => el ? this.nailUnitData[j] : null) });

        });
      });

      let formObj = { ...fmData }; // {name: '', description: ''}
      let serializedForm = JSON.stringify(formObj);
      this.itemsService.specimen_check(serializedForm).subscribe(data => {
        let dt;
        let res;
        dt = data;

        //console.log(dt['status']);
        if (dt['status'] == '1') {
          res = confirm(dt['msg']);
        } else {
          res = true;
        }
        if (res) {
          this.itemsService.add_specimen(serializedForm).subscribe(data => {
            this.spinner.hide();
            this.formData = data['returnUrl'];
            if (data['qrCode'] != '') {
              data['qrCode'].forEach((element, ind) => {
                this.print(data['assessioning_num'][ind], element, data['base_url']);
                this.print(data['assessioning_num'][ind], element, data['base_url']);
              });
            }
            this.route.navigateByUrl(data['returnUrl']);
          });
        }
      });




    }
    else {
      this.toastr.info('Please fill all required fields.', 'Error', {
        timeOut: 3000
      });
      let invalidFields = [].slice.call(document.getElementsByClassName('ng-invalid'));
      invalidFields[1].focus();
      this.validateAllFormFields(this.addSpecimen);
    }
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];

      return this.searchData;
    }

    return this.stateGroups;
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.addSpecimen.get(field).valid && (this.addSpecimen.get(field).touched || this.addSpecimen.get(field).dirty);
  }

  isNailUnitValid(firstField: any) {
    return !firstField.valid && (this.addSpecimen.get('wp_submit').touched || this.addSpecimen.get('specimen').touched);
  }

  addControlsToArray(data: any) {
    const arr = data.map(elem => {
      return this.fb.control(false);
    });
    return this.fb.array(arr);
  }

  get specimenArray() {
    return <FormArray>this.addSpecimen.get('specimen');
  }

  get relationArray() {
    return <FormArray>this.addSpecimen.get('pri_subscriber_relation');
  }

  get relationArraySec() {
    return <FormArray>this.addSpecimen.get('sec_subscriber_relation');
  }

  // get getInsuranceTypeArr(){

  //   return <FormArray>this.addSpecimen.get('insurance_type');
  // }

  get getBillArr() {

    return <FormArray>this.addSpecimen.get('bill');
  }

  get getClinicalSpecimenArr() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    let spcFrmGrp: any = [];
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      <FormArray>spcFrmGrp.push(<FormArray>innerGroup.controls['clinical_specimen']);

    }
    return spcFrmGrp;

  }

  get getNailUnitArr() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    let spcFrmGrp: any = [];
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      <FormArray>spcFrmGrp.push(<FormArray>innerGroup.controls['nail_unit']);

    }
    return spcFrmGrp;

  }

  getRelationValue() {
    this.selectedCheckBoxValue = [];
  }
  getBillVal() {
    let count = 0;
    this.getBillArr.controls.forEach((elm, i) => {
      elm.value ? count++ : '';
    });
    this.billErrors = count > 0 ? false : true;
    this.billValidCheck = count > 0 ? true : false;
    this.addSpecimen.controls['billValidCheck'].setValue(this.billValidCheck);

  }

  getSubscriberVal() {
    let count = 0;
    let patient_dob;
    let patient_sex;
    this.relationArray.controls.forEach((elm, i) => {
      elm.value ? count++ : '';
      if (elm.value && i === 0) {
        patient_dob = this.addSpecimen.controls['patient_dob'].value;
        patient_sex = this.addSpecimen.get('patient_sex').value;
        this.addSpecimen.controls['pri_subscriber_dob'].setValue(patient_dob);
        this.addSpecimen.controls['pri_sex'].setValue(patient_sex);
      }
    });
    this.relationValidCheck = count > 0 ? true : false;
    this.addSpecimen.controls['priSubscriberRelationValidCheck'].setValue(this.relationValidCheck);
  }
  getNailUnitVal() {
    let spcFrm = <FormArray>this.addSpecimen.get('specimen');
    let ln = spcFrm.controls.length;
    for (let i = 0; i < ln; i++) {
      const innerGroup: FormGroup = spcFrm.controls[i] as FormGroup;
      let spcFrmGrp = <FormArray>innerGroup.controls['nail_unit'];
      let st1: boolean = false;
      let st2: boolean = false;
      let st3: boolean = false;
      let st4: boolean = false;
      let st5: boolean = false;
      spcFrmGrp.controls.forEach((eml, ind) => {
        if ((ind === 0 && eml.value)) {
          st1 = true;
        }
        if ((ind === 3 && eml.value)) {
          st2 = true;
        }
        if (ind === 5 && eml.value) {
          st4 = true;
        }
        if (ind === 4 && eml.value) {
          st5 = true;
        }

        if ((ind === 1 && eml.value) || (ind === 2 && eml.value)) {
          st3 = true;
        }

      });
      if (!st1 && !st2 && !st4 && !st3 && !st5 && this.nailCheck) {
        this.nailUnitValid = false;
        this.nailUnitValidMsg = "Please check atlest one nail unit.";
        //console.log(this.nailUnitValid);
      } else if ((st1 || st2 || st4) && !st3) {
        this.nailUnitValid = false;
        this.nailUnitValidMsg = "Please check at least one between PAS/GMS/FM or PAS/GMS.";
      } else {
        this.nailUnitValid = true;
        this.nailUnitValidMsg = "";
      }
      innerGroup.controls['clinicalSpecimenValidCheck'].setValue(this.nailUnitValid);
    }
  }

  billValidateTouch() {
    let flg = false;
    this.getBillArr.controls.forEach((eml, i) => {
      if (eml.touched) {
        flg = true;
      }
    });
    return flg;
  }

  print(assessioning_num: any, qrname: any, qr_path: any): void {
    let printContents, popupWin;
    //printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">
  <div id="${assessioning_num}_1;?>" class="print_qr_code">
  <img src='${qr_path}assets/uploads/qr_images/${qrname}'>
  <p style='font-size:15px; margin-top: -12px;'><strong>${assessioning_num}</strong></p>
  </div>
  </body>
    </html>`
    );
    popupWin.document.close();
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [mnth, day, date.getFullYear()].join("/");
    }
    return newDate;
  }

  onChangeCalculateAge(value: Date) {
    if (value) {
      var today = new Date();
      var diff = Math.abs(today.getTime() - value.getTime());
      var diffYears = Math.floor(diff / (1000 * 3600 * 24 * 365));
      this.addSpecimen.controls['patient_age'].setValue(diffYears);
    }

  }

  callSomeFunction(phy_id: any) {
    this.addSpecimen.controls['physician_id'].setValue(phy_id);

    this.itemsService.getPhisicianDetails(phy_id).subscribe(data => {
      this.postsArray = data;
      if (this.postsArray.status === '1') {
        this.physicians_list_data = this.postsArray.phy_det[0];
      } else {
        this.physicians_list_data = '';
      }


    });
  }

  onEnter(evt: any) {
    this.physicians_list_data = '';
    this.addSpecimen.controls['physician_id'].setValue('');
  }

  phone_format(phone) {
    var city, number;
    let ph_no = phone;
    var value = ph_no.toString().trim().replace(/^\+/, '');
    city = value.slice(0, 3);
    number = value.slice(3);
    number = number.slice(0, 3) + '-' + number.slice(3);
    number = city + "-" + number.trim();
    return number;
  }

  selectTestType(e) {
    if (e.target.value === 'W' || e.target.value === 'RPP' || e.target.value === 'UTI' ) {

      this.addSpecimen.controls['process_done'].setValidators([Validators.required]);
      this.showProcess = true;

    } else {
      this.addSpecimen.controls['process_done'].setErrors(null);
      this.showProcess = false;
    }
  }

}
