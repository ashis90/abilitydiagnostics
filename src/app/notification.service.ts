import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  headers: Headers = new Headers;
  options: any;
  constructor(private http:HttpClient) { 
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept','application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl+'Notifications/'

  getAllNotifications(data)
  {
    return this.http.post(this.url+'get_all_notifications',JSON.stringify(data), this.options);
   
  }

  readNotifications(data)
  {
    return this.http.post(this.url+'read_notifications',JSON.stringify(data), this.options);
   
  }

  dismissNotifications(data)
  {
    return this.http.post(this.url+'dismiss_notifications',JSON.stringify(data), this.options);
   
  }

  getPendingNotifications(data)
  {
    return this.http.post(this.url+'get_pending_notifications',JSON.stringify(data), this.options);
   
  }
  addDataEntryNotifications(data)
  {
    return this.http.post(this.url+'data_entry_notification',JSON.stringify(data), this.options);
   
  }
  physicianReportAfterThirtyDaysNotification(data)
  {
    return this.http.post(this.url+'physician_report_after_thirty_days_notification',JSON.stringify(data), this.options);
   
  }
}
