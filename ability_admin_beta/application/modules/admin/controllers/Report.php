<?php
if(!defined('BASEPATH'))
EXIT("No direct script access allowed");
class Report extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->session_checked($is_active_session = 1);
	}
	
	public function index(){
	
		$report_conditions = " (`id`<>'')";
		$select_fields = '*';
		$is_multy_result = 0;		
		$report_details = $this->BlankModel->getTableData('nail_macro_codes', $report_conditions, $select_fields, $is_multy_result,'id','DESC');
			
		common_viewloader('Report/report_list' , array('report_details' => $report_details));
		
	}
	
	 function reportEdit()
	 {
		if(isset($_REQUEST['id'])){
		$id = $_REQUEST['id'];
		
		$conditions = " ( `id` = '".$id."')";
		$select_fields = '*';
		$is_multy_result = 1;
		
		$report = $this->BlankModel->getTableData('nail_macro_codes', $conditions, $select_fields, $is_multy_result);
		common_viewloader('Report/report_edit', array('report' => $report)); 	
		}
	}
 
	function reportEditing(){
		if ($this->input->post()) {
			$data = $this->input->post();
			$report_id = $data['id'];
			$this->form_validation->set_rules('sc','Short Code','required');
			// $this->form_validation->set_rules('diagnosis','Diagnosis','required');
			$this->form_validation->set_rules('text','Shortcode Description','required');
			// $this->form_validation->set_rules('comments','Comments','required');
			// $this->form_validation->set_rules('color','Color','required');
		
			if ($this->form_validation->run()==FALSE) {
				$this->reportEdit($report_id);
			} else{
			    $data = array();
			    $data = $this->input->post();
				$text=str_ireplace('<p>','',$data['text']);
				$text=str_ireplace('</p>','',$text); 
				$comments = str_ireplace('<p>','',$data['comments']);
				$comments = str_ireplace('</p>','',$comments);
			
				$redirect_url=base_url().'admin/report/';
				 
				$conditions = " ( `id` = '".$data['id']."')";
		  		$service_data = array('sc' => $data['sc'], 'diagnosis' => $data['diagnosis'], 'text' =>  $text,'comments' => $comments,'color' => $data['color']);
		  		
		  		$programsedit = $this->BlankModel->editTableData('nail_macro_codes', $service_data, $conditions);
		  		$this->session->set_flashdata('succ', 'Updated successfully');
			    header('location:'.$redirect_url);
				exit;
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/report/');
		}
	}

	function shortcode_add()
	{
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('sc','Short Code','required');
			// $this->form_validation->set_rules('diagnosis','Diagnosis','required');
			$this->form_validation->set_rules('text','Shortcode Description','required');
			// $this->form_validation->set_rules('comments','Comments','required');
			// $this->form_validation->set_rules('color','Color','required');
			if ($this->form_validation->run()==FALSE) {
				common_viewloader('Report/report_add');
			} else{

				$data = array();
				$data = $this->input->post();
				$text=str_ireplace('<p>','',$data['text']);
				$text=str_ireplace('</p>','',$text); 
				$comments = str_ireplace('<p>','',$data['comments']);
				$comments = str_ireplace('</p>','',$comments);
				$shortcode_data = array('sc' => $data['sc'], 'diagnosis' => $data['diagnosis'], 'text' => $text,'comments' => $comments,'color' => $data['color'],'create_date'=>date('Y-m-d H:i:s'));
				$redirect_url=base_url().'admin/report/';
				$programsedit = $this->BlankModel->addTableData('nail_macro_codes', $shortcode_data);
				$this->session->set_flashdata('succ', 'Added successfully');
				header('location:'.$redirect_url);
				exit;
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/report');
		}

	}
  	function add_short(){
		common_viewloader('Report/report_add'); 	
 	}
 	/////////////////////Report short code excel Download//////////////////////////////
 	public function report_shortcode_list_export(){
	
	$this->load->library('Classes/PHPExcel');
	$report_conditions = " (`id`<>'')";
	$select_fields = '*';
	$is_multy_result = 0;		
	$details = $this->BlankModel->getTableData('nail_macro_codes', $report_conditions, $select_fields, $is_multy_result,'id','DESC');
    $objPHPExcel = new PHPExcel();
    $excel_title = 'Report_short_code_list';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Report_short_code_list');



    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Short Codes');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Diagnosis Text');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Shortcode Description');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Comments');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Color');

      $row = 2;
      foreach($details as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row, $value['sc']);
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row, $value['diagnosis']);
          $text_strip_tags = strip_tags($value['text']);
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row, $text_strip_tags);
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row, $value['comments']);
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row, $value['color']);

          $row++;

            }


          
  
      	$objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_title.'.csv"');
      //header('Cache-Control: max-age=0');

      // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->save('php://output');	
 	}	  		
}
?>