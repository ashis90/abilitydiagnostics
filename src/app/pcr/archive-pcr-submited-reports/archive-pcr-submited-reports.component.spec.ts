import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivePcrSubmitedReportsComponent } from './archive-pcr-submited-reports.component';

describe('ArchivePcrSubmitedReportsComponent', () => {
  let component: ArchivePcrSubmitedReportsComponent;
  let fixture: ComponentFixture<ArchivePcrSubmitedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivePcrSubmitedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivePcrSubmitedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
