import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";

import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { FooterComponent } from "./footer/footer.component";
import { PrivacypolicyComponent } from "./privacypolicy/privacypolicy.component";
import { CommonModule } from "@angular/common";
import { ServicedetComponent } from "./servicedet/servicedet.component";
import { SuspendedComponent } from "./specimen/suspended/suspended.component";
import { SpecimensListComponent } from "./specimen/specimens-list/specimens-list.component";

import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MenuComponent } from "./menu/menu.component";
import { AuthGuard } from "./auth.guard";
import { DataTableModule } from "angular-6-datatable";
import { MyaccountComponent } from "./myaccount/myaccount.component";
import { NgxPaginationModule } from "ngx-pagination";
import { SpecimenAddComponent } from "./specimen/specimen-add/specimen-add.component";
import { NgxLoadingModule } from "ngx-loading";
import { QcConfirmationComponent } from "./qc-confirmation/qc-confirmation.component";
import { CancelledComponent } from "./specimen/cancelled/cancelled.component";
import { StagesComponent } from "./specimen/stages/stages.component";
import { ExtractionComponent } from "./specimen/extraction/extraction.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { ViewSpecimenComponent } from "./view-specimen/view-specimen.component";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatInputModule,
  MatRippleModule
} from "@angular/material";
import { ViewStagesComponent } from "./view-stages/view-stages.component";
import { DeliveryToExtractionComponent } from "./pcr/delivery-to-extraction/delivery-to-extraction.component";
// import { SpecimenNxtStageComponent } from './specimen-nxt-stage/specimen-nxt-stage.component';
import { SpecimensEditComponent } from "./specimen/specimens-edit/specimens-edit.component";
import { ToastrModule } from "ngx-toastr";
import { LabAnalyticsComponent } from "./lab-analytics/lab-analytics.component";
import { DeliveryToPcrComponent } from "./pcr/delivery-to-pcr/delivery-to-pcr.component";
import { ExcelService } from "./excel.service";
import { AssignWellStageComponent } from "./pcr/assign-well-stage/assign-well-stage.component";
import { HistoPcrAnalyticsComponent } from "./histo-pcr-analytics/histo-pcr-analytics.component";
import { PcrLeftMenuComponent } from "./pcr/pcr-left-menu/pcr-left-menu.component";
import { DataAnalysisComponent } from "./pcr/data-analysis/data-analysis.component";
import { LabDataComponent } from "./lab-data/lab-data.component";
import { ShortCodeAnalyticsComponent } from "./short-code-analytics/short-code-analytics.component";
import { ProcessStageModalComponent } from "./pcr/process-stage-modal/process-stage-modal.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { DiagnosesAnalysisComponent } from "./diagnoses-analysis/diagnoses-analysis.component";
import { AvgTurnTimeComponent } from "./avg-turn-time/avg-turn-time.component";
import { ManualReviewComponent } from "./pcr/manual-review/manual-review.component";
import { SlideAnalyticsComponent } from "./slide-analytics/slide-analytics.component";
import { PendingReportsComponent } from "./reports/pending-reports/pending-reports.component";
import { PendingSpecimensListComponent } from "./reports/pending-specimens-list/pending-specimens-list.component";
import { SubmittedReportsComponent } from "./reports/submitted-reports/submitted-reports.component";
import { AmendedReportsComponent } from "./reports/amended-reports/amended-reports.component";
import { HistoAvgStageComponent } from "./histo-avg-stage/histo-avg-stage.component";
import { ReportGenerateComponent } from "./pcr/report-generate/report-generate.component";
import { BatchDeleteComponent } from "./pcr/batch-delete/batch-delete.component";
import { PcrAvgStageComponent } from "./pcr-avg-stage/pcr-avg-stage.component";
import { SpecimenTrackComponent } from "./specimen-track/specimen-track.component";
import { PcrSubmitedReportsComponent } from "./pcr/pcr-submited-reports/pcr-submited-reports.component";
import { ArchiveDocumentsComponent } from "./pcr/archive-documents/archive-documents.component";
import { ClinicalReportComponent } from "./clinical-report/clinical-report.component";
import { PhysicianNotificationAeComponent } from "./physician-notification-ae/physician-notification-ae.component";
import { PhysicianNotesComponent } from "./physician-notes/physician-notes.component";
import { PendingReportEditComponent } from "./reports/pending-reports/pending-report-edit/pending-report-edit.component";
import { ViewAmendedReportComponent } from "./reports/amended-reports/view-amended-report/view-amended-report.component";
import { DataEntryPeopleListComponent } from "./data-entry-people/data-entry-people-list/data-entry-people-list.component";
import { ViewDataEntryPeopleComponent } from "./data-entry-people/view-data-entry-people/view-data-entry-people.component";
import { EditDataEntryPeopleComponent } from "./data-entry-people/edit-data-entry-people/edit-data-entry-people.component";
import { AddDataEntryPeopleComponent } from "./data-entry-people/add-data-entry-people/add-data-entry-people.component";
import { NotificationListComponent } from "./notifications/notification-list/notification-list.component";
import { SurveyComponent } from "./survey/survey.component";
import { EditSubmittedReportComponent } from "./edit-submitted-report/edit-submitted-report.component";
import { ArchiveComponent } from "./archive/archive.component";
// import { AddSpecimenReportComponent } from './reports/add-specimen-report/add-specimen-report.component';
import { EditPendingReportComponent } from "./reports/edit-pending-report/edit-pending-report.component";
// Import library module
import { NgxSpinnerModule } from "ngx-spinner";
import { PcrStagesComponent } from "./pcr/pcr-stages/pcr-stages.component";
import { TrackModalComponent } from "./track-modal/track-modal.component";
import { ContactComponent } from "./contact/contact.component";
import { ConfirmationPopoverModule } from "angular-confirmation-popover";
import { MainComponent } from "./main/main.component";
import { NavbarModule } from "./core/navbar/navbar.module";
import { AppRoutingModule } from "./app-routing.module";
import { DeliveryExtComponent } from "./delivery-ext/delivery-ext.component";
import { AssignWellComponent } from "./assign-well/assign-well.component";
import { PendingStageDetailsComponent } from "./specimen/pending-stage-details/pending-stage-details.component";
import { DeliveryToExtractionStageComponent } from "./pcr/delivery-to-extraction-stage/delivery-to-extraction-stage.component";
import { FieldErrorDisplayModule } from "./field-error-display/field-error-display.module";
import { NgxMaskModule, IConfig } from "ngx-mask";
import { NgxCaptchaModule } from "ngx-captcha";
import { FullSpecimenTarckingComponent } from "./full-specimen-tarcking/full-specimen-tarcking.component";
import { WoundCareComponent } from "./wound-care/wound-care.component";
import { DystrophicNailPresentmentComponent } from "./dystrophic-nail-presentment/dystrophic-nail-presentment.component";
import { ImportSpecimenDataComponent } from "./import-specimen-data/import-specimen-data.component";
import { ArchiveSpecimenListComponent } from "./specimen/archive-specimen-list/archive-specimen-list.component";
import { ArchiveSubmittedReportsComponent } from "./reports/archive-submitted-reports/archive-submitted-reports.component";
import { ArchivePcrSubmitedReportsComponent } from "./pcr/archive-pcr-submited-reports/archive-pcr-submited-reports.component";
import { QualityReportsComponent } from './analytics/quality-reports/quality-reports.component';

// Import angular-fusioncharts
import { FusionChartsModule } from "angular-fusioncharts";
// Import FusionCharts library
import * as FusionCharts from "fusioncharts";
// Load FusionCharts Individual Charts
import * as Charts from "fusioncharts/fusioncharts.charts";
import * as TimeSeries from "fusioncharts/fusioncharts.timeseries";
import { CertificateComponent } from './certificate/certificate.component';
import { ReferenceLabReportComponent } from './analytics/reference-lab-report/reference-lab-report.component';
import { UtiComponent } from './uti/uti.component';
import { RppComponent } from './rpp/rpp.component';
// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts, TimeSeries);

@NgModule({
  imports: [
    BrowserModule,
    NgxPaginationModule,
    DataTableModule,
    CommonModule,
    NavbarModule,
    CoreModule, // Singleton objects (services, components that are loaded only once, etc.)
    SharedModule, // Shared (multi-instance) objects
    ReactiveFormsModule,
    FormsModule,
    NgxLoadingModule.forRoot({}),
    MatAutocompleteModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
    NgxSpinnerModule,
    FusionChartsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: "danger" // set defaults here
    }),
    AppRoutingModule, // Main routes for application
    FieldErrorDisplayModule,
    NgxMaskModule.forRoot(),
    NgxCaptchaModule
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatAutocompleteModule
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    PrivacypolicyComponent,
    ServicedetComponent,
    SuspendedComponent,
    SpecimensListComponent,
    DashboardComponent,
    MenuComponent,
    MyaccountComponent,
    SpecimenAddComponent,
    QcConfirmationComponent,
    CancelledComponent,
    StagesComponent,
    ExtractionComponent,
    LoginComponent,
    HomeComponent,
    ViewSpecimenComponent,
    ViewStagesComponent,
    DeliveryToExtractionComponent,
    SpecimensEditComponent,
    LabAnalyticsComponent,
    DeliveryToPcrComponent,
    AssignWellStageComponent,
    HistoPcrAnalyticsComponent,
    PcrLeftMenuComponent,
    LabDataComponent,
    ShortCodeAnalyticsComponent,
    DataAnalysisComponent,
    ProcessStageModalComponent,
    DiagnosesAnalysisComponent,
    AvgTurnTimeComponent,
    ManualReviewComponent,
    SlideAnalyticsComponent,
    PendingReportsComponent,
    PendingSpecimensListComponent,
    SubmittedReportsComponent,
    AmendedReportsComponent,
    HistoAvgStageComponent,
    ReportGenerateComponent,
    BatchDeleteComponent,
    PcrAvgStageComponent,
    SpecimenTrackComponent,
    PcrSubmitedReportsComponent,
    ArchiveDocumentsComponent,
    ClinicalReportComponent,
    PhysicianNotificationAeComponent,
    PhysicianNotesComponent,
    ViewAmendedReportComponent,
    PendingReportEditComponent,
    DataEntryPeopleListComponent,
    ViewDataEntryPeopleComponent,
    EditDataEntryPeopleComponent,
    AddDataEntryPeopleComponent,
    NotificationListComponent,
    SurveyComponent,
    EditSubmittedReportComponent,
    ArchiveComponent,
    EditPendingReportComponent,
    PcrStagesComponent,
    TrackModalComponent,
    ContactComponent,
    MainComponent,
    DeliveryExtComponent,
    AssignWellComponent,
    PendingStageDetailsComponent,
    DeliveryToExtractionStageComponent,
    FullSpecimenTarckingComponent,
    WoundCareComponent,
    DystrophicNailPresentmentComponent,
    ImportSpecimenDataComponent,
    ArchiveSpecimenListComponent,
    ArchiveSubmittedReportsComponent,
    ArchivePcrSubmitedReportsComponent,
    QualityReportsComponent,
    CertificateComponent,
    ReferenceLabReportComponent,
    UtiComponent,
    RppComponent
  ],
  providers: [AuthGuard, ExcelService],
  bootstrap: [AppComponent],
  entryComponents: [ProcessStageModalComponent, TrackModalComponent]
})
export class AppModule {}
