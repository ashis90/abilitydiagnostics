import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators'; //add this
import { NgxSpinnerService } from 'ngx-spinner'

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-avg-turn-time',
  templateUrl: './avg-turn-time.component.html',
  styleUrls: ['./avg-turn-time.component.css']
})
export class AvgTurnTimeComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  data;
  no_data: any;
  filterData: any;
  searchData: boolean = false;
  specimen_count: any;
  total_working_day: any;
  avg_time: any;
  no_data_status = false;

  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({

      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)

    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.spinner.show();
      this.itemsService.get_avg_time().subscribe(data => {
        this.spinner.hide();
        this.searchData = false;
        this.postsArray = data;
        if (this.postsArray.status === '1') {
          this.data = this.postsArray.details;
          this.specimen_count = this.data.length;
        }
      });
    }
  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), 'to_date': this.convertDate(this.searchForm.value.to_date), "dataType": this.searchForm.value.dataType };
      this.itemsService.search_avg_time(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.searchData = true;
        this.postsArray = data;
        this.data = this.postsArray.details;

        this.total_working_day = this.postsArray.total_working_day;

        this.no_data_status = false;

        if (this.postsArray.status === '1') {
          this.specimen_count = this.postsArray.count[0].specimen_count;
          this.avg_time = (this.total_working_day / this.specimen_count).toFixed(2);
        }
        if (this.postsArray.status === '0') {
          this.no_data_status = true;
          this.no_data = "No Data Found.";
          this.specimen_count = 0;
          this.avg_time = 0;
        }
      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }

  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }
  search(term: string) {
    let allData: any;
    allData = this.postsArray.details;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.assessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }

  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
