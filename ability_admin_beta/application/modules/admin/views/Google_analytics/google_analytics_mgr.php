<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Google Analytics
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Google Analytics List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Google Analytics List </h4>
          </div>
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?></div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>

            <?php
            if ($this->session->flashdata('update')) {
            ?>
            <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('update');
              echo ($message);
              ?>
            </div>
            <?php
              }
              ?>
          <div class="box-body">
            <!--<div id="alert-success" style="display: none;" class="alert alert-success" role="alert" ></div>-->
           <div class="svt">
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="basicDataTable">
                <thead>
                  <tr>
                    <th width="8%">Sr No</th>  
					          <th width="15%">Description</th>
                    <th width="55%">Type</th>                    
                    <th width="55%">Status</th>                    
                    <th width="25%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($analyticsList): ?>
                  <?php
                    $count= 1;
                    foreach($analyticsList as $analytics):	
                    ?>
                  <tr class="odd gradeX" id="data-<?php echo $analytics['id'];?>">
                    <td><?php echo $count;?> </td>
                    
                    <td><?php echo short_description($analytics['description'],100);?></td>  

                    <td><?php echo $analytics['type'];  ?></td>   

                    <td><?php echo $analytics['is_active'];  ?></td>                   
                    <td>
                      <a href="<?php echo base_url('analytics-update/'.$analytics['id']);?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

                      <!-- <a href="<?php //echo base_url('partners-update/'.$partnersCompany['partner_id']); ?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a> -->


                    </td>
                  </tr>
                  <?php 
                    $count++;
                    endforeach ;
                    else:?>
                  <tr class="odd gradeX">
                    <td colspan="5">
                      <p>No data here.</p>
                    </td>
                  </tr>
                  <?php endif;?>
                </tbody>
              </table>
            </div>
           </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
