import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataEntryPeopleListComponent } from './data-entry-people-list.component';

describe('DataEntryPeopleListComponent', () => {
  let component: DataEntryPeopleListComponent;
  let fixture: ComponentFixture<DataEntryPeopleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataEntryPeopleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataEntryPeopleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
