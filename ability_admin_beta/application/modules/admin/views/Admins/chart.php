<?php if(!defined('BASEPATH')) EXIT("No direct script access allowed"); ?>
<!-- Content Wrapper. Contains page content -->


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawCharts);

function drawCharts() {
    var data = google.visualization.arrayToDataTable([
     						  ['Language', 'Rating'],
							     <?php  

							      foreach($result as $specialties)
							      {
							        echo "['".$specialties['test_type']."', ".$specialties['COUNT_TYPE']."],";
							      }
                     ?>
   ]);
    
    var options = {
        title: '',
        width:  650,
        height: 500,
        is3D: true,
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('piecharts'));
    
    chart.draw(data, options);
}
</script>
<style type="text/css">
 #piecharts svg{width: 650px !important;
   } 

    div#piecharts{
        background: #fff;width: 100%; 
    }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section> -->
  <!-- Main content -->
  <!-- <section class="content"> -->
    <!-- Small boxes (Stat box) -->
    <!-- <div class="row"> -->
        <!-- small box -->
      <!-- <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3> </h3>
            <p>User List</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url('admin/user')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
        <!-- small box -->        
      <!-- <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
          <div class="inner">
            <h3><sup style="font-size: 20px"></sup></h3>
            <p>Services</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo base_url('admin/service')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
        <!-- small box -->
      <!-- <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3></h3>
            <p>Pages</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="<?php echo base_url('admin/cms')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->

      <!-- ./col -->
        <!-- small box -->
      <!-- <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
          <div class="inner">
            <h3></h3>
            <p>Report Shortcodes</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo base_url('admin/report')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
      <div class="clearfix"></div>
       <div>
        
        
        <!--==================PIE Chart====================-->
        <div class="clearfix"></div>
        <div id="piecharts"></div>

                    <!--=====================END=======================-->
      </div>
      
    </div>
    <!--------tab------>
  </section>
</div>
<script type="text/javascript">
$('#basic_DataTable').dataTable({
   "paging":   false,    
});
</script>