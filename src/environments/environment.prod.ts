export const environment = {
  production: true,
    //---------------- Live Url Start-------------------//
  //  baseUrl: "https://www.abilitydiagnostics.com/abadmin/api/",
  //  assetsUrl: "https://www.abilitydiagnostics.com/abadmin/"

    //---------------- Live Url End-------------------//

  //---------------- beta-2.0 Url Start-------------------//
   baseUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/api/",
   assetsUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/"
    //---------------- beta-2.0 Url End-------------------//
};

