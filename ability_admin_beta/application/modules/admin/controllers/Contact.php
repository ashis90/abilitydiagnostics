<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class  Contact extends MY_Controller {

    function __construct() {
        parent::__construct();
          $this->session_checked($is_active_session = 1);
    }

 public function index() {
 	$this->session_checked($is_active_session = 1);
    $conditions = " (`contact_status` <>'Deleted')";
  	$select_fields = '*';
    $is_multy_result = 0;
    $contactList = $this->BlankModel->getTableData('contact', $conditions, $select_fields, $is_multy_result);
    common_viewloader('Contact/contact_list', array('contactList' => $contactList)); 
 }
   
}
?>