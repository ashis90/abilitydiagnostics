import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {
  public data : any;
  specimens_list:any = [];
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  incompleted_notifications: any;
  completed_notifications: any;
  incomplete_status:boolean=false;
  complete_status:boolean=false;
  user_id:any=localStorage.userid;
  constructor(
    private http:HttpClient, 
    public itemsService:NotificationService,
    public route:Router,
    private spinner: NgxSpinnerService
    ) { }

  ngOnInit() {
    this.getNotifications();
  }


  getNotifications()
    {
      let params;
      this.spinner.show();
      params = {'user_role':localStorage.user_role,'user_id':localStorage.userid};
      this.itemsService.getAllNotifications(params).subscribe(data => {
          
          this.postsArray = data;
          this.incompleted_notifications = this.postsArray.incompleted_notes;
          this.completed_notifications = this.postsArray.complted_notes;
          if(this.postsArray['status'] == '1')
          {
            this.spinner.hide();
            if(this.incompleted_notifications){
              this.incomplete_status = true;
            }else{
              this.incomplete_status = false;
            }

            if(this.completed_notifications){
              this.complete_status = true;
            }else{
              this.complete_status = false;
            }
          }
          if(this.postsArray['status'] == '0')
          {
            this.spinner.hide();
          }                  
      });
      
    }

    read_notification(note_id:any){
      let params;
      params = {'note_id':note_id};
      let post_arr;
      this.itemsService.readNotifications(params).subscribe(data => {
        post_arr = data;
        if(post_arr['status'] === '1'){
          this.getNotifications();
        }
      });
    }

    dismiss_notification(note_id:any){
      let params;
      params = {'note_id':note_id,'reader_id':localStorage.userid};
      let post_arr;
      this.itemsService.dismissNotifications(params).subscribe(data => {
        post_arr = data;
        if(post_arr['status'] === '1'){
          this.getNotifications();
        }
      });
    }

}
