<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Role Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Role List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Update New Role</h4>
          </div>
          <form action="<?php echo base_url().'admin/role/roleEditing/';?>" method="POST" enctype="multipart/form-data" name="role_frm" id="role_frm">
            <input type="hidden" name="role_id" value="<?php echo $role_data['role_id'];  ?>">
            <div class="form-group">
              <label for="exampleTextarea"> Role Name </label>
              <input type="text" id="role_names" name="role_name"  placeholder="Enter Role Name" value="<?php echo $role_data['role_name']; ?>">
              <?php echo form_error('role_name','<div class="text-danger">','</div>'); ?>
            </div>
            
            <div class="form-group">
              <label for="exampleTextarea"> Role Short Name </label>
              <input type="text" id="role_shortname" name="role_shortname" readonly="" placeholder="Enter Short name" value="<?php echo $role_data['role_shortname'];  ?>">
              <?php echo form_error('role_shortname','<div class="text-danger">','</div>'); ?>
            </div>
            
            <button type="submit" class="btn btn-primary ban-sbmt">Update Role</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>

