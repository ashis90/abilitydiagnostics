<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Page Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Page List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Add New Page</h4>
          </div>
          <form action="<?php echo base_url().'admin/cms/createNewPage/';?>" method="POST" enctype="multipart/form-data" name="page_frm" id="page_frm">
            <div class="form-group">
              <label for="exampleTextarea"> Page Name </label>
              <input type="text" id="site_page_name" name="site_page_name"  placeholder="Enter Page Name">
              <?php echo form_error('site_page_name','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> Page Title </label>
              <input type="text" id="site_page_title" name="site_page_title"  placeholder="Enter Page Title">
              <?php echo form_error('site_page_title','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> Page Description </label>
              <textarea class="tnytextarea" id="site_page_desc" name="site_page_desc" rows="10" cols="80" placeholder="Enter Page Description"></textarea>
              <?php 
                echo form_error('site_page_desc','<div class="text-danger">','</div>'); 
              ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> File </label>
              <input type="file" name="site_page_image" class="btn btn-default btn-file">
            </div>
            <div class="form-group">
              <label for="exampleTextarea">Page Type</label>
              <select name="site_page_type" id="site_page_type">
                <option value="">Select Status</option>
                <option value="ABLITY">Ability</option>
                <option value="APS">APS</option>
              </select>
            </div>
            
            
            
            <button type="submit" class="btn btn-primary ban-sbmt">Add Page</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
