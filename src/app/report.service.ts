import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PendingSpecimensService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl + 'Reports/';

  pendingSpecimensList() {
    return this.http.get(this.url + 'pending_specimens_list', this.options);
  }

  allpendingSpecimensList() {
    return this.http.get(this.url + 'all_pending_specimens_list', this.options);
  }

  search_pending_spe(data) {
    return this.http.post(this.url + 'search_pending_specimens_list', JSON.stringify(data), this.options);
  }
  //////////////////////// pending Report ////////////////////////////////////


  physician_active_list() {
    return this.http.get(this.url + 'get_physicians_active_list', this.options);
  }

  pendingReport() {
    return this.http.get(this.url + 'pending_reports', this.options);
  }

  allpendingReport() {
    return this.http.get(this.url + 'all_pending_reports', this.options);
  }

  search_pending_reports(data) {
    return this.http.post(this.url + 'search_pending_reports', JSON.stringify(data), this.options);
  }

  //////////////////////// sumitted Report ////////////////////////////////////

  submittedReport(data) {
    return this.http.post(this.url + 'submitted_reports', JSON.stringify(data), this.options);
  }


  loadMoreSubmittedReport(data) {
    return this.http.post(this.url + 'load_more_submitted_reports', JSON.stringify(data), this.options);
  }

  fivedysReport() {
    return this.http.get(this.url + 'fivedays_reports', this.options);
  }

  allsubmittedReport() {
    return this.http.get(this.url + 'all_submitted_reports', this.options);
  }

  search_submitted_reports(data) {
    return this.http.post(this.url + 'search_submitted_reports', JSON.stringify(data), this.options);
  }

  edit_submitted_report(data) {
    return this.http.post(this.url + 'submitted_reports_edit', data, this.options);
  }


  //////////////////////// pending Report ////////////////////////////////////

  amendedReport() {
    return this.http.get(this.url + 'amended_reports', this.options);
  }

  allamendedReport() {
    return this.http.get(this.url + 'all_amended_reports', this.options);
  }

  search_amended_reports(data) {
    return this.http.post(this.url + 'search_amended_reports', JSON.stringify(data), this.options);
  }

  get_amended_report(data) {
    return this.http.post(this.url + 'amended_report_details', JSON.stringify(data), this.options);
  }

  sendPendingReport(data) {
    return this.http.post(this.url + 'send_pending_report', JSON.stringify(data), this.options);
  }

  get_fungal_pathology_report(data) {
    return this.http.post(this.url + 'get_nail_fungal_pathology_report', JSON.stringify(data), this.options);

  }

  getNailMacroCode(data) {
    return this.http.post(this.url + 'get_nail_macro_code', JSON.stringify(data), this.options);
  }

  addReportData(data) {
    return this.http.post(this.url + 'add_report_data', data, this.options);
  }

  get_pending_report_details(data) {
    return this.http.post(this.url + 'get_pending_report_details', JSON.stringify(data), this.options);
  }
  editReportData(data) {
    return this.http.post(this.url + 'edit_report_data', data, this.options);
  }

  pendingReportsTatNotification() {
    return this.http.get(this.url + 'pendingReportsTatNotification', this.options);

  }

  specimenPendingTatNotification() {
    return this.http.get(this.url + 'specimenPendingTatNotification', this.options);

  }
}
