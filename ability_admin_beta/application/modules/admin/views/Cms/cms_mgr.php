<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Page Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Banners List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <a href="<?php echo base_url('admin/cms/add_page');?>" class="btn btn-primary">Add new Page</a>
          <div class="box-header">
            <h4 class="title">Page List </h4>
          </div>
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?></div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>

            <?php
            if ($this->session->flashdata('succs')) {
            ?>
            <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('succs');
              echo ($message);
              ?>
            </div>
            <?php
              }
              ?>
          <div class="box-body">
            <!--<div id="alert-success" style="display: none;" class="alert alert-success" role="alert" ></div>-->
           <div class="svt">
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="basicDataTable">
                <thead>
                  <tr>
                    <th width="8%">Sr No</th>  
					          <th width="15%">Page Name</th>
                    <th width="55%">Page Content</th>                    
                    <th width="55%">Site Type</th>                    
                    <th width="25%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($pageList): ?>
                  <?php
                    $count= 1;
                    foreach($pageList as $page):	
                    ?>
                  <tr class="odd gradeX" id="data-<?php echo $page['site_page_id'];?>">
                    <td><?php echo $count;?> </td>

					          <td><?php echo $page['site_page_name'];?></td>
                    
                    <td><?php echo short_description($page['site_page_desc'],100);?></td>  

                    <td><?php echo $page['site_page_type'];  ?></td>                   
                    <td>
                      <a href="<?php echo base_url().'admin/cms/pageEdit?site_page_id='.$page['site_page_id'];?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                     </td>
                  </tr>
                  <?php 
                    $count++;
                    endforeach ;
                    else:?>
                  <tr class="odd gradeX">
                    <td colspan="5">
                      <p>No data here.</p>
                    </td>
                  </tr>
                  <?php endif;?>
                </tbody>
              </table>
            </div>
           </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
