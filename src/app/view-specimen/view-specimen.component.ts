import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-view-specimen',
  templateUrl: './view-specimen.component.html',
  styleUrls: ['./view-specimen.component.css']
})
export class ViewSpecimenComponent implements OnInit {

  postsArray:any = [] ;
  load_status:boolean = true;
  public data : any;
  physicians_list:any;
  msg_yes:any;
  bill:any;
  subscriber:any;
  subscriber_sec:any;
  clinical_specimen:any;
  nail_unit:any;
  bill_1= false;
  bill_2 = false;
  subscriber_1 = false;
  subscriber_2 = false;
  subscriber_3 = false;
  first = false;
  second = false;
  third = false;
  fourth = false;
  nail_unit1 = false;
  nail_unit2 = false;
  nail_unit3 = false;
  nail_unit4 = false;
  nail_unit5 = false;
  nail_unit6 = false;
  typesArr:any;
  test_type_list:any;
  test_type:string;
  process_done:string;
  partners_company_name:string;
  constructor(
    private http:HttpClient, 
    public itemsService:SpecimenService,
    public route:Router,
    public activatedRoute : ActivatedRoute,
    private spinner: NgxSpinnerService
    ) { }


  ngOnInit() {
    this.getAllTestTypes();
    if(this.load_status==true)
    {
      this.spinner.show();
        var id=this.activatedRoute.snapshot.paramMap.get('id');
      var dataType = this.activatedRoute.snapshot.paramMap.get('dataType');
      let params = { 'id': id, 'dataType': dataType }
      this.itemsService.makeSpecimenViewRequest(params).subscribe(data => {
          this.postsArray = data;
          console.log(this.postsArray)
          this.spinner.hide();
          this.data  =  this.postsArray.spe_det[0];
          this.physicians_list = this.postsArray.phy_det[0];
          this.test_type_list = this.postsArray.test_types;
          this.bill = this.postsArray.bill;
          this.subscriber = this.postsArray.subscriber;
          this.subscriber_sec = this.postsArray.subscriber_sec;
          this.clinical_specimen = this.postsArray.clinical_specimen;
          this.nail_unit = this.postsArray.nail_unit;
          if(this.data['test_type']){

            this.test_type_list.forEach(elem=>{
              if( elem.type_code === this.data['test_type']){
                this.test_type = elem.type_name;
              }
            })
            if(this.data['test_type'] === 'W'){
              this.process_done = this.data['process_done'];
            }
          }

          
      if(this.data['partners_company_name']){
        this.partners_company_name = this.data['partners_company_name'];
        console.log(this.partners_company_name)
      }
          for(let i=0;i<this.bill.length;i++)
          {
            if(this.bill[i]=='1')
            {
              this.bill_1 = true;
            }
            if(this.bill[i]=='2')
            {
              this.bill_2 = true;
            }
          }
          for(let i=0;i<this.subscriber.length;i++)
          {
            if(this.subscriber[i]=='Self')
            {
              this.subscriber_1 = true;
            }
            if(this.subscriber[i]=='Spouse')
            {
              this.subscriber_2 = true;
            }
            if(this.subscriber[i]=='Dependent')
            {
              this.subscriber_3 = true;
            }
          }
          for(let i=0;i<this.clinical_specimen.length;i++)
          {
            if(this.clinical_specimen[i]=='Right')
            {
              this.first = true;
            }
            if(this.clinical_specimen[i]=='Left')
            {
              this.second = true;
            }
            if(this.clinical_specimen[i]=='Biopsy')
            {
              this.third = true;
            }
            if(this.clinical_specimen[i]=='Excision')
            {
              this.fourth = true;
            }
          }
          for(let i=0;i<this.nail_unit.length;i++)
          {
            if(this.nail_unit[i]=='5')
            {
              this.nail_unit1 = true;
            }
            if(this.nail_unit[i]=='1')
            {
              this.nail_unit2 = true;
            }
            if(this.nail_unit[i]=='2')
            {
              this.nail_unit3 = true;
            }
            if(this.nail_unit[i]=='6')
            {
              this.nail_unit4 = true;
            }
            if(this.nail_unit[i]=='4')
            {
              this.nail_unit5 = true;
            }
            if(this.nail_unit[i]=='7')
            {
              this.nail_unit6 = true;
            }
          }
          if(this.data.physician_accepct=='0')
          {
            this.msg_yes = "Yes";
          }
          else
          {
            this.msg_yes = "No";
          }
          
        });
        
    }

  }

  getAllTestTypes() {
    this.itemsService.getAllTestTypes().subscribe(data => {
      let postData = data;
      this.typesArr = postData['test_types'];
    });
  }

}
