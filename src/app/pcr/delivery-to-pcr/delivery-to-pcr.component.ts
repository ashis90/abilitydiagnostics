import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { AnlyticsService } from '../../anlytics.service';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-delivery-to-pcr',
  templateUrl: './delivery-to-pcr.component.html',
  styleUrls: ['./delivery-to-pcr.component.css']
})
export class DeliveryToPcrComponent implements OnInit {

  public data : any;
  specimens_list:any = [];
  specimen_data:any = [];
  physicians_list:any = [];
  searchForm: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;

  physicians_list_data:any = [];
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  modalRef: BsModalRef;
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  public stateGroups: StateGroup[] = [{
      ids: [],
      names: []
    }]//test
  public searchData: StateGroup[] = [{
      ids: [],
      names: []
    }]

  constructor(private http:HttpClient, public itemsService:PcrService,public route:Router,private modalService: BsModalService,public analyticsService:AnlyticsService)
   {
      this.searchForm = new FormGroup({        
       "barcode" : new FormControl(),
       "physician_name" : new FormControl(), 
      "physician_id" : new FormControl(),  
       "assessioning_num" : new FormControl(), 
      });
    }

    onSubmit()
    {
        if(this.searchForm.status == "VALID")
        {   
            this.loading = true;
            this.load_status=false;
            this.sendData = {"barcode":this.searchForm.value.barcode, "assessioning_num":this.searchForm.value.assessioning_num, "physician":this.searchForm.value.physician_id  };
            this.itemsService.serachDeliveryToPcr(this.sendData).subscribe( data=> {
                this.loading = false;
                if(this.specimens_list.status === '1'){
                  this.specimens_list = data;
                  this.data  =  this.specimens_list.specimen_results;
                } 
                if(this.specimens_list.status === '0')
                {
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
                }
             });
        }
        else
        {
 
        }
    }

    resetForm(){
      this.searchForm.reset();
      this.getdeliverytopcr();
    }

    ngOnInit() {    
      this.loading = true;
      this.getdeliverytopcr();  
      this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
        startWith(''),
        map(value => this._filterGroup(value?value.toLowerCase():''))
      );        
  }

  load_all()
    {
      this.no_data_status= false;
      this.loading = true;
      this.load_status=false;
      this.itemsService.pcrLoadAllRequest().subscribe(data => {
        this.loading = false; 
        if(this.specimens_list.status === '1')
        { 
        this.specimens_list = data;
        this.data  =  this.specimens_list.specimen_results;
        }
        if(this.specimens_list.status === '0')
        {
          this.no_data_status= true;
          this.no_data="No Data Found.";
        }
    });
    }

    getdeliverytopcr() {
    this.loading = true;
    this.itemsService.deliveryToPcr().subscribe(data => {          
    this.specimens_list = data;
      if(this.specimens_list.status == '1')
      {
        this.loading = false;
        this.no_data_status = false;
        this.data  =  this.specimens_list.specimen_results;
        this.physicians_list = this.specimens_list.physicians_data;

        this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
        this.physicians_list.map((val) => this.allIds.push ( val.id));
        this.physicians_list.map((data) => this.filterVal.push ( data));
      }
      if(this.specimens_list.status == '0')
      {
        this.loading = false;
        this.no_data_status= true;
        this.no_data="No Data Found.";
      }
               
    });
    this.stateGroups = [{
      ids:this.allIds,
      names:this.allNames
    }];
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount : number = this.filterVal.length;
        this.options = [];
        this.ids=[];
        for(let i=0;i<itemCount;i++){
            if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) === 0 ){
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids:this.ids,
          names:this.options
        }];
        
        return this.searchData;
    }
    
    return this.stateGroups;
}

callSomeFunction(phy_id :any){
  this.searchForm.controls['physician_id'].setValue(phy_id);
}

displayProcessStage(specimen_id:any){
  this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
    initialState: {
      title: 'fifth',
      specimen_id: specimen_id,
    }
  });
}

displaytrackmodal(specimen_id:any){

  this.sendData = { "acc_no":specimen_id};
  this.analyticsService.specimen_track(this.sendData).subscribe(data => {
    this.postsArray = data;
    if(this.postsArray.histo_status=='1')
    {
      this.acc_no=true;
      this.histo_modal =true;
    }
    else
    {
      this.histo_modal =false;
    }
    if(this.postsArray.pcr_status=='1')
    {
      this.acc_no=true;
      this.pcr_modal =true;
    }
    else
    {
      this.pcr_modal =false;
    }
    if(this.postsArray.status=='0')
    {
       this.acc_no=false;
       this.pcr_modal =false;
       this.histo_modal =false;
    }
    this.modalRef = this.modalService.show(TrackModalComponent,  {
      initialState: {
        title: 'track',
        acc_no:  this.acc_no,
        postsArray: this.postsArray,
        histo_modal: this.histo_modal,
        pcr_modal: this.pcr_modal
      }

    });

  });


}



}
