import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-clinical-report',
  templateUrl: './clinical-report.component.html',
  styleUrls: ['./clinical-report.component.css']
})
export class ClinicalReportComponent implements OnInit {
  @ViewChild('uploadFile') uploadFileInput: ElementRef;
  searchForm: FormGroup;
  pdfForm: FormGroup;
  datePickerConfig: Partial<BsDatepickerConfig>;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  msg = false;
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  data;
  no_data;
  no_row;
  file;
  file_data;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private toastr: ToastrService) {
      this._archiveNote = this.itemsService.archiveNote;
      this.searchForm = new FormGroup({

      "from_date": new FormControl(),
      "to_date": new FormControl(),
      "physician_name": new FormControl('', Validators.required),
      "physician_id": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)
    });

    this.pdfForm = new FormGroup({

      "image": new FormControl(),
      "from_date": new FormControl(),
      "to_date": new FormControl(),
      "physic_id": new FormControl(),
      "comment": new FormControl()
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.msg = false;
      this.spinner.show();
      this.itemsService.get_physician_clinical().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.physicians_list = this.postsArray.physicians_data;
        this.physicians_list.map((option) => this.allNames.push(option.physician_name));
        this.physicians_list.map((val) => this.allIds.push(val.id));
        this.physicians_list.map((data) => this.filterVal.push(data));
        this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
          startWith(''),
          map(value => this._filterGroup(value ? value.toLowerCase() : ''))
        );

      });
      this.stateGroups = [{
        ids: this.allIds,
        names: this.allNames
      }];
    }
  }

  pdfonSubmit(id, from_date, to_date) {
    if (this.file) {
      this.msg = true;
      this.spinner.show();
      var fileSplit = this.file['name'].split('.');
      var fileExt = '';
      if (fileSplit.length > 1) {
        fileExt = fileSplit[fileSplit.length - 1];
      }
      const formData = new FormData();
      formData.append('fname', this.file['name']);
      formData.append('file_data', this.file);
      formData.append('from_date', from_date);
      formData.append('to_date', to_date);
      formData.append('physician_id', id);
      formData.append('comment', this.pdfForm.value.comment);
      this.itemsService.submit_pdf_data(formData).subscribe(data => {
        this.loading = false;
        this.postsArray = data;
        this.spinner.hide();
        // let printContents, popupWin;
        if (this.postsArray) {
          window.open(this.postsArray.pdf_url, '_blank', 'top=0,left=0,height=100%,width=auto');
        }
      });
    } else {
      this.toastr.info('Please choose a graph to generate report.', 'Error', {
        timeOut: 3000
      });
    }
  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  incomingfile(event) {
    this.file = <File>event.target.files[0];
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];

      return this.searchData;
    }

    return this.stateGroups;
  }

  callSomeFunction(phy_id: any) {
    this.searchForm.controls['physician_id'].setValue(phy_id);
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.load_status = false;
      this.sendData = {
        "from_date": this.convertDate(this.searchForm.value.from_date),
        "to_date": this.convertDate(this.searchForm.value.to_date),
        "physician_id": this.searchForm.value.physician_id,
        'dataType': this.searchForm.value.dataType
      };
      this.itemsService.search_clinical_data(this.sendData).subscribe(data => {
        this.msg = true;
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
        if (this.postsArray.status === '0') {
          this.msg = false;
        }

      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
