<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class SpecimenStages extends MY_Controller {

    function __construct() {
        parent::__construct();
       	date_default_timezone_set('MST7MDT');
    }
    function index(){}

    function view_stages()
    {
        $time_det= array();
        $data = json_decode(file_get_contents('php://input'), true);
        $id = "'".$data['id']."'";
        $dataType = $data['dataType'];
        $tbl1=SPECIMEN_STAGE_DETAILS_MAIN;
        if($data['dataType'] == 'general'){
            $tbl1=SPECIMEN_STAGE_DETAILS_MAIN;
        }else if($data['dataType'] == 'archive'){
            $tbl1=SPECIMEN_STAGE_DETAILS_ARCHIVE;
        }
        $specimen_stages_sql = "SELECT * FROM (SELECT * FROM ".$tbl1.")stage_det
                   INNER JOIN (SELECT * FROM ".SPECIMEN_IDS_MAIN.")stages ON `stage_det`.`stage_id` =  `stages`.`specime_stage_id` 
                    AND `stage_det`.`specimen_id`=".$id." ORDER BY `stage_det`.`stage_id` ASC"; 
        $specimens_details = $this->BlankModel->customquery($specimen_stages_sql);
        foreach ($specimens_details as $stage_det)
        {
            // $time_sql = "select * from `wp_abd_specimen_nxt_timestamp` where stage_id ='".$stage_det['stage_id']."' AND specimen_id ='".$stage_det['specimen_id']."'";
            // $time_result = $this->BlankModel->customquery($time_sql);
            //  array_push($time_det, $time_result);

            $physician_first_name = get_user_meta($stage_det['lab_tech'], 'first_name' );
            $physician_last_name  = get_user_meta($stage_det['lab_tech'], 'last_name');
            $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
            if($stage_det['pass_fail']=='1')
            {
                $val = 'ticked';
            }
            else{
                $val = 'crossed';
            }

            $specimen_information = array('id'=>$stage_det['specimen_id'],'stage_name'=>$stage_det['stage_name'],'specimen_timestamp'=>$stage_det['specimen_timestamp'],'comments'=>$stage_det['comments'],'addtional_desc'=>$stage_det['addtional_desc'],'physician_name'=>$physician_name,'pass_fail' => $val);
            array_push($time_det, $specimen_information);
        }
        
        if($specimens_details)
		{
			echo json_encode(array('details'=>$time_det));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
        
    }
    function specimen_next()
    {
        $time_det= array(); 
        $data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){

        $arr_val[0]= "";
        $arr_val[1]= "";
        $num = "";
        $color_val[1] = "tan";
        $sub = "";
        $cas_val = "1";
        $nail = "nail";
        $histo_chk = "";
        $pcr_chk = "";

        $id = "'".$data['id']."'";
        $stageid = "'".$data['stageid']."'";

        $conditions = " ( `id` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_data  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
	
        $conditions1 = " (`specime_stage_id` = ".$stageid.")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_data  = $this->BlankModel->getTableData('specimen_ids', $conditions1, $select_fields, $is_multy_result);

        $conditions2 = " (`specimen_id`= ".$id." AND `stage_id` = ".$stageid." AND `status` = 'Complete')";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_det  = $this->BlankModel->getTableData('specimen_stage_details', $conditions2, $select_fields, $is_multy_result);
       
        if(!empty($specimen_stage_det))
        {
            if($specimen_stage_det['stage_id']==2)
            {
                $ori_value = substr($specimen_stage_det['total_desc'],0,10);
                $dimen_value = substr($specimen_stage_det['total_desc'],11);
                $arr_val = explode("x",$ori_value);
                $color_val = explode(" ",$specimen_stage_det['total_desc']);
                if(!empty($arr_val) && !empty($ori_value))
                {
                 $third_val = substr($arr_val[2],0,2);
                 $num =  filter_var($third_val, FILTER_SANITIZE_NUMBER_INT);
                }
                else
                {
                    $arr_val[0]= "";
                    $arr_val[1]= "";
                    $color_val[1]= "";
                }
        
                $desc = explode(" ",$specimen_stage_det['addtional_desc']);
                $new_cas_val = substr($specimen_stage_det['addtional_desc'],-13);
                $cas_val=filter_var($new_cas_val, FILTER_SANITIZE_NUMBER_INT);
                if(in_array('nail', $desc) || empty($specimen_stage_det['addtional_desc'])) 
                { 
                  $nail = "nail"; 
                }
                else if(in_array('skin', $desc))
                {
                  $nail = "skin"; 
                }
                else
                {
                  $nail = "nail"; 
                } 
                 if(!empty($specimen_stage_det['submitted']))
                 {
                     $sub = 1;
                 }
                 else
                 {
                     $sub = 0;
                 }
                 if($specimen_stage_det['histo']!="")
                 {
                     $histo_chk = 1;
                 }
                 else
                 {
                     $histo_chk = 0;
                 }
                 if($specimen_stage_det['pcr']!="")
                 {
                     $pcr_chk = 1;
                 }
                 else
                 {
                     $pcr_chk = 0 ;
                 }
            }

        }
        if(empty($specimen_stage_det))
        {
            $specimen_stage_det ="Nil";
        }
        $specimen_stages_sql = "SELECT * FROM (SELECT * FROM `wp_abd_specimen_stage_details`)stage_det
                   INNER JOIN (SELECT * FROM `wp_abd_specimen_ids`)stages ON `stage_det`.`stage_id` =  `stages`.`specime_stage_id` 
                    AND `stage_det`.`specimen_id`=".$id." ORDER BY `stage_det`.`stage_id` ASC"; 
        $specimens_details = $this->BlankModel->customquery($specimen_stages_sql);
        foreach ($specimens_details as $stage_det)
        {
            $physician_first_name = get_user_meta($stage_det['lab_tech'], 'first_name' );
            $physician_last_name  = get_user_meta($stage_det['lab_tech'], 'last_name');
            $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
            if($stage_det['pass_fail']=='1')
            {
                $val = 'ticked';
            }
            else{
                $val = 'crossed';
            }

            $specimen_information = array('id'=>$stage_det['specimen_id'],'stage_name'=>$stage_det['stage_name'],'specimen_timestamp'=>$stage_det['specimen_timestamp'],'comments'=>$stage_det['comments'],'addtional_desc'=>$stage_det['addtional_desc'],'physician_name'=>$physician_name,'pass_fail' => $val);
            array_push($time_det, $specimen_information);
        }

        $conditions3 = " ( `specimen_id` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$clinical_data  = $this->BlankModel->getTableData('clinical_info', $conditions3, $select_fields, $is_multy_result);
        $clinical_specimen     = explode(",",$clinical_data['clinical_specimen']);
      
        if($specimen_data)
		{
            echo json_encode(array('status'=>'1','spe_details'=>$specimen_data,'stage_data'=>$specimen_stage_data,'stage_det'=>$specimen_stage_det,'stage_time'=>$time_det,'clinical_info'=>$clinical_specimen,
            'first'=>$arr_val[0],'second'=>$arr_val[1],'third'=>$num,'color'=> $color_val[1],'cassettes'=>$cas_val,'nail'=>$nail,
        'sub'=>$sub,'histo_chk'=>$histo_chk,'pcr_chk'=>$pcr_chk));
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }else{
    	echo json_encode(array('status'=>'0'));
    }

    }

    function specimen_next_stage_search()
    {
        $time_det= array(); 
        $data = json_decode(file_get_contents('php://input'), true);
        
        if(!empty($data)){
        $arr_val[0]= "";
        $arr_val[1]= "";
        $num = "";
        $color_val[1] = "tan";
        $sub = "";
        $cas_val = "1";
        $nail = "nail";
        $histo_chk = "";
        $pcr_chk = "";

        $acc_no = "'".$data['accessioning_number']."'";
        $stageid = "'".$data['stageid']."'";

        $conditions = " ( `assessioning_num` = ".$acc_no .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_data  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
        $id = $specimen_data['id'];
        $conditions1 = " (`specime_stage_id` = ".$stageid.")";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_data  = $this->BlankModel->getTableData('specimen_ids', $conditions1, $select_fields, $is_multy_result);

        $conditions2 = " (`specimen_id`= ".$id." AND `stage_id` = ".$stageid." AND `status` = 'Complete')";
		$select_fields = '*';
		$is_multy_result = 1;
		$specimen_stage_det  = $this->BlankModel->getTableData('specimen_stage_details', $conditions2, $select_fields, $is_multy_result);
       
        if(!empty($specimen_stage_det))
        {
            if($specimen_stage_det['stage_id']==2)
            {
                $ori_value = substr($specimen_stage_det['total_desc'],0,10);
                $dimen_value = substr($specimen_stage_det['total_desc'],11);
                $arr_val = explode("x",$ori_value);
                $color_val = explode(" ",$specimen_stage_det['total_desc']);
                if(!empty($arr_val) && !empty($ori_value))
                {
                 $third_val = substr($arr_val[2],0,2);
                 $num =  filter_var($third_val, FILTER_SANITIZE_NUMBER_INT);
                }
                else
                {
                    $arr_val[0]= "";
                    $arr_val[1]= "";
                    $color_val[1]= "";
                }
        
                $desc = explode(" ",$specimen_stage_det['addtional_desc']);
                $new_cas_val = substr($specimen_stage_det['addtional_desc'],-13);
                $cas_val=filter_var($new_cas_val, FILTER_SANITIZE_NUMBER_INT);
                if(in_array('nail', $desc) || empty($specimen_stage_det['addtional_desc'])) 
                { 
                  $nail = "nail"; 
                }
                else if(in_array('skin', $desc))
                {
                  $nail = "skin"; 
                }
                else
                {
                  $nail = "nail"; 
                } 
                 if(!empty($specimen_stage_det['submitted']))
                 {
                     $sub = 1;
                 }
                 else
                 {
                     $sub = 0;
                 }
                 if($specimen_stage_det['histo']!="")
                 {
                     $histo_chk = 1;
                 }
                 else
                 {
                     $histo_chk = 0;
                 }
                 if($specimen_stage_det['pcr']!="")
                 {
                     $pcr_chk = 1;
                 }
                 else
                 {
                     $pcr_chk = 0 ;
                 }
            }

        }
        if(empty($specimen_stage_det))
        {
            $specimen_stage_det ="Nil";
        }
        $specimen_stages_sql = "SELECT * FROM (SELECT * FROM `wp_abd_specimen_stage_details`)stage_det
                   INNER JOIN (SELECT * FROM `wp_abd_specimen_ids`)stages ON `stage_det`.`stage_id` =  `stages`.`specime_stage_id` 
                    AND `stage_det`.`specimen_id`=".$id." ORDER BY `stage_det`.`stage_id` ASC"; 
        $specimens_details = $this->BlankModel->customquery($specimen_stages_sql);
        foreach ($specimens_details as $stage_det)
        {
            $physician_first_name = get_user_meta($stage_det['lab_tech'], 'first_name' );
            $physician_last_name  = get_user_meta($stage_det['lab_tech'], 'last_name');
            $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
            if($stage_det['pass_fail']=='1')
            {
                $val = 'ticked';
            }
            else{
                $val = 'crossed';
            }

            $specimen_information = array('id'=>$stage_det['specimen_id'],'stage_name'=>$stage_det['stage_name'],'specimen_timestamp'=>$stage_det['specimen_timestamp'],'comments'=>$stage_det['comments'],'addtional_desc'=>$stage_det['addtional_desc'],'physician_name'=>$physician_name,'pass_fail' => $val);
            array_push($time_det, $specimen_information);
        }

        $conditions3 = " ( `specimen_id` = ".$id .")";
		$select_fields = '*';
		$is_multy_result = 1;
		$clinical_data  = $this->BlankModel->getTableData('clinical_info', $conditions3, $select_fields, $is_multy_result);
        $clinical_specimen     = explode(",",$clinical_data['clinical_specimen']);
      
        if($specimen_data)
		{
            echo json_encode(array('status'=>'1','spe_details'=>$specimen_data,'stage_data'=>$specimen_stage_data,'stage_det'=>$specimen_stage_det,'stage_time'=>$time_det,'clinical_info'=>$clinical_specimen,
            'first'=>$arr_val[0],'second'=>$arr_val[1],'third'=>$num,'color'=> $color_val[1],'cassettes'=>$cas_val,'nail'=>$nail,
        'sub'=>$sub,'histo_chk'=>$histo_chk,'pcr_chk'=>$pcr_chk));
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }else{
        echo json_encode(array('status'=>'0'));
    }

    }
    function specimen_stage_update()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data)){
        $id = "'".$data['specimen_id']."'";
        $uid = "'".$data['lab_tech']."'";
        $stageid = "'".$data['stage_id']."'";
        $pass = "'".$data['pass']."'";
        $newstageid = trim($stageid,"'");
        $newuid = trim($uid, "'");
        $newpass = trim($pass,"'");
        $new_spe = trim($id,"'");
        
        if($newstageid==2)
        {
            $xval =  "'".$data['xvalue']."'";
            $yval = "'".$data['yvalue']."'";
            $zval =  "'".$data['zvalue']."'";
            $color = "'".$data['color']."'";
            $pieces =  "'".$data['pieces']."'";
            $tissue =  "'".$data['tissue']."'";
            $cassettes =  "'".$data['cassettes']."'";
            if(!empty("'".$data['opt_sub']."'"))
            {
                $opt_sub =  "'".$data['opt_sub']."'";
            }
            else
            {
                $opt_sub= "";
            }
            if(!empty($data['histo']))
            {
                $histo_chk_val =  $data['histo'];
            }
            else
            {
                $histo_chk_val="";
            }
            if(!empty($data['pcr']))
            {
                $pcr_chk_val =  $data['pcr'];
            }
            else
            {
                $pcr_chk_val="";
            }
            if(!empty($data['histo_val']))
            {
                $histo_val =  $data['histo_val'];
            }
            else
            {
                $histo_val="" ;
            }
            if(!empty($data['pcr_val']))
            {
                $pcr_val =  $data['pcr_val'];
            }
            else
            {
                $pcr_val ="" ;
            }

            if(!empty($opt_sub))
            {
              $opt = $opt_sub;
            }
            else
            {
              $opt = "";
            }
            if(!empty($histo_val))
            {
              $histo_val= $histo_val;
            }
            else
            {
              $histo_val ="";
            }
            if(!empty($pcr_val))
            {
              $pcr_val= $pcr_val;
            }
            else
            {
              $pcr_val ="";
            }
      
            if(trim($pieces,"'")==1)
            {
              $word = trim($pieces,"'")." piece";
              $piece_word = "";
            }
            else if(trim($pieces,"'")==2 || trim($pieces,"'")==3 || trim($pieces,"'")=="multiple")
            {
              $word = trim($pieces,"'")." pieces";
              $piece_word = " in aggregate";
            }
            
             if($opt)
            {
              if(trim($cassettes,"'")>1)
              {
                $histo_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is ".trim($opt_sub,"'")." in ".trim($cassettes,"'")." cassettes.";
              }
              else
              {
                $histo_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is ".trim($opt_sub,"'")." in ".trim($cassettes,"'")." cassette.";
              }
            }
            if($histo_chk_val == 'histo')
            {
               if(trim($cassettes,"'")>1)
              {
                 $histo_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is submitted ".trim($histo_val,"'")."% in ".trim($cassettes,"'")." cassettes.";
              }
              else
              {
                 $histo_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is submitted ".trim($histo_val,"'")."% in ".trim($cassettes,"'")." cassette.";
              }
            }
            // else
            // {
            //     $histo_merged_value = "";
            // }
            if($pcr_chk_val == 'pcr')
            {
            //   if(trim($cassettes,"'")>1)
            //   {
            //     $pcr_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is submitted ".trim($pcr_val,"'")."% in ".trim($cassettes,"'")." cassettes.";
            //   }
            //   else
            //   {
            //     $pcr_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is submitted ".trim($pcr_val,"'")."% in ".trim($cassettes,"'")." cassette.";
            //   }
            $pcr_merged_value ="The specimen consists of ".$word." of ".trim($color,"'")." ".trim($tissue,"'")." measuring ".trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm".$piece_word.". The specimen is submitted ".trim($pcr_val,"'")."%.";
            }
            else
            {
                $pcr_merged_value = "";
            }
      
            if($pieces==2 || $pieces==3 || $pieces=="multiple")
            {
              $tot_val = trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm ".trim($color,"'")." aggregate ".trim($opt_sub,"'");
            }
            else
            {
              $tot_val = trim($xval,"'")."x".trim($yval,"'")."x".trim($zval,"'")."mm ".trim($color,"'")." ".trim($opt_sub,"'");
            }
      
      }
      else
      {
          $histo_merged_value ="";
          $pcr_merged_value ="";
          $tot_val ="";
          $opt = "";
          $histo_val ="";
          $pcr_val = "";
          $pieces = "";

      }
        
        // if(!empty($data['comment']))
        // {
        // $comments = "'".$data['comment']."'";
        // }
        // if(!empty($data['add_desc']))
        // {
        // $add_desc = "'".$data['add_desc']."'";
        // }
        
        // $newstageid = trim($stageid,"'");
        // $newuid = trim($uid, "'");
        // $newpass = trim($pass,"'");
        // $new_spe = trim($id,"'");
        // if(!empty($data['comment']))
        // {
        // $newcomm = trim($comments,"'");
        // }
        // else
        // {
        //     $newcomm= "";
        // }
        // if(!empty($data['add_desc']))
        // {
        // $newdesc = trim($add_desc,"'");
        // }
        // else
        // {
        //     $newdesc= "";
        // }

        $conditions2 = " (`specimen_id`= ".$id." AND `stage_id` = ".$stageid." )";
		$select_fields = '*';
		$is_multy_result = 1;
        $specimen_stage_det  = $this->BlankModel->getTableData('specimen_stage_details', $conditions2, $select_fields, $is_multy_result);
        
        if($specimen_stage_det)
        {
            $specimen_timestamp = $specimen_stage_det['specimen_timestamp'];
            $stage_det_id 	  = $specimen_stage_det['stage_det_id'];
            $stages_stage_id    = $specimen_stage_det['stage_id'];
            $specimen_id    = $specimen_stage_det['specimen_id'];

            $specimen_information = array( 
                'stage_id' => $stages_stage_id, 
                'lab_id' => $newuid, 
                'specimen_id' =>  $specimen_id, 
                'stage_timestamp' => date("M j, Y, H:i:s") 
            );
            $sql_insert= $this->BlankModel->addTableData('specimen_nxt_timestamp',$specimen_information);
            
            $del_spe_id = $data['specimen_id'];
            $del_stage_id = $data['stage_id'];
            $delete_details = $this->BlankModel->delete_data_condition('specimen_stage_details', $del_spe_id, $del_stage_id);

        }

            $stage_info= array( 
                'stage_id' => $newstageid, 
                'pass_fail' => $newpass, 
                'lab_tech' => $newuid, 
                // 'comments' => $newcomm, 
                'addtional_desc' => $histo_merged_value,
                'pcr_additional_desc' => $pcr_merged_value,  
                'total_desc' => $tot_val,
                'histo' =>trim($histo_val,"'"), 
                'pcr' =>trim($pcr_val,"'"), 
                'pieces' =>trim($pieces,"'"),
                'submitted' => trim($opt,"'"),
                'specimen_id' => $new_spe,
                'status' => 'Complete',
                'specimen_timestamp' => date("M j, Y, H:i:s") 
            );
            $stage_sql_insert = $this->BlankModel->addTableData('specimen_stage_details',$stage_info);

            $pcr_sql = "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id`='".$new_spe."' AND `stage_id`= '1'";
            $sql_pcr_det = $this->BlankModel->customquery($pcr_sql);
            
            $clinic_data_sql = "SELECT * FROM `wp_abd_clinical_info` WHERE `specimen_id`='".$new_spe."'";
            $clinic_data_result = $this->BlankModel->customquery($clinic_data_sql);
            if($newstageid==2)
            {
                if(!empty($clinic_data_result))
                {
                    $nail_unit_data = explode(",",$clinic_data_result[0]['nail_unit']);
                    if(in_array("4", $nail_unit_data) || in_array("5", $nail_unit_data) || in_array("7", $nail_unit_data))
                    {
                        if(!empty($sql_pcr_det))
                        {

                         $data = array(
							'addtional_desc' => $pcr_merged_value,
                            'total_desc' => $tot_val,
                            'pcr' =>trim($pcr_val,"'"), 
                            'pieces' =>trim($pieces,"'"),
                            'stage_id' => '1'
							);

							$this->db->where('specimen_id', $new_spe);
							$this->db->where('stage_id', 1);
							$this->db->update('pcr_stage_details', $data);
                        }
                        else
                        {
                            $insert_info = array( 
                                'addtional_desc' => $pcr_merged_value, 
                                'specimen_id' => $new_spe,
                                'stage_id' => '1', 
                                'lab_tech' => $newuid,
                                'total_desc' => $tot_val,
                                'pcr' =>trim($pcr_val,"'"), 
                                'pieces' =>trim($pieces,"'"),
                            );
                            $insert_pcr = $this->BlankModel->addTableData('pcr_stage_details',$insert_info);
                        }
            
                    }
                
                }
            }
            

            if($stage_sql_insert)
            {
                echo json_encode(array('status'=>'1'));
            }
            else
            {
                echo json_encode(array('status'=>'0'));
            }
     
            
        }else
            {
                echo json_encode(array('status'=>'0'));
            }
    }


}

?>