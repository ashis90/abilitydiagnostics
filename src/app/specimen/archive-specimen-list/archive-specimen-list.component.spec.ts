import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveSpecimenListComponent } from './archive-specimen-list.component';

describe('ArchiveSpecimenListComponent', () => {
  let component: ArchiveSpecimenListComponent;
  let fixture: ComponentFixture<ArchiveSpecimenListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchiveSpecimenListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveSpecimenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
