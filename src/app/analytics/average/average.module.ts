import { NgModule } from '@angular/core';

import { AverageRoutingModule } from './average-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';

@NgModule({
  imports:      [ AverageRoutingModule,NavbarModule ],
  declarations: [ AverageRoutingModule.components ]
})
export class AverageModule { }
