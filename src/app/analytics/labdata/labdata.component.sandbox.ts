import { sandboxOf } from 'angular-playground';
import { LabdataComponent } from './labdata.component';

export default sandboxOf(LabdataComponent)
  .add('Labdata Component', {
    template: `<cm-labdata></cm-labdata>`
  });
