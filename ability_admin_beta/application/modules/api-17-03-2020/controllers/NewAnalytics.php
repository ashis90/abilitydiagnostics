<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class NewAnalytics extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {}
     
    function get_lab_anlytics()
    {
        $details = array();
      //Current month
        $currentmonth              = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $currentmonthday           = mktime(0, 0, 0, date("m"),date("d"),date("Y"));
        $Current_Month             = date('M j',  $currentmonth);
        $Current_Month_Day         = date('M j',  $currentmonthday);
        $current_Month_LastDay     = date('t', strtotime(date('Y-m-d',$currentmonth)) );
        $Current_Month_present_Day = date('j',    $currentmonthday);
        $query_current_month       = date('Y-m-d',$currentmonth);
        $query_current_month_day   = date('Y-m-d',$currentmonthday);
        // last month
        $lastmonth                 = mktime(0, 0, 0, date("m")-1, 1, date("Y"));
        $Last_Month_Name           = date('M', $lastmonth); 
        $Last_Month                = date('M j', $lastmonth) ; 
        $Last_Month_Day            = date('t', strtotime(date('Y-m-d',$lastmonth)) );
        $query_last_month          = date('Y-m-d', $lastmonth);
        $query_last_month_days     = mktime(0, 0, 0, date("m")-1,$Last_Month_Day,date("Y"));
        $query_last_month_day      = date('Y-m-d', $query_last_month_days);
        //Current Month and date Details of Specimen
        $current_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_specimen_added_count = $this->BlankModel->customquery($current_date_specimen_count_sql);
        //Previous Month and date Details of Specimen
        $previous_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $previous_date_specimen_added_count = $this->BlankModel->customquery($previous_date_specimen_count_sql);
        //Current Month and date Details of Qc Check List
        $current_date_QcCheck_List_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_QcCheck_List_count = $this->BlankModel->customquery($current_date_QcCheck_List_count_sql);

        //Current Month Physician Count
        $get_users_sql = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_current_month." 00:00:00' AND '".$query_current_month_day." 23:59:59'";
        $users = $this->BlankModel->customquery($get_users_sql);
        $current_month_physician_count = count($users);
        
        //Previous Month Physician Count
        $get_users_sql_last = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_last_month." 00:00:00' AND '".$query_last_month_day." 23:59:59'";
        $users_last = $this->BlankModel->customquery($get_users_sql_last);
        $previous_month_physician_count = count($users_last);

        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
        $total_physician_count = count($physician_details);

        //Current Month and date Details of Nail Reports
        $current_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Inactive'";
        $current_date_pending_report_added_count = $this->BlankModel->customquery($current_date_pending_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Inactive'";
        $previous_date_pending_report_added_count = $this->BlankModel->customquery($previous_date_pending_report_count_sql);

        //Current Month and date Details of Nail Reports
        $current_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 'Active'";
        $current_date_issued_report_added_count = $this->BlankModel->customquery($current_date_issued_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Active'";
        $previous_date_issued_report_added_count = $this->BlankModel->customquery($previous_date_issued_report_count_sql);

        $active_physician_sql = "SELECT DISTINCT `physician_id` FROM `wp_abd_specimen`";
        $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
        $count_active = count($active_phy_count);

        if(!empty($users))
        {
            $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
        }
        else
        {
            $physicians_sending_specimens_count=0;  
        }
        if(!empty($users_last))
        {
            $pre_physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users_last);
        }
        else
        {
            $pre_physicians_sending_specimens_count=0;
        }
       
        $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '1')";
        $stages1 = $this->BlankModel->customquery($stage1_sql);
        $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2')";
        $stages2 = $this->BlankModel->customquery($stage2_sql);
        $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '3')";
        $stages3 = $this->BlankModel->customquery($stage3_sql);
        $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '4')";
        $stages4 = $this->BlankModel->customquery($stage4_sql);
       
        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day, 'last_Month'=>$Last_Month, 
        'last_Month_Name'=>$Last_Month_Name, 'last_Month_Day'=>$Last_Month_Day,'current_specimen_count'=>$current_date_specimen_added_count[0]['number_of_id'],
        'previous_specimen_count'=>$previous_date_specimen_added_count[0]['number_of_id'],'current_phy_count'=>$current_month_physician_count, 'pre_phy_count'=>$previous_month_physician_count,
        'specimen_per_phy_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_phy_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'specimen_per_active_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'pre_physicians_sending_specimens_count'=>$pre_physicians_sending_specimens_count,
        'report_issued_curr'=>$current_date_issued_report_added_count[0]['issued_report_count'],
        'report_issued_prev'=>$previous_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$current_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$current_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function search_get_lab_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $spe_opt = $data['histo_pcr'];
        if(empty($spe_opt))
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);
            }
            else if($spe_opt == 'Histo')
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM 
                                            (SELECT `id`,`assessioning_num` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`"; 
            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);
            }
            else if($spe_opt == 'PCR')
            {
                $PCR_search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id
                                                FROM `wp_abd_specimen`
                                                INNER JOIN `wp_abd_clinical_info`
                                                ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
                                                WHERE `wp_abd_clinical_info`.`nail_unit`  LIKE '%4%'
                                                AND `wp_abd_specimen`.`status` = '0'
                                                AND `wp_abd_specimen`.`qc_check` = '0'
                                                AND `wp_abd_specimen`.`physician_accepct` = '0' 
                                                AND `wp_abd_specimen`.`create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59'";
                $search_date_specimen_count = $this->BlankModel->customquery($PCR_search_date_specimen_count_sql);								

            }
            $Current_Month     = date('M j Y', strtotime($from_date));
            $Current_Month_Day = date('M j Y',  strtotime($to_date));
            $get_users_sql = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $users = $this->BlankModel->customquery($get_users_sql);
            $search_physician_count = count($users);
            
            $query_from_month = date('Y-m-d',strtotime($from_date));
            $query_to_month = date('Y-m-d',strtotime($to_date));
            
            $get_users_sql_new = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $physician_arg = $this->BlankModel->customquery($get_users_sql_new);
            $physicians_specimen_count = count($physician_arg);
            
            $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
            $total_physician_count = count($physician_details);

            $search_date_QcCheck_List_count_sql ="SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_QcCheck_List_count = $this->BlankModel->customquery($search_date_QcCheck_List_count_sql);
            $search_date_pending_report_count_sql ="SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Inactive'";
            $search_date_pending_report_added_count =$this->BlankModel->customquery($search_date_pending_report_count_sql);
            
            if($spe_opt == 'PCR')
            {
            $search_date_issued_report_count_sql ="SELECT count(`id`) as issued_report_count FROM `wp_abd_auto_mail` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `report_id` = '0'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }
            else
            {
            $search_date_issued_report_count_sql ="SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 'Active'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }

            $active_physician_sql = "SELECT DISTINCT `physician_id` FROM `wp_abd_specimen`";
            $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
            $count_active = count($active_phy_count);

            if(!empty($users))
            {
                $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
            }
            else
            {
                $physicians_sending_specimens_count=0;  
            }
            $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '1')";
            $stages1 = $this->BlankModel->customquery($stage1_sql);
            $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2')";
            $stages2 = $this->BlankModel->customquery($stage2_sql);
            $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '3')";
            $stages3 = $this->BlankModel->customquery($stage3_sql);
            $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '4')";
            $stages4 = $this->BlankModel->customquery($stage4_sql);

        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day,
        'current_specimen_count'=>$search_date_specimen_count[0]['number_of_id'],
        'current_phy_count'=>$search_physician_count,
        'specimen_per_phy_current'=>round($search_date_specimen_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($search_date_specimen_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'report_issued_curr'=>$search_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$search_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$search_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function get_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $apiData = json_decode(file_get_contents('php://input'), true);
        $sql ='';
        if(!empty($apiData)){
            $sql = 'LIMIT 0,'.$apiData;
        }
       //die( json_encode(array('status'=>$sql)));
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC $sql";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
            //die( json_encode(array('status'=>$specimen_data)));
        if($specimen_data) {
	       $run_sql_results = $this->db->get('wp_abd_import_data')->result_array();

        foreach($specimen_data as $histo_pcr_ana)
        {
					
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

       // $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";	
       // $specimen_resultss = $this->BlankModel->customquery($specimen_det);

        // $this->db->where('accessioning_num',$accessioning_num);
        // $this->db->where('positive_negtaive','positive');
        // $specimen_resultss = $this->db->get('wp_abd_import_data')->result_array();
        // $count = count($specimen_resultss);

        //$negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
       // $negative_results = $this->BlankModel->customquery($negative_sql);

        // $this->db->select('positive_negtaive');
        // $this->db->where('accessioning_num',$accessioning_num);
        // $this->db->where('positive_negtaive','negative');
        // $negative_results = $this->db->get('wp_abd_import_data')->result_array();
        // $count_negative= count($negative_results);

       // $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
       // $run_sql_results = $this->BlankModel->customquery($run_sql);
        foreach ($variable as $key => $value) {
            # code...
        }
        
   //      if(!empty($count))
   //      {
   //          $count_result_pos= $count." Pos ";
   //          if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
   //          $count_result = $count_result_pos.$count_result_neg;
   //      }
   //      if(empty($run_sql_results)) 
   //      {
   //          $count_result = "Not Run";
   //      }
   //      if (($count<1) && (!empty($run_sql_results)))
   //      {
   //          $count_result = "All Negative";
   //      }

							
	  //   $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 // array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
         die( json_encode(array('status'=>$run_sql_results)));
			 //echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }

    function get_all_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
	
        // foreach($specimen_data as $histo_pcr_ana)
        // {
					
        // $specimen_id = $histo_pcr_ana['id'];
        // $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        // $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        // $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

        // $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";	
        // $specimen_resultss = $this->BlankModel->customquery($specimen_det);
        // $count = count($specimen_resultss);

        // $negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
        // $negative_results = $this->BlankModel->customquery($negative_sql);
        // $count_negative= count($negative_results);

        // $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        // $run_sql_results = $this->BlankModel->customquery($run_sql);

        // if(!empty($count))
        // {
        //     $count_result_pos= $count." Pos ";
        //     if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
        //     $count_result = $count_result_pos.$count_result_neg;
        // }
        // if(empty($run_sql_results)) 
        // {
        //     $count_result = "Not Run";
        // }
        // if (($count<1) && (!empty($run_sql_results)))
        // {
        //     $count_result = "All Negative";
        // }

							
	    // $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		//  array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		//  }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $specimen_data));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}
    }

}       


?>