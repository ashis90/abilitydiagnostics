<?php
if(!defined('BASEPATH')) EXIT("No direct script access allowed");

if(!function_exists('common_viewloader')){
 	function common_viewloader($viewfilepath='',$param=array()){
		$CI = &get_instance();
		$CI->load->view('header');
		
		if($viewfilepath!=''){
			$CI->load->view($viewfilepath,$param);
		}
		$CI->load->view('footer');
	}
}

// display function 
if(!function_exists('pr')){
	function pr($display_data=array()){
		if(!empty($display_data)){
			echo "<pre>";
			print_r($display_data);
			echo "</pre>";
		}
	}
}

function short_description($string,$count_value){
		$string = strip_tags($string);
		if (strlen($string) > $count_value) {
		$stringCut = substr($string, 0, $count_value);
		$string = substr($stringCut, 0, strrpos($stringCut, ' ')).' ...'; 
		}
		return $string;
	}

if(!function_exists('get_user_details')){
	function get_user_details($logged_in_id){
		$CI = & get_instance();	
		if(!empty($logged_in_id)){
		$conditions = " ( `sysadm_id` = '".$logged_in_id."' AND `sysadm_status` = 'Active')";
		$select_fields = '*';
		$is_multy_result = 1;
		$admindata = $CI->BlankModel->getTableData('sysadmin', $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}	
}
  

 function file_upload($path='',$fieldname='')
  {
		$CI = &get_instance();
		$config['upload_path']          = $path;
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['encrypt_name']         = true;
		$CI->load->library('upload', $config);              
		$CI->upload->do_upload($fieldname);
		$filename = $CI->upload->data();
		return $filename=$filename['file_name'];
  }

  function handle_error($err) {
     	$CI = & get_instance();	
        $error = $err . "\r\n";
        return $error;
    }
    


  function get_user_role($user_id){
     $CI = & get_instance();	
     $conditions = "`user_id` = '".$user_id."' AND `meta_key` ='wp_abd_capabilities'";
     $sql_get_user_meta = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, '*', 1);
     $serialized_data = $sql_get_user_meta['meta_value'];
     $role_array = unserialize($serialized_data);
     $role='';
     foreach($role_array as $key=>$value){
      $role.= $key;
     }
      return $role;
  }	
  
   function get_users_by_role($role, $select_data){
      $CI = & get_instance();	
     
      $get_users_det = $CI->BlankModel->customquery( "SELECT ".$select_data." FROM wp_abd_users INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id WHERE wp_abd_usermeta.meta_key = 'wp_abd_capabilities' AND wp_abd_usermeta.meta_value LIKE '%".$role."%' ORDER BY wp_abd_users.user_nicename");
     
      return $get_users_det;
  }	  
     
     
   function get_users_by_role_n_status($role, $select_data, $status){
      $CI = & get_instance();     
      $get_users_det = $CI->BlankModel->customquery("SELECT ".$select_data." 
						FROM wp_abd_users AS u
						LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
						LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
						WHERE  um1.meta_key = '_status' AND um1.meta_value = '".$status."'
						AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%".$role."%' ORDER BY u.user_nicename ASC");
    
      return $get_users_det;
  }	  
   
   function get_user_meta($user_id, $key, $single = ""){
      $CI = & get_instance();    
      
      if(!empty($user_id)){
      $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
      $select_fields = 'meta_value';
    $is_multy_result = 1;
    $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
      return $get_users_det;
  }else {
    return false;
  }
  }

  function get_user_meta_value($user_id, $key, $single = ""){
      $CI = & get_instance();
      if(!empty($user_id)){     
        $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
        $select_fields = 'meta_value';
      $is_multy_result = 1;
      $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
        return $get_users_det['meta_value'];
      }else{
        return false;
      }
  }
  
   function get_role($sdata)
   {
     $role_array = unserialize($sdata);
     $role='';
     foreach($role_array as $key=>$value){
     	$role.= $key;
	 }
	 return $role;
   }

   function update_user_meta($user_id, $key, $value){
      $CI = & get_instance();	     
      $update_conditions = "( `user_id` = ".$user_id." AND `meta_key` = '".$key."' )";    
	  $update_data = array('meta_value' => $value);	
	  $update_user_meta = $CI->BlankModel->editTableData('wp_abd_usermeta', $update_data, $update_conditions);     
      return $update_user_meta;
  }

  function add_user_meta($user_id,$key,$value){
      $CI = & get_instance(); 
      $insert_data = array(
        'user_id' => $user_id,
        'meta_key' => $key,
        'meta_value' => $value);  
      $add_user_meta = $CI->BlankModel->addTableData('wp_abd_usermeta', $insert_data);     
      return $add_user_meta;
  }


///////////////////////////////////////////added by me on 21.8.19//////////////
  function get_total_user(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT count(*) as totuser FROM wp_abd_users WHERE user_status=0");
    return $get_users_det[0]['totuser'];
  }
  
  function get_latest_user(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT * FROM wp_abd_users WHERE user_status=0 ORDER BY user_registered desc LIMIT 5");
    return $get_users_det;
  }  
 
  function get_latest_login(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT u1.ID,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS lastlogincount,m3.meta_value AS lastlogin FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m1 
      ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
      JOIN wp_abd_usermeta m2 
      ON (m2.user_id = u1.ID AND m2.meta_key = 'when_last_login_count') 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'when_last_login') 
      ORDER BY m3.meta_value DESC LIMIT 5");
    return $get_users_det;
  }
  

  function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
 }

 
 
 /**
 * Get SD Number form Accessioning Number
 * 
 */
 
 function get_sd_number($accessioning_num){
 	
 	  $CI = & get_instance(); 
      $sd_number_det = $CI->BlankModel->customquery( "SELECT `sd_number` FROM `wp_abd_specimen` WHERE `assessioning_num`= '$accessioning_num' ");
     
     return $sd_number_det;
 	
 	
 }







?>