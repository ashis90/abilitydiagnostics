import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDataEntryPeopleComponent } from './edit-data-entry-people.component';

describe('EditDataEntryPeopleComponent', () => {
  let component: EditDataEntryPeopleComponent;
  let fixture: ComponentFixture<EditDataEntryPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDataEntryPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDataEntryPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
