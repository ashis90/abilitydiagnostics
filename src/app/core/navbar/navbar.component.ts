import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserserviceService } from '../../userservice.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { GrowlerService, GrowlerMessageType } from '../growler/growler.service';
import { LoggerService } from '../services/logger.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';
import { NotificationService } from '../../notification.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'cm-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

    deleteArray:any = [];
    message = false;
    modalRef: BsModalRef;
    laboratory:boolean=false;
    labdoctor:boolean=false;
    office_manager:boolean=false;
    data_entry_oparator:boolean=false;
    aps_sales_manager: boolean = false;
    generic_account:  boolean=false;
    loggedIn: boolean=true;
    acc_cntrl:boolean=false;
    acc_cntrl_qc:boolean=false;
    public url = environment.assetsUrl;
    constructor(private router: Router,public itemsService:UserserviceService,public activatedRoute : ActivatedRoute,public notificationService : NotificationService,private modalService: BsModalService) { }
 

    ngOnInit() {
        if(localStorage.loggedIn==='true'){
            this.loggedIn=false;
            //console.log(localStorage.user_role);
            if(localStorage.user_role==='laboratory')
            {
                this.laboratory = true;
                this.labdoctor = false;
                this.office_manager = false; 
                this.data_entry_oparator = false;
                this.generic_account = false;
            }
            else if(localStorage.user_role==='labdoctor')
            {
                this.laboratory = false;
                this.labdoctor = true;
                this.office_manager = false; 
                this.data_entry_oparator = false;
                this.generic_account = false;
            }
            else if(localStorage.user_role==='office_manager')
            {
                this.laboratory =false;
                this.labdoctor = false;
                this.office_manager = true; 
                this.data_entry_oparator = false;
                this.generic_account = false;
            }
            else if(localStorage.user_role==='aps_sales_manager')
            {
                this.laboratory =false;
                this.labdoctor = false;
                this.office_manager = false; 
                this.data_entry_oparator = false;
                this.generic_account = false;
                this.aps_sales_manager = true;
            }
            else if(localStorage.user_role==='data_entry_oparator')
            {
                this.laboratory = false;
                this.labdoctor = false;
                this.office_manager = false; 
                this.data_entry_oparator = true;
                this.generic_account = false;
                console.log(localStorage.acc_cntrl);
                if(localStorage.acc_cntrl === 'add'){
                    this.acc_cntrl = true;
                }
                if(localStorage.acc_cntrl_qc === 'check'){
                    this.acc_cntrl_qc= true;
                }
            }
            else if(localStorage.user_role==='generic_account')
            {
                this.laboratory = false;
                this.labdoctor = false;
                this.office_manager = false; 
                this.data_entry_oparator = false;
                this.generic_account = true;
            }
        }else{
            this.loggedIn=true;
        }
        
       
    }

    ngOnDestroy() {
        
    }

    logout_user()
    { 
        this.itemsService.logout(localStorage.userToken).subscribe(data => {
        this.deleteArray = data;
        localStorage.removeItem('userToken');
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('userid');
        localStorage.removeItem('user_role');
        localStorage.removeItem('acc_cntrl');
        localStorage.removeItem('acc_cntrl_qc');
        if(this.deleteArray.status ==1)
        {
          this.message = true;
          this.loggedIn=true;
          this.router.navigate(["/home"]);
        }
        }
        
        );
    }

    displayPendingNotifications(){
        let post_array;
        let pendingData;
        let params = {'user_id':localStorage.userid};
        this.notificationService.getPendingNotifications(params).subscribe(data => {
            post_array = data; 
            if(post_array['status']==='1'){
                pendingData = post_array.pending_notifications;
            }
        this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
            initialState: {
                title: 'sixth',
                data:pendingData
            }
        });
        });
    }
   

}
