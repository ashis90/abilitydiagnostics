import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  @ViewChild('popupImg') popupImg: ElementRef;
  postsArray: any = [];
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/inner_banner.jpg';
  constructor(
    private http: HttpClient,
    public itemsService: FrontendService,
    public route: Router,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {

    this.getuser();

  }
  getuser() {
    this.spinner.show();
    this.itemsService.makeServiceRequestForNails().subscribe(data => {
      this.postsArray = data
      console.log(this.postsArray);
      this.spinner.hide();
    });

  }

  openLightBox(e) {
    let popup_content = this.popupImg.nativeElement;
    popup_content.src = e;
  }

}
