<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Analytics extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {}
     
  /**
	* /
	*  @ get_physician_active_list
	*
	*/
 function get_physician_active_list(){
 	    $physicians_data = array();
        $physicians_det = array();
        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
       
        foreach($physician_details as $key => $physician){        
			$physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['ID'];        
	        array_push($physicians_data, $physicians_det);
        }         
        echo json_encode(array('status'=>'1',  'physicians_data' => $physicians_data ));
   }
   
    
    function get_lab_anlytics()
    {
        $details = array();
      //Current month
        $currentmonth              = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $currentmonthday           = mktime(0, 0, 0, date("m"),date("d"),date("Y"));
        $Current_Month             = date('M j',  $currentmonth);
        $Current_Month_Day         = date('M j',  $currentmonthday);
        $current_Month_LastDay     = date('t', strtotime(date('Y-m-d',$currentmonth)) );
        $Current_Month_present_Day = date('j',    $currentmonthday);
        $query_current_month       = date('Y-m-d',$currentmonth);
        $query_current_month_day   = date('Y-m-d',$currentmonthday);
        // last month
        $lastmonth                 = mktime(0, 0, 0, date("m")-1, 1, date("Y"));
        $Last_Month_Name           = date('M', $lastmonth); 
        $Last_Month                = date('M j', $lastmonth) ; 
        $Last_Month_Day            = date('t', strtotime(date('Y-m-d',$lastmonth)) );
        $query_last_month          = date('Y-m-d', $lastmonth);
        $query_last_month_days     = mktime(0, 0, 0, date("m")-1,$Last_Month_Day,date("Y"));
        $query_last_month_day      = date('Y-m-d', $query_last_month_days);
        //Current Month and date Details of Specimen
        $current_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_specimen_added_count = $this->BlankModel->customquery($current_date_specimen_count_sql);
        //Previous Month and date Details of Specimen
        $previous_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $previous_date_specimen_added_count = $this->BlankModel->customquery($previous_date_specimen_count_sql);
        //Current Month and date Details of Qc Check List
        $current_date_QcCheck_List_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_QcCheck_List_count = $this->BlankModel->customquery($current_date_QcCheck_List_count_sql);

        //Current Month Physician Count
        $get_users_sql = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_current_month." 00:00:00' AND '".$query_current_month_day." 23:59:59'";
        $users = $this->BlankModel->customquery($get_users_sql);
        $current_month_physician_count = count($users);
        
        //Previous Month Physician Count
        $get_users_sql_last = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_last_month." 00:00:00' AND '".$query_last_month_day." 23:59:59'";
        $users_last = $this->BlankModel->customquery($get_users_sql_last);
        $previous_month_physician_count = count($users_last);

        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
        $total_physician_count = count($physician_details);

        //Current Month and date Details of Nail Reports
        $current_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Inactive'";
        $current_date_pending_report_added_count = $this->BlankModel->customquery($current_date_pending_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Inactive'";
        $previous_date_pending_report_added_count = $this->BlankModel->customquery($previous_date_pending_report_count_sql);

        //Current Month and date Details of Nail Reports
        $current_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 'Active'";
        $current_date_issued_report_added_count = $this->BlankModel->customquery($current_date_issued_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Active'";
        $previous_date_issued_report_added_count = $this->BlankModel->customquery($previous_date_issued_report_count_sql);

        $active_physician_sql = "SELECT DISTINCT `physician_id` FROM `wp_abd_specimen`";
        $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
        $count_active = count($active_phy_count);

        if(!empty($users))
        {
            $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
        }
        else
        {
            $physicians_sending_specimens_count=0;  
        }
        if(!empty($users_last))
        {
            $pre_physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users_last);
        }
        else
        {
            $pre_physicians_sending_specimens_count=0;
        }
       
        $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '1')";
        $stages1 = $this->BlankModel->customquery($stage1_sql);
        $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2')";
        $stages2 = $this->BlankModel->customquery($stage2_sql);
        $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '3')";
        $stages3 = $this->BlankModel->customquery($stage3_sql);
        $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '4')";
        $stages4 = $this->BlankModel->customquery($stage4_sql);
       
        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day, 'last_Month'=>$Last_Month, 
        'last_Month_Name'=>$Last_Month_Name, 'last_Month_Day'=>$Last_Month_Day,'current_specimen_count'=>$current_date_specimen_added_count[0]['number_of_id'],
        'previous_specimen_count'=>$previous_date_specimen_added_count[0]['number_of_id'],'current_phy_count'=>$current_month_physician_count, 'pre_phy_count'=>$previous_month_physician_count,
        'specimen_per_phy_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_phy_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'specimen_per_active_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'pre_physicians_sending_specimens_count'=>$pre_physicians_sending_specimens_count,
        'report_issued_curr'=>$current_date_issued_report_added_count[0]['issued_report_count'],
        'report_issued_prev'=>$previous_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$current_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$current_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function search_get_lab_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $spe_opt = $data['histo_pcr'];
        $tbl1=SPECIMEN_MAIN;
        $tbl2 = CLINICAL_INFO_MAIN;
        $tbl3 = NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl4 = SPECIMEN_STAGE_DETAILS_MAIN;
        $dataType = $data['dataType'];
        if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2 = CLINICAL_INFO_ARCHIVE;
            $tbl3 = NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl4 = SPECIMEN_STAGE_DETAILS_ARCHIVE;
        }else{
            $tbl1=SPECIMEN_MAIN;
            $tbl2 = CLINICAL_INFO_MAIN;
            $tbl3 = NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl4 = SPECIMEN_STAGE_DETAILS_MAIN;
        }
        if(empty($spe_opt))
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM $tbl1 WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);

            }
            else if($spe_opt == 'Histo')
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id
                                                FROM $tbl1 s1
                                                INNER JOIN $tbl2 s2
                                                ON s1.`id` = s2.`specimen_id` 
                                                 WHERE (s2.`nail_unit`  LIKE '%1%' OR s2.`nail_unit`  LIKE '%2%' OR s2.`nail_unit`  LIKE '%3%')
                                                AND s1.`status` = '0'
                                                AND s1.`qc_check` = '0'
                                                AND s1.`physician_accepct` = '0' 
                                                AND s1.`create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59'"; 

            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);
            }
            else if($spe_opt == 'PCR')
            {
                $PCR_search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id
                                                FROM $tbl1 s1
                                                INNER JOIN $tbl2 s2
                                                ON s1.`id` = s2.`specimen_id` 
                                                 WHERE (s2.`nail_unit`  LIKE '%4%' OR s2.`nail_unit`  LIKE '%5%' OR s2.`nail_unit`  LIKE '%6%' OR s2.`nail_unit`  LIKE '%7%')
                                                AND s1.`status` = '0'
                                                AND s1.`qc_check` = '0'
                                                AND s1.`physician_accepct` = '0' 
                                                AND s1.`create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59'";
                $search_date_specimen_count = $this->BlankModel->customquery($PCR_search_date_specimen_count_sql);								

            }
            $Current_Month     = date('M j Y', strtotime($from_date));
            $Current_Month_Day = date('M j Y',  strtotime($to_date));
            $get_users_sql = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $users = $this->BlankModel->customquery($get_users_sql);
            $search_physician_count = count($users);
            
            $query_from_month = date('Y-m-d',strtotime($from_date));
            $query_to_month = date('Y-m-d',strtotime($to_date));
            
            $get_users_sql_new = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $physician_arg = $this->BlankModel->customquery($get_users_sql_new);
            $physicians_specimen_count = count($physician_arg);
            
            $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
            $total_physician_count = count($physician_details);

            $search_date_QcCheck_List_count_sql ="SELECT count(`id`) as number_of_id FROM $tbl1 WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_QcCheck_List_count = $this->BlankModel->customquery($search_date_QcCheck_List_count_sql);
            $search_date_pending_report_count_sql ="SELECT count(`nail_funagl_id`) as pending_report_count FROM $tbl3 WHERE `status` = 'Inactive'";
            $search_date_pending_report_added_count =$this->BlankModel->customquery($search_date_pending_report_count_sql);
            
            if($spe_opt == 'PCR')
            {
            $search_date_issued_report_count_sql ="SELECT count(`id`) as issued_report_count FROM `wp_abd_auto_mail` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `report_id` = '0'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }
            else
            {
            $search_date_issued_report_count_sql ="SELECT count(`nail_funagl_id`) as issued_report_count FROM $tbl3 WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 'Active'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }

            $active_physician_sql = "SELECT DISTINCT `physician_id` FROM $tbl1";
            $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
            $count_active = count($active_phy_count);

            if(!empty($users))
            {
                $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
            }
            else
            {
                $physicians_sending_specimens_count=0;  
            }
            $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM $tbl1 WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `status` = 'Complete' AND `stage_id` = '1')";
            $stages1 = $this->BlankModel->customquery($stage1_sql);
            $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM $tbl1 WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `status` = 'Complete' AND `stage_id` = '2')";
            $stages2 = $this->BlankModel->customquery($stage2_sql);
            $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM $tbl1 WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `status` = 'Complete' AND `stage_id` = '3')";
            $stages3 = $this->BlankModel->customquery($stage3_sql);
            $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM $tbl1 WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `status` = 'Complete' AND `stage_id` = '4')";
            $stages4 = $this->BlankModel->customquery($stage4_sql);

        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day,
        'current_specimen_count'=>$search_date_specimen_count[0]['number_of_id'],
        'current_phy_count'=>$search_physician_count,
        'specimen_per_phy_current'=>round($search_date_specimen_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($search_date_specimen_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'report_issued_curr'=>$search_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$search_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$search_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function get_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC LIMIT 0,10";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
	
        foreach($specimen_data as $histo_pcr_ana)
        {
					
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

        $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";	
        $specimen_resultss = $this->BlankModel->customquery($specimen_det);
        $count = count($specimen_resultss);

        $negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
        $negative_results = $this->BlankModel->customquery($negative_sql);
        $count_negative= count($negative_results);

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }


    function get_more_histo_pcr_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data)){
            $start = $data['load'];
            $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC LIMIT $start,10";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
    
        foreach($specimen_data as $histo_pcr_ana)
        {
                    
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

        $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";    
        $specimen_resultss = $this->BlankModel->customquery($specimen_det);
        $count = count($specimen_resultss);

        $negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
        $negative_results = $this->BlankModel->customquery($negative_sql);
        $count_negative= count($negative_results);

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

                            
        $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
                
         array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
         
         }
             echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));     
        }
        else
        {
             echo json_encode(array('status'=>'0'));
        }

        }
    }

    function get_all_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
	
        foreach($specimen_data as $histo_pcr_ana)
        {
					
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));
        $count =0;
        $count_negative=0;
        $count_result = '';

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);
        if(!empty($run_sql_results)){

            for($i=0;$i<count($run_sql_results);$i++){

                if($run_sql_results[$i]['positive_negtaive'] == 'positive'){
                   $count++; 
                }
                if($run_sql_results[$i]['positive_negtaive'] == 'negative'){
                   $count_negative++; 
                }
            }
            
        }
        
        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}
    }

    function search_all_histo_pcr_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $dataType = $data['dataType'];
        $tbl1=SPECIMEN_MAIN;
        $tbl2=CLINICAL_INFO_MAIN;
        $tbl3=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl4=IMPORT_DATA_MAIN;

        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=CLINICAL_INFO_MAIN;
            $tbl3=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl4=IMPORT_DATA_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=CLINICAL_INFO_ARCHIVE;
            $tbl3=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl4=IMPORT_DATA_ARCHIVE;
        }
       /* if($data['acc_no'])
        {
            $search.= " AND `specimen`.`assessioning_num` = '".$data['acc_no']."'";
        }*/
        if($data['from_date'] != "" &&  $data['to_date'] != ""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";
        }

        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM $tbl1 WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM $tbl3)report
        ON
        `specimen`.`id` = `report`.`specimen_id`".$search." ORDER BY `report`.`nail_funagl_id` DESC";
        $histo_pcr_ana = $this->BlankModel->customquery($specimen_sql);
        $spe_count = count($histo_pcr_ana);
        if($histo_pcr_ana) {
	
        for($j=0;$j<$spe_count;$j++){
					
        $specimen_id = $histo_pcr_ana[$j]['id'];
        $accessioning_num = $histo_pcr_ana[$j]['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana[$j]['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana[$j]['report_create_date']));
        $count =0;
        $count_negative=0;
        $count_result = '';

        $run_sql =  "SELECT * FROM $tbl4 WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($run_sql_results)){

            for($i=0;$i<count($run_sql_results);$i++){

                if($run_sql_results[$i]['positive_negtaive'] == 'positive'){
                   $count++; 
                }
                if($run_sql_results[$i]['positive_negtaive'] == 'negative'){
                   $count_negative++; 
                }
            }
            
        }

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }
  //   function get_lab_data_anlytics()
  //   {   
  //       $data = json_decode(file_get_contents('php://input'), true);
  //       $search = "";
  //       $date= date('Y-m-d');
  //       $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '2018-03-01 00:00:00' AND '".$date." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
  //       $run_sql_results = $this->BlankModel->customquery($specimen_sql);
  //       $count = count($run_sql_results);
  //       $data_display_date = "Data display from March 01, 2018 to the current date.";

  //       if($run_sql_results)
  //       {
  //           echo json_encode(array('status'=>'1','count' => $count,"details" => $run_sql_results,'data_display_date' =>$data_display_date));	 
  //       }
  //       else
  //       {
		// 	 echo json_encode(array('status'=>'0'));
		// }

  //   }

    function get_lab_data_anlytics()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $date= date('Y-m-d');

        $new_array = array();

        $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '2018-03-01 00:00:00' AND '".$date." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        $count = count($run_sql_results);
        $data_display_date = "Data display from March 01, 2018 to the current date.";
        foreach($run_sql_results as $val)
        {
        $new_array_data = array(
			'assessioning_num' =>  $val['assessioning_num'],
			'p_firstname'=>  trim($val['p_firstname']),
			'p_lastname'        =>  $val['p_lastname'],
			'create_date'       =>  $val['create_date'],
			'patient_age'        => (int)$val['patient_age'],
			'id'        => (int) $val['id'],
		);
		array_push($new_array,$new_array_data);
        }
        if($run_sql_results)
        {
            echo json_encode(array('status'=>'1','count' => $count,"details" => $new_array,'data_display_date' =>$data_display_date));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }
    
    function search_lab_data_anlytics()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $date= date('Y-m-d');
        $dataType = $data['dataType'];
        $tbl1=SPECIMEN_MAIN;
        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
        }
        $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM $tbl1 WHERE `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        $count = count($run_sql_results);
        $data_display_date = "Data display from ".$data['from_date']." to ".$data['to_date'];

        if($run_sql_results)
        {
            echo json_encode(array('status'=>'1','count' => $count,"details" => $run_sql_results,'data_display_date' =>$data_display_date));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}


    }

    function get_short_code_anlytics()
    {
        $details =  array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`addendum_desc`, `report`.`addendum`, `report`.`stains`,`report`.`gross_description`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date` FROM 
        (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN (SELECT * FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        
        $count = count($run_sql_results);
        if($run_sql_results)
        {
            foreach($run_sql_results as $specimen)
            {
                $physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name', '' );
                $physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name' , '' );
                $phy_name =  $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
                $specimen_information =  array('id' => $specimen['id'], 'assessioning_num' => $specimen['accessioning_number'], 'physician_name' => $phy_name, 'patient_name' => $specimen['patient_name'], 'patient_age' => $specimen['patient_age'], 'patient_state' => $specimen['patient_state'],'patient_city' => $specimen['patient_city'],'report_create_date' => $specimen['report_create_date'],'gross_description' => $specimen['gross_description'],'short_code'=>$specimen['diagnostic_short_code']);
                array_push($details, $specimen_information);
		
            }
            echo json_encode(array('status'=>'1','count' => $count,"details" => $details));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}
    }

    /**
	* 
	* Search Short Code
	*/

     function search_short_code_anlytics()
     {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $tbl1=SPECIMEN_MAIN;
        $tbl2=CLINICAL_INFO_MAIN;
        $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
        $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl5=GENERATE_PCR_REPORT_MAIN;
        $tbl6=ASSIGN_STAGE_MAIN;
        $tbl7=IMPORT_DATA_MAIN;
        if($data['dataType'] == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=CLINICAL_INFO_MAIN;
            $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
            $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl5=GENERATE_PCR_REPORT_MAIN;
            $tbl6=ASSIGN_STAGE_MAIN;
            $tbl7=IMPORT_DATA_MAIN;
        }else if($data['dataType'] == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=CLINICAL_INFO_ARCHIVE;
            $tbl3=SPECIMEN_STAGE_DETAILS_ARCHIVE;
            $tbl4=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl5=GENERATE_PCR_REPORT_ARCHIVE;
            $tbl6=ASSIGN_STAGE_ARCHIVE;
            $tbl7=IMPORT_DATA_ARCHIVE;
        }
		  
            if($data['from_date']!="" && $data['to_date']!=""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
            }
            if($data['report_code']!=""){
            $search.= " AND `report`.`diagnostic_short_code` LIKE '%".$data['report_code']."%'";
            }
            if($data['patient_age_from']!="" && $data['patient_age_to']!=""){
            $search.= " AND `specimen`.`patient_age` BETWEEN '".$data['patient_age_from']."' AND '".$data['patient_age_to']."'";
            }
            if($data['sex']!=""){
            $search.= " AND `specimen`.`patient_sex`='".$data['sex']."'";
            }
            if($data['patient_name']!=""){
            $search.= " AND (CONCAT( `specimen`.`p_firstname`, ' ', `specimen`.`p_lastname`) LIKE '%".$data['patient_name']."%')";
            } 
            if($data['state']!=""){
            $search.= " AND `specimen`.`patient_state` LIKE '%".$data['state']."%'";
            }
            if($data['city']!=""){
            $search.= " AND `specimen`.`patient_city` LIKE '%".$data['city']."%'";
            }
            if($data['stain_code']!=""){
            $search.= " AND `report`.`stains` LIKE '%".$data['stain_code']."%'";
            } 
         
            if($data['acc_no']!=""){
            $search.= " AND `specimen`.`assessioning_num` LIKE '%".$data['acc_no']."%'";
            }
            if($data['physician_id']!=""){
            $search.= " AND `specimen`.`physician_id` = '".$data['physician_id']."'";
            }
          
            $details =  array();
            $specimen_sql = "SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`gross_description`, `report`.`addendum_desc`, `report`.`addendum`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date`, `report`.`stains`  
            FROM 
            (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM $tbl1 WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
          
            INNER JOIN (SELECT `diagnostic_short_code`, `specimen_id`, `nail_funagl_id`, `gross_description`, `addendum_desc`, `addendum`, `create_date`, `stains`  FROM $tbl4)report
            ON
            `specimen`.`id` = `report`.`specimen_id` ".$search." ORDER BY `specimen`.`assessioning_num` DESC";
           
            $run_sql_results = $this->BlankModel->customquery($specimen_sql);
            
            $count = count($run_sql_results);
            if($run_sql_results){   
                foreach($run_sql_results as $specimen){
                
                    $pos_count      =  0;
                    $count_result   = "";
                    $physician_first_name = get_user_meta_value( $specimen['physician_id'], 'first_name', '' );
                    $physician_last_name  = get_user_meta_value( $specimen['physician_id'], 'last_name' , '' );
                    $phy_name             = $physician_first_name.' '.$physician_last_name;
           
                    $specimen_id = $specimen['id'];
                    $accessioning_num = $specimen['accessioning_number']; 

                    $import_data_sql = "SELECT COUNT( `positive_negtaive` ) AS `count_data`, `positive_negtaive` FROM $tbl7 WHERE `accessioning_num` = '".$accessioning_num."' GROUP BY `positive_negtaive` ";
                    
                    $import_data_results = $this->BlankModel->customquery($import_data_sql);


                    if(!empty($import_data_results)){

                            foreach($import_data_results as $pos_nev_count){
						    if($pos_nev_count['positive_negtaive'] == 'positive'){                      	
                               $count_result_pos = $pos_nev_count['count_data']." Pos ";                        
                               $pos_count = $pos_nev_count['count_data'];                                                           
                            }
                            
                            if($pos_nev_count['positive_negtaive'] == 'negative'){
                               $count_result_neg = $pos_nev_count['count_data']." Neg";                      
                            }
                          	}                     
                            $count_result = $count_result_pos.$count_result_neg;
                    }
                    else{
						    $count_result = "Not Run";
					}                    
                    if( ($pos_count < 1) && (!empty($import_data_results)) ){
						    $count_result = "All Negative";
					} 
                     
                
                    $specimen_information = array('id' => $specimen_id, 'assessioning_num' => $accessioning_num, 'physician_name' => $phy_name, 'patient_name' => $specimen['patient_name'], 'patient_age' => $specimen['patient_age'], 'patient_state' => $specimen['patient_state'],'patient_city' => $specimen['patient_city'],'report_create_date' => $specimen['report_create_date'], 'gross_description' => $specimen['gross_description'], 'short_code'=> $specimen['diagnostic_short_code'], 'count_result' => $count_result );
                  
                    array_push($details, $specimen_information);
                     
                 }
                echo json_encode(array('status' => '1', 'count' => $count, 'details' => $details ));	 
            }
            else
            {
                 echo json_encode(array('status'=>'0','msg' => 'No data found'));
            } 
                 
            
    }

	 /**
	 * 
	 * Export Short Code
	 */
    function export_short_code_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $tbl1=SPECIMEN_MAIN;
        $tbl2=CLINICAL_INFO_MAIN;
        $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
        $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl5=GENERATE_PCR_REPORT_MAIN;
        $tbl6=ASSIGN_STAGE_MAIN;
        $tbl7=IMPORT_DATA_MAIN;
        if($data['dataType'] == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=CLINICAL_INFO_MAIN;
            $tbl3=SPECIMEN_STAGE_DETAILS_MAIN;
            $tbl4=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl5=GENERATE_PCR_REPORT_MAIN;
            $tbl6=ASSIGN_STAGE_MAIN;
            $tbl7=IMPORT_DATA_MAIN;
        }else if($data['dataType'] == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=CLINICAL_INFO_ARCHIVE;
            $tbl3=SPECIMEN_STAGE_DETAILS_ARCHIVE;
            $tbl4=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl5=GENERATE_PCR_REPORT_ARCHIVE;
            $tbl6=ASSIGN_STAGE_ARCHIVE;
            $tbl7=IMPORT_DATA_ARCHIVE;
        }
            if($data['from_date']!="" && $data['to_date']!=""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
            }
            if($data['report_code']!=""){
            $search.= " AND `report`.`diagnostic_short_code` LIKE '%".$data['report_code']."%'";
            }
            if($data['patient_age_from']!="" && $data['patient_age_to']!=""){
            $search.= " AND `specimen`.`patient_age` BETWEEN '".$data['patient_age_from']."' AND '".$data['patient_age_to']."'";
            }
            if($data['sex']!=""){
            $search.= " AND `specimen`.`patient_sex`='".$data['sex']."'";
            }
            if($data['patient_name']!=""){
            $search.= " AND (CONCAT( `specimen`.`p_firstname`, ' ', `specimen`.`p_lastname`) LIKE '%".$data['patient_name']."%')";
            } 
            if($data['state']!=""){
            $search.= " AND `specimen`.`patient_state` LIKE '%".$data['state']."%'";
            }
            if($data['city']!=""){
            $search.= " AND `specimen`.`patient_city` LIKE '%".$data['city']."%'";
            }
            if($data['stain_code']!=""){
            $search.= " AND `report`.`stains` LIKE '%".$data['stain_code']."%'";
            }                       
            if($data['acc_no']!=""){
            $search.= " AND `specimen`.`assessioning_num` LIKE '%".$data['acc_no']."%'";
            }
            if($data['physician_id']!=""){
            $search.= " AND `specimen`.`physician_id` = '".$data['physician_id']."'";
            }                 

            $details =  array();            
            $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`gross_description`, `report`.`addendum_desc`, `report`.`addendum`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date`, `report`.`stains`,`report`.`internal_short_code`,`report`.`internal_short_code_comments`
            FROM 
            (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM $tbl1 WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
            
            INNER JOIN (SELECT `diagnostic_short_code`, `specimen_id`, `nail_funagl_id`, `gross_description`, `addendum_desc`, `addendum`, `create_date`, `stains`,internal_short_code,internal_short_code_comments  FROM $tbl4)report
            ON
            `specimen`.`id` = `report`.`specimen_id` ".$search." ORDER BY `specimen`.`assessioning_num` DESC";
           
   
            $run_sql_results = $this->BlankModel->customquery($specimen_sql);           
            $count = count($run_sql_results);
            $field_arr = array();
            $api_data = array();
            $target_arr = array();
            $neg_arr = array();
            $access_arr = array();
            $selected_fields['physician_id'] = $data['physicianId'] ;       
            $selected_fields['accessioning_number'] = $data['accessioning_number'] ;      
            $selected_fields['patient_sex'] = $data['gender'] ;        
            $selected_fields['patient_name'] = $data['patientName'];       
            $selected_fields['patient_age'] = $data['patient_age'];          
            $selected_fields['patient_state'] = $data['patient_state'];    
            $selected_fields['patient_city'] = $data['patient_city'];        
            $selected_fields['report_create_date'] = $data['report_genarate_date'];       
            $selected_fields['diagnostic_short_code'] = $data['diagnostic_short_code'];         
            $selected_fields['stains'] = $data['stains'];         
            $selected_fields['gross_description'] = $data['gross_dimension'];           
            $selected_fields['pcr_test'] = $data['pcr_test'];       
            $selected_fields['addendum_desc'] = $data['addendum'];  
            $selected_fields['internal_short_code'] = $data['addn_comment'];        
            
            $filter_array = array_filter($selected_fields);
            $fields_Name = array_keys($filter_array);
           
        
           if($run_sql_results){
               foreach($run_sql_results as $specimen_result){
                    $schema_insert = "";
                   
                    foreach ($fields_Name as $value){
                       
                        if($value == "physician_id"){
                        $field_value = get_user_meta_value($specimen_result[$value], 'first_name', true).' '.get_user_meta_value($specimen_result[$value], 'last_name', true);
                        }                      
                       else if($value == "pcr_test"){                        	
			            $field_value = "";
                        $spe_acc_no  = $specimen_result['accessioning_number'];                           
			            $import_data_sql = "SELECT `target_name`,`accessioning_num`, `positive_negtaive` FROM $tbl7 WHERE `accessioning_num` = '".$spe_acc_no."'";
			            $import_data_results = $this->BlankModel->customquery($import_data_sql);
		                     
                        $target_name = "";
                        $neg_arr = array();
                        if(!empty($import_data_results)){
                        
                            foreach ($import_data_results as $key) {
                               $count_negative = 0;
                              
                            if( $key['accessioning_num'] == $spe_acc_no && $key['positive_negtaive'] == 'positive'){
                              
                                $nw_arr = array( 'target_name' => $key['target_name'] );
                                array_push($target_arr, $nw_arr);


                                if($key['target_name'] != "Xeno_Ac00010014_a1"){
                                
                                    $assay_name = explode("_", $key['target_name']);
                                    $excel_data = $assay_name[0]." >> ";
                                    $target_name.= $excel_data;	
                                    }
                            }
                            
                            if($key['accessioning_num'] == $spe_acc_no && $key['positive_negtaive'] == 'negative'){
                                array_push($neg_arr, $key['positive_negtaive']);
                            }
                            
                            if($key['accessioning_num'] == $spe_acc_no){
                                $nw_arr = array('accessioning_num'=>$key['accessioning_num']);
                                array_push($access_arr, $nw_arr);
                            }
                        }
                        
                        $target_count   = count($target_arr);
                        $count_negative = count($neg_arr);
                 
                        if(!empty($target_arr)){
                            if(!empty($count_negative)){ 
                                $negative_count = $count_negative." Neg";
                            }             
                            $field_value = $target_name.' '.$negative_count ;
                        }

                        if (($target_count < 1) && (!empty($access_arr['accessioning_num']))){
                            $field_value = "All Negative";
                        }
                       
                        }
                        else{
						    $field_value = "Not Run";
					        }    
                        }
                      
                        else if($value == "addendum_desc"){
                            $field_value = "";
                            $addendum_val = unserialize($specimen_result['addendum_desc']);
                            if(!empty($addendum_val[0]))
                            {
                           	 $rep_field_value='';
                            foreach($addendum_val as $rep_value)
                            {
                           	 $rep_field_value.=  $rep_value.',-';
                            } 
                            
                           	 $field_value = $rep_field_value;
                            }

                            else if($specimen_result['addendum'] != "")
                            {
	                            $nail_addendum = $specimen_result['addendum'];
	                            $addendum_codes  = explode(",",$nail_addendum);
	                            
	                            foreach($addendum_codes as $addendum_code){
		                            if($addendum_code){
		                                $addendum_desc = get_new_stains_desc($addendum_code); 
		                                $field_value = $addendum_desc;
		                            }
	                            }
                            }
                        }
                        
                        else{
                        $field_value = "";
                        $field_value = $specimen_result[$value];
                        }
                                          
                        if($value == 'addendum_desc'){                            
                            $field_arr['addendum_desc'] = $field_value;                           
                        }
                        if($value == 'accessioning_number'){
                            $field_arr['accessioning_number'] = $field_value;
                        }
                        if($value == 'patient_age'){
                            $field_arr['patient_age'] = $field_value;
                        }
                        if($value == 'patient_name'){
                            $field_arr['patient_name'] = $field_value;
                        }
                        if($value == 'patient_sex'){
                            $field_arr['patient_sex'] = $field_value;
                        }
                        if($value == 'patient_city'){
                            $field_arr['patient_city'] = $field_value;
                        }
                        if($value == 'patient_state'){
                            $field_arr['patient_state'] = $field_value;
                        }
                        if($value == 'diagnostic_short_code'){
                            $field_arr['diagnostic_short_code'] = $field_value;
                        }
                        if($value == 'nail_funagl_id'){
                            $field_arr['nail_funagl_id'] = $field_value;
                        }
                        if($value == 'stains'){
                            $field_arr['stains'] = $field_value;
                        }
                        if($value == 'gross_description'){
                            $field_arr['gross_description'] = $field_value;
                        }
                        if($value == 'physician_id'){
                            $field_arr['physician_name'] = $field_value;
                        }
                        if($value == 'report_create_date'){
                            $field_arr['report_create_date'] = $field_value;
                        }
                        if($value == 'pcr_test'){
                            $field_arr['pcr_test'] = $field_value;
                        }  
                        if($value == 'internal_short_code'){
                          $field_arr['internal_short_code'] = $field_value;
                          $field_arr['internal_comment'] = $specimen_result['internal_short_code_comments'];
                      }                       
                    }
               
                    array_push($api_data, $field_arr);                               
                }
    
                die(json_encode(array('status'=>'1', 'details'=> $api_data, 'count' => $count)));
            }
            else{
                 die(json_encode(array('status'=>'0','msg' =>'No data found')));
            }    
            
    }
        
    function get_diagnosis_list()
    {
        $information = array();
        $information2 = array();
        $information3 = array();
        $total_specimen_green_code=0;
        $total_specimen_yellow_code=0;
        $total_specimen_red_code=0;
        $physicians_data = array();
        $physicians_det = array();
        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
        foreach($physician_details as $key => $physician){        
			$physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['ID'];        
            array_push($physicians_data, $physicians_det);
        } 
          
        $green_report_codes = $this->BlankModel->customquery("select * from ".NAIL_MACRO_CODES." where color = 'green'");
        foreach($green_report_codes as $dis){
            $total_green_code =0;
            $specimen_count = specimen_code_count($dis['sc']);
            $total_green_code += $specimen_count;    
            $total_specimen_green_code += $total_green_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $green_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info['count'] = $green_specimen_count;
            array_push($information,$info);
        }
   										
        $yellow_report_codes = $this->BlankModel->customquery("select * from ".NAIL_MACRO_CODES." where color = 'yellow'");
        foreach($yellow_report_codes as $dis)
        {	
            $total_yellow_code =0;	
  	        $specimen_count = specimen_code_count($dis['sc']);
  		    $total_yellow_code += $specimen_count;
            $total_specimen_yellow_code += $total_yellow_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $yellow_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info_yellow['count'] = $yellow_specimen_count;
            array_push($information2,$info_yellow);
  			
        }
  
        $red_report_codes = $this->BlankModel->customquery("select * from ".NAIL_MACRO_CODES." where color = 'red'");
        foreach($red_report_codes as $dis) 
        {	
            $total_red_code =0;	
            $specimen_count = specimen_code_count($dis['sc']);
            $total_red_code += $specimen_count;
            $total_specimen_red_code += $total_red_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $red_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info_red['count'] = $red_specimen_count;
            array_push($information3,$info_red);
        }
        $total_specimen_codes_count = $total_specimen_red_code+	$total_specimen_yellow_code +$total_specimen_green_code;	
        $green_per = round(($total_specimen_green_code / $total_specimen_codes_count) *100);
        $yellow_per = round(($total_specimen_yellow_code / $total_specimen_codes_count) *100);
        $red_per = round(($total_specimen_red_code / $total_specimen_codes_count) *100);
        
        echo json_encode(array('status'=>'1','physicians_data' => $physicians_data,
        'green'=>$total_specimen_green_code,'yellow'=>$total_specimen_yellow_code,'red'=>$total_specimen_red_code,
        'green_per'=>$green_per, 'yellow_per'=>$yellow_per,'red_per'=>$red_per,'short_code1'=>$green_report_codes,
        'short_code2'=>$yellow_report_codes,'short_code3'=>$red_report_codes,'green_info'=>$information,'yellow_info'=>$information2,'red_info'=>$information3));
    }

	 /**
	 * /
	 * 
	 * @ search_diagnosis
	 */

    function search_diagnosis()
    {   
        $green_information = array();
        $yellow_information = array();
        $red_information = array();
        $total_green_report = 0;
        $total_yellow_report =0;
        $total_red_report = 0;
        $total_specimen_red_code = 0;
        $total_green_code = 0;
        $specimen_count = 0;
        $search = "";
        $number_of_specimen = 0;
        $total_specimen_codes_count = 0;
        $data = json_decode(file_get_contents('php://input'), true);
        $dataType = $data['dataType'];
        $tbl1=SPECIMEN_MAIN;
        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
        }
        
		if($data['physician'] !=""){
            $specimen_det = $this->BlankModel->customquery("SELECT `id` FROM $tbl1 WHERE status = '0' AND `physician_id`='".$data['physician']."' AND `physician_accepct` = '0' AND qc_check = '0' AND `create_date` > '2017-03-27 23:59:59'");	
         }
         if($data['from_date']!="" && $data['to_date']!="")
         {  
            $specimen_det = $this->BlankModel->customquery("SELECT `id` FROM $tbl1 WHERE status = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59' AND `physician_accepct` = '0' AND qc_check = '0' AND `create_date` > '2017-03-27 23:59:59'");
         }
         
		  /**
		   * 
		   * @var / Green Report specimen count
		   * 
		   */	        
         $green_report_codes = $this->BlankModel->customquery("select `sc` from ".NAIL_MACRO_CODES." where color = 'green'");
         if($green_report_codes){			
	      foreach($green_report_codes as $green_report_d_s_code) 
          {
            
           $number_of_specimen = 0;
           $info = array();
           foreach($specimen_det as $specimen)
           {                        
            $green_specimen_count = specimen_color_code_count($green_report_d_s_code['sc'] ,$specimen['id'],$dataType);
	        if($green_specimen_count){		 
				$number_of_specimen += $green_specimen_count;
		        $info['count'] = $number_of_specimen;
	          }              
            } 
            array_push($green_information, $info);          
            $total_green_report += $number_of_specimen;		 
		  }         
          $total_specimen_green_code = $total_green_report;      	 
      	 }  
   
		   /**
		   * 
		   * @var / Yellow Report specimen count
		   * 
		   */	  
	      $yellow_report_codes = $this->BlankModel->customquery("select `sc` from ".NAIL_MACRO_CODES." where color = 'yellow'");
        if($yellow_report_codes){			
	      foreach($yellow_report_codes as $yellow_report_d_s_code) 
          {            
           $number_of_specimen = 0;
           $info_yellow = array();
           foreach($specimen_det as $specimen)
           {                        
             $yellow_report_count = specimen_color_code_count($yellow_report_d_s_code['sc'] ,$specimen['id'],$dataType);
	        if($yellow_report_count){		 
				$number_of_specimen += $yellow_report_count;
		        $info_yellow['count'] = $number_of_specimen;
	          }              
            } 
            array_push($yellow_information, $info_yellow);          
            $total_yellow_report += $number_of_specimen;		 
		  }         
          $total_specimen_yellow_code = $total_yellow_report;      	 
      	 }      
      	 
      	 /**
		   * 
		   * @var / Red Report specimen count
		   * 
		   */
          $red_report_codes = $this->BlankModel->customquery("select `sc` from ".NAIL_MACRO_CODES." where color = 'red'");
        if($red_report_codes){			
	      foreach($red_report_codes as $red_report_d_s_code) 
          {            
           $number_of_specimen = 0;
           $info_red = array();
           foreach($specimen_det as $specimen)
           {                        
             $red_report_count = specimen_color_code_count($red_report_d_s_code['sc'] ,$specimen['id'],$dataType);
	        if($red_report_count){		 
				$number_of_specimen += $red_report_count;
		        $info_red['count'] = $number_of_specimen;
	          }              
            } 
            array_push($red_information, $info_red);          
            $total_red_report += $number_of_specimen;		 
		  }         
          $total_specimen_red_code = $total_red_report;      	 
      	 }  

         $total_specimen_codes_count = ($total_specimen_red_code+$total_specimen_yellow_code +$total_specimen_green_code);	
         if($total_specimen_codes_count!=0)
         {
            $yellow_per = round(($total_specimen_yellow_code / $total_specimen_codes_count) *100);
            $red_per = round(($total_specimen_red_code / $total_specimen_codes_count) *100);
            $green_per = round(($total_specimen_green_code / $total_specimen_codes_count) *100);
       
         }
         else
         {
            $yellow_per=0;
            $red_per=0;
            $green_per=0;
         }
         
         echo json_encode(array('status' => '1',
         'green' => $total_specimen_green_code,
         'yellow' => $total_specimen_yellow_code,
         'red' => $total_specimen_red_code,
         'short_code1' => $green_report_codes,
         'short_code2' => $yellow_report_codes,
         'short_code3' => $red_report_codes,
         'green_info' => $green_information,
         'yellow_info' => $yellow_information,
         'red_info' => $red_information,
         'green_per' => $green_per, 
         'yellow_per' => $yellow_per,
         'red_per' => $red_per        
         ));      
        
    }

    function get_avg_time()
    {
        $turn_around_time_specimen_sql = "SELECT DATEDIFF(`nail_report`.`create_date`, `specimen`.`create_date`) as difference, `specimen`.`assessioning_num` FROM (SELECT * FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0')specimen INNER JOIN (SELECT * FROM `wp_abd_nail_pathology_report`) nail_report ON `nail_report`.`specimen_id` = `specimen`. `id`"; 
        $turn_around_time_specimen = $this->BlankModel->customquery($turn_around_time_specimen_sql);
        $count = count($turn_around_time_specimen);
        $turn_around_time = array();
        if($turn_around_time_specimen)
        {
         foreach($turn_around_time_specimen as $data)
         {
         	$result_data['difference']	     = (int)$data['difference'];
			$result_data['assessioning_num'] = $data['assessioning_num']; 
			array_push($turn_around_time, $result_data);   
	     } 
		}
        if(!empty( $turn_around_time ))
        {          	
         echo json_encode(array('status' => '1', 'details' => $turn_around_time, 'count' => $count));   
        }
        else
        {
         echo json_encode(array('status'=>'0'));
        }
    }

    function search_avg_time()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // $turn_around_time_specimen_sql = "SELECT DATEDIFF(`nail_report`.`create_date`, `specimen`.`create_date`) as difference, `specimen`.`assessioning_num`, `nail_report`.`create_date` as 'nail_create_date', `specimen`.`create_date` as 'specimen_create_date' FROM (SELECT * FROM `wp_abd_specimen` WHERE `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0')specimen 
        // INNER JOIN (SELECT * FROM `wp_abd_nail_pathology_report`) nail_report ON `nail_report`.`specimen_id` = `specimen`. `id` AND `specimen`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'"; 
        $total_working_day =0;
        $tbl1=SPECIMEN_MAIN;
        $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
        $dataType = $data['dataType'];
        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
        }
        $search_date_turn_around_time_specimen_sql = 
    "SELECT `specimen`.`assessioning_num`,`specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date
    FROM 
    (SELECT * FROM $tbl1 WHERE `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59')specimen 
    INNER JOIN 
    (SELECT * FROM $tbl2) nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id` ";

    $search_date_turn_around_time_specimen = $this->BlankModel->customquery($search_date_turn_around_time_specimen_sql);

    $search_specimen_count_sql = "SELECT count(`specimen`.`id`) as `specimen_count` 
    FROM 
    (SELECT * FROM $tbl1 WHERE `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59')specimen 
    INNER JOIN 
    (SELECT * FROM $tbl2) nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`";

		$specimen_count_by_date_range = $this->BlankModel->customquery($search_specimen_count_sql);
  
        $get_working_days = "";
        $holiday_sql = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' ";
        $holiday_range = $this->BlankModel->customquery($holiday_sql);  

        foreach($holiday_range as $holidays ){
        $holiday_list[] = $this->getDatesFromRange($holidays['start_date'], $holidays['end_date']);
        }
        $holidays_list = $this->array_flatten($holiday_list);
        $i=0;
        foreach($search_date_turn_around_time_specimen as $specimens){

            $specimen_date = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $nail_date = date('Y-m-d', strtotime($specimens['nail_create_date']));
            $get_working_days = $this->getWorkingDays($specimen_date, $nail_date, $holidays_list)-1;
            $total_working_day += $get_working_days;
            $search_date_turn_around_time_specimen[$i]['working_days'] = $get_working_days;
            $i++;
        }
    
        if($search_date_turn_around_time_specimen)
        {
            echo json_encode(array('status'=>'1','details' => $search_date_turn_around_time_specimen,'count'=>$specimen_count_by_date_range,'total_working_day'=>$total_working_day));   
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }

    //The function returns the no. of business days between two dates and it skips the holidays
function getWorkingDays($startDate,$endDate,$holidays){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }

        return $array;
    }

     function array_flatten($array_list) { 
      if (!is_array($array_list)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array_list as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, $this->array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return $result; 
    } 

    function get_slide_analytics()
    {
        $from_date = date("Y", strtotime("-1 years"));
        $to_date = date("Y");
        $FM_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `fm_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' ";    
        $fm_clinical_datas = $this->BlankModel->customquery($FM_clinical_data_sql);	 
        $tota_pas_gms_fm = $fm_clinical_datas[0]['fm_number_report_count'] * 4 ;
        
        $gms_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `gms_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%2%'";   
        $gms_clinical_data = $this->BlankModel->customquery($gms_clinical_data_sql);	
        
        $tota_pas_gms = $gms_clinical_data[0]['gms_number_report_count'] * 3;

        $clinical_data_PAS_sql = "SELECT COUNT(`nail_unit`) AS `pas_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%3%'";   
        $clinical_PAS_datas = $this->BlankModel->customquery($clinical_data_PAS_sql);	
        
        $tota_pas = $clinical_PAS_datas[0]['pas_number_report_count'] *2;
        $total = $tota_pas_gms_fm + $tota_pas_gms + $tota_pas;

        echo json_encode(array('status'=>'1','first_val' => $tota_pas_gms_fm,'second_val'=>$tota_pas_gms,'third_val'=>$tota_pas,'total'=>$total,'from_date'=>$from_date,'to_date'=> $to_date));   

    }

    function search_slide_analytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $dataType = $data['dataType'];
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $tbl1=CLINICAL_INFO_MAIN;
        if($dataType == 'general'){
            $tbl1=CLINICAL_INFO_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=CLINICAL_INFO_ARCHIVE;
        }
        $search =" AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
        if(!empty($data['from_date']) && !empty($data['to_date']))
        {
        $FM_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `fm_number_report_count` FROM $tbl1 WHERE `nail_unit` LIKE '%1%' ".$search;    
        $fm_clinical_datas = $this->BlankModel->customquery($FM_clinical_data_sql);	 
        $tota_pas_gms_fm = $fm_clinical_datas[0]['fm_number_report_count'] * 4 ;
        
        $gms_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `gms_number_report_count` FROM $tbl1 WHERE `nail_unit` LIKE '%2%'".$search;   
        $gms_clinical_data = $this->BlankModel->customquery($gms_clinical_data_sql);	
        
        $tota_pas_gms = $gms_clinical_data[0]['gms_number_report_count'] * 3;

        $clinical_data_PAS_sql = "SELECT COUNT(`nail_unit`) AS `pas_number_report_count` FROM $tbl1 WHERE `nail_unit` LIKE '%3%'".$search;   
        $clinical_PAS_datas = $this->BlankModel->customquery($clinical_data_PAS_sql);	
        
        $tota_pas = $clinical_PAS_datas[0]['pas_number_report_count'] *2;
        $total = $tota_pas_gms_fm + $tota_pas_gms + $tota_pas;

        echo json_encode(array('status'=>'1','first_val' => $tota_pas_gms_fm,'second_val'=>$tota_pas_gms,'third_val'=>$tota_pas,'total'=>$total,'from_date'=>$from_date,'to_date'=> $to_date));   
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }
   
 /**
   * 
   * Get HISTO Avg time
   */
  
    function get_histo_avg_time()
    {   
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
        (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` NOT IN 
        (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
        (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` limit 0,10");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
            	
            	/**
				* 
				* @var Delivary to Lab 24 hrs.
				* 
				*/
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                /**
				* 
				* @var Gross Description 8hrs.
				* 
				*/
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>28800)  // 8 hrs.
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

					/**
					* 
					* @var Microtomy  24 hrs.
					* 
					*/
                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>86400) // 24 hrs.
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  /**
				  * 
				  * @var QC Check  6 hrs.
				  * 
				  */
				  
                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>21600) // 6 hrs.
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                   /**
				   * 
				   * @var Pending Specimen List 48 hrs.
				   * 
				   */

                    $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>172800) // 48 hrs.
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

					/**
					* 
					* @var Pending Report 48 hrs.
					* 
					*/

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>172800) // 48 hrs.
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
               'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }
            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
    }
    
    /**
	* Load More HISTO Stages Avg Time
	* 
	* @return
	*/
    

    function get_more_histo_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data))
        {
            $start = $data['load'];
            $tot_minutes=0;
            $tot_minutes_stage2=0;
            $tot_minutes_stage3=0;
            $tot_minutes_stage4=0;
            $tot_minutes_pending=0;
            $tot_minutes_stage=0;
            $count =0;
            $details = array();
            $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` NOT IN 
            (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
            (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` limit $start,10");
            if($specimen_results)
            {
                $count= $start+10;
                foreach ($specimen_results as $specimen_data)
                {
                	/**
					* 
					* @var Delivery to Lab
					* 
					*/
                    $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                    if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                    {
                    $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                    $formatted_time= date('Y-m-d H:i:s',$timestamp);
                    $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                    
                    $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                    $tot_minutes+= $minutes;
                    $avg_minutes = floor($tot_minutes/$count);
                    $hour = round(($avg_minutes/60),1);
                    $day = floor($hour/24);
    
                    $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                    if($chk_hour_stage1>86400) // 24 hrs.
                    {
                      $color1 = 'Red';
                      $time = dateDiff($formatted_time,$original_time_stage1);
                    }
                    else
                    {
                      $color1 = '';
                      $time =  dateDiff($formatted_time,$original_time_stage1);
                    }
                    }
                    else
                    {
                      $color1 = '';
                      $time = "Not Processed";
                    }
                    
                    /**
					* 
					* @var Gross Description
					* 
					*/
					
                    $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                      {
                      $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                      $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                      $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage2+= $minutes_stage2;
                      $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                      $hour_stage2 = round(($avg_minutes_stage2/60),1);
                      $day_stage2 = floor($hour_stage2/24); 
    
                      $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage2>28800)// 8 hrs.
                      {
                       $color2 = 'Red';
                       $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = "Not Processed";
                      }
                      
                      /**
					  * 
					  * @ Microtomy 24 hrs.
					  * 
					  */
					  
                      $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                     if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                     {
                      $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                      $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                      $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage3+= $minutes_stage3;
                      $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                      $hour_stage3 = round(($avg_minutes_stage3/60),1);
                      $day_stage3 = floor($hour_stage3/24);
    
                      $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage3>86400) // 24 hrs.
                      {
                        $color3 = 'Red';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      }
                      else 
                      {
                        $color3 = '';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      } 
                      
                      }  
                      else
                      {
                        $color3 = '';
                        $time3 = "Not Processed";
                      }
    
                      /**
					  * 
					  * @var QC Check 6 hrs.
					  * 
					  */ 
                       
                      $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                      {
                      $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                      $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                      $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage4+= $minutes_stage4;
                      $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                      $hour_stage4 = round(($avg_minutes_stage4/60),1);
                      $day_stage4 = floor($hour_stage4/24); 
    
                      $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage4>21600) // 6 hrs.
                      {
                        $color4 = 'Red';
                        $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      else
                      {
                        $color4 = '';
                        $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      }
                      else
                      {
                        $color4 = '';
                        $time4 = "Not Processed";
                      }
    
                      /**
					  * 
					  * @var Pending Specimen List 48 hrs.
					  * 
					  */
					  
                      $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                      
                        if(!empty($pending_sql[0]['max_date']))
                        {
                          $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                          $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                          $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                          $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_pending+= $minutes_pending;
                          $avg_minutes_pending = floor($tot_minutes_pending/$count);
                          $hour_stage_pending = round(($avg_minutes_pending/60),1);
                          $day_stage_pending = floor($hour_stage_pending/24); 
    
                          $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage5>172800)
                          {
                             $color5 = 'Red';
                             $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                          else
                          {
                            $color5 = '';
                             $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                            
                        }
                        else
                        {
                          $color5 = '';
                          $time5 = "Not Processed";
                        }
    
                         /**
						 * 
						 * @var Pending Report
						 * 
						 */
    
                         $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                        if(!empty($report_sql[0]['create_date']))
                        {
                          $timestamp_report = strtotime($report_sql[0]['create_date']);
                          $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                          $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                          
                          $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_stage+= $minutes_stage;
                          $avg_minutes_stage = floor($tot_minutes_stage/$count);
                          $hour_stage = round(($avg_minutes_stage/60),1);
                          $day_stage = floor($hour_stage/24); 
    
                          $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage6>172800)
                          {
                             $color6 = 'Red';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                          else
                          {
                             $color6 = '';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                        }
                        else
                        {
                            $color6 = '';
                            $time6 = "Not Processed";
                        }
    
                    $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                    'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
                   'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                    array_push($details, $specimen_information); 
                }
    
                if(!empty($day))
                {
                    $day1 = $day." Days";
                }
                else{
                    $day1= "" ;
                }
                if(!empty($hour))
                {
                    $hour1 = $hour." Hours";
                }
                else{
                    $hour1= "" ;
                }
    
                if(!empty($day_stage2))
              {
                  $day2 = $day_stage2." Days";
              }
              else{
                  $day2= "" ;
              }
              if(!empty($hour_stage2))
              {
                  $hour2 = $hour_stage2." Hours";
              }
              else{
                  $hour2= "" ;
              }
    
              if(!empty($day_stage3))
              {
                  $day3 = $day_stage3." Days";
              }
              else{
                  $day3= "" ;
              }
              if(!empty($hour_stage3))
              {
                  $hour3 = $hour_stage3." Hours";
              }
              else{
                  $hour3= "" ;
              }
                if(!empty($day_stage4))
                {
                    $day4 = $day_stage4." Days";
                }
                else{
                    $day4= "" ;
                }
                if(!empty($hour_stage4))
                {
                    $hour4 = $hour_stage4." Hours";
                }
                else{
                    $hour4= "" ;
                }
    
                  if(!empty($day_stage_pending))
                  {
                      $day5 = $day_stage_pending." Days";
                  }
                  else{
                      $day5= "" ;
                  }
                  if(!empty($hour_stage_pending))
                  {
                      $hour5 = $hour_stage_pending." Hours";
                  }
                  else{
                      $hour5= "" ;
                  }
                if(!empty($day_stage))
                  {
                      $day6 = $day_stage." Days";
                  }
                  else{
                      $day6= "" ;
                  }
                  if(!empty($hour_stage))
                  {
                      $hour6 = $hour_stage." Hours";
                  }
                  else{
                      $hour6= "" ;
                  }
                echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
                'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
            }
        }

    }
/**
	* 
	* Search HISTO avg time
	* 
	*/


    function search_histo_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data)){
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $tbl1=SPECIMEN_MAIN;
        $tbl2=CLINICAL_INFO_MAIN;
        $tbl3=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl4 = SPECIMEN_STAGE_DETAILS_MAIN;
        $dataType = $data['dataType'];
	
        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=CLINICAL_INFO_MAIN;
            $tbl3=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl4 = SPECIMEN_STAGE_DETAILS_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=CLINICAL_INFO_ARCHIVE;
            $tbl3=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl4 = SPECIMEN_STAGE_DETAILS_ARCHIVE;
        }
        
        if($data['from_date']!="" && $data['to_date']!="" && ($data['from_date'] > '2017-03-27 23:59:59') && ($data['to_date'] > '2017-03-27 23:59:59'))
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM $tbl1 WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59' AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM $tbl2 WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        }
        
        if($data['acc']!="")
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM $tbl1 WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `assessioning_num`='".$data['acc']."' AND `id` NOT IN (SELECT `specimen_id` FROM $tbl4 WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM $tbl2 WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        }
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
            	/**
				* 
				* @var Delivery to Lab
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM $tbl4 WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                /**
				* 
				* @var Gross Description
				* 
				*/
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM $tbl4 WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>28800) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  /**
				  * 
				  * @var Microtomy
				  * 
				  */
                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>86400)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

					/**
					* 
					* @var QC Check 
					* 
					*/
				                  
				  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM $tbl4 WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>21600)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

					/**
					* 
					* @var Pending Specimen list
					* 
					*/

                   $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM $tbl4 WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>172800)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }
					
					/**
					* 
					* @var Pending Report
					* 
					*/
					
                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM $tbl3 WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>172800)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }

            die(json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6)));
        }else{
            die(json_encode(array('status'=>'0')));
        }
    }
    }
				

	
    /**
	* 
	* Last Month HISTO Stages Avg
	*/
	
    function last_one_month()
    {
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
        (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` >= DATE(NOW()) - INTERVAL 1 MONTH AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
            	/**
				* 
				* @var Delivery To Lab
				* 
				*/
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                /**
				* 
				* @var Gross Description
				* 
				*/
				
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>28800) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  /**
				  * 
				  * @var Microtomy
				  * 
				  */
                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>86400)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                 /**
				 * 
				 * @var QC Check
				 * 
				 */
                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>21600)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }
                   
                   /**
				   * 
				   * @var Pending Specimen List
				   * 
				   */
                   
                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>172800)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>172800)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }
            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
        else{
            echo json_encode(array('status'=>'0'));
        }
    }
    

	/**
	* 
	* PCR Avg time
	*/
	
     function get_pcr_avg_time()
    {   
        $tot_minutes=0;
        $tot_minutes_report = 0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
   
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
        FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
        AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
        AND wp_abd_specimen.create_date > '2017-03-27 23:59:59' ORDER BY `id` DESC limit 0,10");
        if($specimen_results)
        {
            $count = count($specimen_results);
            $total_specimen_count = 0;
            $stage2_total_specimen_count = 0;
            $stage3_total_specimen_count = 0;
            $report_total_specimen_count = 0;
            foreach ($specimen_results as $specimen_data)
            {
                /**
				* 
				* @var Delivery to Extraction
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
            
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $specimen_count = 1;
				$total_specimen_count+= $specimen_count;	
				
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$total_specimen_count);
               /* $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);
*/
                $n = $avg_minutes*60; 
				$day = floor($n / (24 * 3600)); 

			 	$n = ($n % (24 * 3600)); 
				$hour = round(($n / 3600),1); 


                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400) // 24 hrs
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                /**
				* 
				* @var Assign Well Stages
				* 
				*/
								
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
              
                 if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $stage2_specimen_count = 1;           
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2 = date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                
				  $stage2_total_specimen_count+= $stage2_specimen_count;	
                  $minutes_stage2 = floor(abs($timestamp_stage2 -(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$stage2_total_specimen_count);
                
                  $n = $avg_minutes_stage2*60; 
				  $day_stage2 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage2 = round(($n / 3600),1); 




                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>86400) // 24 hrs
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }
               
                 /**
				  * 
				  * @var Data Analysis 72 hrs.
				  * 
				  */
				  
                 $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
                
                
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $stage3_specimen_count	   = 1;                 	
                  $stage3_total_specimen_count+= $stage3_specimen_count;	
                 	
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$stage3_total_specimen_count);
        
				  $n = $avg_minutes_stage3*60; 
				  $day_stage3 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage3 = round(($n / 3600),1); 

			


                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>259600) // 72 hrs.
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }
				/**
				* 
				* @var Report Generated
				* 
				*/

                $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
                  
                    if(!empty($pcr_report_sql[0]['created_date']))
                    {
                    	
					$report_specimen_count	   = 1;                 	
					$report_total_specimen_count+= $report_specimen_count;	
                    	
                      $timestamp_report = strtotime($pcr_report_sql[0]['created_date']);
                      $formatted_time_report = date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time_report  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_report  = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_report += $minutes_report;
                      $avg_minutes_report  = floor($tot_minutes_report/$report_total_specimen_count);
                    
                    
					  $n = $avg_minutes_report*60; 
					  $day_stage_report = floor($n / (24 * 3600)); 
     				  $n = ($n % (24 * 3600)); 
					  $hour_stage_report = round(($n / 3600),1); 

                    
                    
                      /*$hour_stage_report   = round(($avg_minutes_report/60),1);
                      $day_stage_report    = floor($hour_stage_report/24); */

                      $chk_hour_pcr_report = floor(abs(strtotime($pcr_report_sql[0]['created_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_pcr_report > 144000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_report, $original_time_report); 
                      }
                      else
                      {
                         $color5 = '';
                         $time5 =  dateDiff($formatted_time_report, $original_time_report); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }


                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2, 'stage3'=>$time3, 'report'=>$time5,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,'color4'=>$color5);
                
                array_push($details, $specimen_information); 
            
            }

			if(!empty($day))
			{
			$day1 = $day." Days";
			}
			else{
			$day1= "" ;
			}
			if(!empty($hour))
			{
			$hour1 = $hour." Hours";
			}
			else{
			$hour1= "" ;
			}

			if(!empty($day_stage2))
			{
			$day2 = $day_stage2." Days";
			}
			else{
			$day2= "" ;
			}
			if(!empty($hour_stage2))
			{
			$hour2 = $hour_stage2." Hours";
			}
			else{
			$hour2= "" ;
			}

			if(!empty($day_stage3))
			{
			$day3 = $day_stage3." Days";
			}
			else{
			$day3= "" ;
			}
			if(!empty($hour_stage3))
			{
			$hour3 = $hour_stage3." Hours";
			}
			else{
			$hour3= "" ;
			}
			
		
			
		    if(!empty($day_stage_report))
			{
			$day4 = $day_stage_report." Days";
			}
			else{
			$day4= "" ;
			}
			
			if(!empty($hour_stage_report))
			{
			$hour4 = $hour_stage_report." Hours";
			}
			else{
			$hour4= "" ;
			}

            echo json_encode(array('status'=>'1','details'=> $details, 'count'=>$count, 'day1'=>$day1, 'hour1'=>$hour1, 'day2'=>$day2,'hour2'=>$hour2, 'day3'=>$day3, 'hour3'=>$hour3, 'day4'=>$day4, 'hour4'=>$hour4));
      
        }
   
    }
    
     function get_more_pcr_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $start = $data['load'];
        if(!empty($data))
        {
            $tot_minutes=0;
            $tot_minutes_report = 0;
            $tot_minutes_stage2=0;
            $tot_minutes_stage3=0;
            $tot_minutes_stage4=0;
            $tot_minutes_pending=0;
            $tot_minutes_stage=0;
            $count =0;
            $details = array();
            $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
            FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
            AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
            AND wp_abd_specimen.create_date > '2017-03-27 23:59:59' ORDER BY `id` DESC limit $start,10");
            if($specimen_results)
            {
                $count= $start+10;
				$total_specimen_count = 0;
				$stage2_total_specimen_count = 0;
				$stage3_total_specimen_count = 0;
				$report_total_specimen_count = 0;
                foreach ($specimen_results as $specimen_data)
                {
                /**
				* 
				* @var Delivery to Extraction
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                    
                    if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                    {
                    $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                    $specimen_count = 1;
				    $total_specimen_count+= $specimen_count;	
                    $formatted_time= date('Y-m-d H:i:s',$timestamp);
                    $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));    
                              
                    
                    $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                    $tot_minutes+= $minutes;
                    $avg_minutes = floor($tot_minutes/$total_specimen_count);
					$n = $avg_minutes*60; 
					$day = floor($n / (24 * 3600)); 

					$n = ($n % (24 * 3600)); 
					$hour = round(($n / 3600),1); 
					
                    $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                    if($chk_hour_stage1>86400)
                    {
                      $color1 = 'Red';
                      $time = dateDiff($formatted_time,$original_time_stage1);
                    }
                    else
                    {
                      $color1 = '';
                      $time =  dateDiff($formatted_time,$original_time_stage1);
                    }
                    }
                    else
                    {
                      $color1 = '';
                      $time = "Not Processed";
                    }
                    
                /**
				* 
				* @var Assign Well Stages
				* 
				*/
								
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
                     
                      if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                      {
                      $stage2_specimen_count = 1;         
                      $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                      $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                      $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      $stage2_total_specimen_count+= $stage2_specimen_count;	
                      $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage2+= $minutes_stage2;
                      $avg_minutes_stage2 = floor($tot_minutes_stage2/$stage2_total_specimen_count);
                      $n = $avg_minutes_stage2*60; 
				      $day_stage2 = floor($n / (24 * 3600)); 

			 	      $n = ($n % (24 * 3600)); 
				      $hour_stage2 = round(($n / 3600),1); 
   
    
                      $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage2>86400) // 24 hrs
                      {
                       $color2 = 'Red';
                       $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = "Not Processed";
                      }
                  /**
				  * 
				  * @var Data Analysis 72 hrs.
				  * 
				  */
                    
                   $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
                
                     if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                     {
                     
                      $stage3_specimen_count	   = 1;                 	
                      $stage3_total_specimen_count+= $stage3_specimen_count;	
                 
                      $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                      $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                      $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
					  $tot_minutes_stage3+= $minutes_stage3;
				      $avg_minutes_stage3 = floor($tot_minutes_stage3/$stage3_total_specimen_count);

					  $n = $avg_minutes_stage3*60; 
					  $day_stage3 = floor($n / (24 * 3600)); 

					  $n = ($n % (24 * 3600)); 
					  $hour_stage3 = round(($n / 3600),1); 
    
                      $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                     
                      if($chk_hour_stage3>259600) // 72 hrs.
                      {
                        $color3 = 'Red';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      }
                      else 
                      {
                        $color3 = '';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      } 
                      
                      }  
                      else
                      {
                        $color3 = '';
                        $time3 = "Not Processed";
                      }
    
    
				/**
				* 
				* @var Report Generated 4 hrs.
				* 
				*/

                $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
                  
                    if(!empty($pcr_report_sql[0]['created_date']))
                    {
                    	
					$report_specimen_count	   = 1;                 	
					$report_total_specimen_count+= $report_specimen_count;	
                    	
                      $timestamp_report = strtotime($pcr_report_sql[0]['created_date']);
                      $formatted_time_report = date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time_report  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_report  = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_report += $minutes_report;
                      $avg_minutes_report  = floor($tot_minutes_report/$report_total_specimen_count);
                    
                    
					  $n = $avg_minutes_report*60; 
					  $day_stage_report = floor($n / (24 * 3600)); 
     				  $n = ($n % (24 * 3600)); 
					  $hour_stage_report = round(($n / 3600),1); 

                    
                    
                      /*$hour_stage_report   = round(($avg_minutes_report/60),1);
                      $day_stage_report    = floor($hour_stage_report/24); */

                      $chk_hour_pcr_report = floor(abs(strtotime($pcr_report_sql[0]['created_date'])-strtotime($specimen_data['create_date'])));
                     
                      if($chk_hour_pcr_report > 144000) // 4 hrs.
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_report, $original_time_report); 
                      }
                      else
                      {
                         $color5 = '';
                         $time5 =  dateDiff($formatted_time_report, $original_time_report); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

    
                   $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2, 'stage3'=>$time3, 'report'=>$time5,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,'color4'=>$color5);
                   
                   
                    array_push($details, $specimen_information); 
                }
    
			if(!empty($day))
			{
			$day1 = $day." Days";
			}
			else{
			$day1= "" ;
			}
			if(!empty($hour))
			{
			$hour1 = $hour." Hours";
			}
			else{
			$hour1= "" ;
			}

			if(!empty($day_stage2))
			{
			$day2 = $day_stage2." Days";
			}
			else{
			$day2= "" ;
			}
			if(!empty($hour_stage2))
			{
			$hour2 = $hour_stage2." Hours";
			}
			else{
			$hour2= "" ;
			}

			if(!empty($day_stage3))
			{
			$day3 = $day_stage3." Days";
			}
			else{
			$day3= "" ;
			}
			if(!empty($hour_stage3))
			{
			$hour3 = $hour_stage3." Hours";
			}
			else{
			$hour3= "" ;
			}
					
		    if(!empty($day_stage_report))
			{
			$day4 = $day_stage_report." Days";
			}
			else{
			$day4= "" ;
			}
			
			if(!empty($hour_stage_report))
			{
			$hour4 = $hour_stage_report." Hours";
			}
			else{
			$hour4= "" ;
			}
    
               echo json_encode(array('status'=>'1','details'=> $details, 'count'=>$count, 'day1'=>$day1, 'hour1'=>$hour1, 'day2'=>$day2,'hour2'=>$hour2, 'day3'=>$day3, 'hour3'=>$hour3, 'day4'=>$day4, 'hour4'=>$hour4));
            }
        }
       
    }



	 /**
	 * 
	 * Search PCR Avgrage time
	 */


    function search_pcr_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $tot_minutes=0;
        $tot_minutes_report = 0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $specimen_results ='';
        $details = array();
        $day=0;
        $tbl1=SPECIMEN_MAIN;
        $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
        $tbl3=GENERATE_PCR_REPORT_MAIN;
        $tbl4=CLINICAL_INFO_MAIN;
        $tbl5=SPECIMEN_STAGE_DETAILS_MAIN;
        $dataType = $data['dataType'];

        if($dataType == 'general'){
            $tbl1=SPECIMEN_MAIN;
            $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
            $tbl3=GENERATE_PCR_REPORT_MAIN;
            $tbl4=CLINICAL_INFO_MAIN;
            $tbl5=SPECIMEN_STAGE_DETAILS_MAIN;
        }else if($dataType == 'archive'){
            $tbl1=SPECIMEN_ARCHIVE;
            $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
            $tbl3=GENERATE_PCR_REPORT_ARCHIVE;
            $tbl4=CLINICAL_INFO_ARCHIVE;
            $tbl5=SPECIMEN_STAGE_DETAILS_ARCHIVE;
        }
        
        if($data['from_date']!="" && $data['to_date']!="" && ($data['from_date'] > '2017-03-27 23:59:59') && ($data['to_date'] > '2017-03-27 23:59:59'))
        {
 $specimen_results = $this->BlankModel->customquery("SELECT s1.`assessioning_num`, s1.`id`, s1.`create_date`, s1.`qc_check`
            FROM $tbl1 s1
            INNER JOIN $tbl4 c1
            ON s1.id = c1.specimen_id 
            WHERE (c1.`nail_unit`  LIKE '%4%' OR c1.`nail_unit`  LIKE '%5%' OR c1.`nail_unit`  LIKE '%6%' OR c1.`nail_unit`  LIKE '%7%')
            AND s1.status = '0'
            AND s1.qc_check = '0' 
            AND s1.physician_accepct = '0'
            AND s1.create_date BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'");

        }
        if($data['acc']!="")
        {
            $specimen_results = $this->BlankModel->customquery("SELECT s1.`assessioning_num`, s1.`id`, s1.`create_date`, s1.`qc_check`
            FROM $tbl1 s1
            INNER JOIN $tbl4 c1
            ON s1.id = c1.specimen_id 
            WHERE (c1.`nail_unit`  LIKE '%4%' OR c1.`nail_unit`  LIKE '%5%' OR c1.`nail_unit`  LIKE '%6%' OR c1.`nail_unit`  LIKE '%7%')
            AND s1.status = '0'
            AND s1.qc_check = '0' 
            AND s1.physician_accepct = '0'
            AND s1.assessioning_num ='".$data['acc']."'");
        }
        
        if(!empty($specimen_results))
        {
            $count= count($specimen_results);
            
            $total_specimen_count = 0;
            $stage2_total_specimen_count = 0;
            $stage3_total_specimen_count = 0;
            $report_total_specimen_count = 0;
           
            foreach ($specimen_results as $specimen_data)
            {
               /**
				* 
				* @var Delivery to Extraction
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $specimen_count = 1;
				$total_specimen_count+= $specimen_count;	
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$total_specimen_count);
                
                $n = $avg_minutes*60; 
				$day = floor($n / (24 * 3600)); 

			 	$n = ($n % (24 * 3600)); 
				$hour = round(($n / 3600),1); 

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                
               /**
				* 
				* @var Assign Well Stages
				* 
				*/
								
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
            
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $stage2_specimen_count = 1;           
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  $stage2_total_specimen_count+= $stage2_specimen_count;
                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$stage2_total_specimen_count);
                
                  $n = $avg_minutes_stage2*60; 
				  $day_stage2 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage2 = round(($n / 3600),1); 
                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                 
                  if($chk_hour_stage2>86400) // 24 hrs
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                /**
				  * 
				  * @var Data Analysis 72 hrs.
				  * 
				  */
				  
                 $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
               
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $stage3_specimen_count	   = 1;                 	
                  $stage3_total_specimen_count+= $stage3_specimen_count;	
                 	
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$stage3_total_specimen_count);
        
				  $n = $avg_minutes_stage3*60; 
				  $day_stage3 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage3 = round(($n / 3600),1); 

			
                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  
                  if($chk_hour_stage3>259600) // 72 hrs.
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                /**
				* 
				* @var Report Generated
				* 
				*/

                $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
                  
                    if(!empty($pcr_report_sql[0]['created_date']))
                    {
                    	
					$report_specimen_count	   = 1;                 	
					$report_total_specimen_count+= $report_specimen_count;	
                    	
                      $timestamp_report = strtotime($pcr_report_sql[0]['created_date']);
                      $formatted_time_report = date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time_report  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_report  = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_report += $minutes_report;
                      $avg_minutes_report  = floor($tot_minutes_report/$report_total_specimen_count);
                    
                    
					  $n = $avg_minutes_report*60; 
					  $day_stage_report = floor($n / (24 * 3600)); 
     				  $n = ($n % (24 * 3600)); 
					  $hour_stage_report = round(($n / 3600),1); 

                      $chk_hour_pcr_report = floor(abs(strtotime($pcr_report_sql[0]['created_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_pcr_report > 144000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_report, $original_time_report); 
                      }
                      else
                      {
                         $color5 = '';
                         $time5 =  dateDiff($formatted_time_report, $original_time_report); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }


                 $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2, 'stage3'=>$time3, 'report'=>$time5,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,'color4'=>$color5);
                
                array_push($details, $specimen_information); 
            }

          	if(!empty($day))
			{
			$day1 = $day." Days";
			}
			else{
			$day1= "" ;
			}
			if(!empty($hour))
			{
			$hour1 = $hour." Hours";
			}
			else{
			$hour1= "" ;
			}

			if(!empty($day_stage2))
			{
			$day2 = $day_stage2." Days";
			}
			else{
			$day2= "" ;
			}
			if(!empty($hour_stage2))
			{
			$hour2 = $hour_stage2." Hours";
			}
			else{
			$hour2= "" ;
			}

			if(!empty($day_stage3))
			{
			$day3 = $day_stage3." Days";
			}
			else{
			$day3= "" ;
			}
			if(!empty($hour_stage3))
			{
			$hour3 = $hour_stage3." Hours";
			}
			else{
			$hour3= "" ;
			}
			
    	    if(!empty($day_stage_report))
			{
			$day4 = $day_stage_report." Days";
			}
			else{
			$day4= "" ;
			}
			
			if(!empty($hour_stage_report))
			{
			$hour4 = $hour_stage_report." Hours";
			}
			else{
			$hour4= "" ;
			}

            echo json_encode(array('status'=>'1','details'=> $details, 'count'=>$count, 'day1'=>$day1, 'hour1'=>$hour1, 'day2'=>$day2,'hour2'=>$hour2, 'day3'=>$day3, 'hour3'=>$hour3, 'day4'=>$day4, 'hour4'=>$hour4));
            
        }else{
            echo json_encode(array('status'=>'0'));
        }
	}

   /**
   * Last Month PCR Stages AVG
   * 
   * @return
   */
   
   
    function pcr_last_one_month()
    {
        $tot_minutes=0;
        $tot_minutes_report = 0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
        FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id   WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
        AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
        AND wp_abd_specimen.create_date >= DATE(NOW()) - INTERVAL 2 MONTH ORDER BY `id` DESC");
        if($specimen_results)
        {
            $count= count($specimen_results);
            $total_specimen_count = 0;
            $stage2_total_specimen_count = 0;
            $stage3_total_specimen_count = 0;
            $report_total_specimen_count = 0;
            foreach ($specimen_results as $specimen_data)
            {
            	
               /**
				* 
				* @var Delivery to Extraction
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $specimen_count = 1;
				$total_specimen_count+= $specimen_count;	
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$total_specimen_count);
                
                $n = $avg_minutes*60; 
				$day = floor($n / (24 * 3600)); 

			 	$n = ($n % (24 * 3600)); 
				$hour = round(($n / 3600),1); 

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                
               /**
				* 
				* @var Assign Well Stages
				* 
				*/
								
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
            
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $stage2_specimen_count = 1;           
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  $stage2_total_specimen_count+= $stage2_specimen_count;
                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$stage2_total_specimen_count);
                
                  $n = $avg_minutes_stage2*60; 
				  $day_stage2 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage2 = round(($n / 3600),1); 
                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                 
                  if($chk_hour_stage2>86400) // 24 hrs
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                /**
				  * 
				  * @var Data Analysis 72 hrs.
				  * 
				  */
				  
                 $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
               
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $stage3_specimen_count	   = 1;                 	
                  $stage3_total_specimen_count+= $stage3_specimen_count;	
                 	
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$stage3_total_specimen_count);
        
				  $n = $avg_minutes_stage3*60; 
				  $day_stage3 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage3 = round(($n / 3600),1); 

			
                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  
                  if($chk_hour_stage3>259600) // 72 hrs.
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                /**
				* 
				* @var Report Generated
				* 
				*/

                $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
                  
                    if(!empty($pcr_report_sql[0]['created_date']))
                    {
                    	
					$report_specimen_count	   = 1;                 	
					$report_total_specimen_count+= $report_specimen_count;	
                    	
                      $timestamp_report = strtotime($pcr_report_sql[0]['created_date']);
                      $formatted_time_report = date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time_report  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_report  = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_report += $minutes_report;
                      $avg_minutes_report  = floor($tot_minutes_report/$report_total_specimen_count);
                    
                    
					  $n = $avg_minutes_report*60; 
					  $day_stage_report = floor($n / (24 * 3600)); 
     				  $n = ($n % (24 * 3600)); 
					  $hour_stage_report = round(($n / 3600),1); 

  
                      $chk_hour_pcr_report = floor(abs(strtotime($pcr_report_sql[0]['created_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_pcr_report > 144000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_report, $original_time_report); 
                      }
                      else
                      {
                         $color5 = '';
                         $time5 =  dateDiff($formatted_time_report, $original_time_report); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }


                 $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2, 'stage3'=>$time3, 'report'=>$time5,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,'color4'=>$color5);
                
                array_push($details, $specimen_information); 
            }

           if(!empty($day))
			{
			$day1 = $day." Days";
			}
			else{
			$day1= "" ;
			}
			if(!empty($hour))
			{
			$hour1 = $hour." Hours";
			}
			else{
			$hour1= "" ;
			}

			if(!empty($day_stage2))
			{
			$day2 = $day_stage2." Days";
			}
			else{
			$day2= "" ;
			}
			if(!empty($hour_stage2))
			{
			$hour2 = $hour_stage2." Hours";
			}
			else{
			$hour2= "" ;
			}

			if(!empty($day_stage3))
			{
			$day3 = $day_stage3." Days";
			}
			else{
			$day3= "" ;
			}
			if(!empty($hour_stage3))
			{
			$hour3 = $hour_stage3." Hours";
			}
			else{
			$hour3= "" ;
			}
			
		
			
		    if(!empty($day_stage_report))
			{
			$day4 = $day_stage_report." Days";
			}
			else{
			$day4= "" ;
			}
			
			if(!empty($hour_stage_report))
			{
			$hour4 = $hour_stage_report." Hours";
			}
			else{
			$hour4= "" ;
			}

            echo json_encode(array('status'=>'1','details'=> $details, 'count'=>$count, 'day1'=>$day1, 'hour1'=>$hour1, 'day2'=>$day2,'hour2'=>$hour2, 'day3'=>$day3, 'hour3'=>$hour3, 'day4'=>$day4, 'hour4'=>$hour4));
        }
        else{
            echo json_encode(array('status'=>'0'));
        }

    }
    

   
    /**
    * 
	* Quality Reports Backup
	* 
	*/
	
    function quality_report_19_05_2020()
    {     
      $data = json_decode(file_get_contents('php://input'), true);   
    
     /* $data['from_date']       = '2020-01-14';
      $data['to_date']         = '2020-01-15';
      $data['dataType']        = 'general';
      $data['physician_id']    = '76077'; */
        
      $result_status = 0;
      $histo_array   = "";
      $pcr_array     = "";
      $diagnostics   = "";
      
	  $tbl1=SPECIMEN_MAIN;
	  $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
	  $tbl3=GENERATE_PCR_REPORT_MAIN;
	  $tbl4=IMPORT_DATA_MAIN;
	  $tbl5=CLINICAL_INFO_MAIN;
	 
      $data_type = $data['dataType'];
      if($data_type == 'general'){
         $tbl1=SPECIMEN_MAIN;
         $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
         $tbl3=GENERATE_PCR_REPORT_MAIN;
         $tbl4=IMPORT_DATA_MAIN;
         $tbl5=CLINICAL_INFO_MAIN;
       }
      else if($data_type == 'archive'){
         $tbl1=SPECIMEN_ARCHIVE;
         $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
         $tbl3=GENERATE_PCR_REPORT_ARCHIVE;
         $tbl4=IMPORT_DATA_ARCHIVE;
         $tbl5=CLINICAL_INFO_ARCHIVE;
      }
   	  
	  if($data['from_date'] != "" &&  $data['to_date'] != "" ){	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	 }
	 
	 /*HISTO Reports*/
	  
     $histo_report_sql = "SELECT `specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date, `nail_report`.`gross_description`, `short_code`.`color`
    FROM 
    (SELECT `id`, `create_date` FROM $tbl1 WHERE `physician_id` = '".$data['physician_id']."' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN 
    (SELECT `specimen_id`, `create_date`, `gross_description`, `diagnostic_short_code` FROM $tbl2)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
    
    LEFT JOIN 
    
    (SELECT `sc`, `color` FROM wp_abd_nail_macro_codes WHERE `color` = 'red')short_code 
    ON 
    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";
    
     $histo_report_query   = $this->db->query($histo_report_sql);     
     $histo_specimen_count = $histo_report_query->num_rows();
 	 $histo_reports        = $histo_report_query->result_array();
     
     $get_working_days = "";
     $holiday_sql      = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' ";
     $holiday_range    = $this->BlankModel->customquery($holiday_sql);  
    
     foreach($holiday_range as $holidays ){
             $holiday_list[] = $this->getDatesFromRange($holidays['start_date'], $holidays['end_date']);
             }
     $holidays_list  = $this->array_flatten($holiday_list);
   
     /**
	 * 
	 * @var HISTO TAT
	 * 
	 */
   
     $gross_des     = array();
     $red_gross_des = array();
   
     if($histo_reports){ 
        $result_status           = 1;   	 
        $histo_total_working_day = 0;
     foreach($histo_reports as $specimens){
            $specimen_date    = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $nail_date        = date('Y-m-d', strtotime($specimens['nail_create_date']));
            $get_working_days = $this->getWorkingDays($specimen_date, $nail_date, $holidays_list)-1;
            $histo_total_working_day += $get_working_days;           
            
       	    $gross_description = $specimens['gross_description'];
			if($gross_description){	
			   $str = $gross_description; 
			   $x_postion = strpos($str, "x");	
			/**
			* 
			* @var Posistive HISTO
			* 
			*/
			  if($specimens['color'] == 'red'){
				 
			   if(!empty($x_postion)){
			     $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			     $size_data     = substr($str, $get_data_from, 10);
			   }
			   else{	
			    $postion = strpos($str, "*"); 	
			    if(!empty($postion)){
			    $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			    $size_data = substr($str, $get_data_from, 10);
			    }
			   }						
			    preg_match_all('!\d+!', $size_data, $matches);
			 
			    $red_multiply = 1;			
			    for($i=0; $i<3; $i++){
			        $red_multiply = ($matches[0][$i] * $red_multiply);
			       }
			     $red_gross = $red_multiply; 				
			   }
			   else{
			     $red_gross = "";			   	
			   }
			 
			if(!empty($x_postion)){
			   $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			   $size_data     = substr($str, $get_data_from, 10);
			}
			else{	
			   $postion = strpos($str, "*"); 	
			if(!empty($postion)){
			   $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			   $size_data = substr($str, $get_data_from, 10);
			}
			}						
			preg_match_all('!\d+!', $size_data, $matches);
			 
			   $multiply = 1;			
			for($i=0; $i<3; $i++){
			   $multiply = ($matches[0][$i] * $multiply);
			
			   }
			   $gross = $multiply; 				
			
			} 
            else{
               $gross = "";
            }
          array_push($red_gross_des, $red_gross);  
          array_push($gross_des, $gross);
        
        }           
        $histo_tat_calculation = ($histo_total_working_day/$histo_specimen_count);
        $histo_tat =  number_format((float)$histo_tat_calculation, 1, '.', ''); 
      
        if($histo_reports){           
           $histo_array = array('histo_count' => $histo_specimen_count,
           						'histo_tat'   => $histo_tat);   
        }       
       }
 
     /* PCR Reports */      
       
     $pcr_report_sql = "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date, `pcr_report`.`created_date` as pcr_create_date
    FROM 
    (SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `physician_id` = '".$data['physician_id']."' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
    ORDER BY `pcr_report`.`accessioning_num` DESC ";
    
     $pcr_report_query     = $this->db->query($pcr_report_sql);     
     $pcr_specimen_count   = $pcr_report_query->num_rows();
 	 $pcr_reports = $pcr_report_query->result_array();
     
     /**
	 * 
	 * @var PCR TAT
	 * 
	 */     
       
     if($pcr_reports){ 
     $result_status         = 1;  	 
     $pcr_total_working_day = 0;       
     foreach($pcr_reports as $specimens){
            $specimen_date      = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $pcr_create_date    = date('Y-m-d', strtotime($specimens['pcr_create_date']));
            $get_working_days   = $this->getWorkingDays($specimen_date, $pcr_create_date, $holidays_list)-1;
            $pcr_total_working_day += $get_working_days;           
        }           
        
        $pcr_tat_calculation = ($pcr_total_working_day/$pcr_specimen_count);       
        $pcr_tat = number_format((float)$pcr_tat_calculation, 1, '.', ''); 
          
        if($pcr_reports){          
           $pcr_array = array('pcr_count'             => $pcr_specimen_count, 						    
						   	  'pcr_tat'               => $pcr_tat);
        }       
       }       
       
	   /**
	   * 
	   * @var Green, Red, Yellow Diagnosis Percentage
	   * 
	   */	
	   
	 if($data['from_date'] != "" &&  $data['to_date'] != "" ){
	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	  } 
	           
      $green_diagnosis_count  = diagnosis_color_code_count($code_color = 'green', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);  
     
      $red_diagnosis_count    = diagnosis_color_code_count($code_color = 'red', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);
    
      $yellow_diagnosis_count = diagnosis_color_code_count($code_color = 'yellow', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);   
      $total_specimen_codes_count = ($green_diagnosis_count + $red_diagnosis_count + $yellow_diagnosis_count);	
    
      if($total_specimen_codes_count != 0 ){
        $yellow_per = round(($yellow_diagnosis_count / $total_specimen_codes_count) *100);
        $red_per    = round(($red_diagnosis_count / $total_specimen_codes_count) *100);
        $green_per  = round(($green_diagnosis_count / $total_specimen_codes_count) *100);
       
        $diagnostics = array('yellow' => $yellow_per, 'red' => $red_per, 'green' => $green_per );
       }
  		
   
	/**
	* 
	* Combination  Specimen SQL
	* 
	*/ 
	  
	$combination_specimens_sql = "SELECT `assessioning_num`, `id`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y'), '%Y-%m-%d') BETWEEN '".$data['from_date']."'  AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id`";
            
     $combination_specimens_query = $this->db->query($combination_specimens_sql);     
     $combination_specimens_count = $combination_specimens_query->num_rows();
 	 $combination_specimens       = $combination_specimens_query->result_array();
	 
	 $concordance_histo_pcr_pos = 0; 
	 $concordance_histo_pcr_neg = 0;	 
	 $histo_pos_pcr_neg = 0;	 
	 $histo_neg_pcr_pos = 0;	 
	
	 foreach($combination_specimens as $specimen){
	 	 $specimen_id = $specimen['id'];
	 	 $accessioning_num = $specimen['assessioning_num'];
	 	
	 	 /**
		 * Positive HISTO and PCR Process check
		 */
		 
	 	 $pos_histo_pcr_process = pos_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($pos_histo_pcr_process == 1){	
		    $concordance_histo_pcr_pos = $concordance_histo_pcr_pos+1; 
		 }		 	 	
	 	
	 	 /**
		  * Negetive HISTO and PCR Process check
		  */
		  		  		  
	 	  $neg_histo_pcr_process    = neg_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );	 
	 	  if($neg_histo_pcr_process == 1){	
		    $concordance_histo_pcr_neg = $concordance_histo_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Positive and PCR Negative Process check
		 */
		 
	 	 $histo_pos_pcr_neg_process = histo_pos_pcr_neg_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_pos_pcr_neg_process == 1){	
		    $histo_pos_pcr_neg = $histo_pos_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Negative and PCR Positive Process check
		 */
		 
	 	 $histo_neg_pcr_pos_process = pos_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_neg_pcr_pos_process == 1){	
		    $histo_neg_pcr_pos = $histo_neg_pcr_pos+1; 
		 }	
   
	   }
	 
		 /**
	     * 
	     * @var Concordance
	     * 
	     */     
	    $concordance = 0;
	    if($concordance_histo_pcr_pos > 0 || $concordance_histo_pcr_neg > 0){         
	       $concordance = round((($concordance_histo_pcr_pos + $concordance_histo_pcr_neg)/$combination_specimens_count)*100);	
	    }
	    
	    /**
		* 
		* @var  HISTO Pos and PCR Neg
		* 
		*/
	    $histo_pos_pcr_neg_pec = 0;
		if($histo_pos_pcr_neg > 0 ){
		    round((($histo_pos_pcr_neg)/$combination_specimens_count)*100);	
		} 
		
	    
	    /**
		* 
		* HISTO Neg PCR Pos
		* 
		*/
        $histo_neg_pcr_pos_pec = 0;
		if($histo_neg_pcr_pos > 0){	
		   $histo_neg_pcr_pos_pec = round((($histo_neg_pcr_pos)/$combination_specimens_count)*100);	
		}
	    
       
        /**
		 * 
		 * @var MRSA Positive
		 * 
		 */
		 
		 $mrsa_pos = 0;
         if($pcr_specimen_count > 0 ){
		    $mrsq_pos_sql =  "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date
		FROM 
		(SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `physician_id` = '".$data['physician_id']."' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' )specimen 
		INNER JOIN         

		(SELECT `positive_negtaive`, `accessioning_num` FROM $tbl4 WHERE `positive_negtaive` = 'positive' AND ( `target_name` = 'mecA_Pa04230908_s1' )) pcr_report

		ON 
		`pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`";
       
          $mrsa_query    = $this->db->query($mrsq_pos_sql);  
          $mrsa_pcr_pos  = $mrsa_query->num_rows();		
         if($mrsa_pcr_pos > 0){
		 	$mrsa_pos =  round(( $mrsa_pcr_pos / $pcr_specimen_count)*100);	
		   }          
		 }       
   
        /**
		* 
		* @var Size Distribution
		* 
		*/

	    $first  = 0;
	    $second = 0;
	    $third  = 0; 
	    $fourth = 0;
	    $fifth  = 0; 
	    $sixth  = 0;
	    $seventh= 0;
	for ($i=0; $i<count($gross_des); $i++)
       {    	    	
    	if($gross_des[$i] >= 1 && $gross_des[$i] <= 100)
        {
			$first++;
		}
		if($gross_des[$i] >= 101 && $gross_des[$i] <= 200)
        {
			$second++;
		}
		if($gross_des[$i] >= 201 && $gross_des[$i] <= 300)
        {
			$third++;
		}
		if($gross_des[$i] >= 301 && $gross_des[$i] <= 400)
        {
			$fourth++;
		}
		if($gross_des[$i] >= 401 && $gross_des[$i] <= 500)
        {
			$fifth++;
		}
		if($gross_des[$i] >= 501 && $gross_des[$i] <= 600)
        {
			$sixth++;
		}
    	if($gross_des[$i] >= 601 )
        {
           $seventh++; 
        }
       }
	 
        $size_distribution = array('0-100' => $first,'101-200' => $second,'201-300' => $third,'301-400' => $fourth,'401-500' => $fifth,'501-600' => $sixth, '601-n' => $seventh);
        

	 /**
	 * 
	 * @var Infection graph
	 * 
	 */
	    $first  = 0;
	    $second = 0;
	    $third  = 0; 
	    $fourth = 0;
	    $fifth  = 0; 
	    $sixth  = 0;
	    $seventh= 0;
	for ($i=0; $i<count($red_gross_des); $i++)
       {    	    	
    	if($red_gross_des[$i] >= 1 && $red_gross_des[$i] <= 100)
        {
			$first++;
		}
		if($red_gross_des[$i] >= 101 && $red_gross_des[$i] <= 200)
        {
			$second++;
		}
		if($red_gross_des[$i] >= 201 && $red_gross_des[$i] <= 300)
        {
			$third++;
		}
		if($red_gross_des[$i] >= 301 && $red_gross_des[$i] <= 400)
        {
			$fourth++;
		}
		if($red_gross_des[$i] >= 401 && $red_gross_des[$i] <= 500)
        {
			$fifth++;
		}
		if($red_gross_des[$i] >= 501 && $red_gross_des[$i] <= 600)
        {
			$sixth++;
		}
    	if($red_gross_des[$i] >= 601 )
        {
           $seventh++; 
        }
       }
	 
        $infection = array('0-100' => $first,'101-200' => $second,'201-300' => $third,'301-400' => $fourth,'401-500' => $fifth,'501-600' => $sixth, '601-n' => $seventh);
        


	/**
	* 
	* @var Volume Start Here
	* 
	*/
	
	$volume = "";
    $accessioned_physician_specimen_data = array();
    $reported_physician_specimen_data = array();
  
   
    /**
	 * 
	 * @var Accessioned Specimen Count
	 * 
	 */
	
	/**
	* 
	* @var Accessioned All Specimens
	* 
	*/
		  
    $accessioned_physician_all_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."'  AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
	
	 /**
	 * 
	 * @var Accessioned HISTO
	 * 
	 */
		  
    $accessioned_physician_histo_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."'     AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
		
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
	$accessioned_physician_pcr_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."'     AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%4%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%7%' ))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
	

    /**
	* 
	* @var Accessioned All Specimens
	* 
	*/
	
    $accessioned_physician_all_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_all_specimen_count_sql);
  
    /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $accessioned_physician_histo_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $accessioned_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($accessioned_physician_pcr_specimen_count_sql);

   
    if(!empty($accessioned_physician_histo_specimen_count_data) || !empty($accessioned_physician_pcr_specimen_count_data) || !empty($accessioned_physician_all_specimen_count_data) ){
    	
    	
      $collection_date_arr = array();
      $spe_count_arr       = array();
      $sum_accessioned_histo_specimen = 0;
      foreach ($accessioned_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_accessioned_histo_specimen += $total_specimen;
        array_push($collection_date_arr, $collection_date);
        array_push($spe_count_arr, $total_specimen);
        }
   
		$histo_phy_from_date = $data['from_date'];
		$histo_phy_to_date   = $data['to_date'];

		$pcr_collection_date_arr = array();
		$pcr_spe_count_arr       = array();
		$sum_accessioned_pcr_specimen = 0;
      foreach ($accessioned_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_accessioned_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_collection_date_arr, $pcr_collection_date);
        array_push($pcr_spe_count_arr, $pcr_total_specimen);
        }		
        
        $all_collection_date_arr = array();
		$all_spe_count_arr       = array();
		$sum_accessioned_all_specimen = 0;
      foreach ($accessioned_physician_all_specimen_count_data as $key => $value) {
        $all_collection_date = $value['collection_date'];
        $all_total_specimen  = $value['total_specimen'];
        $sum_accessioned_all_specimen += $all_total_specimen;
        array_push($all_collection_date_arr, $all_collection_date);
        array_push($all_spe_count_arr, $all_total_specimen);
        }
       
    while (strtotime($histo_phy_from_date) <= strtotime($histo_phy_to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($histo_phy_from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($histo_phy_from_date, 'ALL', (Int)$all_spe_count_arr[array_search($histo_phy_from_date, $all_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($histo_phy_from_date,'ALL', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        }
     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($histo_phy_from_date, $collection_date_arr)){
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$spe_count_arr[array_search($histo_phy_from_date,$collection_date_arr)]);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($histo_phy_from_date, $pcr_collection_date_arr)){
         
          $pcr_new_collection_arr = array($histo_phy_from_date, 'PCR', (Int)$pcr_spe_count_arr[array_search($histo_phy_from_date, $pcr_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($histo_phy_from_date,'PCR', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         $histo_phy_from_date = date ("Y-m-d", strtotime("+1 day", strtotime($histo_phy_from_date)));
       
       }     
     }

    
    /**
	* 
	* @var Reported Specimens Count
	* 
	*/

	/**
	* 
	* @var Reported All Specimens
	* 
	*/
		  
    $reported_physician_all_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM 
    (SELECT `id`, `create_date`, `assessioning_num`, `collection_date` FROM `$tbl1` WHERE `physician_id` = '".$data['physician_id']."' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."'   
	AND `create_date` > '2017-03-27 23:59:59'  )specimen 
    LEFT JOIN 
    (SELECT `specimen_id`, `create_date` FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Active')nail_report
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
 	LEFT JOIN         
    (SELECT `id`,`accessioning_num`, `created_date` FROM `wp_abd_generated_pcr_reports` )pcr_report               
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
    
     GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
     ORDER BY COUNT(`specimen`.`id`) DESC";
		
	/**
	* 
	* @var Reported HISTO Specimens
	* 
	*/	
	 
	$reported_physician_histo_specimen_count_sql = "SELECT date_format(str_to_date(`specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date(`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."'  AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
     
    INNER JOIN 
    (SELECT `specimen_id`, `nail_funagl_id` FROM `$tbl2` WHERE `status` = 'Active')nail_patho      
    ON
	`specimen`.`id` = `nail_patho`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
 	
   /**
	* 
	* @var Reported PCR Specimens
	* 
	*/	
	
	$reported_physician_pcr_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$data['from_date']."'     AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
 

   /**
	* 
	* @var Reported All Specimens
	* 
	*/
	
    $reported_physician_all_specimen_count_data = $this->BlankModel->customquery($reported_physician_all_specimen_count_sql);
  
    /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $reported_physician_histo_specimen_count_data = $this->BlankModel->customquery($reported_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $reported_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($reported_physician_pcr_specimen_count_sql);
   
    if(!empty($reported_physician_histo_specimen_count_data) || !empty($reported_physician_pcr_specimen_count_data) || !empty($reported_physician_all_specimen_count_data) ){
    	
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
      /**
	  * 
	  * @var All Reported
	  * 
	  */
      	
      $all_collection_date_arr = array();
	  $all_spe_count_arr       = array();
	  $sum_reported_all_specimen = 0;
      foreach ($reported_physician_all_specimen_count_data as $key => $value) {
        $all_collection_date = $value['collection_date'];
        $all_total_specimen  = $value['total_specimen'];
        $sum_reported_all_specimen += $all_total_specimen;
        array_push($all_collection_date_arr, $all_collection_date);
        array_push($all_spe_count_arr, $all_total_specimen);
        }	
    	
      /**
	   * 
	   * @var HISTO Reported
	   * 
	   */	
      $reported_collection_date_arr = array();
      $reported_spe_count_arr       = array();
      $sum_reported_histo_specimen = 0;
      foreach ($reported_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_reported_histo_specimen += $total_specimen;
        array_push($reported_collection_date_arr, $collection_date);
        array_push($reported_spe_count_arr, $total_specimen);
        }
    
       /**
	   * 
	   * @var PCR Reported
	   * 
	   */	

		$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_reported_pcr_specimen = 0;
      foreach ($reported_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_reported_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	          
       
       while (strtotime($from_date) <= strtotime($to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($from_date, 'ALL', (Int)$all_spe_count_arr[array_search($from_date, $all_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $all_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($from_date,'ALL', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $all_new_collection_arr);
        }     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($from_date, $reported_collection_date_arr)){
          $new_collection_arr = array($from_date, 'HISTO', (Int)$reported_spe_count_arr[array_search($from_date, $reported_collection_date_arr)]);
          array_push($reported_physician_specimen_data, $new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($from_date, 'HISTO', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){
         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
    
     $volume =  array( 'accessioned_specimen_data' => $accessioned_physician_specimen_data, 
                       'reporte_specimen_data' => $reported_physician_specimen_data);   
     
     
    /**
	 * 
	 * @var BreakDown
	 * 
	 */
	 
     /**
	 * 
	 * @var Combination
	 * 
	 */
    
    $clinical_specimen_data = array(); 
     
    $combination_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y'), '%Y-%m-%d') BETWEEN '".$data['from_date']."'  AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
            
    $combination_specimen_count_data = $this->BlankModel->customquery($combination_specimen_count_sql); 
     
   /**
	* 
	* @var PAS GMS FM
	* 
	*/       
            
    $pas_gms_fm_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y'), '%Y-%m-%d') BETWEEN '".$data['from_date']."'      AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%1%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";    
    
    $pas_gms_fm_specimen_count_data = $this->BlankModel->customquery($pas_gms_fm_specimen_count_sql); 
   
    
    /**
	* 
	* @var PAS GMS
	* 
	*/
    
    $pas_gms_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y'), '%Y-%m-%d') BETWEEN '".$data['from_date']."'  AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%2%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $pas_gms_specimen_count_data = $this->BlankModel->customquery($pas_gms_specimen_count_sql);   
    
    /**
	* 
	* @var ONLY PCR
	* 
	*/
    
    $only_pcr_specimen_count_sql = "SELECT date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND date_format( str_to_date( `collection_date`,'%m/%d/%Y'), '%Y-%m-%d') BETWEEN '".$data['from_date']."'      AND '".$data['to_date']."'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%4%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY date_format( str_to_date( `specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $only_pcr_specimen_count_data = $this->BlankModel->customquery($only_pcr_specimen_count_sql); 
   
    
    if(!empty($combination_specimen_count_data) || !empty($pas_gms_fm_specimen_count_data) || !empty($pas_gms_specimen_count_data) || !empty($only_pcr_specimen_count_data) ){
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
      
      /**
	  * 
	  * @var Combination Data
	  * 
	  */
      	
      $combination_collection_date_arr = array();
	  $combination_spe_count_arr       = array();
	  $sum_combination_specimen = 0;
      foreach ($combination_specimen_count_data as $key => $value) {
        $combination_collection_date = $value['collection_date'];
        $combination_total_specimen  = $value['total_specimen'];
        $sum_combination_specimen   += $combination_total_specimen;
        array_push($combination_collection_date_arr, $combination_collection_date);
        array_push($combination_spe_count_arr, $combination_total_specimen);
        }	
    	
      /**
	   * 
	   * @var PAS GMS FM
	   * 
	   */	
      $pas_gms_fm_collection_date_arr = array();
      $pas_gms_fm_count_arr       = array();
      $sum_pas_gms_fm_specimen    = 0;
      foreach ($pas_gms_fm_specimen_count_data as $key => $value) {
        $pas_gms_fm_collection_date = $value['collection_date'];
        $pas_gms_fm_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_fm_specimen   += $pas_gms_fm_total_specimen;
        array_push($pas_gms_fm_collection_date_arr, $pas_gms_fm_collection_date);
        array_push($pas_gms_fm_count_arr, $pas_gms_fm_total_specimen);
        }
    
       /**
	   * 
	   * @var PAS GMS
	   * 
	   */	

	  $pas_gms_collection_date_arr = array();
      $pas_gms_count_arr        = array();
      $sum_pas_gms_specimen     = 0;
      foreach ($pas_gms_specimen_count_data as $key => $value) {
        $pas_gms_collection_date = $value['collection_date'];
        $pas_gms_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_specimen   += $pas_gms_total_specimen;
        array_push($pas_gms_collection_date_arr, $pas_gms_collection_date);
        array_push($pas_gms_count_arr, $pas_gms_total_specimen);
        }        
       
       /**
	   * 
	   * @var PCR ONLY
	   * 
	   */    
       
       	$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_pcr_specimen = 0;
      foreach ($only_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_pcr_specimen   += $pcr_total_specimen; 
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	
       
       while (strtotime($from_date) <= strtotime($to_date)) {    	
    	/**
		  * 
		  * @var Combination Specimens
		  * 
		  */ 
             
        if(in_array($from_date, $combination_collection_date_arr)){         
          $combination_new_collection_arr = array($from_date, 'Combination', (Int)$combination_spe_count_arr[array_search($from_date, $combination_collection_date_arr)]);         
          array_push($clinical_specimen_data, $combination_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $combination_new_collection_arr = array($from_date,'Combination', (Int)$blank_data);
          array_push($clinical_specimen_data, $combination_new_collection_arr);
         }
        
    	/**
		* 
		* @var PAS GSM FM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_fm_collection_date_arr)){
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$pas_gms_fm_count_arr[array_search($from_date, $pas_gms_fm_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);
        }
        
        /**
		* 
		* @var PAS GSM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_collection_date_arr)){
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$pas_gms_count_arr[array_search($from_date, $pas_gms_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);
        }
                 
         /**
		 * 
		 * @var Only PCR Specimens
		 * 
		 */ 
		 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);         
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
         }
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
     
     $breakdown = array('breakdown' => $clinical_specimen_data);  
     
           
     if($result_status == 1){
	    echo json_encode(array('status' => '1', 'histo_details' => $histo_array , 'pcr_details' => $pcr_array, 'diagnostics' => $diagnostics,
	    'concordance_histo_pcr_pos' => $concordance_histo_pcr_pos,
	    'concordance_histo_pcr_neg' => $concordance_histo_pcr_neg,
	    'combination_specimens_count' => $combination_specimens_count,
	    'concordance' => $concordance, 'histo_pos_pcr_neg_number' => $histo_pos_pcr_neg, 'histo_neg_pcr_pos_number' => $histo_neg_pcr_pos, 'histo_pos_pcr_neg' => $histo_pos_pcr_neg_pec, 'histo_neg_pcr_pos' => $histo_neg_pcr_pos_pec, 'mrsa_pos' => $mrsa_pos, 'size_distribution' => $size_distribution, 'infection' => $infection, 'red_gross' => $red_gross_des, 'volume' => $volume, 'sum_accessioned_all_specimen' => $sum_accessioned_all_specimen, 'sum_accessioned_histo_specimen'=> $sum_accessioned_histo_specimen, 'sum_accessioned_pcr_specimen' => $sum_accessioned_pcr_specimen, 
	    
	    'sum_reported_all_specimen' => $sum_reported_all_specimen, 'sum_reported_histo_specimen'=> $sum_reported_histo_specimen, 'sum_reported_pcr_specimen' => $sum_reported_pcr_specimen,
	    
	     'breakdown' => $breakdown, 'total_combination_specimen' => $sum_combination_specimen, 'total_pas_gms_fm_specimen' => $sum_pas_gms_fm_specimen, 'total_pas_gms_specimen' => $sum_pas_gms_specimen ,'total_pcr_specimen'=> $sum_pcr_specimen));
	   }        
       else{
         echo json_encode(array('status'=>'0'));
       }    
    }  

	
  
	/**
    * 
	* Quality Reports Backup Function
	* 
	*/ 
   function bkp_quality_report()
   {     
     $data = json_decode(file_get_contents('php://input'), true);   
    
     /* $data['from_date']       = '2020-05-11';
      $data['to_date']         = '2020-05-22';
      $data['dataType']        = 'general';
      $data['physician_id']    = '76078'; */
        
      $result_status = 0;
      $histo_array   = "";
      $pcr_array     = "";
      $diagnostics   = "";
      
	  $tbl1=SPECIMEN_MAIN;
	  $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
	  $tbl3=GENERATE_PCR_REPORT_MAIN;
	  $tbl4=IMPORT_DATA_MAIN;
	  $tbl5=CLINICAL_INFO_MAIN;
	 
      $data_type = $data['dataType'];
      if($data_type == 'general'){
         $tbl1=SPECIMEN_MAIN;
         $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
         $tbl3=GENERATE_PCR_REPORT_MAIN;
         $tbl4=IMPORT_DATA_MAIN;
         $tbl5=CLINICAL_INFO_MAIN;
       }
      else if($data_type == 'archive'){
         $tbl1=SPECIMEN_ARCHIVE;
         $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
         $tbl3=GENERATE_PCR_REPORT_ARCHIVE;
         $tbl4=IMPORT_DATA_ARCHIVE;
         $tbl5=CLINICAL_INFO_ARCHIVE;
      }
   	  
	  if($data['from_date'] != "" &&  $data['to_date'] != "" ){	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	 }
	 
	 /*HISTO Reports*/
	  
    $histo_report_sql = "SELECT `specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date, `nail_report`.`gross_description`, `short_code`.`color`
    FROM 
    (SELECT `id`, `create_date` FROM $tbl1 WHERE `physician_id` = '".$data['physician_id']."' AND `test_type` = 'NF' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN 
    (SELECT `specimen_id`, `create_date`, `gross_description`, `diagnostic_short_code` FROM $tbl2)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
    
    LEFT JOIN 
    
    (SELECT `sc`, `color` FROM wp_abd_nail_macro_codes WHERE `color` = 'red')short_code 
    ON 
    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";
    
     $histo_report_query   = $this->db->query($histo_report_sql);     
     $histo_specimen_count = $histo_report_query->num_rows();
 	 $histo_reports        = $histo_report_query->result_array();
     
     $get_working_days = "";
     $holiday_sql      = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' ";
     $holiday_range    = $this->BlankModel->customquery($holiday_sql);  
    
     foreach($holiday_range as $holidays ){
             $holiday_list[] = $this->getDatesFromRange($holidays['start_date'], $holidays['end_date']);
             }
     $holidays_list  = $this->array_flatten($holiday_list);
   
     /**
	 * 
	 * @var HISTO TAT
	 * 
	 */
   
     $gross_des     = array();
     $red_gross_des = array();
   
     if($histo_reports){ 
        $result_status           = 1;   	 
        $histo_total_working_day = 0;
     foreach($histo_reports as $specimens){
            $specimen_date    = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $nail_date        = date('Y-m-d', strtotime($specimens['nail_create_date']));
            $get_working_days = $this->getWorkingDays($specimen_date, $nail_date, $holidays_list)-1;
            $histo_total_working_day += $get_working_days;           
            
       	    $gross_description = $specimens['gross_description'];
			if($gross_description){	
			   $str = $gross_description; 
			   $x_postion = strpos($str, "x");	
			/**
			* 
			* @var Posistive HISTO
			* 
			*/
			  if($specimens['color'] == 'red'){
				 
			   if(!empty($x_postion)){
			     $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			     $size_data     = substr($str, $get_data_from, 10);
			   }
			   else{	
			    $postion = strpos($str, "*"); 	
			    if(!empty($postion)){
			    $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			    $size_data = substr($str, $get_data_from, 10);
			    }
			   }						
			    preg_match_all('!\d+!', $size_data, $matches);
			 
			    $red_multiply = 1;			
			    for($i=0; $i<3; $i++){
			        $red_multiply = ($matches[0][$i] * $red_multiply);
			       }
			     $red_gross = $red_multiply; 				
			   }
			   else{
			     $red_gross = "";			   	
			   }
			 
			if(!empty($x_postion)){
			   $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			   $size_data     = substr($str, $get_data_from, 10);
			}
			else{	
			   $postion = strpos($str, "*"); 	
			if(!empty($postion)){
			   $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			   $size_data = substr($str, $get_data_from, 10);
			}
			}						
			preg_match_all('!\d+!', $size_data, $matches);
			 
			   $multiply = 1;			
			for($i=0; $i<3; $i++){
			   $multiply = ($matches[0][$i] * $multiply);
			
			   }
			   $gross = $multiply; 				
			
			} 
            else{
               $gross = "";
            }
          array_push($red_gross_des, $red_gross);  
          array_push($gross_des, $gross);
        
        }           
        $histo_tat_calculation = ($histo_total_working_day/$histo_specimen_count);
        $histo_tat =  number_format((float)$histo_tat_calculation, 1, '.', ''); 
      
        if($histo_reports){           
           $histo_array = array('histo_count' => $histo_specimen_count,
           						'histo_tat'   => $histo_tat);   
        }     
       }
       else{           
           $histo_array = array('histo_count' => '0',
           						'histo_tat'   => '0');   
        }  
 
     /* PCR Reports */      
       
    $pcr_report_sql = "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date, `pcr_report`.`created_date` as pcr_create_date
    FROM 
    (SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `physician_id` = '".$data['physician_id']."' AND `test_type` = 'NF' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
    ORDER BY `pcr_report`.`accessioning_num` DESC ";
    
     $pcr_report_query     = $this->db->query($pcr_report_sql);     
     $pcr_specimen_count   = $pcr_report_query->num_rows();
 	 $pcr_reports = $pcr_report_query->result_array();
     
     /**
	 * 
	 * @var PCR TAT
	 * 
	 */     
       
     if($pcr_reports){ 
     $result_status         = 1;  	 
     $pcr_total_working_day = 0;       
     foreach($pcr_reports as $specimens){
            $specimen_date      = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $pcr_create_date    = date('Y-m-d', strtotime($specimens['pcr_create_date']));
            $get_working_days   = $this->getWorkingDays($specimen_date, $pcr_create_date, $holidays_list)-1;
            $pcr_total_working_day += $get_working_days;           
        }           
        
        $pcr_tat_calculation = ($pcr_total_working_day/$pcr_specimen_count);       
        $pcr_tat = number_format((float)$pcr_tat_calculation, 1, '.', ''); 
          
        if($pcr_reports){          
           $pcr_array = array('pcr_count'             => $pcr_specimen_count, 						    
						   	  'pcr_tat'               => $pcr_tat);
        } 
            
       }       
       else{          
           $pcr_array = array('pcr_count'             => '0', 						    
						   	  'pcr_tat'               => '0');
        } 
	   /**
	   * 
	   * @var Green, Red, Yellow Diagnosis Percentage
	   * 
	   */	
	   
	 if($data['from_date'] != "" &&  $data['to_date'] != "" ){
	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	  } 
	           
      $green_diagnosis_count  = diagnosis_color_code_count($code_color = 'green', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);  
     
      $red_diagnosis_count    = diagnosis_color_code_count($code_color = 'red', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);
    
      $yellow_diagnosis_count = diagnosis_color_code_count($code_color = 'yellow', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);   
      $total_specimen_codes_count = ($green_diagnosis_count + $red_diagnosis_count + $yellow_diagnosis_count);	
    
      if($total_specimen_codes_count != 0 ){
        $yellow_per = round(($yellow_diagnosis_count / $total_specimen_codes_count) *100);
        $red_per    = round(($red_diagnosis_count / $total_specimen_codes_count) *100);
        $green_per  = round(($green_diagnosis_count / $total_specimen_codes_count) *100);
       
        $diagnostics = array('yellow' => $yellow_per, 'red' => $red_per, 'green' => $green_per );
       }
  		
   
	/**
	* 
	* Combination  Specimen SQL
	* 
	*/ 
	  
	/*$combination_specimens_sql = "SELECT `assessioning_num`, `id`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id`";
            */
            
   $combination_specimens_sql = "SELECT `assessioning_num`, `id`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id`
	
	INNER JOIN 
    (SELECT `specimen_id` FROM `$tbl2`)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
    
    INNER JOIN 
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3`)pcr_report
     ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`";       
     
            
     $combination_specimens_query = $this->db->query($combination_specimens_sql);     
     $combination_specimens_count = $combination_specimens_query->num_rows();
 	 $combination_specimens       = $combination_specimens_query->result_array();
	 
	 $concordance_histo_pcr_pos = 0; 
	 $concordance_histo_pcr_neg = 0;	 
	 $histo_pos_pcr_neg = 0;	 
	 $histo_neg_pcr_pos = 0;	 
	
	 foreach($combination_specimens as $specimen){
	 	 $specimen_id = $specimen['id'];
	 	 $accessioning_num = $specimen['assessioning_num'];
	 	
	 	 /**
		 * Positive HISTO and PCR Process check
		 */
		 
	 	 $pos_histo_pcr_process = pos_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($pos_histo_pcr_process == 1){	
		    $concordance_histo_pcr_pos = $concordance_histo_pcr_pos+1; 
		 }		 	 	
	 	
	 	 /**
		  * Negetive HISTO and PCR Process check
		  */
		  		  		  
	 	  $neg_histo_pcr_process    = neg_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );	 
	 	  if($neg_histo_pcr_process == 1){	
		    $concordance_histo_pcr_neg = $concordance_histo_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Positive and PCR Negative Process check
		 */
		 
	 	 $histo_pos_pcr_neg_process = histo_pos_pcr_neg_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_pos_pcr_neg_process == 1){	
		    $histo_pos_pcr_neg = $histo_pos_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Negative and PCR Positive Process check
		 */
		 
	 	 $histo_neg_pcr_pos_process = histo_neg_pcr_pos_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_neg_pcr_pos_process == 1){	
		    $histo_neg_pcr_pos = $histo_neg_pcr_pos+1; 
		 }	
   
	   }
	 
		 /**
	     * 
	     * @var Concordance
	     * 
	     */     
	    $concordance = 0;
	    if($concordance_histo_pcr_pos > 0 || $concordance_histo_pcr_neg > 0){         
	       $concordance = round((($concordance_histo_pcr_pos + $concordance_histo_pcr_neg)/$combination_specimens_count)*100);	
	    }
	    
	    /**
		* 
		* @var  HISTO Pos and PCR Neg
		* 
		*/
	    $histo_pos_pcr_neg_pec = 0;
		if($histo_pos_pcr_neg > 0 ){
		   $histo_pos_pcr_neg_pec = round((($histo_pos_pcr_neg)/$combination_specimens_count)*100);	
		} 
		
	    
	    /**
		* 
		* HISTO Neg PCR Pos
		* 
		*/
        $histo_neg_pcr_pos_pec = 0;
   
		if($histo_neg_pcr_pos > 0){	
		   $histo_neg_pcr_pos_pec = round((($histo_neg_pcr_pos)/$combination_specimens_count)*100);	
		}
	    
       
        /**
		 * 
		 * @var MRSA Positive
		 * 
		 */
		 
	    $mrsa_pos = 0;
        if($pcr_specimen_count > 0 ){
		$mrsq_pos_sql =  "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date
		FROM 
		(SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `physician_id` = '".$data['physician_id']."' AND `test_type` = 'NF' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' )specimen 
		INNER JOIN         

		(SELECT `positive_negtaive`, `accessioning_num` FROM $tbl4 WHERE `positive_negtaive` = 'positive' AND ( `target_name` = 'mecA_Pa04230908_s1' )) pcr_report

		ON 
		`pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`";
       
          $mrsa_query    = $this->db->query($mrsq_pos_sql);  
          $mrsa_pcr_pos  = $mrsa_query->num_rows();		
         if($mrsa_pcr_pos > 0){
		 	$mrsa_pos =  round(( $mrsa_pcr_pos / $pcr_specimen_count)*100);	
		   }          
		 }       
   
        /**
		* 
		* @var Size Distribution
		* 
		*/

	    $first  = 0;
	    $second = 0;
	    $third  = 0; 
	    $fourth = 0;
	    $fifth  = 0; 
	    $sixth  = 0;
	    $seventh= 0;
	for ($i=0; $i<count($gross_des); $i++)
       {    	    	
    	if($gross_des[$i] >= 1 && $gross_des[$i] <= 100)
        {
			$first++;
		}
		if($gross_des[$i] >= 101 && $gross_des[$i] <= 200)
        {
			$second++;
		}
		if($gross_des[$i] >= 201 && $gross_des[$i] <= 300)
        {
			$third++;
		}
		if($gross_des[$i] >= 301 && $gross_des[$i] <= 400)
        {
			$fourth++;
		}
		if($gross_des[$i] >= 401 && $gross_des[$i] <= 500)
        {
			$fifth++;
		}
		if($gross_des[$i] >= 501 && $gross_des[$i] <= 600)
        {
			$sixth++;
		}
    	if($gross_des[$i] >= 601 )
        {
           $seventh++; 
        }
       }
	 
        $size_distribution = array('0-100' => $first,'101-200' => $second,'201-300' => $third,'301-400' => $fourth,'401-500' => $fifth,'501-600' => $sixth, '601-n' => $seventh);
        

	 /**
	 * 
	 * @var Infection graph
	 * 
	 */
	    $inf_first  = 0;
	    $inf_second = 0;
	    $inf_third  = 0; 
	    $inf_fourth = 0;
	    $inf_fifth  = 0; 
	    $inf_sixth  = 0;
	    $inf_seventh= 0;
	for ($i=0; $i<count($red_gross_des); $i++)
       {    	    	
    	if($red_gross_des[$i] >= 1 && $red_gross_des[$i] <= 100)
        {
			$inf_first++;
		}
		if($red_gross_des[$i] >= 101 && $red_gross_des[$i] <= 200)
        {
			$inf_second++;
		}
		if($red_gross_des[$i] >= 201 && $red_gross_des[$i] <= 300)
        {
			$inf_third++;
		}
		if($red_gross_des[$i] >= 301 && $red_gross_des[$i] <= 400)
        {
			$inf_fourth++;
		}
		if($red_gross_des[$i] >= 401 && $red_gross_des[$i] <= 500)
        {
			$inf_fifth++;
		}
		if($red_gross_des[$i] >= 501 && $red_gross_des[$i] <= 600)
        {
			$inf_sixth++;
		}
    	if($red_gross_des[$i] >= 601 )
        {
           $inf_seventh++; 
        }
       }
	 
 
	   if($inf_first > 0){	
		  $inf_first  = round(($inf_first/$first)*100);
		}   
	   if($inf_second > 0){	
		  $inf_second  = round(($inf_second/$second)*100);
		}
	   if($inf_third > 0){	
		  $inf_third  = round(($inf_third/$third)*100);
		}   
	   if($inf_fourth > 0){	
		  $inf_fourth  = round(($inf_fourth/$fourth)*100);
		}  
	   if($inf_fifth > 0){	
		  $inf_fifth  = round(($inf_fifth/$fifth)*100);
		}   
	   if($inf_sixth > 0){	
		  $inf_sixth  = round(($inf_sixth/$sixth)*100);
		}
		if($inf_seventh > 0){	
		  $inf_seventh  = round(($inf_seventh/$seventh)*100);
		}
		 
        $infection = array('0-100' => $inf_first.'%','101-200' => $inf_second.'%','201-300' => $inf_third.'%','301-400' => $inf_fourth.'%','401-500' => $inf_fifth.'%','501-600' => $inf_sixth.'%', '601-n' => $inf_seventh.'%');
  

	/**
	* 
	* @var Volume Start Here
	* 
	*/
	
	$volume = "";
    $accessioned_physician_specimen_data = array();
    $reported_physician_specimen_data = array();
  
   
    /**
	 * 
	 * @var Accessioned Specimen Count
	 * 
	 */
	
	/**
	* 
	* @var Accessioned All Specimens
	* 
	*/
		  
    $accessioned_physician_all_specimen_count_sql = "SELECT date_format(`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
	
	 /**
	 * 
	 * @var Accessioned HISTO
	 * 
	 */
		  
   $accessioned_physician_histo_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
		
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
	$accessioned_physician_pcr_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%4%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%7%' ))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
	

    /**
	* 
	* @var Accessioned All Specimens
	* 
	*/
	
    $accessioned_physician_all_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_all_specimen_count_sql);
  
    /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $accessioned_physician_histo_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $accessioned_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($accessioned_physician_pcr_specimen_count_sql);

   
    if(!empty($accessioned_physician_histo_specimen_count_data) || !empty($accessioned_physician_pcr_specimen_count_data) || !empty($accessioned_physician_all_specimen_count_data) ){
    	
    	
      $collection_date_arr = array();
      $spe_count_arr       = array();
      $sum_accessioned_histo_specimen = 0;
      foreach ($accessioned_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_accessioned_histo_specimen += $total_specimen;
        array_push($collection_date_arr, $collection_date);
        array_push($spe_count_arr, $total_specimen);
        }
   
		$histo_phy_from_date = $data['from_date'];
		$histo_phy_to_date   = $data['to_date'];

		$pcr_collection_date_arr = array();
		$pcr_spe_count_arr       = array();
		$sum_accessioned_pcr_specimen = 0;
      foreach ($accessioned_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_accessioned_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_collection_date_arr, $pcr_collection_date);
        array_push($pcr_spe_count_arr, $pcr_total_specimen);
        }		
        
        $all_collection_date_arr = array();
		$all_spe_count_arr       = array();
		$sum_accessioned_all_specimen = 0;
      foreach ($accessioned_physician_all_specimen_count_data as $key => $value) {
        $all_collection_date = $value['collection_date'];
        $all_total_specimen  = $value['total_specimen'];
        $sum_accessioned_all_specimen += $all_total_specimen;
        array_push($all_collection_date_arr, $all_collection_date);
        array_push($all_spe_count_arr, $all_total_specimen);
        }
       
    while (strtotime($histo_phy_from_date) <= strtotime($histo_phy_to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($histo_phy_from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($histo_phy_from_date, 'ALL', (Int)$all_spe_count_arr[array_search($histo_phy_from_date, $all_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($histo_phy_from_date,'ALL', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        }
     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($histo_phy_from_date, $collection_date_arr)){
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$spe_count_arr[array_search($histo_phy_from_date,$collection_date_arr)]);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($histo_phy_from_date, $pcr_collection_date_arr)){
         
          $pcr_new_collection_arr = array($histo_phy_from_date, 'PCR', (Int)$pcr_spe_count_arr[array_search($histo_phy_from_date, $pcr_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($histo_phy_from_date,'PCR', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         $histo_phy_from_date = date ("Y-m-d", strtotime("+1 day", strtotime($histo_phy_from_date)));
       
       }     
     }

    
    /**
	* 
	* @var Reported Specimens Count
	* 
	*/

			
	/**
	* 
	* @var Reported HISTO Specimens
	* 
	*/	
	 
	$reported_physician_histo_specimen_count_sql = "SELECT date_format(`nail_patho`.`dor`, '%Y-%m-%d')  AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."'
	AND `test_type` = 'NF' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'

	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
     
    INNER JOIN 
    (SELECT `specimen_id`, `nail_funagl_id`, `dor` FROM `$tbl2` WHERE `status` = 'Active' AND `dor` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59')nail_patho      
    ON
	`specimen`.`id` = `nail_patho`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
 	
   /**
	* 
	* @var Reported PCR Specimens
	* 
	*/	
	
	$reported_physician_pcr_specimen_count_sql = "SELECT date_format(`pcr_report`.`created_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `test_type` = 'NF'
	AND `physician_accepct` = '0'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` WHERE `created_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
 

   /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $reported_physician_histo_specimen_count_data = $this->BlankModel->customquery($reported_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $reported_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($reported_physician_pcr_specimen_count_sql);
   
    if(!empty($reported_physician_histo_specimen_count_data) || !empty($reported_physician_pcr_specimen_count_data) || !empty($reported_physician_all_specimen_count_data) ){
    	
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
     
      /**
	  * 
	  * @var All Reported
	  * 
	  */
      	
      $all_collection_date_arr = array();
	  $all_spe_count_arr       = array();
	  $sum_reported_all_specimen = 0;
    
    	
      /**
	   * 
	   * @var HISTO Reported
	   * 
	   */	
      $reported_collection_date_arr = array();
      $reported_spe_count_arr       = array();
      $sum_reported_histo_specimen = 0;
      foreach ($reported_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_reported_histo_specimen += $total_specimen;
        array_push($reported_collection_date_arr, $collection_date);
        array_push($reported_spe_count_arr, $total_specimen);
        }
    
       /**
	   * 
	   * @var PCR Reported
	   * 
	   */	

		$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_reported_pcr_specimen = 0;
      foreach ($reported_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_reported_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	          
       
         
       $histo_report = array_combine($reported_collection_date_arr, $reported_spe_count_arr); 
       $pcr_report   = array_combine($pcr_reported_collection_date_arr, $pcr_reported_spe_count_arr); 
  
       $other_report = array_fill_keys($pcr_reported_collection_date_arr, '0');
       
       $all_report_date_arr = ($histo_report + $other_report);
    
       $all_report_count = sum_arr($histo_report, $pcr_report, $all_report_date_arr);
      
	   $all_collection_date_arr = array_keys($all_report_count);
	   $all_spe_count_arr       = array_values($all_report_count);
       if($all_spe_count_arr){
		   foreach ($all_spe_count_arr as $value) {      
                    $sum_reported_all_specimen += $value;    
           }		
		}
           
           
             
       while (strtotime($from_date) <= strtotime($to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($from_date, 'ALL', (Int)$all_spe_count_arr[array_search($from_date, $all_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $all_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($from_date,'ALL', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $all_new_collection_arr);
        }     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($from_date, $reported_collection_date_arr)){
          $new_collection_arr = array($from_date, 'HISTO', (Int)$reported_spe_count_arr[array_search($from_date, $reported_collection_date_arr)]);
          array_push($reported_physician_specimen_data, $new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($from_date, 'HISTO', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){
         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         
         
         
         
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
    
     $volume =  array( 'accessioned_specimen_data' => $accessioned_physician_specimen_data, 
                       'reporte_specimen_data' => $reported_physician_specimen_data);   
     
     
    /**
	 * 
	 * @var BreakDown
	 * 
	 */
	 
     /**
	 * 
	 * @var Combination
	 * 
	 */
    
    $clinical_specimen_data = array(); 
     
    $combination_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `status` = '0' AND `qc_check` = '0' 
	AND `test_type` = 'NF'
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
            
    $combination_specimen_count_data = $this->BlankModel->customquery($combination_specimen_count_sql); 
     
   /**
	* 
	* @var PAS GMS FM
	* 
	*/       
            
    $pas_gms_fm_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%1%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";    
    
    $pas_gms_fm_specimen_count_data = $this->BlankModel->customquery($pas_gms_fm_specimen_count_sql); 
   
    
    /**
	* 
	* @var PAS GMS
	* 
	*/
    
    $pas_gms_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%2%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $pas_gms_specimen_count_data = $this->BlankModel->customquery($pas_gms_specimen_count_sql);   
    
    /**
	* 
	* @var ONLY PCR
	* 
	*/
    
    $only_pcr_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
    
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%4%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $only_pcr_specimen_count_data = $this->BlankModel->customquery($only_pcr_specimen_count_sql); 
   
    
    if(!empty($combination_specimen_count_data) || !empty($pas_gms_fm_specimen_count_data) || !empty($pas_gms_specimen_count_data) || !empty($only_pcr_specimen_count_data) ){
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
      
      /**
	  * 
	  * @var Combination Data
	  * 
	  */
      	
      $combination_collection_date_arr = array();
	  $combination_spe_count_arr       = array();
	  $sum_combination_specimen = 0;
      foreach ($combination_specimen_count_data as $key => $value) {
        $combination_collection_date = $value['collection_date'];
        $combination_total_specimen  = $value['total_specimen'];
        $sum_combination_specimen   += $combination_total_specimen;
        array_push($combination_collection_date_arr, $combination_collection_date);
        array_push($combination_spe_count_arr, $combination_total_specimen);
        }	
    	
      /**
	   * 
	   * @var PAS GMS FM
	   * 
	   */	
      $pas_gms_fm_collection_date_arr = array();
      $pas_gms_fm_count_arr       = array();
      $sum_pas_gms_fm_specimen    = 0;
      foreach ($pas_gms_fm_specimen_count_data as $key => $value) {
        $pas_gms_fm_collection_date = $value['collection_date'];
        $pas_gms_fm_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_fm_specimen   += $pas_gms_fm_total_specimen;
        array_push($pas_gms_fm_collection_date_arr, $pas_gms_fm_collection_date);
        array_push($pas_gms_fm_count_arr, $pas_gms_fm_total_specimen);
        }
    
       /**
	   * 
	   * @var PAS GMS
	   * 
	   */	

	  $pas_gms_collection_date_arr = array();
      $pas_gms_count_arr        = array();
      $sum_pas_gms_specimen     = 0;
      foreach ($pas_gms_specimen_count_data as $key => $value) {
        $pas_gms_collection_date = $value['collection_date'];
        $pas_gms_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_specimen   += $pas_gms_total_specimen;
        array_push($pas_gms_collection_date_arr, $pas_gms_collection_date);
        array_push($pas_gms_count_arr, $pas_gms_total_specimen);
        }        
       
       /**
	   * 
	   * @var PCR ONLY
	   * 
	   */    
       
       	$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_pcr_specimen = 0;
      foreach ($only_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_pcr_specimen   += $pcr_total_specimen; 
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	
       
       while (strtotime($from_date) <= strtotime($to_date)) {    	
    	/**
		  * 
		  * @var Combination Specimens
		  * 
		  */ 
             
        if(in_array($from_date, $combination_collection_date_arr)){         
          $combination_new_collection_arr = array($from_date, 'Combination', (Int)$combination_spe_count_arr[array_search($from_date, $combination_collection_date_arr)]);         
          array_push($clinical_specimen_data, $combination_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $combination_new_collection_arr = array($from_date,'Combination', (Int)$blank_data);
          array_push($clinical_specimen_data, $combination_new_collection_arr);
         }
        
    	/**
		* 
		* @var PAS GSM FM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_fm_collection_date_arr)){
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$pas_gms_fm_count_arr[array_search($from_date, $pas_gms_fm_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);
        }
        
        /**
		* 
		* @var PAS GSM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_collection_date_arr)){
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$pas_gms_count_arr[array_search($from_date, $pas_gms_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);
        }
                 
         /**
		 * 
		 * @var Only PCR Specimens
		 * 
		 */ 
		 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);         
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
         }
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
     
     $breakdown = array('breakdown' => $clinical_specimen_data);  
     
           
     if($result_status == 1){
	    echo json_encode(array('status' => '1', 'histo_details' => $histo_array , 'pcr_details' => $pcr_array, 'diagnostics' => $diagnostics,
	    'concordance_histo_pcr_pos' => $concordance_histo_pcr_pos,
	    'concordance_histo_pcr_neg' => $concordance_histo_pcr_neg,
	    'combination_specimens_count' => $combination_specimens_count,
	    'concordance' => $concordance, 'histo_pos_pcr_neg_number' => $histo_pos_pcr_neg, 'histo_neg_pcr_pos_number' => $histo_neg_pcr_pos, 'histo_pos_pcr_neg' => $histo_pos_pcr_neg_pec, 'histo_neg_pcr_pos' => $histo_neg_pcr_pos_pec, 'mrsa_pos' => $mrsa_pos, 'size_distribution' => $size_distribution, 'infection' => $infection, 'red_gross' => $red_gross_des, 
	    
	    'volume' => $volume, 
	    'sum_accessioned_all_specimen' => $sum_accessioned_all_specimen, 
	    'sum_accessioned_histo_specimen'=> $sum_accessioned_histo_specimen, 
	    'sum_accessioned_pcr_specimen' => $sum_accessioned_pcr_specimen, 
	    
	    'sum_reported_all_specimen' => $sum_reported_all_specimen, 'sum_reported_histo_specimen'=> $sum_reported_histo_specimen, 'sum_reported_pcr_specimen' => $sum_reported_pcr_specimen,
	    
	     'breakdown' => $breakdown, 'total_combination_specimen' => $sum_combination_specimen, 'total_pas_gms_fm_specimen' => $sum_pas_gms_fm_specimen, 'total_pas_gms_specimen' => $sum_pas_gms_specimen ,'total_pcr_specimen'=> $sum_pcr_specimen));
	   }        
       else{
         echo json_encode(array('status'=>'1', 'histo_details' => $histo_array , 'pcr_details' => $pcr_array, 'diagnostics' => $diagnostics,
	    'concordance_histo_pcr_pos' => $concordance_histo_pcr_pos,
	    'concordance_histo_pcr_neg' => $concordance_histo_pcr_neg,
	    'combination_specimens_count' => $combination_specimens_count,
	    'concordance' => $concordance, 'histo_pos_pcr_neg_number' => $histo_pos_pcr_neg, 'histo_neg_pcr_pos_number' => $histo_neg_pcr_pos, 'histo_pos_pcr_neg' => $histo_pos_pcr_neg_pec, 'histo_neg_pcr_pos' => $histo_neg_pcr_pos_pec, 'mrsa_pos' => $mrsa_pos, 'size_distribution' => $size_distribution, 'infection' => $infection, 'red_gross' => $red_gross_des, 
	    
	    'volume' => $volume, 
	    'sum_accessioned_all_specimen' => $sum_accessioned_all_specimen, 
	    'sum_accessioned_histo_specimen'=> $sum_accessioned_histo_specimen, 
	    'sum_accessioned_pcr_specimen' => $sum_accessioned_pcr_specimen, 
	    
	    'sum_reported_all_specimen' => $sum_reported_all_specimen, 'sum_reported_histo_specimen'=> $sum_reported_histo_specimen, 'sum_reported_pcr_specimen' => $sum_reported_pcr_specimen,
	    
	     'breakdown' => $breakdown, 'total_combination_specimen' => $sum_combination_specimen, 'total_pas_gms_fm_specimen' => $sum_pas_gms_fm_specimen, 'total_pas_gms_specimen' => $sum_pas_gms_specimen ,'total_pcr_specimen'=> $sum_pcr_specimen));
       }    
    }  







   /**
    * 
	* Quality Reports Main Function
	* 
	*/

   function quality_report()
   {     
      $data = json_decode(file_get_contents('php://input'), true);   
    
     /* $data['from_date']    = '2020-05-11';
      $data['to_date']      = '2020-05-22';
      $data['dataType']     = 'general';
      //$data['physician_id'] = '76078'; 
      $data['physician_id'] = '';*/
        
      $result_status = 0;
      $histo_array   = "";
      $pcr_array     = "";
      $diagnostics   = "";
      
	  $tbl1 = SPECIMEN_MAIN;
	  $tbl2 = NAIL_PATHOLOGY_REPORT_MAIN;
	  $tbl3 = GENERATE_PCR_REPORT_MAIN;
	  $tbl4 = IMPORT_DATA_MAIN;
	  $tbl5 = CLINICAL_INFO_MAIN;
	 
      $data_type = $data['dataType'];
      if($data_type == 'general'){
         $tbl1 = SPECIMEN_MAIN;
         $tbl2 = NAIL_PATHOLOGY_REPORT_MAIN;
         $tbl3 = GENERATE_PCR_REPORT_MAIN;
         $tbl4 = IMPORT_DATA_MAIN;
         $tbl5 = CLINICAL_INFO_MAIN;
      }
      else if($data_type == 'archive'){
         $tbl1 = SPECIMEN_ARCHIVE;
         $tbl2 = NAIL_PATHOLOGY_REPORT_ARCHIVE;
         $tbl3 = GENERATE_PCR_REPORT_ARCHIVE;
         $tbl4 = IMPORT_DATA_ARCHIVE;
         $tbl5 = CLINICAL_INFO_ARCHIVE;
      }
   	  
	  if($data['from_date'] != "" &&  $data['to_date'] != "" ){	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	 }
	 
	 /*HISTO Reports*/
	
	if($data['physician_id'] != '') {
	   $physician_sql = " AND `physician_id` = '".$data['physician_id']."'";
	}
	else{
	   $physician_sql = "";
	} 
	  
    $histo_report_sql = "SELECT `specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date, `nail_report`.`gross_description`, `short_code`.`color`
    FROM 
    (SELECT `id`, `create_date` FROM $tbl1 WHERE `test_type` = 'NF' ".$physician_sql." AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN 
    (SELECT `specimen_id`, `create_date`, `gross_description`, `diagnostic_short_code` FROM $tbl2)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
    
    LEFT JOIN 
    
    (SELECT `sc`, `color` FROM wp_abd_nail_macro_codes WHERE `color` = 'red')short_code 
    ON 
    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";
    
     $histo_report_query   = $this->db->query($histo_report_sql);     
     $histo_specimen_count = $histo_report_query->num_rows();
 	 $histo_reports        = $histo_report_query->result_array();
     
     $get_working_days = "";
     $holiday_sql      = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' ";
     $holiday_range    = $this->BlankModel->customquery($holiday_sql);  
    
     foreach($holiday_range as $holidays ){
             $holiday_list[] = $this->getDatesFromRange($holidays['start_date'], $holidays['end_date']);
             }
     $holidays_list  = $this->array_flatten($holiday_list);
   
     /**
	 * 
	 * @var HISTO TAT
	 * 
	 */
   
     $gross_des     = array();
     $red_gross_des = array();
   
     if($histo_reports){ 
        $result_status           = 1;   	 
        $histo_total_working_day = 0;
     foreach($histo_reports as $specimens){
            $specimen_date    = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $nail_date        = date('Y-m-d', strtotime($specimens['nail_create_date']));
            $get_working_days = $this->getWorkingDays($specimen_date, $nail_date, $holidays_list)-1;
            $histo_total_working_day += $get_working_days;           
            
       	    $gross_description = $specimens['gross_description'];
			if($gross_description){	
			   $str = $gross_description; 
			   $x_postion = strpos($str, "x");	
			/**
			* 
			* @var Posistive HISTO
			* 
			*/
			  if($specimens['color'] == 'red'){
				 
			   if(!empty($x_postion)){
			     $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			     $size_data     = substr($str, $get_data_from, 10);
			   }
			   else{	
			    $postion = strpos($str, "*"); 	
			    if(!empty($postion)){
			    $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			    $size_data = substr($str, $get_data_from, 10);
			    }
			   }						
			    preg_match_all('!\d+!', $size_data, $matches);
			 
			    $red_multiply = 1;			
			    for($i=0; $i<3; $i++){
			        $red_multiply = ($matches[0][$i] * $red_multiply);
			       }
			     $red_gross = $red_multiply; 				
			   }
			   else{
			     $red_gross = "";			   	
			   }
			 
			if(!empty($x_postion)){
			   $get_data_from = ($x_postion == 1 ? $x_postion-1 : $x_postion-2);
			   $size_data     = substr($str, $get_data_from, 10);
			}
			else{	
			   $postion = strpos($str, "*"); 	
			if(!empty($postion)){
			   $get_data_from = ($postion == 1 ? $postion-1 : $postion-2);
			   $size_data = substr($str, $get_data_from, 10);
			}
			}						
			preg_match_all('!\d+!', $size_data, $matches);
			 
			   $multiply = 1;			
			for($i=0; $i<3; $i++){
			   $multiply = ($matches[0][$i] * $multiply);
			
			   }
			   $gross = $multiply; 				
			
			} 
            else{
               $gross = "";
            }
          array_push($red_gross_des, $red_gross);  
          array_push($gross_des, $gross);
        
        }           
        $histo_tat_calculation = ($histo_total_working_day/$histo_specimen_count);
        $histo_tat =  number_format((float)$histo_tat_calculation, 1, '.', ''); 
      
        if($histo_reports){           
           $histo_array = array('histo_count' => $histo_specimen_count,
           						'histo_tat'   => $histo_tat);   
        }     
       }
       else{           
           $histo_array = array('histo_count' => '0',
           						'histo_tat'   => '0');   
        }  
 
     /* PCR Reports */      
       
    $pcr_report_sql = "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date, `pcr_report`.`created_date` as pcr_create_date
    FROM 
    (SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `test_type` = 'NF' ".$physician_sql." AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' $date_range )specimen 
    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
    ORDER BY `pcr_report`.`accessioning_num` DESC ";
    
     $pcr_report_query     = $this->db->query($pcr_report_sql);     
     $pcr_specimen_count   = $pcr_report_query->num_rows();
 	 $pcr_reports = $pcr_report_query->result_array();
     
     /**
	 * 
	 * @var PCR TAT
	 * 
	 */     
       
     if($pcr_reports){ 
     $result_status         = 1;  	 
     $pcr_total_working_day = 0;       
     foreach($pcr_reports as $specimens){
            $specimen_date      = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $pcr_create_date    = date('Y-m-d', strtotime($specimens['pcr_create_date']));
            $get_working_days   = $this->getWorkingDays($specimen_date, $pcr_create_date, $holidays_list)-1;
            $pcr_total_working_day += $get_working_days;           
        }           
        
        $pcr_tat_calculation = ($pcr_total_working_day/$pcr_specimen_count);       
        $pcr_tat = number_format((float)$pcr_tat_calculation, 1, '.', ''); 
          
        if($pcr_reports){          
           $pcr_array = array('pcr_count'             => $pcr_specimen_count, 						    
						   	  'pcr_tat'               => $pcr_tat);
        } 
            
       }       
       else{          
           $pcr_array = array('pcr_count'             => '0', 						    
						   	  'pcr_tat'               => '0');
        } 
	   /**
	   * 
	   * @var Green, Red, Yellow Diagnosis Percentage
	   * 
	   */	
	   
	 if($data['from_date'] != "" &&  $data['to_date'] != "" ){
	   	
	   	$date_range = " AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'";
	  } 
	           
      $green_diagnosis_count  = diagnosis_color_code_count($code_color = 'green', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);  
     
      $red_diagnosis_count    = diagnosis_color_code_count($code_color = 'red', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);
    
      $yellow_diagnosis_count = diagnosis_color_code_count($code_color = 'yellow', $data_type, $data['physician_id'], $data['from_date'], $data['to_date']);   
      $total_specimen_codes_count = ($green_diagnosis_count + $red_diagnosis_count + $yellow_diagnosis_count);	
    
      if($total_specimen_codes_count != 0 ){
        $yellow_per = round(($yellow_diagnosis_count / $total_specimen_codes_count) *100);
        $red_per    = round(($red_diagnosis_count / $total_specimen_codes_count) *100);
        $green_per  = round(($green_diagnosis_count / $total_specimen_codes_count) *100);
       
        $diagnostics = array('yellow' => $yellow_per, 'red' => $red_per, 'green' => $green_per );
       }
  		
   
	/**
	* 
	* Combination  Specimen SQL
	* 
	*/ 
	  
	/*$combination_specimens_sql = "SELECT `assessioning_num`, `id`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `physician_id` = '".$data['physician_id']."' 
	AND `test_type` = 'NF'
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id`";
            */
            
   $combination_specimens_sql = "SELECT `assessioning_num`, `id`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `collection_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'  
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id`
	
	INNER JOIN 
    (SELECT `specimen_id` FROM `$tbl2`)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`
    
    INNER JOIN 
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3`)pcr_report
     ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`";       
     
            
     $combination_specimens_query = $this->db->query($combination_specimens_sql);     
     $combination_specimens_count = $combination_specimens_query->num_rows();
 	 $combination_specimens       = $combination_specimens_query->result_array();
	 
	 $concordance_histo_pcr_pos = 0; 
	 $concordance_histo_pcr_neg = 0;	 
	 $histo_pos_pcr_neg = 0;	 
	 $histo_neg_pcr_pos = 0;	 
	
	 foreach($combination_specimens as $specimen){
	 	 $specimen_id = $specimen['id'];
	 	 $accessioning_num = $specimen['assessioning_num'];
	 	
	 	 /**
		 * Positive HISTO and PCR Process check
		 */
		 
	 	 $pos_histo_pcr_process = pos_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($pos_histo_pcr_process == 1){	
		    $concordance_histo_pcr_pos = $concordance_histo_pcr_pos+1; 
		 }		 	 	
	 	
	 	 /**
		  * Negetive HISTO and PCR Process check
		  */
		  		  		  
	 	  $neg_histo_pcr_process    = neg_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );	 
	 	  if($neg_histo_pcr_process == 1){	
		    $concordance_histo_pcr_neg = $concordance_histo_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Positive and PCR Negative Process check
		 */
		 
	 	 $histo_pos_pcr_neg_process = histo_pos_pcr_neg_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_pos_pcr_neg_process == 1){	
		    $histo_pos_pcr_neg = $histo_pos_pcr_neg+1; 
		 }	
	   
	   
	     /**
		 *  HISTO Negative and PCR Positive Process check
		 */
		 
	 	 $histo_neg_pcr_pos_process = histo_neg_pcr_pos_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 );		 
		 if($histo_neg_pcr_pos_process == 1){	
		    $histo_neg_pcr_pos = $histo_neg_pcr_pos+1; 
		 }	
   
	   }
	 
		 /**
	     * 
	     * @var Concordance
	     * 
	     */     
	    $concordance = 0;
	    if($concordance_histo_pcr_pos > 0 || $concordance_histo_pcr_neg > 0){         
	       $concordance = round((($concordance_histo_pcr_pos + $concordance_histo_pcr_neg)/$combination_specimens_count)*100);	
	    }
	    
	    /**
		* 
		* @var  HISTO Pos and PCR Neg
		* 
		*/
	    $histo_pos_pcr_neg_pec = 0;
		if($histo_pos_pcr_neg > 0 ){
		   $histo_pos_pcr_neg_pec = round((($histo_pos_pcr_neg)/$combination_specimens_count)*100);	
		} 
		
	    
	    /**
		* 
		* HISTO Neg PCR Pos
		* 
		*/
        $histo_neg_pcr_pos_pec = 0;
   
		if($histo_neg_pcr_pos > 0){	
		   $histo_neg_pcr_pos_pec = round((($histo_neg_pcr_pos)/$combination_specimens_count)*100);	
		}
	    
       
        /**
		 * 
		 * @var MRSA Positive
		 * 
		 */
		 
	    $mrsa_pos = 0;
        if($pcr_specimen_count > 0 ){
		$mrsq_pos_sql =  "SELECT `specimen`.`assessioning_num`,`specimen`.`create_date` as specimen_create_date
		FROM 
		(SELECT `id`, `create_date`, `assessioning_num` FROM `$tbl1` WHERE `test_type` = 'NF' ".$physician_sql." AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' )specimen 
		INNER JOIN         

		(SELECT `positive_negtaive`, `accessioning_num` FROM $tbl4 WHERE `positive_negtaive` = 'positive' AND ( `target_name` = 'mecA_Pa04230908_s1' )) pcr_report

		ON 
		`pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`";
       
          $mrsa_query    = $this->db->query($mrsq_pos_sql);  
          $mrsa_pcr_pos  = $mrsa_query->num_rows();		
         if($mrsa_pcr_pos > 0){
		 	$mrsa_pos =  round(( $mrsa_pcr_pos / $pcr_specimen_count)*100);	
		   }          
		 }       
   
        /**
		* 
		* @var Size Distribution
		* 
		*/

	    $first  = 0;
	    $second = 0;
	    $third  = 0; 
	    $fourth = 0;
	    $fifth  = 0; 
	    $sixth  = 0;
	    $seventh= 0;
	for ($i=0; $i<count($gross_des); $i++)
       {    	    	
    	if($gross_des[$i] >= 1 && $gross_des[$i] <= 100)
        {
			$first++;
		}
		if($gross_des[$i] >= 101 && $gross_des[$i] <= 200)
        {
			$second++;
		}
		if($gross_des[$i] >= 201 && $gross_des[$i] <= 300)
        {
			$third++;
		}
		if($gross_des[$i] >= 301 && $gross_des[$i] <= 400)
        {
			$fourth++;
		}
		if($gross_des[$i] >= 401 && $gross_des[$i] <= 500)
        {
			$fifth++;
		}
		if($gross_des[$i] >= 501 && $gross_des[$i] <= 600)
        {
			$sixth++;
		}
    	if($gross_des[$i] >= 601 )
        {
           $seventh++; 
        }
       }
	 
        $size_distribution = array('0-100' => $first,'101-200' => $second,'201-300' => $third,'301-400' => $fourth,'401-500' => $fifth,'501-600' => $sixth, '601-n' => $seventh);
        

	 /**
	 * 
	 * @var Infection graph
	 * 
	 */
	    $inf_first  = 0;
	    $inf_second = 0;
	    $inf_third  = 0; 
	    $inf_fourth = 0;
	    $inf_fifth  = 0; 
	    $inf_sixth  = 0;
	    $inf_seventh= 0;
	for ($i=0; $i<count($red_gross_des); $i++)
       {    	    	
    	if($red_gross_des[$i] >= 1 && $red_gross_des[$i] <= 100)
        {
			$inf_first++;
		}
		if($red_gross_des[$i] >= 101 && $red_gross_des[$i] <= 200)
        {
			$inf_second++;
		}
		if($red_gross_des[$i] >= 201 && $red_gross_des[$i] <= 300)
        {
			$inf_third++;
		}
		if($red_gross_des[$i] >= 301 && $red_gross_des[$i] <= 400)
        {
			$inf_fourth++;
		}
		if($red_gross_des[$i] >= 401 && $red_gross_des[$i] <= 500)
        {
			$inf_fifth++;
		}
		if($red_gross_des[$i] >= 501 && $red_gross_des[$i] <= 600)
        {
			$inf_sixth++;
		}
    	if($red_gross_des[$i] >= 601 )
        {
           $inf_seventh++; 
        }
       }
	 
 
	   if($inf_first > 0){	
		  $inf_first  = round(($inf_first/$first)*100);
		}   
	   if($inf_second > 0){	
		  $inf_second  = round(($inf_second/$second)*100);
		}
	   if($inf_third > 0){	
		  $inf_third  = round(($inf_third/$third)*100);
		}   
	   if($inf_fourth > 0){	
		  $inf_fourth  = round(($inf_fourth/$fourth)*100);
		}  
	   if($inf_fifth > 0){	
		  $inf_fifth  = round(($inf_fifth/$fifth)*100);
		}   
	   if($inf_sixth > 0){	
		  $inf_sixth  = round(($inf_sixth/$sixth)*100);
		}
		if($inf_seventh > 0){	
		  $inf_seventh  = round(($inf_seventh/$seventh)*100);
		}
		 
        $infection = array('0-100' => $inf_first.'%','101-200' => $inf_second.'%','201-300' => $inf_third.'%','301-400' => $inf_fourth.'%','401-500' => $inf_fifth.'%','501-600' => $inf_sixth.'%', '601-n' => $inf_seventh.'%');
  

	/**
	* 
	* @var Volume Start Here
	* 
	*/
	
	$volume = "";
    $accessioned_physician_specimen_data = array();
    $reported_physician_specimen_data = array();
  
   
    /**
	 * 
	 * @var Accessioned Specimen Count
	 * 
	 */
	
	/**
	* 
	* @var Accessioned All Specimens
	* 
	*/
		  
    $accessioned_physician_all_specimen_count_sql = "SELECT date_format(`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
	
	 /**
	 * 
	 * @var Accessioned HISTO
	 * 
	 */
		  
   $accessioned_physician_histo_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
		
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
	$accessioned_physician_pcr_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d') AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%4%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%7%' ))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
	

    /**
	* 
	* @var Accessioned All Specimens
	* 
	*/
	
    $accessioned_physician_all_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_all_specimen_count_sql);
  
    /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $accessioned_physician_histo_specimen_count_data = $this->BlankModel->customquery($accessioned_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $accessioned_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($accessioned_physician_pcr_specimen_count_sql);

   
    if(!empty($accessioned_physician_histo_specimen_count_data) || !empty($accessioned_physician_pcr_specimen_count_data) || !empty($accessioned_physician_all_specimen_count_data) ){
    	
    	
      $collection_date_arr = array();
      $spe_count_arr       = array();
      $sum_accessioned_histo_specimen = 0;
      foreach ($accessioned_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_accessioned_histo_specimen += $total_specimen;
        array_push($collection_date_arr, $collection_date);
        array_push($spe_count_arr, $total_specimen);
        }
   
		$histo_phy_from_date = $data['from_date'];
		$histo_phy_to_date   = $data['to_date'];

		$pcr_collection_date_arr = array();
		$pcr_spe_count_arr       = array();
		$sum_accessioned_pcr_specimen = 0;
      foreach ($accessioned_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_accessioned_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_collection_date_arr, $pcr_collection_date);
        array_push($pcr_spe_count_arr, $pcr_total_specimen);
        }		
        
        $all_collection_date_arr = array();
		$all_spe_count_arr       = array();
		$sum_accessioned_all_specimen = 0;
      foreach ($accessioned_physician_all_specimen_count_data as $key => $value) {
        $all_collection_date = $value['collection_date'];
        $all_total_specimen  = $value['total_specimen'];
        $sum_accessioned_all_specimen += $all_total_specimen;
        array_push($all_collection_date_arr, $all_collection_date);
        array_push($all_spe_count_arr, $all_total_specimen);
        }
       
    while (strtotime($histo_phy_from_date) <= strtotime($histo_phy_to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($histo_phy_from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($histo_phy_from_date, 'ALL', (Int)$all_spe_count_arr[array_search($histo_phy_from_date, $all_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($histo_phy_from_date,'ALL', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $all_new_collection_arr);
        }
     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($histo_phy_from_date, $collection_date_arr)){
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$spe_count_arr[array_search($histo_phy_from_date,$collection_date_arr)]);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($histo_phy_from_date, 'HISTO', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($histo_phy_from_date, $pcr_collection_date_arr)){
         
          $pcr_new_collection_arr = array($histo_phy_from_date, 'PCR', (Int)$pcr_spe_count_arr[array_search($histo_phy_from_date, $pcr_collection_date_arr)]);
         
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($histo_phy_from_date,'PCR', (Int)$blank_data);
          array_push($accessioned_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         $histo_phy_from_date = date ("Y-m-d", strtotime("+1 day", strtotime($histo_phy_from_date)));
       
       }     
     }

    
    /**
	* 
	* @var Reported Specimens Count
	* 
	*/

			
	/**
	* 
	* @var Reported HISTO Specimens
	* 
	*/	
	 
	$reported_physician_histo_specimen_count_sql = "SELECT date_format(`nail_patho`.`dor`, '%Y-%m-%d')  AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id` FROM `$tbl1` 
	WHERE `test_type` = 'NF' 
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'

	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5`
	WHERE (`nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%'))clinical_info ON `specimen`.`id` = `clinical_info`.`specimen_id` 
     
    INNER JOIN 
    (SELECT `specimen_id`, `nail_funagl_id`, `dor` FROM `$tbl2` WHERE `status` = 'Active' AND `dor` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59')nail_patho      
    ON
	`specimen`.`id` = `nail_patho`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
 	
   /**
	* 
	* @var Reported PCR Specimens
	* 
	*/	
	
	$reported_physician_pcr_specimen_count_sql = "SELECT date_format(`pcr_report`.`created_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`specimen`.`id`) AS 'total_specimen'
    FROM ( SELECT `assessioning_num`, `id`, `physician_id` FROM `$tbl1` 
	WHERE `status` = '0' AND `qc_check` = '0' 
	AND `test_type` = 'NF'
	".$physician_sql."
	AND `physician_accepct` = '0'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

    INNER JOIN         
    (SELECT `accessioning_num`, `created_date` FROM `$tbl3` WHERE `created_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' )pcr_report                     
    ON 
    `pcr_report`.`accessioning_num` = `specimen`.`assessioning_num`
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
 

   /**
	* 
	* @var Accessioned HISTO
	* 
	*/
	
    $reported_physician_histo_specimen_count_data = $this->BlankModel->customquery($reported_physician_histo_specimen_count_sql);
	
	/**
	* 
	* @var Accessioned PCR
	* 
	*/
	
    $reported_physician_pcr_specimen_count_data   = $this->BlankModel->customquery($reported_physician_pcr_specimen_count_sql);
   
    if(!empty($reported_physician_histo_specimen_count_data) || !empty($reported_physician_pcr_specimen_count_data) || !empty($reported_physician_all_specimen_count_data) ){
    	
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
     
      /**
	  * 
	  * @var All Reported
	  * 
	  */
      	
      $all_collection_date_arr = array();
	  $all_spe_count_arr       = array();
	  $sum_reported_all_specimen = 0;
    
    	
      /**
	   * 
	   * @var HISTO Reported
	   * 
	   */	
      $reported_collection_date_arr = array();
      $reported_spe_count_arr       = array();
      $sum_reported_histo_specimen = 0;
      foreach ($reported_physician_histo_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen  = $value['total_specimen'];
        $sum_reported_histo_specimen += $total_specimen;
        array_push($reported_collection_date_arr, $collection_date);
        array_push($reported_spe_count_arr, $total_specimen);
        }
    
       /**
	   * 
	   * @var PCR Reported
	   * 
	   */	

		$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_reported_pcr_specimen = 0;
      foreach ($reported_physician_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_reported_pcr_specimen += $pcr_total_specimen;
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	          
       
         
       $histo_report = array_combine($reported_collection_date_arr, $reported_spe_count_arr); 
       $pcr_report   = array_combine($pcr_reported_collection_date_arr, $pcr_reported_spe_count_arr); 
  
       $other_report = array_fill_keys($pcr_reported_collection_date_arr, '0');
       
       $all_report_date_arr = ($histo_report + $other_report);
    
       $all_report_count = sum_arr($histo_report, $pcr_report, $all_report_date_arr);
      
	   $all_collection_date_arr = array_keys($all_report_count);
	   $all_spe_count_arr       = array_values($all_report_count);
       if($all_spe_count_arr){
		   foreach ($all_spe_count_arr as $value) {      
                    $sum_reported_all_specimen += $value;    
           }		
		}
           
           
             
       while (strtotime($from_date) <= strtotime($to_date)) {
    	
    	/**
		 * 
		 * @var Accessioned All Specimens
		 * 
		 */ 
             
        if(in_array($from_date, $all_collection_date_arr)){
         
          $all_new_collection_arr = array($from_date, 'ALL', (Int)$all_spe_count_arr[array_search($from_date, $all_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $all_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $all_new_collection_arr = array($from_date,'ALL', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $all_new_collection_arr);
        }     	
    	
    	/**
		* 
		* @var Accessioned HISTO Specimen
		* 
		*/
    	
        if(in_array($from_date, $reported_collection_date_arr)){
          $new_collection_arr = array($from_date, 'HISTO', (Int)$reported_spe_count_arr[array_search($from_date, $reported_collection_date_arr)]);
          array_push($reported_physician_specimen_data, $new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($from_date, 'HISTO', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $new_collection_arr);
        }
        
         /**
		 * 
		 * @var Accessioned PCR Specimens
		 * 
		 */ 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){
         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);
         
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($reported_physician_specimen_data, $pcr_new_collection_arr);
         }
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
    
     $volume =  array( 'accessioned_specimen_data' => $accessioned_physician_specimen_data, 
                       'reporte_specimen_data' => $reported_physician_specimen_data);   
     
     
    /**
	 * 
	 * @var BreakDown
	 * 
	 */
	 
     /**
	 * 
	 * @var Combination
	 * 
	 */
    
    $clinical_specimen_data = array(); 
     
    $combination_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `status` = '0' AND `qc_check` = '0' 
	AND `test_type` = 'NF'
	".$physician_sql."
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%5%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";
            
    $combination_specimen_count_data = $this->BlankModel->customquery($combination_specimen_count_sql); 
     
   /**
	* 
	* @var PAS GMS FM
	* 
	*/       
            
    $pas_gms_fm_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%1%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date`
    ORDER BY COUNT(`specimen`.`id`) DESC";    
    
    $pas_gms_fm_specimen_count_data = $this->BlankModel->customquery($pas_gms_fm_specimen_count_sql); 
   
    
    /**
	* 
	* @var PAS GMS
	* 
	*/
    
    $pas_gms_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%2%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $pas_gms_specimen_count_data = $this->BlankModel->customquery($pas_gms_specimen_count_sql);   
    
    /**
	* 
	* @var ONLY PCR
	* 
	*/
    
    $only_pcr_specimen_count_sql = "SELECT date_format(`specimen`.`create_date`, '%Y-%m-%d')  AS 'collection_date', COUNT(`nail_unit`) AS `total_specimen`
    FROM ( SELECT `assessioning_num`, `id`, `physician_id`, `create_date` FROM `$tbl1` 
	WHERE `test_type` = 'NF'
	".$physician_sql."
	AND `status` = '0' AND `qc_check` = '0' 
	AND `physician_accepct` = '0'
	
	AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59'
	AND `create_date` > '2017-03-27 23:59:59' 
	)specimen

	INNER JOIN ( SELECT `nail_unit`, `specimen_id`, `assessioning_num` FROM `$tbl5` WHERE `nail_unit` LIKE '%4%' )clinical_info 
	ON `specimen`.`id` = `clinical_info`.`specimen_id` 
      
    GROUP BY `collection_date` 
    ORDER BY COUNT(`specimen`.`id`) DESC";
   
    $only_pcr_specimen_count_data = $this->BlankModel->customquery($only_pcr_specimen_count_sql); 
   
    
    if(!empty($combination_specimen_count_data) || !empty($pas_gms_fm_specimen_count_data) || !empty($pas_gms_specimen_count_data) || !empty($only_pcr_specimen_count_data) ){
    	
      $from_date = $data['from_date'];
	  $to_date   = $data['to_date'];	
      
      /**
	  * 
	  * @var Combination Data
	  * 
	  */
      	
      $combination_collection_date_arr = array();
	  $combination_spe_count_arr       = array();
	  $sum_combination_specimen = 0;
      foreach ($combination_specimen_count_data as $key => $value) {
        $combination_collection_date = $value['collection_date'];
        $combination_total_specimen  = $value['total_specimen'];
        $sum_combination_specimen   += $combination_total_specimen;
        array_push($combination_collection_date_arr, $combination_collection_date);
        array_push($combination_spe_count_arr, $combination_total_specimen);
        }	
    	
      /**
	   * 
	   * @var PAS GMS FM
	   * 
	   */	
      $pas_gms_fm_collection_date_arr = array();
      $pas_gms_fm_count_arr       = array();
      $sum_pas_gms_fm_specimen    = 0;
      foreach ($pas_gms_fm_specimen_count_data as $key => $value) {
        $pas_gms_fm_collection_date = $value['collection_date'];
        $pas_gms_fm_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_fm_specimen   += $pas_gms_fm_total_specimen;
        array_push($pas_gms_fm_collection_date_arr, $pas_gms_fm_collection_date);
        array_push($pas_gms_fm_count_arr, $pas_gms_fm_total_specimen);
        }
    
       /**
	   * 
	   * @var PAS GMS
	   * 
	   */	

	  $pas_gms_collection_date_arr = array();
      $pas_gms_count_arr        = array();
      $sum_pas_gms_specimen     = 0;
      foreach ($pas_gms_specimen_count_data as $key => $value) {
        $pas_gms_collection_date = $value['collection_date'];
        $pas_gms_total_specimen  = $value['total_specimen'];
        $sum_pas_gms_specimen   += $pas_gms_total_specimen;
        array_push($pas_gms_collection_date_arr, $pas_gms_collection_date);
        array_push($pas_gms_count_arr, $pas_gms_total_specimen);
        }        
       
       /**
	   * 
	   * @var PCR ONLY
	   * 
	   */    
       
       	$pcr_reported_collection_date_arr = array();
		$pcr_reported_spe_count_arr       = array();
		$sum_pcr_specimen = 0;
      foreach ($only_pcr_specimen_count_data as $key => $value) {
        $pcr_collection_date = $value['collection_date'];
        $pcr_total_specimen  = $value['total_specimen'];
        $sum_pcr_specimen   += $pcr_total_specimen; 
        array_push($pcr_reported_collection_date_arr, $pcr_collection_date);
        array_push($pcr_reported_spe_count_arr, $pcr_total_specimen);
        }	
       
       while (strtotime($from_date) <= strtotime($to_date)) {    	
    	/**
		  * 
		  * @var Combination Specimens
		  * 
		  */ 
             
        if(in_array($from_date, $combination_collection_date_arr)){         
          $combination_new_collection_arr = array($from_date, 'Combination', (Int)$combination_spe_count_arr[array_search($from_date, $combination_collection_date_arr)]);         
          array_push($clinical_specimen_data, $combination_new_collection_arr);        
        }
        else{
          $blank_data = 0;
          $combination_new_collection_arr = array($from_date,'Combination', (Int)$blank_data);
          array_push($clinical_specimen_data, $combination_new_collection_arr);
         }
        
    	/**
		* 
		* @var PAS GSM FM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_fm_collection_date_arr)){
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$pas_gms_fm_count_arr[array_search($from_date, $pas_gms_fm_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_fm_collection_arr = array($from_date, 'PAS GMS FM', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_fm_collection_arr);
        }
        
        /**
		* 
		* @var PAS GSM Specimen
		* 
		*/
    	
        if(in_array($from_date, $pas_gms_collection_date_arr)){
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$pas_gms_count_arr[array_search($from_date, $pas_gms_collection_date_arr)]);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);        
        }
        else{
          $blank_data = 0;
          $pas_gms_collection_arr = array($from_date, 'PAS GMS', (Int)$blank_data);
          array_push($clinical_specimen_data, $pas_gms_collection_arr);
        }
                 
         /**
		 * 
		 * @var Only PCR Specimens
		 * 
		 */ 
		 
         if(in_array($from_date, $pcr_reported_collection_date_arr)){         
          $pcr_new_collection_arr = array($from_date, 'PCR', (Int)$pcr_reported_spe_count_arr[array_search($from_date, $pcr_reported_collection_date_arr)]);         
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
        
        }
        else{
          $blank_data = 0;
          $pcr_new_collection_arr = array($from_date,'PCR', (Int)$blank_data);
          array_push($clinical_specimen_data, $pcr_new_collection_arr);
         }
         
         $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));        
        }     
      }
     
     $breakdown = array('breakdown' => $clinical_specimen_data);  
     
           
     if($result_status == 1){
	    echo json_encode(array('status' => '1', 'histo_details' => $histo_array , 'pcr_details' => $pcr_array, 'diagnostics' => $diagnostics,
	    'concordance_histo_pcr_pos' => $concordance_histo_pcr_pos,
	    'concordance_histo_pcr_neg' => $concordance_histo_pcr_neg,
	    'combination_specimens_count' => $combination_specimens_count,
	    'concordance' => $concordance, 'histo_pos_pcr_neg_number' => $histo_pos_pcr_neg, 'histo_neg_pcr_pos_number' => $histo_neg_pcr_pos, 'histo_pos_pcr_neg' => $histo_pos_pcr_neg_pec, 'histo_neg_pcr_pos' => $histo_neg_pcr_pos_pec, 'mrsa_pos' => $mrsa_pos, 'size_distribution' => $size_distribution, 'infection' => $infection, 'red_gross' => $red_gross_des, 
	    
	    'volume' => $volume, 
	    'sum_accessioned_all_specimen' => $sum_accessioned_all_specimen, 
	    'sum_accessioned_histo_specimen'=> $sum_accessioned_histo_specimen, 
	    'sum_accessioned_pcr_specimen' => $sum_accessioned_pcr_specimen, 
	    
	    'sum_reported_all_specimen' => $sum_reported_all_specimen, 'sum_reported_histo_specimen'=> $sum_reported_histo_specimen, 'sum_reported_pcr_specimen' => $sum_reported_pcr_specimen,
	    
	     'breakdown' => $breakdown, 'total_combination_specimen' => $sum_combination_specimen, 'total_pas_gms_fm_specimen' => $sum_pas_gms_fm_specimen, 'total_pas_gms_specimen' => $sum_pas_gms_specimen ,'total_pcr_specimen'=> $sum_pcr_specimen));
	   }        
       else{
         echo json_encode(array('status'=>'1', 'histo_details' => $histo_array , 'pcr_details' => $pcr_array, 'diagnostics' => $diagnostics,
	    'concordance_histo_pcr_pos' => $concordance_histo_pcr_pos,
	    'concordance_histo_pcr_neg' => $concordance_histo_pcr_neg,
	    'combination_specimens_count' => $combination_specimens_count,
	    'concordance' => $concordance, 'histo_pos_pcr_neg_number' => $histo_pos_pcr_neg, 'histo_neg_pcr_pos_number' => $histo_neg_pcr_pos, 'histo_pos_pcr_neg' => $histo_pos_pcr_neg_pec, 'histo_neg_pcr_pos' => $histo_neg_pcr_pos_pec, 'mrsa_pos' => $mrsa_pos, 'size_distribution' => $size_distribution, 'infection' => $infection, 'red_gross' => $red_gross_des, 
	    
	    'volume' => $volume, 
	    'sum_accessioned_all_specimen' => $sum_accessioned_all_specimen, 
	    'sum_accessioned_histo_specimen'=> $sum_accessioned_histo_specimen, 
	    'sum_accessioned_pcr_specimen' => $sum_accessioned_pcr_specimen, 
	    
	    'sum_reported_all_specimen' => $sum_reported_all_specimen, 'sum_reported_histo_specimen'=> $sum_reported_histo_specimen, 'sum_reported_pcr_specimen' => $sum_reported_pcr_specimen,
	    
	     'breakdown' => $breakdown, 'total_combination_specimen' => $sum_combination_specimen, 'total_pas_gms_fm_specimen' => $sum_pas_gms_fm_specimen, 'total_pas_gms_specimen' => $sum_pas_gms_specimen ,'total_pcr_specimen'=> $sum_pcr_specimen));
       }    
    }  





}       


?>