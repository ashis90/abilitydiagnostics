import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cm-pcr-left-menu',
  templateUrl: './pcr-left-menu.component.html',
  styleUrls: ['./pcr-left-menu.component.css']
})
export class PcrLeftMenuComponent implements OnInit {
  url:any;
  constructor(private _ActivatedRoute:ActivatedRoute) { }

  ngOnInit() {
    const root= this._ActivatedRoute;
    let url = root.url['value'];
    this.url = url[0].path
  }

}
