import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PcrService } from '../../pcr.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators'; //add this
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'cm-report-generate',
  templateUrl: './report-generate.component.html',
  styleUrls: ['./report-generate.component.css']
})
export class ReportGenerateComponent implements OnInit {
  @ViewChild('uploadFile') uploadFileInput: ElementRef;
  @ViewChild('accNo') accNoData: ElementRef;
  @ViewChild('specimenType') specimenTypeData: ElementRef;
  public data: any;
  specimens_list: any = [];
  post_array: any;
  batch_review: any;
  specimen_count: any;
  loading: boolean;
  no_data: any;
  no_data_status: boolean = false;
  modalRef: BsModalRef;
  toggle: boolean = false
  filterData: any;
  specimen_id: any;
  file: any;
  hideBtn: any = false;
  pdf_generated: boolean = false;
  wounds_status:string;
  specimen_type_status:string;
  public url = environment.baseUrl + 'PdfGenerate/'
  constructor(
    private http: HttpClient,
    public itemsService: PcrService,
    public route: Router,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.specimen_id = params["accessioning_no"];
    });
  }

  ngOnInit() {
    this.spinner.show();
    //console.log(this.specimen_id);
    if (this.specimen_id) {
      this.getReportDataById(this.specimen_id);
    } else {
      this.getReportData();
    }

  }

  reviewProcessState(accessioning_no: any) {
    this.modalRef = this.modalService.show(ProcessStageModalComponent, {
      initialState: {
        title: 'reviewreport',
        accessioning_no: accessioning_no
      }
    });
  }

  getReportData() {
    this.pdf_generated = false;
    this.itemsService.reportData().subscribe(data => {
      this.post_array = data;
      this.data = this.post_array.report_specimen;
      this.specimen_count = this.post_array.specimen_count;
      if (this.post_array.status === '1') {
        this.spinner.hide();
      }
      if (this.post_array.status === '0') {
        this.no_data_status = true;
        this.spinner.hide();
        this.no_data = "No Data Found.";
      }

    });
  }

  getReportDataById(id: any) {
    let params;
    this.pdf_generated = false;
    params = { 'id': id };
    this.itemsService.reportDataById(params).subscribe(data => {
      this.post_array = data;
      this.data = this.post_array.report_specimen;
      this.specimen_count = this.post_array.specimen_count;
      this.hideBtn = this.data[0].process_done == 'Partner company' ? true : false;
      if (this.post_array.status === '1') {
        this.spinner.hide();
      }
      if (this.post_array.status === '0') {
        this.no_data_status = true;
        this.spinner.hide();
        this.no_data = "No Data Found.";
      }

    });
  }

  processReport() {
    let assignArr: any = [];
    this.data.forEach((item, indx) => {
      if (item.checked == true) {
        assignArr.push(item.accessioning_num);
      }
    });
    if (assignArr.length !== 0) {
      this.spinner.show();
      let dataset;
      this.itemsService.pcrReportView(assignArr).subscribe(data => {
        dataset = data;
        this.pdf_generated = true;
        this.data = dataset.generated_pdf;
        console.log(dataset.generated_pdf);
        this.spinner.hide();
        //  if(data['status']==='1'){
        //   this.spinner.hide();
        //    this.toastr.info('Fax send Successfully', 'Success', {
        //      timeOut: 3000
        //    });
        //    this.spinner.hide();
        //    this.route.navigate(['/pcr-stages']);
        //  }
      });
    } else {
      this.toastr.info('Please select data first', 'Error', {
        timeOut: 3000
      });
    }
  }

  sendEmailFax() {
    let dataset;
    this.spinner.show();
    let params = { 'dataList': this.data, 'user_id': localStorage.userid };
    this.itemsService.sendEmailFax(params).subscribe(data => {
      dataset = data;
      this.spinner.hide();
      if (dataset.status === '1') {
        this.toastr.info('Fax send Successfully', 'Success', {
          timeOut: 3000
        });
        this.route.navigate(['/pcr-stages']);
      }
      if (dataset.status === '0') {
        this.toastr.info('Fax send Failed', 'Error', {
          timeOut: 3000
        });
        this.route.navigate(['/pcr-stages']);
      }
    });
  }

  cancelSubmit() {
    let dataset;
    this.itemsService.unlinkGeneratedPdf(this.data).subscribe(data => {
      dataset = data;
      if (dataset.status === '1') {
        if (this.specimen_id) {
          this.getReportDataById(this.specimen_id);
        } else {
          this.getReportData();
        }
      }
    });
  }

  toggleItem(item) {
    item.checked = !item.checked;
    this.toggle = this.data.every(item => item.checked);
  }

  toggleAll() {
    this.toggle = !this.toggle;
    this.data.forEach(item => item.checked = this.toggle);
  }

  search(term: string) {
    let allData: any;
    allData = this.post_array.report_specimen;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.accessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }

  }

  getWoundStatus(e){
    this.wounds_status = e.target.value;
  }


  incomingfile(event) {
    let accessioning_num = this.accNoData.nativeElement.value;
    let specimen_type_status = this.specimenTypeData.nativeElement.value;
    if(specimen_type_status=='W'){
      //--------If specimen type == Wound-----------//
      if(this.wounds_status){
        this.file = <File>event.target.files[0];
        if (this.file) {
          this.spinner.show();
          var fileSplit = this.file['name'].split('.');
          var fileExt = '';
          if (fileSplit.length > 1) {
            fileExt = fileSplit[fileSplit.length - 1];
          }
          if (fileExt === 'xls' || fileExt === 'xlsx' || fileExt === 'doc' || fileExt === 'docx' || fileExt === 'pdf' || fileExt === 'eds') {
            const formData = new FormData();
            formData.append('name', this.file['name']);
            formData.append('file_data', this.file);
            formData.append('accessioning_num', accessioning_num);
            formData.append('wound_report_status', this.wounds_status);
            this.itemsService.uploadWoundsReport(formData).subscribe(data => {
              let response = data;
              this.getReportDataById(accessioning_num);
              if (response['status'] === '1') {
                this.spinner.hide();
                this.uploadFileInput.nativeElement.value = "";
                this.toastr.info('The file is successfully uploaded.', 'Success', {
                  timeOut: 3000
                });
              } else {
                this.spinner.hide();
                this.toastr.info('Sorry, Faild to upload the file!', 'Error', {
                  timeOut: 3000
                });
              }
            });
          } else {
            this.spinner.hide();
            this.toastr.info('Sorry, File type is not allowed. Only doc, excel and pdf!', 'Error', {
              timeOut: 3000
            });
          }
        } else {
          this.spinner.hide();
          this.toastr.info('Sorry, no file selected!', 'Error', {
            timeOut: 3000
          });
        }
      }else{
        this.spinner.hide();
          this.toastr.info('Please choose report status.', 'Error', {
            timeOut: 3000
          });
      }

    }else if(specimen_type_status=="RPP" || specimen_type_status=="UTI"){
      //--------If specimen type == RPP OR UTI-----------//
      this.file = <File>event.target.files[0];
      if (this.file) {
        this.spinner.show();
        var fileSplit = this.file['name'].split('.');
        var fileExt = '';
        if (fileSplit.length > 1) {
          fileExt = fileSplit[fileSplit.length - 1];
        }
        if (fileExt === 'xls' || fileExt === 'xlsx' || fileExt === 'doc' || fileExt === 'docx' || fileExt === 'pdf' || fileExt === 'eds') {
          const formData = new FormData();
          formData.append('name', this.file['name']);
          formData.append('file_data', this.file);
          formData.append('accessioning_num', accessioning_num);
          formData.append('wound_report_status', this.wounds_status);
          this.itemsService.uploadWoundsReport(formData).subscribe(data => {
            let response = data;
            this.getReportDataById(accessioning_num);
            if (response['status'] === '1') {
              this.spinner.hide();
              this.uploadFileInput.nativeElement.value = "";
              this.toastr.info('The file is successfully uploaded.', 'Success', {
                timeOut: 3000
              });
            } else {
              this.spinner.hide();
              this.toastr.info('Sorry, Faild to upload the file!', 'Error', {
                timeOut: 3000
              });
            }
          });
        } else {
          this.spinner.hide();
          this.toastr.info('Sorry, File type is not allowed. Only doc, excel and pdf!', 'Error', {
            timeOut: 3000
          });
        }
      } else {
        this.spinner.hide();
        this.toastr.info('Sorry, no file selected!', 'Error', {
          timeOut: 3000
        });
      }
    }

  }

}

