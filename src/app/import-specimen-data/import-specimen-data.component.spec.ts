import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportSpecimenDataComponent } from './import-specimen-data.component';

describe('ImportSpecimenDataComponent', () => {
  let component: ImportSpecimenDataComponent;
  let fixture: ComponentFixture<ImportSpecimenDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportSpecimenDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportSpecimenDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
