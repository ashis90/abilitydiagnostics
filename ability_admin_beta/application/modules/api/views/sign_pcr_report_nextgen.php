<p style="padding-top:1px;"></p>

<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4; border-top:none;font-family:Arial, Helvetica, sans-serif;font-size: 11px;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;font-size: 11px;">
    <h4 style="font:700 11px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Nail Fungal Molecular Report</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 5px 0;">
      <div style="width:15%; float:left; margin:0; padding:1% 0 1% 1%;">
        <div style="max-width:100%; margin:0 auto;">
          <img src="{logo}" alt = "ability_logo"/>
        </div>
      </div>
      <div style="width:55%; float:left; margin:20px 5px 0px; padding:0;">
        <div class="comny_tag" style="float:left; width:60%;">
          <h4 style="font:700 18px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;text-align:left;margin-left:30px;">{partner_name}</h4>
          <p style="color:#444; margin:0; padding:0; font:400 11px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{address_line1}</p>
          <p style="color:#444; margin:0; padding:0; font:400 11px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{address_line2}</p>
        </div>
        <div class="n_phn_no" style="float:left; width:40%;">
          <p style="color:#444; margin:0;font:400 11px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;">
          <span style="width:30%; display: inline-block;">Phone:</span>&nbsp;&nbsp;<span style="width:70%;padding-left:20px;">{partner_phone}</span></p>
          <p style="color:#444; margin:0;font:400 11px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;"><span>Fax:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:20%;padding-left:20px;">{partner_fax}</span></p>
          <p style="color:#444; margin:0;font:400 11px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;"><span>CLIA:</span>&nbsp;&nbsp;&nbsp;&nbsp; <span style="width:20%;padding-left:20px;">{partner_cli}</span></p>
        </div>
        <p style="color:#444; margin:0;font:400 11px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{partner_lab_doc}</p>
      </div>
      <div style="width:20%; float:left; margin:0; padding:0.5%;">
        <div style="font:400 11px/14px Arial, Helvetica, sans-serif;color: #444;margin-top:-20px;">
          <p style="margin-bottom:0;text-align:left;">Date of Report</p>
          <p style="margin:0;text-align:left;margin-bottom: 7px;"><span style="display:block;">{date}</span></p>
          <h6 style="font:700 13px/14px Arial, Helvetica, sans-serif; color:#000;text-align:left;margin:0;">Accessioning Number</h6>
          <p style="margin-top:0px;color:#000;font:bold 13px/14px Arial, Helvetica, sans-serif;text-align:left;margin-bottom: 7px;">{acc_num}</p>
          <h6 style="font:700 13px/14px Arial, Helvetica, sans-serif; color:#000;text-align:left;margin:0;">External ID</h6>
          <p style="margin-top:0px;color:#000;font:bold 13px/14px Arial, Helvetica, sans-serif;text-align:left;margin-bottom: 5px;">{patient_id}</p>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
   <div style="width:100%;border-bottom:1px dotted #8db3e2; height:2px;"></div>
</div>
  <div style="padding:3px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
		<p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 11px Arial, Helvetica, sans-serif; color:#000;">Patient Information:</div>
        <div style="width:50%;float:left;color:#00;font:bold 11px Arial, Helvetica, sans-serif;margin:0 0 0px;text-transform:capitalize;">{p_name}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Phone:</div>
        <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{p_phone}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">DOB:</div>
        <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{p_dob}</div>
        </p>
        <p style="display:block;margin:0 0 5px">
        <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Collection Date:</div>
        <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{p_col_date}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Date Received:</div>
        <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{p_rec_date}</div>
        </p>
      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
		<div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 11px Arial, Helvetica, sans-serif;">Referring Physician:</div>
        <div style="width:50%;float:left;color:#00;font:bold 11px Arial, Helvetica, sans-serif;margin:0 0 0px;">{phy_name}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Address:</div>
          <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{phy_address}</div>
          </p>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px;">
          <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Phone:</div>
          <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{phy_phone}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px">
          <div style="width:40%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">Fax:</div>
          <div style="width:50%;float:left;color:#444;font:400 11px/14px Arial, Helvetica, sans-serif;">{phy_fax}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
   <div class="clearfix"></div>             
  </div>           
                
                
<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4;border-top:none;">        
                
 <div style="padding: 0 30px;padding-bottom:10px;">             
  <div style="margin-bottom:0; float:left; width:20%;"><h2 style="color: #000;font-size: 11px;margin-bottom:0;font-family:Arial, Helvetica, sans-serif;"><span style="border-bottom: 2px solid #000;margin-top: 0;">PCR Results:</span></h2></div>
		   <div style=" float :left;width: 50%;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size: 11px; margin-top:5px;margin-bottom: 20px;">
            {table_data}
          </div>
          <div style="width:40%; float:left; margin: 0; padding:0;">
            <span style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;border-bottom: 2px solid #000;">Gross Description:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{addi_desc}</span>
     </div>
     
       <div style="width:28%; float:left; margin: 0; padding:0;">
            <span style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;margin-left: 5px;border-bottom: 2px solid #000;">Location:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{location}</span>
     </div>
     
     
     
       <div style="width:31.3333%; float:left; margin: 0; padding:0;">
            <span style="border-bottom: 2px solid #000;margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;">Site Indicator:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{site_indicator}</span>
     </div>
     
    
           <div style="clear:both"></div>
        
					<div><h2 style="margin-bottom:1px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;"><span style="border-bottom: 2px solid #000;
margin-top: 0;">Molecular Interpretation of Fungus and mecA gene:</span></h2></div>
					<div style="font-family:Arial, Helvetica, sans-serif;font-size:11px;">
					<p style="margin:0;margin-top:3px;font-size:11px;">A positive result indicates amplification of DNA from the specified organism.</p>
					<p style="margin:0;margin-top:5px;font-size:11px;">A negative result indicates the absence of amplification of DNA from the specified organism.</p>
					
	         	  	
					<p style="margin:0;margin-top:5px;font-size:11px;">Clinicians must use clinical judgment when basing treatment on PCR (polymerase chain reaction) results only.
					
					Clinical false positives and false negatives occur. A clinical false positive occurs when organisms are detected in the
					specimen, but are not causing infection or pathogenic change in the patient. A clinical false negative occurs when
					an organism is present in the nail and causing clinical disease, but amplification does not occur.</p>
					</div>
              {yeast}

				
            <div class="sign" style="margin-top:6px;">
            
         
            <div style="clear:both"></div>
            <span style="display:block;font-family:Arial, Helvetica, sans-serif;font-size:11px;">Report verified and autogenerated on <span style="display:block;">{date_generate}</span> at <span style="display:block;">{time_report}</span>
            
            </div>
            
				<div><h2 style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;"><span style="border-bottom: 2px solid #000;
margin-top: 0;">Test Method Validation:</span></h2></div>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:11px;">
					<p style="margin:0;margin-top:2px;font-size:11px;">The test method used to generate the above results is real-time quantitative polymerase chain reaction (QPCR). Analytical and clinical validation studies compliant with CLIA and the College of American Pathologists(CAP) were performed at Ability Diagnostics, LLC. The performance characteristics for this assay are
available upon request.</p>
				</div>
				<div><h2 style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;"><span style="border-bottom: 2px solid #000;
margin-top: 0;">
                    Lab Developed Test (LDT) Disclaimer:</span></h2>
				</div>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:11px;">
					<p style="margin:0;margin-top:2px;font-size:11px;">This test was developed and its performance characteristics determined by Ability Diagnostics, LLC. It has not been cleared or approved by the FDA. The laboratory is regulated under CLIA as qualified to perform high-complexity testing. This test is used for clinical purposes. It should not be regarded as investigational or for research.</p>
				</div>
                </div> 
				
			
		</div>  
  <div style="clear:both;"></div>
  <p style="width:100%; color:#444; font:400 10px/14px Arial,Helvetica,sans-serif; display:block;text-align:right; padding-right:20px; font-weight:bold;">Test performed at Ability Diagnostics. 858 S. Auto Mall Drive, Suite 102, American Fork, UT 84003.
  </p>
