import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcrLeftMenuComponent } from './pcr-left-menu.component';

describe('PcrLeftMenuComponent', () => {
  let component: PcrLeftMenuComponent;
  let fixture: ComponentFixture<PcrLeftMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcrLeftMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcrLeftMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
