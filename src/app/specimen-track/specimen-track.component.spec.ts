import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecimenTrackComponent } from './specimen-track.component';

describe('SpecimenTrackComponent', () => {
  let component: SpecimenTrackComponent;
  let fixture: ComponentFixture<SpecimenTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecimenTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecimenTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
