import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import { SpecimenService } from '../specimen.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


export interface StateGroup {
  ids: Number[];
  names: string[];
}
@Component({
  selector: 'cm-physician-notification-ae',
  templateUrl: './physician-notification-ae.component.html',
  styleUrls: ['./physician-notification-ae.component.css']
})
export class PhysicianNotificationAeComponent implements OnInit {

  public data : any;
  list:any = [];
  physicians_list:any = [];
  physicians_list_data:any = [];
  add_specimen:any = [];
  addNotes: FormGroup;
  public loading = false;
  no_data:any;
  no_data_status= false;
  sendData;
  postsArray:any = [] ;
  load_status:boolean = true;
  notes_count:any;
  // variables
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  public stateGroups: StateGroup[] = [{
      ids: [],
      names: []
    }]//test
  public searchData: StateGroup[] = [{
      ids: [],
      names: []
    }]
  formData: any;
  specimenStatus: any;
  editType: any;
    // variables
  constructor( private http:HttpClient, 
    public itemsService:SpecimenService,
    public notesService:NotesService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
    ){

      this.addNotes = this.fb.group({
        user_id:[''],
        notes : ['',Validators.required],
        wp_submit : ['Submit'],
        physician_id : ['',Validators.required],
        physician_name:['',Validators.required],
        
      });
     this.route.routeReuseStrategy.shouldReuseRoute = function(){
        return false;
     }

     this.route.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
           // trick the Router into believing it's last link wasn't previously loaded
           this.route.navigated = false;
           // if you need to scroll back to top, here is the right place
           window.scrollTo(0, 0);
        }
    });
  }
  ngOnInit() {
    const id= +this._ActivatedRoute.snapshot.paramMap.get('id');
    this.editType = this._ActivatedRoute.snapshot.paramMap.get('type');
    if(id!==0 && this.editType==='edit'){
      this.setFormValue(id);
    }
    this.getPhysicians(); //Getting physician details
    this.getAllNotesDetails();
  /*........Calling physician autocomplete.........*/
    this.filteredOptions = this.addNotes.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value?value.toLowerCase():''))
    );
  /*........End of calling physician autocomplete.........*/

  } //End of ngOnInit
  /*..................Assigning Edit Data...................*/
  setFormValue(id:any){
    let params;
    let notedetails;
    let response;
    let physician_name;
    this.spinner.show();
    params = {'note_id':id};
    this.addNotes.controls['user_id'].setValue(localStorage.userid);
    this.notesService.getNotesDetails(params).subscribe(data => {
    response = data;
    notedetails = response.note_details;
    physician_name = response.physician_name;
    this.addNotes.controls['notes'].setValue(notedetails['notifications']);
    this.addNotes.controls['physician_name'].setValue(physician_name);
    this.addNotes.controls['physician_id'].setValue(notedetails['physician_id']);
   });
  }

  getAllNotesDetails(){
    this.addNotes.controls['user_id'].setValue(localStorage.userid);
    this.addNotes.controls['wp_submit'].setValue('Submit');
    this.spinner.show();
    this.notesService.getAllNotesDetails().subscribe(data => { 
      this.loading = false;
        this.postsArray = data;
        if(this.postsArray['status'] ==='1'){
          this.data = this.postsArray.notes_data;
          this.notes_count = this.postsArray.notes_count;
          this.no_data_status= false;
          this.spinner.hide();
        }
        if(this.postsArray['status'] ==='0'){
          this.no_data_status= true;
          this.no_data="No Data Found.";
          this.spinner.hide();
        }
    });
  }
   /*..................End of assigning Edit Data...................*/

  /*..............Get physician function.............*/
  getPhysicians()
    {   
      this.spinner.show();
      this.itemsService.physicians_list().subscribe(data => { 
        this.list = data;
        this.physicians_list = this.list.physicians_data;    
        this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
          this.physicians_list.map((val) => this.allIds.push ( val.id));
          this.physicians_list.map((data) => this.filterVal.push ( data));
          this.spinner.hide();
      });
      this.stateGroups = [{
        ids:this.allIds,
        names:this.allNames
      }];
    }
  /*..............End of get physician function.............*/
  
  /*..............Form submit function.............*/
    onSubmit()
    {   
        console.log(this.addNotes);
        if(this.addNotes.status == "VALID")
        {   
                  
            let fmData = Object.assign({}, this.addNotes.value); 
            let serializedForm = JSON.stringify(fmData);
            const id= +this._ActivatedRoute.snapshot.paramMap.get('id');
              this.editType = this._ActivatedRoute.snapshot.paramMap.get('type');

              if(id!==0 && this.editType==='edit'){
                let formObj = {...fmData,id}; // {name: '', description: ''}
                this.notesService.updatesNotes( formObj).subscribe(data => {
                  this.setFormValue(id);
                  this.loading = false;
                  if(data['status'] == '1'){
                    this.getAllNotesDetails();
                    this.toastr.info('Data Successfully updated', 'Success', {
                      timeOut: 3000
                    });
                  }
                });
              }else if(id===0 && this.editType==='add'){
                this.notesService.addNotes( serializedForm).subscribe(data => {
                  this.loading = false;   
                  if(data['status'] == '1'){
                    this.resetForm();
                    this.getAllNotesDetails();
                    this.toastr.info('Note successfully added.', 'Success', {
                      timeOut: 3000
                    });
                  }else{
                    this.toastr.info('Fail to add note.', 'Success', {
                      timeOut: 3000
                    });
                  }
                });
              }
           
        }
        else
        {
          this.validateAllFormFields(this.addNotes);
        }
    }

    resetForm(){
      this.addNotes.reset();
      this.getAllNotesDetails();
    }
  /*..............End of form submit function.............*/

  /*..............Autocomplete custom filter function.............*/
  private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount : number = this.filterVal.length;
        this.options = [];
        this.ids=[];
        for(let i=0;i<itemCount;i++){
            if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ){
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids:this.ids,
          names:this.options
        }];
        return this.searchData;
    }
    return this.stateGroups;
  }
  /*..............End of autocomplete custom filter function.............*/
  
  /*..............Form validation functions.............*/
  validateAllFormFields(formGroup: FormGroup) {         //{1}
      Object.keys(formGroup.controls).forEach(field => {  //{2}
        const control = formGroup.get(field);             //{3}
        if (control instanceof FormControl) {             //{4}
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        //{5}
          this.validateAllFormFields(control);            //{6}
        }
      });
    }
  
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.addNotes.get(field).valid && (this.addNotes.get(field).touched || this.addNotes.get(field).dirty);
  }
  /*..............End of Form validation functions.............*/

  /*..............Display physician details function.............*/
  callSomeFunction(phy_id :any){
    this.addNotes.controls['physician_id'].setValue(phy_id);
  }
  /*..............End of display physician details function.............*/

  deleteNotesData(id:any){
    let res = confirm("Want to delete?");
    if(res){
    let params;
    params= {note_id:id};
    this.notesService.notesDelete(params).subscribe(data => { 
      let response = data;
      this.loading = true;
        
      if(response['status'] === '1'){
        this.getAllNotesDetails();
        this.toastr.info('Notification deleted successfully', 'Success', {
          timeOut: 3000
        });
      }
    });
  }

  }
  

}
