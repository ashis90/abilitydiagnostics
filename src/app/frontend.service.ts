import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FrontendService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl + 'Frontapi/';


  makeHttpGetRequest() {

    return this.http.get(this.url + 'home_page_details', this.options);
  }
  makeAboutRequest() {

    return this.http.get(this.url + 'about_page_details', this.options);
  }
  makeContactRequest() {

    return this.http.get(this.url + 'contact_page_details', this.options);
  }
  makePrivacyRequest() {

    return this.http.get(this.url + 'privacy_page_details', this.options);
  }
  makeServiceRequest() {

    return this.http.get(this.url + 'service_page_details', this.options);
  }
  makeServiceRequestForNails() {

    return this.http.get(this.url + 'service_page_details_for_nails', this.options);
  }
  makeWoundRequest() {

    return this.http.get(this.url + 'wound_care_page_details', this.options);
  }

  ourScienceBaisedService() {

    return this.http.get(this.url + 'science_based_service', this.options);
  }

  makeServiceDetailsRequest(id) {
    return this.http.post(this.url + 'service_details', JSON.stringify(id), this.options);
  }

  send_mail(data) {
    return this.http.post(this.url + 'send_contact_mail', JSON.stringify(data), this.options);
  }

}
