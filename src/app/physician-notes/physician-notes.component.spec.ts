import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicianNotesComponent } from './physician-notes.component';

describe('PhysicianNotesComponent', () => {
  let component: PhysicianNotesComponent;
  let fixture: ComponentFixture<PhysicianNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicianNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicianNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
