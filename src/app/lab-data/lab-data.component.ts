import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-lab-data',
  templateUrl: './lab-data.component.html',
  styleUrls: ['./lab-data.component.css']
})
export class LabDataComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  data;
  sendData;
  msg;
  filterData: any;
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({
      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)

    });
    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {

    if (this.load_status == true) {
      this.spinner.show();
      this.itemsService.get_lab_data().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
        this.msg = this.postsArray.data_display_date;
      });

    }
  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }
  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      //this.load_status=false;
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), "to_date": this.convertDate(this.searchForm.value.to_date), 'dataType': this.searchForm.value.dataType };
      this.itemsService.search_lab_data(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
        this.msg = this.postsArray.data_display_date;
      });
    }
    else {
      this.validateAllFormFields(this.searchForm);
    }
  }
  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }
  search(term: string) {
    let allData: any;
    allData = this.postsArray.details;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.assessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }

  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}

