import { sandboxOf } from 'angular-playground';
import { ClinicalComponent } from './clinical.component';

export default sandboxOf(ClinicalComponent)
  .add('Clinical Component', {
    template: `<cm-clinical></cm-clinical>`
  });
