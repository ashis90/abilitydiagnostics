import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoundCareComponent } from './wound-care.component';

describe('WoundCareComponent', () => {
  let component: WoundCareComponent;
  let fixture: ComponentFixture<WoundCareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoundCareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoundCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
