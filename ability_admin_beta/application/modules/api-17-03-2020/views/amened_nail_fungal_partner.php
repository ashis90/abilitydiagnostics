{Amended_Report}
<div style="width:96%; margin:15px auto;display:block;border:1px solid #4285f4; border-top:none;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;">
    <h4 style="font:700 16px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Nail Fungal Pathology Report</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 5px 0;">
      <div style="width:15%; float:left; margin:0; padding:1% 0 1% 1%;">
        <div style="max-width:100%; margin:15px auto 0 auto;">
          <img src="{logo}" alt = "ability_logo"/>
        </div>
      </div>
      <div style="width:55%; float:left; margin:20px 5px 0px; padding:0;">
        <div class="comny_tag" style="float:left; width:60%;">
          <h4 style="font:700 18px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;text-align:left;margin-left:30px;">{partner_name}</h4>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{address_line1}</p>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{address_line2}</p>
        </div>
        <div class="n_phn_no" style="float:left; width:40%;">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;">
          <span style="width:30%; display: inline-block;">Phone:</span>&nbsp;&nbsp;<span style="width:70%;padding-left:20px;">{partner_phone}</span></p>
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;"><span>Fax:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:20%;padding-left:20px;">{partner_fax}</span></p>
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:5px;"><span>CLIA:</span>&nbsp;&nbsp;&nbsp;&nbsp; <span style="width:20%;padding-left:20px;">{partner_cli}</span></p>
        </div>
        <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;text-align:left;margin-left:30px;">{partner_lab_doc}</p>
      </div>
      <div style="width:20%; float:left; margin:0; padding:0.5%;">
        <div style="font:400 12px/14px Arial, Helvetica, sans-serif;color: #444;margin-top:-20px;">
          <p style="margin-bottom:0;text-align:left;">Date of Report</p>
          <p style="margin:0;text-align:left;margin-bottom: 7px;"><span style="display:block;">{date}</span></p>
          <h6 style="font:700 13px/14px Arial, Helvetica, sans-serif; color:#000;text-align:left;margin:0;">Accessioning Number</h6>
          <p style="margin-top:0px;color:#000;font:bold 13px/14px Arial, Helvetica, sans-serif;text-align:left;margin-bottom: 7px;">{assessioning_num}</p>
          <h6 style="font:700 13px/14px Arial, Helvetica, sans-serif; color:#000;text-align:left;margin:0;">External ID</h6>
          <p style="margin-top:0px;color:#000;font:bold 13px/14px Arial, Helvetica, sans-serif;text-align:left;margin-bottom: 5px;">{patient_id}</p>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
   <div style="width:100%;border-bottom:1px dotted #8db3e2; height:2px;"></div>
</div>
  <div style="padding:5px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
		<p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 14px Arial, Helvetica, sans-serif; color:#000;">Patient Information:</div>
        <div style="width:50%;float:left;color:#00;font:bold 14px Arial, Helvetica, sans-serif;margin:0 0 0px;text-transform:capitalize;">{patient_name}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_phone}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">DOB:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_dob}</div>
        </p>
        <p style="display:block;margin:0 0 5px">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Collection Date:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_col}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Date Received:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{patient_recive}</div>
        </p>
      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
		<div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 14px Arial, Helvetica, sans-serif;">Referring Physician:</div>
        <div style="width:50%;float:left;color:#00;font:bold 14px Arial, Helvetica, sans-serif;margin:0 0 0px;">{physician_name}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Address:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_addresss}</div>
          </p>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_mobile}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
        <div style="margin:0 0 6px 0;">
          <p style="display:block;margin:0 0 5px">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{physician_fax}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>
  <div class="patholgy_rpt">
    <div class="tp_hdng" style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:5px 8px; text-align:center; font:700 22px Arial, Helvetica, sans-serif;color:#000;">
      <h2 style="font:700 16px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Pathology Report</h2>
    </div>
    <div style="padding:10px;">
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label> Clinical History: </label>
        </div>
        <div style=" width:38%; padding:0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          {clinical_history}
        </div>
        <div style=" width:38%; padding:0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          {clinicial_info}
        </div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Gross Description:</label>
        </div>
        <div style=" width:76%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif">
          {gross_desc}
        </div>
        <div style="clear:both;"></div>
      </div>
        <div style="margin:0 0 10px 0;">
        <div class="note_heading">
          <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
            Amended Diagnosis:
          </div>
          <div style="width:76%; padding:0;float:left; margin:0;color:#ff0000;font:bold 12px/14px Arial,Helvetica,sans-serif">
            {note}
          </div>
          <div class="clear"></div>
        </div>
      </div>     
      
      <div style="margin:0 0 5px 0; padding:3px 2px;">
        <div style="width:20%; border:4px solid {diagnostic_color};  background:white; padding:2px 0 2px 2px;float:left; margin:0 0 0 -5px; color:#000;font:400 16px Arial,Helvetica,sans-serif; ">
          <label style=""><strong >Diagnosis: </strong></label>
        </div>
        <div style=" width:76%; float:left; margin:0 0 0 2%; font:400 16px Arial,Helvetica,sans-serif; color:#000; border:4px solid {diagnostic_color};  background:white; padding:2px 0 2px 2px;">
          <strong style="padding-left:2%;">{diagnostic}</strong>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style="width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Micro Description:</label>
        </div>
        <div style=" width:76%; float:left; margin:0;color:#444; font:400 12px/14px Arial,Helvetica,sans-serif; ">
          {gross_micro_description}
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="margin:0 0 5px 0;">
        <div style=" width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
          <label>Stains:</label>
        </div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_fst}</div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_sec}</div>
        <div style=" width:77%; padding:0;float:right; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">{Stains_desc_thr}</div>
		    <div style="clear:both;"></div>
      </div>
	    {nail_fungal_img}
		{addendum}
			<div style="margin:0 0 5px 0;">
		        <div style="width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px/14px Arial,Helvetica,sans-serif">
		          <label>Comments:</label>
		        </div>
		        <div style="width:76%; float:left; margin:0;color:#444; font:400 12px/14px Arial,Helvetica,sans-serif; ">
		          {stains_comments}
				  {comments}
		        </div>  
				 <div style="clear:both;"></div>
		      </div>
			</div>
	    <div style="width:31.3333%; float:left; margin:0; padding:1%;">
       <p style="color:#444; margin:0; padding:0; font:100 10px/14px Arial, Helvetica, sans-serif; float:right;">
        {lab_doc_desc}<br/>
        Board Certified Dermatopathologist<br/>
        <img style="height:70px; width:128px;" src={sign_img} alt="laboratory doctor sign" />
      </p>
	  
	   </div>
	   <div style="clear:both;"></div>
	   
	   <div style="width:48%; float:left; color:#444; padding:1%; font:400 10px/14px Arial,Helvetica,sans-serif; ">Report signed on <span style="display:block;">{date}</span>at <span style="display:block;">{time}</span></div>   
	   
	   <div style="float:left; color:#444; width:48%; padding:1%; text-align:right; font:400 10px/14px Arial,Helvetica,sans-serif; ">{partner_footer_data}</div>
     <div style="clear:both;"></div>
    </div>
  </div>		