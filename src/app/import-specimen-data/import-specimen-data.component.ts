import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SpecimenService } from '../specimen.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-import-specimen-data',
  templateUrl: './import-specimen-data.component.html',
  styleUrls: ['./import-specimen-data.component.css']
})


export class ImportSpecimenDataComponent implements OnInit {
  @ViewChild("xlFile") xlFile: ElementRef;
  @ViewChild("partnerVal") partnerVal: ElementRef;
  arrayBuffer: any;
  file: File;
  excelData: any[][];
  postArray: any;
  analyze_data: any;
  unique_data: any;
  data: any;
  barcode: any;
  analyze_data_count: any;
  loading: boolean;
  object_keys: any;
  parrners_data: any;
  error_data: any;
  showErrList: boolean = false;
  error_message: any;
  constructor(public itemsService: SpecimenService, private toastr: ToastrService, private spinner: NgxSpinnerService) {
    this.excelData = [];
  }

  ngOnInit() {
    this.itemsService.getPartnersCompanyDetails().subscribe(data => {
      let post_array;
      post_array = data;
      if (post_array.status === '1') {
        this.parrners_data = post_array.parners_detail;
      }

    });
  }

  incomingfile(event) {
    this.file = event.target.files[0];
  }

  Upload() {

    let partnersCompany = this.partnerVal.nativeElement.value;
    if (!partnersCompany) {
      this.toastr.info("Please Select a Partner's company.", 'Error', {
        timeOut: 3000
      });
    } else if (this.file) {
      if (confirm("Want to import excel?")) {
        //this.spinner.show();
        let fileReader = new FileReader();
        let fileData: any;
        fileReader.readAsArrayBuffer(this.file);
        fileReader.onload = (e) => {
          this.arrayBuffer = fileReader.result;
          var data = new Uint8Array(this.arrayBuffer);
          var arr = new Array();
          for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, { type: "binary" });
          var first_sheet_name = workbook.SheetNames[0];
          var worksheet = workbook.Sheets[first_sheet_name];
          fileData = XLSX.utils.sheet_to_json(worksheet, { raw: false });

          fileData.forEach(function (row) {
            Object.keys(row).map((elem, indx) => {
              row[elem.replace(/\s+/g, '_').toLowerCase()] = row[elem];
              delete row[elem];
            })

          });
          console.log(fileData);
          /* display the result */
          let params = { 'import_data': fileData, 'user_id': localStorage.userid, 'partners_company': partnersCompany };
          this.itemsService.importSpecimenData(params).subscribe(data => {
            this.spinner.hide();
            let postarr;
            postarr = data;
            if (postarr['status'] === '1') {
              this.xlFile.nativeElement.value = '';
              this.partnerVal.nativeElement.value = '';
              this.error_data = postarr.error_fields;
              if (Object.keys(this.error_data)) {
                this.showErrList = true;
              } else {
                this.showErrList = false;
              }
              if (postarr['message'] != '') {
                this.error_message = postarr['message'];
              }
              this.toastr.info('Data Successfully imported', 'Success', {
                timeOut: 3000
              });

            } else if (postarr['status'] === '0') {
              this.toastr.info('Data faild to import', 'Error', {
                timeOut: 3000
              });
            } else if (postarr['status'] === '2') {
              this.toastr.info(postarr['message'], 'Error', {
                timeOut: 3000
              });
            }
          });

        }
      }
    } else {
      this.toastr.info('Please choose a file.', 'Error', {
        timeOut: 3000
      });
    }

  }

}
