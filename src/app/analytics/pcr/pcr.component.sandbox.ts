import { sandboxOf } from 'angular-playground';
import { PcrComponent } from './pcr.component';

export default sandboxOf(PcrComponent)
  .add('Pcr Component', {
    template: `<cm-pcr></cm-pcr>`
  });
