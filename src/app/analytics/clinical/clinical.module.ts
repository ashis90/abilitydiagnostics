import { NgModule } from '@angular/core';

import { ClinicalRoutingModule } from './clinical-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';

@NgModule({
  imports:      [ ClinicalRoutingModule,NavbarModule ],
  declarations: [ ClinicalRoutingModule.components ]
})
export class ClinicalModule { }
