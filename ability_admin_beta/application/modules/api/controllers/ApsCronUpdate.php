<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class ApsCronUpdate extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {

    }
     
  /**
    * /
    *  @ get_physician_active_list
    *
    */

        /*
      |--------------------------------------------------------------------------
      | ability Add User Table
      |--------------------------------------------------------------------   
      */

       function get_ability_table_details(){
        $data = json_decode(file_get_contents('php://input'), true);
        
      $sql = 'SELECT * FROM '. $data['table'] .' WHERE `id` > '. $data['last_id'] .' ORDER BY id ASC';
      $details = $this->BlankModel->customquery($sql);


        echo json_encode(array('details'=>$details));die;
   }

    function specimen_corn(){

      //$from_date=date('Y-m-d',strtotime("- 1 days"));
      //$to_date=date('Y-m-d');
      $from_date   = date('2020-06-08');
      $to_date     = date('2020-12-15');
             
       $this->db->where('modify_date >=',$from_date.' 00:00:00');
       $this->db->where('modify_date <=',$to_date.' 23:59:59');
       $query=$this->db->get('wp_abd_nail_pathology_report');
       $wp_abd_nail_pathology_report_data=$query->result_array();

       
       $this->db->where('modify_date >=',$from_date.' 00:00:00');
       $this->db->where('modify_date <=',$to_date.' 23:59:59');
       $query2=$this->db->get('wp_abd_nail_pathology_report_archive');
       $wp_abd_nail_pathology_report_archive_data=$query2->result_array();

       
       $query3=$this->db->get('wp_abd_partners_company');
       $wp_abd_partners_company_data=$query3->result_array();
       
       $query4=$this->db->get('wp_abd_partners_specimen');
       $wp_abd_partners_specimen_data=$query4->result_array();
       
       $this->db->where('created_date >=',$from_date.' 00:00:00');
       $this->db->where('created_date <=',$to_date.' 23:59:59');
       $query5=$this->db->get('wp_abd_generated_pcr_reports');
       $wp_abd_generated_pcr_reports_data=$query5->result_array();

       
       $this->db->where('created_date >=',$from_date.' 00:00:00');
       $this->db->where('created_date <=',$to_date.' 23:59:59');
       $query6=$this->db->get('wp_abd_generated_pcr_reports_archive');
       $wp_abd_generated_pcr_reports_archive_data=$query6->result_array();

      $query7=$this->db->where(' `modify_date` >=',$from_date.' 00:00:00');
      $query7=$this->db->where(' `modify_date` <=',$to_date.' 23:59:59');
    
      $query7=$this->db->get('wp_abd_specimen');
      $wp_abd_specimen=$query7->result_array();
      //print_r($wp_abd_specimen);die;


      $query8=$this->db->where(' `modify_date` >=',$from_date.' 00:00:00');
      $query8=$this->db->where(' `modify_date` <=',$to_date.' 23:59:59');
    
      $query8=$this->db->get('wp_abd_specimen_archive');
      $wp_abd_specimen_archive=$query8->result_array();


      $query9=$this->db->where(' `create_date` >=',$from_date.' 00:00:00');
      $query9=$this->db->where(' `create_date` <=',$to_date.' 23:59:59');
    
      $query9=$this->db->get('wp_abd_clinical_info');
      $wp_abd_clinical_info=$query9->result_array();


      $query10=$this->db->where(' `create_date` >=',$from_date.' 00:00:00');
      $query10=$this->db->where(' `create_date` <=',$to_date.' 23:59:59');
    
      $query10=$this->db->get('wp_abd_clinical_info_archive');
      $wp_abd_clinical_info_archive=$query10->result_array();

      $query11=$this->db->where(' `modify_date` >=',$from_date.' 00:00:00');
      $query11=$this->db->where(' `modify_date` <=',$to_date.' 23:59:59');
    
      $query11=$this->db->get('wp_abd_archive_notifiation');
      $wp_abd_archive_notifiation=$query11->result_array();
      

      $query12=$this->db->where(' `create_date` >=',$from_date.' 00:00:00');
      $query12=$this->db->where(' `create_date` <=',$to_date.' 23:59:59');
    
      $query12=$this->db->get('wp_abd_auto_mail');
      $wp_abd_auto_mail=$query12->result_array();

      $query13=$this->db->where(' `create_date` >=',$from_date.' 00:00:00');
      $query13=$this->db->where(' `create_date` <=',$to_date.' 23:59:59');
    
      $query13=$this->db->get('wp_abd_holidays');
      $wp_abd_holidays=$query13->result_array();

      $query14=$this->db->where(' `modify_date` >=',$from_date.' 00:00:00');
      $query14=$this->db->where(' `modify_date` <=',$to_date.' 23:59:59');
    
      $query14=$this->db->get('wp_abd_notification');
      $wp_abd_notification=$query14->result_array();
      

      $query15=$this->db->where(' `create_date` >=',$from_date.' 00:00:00');
      $query15=$this->db->where(' `create_date` <=',$to_date.' 23:59:59');
    
      $query15=$this->db->get('wp_abd_data_entry_notification');
      $wp_abd_data_entry_notification=$query15->result_array();
      //print_r($wp_abd_notification);die;

      $query16=$this->db->where(' `time_stamp` >=',$from_date.' 00:00:00');
      $query16=$this->db->where(' `time_stamp` <=',$to_date.' 23:59:59');
    
      $query16=$this->db->get('wp_abd_user_log');
      $wp_abd_user_log=$query16->result_array();


       $objarray=array(
            "wp_abd_specimen" =>$wp_abd_specimen,
            "wp_abd_specimen_archive" =>$wp_abd_specimen_archive,
            "wp_abd_nail_pathology_report"=>$wp_abd_nail_pathology_report_data,
            "wp_abd_nail_pathology_report_archive"=>$wp_abd_nail_pathology_report_archive_data,
            "wp_abd_partners_company"=>$wp_abd_partners_company_data,
            "wp_abd_partners_specimen"=>$wp_abd_partners_specimen_data,
            "wp_abd_generated_pcr_reports"=>$wp_abd_generated_pcr_reports_data,
            "wp_abd_generated_pcr_reports_archive"=>$wp_abd_generated_pcr_reports_archive_data,
            "wp_abd_clinical_info"=>$wp_abd_clinical_info,
            "wp_abd_clinical_info_archive"=>$wp_abd_clinical_info_archive,
            "wp_abd_archive_notifiation"=>$wp_abd_archive_notifiation,
            "wp_abd_auto_mail"=>$wp_abd_auto_mail,
            "wp_abd_holidays"=>$wp_abd_holidays,
            "wp_abd_notification"=>$wp_abd_notification,
            "wp_abd_data_entry_notification"=>$wp_abd_data_entry_notification,
            "wp_abd_user_log"=>$wp_abd_user_log

            );
           
           //echo json_encode($objarray);die;

           $api_url = $this->config->item('api_url').'AbilityCronUpdate/curl_aps_update';

           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response_specimen = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response_specimen,true);

           print_r($result);die;
           //return $result['status'];

    }

    

}
?>