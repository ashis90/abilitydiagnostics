import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingReportEditComponent } from './pending-report-edit.component';

describe('PendingReportEditComponent', () => {
  let component: PendingReportEditComponent;
  let fixture: ComponentFixture<PendingReportEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingReportEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingReportEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
