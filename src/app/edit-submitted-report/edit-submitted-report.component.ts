import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {PendingSpecimensService } from '../report.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  FormBuilder
} from "@angular/forms";
import {map, startWith} from 'rxjs/operators';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../environments/environment';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'cm-edit-submitted-report',
  templateUrl: './edit-submitted-report.component.html',
  styleUrls: ['./edit-submitted-report.component.css']
})
export class EditSubmittedReportComponent implements OnInit {
  @ViewChild("uploadFile") uploadFileInput: ElementRef;
  public signature_id: any;
  postsArray: any = [] ;
  load_status:boolean = true;
  loading;
  pending_report: FormGroup;
  clinical_history;
  gross_description;
  diagnostic_short_code;
  diagnostic;
  st_val1;
  st_val2;
  st_val3;
  addendum;
  sendData;
  amend_note;
  id;
  user_id:any;
  sortCodeData: any;
  name: any;
  data: any;
  text: any;
  color: any;
  file:any;
  image_url:any;
  amended_report_view_details:any;
  addendum_val:any;
  old_date;
  date_check;
  check_old_data;
  specimen_result;
  physician_info;
  clinician_location;
  internal_short_code_comments:any;
  public url = environment.assetsUrl+'assets/uploads/';
  public link = environment.assetsUrl;
  constructor(
    private http: HttpClient, 
    public itemsService: PendingSpecimensService, 
    public route: Router,
    public activatedRoute : ActivatedRoute,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    ) 
  {
    this.pending_report = this.fb.group({
      user_id: [""],
      clinical_history: ["Dystrophic Toenail"],
      gross_description: [""],
      diagnostic_short_code: ["", Validators.required],
      diagnostic_text: [""],
      diagnostic_color: [""],
      gross_micro_desc_text: [""],
      stains_first: ["", Validators.required],
      stains_addendum_first: [""],
      stains_sec: [""],
      stains_addendum_sec: [""],
      stains_third: [""],
      stains_addendum_third: [""],
      old_addendum_text: [""],
      amend_note: ["***"],
      internal_code: this.fb.array([]),
      addendum: this.fb.array([
        //this.addAddendum('',''),
      ]),
      nail_fungal_img: [""],
      labdoc: [""],
      submit: ["Save", Validators.required]
    });
   this.user_id = localStorage.userid;
  }

  add_addendum_data(): void {
    (<FormArray>this.pending_report.get("addendum")).push(
      this.addAddendum("", "")
    );
  }

  delete_addendum_data(spIndex: number): void {
    (<FormArray>this.pending_report.get("addendum")).removeAt(spIndex);
  }

  add_internal_data(): void {
		(<FormArray>this.pending_report.get('internal_code')).push(this.internalCode("", ""));
	}

	delete_internal_data(spIndex: number): void {
		(<FormArray>this.pending_report.get('internal_code')).removeAt(spIndex);
	}

  addAddendum(code, text): FormGroup {
    return this.fb.group({
      addendum_code: [code],
      addendum_text: [text]
    });
  }

  internalCode(code, text): FormGroup {
		return this.fb.group({
			internal_short_code1: [code],
			internal_short_code_addendum1: [text],
		});
	}
  ngOnInit() 
  {
    var sid=this.activatedRoute.snapshot.paramMap.get('id');
    // if(this.load_status==true)
    // {
    //     let params = {"id": sid};
    //     this.spinner.show();
    //     this.itemsService.get_amended_report(params).subscribe(data => {
    //     this.loading = false;
    //     this.postsArray = data;
    //     this.clinical_history= this.postsArray.amended_report_view_details.clinical_history;
    //     this.gross_description = this.postsArray.amended_report_view_details.gross_description;
    //     this.diagnostic_short_code = this.postsArray.diagnostic_short_code.sc;
    //     this.diagnostic = this.postsArray.diagnostic_short_code.diagnosis;
    //     this.text = this.postsArray.diagnostic_short_code.text;
    //     this.st_val1 = this.postsArray.st1val;
    //     this.st_val2 = this.postsArray.st2val;
    //     this.st_val3 = this.postsArray.st3val;
    //     this.addendum = this.postsArray.addendum_val;
    //     this.pending_report.controls['labdoc'].setValue(this.postsArray.amended_report_view_details.signature_id);
    //     if(this.postsArray.amended_report_view_details.amend_note)
    //     {
    //       this.amend_note =this.postsArray.amended_report_view_details.amend_note;
    //     }
    //     else
    //     {
    //       this.amend_note = "***";
    //     }
    //     this.spinner.hide();
    // });

    // }
    this.getPendingReport(sid);
  }


  getPendingReport(id: any) {
    let params;
    params = { id: id };
    this.spinner.show();
    this.pending_report.controls["user_id"].setValue(localStorage.userid);
    this.itemsService.get_amended_report(params).subscribe(data => {
      this.loading = false;
      this.postsArray = data;
      //console.log(this.postsArray)
      this.amended_report_view_details = this.postsArray.amended_report_view_details;
      this.old_date = new Date(this.amended_report_view_details.create_date);
      this.date_check = new Date("2018-06-12 00:00:00");
      this.check_old_data =
        this.amended_report_view_details.addendum[0].indexOf(" ") >= 0;
      this.specimen_result = this.postsArray.specimen_results;
      this.physician_info = this.postsArray.physician_info;
      this.clinician_location = this.postsArray.clinician_location;
      this.diagnostic_short_code = this.postsArray.diagnostic_short_code;

      this.color = this.isEmpty(this.diagnostic_short_code)
        ? ""
        : this.diagnostic_short_code.color;
      this.addendum_val = this.postsArray.addendum_val;
      this.internal_short_code_comments = this.postsArray.internal_short_code_comments;
      this.pending_report.controls["gross_description"].setValue(
        this.postsArray.gross_description
      );
      this.pending_report.controls["clinical_history"].setValue(
        this.amended_report_view_details.clinical_history
      );
      if (!this.isEmpty(this.amended_report_view_details.amend_note)) {
      this.pending_report.controls["amend_note"].setValue(
        this.amended_report_view_details.amend_note
      );
      }
      if (!this.isEmpty(this.diagnostic_short_code)) {
        this.pending_report.controls["diagnostic_short_code"].setValue(
          this.diagnostic_short_code.sc
        );
        this.pending_report.controls["diagnostic_text"].setValue(
          this.diagnostic_short_code.diagnosis
        );
        this.pending_report.controls["diagnostic_color"].setValue(
          this.diagnostic_short_code.color
        );
        this.pending_report.controls["gross_micro_desc_text"].setValue(
          this.diagnostic_short_code.text
        );
      }
      this.pending_report.controls["stains_first"].setValue(
        this.postsArray.st1val
      );
      this.pending_report.controls["stains_addendum_first"].setValue(
        this.postsArray.st1
      );
      this.pending_report.controls["stains_sec"].setValue(
        this.postsArray.st2val
      );
      this.pending_report.controls["stains_addendum_sec"].setValue(
        this.postsArray.st2
      );
      this.pending_report.controls["stains_third"].setValue(
        this.postsArray.st3val
      );
      this.pending_report.controls["stains_addendum_third"].setValue(
        this.postsArray.st3
      );
      this.pending_report.controls["labdoc"].setValue(
        this.amended_report_view_details.signature_id
      );
      this.signature_id = this.amended_report_view_details.signature_id;
      this.image_url =
        this.url +
        "nail_fungal/" +
        this.amended_report_view_details.images;
      if (this.old_date < this.date_check) {
        if (this.amended_report_view_details.addendum[0].indexOf(" ") >= 0) {
          this.pending_report.controls["old_addendum_text"].setValue(
            this.amended_report_view_details.addendum[0]
          );
        }
        this.addendum_val.forEach((element, indx) => {
          (<FormArray>this.pending_report.get("addendum")).push(
            this.addAddendum(
              this.amended_report_view_details.addendum[indx],
              element
            )
          );
        });
      } else {
        this.amended_report_view_details.addendum.forEach((element, indx) => {
          (<FormArray>this.pending_report.get("addendum")).push(
            this.addAddendum(element, this.addendum_val[indx])
          );
        });
      }
      if(!this.isEmpty(this.amended_report_view_details.internal_short_code)){
        this.amended_report_view_details.internal_short_code.forEach((element, indx) => {
          (<FormArray>this.pending_report.get("internal_code")).push(
            this.internalCode(element, this.internal_short_code_comments[indx])
          );
        });
      }

      if (this.postsArray["status"] === "1") {
        this.spinner.hide();
      }
    });
  }
  onSubmit(event)
  {
   // console.log(event.keyCode);
  //   if (event.keyCode !== 13) {
  //   if(this.pending_report.status == "VALID")
  //   {   
  //       this.loading = true;
  //       this.load_status=false;
  //       this.sendData = {"nail_id": nail_fungal_id,"amend_note":this.pending_report.value.amend_note,"labdoc":this.pending_report.value.labdoc};
  //       this.itemsService.edit_submitted_report(this.sendData).subscribe(data => {
  //         this.loading = false;
  //         this.postsArray = data;
  //        if(this.postsArray.status=='1')
  //        {
  //         this.route.navigate(['/submitted-reports']);
  //        }
          
  //     });
  //   }
  // }

  if (event.keyCode !== 13) {
    if (this.pending_report.status == "VALID") {
      const formData = new FormData();
      let fmData = Object.assign({}, this.pending_report.value);
      var id = this.activatedRoute.snapshot.paramMap.get("id");
      formData.append("id", id);
      formData.append("addendum", JSON.stringify(fmData.addendum));
      formData.append("clinical_history", fmData.clinical_history);
      formData.append("old_addendum_text", fmData.old_addendum_text);
      formData.append("user_id", fmData.user_id);
      formData.append("diagnostic_color", fmData.diagnostic_color);
      formData.append("diagnostic_short_code", fmData.diagnostic_short_code);
      formData.append("diagnostic_text", fmData.diagnostic_text);
      formData.append("gross_description", fmData.gross_description);
      formData.append("gross_micro_desc_text", fmData.gross_micro_desc_text);
      formData.append("stains_addendum_first", fmData.stains_addendum_first);
      formData.append("stains_addendum_sec", fmData.stains_addendum_sec);
      formData.append("stains_addendum_third", fmData.stains_addendum_third);
      formData.append("stains_first", fmData.stains_first);
      formData.append("stains_sec", fmData.stains_sec);
      formData.append("stains_third", fmData.stains_third);
      formData.append("labdoc", fmData.labdoc);
      formData.append("amend_note", fmData.amend_note);
      formData.append('internal_code', JSON.stringify(fmData.internal_code));

      this.loading = true;
      if (this.file) {
        var fileSplit = this.file["name"].split(".");
        var fileExt = "";
        if (fileSplit.length > 1) {
          fileExt = fileSplit[fileSplit.length - 1];
        }
        if (fileExt === "jpeg" || fileExt === "jpg" || fileExt === "png") {
          formData.append("name", this.file["name"]);
          formData.append("file_data", this.file);
        } else {
          this.toastr.info(
            "Sorry, File type is not allowed. Only jpeg, jpg and png!",
            "Error",
            {
              timeOut: 3000
            }
          );
        }
      }

      this.itemsService.edit_submitted_report(formData).subscribe(data => {
        let response = data;
        if (response["status"] === "1") {
          this.uploadFileInput.nativeElement.value = "";
          this.toastr.info("Report edited successfully.", "Success", {
            timeOut: 3000
          });
          this.loading = true;
          this.route.navigate(["/submitted-reports"]);
        }
        if (response["status"] === "0") {
          this.toastr.info("Report failed to edit.", "Success", {
            timeOut: 3000
          });
        }
      });
    } else {
      this.validateAllFormFields(this.pending_report);
    }
  }
  }

  Stains_fst_info(event: any, name: any) {
		let params;
		let marcro_data;
		let indx_focus;
		if (event.keyCode === 13 && event.key === 'Enter') {
			let id = event.target.id;
			let indx = id.replace(/[^0-9]/g, '');
			let value;
			let category = id.replace(/[^a-zA-Z]/g, '');
			indx_focus = parseInt(indx) + 1;
			if (document.getElementById('shortCode_' + indx) || document.getElementById('internal' + indx)) {
				value = event.target.value;
				params = { sort_code: value, name: name };
				this.itemsService.getNailMacroCode(params).subscribe((data) => {
					this.sortCodeData = data;
					marcro_data = this.sortCodeData.marcro_data;
					if (this.sortCodeData['status'] === '1') {
						this.id = marcro_data.id;
						this.data = marcro_data.diagnosis;
						this.text = marcro_data.text;
					}

					if (this.sortCodeData['status'] === '0') {
						this.id = '';
						this.data = '';
						this.text = '';
						//console.log(this.sortCodeData.name);
					}

					if (this.sortCodeData.name === 'diagnostic') {
						this.name = this.sortCodeData.name;
						this.color = marcro_data.color;
						this.pending_report.controls['diagnostic_text'].setValue(this.data);
						this.pending_report.controls['diagnostic_color'].setValue(this.color);
						this.pending_report.controls['gross_micro_desc_text'].setValue(this.text);
					} else if (this.sortCodeData.name === 'stains1') {
						this.pending_report.controls['stains_addendum_first'].setValue(this.text);
					} else if (this.sortCodeData.name === 'stains2') {
						this.pending_report.controls['stains_addendum_sec'].setValue(this.text);
					} else if (this.sortCodeData.name === 'stains3') {
						this.pending_report.controls['stains_addendum_third'].setValue(this.text);
					} else if (this.sortCodeData.name === 'stains4') {
						this.pending_report.controls['addendum']['controls'][indx - 5].controls[
							'addendum_text'
						].setValue(this.text);
					}else if (this.sortCodeData.name === 'internal_1') {
						//this.pending_report.controls['internal_short_code_addendum1'].setValue(this.text);
						this.pending_report.controls['internal_code']['controls'][indx - 1].controls[
							'internal_short_code_addendum1'
						].setValue(this.text);
					} 
				});
				if(category == 'shortCode'){
					if (document.getElementById('shortCode_' + indx_focus)) {
						document.getElementById('shortCode_' + indx_focus).focus();
					}else if (document.getElementById('internalCode_1')) {
						document.getElementById('internalCode_1').focus();
					}	
				}else if(category == 'internalCode'){
					if (document.getElementById('internalCode_' + indx_focus)) {
						document.getElementById('internalCode_' + indx_focus).focus();
					}
				}

			}
		} else {
		}
	}

  validateAllFormFields(formGroup: FormGroup) {
    //{1}
    Object.keys(formGroup.controls).forEach(field => {
      //{2}
      const control = formGroup.get(field); //{3}
      if (control instanceof FormControl) {
        //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        //{5}
        this.validateAllFormFields(control); //{6}
      }
    });
  }

  get pendingArr() {
    return <FormArray>this.pending_report.get("addendum");
  }

  get internalArr() {
		return <FormArray>this.pending_report.get('internal_code');
	}

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }

  onSelectFile(event) {
    this.file = <File>event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = event => {
        // called once readAsDataURL is completed
        this.image_url = (<FileReader>event.target).result;
      };
    }
  }

  onFilesChange(fileList: Array<File>) {
    //console.log(fileList);
  }

  displayFieldCss(field: string) {
    return {
      "has-error": this.isFieldValid(field),
      "has-feedback": this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return (
      !this.pending_report.get(field).valid &&
      (this.pending_report.get(field).touched ||
        this.pending_report.get(field).dirty)
    );
  }

}
