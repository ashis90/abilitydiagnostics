import { Component, OnInit, Renderer2 } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';
import { SpecimenService } from '../../specimen.service';
import { ToastrService } from 'ngx-toastr';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';


export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-pcr-stages',
  templateUrl: './pcr-stages.component.html',
  styleUrls: ['./pcr-stages.component.css']
})
export class PcrStagesComponent implements OnInit {
  datePickerConfig:Partial<BsDatepickerConfig>;
  public data : any;
  specimens_list:any = [];
  specimen_data:any = [];
  physicians_list:any = [];
  searchForm: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  extractionProcessData:FormGroup;
  physicians_list_data:any = [];
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  pcr_text = false;
  xvalue;
  yvalue;
  zvalue;
  color;
  pieces;
  cassettes;
  nail;
  skin;
  sub;
  histo_chk;
  pcr_chk;
  histo_val;
  pcr_val;
  pass:any;
  test_type;
  load_more_data:any=0;
  load_btn = true;
  spe_count:any;
  filterData:any;
  serch_arr:any;
  public stateGroups: StateGroup[] = [{
      ids: [],
      names: []
    }]//test
  public searchData: StateGroup[] = [{
      ids: [],
      names: []
    }]
  modalRef: BsModalRef;

  tatResponse:any;
  tatDetails:any;
  tatMsg:any;
  stageTATCount:any;
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private modalService: BsModalService,
    public analyticsService:AnlyticsService,
    private renderer: Renderer2,
    private spinner: NgxSpinnerService,
    public stageService:SpecimenService,
    public pcrService:PcrService,
    private toastr: ToastrService,
    private router: Router
    )
   {
      this.searchForm = new FormGroup({        
       "barcode" : new FormControl(),
       "physician_name" : new FormControl(), 
      "physician_id" : new FormControl(),  
       "assessioning_num" : new FormControl(), 
       "test_type": new FormControl(),
       "fromdate": new FormControl(),
       "todate": new FormControl()
      });

      this.datePickerConfig = Object.assign({},{
        containerClass:'theme-blue',
        dateInputFormat:'MM/DD/YYYY'
      
      });
    }

    ngOnInit() {   
      this.load_btn = true; 
      this.loading = true;
      this.getdeliverytoextarction();  
      this. getStageTATDetails();  
      this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
        startWith(''),
        map((value) => this._filterGroup(value?value.toLowerCase():''))
      );  
  }

  getStageTATDetails()
  {
    this.spinner.show();
    this.itemsService.pcrSpecimenTatNotification().subscribe( data=> {
    this.spinner.hide();
    this.tatResponse = data;
    if(this.tatResponse.status == '1'){
      this.tatDetails  =  this.tatResponse.pcr_specimen_details;
      this.stageTATCount = this.tatResponse.note_count;
    
    }else{
      this.tatMsg = this.tatResponse.message;
    }
   
    });
    
  }

 

  onSubmit()
  {
      if(this.searchForm.status == "VALID")
      {   
         this.load_btn = false;
          this.spinner.show();
          this.load_status=false;
          this.sendData = {"barcode":this.searchForm.value.barcode, "assessioning_num":this.searchForm.value.assessioning_num, "physician":this.searchForm.value.physician_id,
          "test_type":this.searchForm.value.test_type, "fromdate":this.convertDate(this.searchForm.value.fromdate),"todate":this.convertDate(this.searchForm.value.todate),'user_id':localStorage.userid
        };
          this.itemsService.serachDeliveryToExtraction(this.sendData).subscribe( data=> {
              this.spinner.hide();
              this.specimens_list = data;
              //console.log(this.specimens_list);
              if(this.specimens_list.status === '1')
              {
                this.no_data_status= false;
                this.no_data=false;
              this.data  =  this.specimens_list.specimen_results;
              this.spe_count = this.specimens_list.specimen_count;
              }
              if(this.specimens_list.status === '0')
              {
                this.data ="";
                this.no_data_status= true;
                this.no_data="No Data Found.";
              }
           });
      }
      else
      {

      }
  }

  convertDate(str: Date) {
    if(!str){
      var newDate = '';
    }else{
      
      var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2),
        newDate = [ mnth, day, date.getFullYear() ].join("/");
    }
    return newDate;
  }

  getdeliverytoextarction() {
    let params = {'load':0,'user_id':localStorage.userid};
    this.spinner.show();
    this.itemsService.deliveryToExtraction(params).subscribe(data => {          
    this.specimens_list = data;
      
      if(this.specimens_list.status == '1')
      {
        this.spinner.hide();
        this.no_data_status = false;
        this.data  =  this.specimens_list.specimen_results;
        //console.log(this.data);
        this.serch_arr =  this.specimens_list.specimen_results;
        this.test_type = this.specimens_list.test_types;
        this.physicians_list = this.specimens_list.physicians_data;
        this.spe_count = this.specimens_list.specimen_count;
        this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
        this.physicians_list.map((val) => this.allIds.push ( val.id));
        this.physicians_list.map((data) => this.filterVal.push ( data));
      }
      if(this.specimens_list.status == '0')
      {
        this.spinner.hide();
        this.no_data_status= true;
        this.no_data="No Data Found.";
      }
               
    });
    this.stateGroups = [{
      ids:this.allIds,
      names:this.allNames
    }];
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount : number = this.filterVal.length;
        this.options = [];
        this.ids=[];
        for(let i=0;i<itemCount;i++){
            if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ){
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids:this.ids,
          names:this.options
        }];
        
        return this.searchData;
    }
    
    return this.stateGroups;
}

callSomeFunction(phy_id :any){
  this.searchForm.controls['physician_id'].setValue(phy_id);
}


resetForm(){
  this.load_btn = true; 
  this.searchForm.reset();
  this.getdeliverytoextarction();
}


load_all()
{
  this.no_data_status= false;
  this.spinner.show();
  this.load_status=false;
  this.itemsService.extractionLoadAllRequest().subscribe(data => {
    this.spinner.hide();
    if(this.specimens_list.status == '1')
  {
    this.specimens_list = data;
    this.data  =  this.specimens_list.specimen_results;
    this.spe_count = this.specimens_list.specimen_count;
  }
    if(this.specimens_list.status == '0')
  {
    this.no_data_status= true;
    this.no_data="No Data Found.";
  }        
});
}

displaytrackmodal(specimen_id:any){
  this.spinner.show();
  this.sendData = { "acc_no":specimen_id, 'dataType': 'general'};
  this.analyticsService.specimen_track(this.sendData).subscribe(data => {
    this.postsArray = data;
    this.spinner.hide();
    if(this.postsArray.histo_status=='1')
    {
      this.acc_no=true;
      this.histo_modal =true;
    }
    else
    {
      this.histo_modal =false;
    }
    if(this.postsArray.pcr_status=='1')
    {
      this.acc_no=true;
      this.pcr_modal =true;
    }
    else
    {
      this.pcr_modal =false;
    }
    if(this.postsArray.status=='0')
    {
       this.acc_no=false;
       this.pcr_modal =false;
       this.histo_modal =false;
    }
    this.modalRef = this.modalService.show(TrackModalComponent,  {
      initialState: {
        title: 'track',
        acc_no:  this.acc_no,
        postsArray: this.postsArray,
        histo_modal: this.histo_modal,
        pcr_modal: this.pcr_modal
      }

    });

  });


}

displayProcessStage(specimen_id:any,stageid){
  this.spinner.show();
  this.renderer.removeClass(document.body,'new-track-class');
  this.itemsService.makeSpecimenNext(specimen_id,stageid).subscribe(data => {
    this.spinner.hide();
    this.postsArray = data;
    if(this.postsArray.stage_det.pass_fail)
    {
      this.pass=this.postsArray.stage_det.pass_fail;
    }
    else
    {
      this.pass="";
    }
    this.xvalue= this.postsArray.first;
    this.yvalue= this.postsArray.second;
    this.zvalue= this.postsArray.third;
    this.color= this.postsArray.color;
    this.cassettes= this.postsArray.cassettes;
    if(this.postsArray.stage_det === 'Nil' || this.postsArray.stage_det.pieces == "")
    {
      this.pieces= '1';
    }
    else
    {
      this.pieces= this.postsArray.stage_det.pieces;
    }
    this.nail= this.postsArray.nail;
    this.pcr_chk= this.postsArray.pcr_chk;
    if(this.postsArray.stage_det.pcr)
    {
      this.pcr_val = this.postsArray.stage_det.pcr;
    }
    else
    {
      this.pcr_val = "";
    }
    if(this.postsArray['status'] ==='1'){
      this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
        initialState: {
          title: 'forth',
          specimen_id: specimen_id,
          stageid: stageid,
          pass:this.pass,
          xval:this.xvalue,
          yval:this.yvalue,
          zval:this.zvalue,
          color:this.color,
          cassettes:this.cassettes,
          pieces:this.pieces,
          nail:this.nail,
          pcr_chk:this.pcr_chk,
          pcr_val:this.pcr_val,
          acc_no: this.postsArray.spe_details.assessioning_num
        }
      });
    }
    

  });
  
}

assignStage(specimen_id:any){
  let params;
  params = {id:specimen_id}
    this.loading = true;
      this.itemsService.assignInStage(params).subscribe( data=> {
        if(data['status']==='1'){
          this.loading = false;
          this.toastr.info('Data Successfully updated', 'Success', {
            timeOut: 3000
          });
          this.getdeliverytoextarction();  
        }
    });
}

unassignStage(specimen_id:any){
  let params:any;
  params = {id:specimen_id}
    this.loading = true;
    this.itemsService.unassignInStage(params).subscribe( data=> {
      if(data['status']==='1'){
        this.loading = false;
        this.toastr.info('Data Successfully updated', 'Success', {
          timeOut: 3000
        });
        this.getdeliverytoextarction();  
      }
    });
}

public onTap(id:any) {
  let navigationExtras: NavigationExtras = {
      queryParams: {
          "accessioning_no": id,
      }
  };
  this.router.navigate(["/report-generate"], navigationExtras);
}

load_more(){
  this.load_more_data = this.load_more_data + 10;
  this.spinner.show();
  let params = {'load':this.load_more_data,'user_id':localStorage.userid};
  this.itemsService.loadMorePCR(params).subscribe(data => {
  this.spinner.hide();  
  this.specimens_list = data;
    if(this.specimens_list){
      this.specimens_list.specimen_results.forEach(element => {
        this.data.push(element);
      });
      this.spe_count = this.data?this.data.length:'';
    }
  });    
}

search(term: string) {
  let allData:any;
  allData = this.serch_arr;
  if(!term) {
    this.filterData = this.data;
    this.data = allData;
  } else {
    this.filterData = allData.filter(x => 
       x.assessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
    );
    this.data = this.filterData;
  }
  
}

reviewProcessState(accessioning_no:any){
  this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
    initialState: {
      title: 'reviewreport',     
      accessioning_no:accessioning_no
    }
  });
}

}
