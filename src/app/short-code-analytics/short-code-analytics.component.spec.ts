import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortCodeAnalyticsComponent } from './short-code-analytics.component';

describe('ShortCodeAnalyticsComponent', () => {
  let component: ShortCodeAnalyticsComponent;
  let fixture: ComponentFixture<ShortCodeAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortCodeAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortCodeAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
