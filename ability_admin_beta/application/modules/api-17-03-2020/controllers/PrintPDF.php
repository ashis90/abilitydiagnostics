<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
    header('Access-Control-Allow-Origin: https://www.abilitydiagnostics.com');
class PrintPDF extends MY_Controller {

    function __construct() {
        parent::__construct();
	  
    }
  
    function print_histo_submitted_report()
    {
	  
	   $nail_report_name = $_REQUEST['histo_nail_report'];
	   $this->load->view('histo_pdf_print', array('nail_report_name' => $nail_report_name));
	  
    } 
    
    function print_pcr_submitted_report()
    {
	  
	   $pcr_report_name = $_REQUEST['pcr_report'];
	   $this->load->view('pcr_pdf_print', array('pcr_report_name' => $pcr_report_name));
	  
    }
 
 }
