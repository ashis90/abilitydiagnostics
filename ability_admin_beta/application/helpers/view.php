<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
$basepath = base_url("assets/");
?>
   <!-- START CONTENT -->
        <section id="content">
          <!--breadcrumbs start-->
          <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Candidates: Add Candidate</h5>
                  <ol class="breadcrumbs">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="#">Candidates</a></li>
                    <li class="active">Add Candidate</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!--breadcrumbs end-->
          <!--start container-->
          <div class="container">
            <div class="section">
              <!--DataTables example-->
              <div id="table-datatables">
                 <form method="post" enctype="multipart/form-data">
                 <div class="row">
                  <div class="col s6">
                    <h4 class="header">Basic Information</h4>
                    <table class="companyTable">
                      <tr>
                        <td>First Name: </td>
                        <td>
                          <div class="input-field col s12">
                         
                            <?php echo $job->name; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Middle Name: </td>
                        <td>
                          <div class="input-field col s12">
                        
                              <?php echo $job->middle_name; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Last Name: </td>
                        <td>
                          <div class="input-field col s12">
                            
                              <?php echo $job->last_name; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>E-Mail: </td>
                        <td>
                          <div class="input-field col s12">
                             
                              <?php echo $job->email; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2nd E-Mail: </td>
                        <td>
                          <div class="input-field col s12">
                               <?php echo $job->sec_email; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Home Phone: </td>
                        <td>
                          <div class="input-field col s12">
                                <?php echo $job->phone; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Cell Phone: </td>
                        <td>
                          <div class="input-field col s12">
                                   <?php echo $job->cell_phone; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Work Phone: </td>
                        <td>
                          <div class="input-field col s12">
                                <?php echo $job->work_phone; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Address: </td>
                        <td>
                          <div class="input-field col s12">
                           
                                  <?php echo $job->address; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>City: </td>
                        <td>
                          <div class="input-field col s12">
                            
                                  <?php echo $job->city; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>State: </td>
                        <td>
                          <div class="input-field col s12">
                            
                                  <?php echo $job->state; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Postal Code: </td>
                        <td>
                          <div class="input-field col s12">
                      
                             <?php echo $job->pin; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Best Time to Call: </td>
                        <td>
                          <div class="input-field col s12">
                               <?php echo $job->call_time; ?>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>

                  <div class="col s6">
                    <h4 class="header">Or Import Resume</h4>
                    <table class="companyTable">
                      <tr>
                        <td>Import Resume: </td>
                        <td>
                          <div class="input-field col s12">
                                <a href="<?php echo base_url().$job->resume; ?>" download>Download Resume</a>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <div class="input-field col s12">
                        
                             <?php echo $job->resume_text; ?>
                            
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>

                  <div class="col s9">
                    <h4 class="header">Other</h4>
                    <table class="companyTable">
                      <tr>
                        <td>AHPRA MEDICARE Registration number3:</td>
                        <td>
                          <div class="input-field col s12">
                             <?php echo $job->reg_num3; ?>
                            
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>AHPRA MEDICARE Registration number2:</td>
                        <td>
                          <div class="input-field col s12">
                               <?php echo $job->reg_num2; ?>
                           
                            
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>AHPRA MEDICARE Registration number:</td>
                        <td>
                          <div class="input-field col s12">
                                <?php echo $job->reg_num1; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Availability Date: </td>
                        <td>
                          <div class="input-field col s12">
                            <select name="avail_date" id="avail_date">
                              <option value="" disabled selected>Choose your option</option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                            </select>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Can Relocate: </td>
                        <td>
                          <p>
<input type="checkbox" class="filled-in" id="relocate" name="is_relocate" <?php echo ($job->is_relocate == "1" ? "checked=checked" : "" );?> value="1"/>
                            <label for="relocate"></label>
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td>Date Available:</td>
                        <td>
                          <div class="input-field col s12">
                            <select name="date_avail" id="date_avail">
                              <option value="" disabled selected>Choose your option</option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                            </select>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Current Employer:</td>
                        <td>
                          <div class="input-field col s12">
                               <?php echo $job->current_emp; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Current Pay:</td>
                        <td>
                          <div class="input-field col s12">
                             <?php echo $job->current_pay; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Desired Pay:</td>
                        <td>
                          <div class="input-field col s12">
                              <?php echo $job->desired_pay; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Source:</td>
                        <td>
                          <div class="input-field col s12">
                                <?php echo $job->source; ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Key Skills:</td>
                        <td>
                          <div class="input-field col s12">
                            <select name="key_skill" id="key_skill">
                              <option value="" disabled selected>Choose your option</option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                            </select>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Misc. Notes: </td>
                        <td colspan="3">
                             <?php echo $job->notes; ?>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div class="col s9">
                 
                    <a class="btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                      <i class="material-icons left">arrow_back</i> Back to Candidates
                    </a>
                  </div>
                </div>
                </form>
              </div>
          </div>
          <!--end container-->
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
       
      </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
