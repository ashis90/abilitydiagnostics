import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcrStagesComponent } from './pcr-stages.component';

describe('PcrStagesComponent', () => {
  let component: PcrStagesComponent;
  let fixture: ComponentFixture<PcrStagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcrStagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcrStagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
