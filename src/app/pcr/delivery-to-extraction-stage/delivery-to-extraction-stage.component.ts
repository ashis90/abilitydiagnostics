import { Component, OnInit, Renderer2 } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import {PcrService } from '../../pcr.service';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';

@Component({
  selector: 'cm-delivery-to-extraction-stage',
  templateUrl: './delivery-to-extraction-stage.component.html',
  styleUrls: ['./delivery-to-extraction-stage.component.css']
})
export class DeliveryToExtractionStageComponent implements OnInit {

  public data : any;
  specimens_list:any = [];
  specimen_data:any = [];
  physicians_list:any = [];
  searchForm: FormGroup;
  extractionProcessData: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  physicians_list_data:any = [];
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  pcr_text = false;
  xvalue;
  yvalue;
  zvalue;
  color;
  pieces;
  cassettes;
  nail;
  skin;
  sub;
  histo_chk;
  pcr_chk;
  histo_val;
  pcr_val;
  pass:any;
  test_type;
  specimen_id;
  stageid;
  xval;
  yval;
  zval;
  show = false;
  filter :boolean = false;
  modalRef: BsModalRef;
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private renderer: Renderer2,
    public activatedRoute : ActivatedRoute
  ) 
  { 
    this.searchForm = new FormGroup({        
      "assessioning_num" : new FormControl('',Validators.required), 
     });

     this.extractionProcessData = new FormGroup({
      "pass" : new FormControl("",[Validators.required]),
      "xvalue" : new FormControl("",[Validators.required]),
      "yvalue" : new FormControl("",[Validators.required]),
      "zvalue" : new FormControl("",[Validators.required]),
      "color" : new FormControl("",[Validators.required]),
      "pieces" : new FormControl("",[Validators.required]),
      "tissue" : new FormControl(),
      "pcr" : new FormControl(),
      "pcr_val" : new FormControl()
     });
  }

  ngOnInit() {

    if(this.load_status==true)
    {
        var specimen_id=this.activatedRoute.snapshot.paramMap.get('id');
        var stageid=this.activatedRoute.snapshot.paramMap.get('stageid');
          this.spinner.show();
          this.load_status=false;
          // this.itemsService.serachsingleStage(this.sendData).subscribe( data=> {
            this.itemsService.makeSpecimenNext(specimen_id,stageid).subscribe(data => {
              this.spinner.hide();
              this.postsArray = data;
              console.log(this.postsArray);
              if(this.postsArray.status === '1')
              {     
                    this.show = true;
                    this.postsArray = data;
                    if(this.postsArray.stage_det.pass_fail)
                    {
                      this.pass=this.postsArray.stage_det.pass_fail;
                    }
                    else
                    {
                      this.pass="";
                    }
                    this.xvalue= this.postsArray.first;
                    this.yvalue= this.postsArray.second;
                    this.zvalue= this.postsArray.third;
                    this.color= this.postsArray.color;
                    this.cassettes= this.postsArray.cassettes;
                    if(this.postsArray.stage_det === 'Nil' || this.postsArray.stage_det.pieces == "")
                    {
                      this.pieces= '1';
                    }
                    else
                    {
                      this.pieces= this.postsArray.stage_det.pieces;
                    }
                    this.nail= this.postsArray.nail;
                    this.pcr_chk= this.postsArray.pcr_chk;
                    if(this.postsArray.stage_det.pcr)
                    {
                      this.pcr_val = this.postsArray.stage_det.pcr;
                    }
                    else
                    {
                      this.pcr_val = "";
                    }
                    this.specimen_id=  this.postsArray.spe_details.id;
                    this.stageid= 1;
                    this.xval=this.xvalue; 
                    this.yval= this.yvalue;
                    this.zval= this.zvalue;
                    this.acc_no= this.postsArray.spe_details.assessioning_num;
                    
              }
              if(this.postsArray.status === '0')
              {
                  this.show = false;
                  this.data ="";
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
              }
           });
          }
  }

  show_pcr(event)
   {
    this.filter = !this.filter;
   }

  
  extractionDisplayFieldCss(field: string) {
    return {
      'has-error': this.extractionIsFieldValid(field),
      'has-feedback': this.extractionIsFieldValid(field)
    };
  }

  extractionIsFieldValid(field: string) {
    return !this.extractionProcessData.get(field).valid && (this.extractionProcessData.get(field).touched || this.extractionProcessData.get(field).dirty);
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  extractionDataSubmit(specimen_id,stageid){
    //console.log(this.extractionProcessData);
    if(this.extractionProcessData.status == "VALID")
    {   
        this.spinner.show();
        let serializedForm = this.extractionProcessData.value;
        let user_id = localStorage.userid;
        let formObj = {...serializedForm,user_id,specimen_id}; // {name: '', description: ''}
    
        this.itemsService.submitExtractionProcessStage(formObj).subscribe(data => { 
          this.spinner.hide();        
          this.route.navigateByUrl('/pcr-stages');
        });
    }
    else

    {
      this.validateAllFormFields(this.extractionProcessData);
    }
}

validateAllFormFields(formGroup: FormGroup) {         

  Object.keys(formGroup.controls).forEach(field => {  

    const control = formGroup.get(field);             

    if (control instanceof FormControl) {             

      control.markAsTouched({ onlySelf: true });

    } else if (control instanceof FormGroup) {        

      this.validateAllFormFields(control);            

    }

  });

}


  resetForm(){
    this.searchForm.reset();
  }
  

}
