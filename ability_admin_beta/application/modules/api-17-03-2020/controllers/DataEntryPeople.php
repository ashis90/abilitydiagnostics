<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class DataEntryPeople extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index(){}

   	function add_data_entry_people(){

    	$data=json_decode(file_get_contents('php://input'), true);
      if(!empty($data)){

        $this->db->where('user_email',$data['email']);
        $email_exit = $this->db->get('wp_abd_users');

         $this->db->where('user_login',$data['userName']);
        $usr_name_exit = $this->db->get('wp_abd_users');

        if($email_exit->num_rows() > 0){
          die(json_encode(array('status'=>'2')));
        }else if($usr_name_exit->num_rows() > 0){
          die(json_encode(array('status'=>'3')));
        }else{

      $userdata = array(
            'user_login'  =>  $data['userName'],
            'user_email'  =>  $data['email'],
            'user_url'    =>  $data['website'],
            'display_name'=>  $data['firstName'].' '.$data['lastName'],
            'user_registered'=>date('Y-m-d H:i:s'),
            'user_nicename'=>strtolower($data['userName']),
            'user_pass'   =>  md5(SECURITY_SALT.$data['password'])           
            );
       $user_sql_insert = $this->BlankModel->addTableData('wp_abd_users',$userdata);
   		if(!empty($user_sql_insert)){
        $role_type_array = array('data_entry_oparator' => '1');
        $role = serialize( $role_type_array);
     
        add_user_meta($user_sql_insert,'added_by', $data['user_id'] );
        add_user_meta($user_sql_insert,'first_name', $data['firstName'] );
        add_user_meta($user_sql_insert,'last_name', $data['lastName'] );
        add_user_meta($user_sql_insert,'_mobile', $data['phone'] );
        add_user_meta($user_sql_insert,'wp_abd_capabilities', $role );
        add_user_meta($user_sql_insert,'_address', $data['address'] );
        if($data['specimenControlAccess']){
          add_user_meta($user_sql_insert,'specimen_control_access',$data['specimenControlAccess']);
        }
        if($data['specimenControlAccessQc']){
          add_user_meta($user_sql_insert,'specimen_control_access_qc',$data['specimenControlAccessQc']);
        }
        add_user_meta($user_sql_insert, 'access_given_by', $data['user_id']);

   	    die(json_encode(array('status'=>'1')));
	   }else{
        die(json_encode(array('status'=>'0')));
     }   
   }
	   		
    }
  }


  function get_data_entry_people(){
    $responseData = array(array());
      $sql = ' 
  SELECT  * 
  FROM    wp_abd_users INNER JOIN wp_abd_usermeta
  ON      wp_abd_users.ID              =      wp_abd_usermeta.user_id 
  WHERE   wp_abd_usermeta.meta_key     =       \'wp_abd_capabilities\' 
  AND     ( wp_abd_usermeta.meta_value LIKE    \'%"data_entry_oparator"%\') ORDER BY display_name';

    $res = $this->BlankModel->customquery($sql); 

    if(!empty($res)){
      $data_entry_oparator_count = count($res);  

      for($i=0;$i<$data_entry_oparator_count;$i++){

        $first_name = get_user_meta( $res[$i]['ID'], 'first_name');       
        $last_name  = get_user_meta( $res[$i]['ID'], 'last_name');
        $mobile  = get_user_meta( $res[$i]['ID'], '_mobile');
        $address  = get_user_meta( $res[$i]['ID'], '_address');
        $access_control = get_user_meta($res[$i]['ID'], 'specimen_control_access');
        $access_control_qc = get_user_meta($res[$i]['ID'], 'specimen_control_access_qc',true);

        $responseData[$i]['fname'] = $first_name['meta_value'];
        $responseData[$i]['lname'] = $last_name['meta_value'];
        $responseData[$i]['mob']  = $mobile['meta_value'];
        $responseData[$i]['address'] = $address['meta_value'];
        $responseData[$i]['acc_cntrl'] = $access_control['meta_value'];
        $responseData[$i]['acc_cntrl_qc'] = $access_control_qc['meta_value'];

        $responseData[$i]['display_name'] =$res[$i]['display_name'];
        $responseData[$i]['user_email'] =$res[$i]['user_email'];
        $responseData[$i]['user_login'] =$res[$i]['user_login'];
        $responseData[$i]['id'] =$res[$i]['ID'];

      }

      die(json_encode(array('status'=> '1','data_entry_oparator'=>$responseData,'list_count'=>$data_entry_oparator_count)));
    }else{
      die(json_encode(array('status'=> '0')));
    }
    

    
  }

  function get_data_entry_people_by_id(){

    $data=json_decode(file_get_contents('php://input'), true);
      if(!empty($data)){
    $responseData = array(array());
      $sql = ' 
  SELECT  * 
  FROM    wp_abd_users INNER JOIN wp_abd_usermeta
  ON      wp_abd_users.ID              =      wp_abd_usermeta.user_id 
  WHERE   wp_abd_usermeta.meta_key     =       \'wp_abd_capabilities\' 
  AND     ( wp_abd_usermeta.meta_value LIKE    \'%"data_entry_oparator"%\') 
  AND      wp_abd_users.ID  = "'.$data["user_id"].'" ORDER BY display_name';

    $res = $this->BlankModel->customquery($sql); 
    if(!empty($res)){
      $data_entry_oparator_count = count($res);  

      for($i=0;$i<$data_entry_oparator_count;$i++){

        $first_name = get_user_meta( $res[$i]['ID'], 'first_name');       
        $last_name  = get_user_meta( $res[$i]['ID'], 'last_name');
        $mobile  = get_user_meta( $res[$i]['ID'], '_mobile');
        $address  = get_user_meta( $res[$i]['ID'], '_address');
        $access_control = get_user_meta($res[$i]['ID'], 'specimen_control_access');
        $access_control_qc = get_user_meta($res[$i]['ID'], 'specimen_control_access_qc',true);

        $responseData[$i]['fname'] = $first_name['meta_value'];
        $responseData[$i]['lname'] = $last_name['meta_value'];
        $responseData[$i]['mob']  = $mobile['meta_value'];
        $responseData[$i]['address'] = $address['meta_value'];
        $responseData[$i]['acc_cntrl'] = $access_control['meta_value'];
        $responseData[$i]['acc_cntrl_qc'] = $access_control_qc['meta_value'];

        $responseData[$i]['display_name'] =$res[$i]['display_name'];
        $responseData[$i]['user_email'] =$res[$i]['user_email'];
        $responseData[$i]['user_login'] =$res[$i]['user_login'];
        $responseData[$i]['user_url'] =$res[$i]['user_url'];
        $responseData[$i]['id'] =$res[$i]['ID'];

      }

      die(json_encode(array('status'=> '1','data_entry_oparator'=>$responseData,'list_count'=>$data_entry_oparator_count)));
    }else{
      die(json_encode(array('status'=> '0')));
    }
    

    
  }
}

function edit_data_entry_people(){

      $data=json_decode(file_get_contents('php://input'), true);
      $userdata = array();
      $up_status= array();
      if(!empty($data)){
        $request_user_id = $data["request_id"];

        $userdata['user_url'] =  $data['website'];
        $userdata['display_name'] =  $data['firstName'].' '.$data['lastName'];
        $userdata['user_nicename']=strtolower($data['userName']);

        if(!empty($data['password'])){
          $userdata['user_pass']   =  md5(SECURITY_SALT.$data['password']);      
        }
             
        $this->db->where('ID',$request_user_id);
        $this->db->update('wp_abd_users',$userdata);

        update_user_meta($request_user_id, 'first_name',$data['firstName'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        update_user_meta($request_user_id, 'last_name' ,$data['lastName'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        update_user_meta($request_user_id, 'user_url'  ,$data['website'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');

        $mob_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='_mobile' AND `user_id`= '".$request_user_id."'";
        $mob_sql_res = $this->BlankModel->customquery($mob_sql);
        if(empty($mob_sql_res))
        { 
          add_user_meta($request_user_id,'_mobile', $data['phone'] );
        }
        else
        {
          update_user_meta($request_user_id, '_mobile'   ,$data['phone'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $add_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='_address' AND `user_id`= '".$request_user_id."'";
        $add_sql_res = $this->BlankModel->customquery($add_sql);
        if(empty($add_sql_res))
        {
          add_user_meta($request_user_id,'_address', $data['address'] );
        }
        else
        {
          update_user_meta($request_user_id, '_address'  ,$data['address'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $acc1_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='specimen_control_access' AND `user_id`= '".$request_user_id."'";
        $acc1_sql_res = $this->BlankModel->customquery($acc1_sql);
        if(empty($acc1_sql_res))
        {
          add_user_meta($request_user_id,'specimen_control_access', 'add');
        }
        else
        {
          update_user_meta($request_user_id, 'specimen_control_access', $data['specimenControlAccess'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $acc2_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='specimen_control_access_qc' AND `user_id`= '".$request_user_id."'";
        $acc2_sql_res = $this->BlankModel->customquery($acc2_sql);
        if(empty($acc2_sql_res))
        {
          add_user_meta($request_user_id,'specimen_control_access_qc', 'check');
        }
        else
        {
          update_user_meta($request_user_id, 'specimen_control_access_qc', $data['specimenControlAccessQc'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }
      
        update_user_meta($request_user_id, 'access_given_by', $data['user_id'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        if($this->db->affected_rows() > 0 || in_array('yes', $up_status)){
          die(json_encode(array('status'=>'1')));
        }else{
          die(json_encode(array('status'=>'0')));
        }
        
    }
  }


  function delete_data_entry_people(){
     $data=json_decode(file_get_contents('php://input'), true);

      if(!empty($data)){
        $id = $data['id'];

       $sql = "DELETE FROM wp_abd_users WHERE `id`= $id";
        $this->db->query($sql);

        $newsql = "DELETE FROM wp_abd_usermeta WHERE `user_id`= $id";
        $this->db->query($newsql);

        if($this->db->affected_rows() > 0){
          die(json_encode(array('status'=>'1')));
        }else{
          die(json_encode(array('status'=>'0')));
        }
      }
  }
}

?>