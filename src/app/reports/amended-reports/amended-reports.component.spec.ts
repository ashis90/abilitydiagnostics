import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmendedReportsComponent } from './amended-reports.component';

describe('AmendedReportsComponent', () => {
  let component: AmendedReportsComponent;
  let fixture: ComponentFixture<AmendedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmendedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
