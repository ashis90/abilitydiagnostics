import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-histo-pcr-analytics',
  templateUrl: './histo-pcr-analytics.component.html',
  styleUrls: ['./histo-pcr-analytics.component.css']
})
export class HistoPcrAnalyticsComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  data;
  sendData;
  load_more_data: any = 0;
  spe_count: any;
  no_data_status = false;
  no_data: any;
  filterData: any;
  isSearch: boolean = false;
  _archiveNote:any;
  constructor(private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({
      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)
    });
    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });

  }


  ngOnInit() {

    this.spinner.show();
    this.isSearch = false;
    this.itemsService.get_histo_pcr_data().subscribe(data => {
      this.spinner.hide();
      this.postsArray = data;
      this.no_data_status = false;
      this.data = this.postsArray.details;
      if (this.data.status === '0') {
        this.no_data_status = true;
        this.no_data = "No Data Found.";
      }
      this.spe_count = this.data ? this.data.length : '';
    });

  }


  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  load_all() {
    this.spinner.show();
    this.load_status = false;
    this.itemsService.loadAllRequest().subscribe(data => {
      this.spinner.hide();
      this.postsArray = data;
      this.data = this.postsArray.details;
      this.spe_count = this.data ? this.data.length : '';
    });
  }

  load_more() {
    this.load_more_data = this.load_more_data + 10;
    this.spinner.show();
    let params = { 'load': this.load_more_data };
    this.itemsService.loadMoreRequest(params).subscribe(data => {
      this.spinner.hide();
      this.postsArray = data;
      if (this.postsArray) {
        this.postsArray.details.forEach(element => {
          this.data.push(element);
        });
        this.spe_count = this.data ? this.data.length : '';
      }
    });
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.isSearch = true;
      this.spinner.show();
      this.load_status = false;
      this.sendData = { "acc_no": this.searchForm.value.acc_no, "from_date": this.convertDate(this.searchForm.value.from_date), "to_date": this.convertDate(this.searchForm.value.to_date), 'dataType': this.searchForm.value.dataType };
      this.itemsService.search_histo_pcr(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
        this.spe_count = this.postsArray.count ? this.postsArray.count : '';
      });
    }
    else {
      this.validateAllFormFields(this.searchForm);
    }
  }
  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  search(term: string) {
    let allData: any;
    allData = this.postsArray.details;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.assessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }

  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
