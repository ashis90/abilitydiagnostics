<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {}
    function login()
    {
		date_default_timezone_set('MST7MDT');
		$details = array();
		$data = json_decode(file_get_contents('php://input'), true);
	    $email = $data['username'];
	    $pass = md5(SECURITY_SALT.$data['password']);
		$token = md5(openssl_random_pseudo_bytes(32));
		$new_token = hash('sha256', $token);
		
		$up_data = array('user_activation_key'=>$new_token);
		$conditions1 = " ( `user_login` = '".$email."')";
		$usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);

    	$conditions = " ( `user_pass` = '".$pass."' AND `user_activation_key` = '".$new_token."' AND `user_login` ='".$email."' AND `user_status` = '0')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);

		// $last_login = array('when_last_login'=> date('Y-m-d h:i:s'));
		// $conditions2 = " ( `user_id` = '".$memDetails['ID']."')";
		// $userstimeedit = $this->BlankModel->editTableData('usermeta', $last_login, $conditions2);

		if(!empty($memDetails['ID']))
        {
            $time = date('Y-m-d H:i:s');
            $current_login_time = strtotime($time);
            $usermetaedit = update_user_meta ($memDetails['ID'], 'when_last_login' , $current_login_time);
        }
			  
		if($memDetails['user_pass'] == $pass)
		{
			$det_row['token'] =  $new_token;
			$det_row['id'] = $memDetails['ID'];
            $det_row['user_login'] = $memDetails['user_login'];
            $det_row['user_pass'] = $memDetails['user_pass'];
            $det_row['user_nicename'] = $memDetails['user_nicename'];
            $det_row['user_email'] = $memDetails['user_email'];
            $det_row['user_url'] = $memDetails['user_url']; 
            $det_row['user_registered'] = $memDetails['user_registered']; 
            $det_row['user_status'] = $memDetails['user_status']; 
			$det_row['display_name'] = $memDetails['display_name'];
			 $uRole = get_user_role($memDetails['ID']); 
			 $det_row['user_role'] = $uRole;
			$specimen_stages = get_user_meta($memDetails['ID'], 'specimen_stages',true);
			$det_row['specimen_stages'] = explode(',',$specimen_stages['meta_value']);
			 if($uRole == 'data_entry_oparator'){
				$access_control = get_user_meta($memDetails['ID'], 'specimen_control_access');
				$access_control_qc = get_user_meta($memDetails['ID'], 'specimen_control_access_qc',true);
				$det_row['acc_cntrl'] = $access_control['meta_value'];
				$det_row['acc_cntrl_qc'] = $access_control_qc['meta_value'];
			 }
			$details = $det_row ;
			
			$uid = $memDetails['ID'];
			$ulogin = $memDetails['user_login'];
			$uemail = $memDetails['user_email'];
			$this->db->insert('wp_abd_user_log',array('user_id'=>$uid,'user_login'=>$ulogin,'user_email'=>$uemail,'site_view'=>'ability', 'time_stamp' => date("Y-m-d h:i:s")));
			
			die( json_encode(array("status" => "1","details" => $details)));			

		}
		else
		{
			die( json_encode(array("status" => "0")));
		}
	}
	function user_exists()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$conditions = " ( `user_activation_key` = '".$data."' AND `user_status` = '0' AND `user_activation_key`!= '')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		if($memDetails)
		{
			echo json_encode(array("status" => "1"));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}

	function get_user_details()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$conditions = " ( `user_activation_key` = '".$data."' AND `user_status` = '0' AND `user_activation_key`!= '')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);

		if($memDetails)
		{
			echo json_encode($memDetails);
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}
	function logout_user()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$up_data = array('user_activation_key'=>'');
		$conditions1 = " ( `user_activation_key` = '".$data."')";
		$usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);
		if($usersedit)
		{
			echo json_encode(array("status" => "1"));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}
		function user_details()
	{	
		$details= array();
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$id = intval($data['userid']);
		$user_id = "'".$id."'";
		$conditions = " ( `ID` = '".$id."')";		
	
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		
		$fname           = get_user_meta_value($user_id, 'first_name');				
		$lname           = get_user_meta_value($user_id, 'last_name');
		$mobile          = get_user_meta_value($user_id, '_mobile');
		$address         = get_user_meta_value($user_id, '_address');
		$clinic          = get_user_meta_value($user_id, 'clinic_addrs');
		$fax             = get_user_meta_value($user_id, 'fax');
		$cell_phone      = get_user_meta_value($user_id, 'cell_phone');
		$manager_contact_name  = get_user_meta_value($user_id, 'manager_contact_name');
		$manager_cell_number   = get_user_meta_value( $user_id, 'manager_cell_number');
		$office_number         = get_user_meta_value( $user_id, 'office_number');
		$npi                   = get_user_meta_value( $user_id, 'npi');
		$access_control        = get_user_meta_value($user_id, 'specimen_control_access');
		$access_control_qc     = get_user_meta_value($user_id, 'specimen_control_access_qc',true);
		$role                  = get_user_role($data['userid']);  

		$specimen_information = array('fname' => $fname, 'lname' => $lname, 'acc_cntrl' => $access_control, 'acc_cntrl_qc' => $access_control_qc, 'email' => $memDetails['user_email'], 'user_name' => $memDetails['user_login'], 'url' => $memDetails['user_url'], 'mob' => $mobile, 'add' => $address, 'clinic' => $clinic, 'fax' => $fax, 'cell' => $cell_phone, 'manager' => $manager_contact_name, 'manager_cell' => $manager_cell_number, 'offce_num' => $office_number, 'npi_api' => $npi, 'role' => $role);
	
	
		if($specimen_information)
		{
			echo json_encode(array("status" => "1","details" =>$specimen_information));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}else{
		echo json_encode(array("status" => "0"));
	}

	}
	function update_user_details()
	{
		$details= array();
		$data = json_decode(file_get_contents('php://input'), true);
		$id = intval($data['userid']);
		$user_id = "'".$id."'";


		update_user_meta($user_id, 'first_name', $data['first_name']);
		update_user_meta($user_id, 'last_name', $data['last_name']);
		update_user_meta($user_id, '_mobile', $data['mobile']);
		update_user_meta($user_id, '_address',$data['address']);

		if(($data['user_pass'] === $data['con_pass']) && !empty($data['user_pass']) && !empty($data['con_pass']))
		{
			
		$pass = md5(SECURITY_SALT.$data['user_pass']);
		$con_pass = md5(SECURITY_SALT.$data['con_pass']);

		$up_data = array('user_pass'=>$pass,'user_email'=>$data['email'],'user_url'=>$data['user_url']);
		$conditions1 = " ( `ID` = '".$id."')";	
		$usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);
		}
		
		$upd_data = array('user_email'=>$data['email'],'user_url'=>$data['user_url']);
		$conditions2 = " ( `ID` = '".$id."')";	
		$usersedit = $this->BlankModel->editTableData('users', $upd_data, $conditions2);
		
		$conditions = " ( `ID` = '".$id."')";		
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		
		$first_name = get_user_meta( $user_id, 'first_name');				
		$last_name  = get_user_meta( $user_id, 'last_name');
		$mobile  = get_user_meta( $user_id, '_mobile');
		$address  = get_user_meta( $user_id, '_address');
		$clinic_addrs  = get_user_meta( $user_id, 'clinic_addrs');
		$fax  = get_user_meta( $user_id, 'fax');
		$cell_phone  = get_user_meta($user_id, 'cell_phone');
		$manager_contact_name  = get_user_meta($user_id, 'manager_contact_name');
		$manager_cell_number  = get_user_meta( $user_id, 'manager_cell_number');
		$office_number  = get_user_meta( $user_id, 'office_number');
		$npi  = get_user_meta( $user_id, 'npi');
		$access_control = get_user_meta($user_id, 'specimen_control_access');
		$access_control_qc = get_user_meta($user_id, 'specimen_control_access_qc',true);
		$role = get_user_role($data['userid']);  

		$fname = $first_name['meta_value'];
		$lname = $last_name['meta_value'];
		$mob  = $mobile['meta_value'];
		$add = $address['meta_value'];
		$clinic = $clinic_addrs['meta_value'];
		$fax = $fax['meta_value'];
		$cell = $cell_phone['meta_value'];
		$manager = $manager_contact_name['meta_value'];
		$manager_cell = $manager_cell_number['meta_value'];
		$offce_num = $office_number['meta_value'];
		$npi_api = $npi['meta_value'];
		$clinic_add = $clinic_addrs['meta_value'];
		$acc_cntrl = $access_control['meta_value'];
		$acc_cntrl_qc = $access_control_qc['meta_value'];

		$specimen_information = array('fname' => $fname,'lname' => $lname,'acc_cntrl' => $acc_cntrl,'acc_cntrl_qc' => $acc_cntrl_qc,'email' => $memDetails['user_email'],'url' => $memDetails['user_url'], 'mob' => $mob,'add' => $add,'clinic' => $clinic,'fax' => $fax,'cell' => $cell,'manager' => $manager, 'manager_cell' => $manager_cell, 'offce_num' => $offce_num,'npi_api' => $npi_api,'role' => $role);
	
	
		if($specimen_information)
		{
			echo json_encode(array("status" => "1","details" =>$specimen_information));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
			
	}
	

}

?>