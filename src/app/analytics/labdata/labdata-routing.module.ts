import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LabdataComponent } from './labdata.component';

const routes: Routes = [
  { path: '', component: LabdataComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class LabdataRoutingModule {
  static components = [ LabdataComponent ];
}
