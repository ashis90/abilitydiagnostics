import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDataEntryPeopleComponent } from './add-data-entry-people.component';

describe('AddDataEntryPeopleComponent', () => {
  let component: AddDataEntryPeopleComponent;
  let fixture: ComponentFixture<AddDataEntryPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDataEntryPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDataEntryPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
