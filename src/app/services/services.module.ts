import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarModule } from '../core/navbar/navbar.module';
import { ServicesRoutingModule } from './services-routing.module';

@NgModule({
  imports:      [ CommonModule,ServicesRoutingModule,CommonModule,NavbarModule],
  declarations: [ ServicesRoutingModule.components ]
})
export class ServicesModule { }
