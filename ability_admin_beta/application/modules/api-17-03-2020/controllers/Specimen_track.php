<?php

if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Specimen_track extends MY_Controller 
{
    function __construct() 
    {
        parent::__construct();
	date_default_timezone_set('MST7MDT');
    }

    function index()
    {}
     
    function track_specimen()
    {   
        $class1="";
        $class2="";
        $class3="";
        $class4="";
        $class5="";
        $histo_stage1="";
        $histo_stage2="";
        $histo_stage3="";
        $histo_stage4="";
        $pcr_class1="";
        $pcr_class2="";
        $pcr_class3_4="";
        $pcr_stage1="";
        $pcr_stage2="";
        $pcr_stage3="";
        $pcr_stage4="";
        $data = json_decode(file_get_contents('php://input'), true);
        $acc= "'".$data['acc_no']."'";
        $specimen_sql =  "SELECT `id`,`qc_check`,`assessioning_num` FROM `wp_abd_specimen` WHERE assessioning_num = ".$acc;
        $specimen_data = $this->BlankModel->customquery($specimen_sql);
        if(!empty($specimen_data))
        {
        $histo_pcr_sql =  "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE specimen_id = '".$specimen_data[0]['id']."'";
        $histo_pcr_results = $this->BlankModel->customquery($histo_pcr_sql);
        if(!empty($histo_pcr_results))
        {
            $unit_array = explode(",",$histo_pcr_results[0]['nail_unit']);
        }
        }
        if(!empty($unit_array))
        {
        if(in_array("1", $unit_array) || in_array("2", $unit_array) || in_array("3", $unit_array) || in_array("5", $unit_array) || in_array("6", $unit_array))
        {
            $histo_status= 1;
            if($specimen_data[0]['qc_check']=='0')
            {
                $class1="active";
            }
            else
            {
                $class1="inactive";
            }
            $stage1_sql =  "SELECT `stage_id` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '1' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage1_sql_results = $this->BlankModel->customquery($stage1_sql);
            $stage2_sql =  "SELECT `stage_id` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '2' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage2_sql_results = $this->BlankModel->customquery($stage2_sql);
            $stage3_sql =  "SELECT `stage_id` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '3' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage3_sql_results = $this->BlankModel->customquery($stage3_sql);
            $stage4_sql =  "SELECT `stage_id` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '4' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage4_sql_results = $this->BlankModel->customquery($stage4_sql);
            
            $pending_spe_sql =  "SELECT `nail_pdf`,`status` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` = '".$specimen_data[0]['id']."'";
            $pending_spe_sql_results = $this->BlankModel->customquery($pending_spe_sql);
            
            if(!empty($stage1_sql_results) || !empty($stage2_sql_results) || !empty($stage3_sql_results) || !empty($stage4_sql_results))
            {
              $class2="active";
            }
            else
            {
              $class2="inactive";
            }
            if(!empty($pending_spe_sql_results))
            {
                if(!empty($pending_spe_sql_results[0]['nail_pdf']))
                {
                    $class3="active";
                }
                else
                {
                    $class3="inactive"; 
                }
            }
            else
            {
                $class3="inactive"; 
            }
          
            if(!empty($pending_spe_sql_results))
            {
                if($pending_spe_sql_results[0]['status'] == 'Active')
                {
                    $class4="active";     
                }
                else
                {
                    $class4="inactive";
                }
            }
            else
            {
                $class4="inactive"; 
            }
            if(!empty($pending_spe_sql_results))
            {
                if($pending_spe_sql_results[0]['status'] == 'Active')
                {
                    $class5="active"; 
                }
                else
                {
                    $class5="inactive"; 
                }
            }
            else
            {
                $class5="inactive"; 
            }
            if(!empty($stage1_sql_results))
            {
                $histo_stage1="active";
            }
            else
            {
                $histo_stage1="inactive";
            }
            if(!empty($stage2_sql_results))
            {
                $histo_stage2="active";
            }
            else
            {
                $histo_stage2="inactive";
            }
            if(!empty($stage3_sql_results))
            {
                $histo_stage3="active";
            }
            else
            {
                $histo_stage3="inactive";
            }
            if(!empty($stage4_sql_results))
            {
                $histo_stage4="active";
            }
            else
            {
                $histo_stage4="inactive";
            }
            
            
        }
        else
        {
            $histo_status=0;
        } 
        }
        if(!empty($unit_array))
        {
        if(in_array("4", $unit_array) || in_array("5", $unit_array)) 
        {   
            $pcr_status= 1;
            if($specimen_data[0]['qc_check']=='0')
            {
                $pcr_class1="active";
            }
            else
            {
                $pcr_class1="inactive";
            } 

            $pcr_stage1_sql =  "SELECT `stage_id` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND stage_id = '1' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $pcr_stage1_sql_results = $this->BlankModel->customquery($pcr_stage1_sql);
            $pcr_stage2_sql =  "SELECT `stage_id` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND stage_id = '2' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $pcr_stage2_sql_results = $this->BlankModel->customquery($pcr_stage2_sql);
            $pcr_stage3_sql =  "SELECT * FROM `wp_abd_assign_stage` WHERE `specimen_id` = '".$specimen_data[0]['id']."'";
            $pcr_stage3_sql_results = $this->BlankModel->customquery($pcr_stage3_sql);
            $pcr_stage4_sql =  "SELECT `accessioning_num` FROM `wp_abd_import_data` WHERE `accessioning_num` = '".$specimen_data[0]['assessioning_num']."' AND `accessioning_num` != ''";
            $pcr_stage4_sql_results =$this->BlankModel->customquery($pcr_stage4_sql);
            
            $generated_pcr_report_sql = "SELECT `accessioning_num` FROM `wp_abd_import_data` WHERE `accessioning_num` != '' AND `accessioning_num` NOT IN('NTC','PTC') AND `report_generate`='yes' AND `accessioning_num`='".$specimen_data[0]['assessioning_num']."'";
            $generated_pcr_report_sql_results = $this->BlankModel->customquery($generated_pcr_report_sql);
           
            if(!empty($pcr_stage1_sql_results) || !empty($pcr_stage2_sql_results) || !empty($pcr_stage3_sql_results) || !empty($pcr_stage4_sql_results)) 
            {
                $pcr_class2="active";
            }
            else
            {
                $pcr_class2="inactive";
            }
            if(!empty($generated_pcr_report_sql_results)) 
            {
                $pcr_class3_4="active";
            }
            else
            {
                $pcr_class3_4="inactive";
            }
            if(!empty($pcr_stage1_sql_results))
            {
                $pcr_stage1="active";
            }
            else
            {
                $pcr_stage1="inactive";
            }
            if(!empty($pcr_stage2_sql_results))
            {
                $pcr_stage2="active";
            }
            else
            {
                $pcr_stage2="inactive";
            }
            if(!empty($pcr_stage3_sql_results))
            {
                $pcr_stage3="active";
            }
            else
            {
                $pcr_stage3="inactive";
            }
            if(!empty($pcr_stage4_sql_results))
            {
                $pcr_stage4="active";
            }
            else
            {
                $pcr_stage4="inactive";
            }

        }
        else
        {
            $pcr_status=0;
        } 
        }
        if(!empty($unit_array))
        {
            echo json_encode(array("histo_status"=>$histo_status,"pcr_status"=>$pcr_status,"acc_no"=>$specimen_data[0]['assessioning_num'], "class1" => $class1, "class2" => $class2,"class3" => $class3,"class4" => $class4,"class5" => $class5,
            "histo_stage1"=>$histo_stage1,"histo_stage2"=>$histo_stage2,"histo_stage3"=>$histo_stage3,"histo_stage4"=>$histo_stage4,
            "pcr_class1" => $pcr_class1,"pcr_class2" => $pcr_class2,"pcr_class3_4" => $pcr_class3_4,
            'pcr_stage1'=>$pcr_stage1,'pcr_stage2'=>$pcr_stage2,'pcr_stage3'=>$pcr_stage3,'pcr_stage4'=>$pcr_stage4));
        }
        else
        {
            echo json_encode(array("status"=>'0'));
        }
        
    } 
   
   
   
     /**
     * 
     * @ Specimen Tracking From Add Specimen to Specimen Report Generate
     */
     
     function track_specimen_add_to_report_generate(){
         
	    /**
	    * 
	    * @var Add Specimen And QC Check Data
	    * 
	    */    
        
	    $data = json_decode(file_get_contents('php://input'), true);
	    $accessioning_num  = $data['acc_no']; 
	    
	  //  $accessioning_num  = "190820-0020-NF"; 
	     
	    $specimen_sql =  "SELECT `id`, `lab_id`, `qc_check_lab_id`, `physician_id`, `qc_check`, `assessioning_num`, `collection_date`, `create_date`, `modify_date` FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '%".$accessioning_num."%'";
	    $specimen_data = $this->BlankModel->customquery($specimen_sql);
	         
		
		$qc_check_lab_name = "";
		$qc_check_specimen_modify_date = ""; 
      
       if(!empty($specimen_data))
        {
           
        /**
        * 
        * @var Lab Data
        * 
        */
           
         $specimen_added_lab_name = get_user_meta_value($specimen_data[0]['lab_id'], 'first_name', $single = "TRUE").' '.get_user_meta_value($specimen_data[0]['lab_id'], 'last_name', $single = "TRUE");
               
         $specimen_created_date = //date( 'm-d-Y', strtotime( $specimen_data[0]['create_date'] ));
            date('M d, Y, H : m',strtotime($specimen_data[0]['create_date']));
        /**
        * 
        * @var QC Checked Lab data
        * 
        */
       
        if( ( $specimen_data[0]['qc_check'] == '0' ) && ( $specimen_data[0]['qc_check_lab_id'] != '0' ) )
        {
           $qc_check_lab_name = get_user_meta_value($specimen_data[0]['qc_check_lab_id'], 'first_name', $single = "TRUE").' '.get_user_meta_value($specimen_data[0]['qc_check_lab_id'], 'last_name', $single = "TRUE");
           
           $qc_check_specimen_modify_date = //date( 'm-d-Y', strtotime( $specimen_data[0]['modify_date'] ));
            date('M d, Y, H : m',strtotime($specimen_data[0]['modify_date']));
        }
               
        /**
        * 
        * @var Histo Stages Data
        * 
        */
           
        $histo_pcr_sql =  "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE specimen_id = '".$specimen_data[0]['id']."'";
        $histo_pcr_results = $this->BlankModel->customquery($histo_pcr_sql);
     
        if(!empty($histo_pcr_results))
        {
            $unit_array = explode(",",$histo_pcr_results[0]['nail_unit']);
        }
    
         /**
         * 
         * @var Histo Specimen Stages
         * 
         */
        
		$stage1_lab_tech_name = ""; 
		$stage1_complete_date = "";
		$stage2_lab_tech_name = "";
		$stage2_complete_date = "";
		$stage3_lab_tech_name = "";
		$stage3_complete_date = "";
		$stage4_lab_tech_name = "";
		$stage4_complete_date = "";
				 
        if(!empty($unit_array))
         {
             
          if(in_array("1", $unit_array) || in_array("2", $unit_array) || in_array("3", $unit_array) || in_array("5", $unit_array) || in_array("6", $unit_array))
           {
          
            $histo_status = 1;
            
            $stage1_sql =  "SELECT `stage_id`, `lab_tech`, `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '1' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage1_sql_results = $this->BlankModel->customquery($stage1_sql);
            
            if($stage1_sql_results[0]['lab_tech'] != 0 && ($stage1_sql_results[0]['lab_tech'])){
            
	           $stage1_lab_tech_name =  get_user_meta_value($stage1_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($stage1_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE"); 	            
	           $stage1_complete_date = substr($stage1_sql_results[0]['specimen_timestamp'], 0, -3);
            }
                       
            $stage2_sql =  "SELECT `stage_id`,`lab_tech`, `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '2' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage2_sql_results = $this->BlankModel->customquery($stage2_sql);
            
           if($stage2_sql_results[0]['lab_tech'] != 0 && ($stage2_sql_results[0]['lab_tech'])){
            
	           $stage2_lab_tech_name =  get_user_meta_value($stage2_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($stage2_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE");             
	           $stage2_complete_date = substr($stage2_sql_results[0]['specimen_timestamp'], 0, -3);
            }
                          
            $stage3_sql =  "SELECT `stage_id`,`lab_tech`, `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '3' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage3_sql_results = $this->BlankModel->customquery($stage3_sql);
            
           if($stage3_sql_results[0]['lab_tech'] != 0 && ($stage3_sql_results[0]['lab_tech'])){
            
	           $stage3_lab_tech_name =  get_user_meta_value($stage3_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($stage3_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE");             
	           $stage3_complete_date =  substr($stage3_sql_results[0]['specimen_timestamp'], 0, -3);
            
            }
           
            
            $stage4_sql =  "SELECT `stage_id`,`lab_tech`, `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE stage_id = '4' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $stage4_sql_results = $this->BlankModel->customquery($stage4_sql);
            
           if($stage4_sql_results){
		   if($stage4_sql_results[0]['lab_tech'] != 0){
            
	           $stage4_lab_tech_name =  get_user_meta_value($stage4_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($stage4_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE");             
	           $stage4_complete_date = substr( $stage4_sql_results[0]['specimen_timestamp'],0, -3);
	             
	            // date('M d, Y, H : m',strtotime($stage4_sql_results[0]['specimen_timestamp']));
	             
              }
			}  
           
           } 
           else
           {
            $histo_status = 0;
           }            
         
          /**
          * 
          * @var PCR Report Stages
          * 
          */
          
            $pcr_stage1_lab_tech_name           = "";
			$pcr_stage1_complete_date           = "";
            $pcr_stage2_lab_tech_name           = "";
			$pcr_stage2_complete_date           = "";	
            $generated_pcr_report_lab_tech_name = "";
			$generated_pcr_report_date          = "";
         
          if(in_array("4", $unit_array) || in_array("5", $unit_array)) 
           {   
           
            $pcr_status = 1;
           
            $pcr_stage1_sql =  "SELECT `stage_id`, `lab_tech`, `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `status` = 'Complete' AND stage_id = '1' AND `specimen_id` = '".$specimen_data[0]['id']."'";
            $pcr_stage1_sql_results = $this->BlankModel->customquery($pcr_stage1_sql);
            
            if($pcr_stage1_sql_results){			
            if($pcr_stage1_sql_results[0]['lab_tech'] != 0){
		   
	            $pcr_stage1_lab_tech_name =  get_user_meta_value($pcr_stage1_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($pcr_stage1_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE");             
	            $pcr_stage1_complete_date = //$pcr_stage1_sql_results[0]['specimen_timestamp'];
	            
	              date('M d, Y, H : m',strtotime($pcr_stage1_sql_results[0]['specimen_timestamp']));
	            
              }
            }          
           
            /**
			* 
			* @var Assign Well Stages
			* 
			*/             
              
            $pcr_stage2_sql =  "SELECT `id`, `lab_tech`, `create_date` FROM `wp_abd_assign_stage` WHERE `specimen_id` = '".$specimen_data[0]['id']."'";
            $pcr_stage2_sql_results = $this->BlankModel->customquery($pcr_stage2_sql);
            
             if($pcr_stage2_sql_results){
			 if( $pcr_stage2_sql_results[0]['lab_tech'] != 0 ){
		   	  
		   		 $pcr_stage2_lab_tech_name =  get_user_meta_value($pcr_stage2_sql_results[0]['lab_tech'], 'first_name', $single = "TRUE").' '.get_user_meta_value($pcr_stage2_sql_results[0]['lab_tech'], 'last_name', $single = "TRUE");             
           		 $pcr_stage2_complete_date = date('M d, Y, H : m',strtotime($pcr_stage2_sql_results[0]['create_date']));
           		 
		      
		      }
			}            
		   
      
            $generated_pcr_report_sql = "SELECT `accessioning_num`, `id`, `sender_id`, `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` = '".$specimen_data[0]['assessioning_num']."'";
            $generated_pcr_report_sql_results = $this->BlankModel->customquery($generated_pcr_report_sql);
           
         
            if($generated_pcr_report_sql_results){
		   	   if($generated_pcr_report_sql_results[0]['sender_id'] != 0 && ( $generated_pcr_report_sql_results[0]['sender_id'] != "") ){
	
	            $generated_pcr_report_lab_tech_name =  get_user_meta_value($generated_pcr_report_sql_results[0]['sender_id'], 'first_name', $single = "TRUE").' '.get_user_meta_value($generated_pcr_report_sql_results[0]['sender_id'], 'last_name', $single = "TRUE");             
	            $generated_pcr_report_date = date('M d, Y, H : m',strtotime($generated_pcr_report_sql_results[0]['created_date']));
            
                 }
		        }           
          
              }
              else{
			  	$pcr_status = 0;
			  }
        
           }           
             
            /**
            * 
            * @var Nail Pathology Report
            * 
            */
			
			$report_generate_details = "";
			$report_generate_date = "";
			$report_sender_details = "";
			$report_sender_date = "";
			
            $histo_report_sql =  "SELECT `nail_pdf`, `lab_id`, `report_sender`, `create_date`, `dor` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` = '".$specimen_data[0]['id']."'";
            $histo_report_result = $this->BlankModel->customquery($histo_report_sql);
            
            if($histo_report_result){
				
		    if($histo_report_result[0]['lab_id']){
				
			$report_generate_details = get_user_meta_value($histo_report_result[0]['lab_id'], 'first_name', $single = "TRUE").' '.get_user_meta_value($histo_report_result[0]['lab_id'], 'last_name', $single = "TRUE");         
          
            }
       
            $report_generate_date = date('M d, Y, H : m',strtotime($histo_report_result[0]['create_date']));
            if($histo_report_result[0]['report_sender']){
				
			$report_sender_details = get_user_meta_value($histo_report_result[0]['report_sender'], 'first_name', $single = "TRUE").' '.get_user_meta_value($histo_report_result[0]['report_sender'], 'last_name', $single = "TRUE");        
           
            $report_sender_date = date('M d, Y, H : m',strtotime($histo_report_result[0]['dor']));
		  	 }
           	} 
            
            echo json_encode(array(
            "status" => '1',
           
            "acc_no" => $accessioning_num, 
            
            "specimen_added_lab_name" => $specimen_added_lab_name, 
            "specimen_created_date"   => $specimen_created_date,
            
            "qc_check_lab_name"             => $qc_check_lab_name, 
            "qc_check_specimen_modify_date" => $qc_check_specimen_modify_date, 
            
            "histo_status"           => $histo_status,
            
            "stage1_lab_tech_name"   => $stage1_lab_tech_name,
            "stage1_complete_date"   => $stage1_complete_date,    
            
            "stage2_lab_tech_name"   => $stage2_lab_tech_name,
            "stage2_complete_date"   => $stage2_complete_date,   
            
            "stage3_lab_tech_name"   => $stage3_lab_tech_name,
            "stage3_complete_date"   => $stage3_complete_date, 
            
            "stage4_lab_tech_name"   => $stage4_lab_tech_name,
            "stage4_complete_date"   => $stage4_complete_date, 
            
            "pcr_status"               => $pcr_status,
            
            "pcr_stage1_lab_tech_name" => $pcr_stage1_lab_tech_name,
            "pcr_stage1_complete_date" => $pcr_stage1_complete_date,        
            
            "pcr_stage2_lab_tech_name" => $pcr_stage2_lab_tech_name,
            "pcr_stage2_complete_date" => $pcr_stage2_complete_date,  
            
            "generated_pcr_report_lab_tech_name" => $generated_pcr_report_lab_tech_name,
            "generated_pcr_report_date"          => $generated_pcr_report_date, 
           
            "report_generate_details"  => $report_generate_details, 
            "report_generate_date"     => $report_generate_date, 
            "report_sender_details"    => $report_sender_details, 
            "report_sender_date"       => $report_sender_date, 
 
            ));
           
              
        }
        else
        {
            echo json_encode(array("status"=>'0'));
        }
          
      }
     /**
	 * End Specimen Tracking end
	 */
   
        
}


?>