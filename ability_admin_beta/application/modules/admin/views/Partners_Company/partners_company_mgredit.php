<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  // if($service['service_status']=="Active"){
  //     $redirect_url=base_url().'admin/nail_fungus/';
  // }else{
  //     $redirect_url = base_url().'admin/nail_fungus/nailFungusEdit?service_id='.$_REQUEST['service_id'];
  //     // $redirect_url=base_url().'admin/nail_fungus/service_list/'.$_REQUEST['service_id'];
  // }  

  // $val = explode(',',$report_recive_way);
  // echo $val;
  // exit();
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Partners Company
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Partners Company List</li>
    </ol>
  </section>
     <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Partners Company List</h4>
			<div id="loaderImg" style="display:none;"><img src="<?php echo base_url('assets/frontend/image/loder.gif');?>" alt="loder" width="50px" height="50px"></div>
          </div>
        <form action="<?php echo base_url().'admin/Partners_company/partnersCompanyEditing/';?>" method="POST" enctype="multipart/form-data" name="partners_company_edit" id="partners_company_edit">
         

          <input type="hidden" value="<?php echo $partnersCompany['partner_id'];?>" name="partner_id"
                 id="partner_id"/>
           
        
          <div class="form-group">
            <label for="exampleTextarea"> Partners Company Name </label>
            <input type="text" id="partner_name" name="partner_name" autocomplete="off" placeholder="Enter Partners Company Name" value="<?php echo $partnersCompany['partner_name'];?>">
            <?php echo form_error('partner_name','<div class="text-danger">','</div>');  ?>
          </div>


          <div class="form-group">
            <label for="exampleTextarea"> Partners Company's Email Id </label>
            <input type="text" id="email" name="email" placeholder="Enter Email Id" autocomplete="off" value="<?php echo $partnersCompany['email'];?>">
            <?php echo form_error('email','<div class="text-danger">','</div>');  ?>
          </div>


          <div class="form-group">
            <label for="exampleTextarea"> Partners Company's Phone No </label>
            <input type="text" id="phone_no" name="phone_no" placeholder="Enter Phone No" autocomplete="off" value="<?php echo $partnersCompany['phone_no'];?>">
            <?php echo form_error('phone_no','<div class="text-danger">','</div>');  ?>
          </div>


          <div class="form-group">
            <label for="exampleTextarea"> Partners Company's Fax No </label>
            <input type="text" id="fax_no" name="fax_no" placeholder="Enter Fax No" autocomplete="off" value="<?php echo $partnersCompany['fax_no'];?>">
            <?php echo form_error('fax_no','<div class="text-danger">','</div>');  ?>
          </div>


          <div class="form-group">
            <label for="exampleTextarea"> Partners Company's CLIA No </label>
            <input type="text" id="clia_no" name="clia_no" placeholder="Enter CLIA No" autocomplete="off" value="<?php echo $partnersCompany['clia_no'];?>">
            <?php echo form_error('clia_no','<div class="text-danger">','</div>');  ?>
          </div>



          <div class="form-group">
            <label for="exampleTextarea"> Partners Company Address1 </label>
            <input type="text" id="partner_address1" name="partner_address1" autocomplete="off" placeholder="Enter Partner Address1" value="<?php echo $partnersCompany['partner_address1'];?>">
            <?php echo form_error('partner_address1','<div class="text-danger">','</div>');  ?>
          </div>



          <div class="form-group">
            <label for="exampleTextarea"> Partners Company Address2 </label>
            <input type="text" id="partner_address2" name="partner_address2" autocomplete="off" placeholder="Enter Partner Address2" value="<?php echo $partnersCompany['partner_address2'];?>">
            <?php echo form_error('partner_address2','<div class="text-danger">','</div>');  ?>
          </div>




          <div class="form-group">
      			<label for="exampleInputFile">Company Logo</label>
      			<input type="file" class="btn btn-default btn-file" id="partner_logo" aria-describedby="fileHelp" name="partner_logo" style="margin-bottom: 10px">

            <?php
              if (($partnersCompany['partner_logo']) != '') { ?>
                <div class="image_cross">
        			     <a href="<?php echo base_url().'assets/uploads/partners/'.$partnersCompany['partner_logo'];?>" class="fancy"><img src = "<?php echo base_url().'assets/uploads/partners/'.$partnersCompany['partner_logo'];?>" 
                    alt="<?php echo $partnersCompany['partner_logo'];?>"  
                    id="fancyLaunch" class="images"></a>
                </div>
                <?php  } else{
                  echo "<div></div>";
                }  ?>

			    </div>


          <div class="form-group">
            <label for="exampleTextarea"> Laboratory Director Details   </label>
            <textarea class="tnytextarea" id="lab_director" name="lab_director" rows="10" cols="80" placeholder="Enter Laboratory Director Details" autocomplete="off"><?php echo $partnersCompany['lab_director'];?></textarea>
            <?php echo form_error('lab_director','<div class="text-danger">','</div>');  ?>
          </div>
          

          <div class="form-group">
            <?php
              $report_recive_way = $partnersCompany['report_recive_way'];
              $receiveArr = explode(',',$report_recive_way);
              
            ?>
            <label>Additional ways to get reports:</label>
            <input type="checkbox" name="way_recive_fax" value="Fax" id="way_recive_fax" <?php if(count($receiveArr)>0){if(in_array('Fax',$receiveArr)){?> checked="checked" <?php }}?>> Fax
            <input type="checkbox" name="way_recive_email" value="Email" id="way_recive_email" <?php if(count($receiveArr)>0){if(in_array('Email',$receiveArr)){?> checked="checked" <?php }}?>> Email
            <input type="checkbox" name="way_recive_none" value="None" id="way_recive_none" <?php if(count($receiveArr)>0){if(in_array('None',$receiveArr)){?> checked="checked" <?php }}?>> None
          </div>    



          <div class="form-group">
            <label for="exampleInputEmail1">Status</label>
            <select name="status" class="form-control">
              <option value="1" <?php echo ($partnersCompany['status'] =='1' ? 'selected' : '');?>>Active</option>
              <option value="0" <?php echo ($partnersCompany['status'] =='0' ? 'selected' : '');?>>Inactive</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary ban-sbmt">Submit</button>
        </form>
        </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
  
    <!-- /.row -->
  </section>
</div>
