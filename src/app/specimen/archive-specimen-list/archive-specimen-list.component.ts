import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators'; //add this
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';
import { ExcelService } from '../../excel.service';
import { ToastrService } from 'ngx-toastr';
import { TestBed } from '@angular/core/testing';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-archive-specimen-list',
  templateUrl: './archive-specimen-list.component.html',
  styleUrls: ['./archive-specimen-list.component.css']
})
export class ArchiveSpecimenListComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  public data: any;
  specimens_list: any = [];
  physicians_list: any = [];
  searchForm: FormGroup;
  exportForm: FormGroup;
  public phy_data: any;
  load_status: boolean = true;
  public loading = false;
  sendData;
  postsArray: any = [];
  no_data: any;
  no_data_status = false;
  public url = environment.assetsUrl;

  physicians_list_data: any = [];
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  modalRef: BsModalRef;
  histo_modal = false;
  pcr_modal = false;
  acc_no = false;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]//test
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  newExeclData
  constructor(
    private http: HttpClient,
    public itemsService: SpecimenService,
    public route: Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    public analyticsService: AnlyticsService,
    private excelService: ExcelService,
    private toastr: ToastrService
  ) {
    this.searchForm = new FormGroup({

      "date": new FormControl(),
      "barcode": new FormControl(),
      "fname": new FormControl(),
      "lname": new FormControl(),
      "physician_name": new FormControl(),
      "physician_id": new FormControl(),
      "assessioning_num": new FormControl(),
    });

    this.exportForm = new FormGroup({

      "frm_date": new FormControl(),
      "to_date": new FormControl(),
      "export_type": new FormControl('create_date')

    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'MM/DD/YYYY'

    });

  }
  resetForm() {
    this.searchForm.reset();
    this.getspecimen();
  }
  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.load_status = false;
      this.sendData = { "date": this.convertDate(this.searchForm.value.date), "barcode": this.searchForm.value.barcode, "fname": this.searchForm.value.fname, "lname": this.searchForm.value.lname, "assessioning_num": this.searchForm.value.assessioning_num, "physician": this.searchForm.value.physician_id, "search_type": 'archive' };
      this.itemsService.search_specimen(this.sendData).subscribe(data => {
        this.loading = false;
        this.specimens_list = data;
        this.data = this.specimens_list.specimens_detail;
        this.physicians_list = this.specimens_list.physicians_data;
        this.no_data_status = false;
        if (this.specimens_list['status'] === '1') {
          this.spinner.hide();
        }
        if (this.specimens_list['status'] === '0') {
          this.no_data_status = true;
          this.no_data = "No Data Found.";
          this.spinner.hide();
        }
      });
    }
    else {

    }
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.no_data_status = false;
      this.spinner.show();
      this.getspecimen();
      this.loading = true;
      this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
        startWith(''),
        map(value => this._filterGroup(value ? value.toLowerCase() : ''))
      );
    }
  }
  getspecimen() {
    this.spinner.show();
    this.itemsService.makeArchiveSpecimenRequest().subscribe(data => {

      this.specimens_list = data;
      if (this.specimens_list.status == '1') {
        this.spinner.hide();
        this.loading = false;
        this.no_data_status = false;
        this.data = this.specimens_list.specimens_detail;
        this.physicians_list = this.specimens_list.physicians_data;
        this.physicians_list.map((option) => this.allNames.push(option.physician_name));
        this.physicians_list.map((val) => this.allIds.push(val.id));
        this.physicians_list.map((data) => this.filterVal.push(data));
      }
      if (this.specimens_list.status == '0') {
        this.spinner.hide();
        this.no_data_status = true;
        this.no_data = "No Data Found.";
      }
    });
    this.stateGroups = [{
      ids: this.allIds,
      names: this.allNames
    }];

  }
  load_all() {
    this.no_data_status = false;
    this.spinner.show();
    this.load_status = false;
    this.itemsService.loadAllRequest().subscribe(data => {
      this.loading = false;
      this.specimens_list = data;
      this.data = this.specimens_list.specimens_detail;
      this.physicians_list = this.specimens_list.physicians_data;

      if (this.specimens_list['status'] === '1') {
        this.spinner.hide();
      }

      if (this.specimens_list['status'] === '0') {
        this.no_data_status = true;
        this.no_data = "No Data Found.";
        this.spinner.hide();
      }
    });
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];

      return this.searchData;
    }

    return this.stateGroups;
  }

  callSomeFunction(phy_id: any) {
    this.searchForm.controls['physician_id'].setValue(phy_id);
  }

  get_a_qrcode(assessioning_num: any) {
    this.print(assessioning_num);
    this.print(assessioning_num);
  }

  print(assessioning_num: any): void {
    let printContents, popupWin;
    let qr_path: any = this.url;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
        <html>
          <head>
            <title>Print tab</title>
            <style>
            //........Customized style.......
            </style>
          </head>
      <body onload="window.print();window.close()">
      <div id="${assessioning_num}_1;?>" class="print_qr_code">
      <img src='${qr_path}assets/uploads/qr_images/qr_${assessioning_num}.png'>
      <p style='font-size:15px; margin-top: -12px;'><strong>${assessioning_num}</strong></p>
      </div>
      </body>
        </html>`
    );
    popupWin.document.close();
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [mnth, day, date.getFullYear()].join("/");
    }
    return newDate;
  }

  displaytrackmodal(specimen_id: any) {
    this.spinner.show();
    this.sendData = { "acc_no": specimen_id, 'dataType': 'archive' };
    this.analyticsService.specimen_track(this.sendData).subscribe(data => {
      this.postsArray = data;
      this.spinner.hide();
      if (this.postsArray.histo_status == '1') {
        this.acc_no = true;
        this.histo_modal = true;
      }
      else {
        this.histo_modal = false;
      }
      if (this.postsArray.pcr_status == '1') {
        this.acc_no = true;
        this.pcr_modal = true;
      }
      else {
        this.pcr_modal = false;
      }
      if (this.postsArray.status == '0') {
        this.acc_no = false;
        this.pcr_modal = false;
        this.histo_modal = false;
      }
      this.modalRef = this.modalService.show(TrackModalComponent, {
        initialState: {
          title: 'track',
          acc_no: this.acc_no,
          postsArray: this.postsArray,
          histo_modal: this.histo_modal,
          pcr_modal: this.pcr_modal
        }

      });

    });


  }

  exportAsXLSX(): void {
    let sendData;
    this.spinner.show();
    if (this.exportForm.value.frm_date && this.exportForm.value.to_date) {
      sendData = { "from_date": this.convertDate(this.exportForm.value.frm_date), "to_date": this.convertDate(this.exportForm.value.to_date), "export_type": this.exportForm.value.export_type };

      let exdata: any = [];
      this.itemsService.getExeclExportData(sendData).subscribe(data => {
        this.newExeclData = data;
        console.log(this.newExeclData['status']);
        this.spinner.hide();
        if (this.newExeclData['status'] === '1') {
          let exportData = this.newExeclData.excel_data;
          exportData.forEach((item, indx) => {
            exdata.push(item);
          });
          if (exdata.length > 0) {
            this.excelService.exportAsExcelFile(exdata, 'sample');
          }
        } else if (this.newExeclData['status'] === '0') {
          this.toastr.info(this.newExeclData.err_msg, 'Error', {
            timeOut: 3000
          });
        }

      });
    } else {
      this.spinner.hide();
      this.toastr.info('Please select a date range', 'Error', {
        timeOut: 3000
      });
    }

  }

}
