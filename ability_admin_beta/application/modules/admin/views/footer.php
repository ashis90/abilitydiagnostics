<?php
if(!defined('BASEPATH')) EXIT("No direct script access allowed");
$basepath = base_url("assets/");
$controllername = $this->uri->segment(2);
?> 

<footer class="main-footer">
    <div class="pull-right hidden-xs">
     
    </div>
    <strong>Copyright &copy; 2017- <?php echo date('Y'); ?> Ability Diagnostics -</strong> All Rights Reserved.
    <!-- Copyright © 2017-2019 Ability Diagnostics - All Rights Reserved -->
</footer>


</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script> -->
<!-- <script src="<?php echo $basepath; ?>plugins/jQuery/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.1.0.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo $basepath; ?>plugins/jQueryUI/jquery-ui.min.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $basepath; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $basepath; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $basepath; ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?=$basepath?>alertify/js/alertify.min.js"></script>
<script  src="<?php echo $basepath; ?>plugins/ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>


<script type="text/javascript">
$(document).ready(function () {
  $('#basicDataTable').dataTable();
});
</script>
<!-- datepicker -->
<script src="<?php echo $basepath; ?>plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo $basepath; ?>dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo $basepath; ?>dist/js/demo.js"></script>
<!--   Core JS Files   -->

<script>
 $(function() 
 {   $( "#datepicker" ).datepicker({
					      maxDate: '+0d',
						  yearRange:"-100:+0",
						  changeMonth: true,
						  changeYear: true,
						  dateFormat: 'yy-mm-dd'
					     });
 });
 </script>  

<script type="text/javascript" src="<?php echo $basepath;?>js/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.min.js"></script>
<script>

	jQuery.validator.addMethod("alpha", function(value, element) {
	return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
	});
	jQuery.validator.addMethod("phone_validation", function(value, element) {
	  return this.optional(element) || /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value);
	}, "Enter Valid Phone Number Please");
	jQuery.validator.addMethod("edit_password", function(value, element) {
	  return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(value);
	}, "password contain at least 8 characters(one digit and one lower case and one upper case)");
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
 return this.optional(element) || value != $(param).val();
}, "This has to be different...");


jQuery(function ($){
      $('#admin_frm').validate({
          rules: {
            email: {
              required: true,
              email: true
            },
            mobile: {
               phone_validation : true
            },
           fname:  {
	            required: true,
	            alpha:true
			}, 
			lname:{
			    alpha:true
			},
			edit_password: {
  			    minlength: 6
  			}, 
  		    edit_con_pass: {
  			    equalTo: "#edit_password"
  			}, 
          },
         messages: {
	        email:'Please Enter a Valid email address',
	        fname:'Please enter alphabet only',
	        lname :'Please enter alphabet only',
            }
        });
});


jQuery(function($) {
  $("#service_frm").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',

    rules: {
        service_title: "required",
        service_desc: {
          required: true,
        },       
        agree: "required"
    },
    messages: {
        service_title: "Please enter your Service Title",
        service_desc: "Please enter your Service Description",
        agree: "Please accept our policy"
    }
  });
});


jQuery(function($) {
  $("#nailfungus_frm").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',

    rules: {
        service_title: "required",
        service_desc: {
          required: true,
        },       
        agree: "required"
    },
    messages: {
        service_title: "Please enter your Nail Fungus Title",
        service_desc: "Please enter your Nail Fungus Description",
        agree: "Please accept our policy"
    }
  });
});



jQuery(function($) {
  $("#page_frm").submit(function() {
    tinyMCE.triggerSave();
  }).validate({
      ignore:'',
      rules: {
        site_page_name: "required",
        site_page_title: "required",
        site_page_desc: "required",
        site_page_type: "required",
        messages: {
          site_page_name: "Please Enter Page Name",
          site_page_title: "Please Enter Page Title",
          site_page_desc: "Please Enter Page Description",
          site_page_type: "Please Select Page Type",
        }
      }
  });
});


jQuery(function($) {
  $("#holiday_frm").validate({
    rules: {
        title: "required",
        start_date: "required",
        end_date: "required",
        agree: "required"
    },
    messages: {
        title: "Please enter your Holiday Title",
        start_date: "Please enter your Holiday Start date",
        end_date: "Please enter a your Holiday End date",        
        agree: "Please accept our policy"
    }
  });
});

jQuery(function($) {
  $("#role_frm").validate({
    rules: {
      role_name: "required",
      role_shortname: "required"
    },
    messages: {
      role_name : "Please Enter Role Name",
      role_shortname: "Please Ener Role Short Name"
    }
  });
});



jQuery(function($) {
  $("#program_frm").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',
    rules: {
      user_username: "required",
      user_firstname: "required",
      user_role: "required",   
      user_email: "required",   
      user_password: "required",
    
    },
    messages: {
      user_username: "Please Enter User Names",
      user_firstname: "Please Enter First Name",
      user_lastname: "Please Enter Last Name",
      user_role: "Please Select Role",   
      user_email: "Please Enter Email",   
      user_password: "Please Enter Password",   
    }
  });
});
 


jQuery(function($) {
  $("#partners_company_edit").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',
    rules: {
      partner_name: "required",
      phone_no: {
        required: true
      },      
      partner_address1: "required",
      lab_director: {
          required: true,
      },
    },
    messages: {
      partner_name: "Please Enter Partners Company Name",
      phone_no:{
        required:'Please Enter Phone No'
      },
      
      partner_address1: "Please Enter Partner Address1",
      lab_director: "Please Enter Lab Director Details",
    }
  });
});

jQuery(function($) {
  $("#partners_company_frm").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',
    rules: {
      partner_name: "required",
      phone_no: {
        required: true
      },
      partner_address1: "required",
      lab_director: {
          required: true,
      },
    },
    messages: {
      partner_name: "Please Enter Partners Company Name",
      phone_no:{
        required:'Please Enter Phone No'
      },
      partner_address1: "Please Enter Partner Address1",
      lab_director: "Please Ener Lab Director Details",
    },
    submitHandler:function(){
      var ajaxurl = '<?php echo base_url();?>admin/partners_company/PartnerAdd';
      var lab = tinymce.get('lab_director').getContent();////////////get textarea value/////////
        // Get form   /////////////// USING FORMDATA///////////////////////
      var form = $('#partners_company_frm')[0];
        // Create an FormData object 
      var formData = new FormData(form);
      formData.append( 'lab_director_new', lab );
      $.ajax({
          url: ajaxurl,
          type: 'POST',
          data: formData, 
          processData: false,
          contentType: false,
          cache: false,
          timeout: 600000,   
          error: function() {
            alert('Something is wrong');
          },
          success: function(data) {

            alert('Partners Companyy Successfully Inserted');
            window.location.href='<?php echo base_url();?>admin/Partners_company';
         
            var jasonData = JSON.parse(data);
            console.log(jasonData);
            var partner_name = jasonData.partner_name;
            var phone_no = jasonData.phone_no;
            var partner_address1 = jasonData.partner_address1;
           
            $("#errorName").html(partner_name);
            $("#errorPh").html(phone_no);
            $("#errorAddress1").html(partner_address1);
                         
          }
      });
    }
  });
});


$(document).ready(function(){
	$("#img_upload").click(function(){    
		$("#img_upload_section").show();
	});
		
	$("#close").click(function(){ 
	 $("#img_upload_section").hide();
	});	

  $("#report_frm").submit(function(){
    tinyMCE.triggerSave();
  }).validate({    
    ignore:'',
    rules: {
      sc: "required",  
      // diagnosis: "required",
      text: {
        required:true,
      },
    
    },
    messages: {
      sc: "Please Enter Short Code",
      text: "Please Enter Shortcode Description",  
    },

  });
});


</script>

<script>
jQuery('#mobile').on('keyup keypress blur change',function(e){
this.value = this.value
.match(/\d*/g).join('')
.match(/(\d{0,3})(\d{0,3})(\d{0,4})/).slice(1).join('-')
.replace(/-*$/g, '');
}); 
</script>

<!---------------------------- Partners Company Fax,clia Validaion -------------------------->
<script type="text/javascript">
  $('#fax_no,#phone_no').keypress(function(event) {
    if (((event.which != 45 || (event.which == 45 && $(this).val() == '')) ||
            $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
  }).on('paste', function(event) {
      event.preventDefault();
  });
</script>
<!----------------------------End Partners Company Fax,clia Validaion -------------------------->

<!--------------When add user & select user role(partner company) list of parners company open------------------------>
<script type="text/javascript">
  $('#user_role').change(function()
  {
    var val = $('#user_role').val();

    if (val == 'partners_company_manager') {
      $('#partner').show();
      // $("#partner_company").prop('required',true);
      $("#partner_company").rules('add',"required");
    } else{
      $('#partner').hide();
      // $("#partner_company").prop('required',false);
      $("#partner_company").rules('remove',"required");

    }
  });
</script>
<!--------------End When add user & select user role(partner company) list of parners company open------------------------>

<!----------When edit user(onload) & select user role(partner company) list of parners company open-------------------->
<script type="text/javascript">
$(document).ready(function() {

    var val = $('#user_role').val();
    if (val == 'partners_company_manager') {
      $('#partner').show();
      // $("#partner_company").prop('required',true);
      $("#partner_company").rules('add',"required");
    } else{
      $('#partner').hide();
      // $("#partner_company").prop('required',false);
      $("#partner_company").rules('remove',"required");
    }
    // alert($("#user_role").val());
});
</script>
<!----------End When edit user(onload) & select user role(partner company) list of parners company open----------------->

<script type="text/javascript">
    $('.roles').click(function(){
      // var value = $("#shows").text();
      var value = $(this).find("p").text();
        // alert(value);
      if (value == 'Partners Company') {
        // alert('ok');
        // alert(r);

        $('#partners_show').show();
      } else{
        $('#partners_show').hide();

      }
    });
</script>

<!---------------------------- Partners Company Fancybox Validaion -------------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $(".fancy").fancybox()
  });
</script>
<!----------------------------End Partners Company Fancybox Validaion -------------------------->


<script type="text/javascript">
$(function(){
  var old_name;
  $('#role_name').keyup(function()
  {
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
      $(this).val(no_spl_char);
      alert('Special Character is not allowed');
    } else{
        var str = func();
        var strlower = str.toLowerCase();
        old_name = str;
        $("#role_shortname").val(strlower);
    }
  });


  $('#role_shortname').keyup(function()
  {
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
      $(this).val(no_spl_char);
      alert('Special Character is not allowed');
      $("#role_shortname").val(old_name);
    }
  });

});


function func() { 

  var str = $("#role_name").val();
  str = str.replace(/\s/g,'_');  
  return str;
} 
</script>
<script type="text/javascript">
$(function(){

  // var old_name;
  $('#role_names').keyup(function()
  {
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
      $(this).val(no_spl_char);
      alert('Special Character is not allowed');
    }
  });
});

</script>

<script>
function delete_data(dataId,table_name,delete_for ){
	alertify.confirm("Are you sure you want to delete?", function (e) { 
    if (e) {     
		var ajaxurl = '<?php echo base_url();?>admin/Ajax/delete_request';
		jQuery.ajax({
			url: ajaxurl,
			type: 'post',
			data: {dataId : dataId, table_name : table_name, delete_for : delete_for},
			success: function(response){			
				if (response == "1"){					
				   jQuery('#data-'+dataId).remove();
				   jQuery('#alert-success').show();
			       jQuery('#alert-success').html('Data Removed Successfully!!').fadeIn('fast').delay(2000).fadeOut('slow');
			      }
			    }
 			});
 	}
	});	
}

function del_data(dataId,table_name,delete_for ){	
	alertify.confirm("Are you sure you want to delete?", function (e) { 
    if (e) {  
		var ajaxurl = '<?php echo base_url();?>admin/Ajax/delete_gallery';
		jQuery.ajax({
			url: ajaxurl,
			type: 'post',
			data: {dataId : dataId, table_name : table_name, delete_for : delete_for},
			success: function(response){			
			if (response == "1")
          		{
				   jQuery('#data-'+dataId).remove();
				   jQuery('#alert-success').show();
			       jQuery('#alert-success').html('Data Removed Successfully!!').fadeIn('fast').delay(2000).fadeOut('slow');
			      }
			    }
 			});
 	}
	});	
}


/*-----------------------------------user send mail---------------------------------*/
    function sendMail(user)
      {
    jQuery.ajax({
      url: '<?php echo base_url();?>admin/User/sendemail',

            type: 'POST',
            data: {
              user:user
            },
            success:function(data){
      
        if(data==1)
              {
                jQuery('#mailSend').show();
                jQuery('#mail_send_status_'+user).html(' <div class="title_aa" id="mail_send_status_"+user style="color: #12ed38; font: 400 18px Roboto Condensed,sans-serif;">Yes</div>');
                jQuery(".pop").slideFadeToggle();
               
              }
              else
              {
                jQuery('#mailSend').show();
                jQuery('#mailSend').html("Mail could not send! Please try again letter");
              }
      
            }
          });
    
      }
    /*-------------------------------------------------------------------*/


$('#select_all').click(function(event) {
  if(this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
          this.checked = true;
      });
  }
  else {
    $(':checkbox').each(function() {
          this.checked = false;
      });
  }
});
</script>

<script src="<?=base_url("assets/")?>tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: ".tnytextarea",
    plugins: "link image,code",  
    mode : "specific_textarea",
    //editor_selector : "content_descr",
 });  

</script>

<script type="text/javascript">
      function deleteData(dataId,table_name,delete_for,del_field){
        alertify.confirm("Are you sure you want to delete this record?", function (e) { 
        if (e) { 
          var ajaxurl = '<?php echo base_url();?>admin/ajax/deletedata';
          jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            data: {dataId : dataId, table_name : table_name, delete_for : delete_for,del_field:del_field},
            success: function(response){
            if (response == 1)
              {
                alertify.success("You have successfully deleted this record.");
                jQuery("#data-"+dataId).remove();
                   
              }
            }
            });
        } 
        });
        return false;
      }
    </script> 
</body>
</html>