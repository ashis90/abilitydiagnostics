import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityReportsComponent } from './quality-reports.component';

describe('QualityReportsComponent', () => {
  let component: QualityReportsComponent;
  let fixture: ComponentFixture<QualityReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
