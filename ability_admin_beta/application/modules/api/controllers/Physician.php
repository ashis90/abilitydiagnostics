<?php
if (!defined('BASEPATH'))
  EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Physician extends MY_Controller
{
  
  function __construct()
  {
    parent::__construct();
  }
  function index()
  {
    echo "test Physician service";
  }
  
  /**
   * Top 10 Physician list
   */
  function top_new_physicians_list()
  {
    $physicians = array();
    $data       = json_decode(file_get_contents('php://input'), true);
    $limit      = ' LIMIT 0, 10';
    $curr_month = date('n');
    $curr_year  = date('Y');
    $user_role  = $data['user_role'];
    $user_id    = $data['user_id'];
    
    if ($user_role != 'aps_sales_manager' && $user_role != 'partner' && $user_role != 'clinic' && $user_role != 'combine_physicians_accounts' && $user_role != 'data_entry_oparator') {
      
      if ($user_role == 'sales_regional_manager' || $user_role == 'regional_manager') {
        $user_id = get_assign_user($data['user_id']);
      }
      
      $sql = 'SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'added_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND  t2.meta_value IN (' . $user_id . ') ';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
      
    }
    
    elseif ($user_role == 'partner') {
      
      $sql = 'SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'_assign_partner\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "' . $user_id . '" ';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
    } 
    
    elseif ($user_role == 'clinic') {
      
      $sql = 'SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'_assign_clinic\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "' . $user_id . '" ';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
    } 

   elseif ($user_role == 'combine_physicians_accounts') {
      
      $sql = 'SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'combined_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "' . $user_id . '" ';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
    }
    
    else {
      $sql = 'SELECT  ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
		      WHERE   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
		      AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\' ';
      $sql .= ' ) ORDER BY user_registered DESC';
    }
    
    if ($data['user_role'] == 'aps_sales_manager' || $data['user_role'] == 'data_entry_oparator') {
      $sql .= $limit;
    }
    
    $physicians_list = $this->BlankModel->customquery($sql);
    $extra = "";
    if ($physicians_list) {
      $physician_count = 0;
      foreach ($physicians_list as $physician) {
        $physician_id = $physician['ID'];
        $sales_id     = get_user_meta_value($physician_id, 'added_by', TRUE);
        if ($sales_id) {
          $sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE) . " " . get_user_meta_value($sales_id, 'last_name', TRUE);
          
        } else {
          $sales_reps_name = '';
        }
        
        $clinic_addrs = get_user_meta_value($physician_id, 'clinic_addrs', TRUE);
        
        $charged_amount = get_total_amt_phy($physician_id, 'chargedamt', $curr_month, $curr_year);
        if ($charged_amount != "") {
          $amt_ch       = "$" . number_format($charged_amount, 2, '.', ',');
          $short_amt_ch = $charged_amount;
        } else {
          $amt_ch       = "$00.00";
          $short_amt_ch = 0;
        }
        /** @var Paid Amount */
        
        $paid_amount = get_total_amt_phy($physician_id, 'paidamt', $curr_month, $curr_year);
        if ($paid_amount != "") {
          $amt_paid       = "$" . number_format($paid_amount, 2, '.', ',');
          $short_amt_paid = $paid_amount;
        } else {
          $amt_paid       = "$00.00";
          $short_amt_paid = 0;
        }
        
        // $specimen_sql = "SELECT MAX(`wp_abd_specimen`.`create_date`) as create_date, COUNT(`wp_abd_specimen`.`id`) AS total_orders, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`collection_date`, `wp_abd_nail_pathology_report`.`nail_pdf`
        // FROM `wp_abd_specimen`
        // INNER JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id`=`wp_abd_nail_pathology_report`.`specimen_id`
        // WHERE ( `wp_abd_specimen`.`physician_id` = '".$physician_id."') AND `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`qc_check` = '0'";
        
        
        if($user_id == "76389" && $physician_id == "137"){
		   $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
		}
        else{
		   $extra = " AND `create_date` > '2017-03-27 23:59:59' ";
		}
        
        $specimen_sql = " SELECT MAX(create_date) as `create_date`, COUNT(id) AS `total_orders`,`test_type` FROM `wp_abd_specimen` WHERE `physician_id` = $physician_id AND `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' $extra GROUP BY `test_type`";
        
        $specimen_submitted = $this->BlankModel->customquery($specimen_sql);
        // $specimen_submitted_date = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
        $specimen_submitted_date = '';
        // $totalSpecimen  = $specimen_submitted[0]['total_orders'];
        $totalSpecimen  =0;
        $totalSpecimen_NF =0;
        $totalSpecimen_W = 0;
        $specimen_submitted_date_NF = '';
        $specimen_submitted_date_W = '';
        if(array_key_exists("0", $specimen_submitted)){
          if($specimen_submitted[0]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }else if($specimen_submitted[0]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }

          if(array_key_exists("1", $specimen_submitted)){
            if($specimen_submitted[1]['test_type'] == 'NF'){
              $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }else if($specimen_submitted[1]['test_type'] == 'W'){
              $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }
          }
          $specimen_submitted_date = ($specimen_submitted_date_NF > $specimen_submitted_date_W? $specimen_submitted_date_NF : $specimen_submitted_date_W);
          $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
        }else{
          $totalSpecimen  =0;
          $specimen_submitted_date = '';
        }

        $physician_data = array(
          'totalSpecimen_NF' => $totalSpecimen_NF,
          'totalSpecimen_W' => $totalSpecimen_W,
          'short_totalSpecimen' => (int) $totalSpecimen,
          'specimen_submitted_date' => $specimen_submitted_date,
          'sales_id' => $sales_id,
          'charged_amt' => $amt_ch,
          'paid_amt' => $amt_paid,
          'short_charged_amt' => (int) $short_amt_ch,
          'short_paid_amt' => (int) $short_amt_paid,
          'clinic_addrs' => $clinic_addrs,
          'physician_id' => $physician_id,
          'full_name' => get_user_meta_value($physician_id, 'first_name', TRUE) . ' ' . get_user_meta_value($physician_id, 'last_name', TRUE),
          'user_status' => get_user_meta_value($physician_id, '_status', TRUE),
          'sales_reps_name' => $sales_reps_name,
        );
        array_push($physicians, $physician_data);
        $physician_count++;
      }
      echo json_encode(array(
        'status' => '1',
        "physicians_details" => $physicians,
        'physicians_count' => $physician_count
      ));
    } else {
      echo json_encode(array(
        'status' => '0',
        "message" => 'no data found!'
      ));
    }
  }
  
  /**
   * /
   * Get all Physician list
   */
  
 function all_physicians_list()
  {
    $physicians = array();
    $data       = json_decode(file_get_contents('php://input'), true);
    $limit      = $data['limit'];
    $extra      = '';
    $user_role  = $data['user_role'];
    $user_id    = $data['user_id'];
    if (($data['user_role'] != 'aps_sales_manager') && ($data['user_role'] != 'data_entry_oparator')) {
      $sql = ' 
      SELECT  ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'added_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "' . $data['user_id'] . '" )';
      $sql .= ' ORDER BY wp_abd_users.user_registered DESC';
      $sql .= " LIMIT $limit,10";
    } else {
      $sql = 'SELECT  ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
      WHERE   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
      AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\' )';
      $sql .= ' ORDER BY wp_abd_users.user_registered DESC';
      $sql .= " LIMIT $limit,10";
    }
    
    $physicians_list = $this->BlankModel->customquery($sql);
    if ($physicians_list) {
      $physician_count = 0;
      foreach ($physicians_list as $physician) {
        $physician_id = $physician['ID'];
        $sales_id     = get_user_meta_value($physician_id, 'added_by', TRUE);
        if ($sales_id) {
          $sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE) . " " . get_user_meta_value($sales_id, 'last_name', TRUE);
        }
        
        // $specimen_sql = "SELECT MAX(create_date) as create_date, COUNT(id) AS total_orders FROM `wp_abd_specimen` WHERE `physician_id` = '".$physician_id."' AND `status` = '0' AND `qc_check` = '0' AND `physician_accepct` = '0'";
        
        // $specimen_sql = "SELECT MAX(`wp_abd_specimen`.`create_date`) as create_date, COUNT(`wp_abd_specimen`.`id`) AS total_orders, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`collection_date`, `wp_abd_nail_pathology_report`.`nail_pdf`
        //      FROM `wp_abd_specimen`
        //      INNER JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id`=`wp_abd_nail_pathology_report`.`specimen_id`
        //      WHERE ( `wp_abd_specimen`.`physician_id` = '".$physician_id."') AND `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`qc_check` = '0'";

        if($user_id == "76389" && $physician_id == "137"){
          $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
        }
        else{
		  $extra = " AND `create_date` > '2017-03-27 23:59:59' ";
		}
        
        $specimen_sql = " SELECT MAX(create_date) as create_date, COUNT(id) AS total_orders,`test_type` FROM wp_abd_specimen WHERE physician_id =  $physician_id AND `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' $extra GROUP BY `test_type`";
        $specimen_submitted      = $this->BlankModel->customquery($specimen_sql);

        // $specimen_submitted_date = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
        // $totalSpecimen  = $specimen_submitted[0]['total_orders'];

        $specimen_submitted_date = '';
        $totalSpecimen  =0;
        $totalSpecimen_NF =0;
        $totalSpecimen_W = 0;
        $specimen_submitted_date_NF = '';
        $specimen_submitted_date_W = '';
        if(array_key_exists("0", $specimen_submitted)){
          if($specimen_submitted[0]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }else if($specimen_submitted[0]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }

          if(array_key_exists("1", $specimen_submitted)){
            if($specimen_submitted[1]['test_type'] == 'NF'){
              $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }else if($specimen_submitted[1]['test_type'] == 'W'){
              $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }
          }
          $specimen_submitted_date = ($specimen_submitted_date_NF > $specimen_submitted_date_W? $specimen_submitted_date_NF : $specimen_submitted_date_W);
          $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
        }else{
          $totalSpecimen  =0;
          $specimen_submitted_date = '';
        }

        $physician_data = array(
          'totalSpecimen_NF' => $totalSpecimen_NF,
          'totalSpecimen_W' => $totalSpecimen_W,
          'short_totalSpecimen' => (int) $totalSpecimen,
          'specimen_submitted_date' => $specimen_submitted_date,
          'sales_id' => $sales_id,
          'physician_id' => $physician_id,
          'full_name' => get_user_meta_value($physician_id, 'first_name', TRUE) . ' ' . get_user_meta_value($physician_id, 'last_name', TRUE),
          'user_status' => get_user_meta_value($physician_id, '_status', TRUE),
          'sales_reps_name' => $sales_reps_name
        );
        array_push($physicians, $physician_data);
        $physician_count++;
      }
      echo json_encode(array(
        'status' => '1',
        "physicians_details" => $physicians,
        'physicians_count' => $physician_count
      ));
    } else {
      echo json_encode(array(
        'status' => '0',
        "message" => 'no data found!'
      ));
    }
  }
  
  
  
  function total_num_of_physicians()
  {
    $physicians = array();
    $data       = json_decode(file_get_contents('php://input'), true);
    if (($data['user_role'] != 'aps_sales_manager') && ($data['user_role'] != 'data_entry_oparator')) {
      $sql = '
      SELECT  ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'added_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "' . $data['user_id'] . '" ';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
    } else {
      $sql = 'SELECT  ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
      WHERE   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
      AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\' ';
      $sql .= ' ) ORDER BY user_registered DESC';
    }
    
    $physicians_list = $this->BlankModel->customquery($sql);
    $physician_count = 0;
    if ($physicians_list) {
      $physician_count = count($physicians_list);
      echo json_encode(array(
        'status' => '1',
        'physicians_count' => $physician_count
      ));
    } else {
      echo json_encode(array(
        'status' => '0',
        "message" => 'no data found!'
      ));
    }
  }
  
  
  function add_physician()
  {
    $data = json_decode(file_get_contents('php://input'), true);
    
    if (!empty($data) && isset($data)) {
      
      $check_email    = email_validation('users', 'user_email', $data['user_email']);
      $check_username = username_validation('users', 'user_login', $data['user_login']);
      
      if (!$check_email && !$check_username) {
        
        
        $password     = md5(SECURITY_SALT . $data['con_pass']);
        $display_name = $data['first_name'] . ' ' . $data['last_name'];
        $created_date = date("Y-m-d h:i:sa");
        $table_data   = array(
          'user_login' => $data['user_login'],
          'user_pass' => $password,
          'user_email' => $data['user_email'],
          'user_url' => $data['user_url'],
          'display_name' => $display_name,
          'user_nicename' => strtolower($data['first_name']),
          'user_registered' => $created_date
        );
        $user_id      = $this->BlankModel->addTableData('users', $table_data);
        if ($user_id) {
          $role_type_array = array(
            'physician' => '1'
          );
          $role            = serialize($role_type_array);
          
          $adderRole = get_user_role($data['sales_rep']);
          
          add_user_meta($user_id, 'first_name', $data['first_name']);
          add_user_meta($user_id, 'last_name', $data['last_name']);
          add_user_meta($user_id, '_mobile', $data['mobile']);
          add_user_meta($user_id, 'wp_abd_capabilities', $role);
          add_user_meta($user_id, '_address', $data['address']);
          add_user_meta($user_id, '_status', $data['user_status']);
          add_user_meta($user_id, 'combined_by', $data['combine_phy_acc']);
          add_user_meta($user_id, 'clinic_addrs', $data['clinic_add']);
          add_user_meta($user_id, 'fax', $data['fax']);
          add_user_meta($user_id, 'anthr_fax_number', $data['anthr_fax']);
          add_user_meta($user_id, 'cell_phone', $data['cell_phone']);
          add_user_meta($user_id, 'manager_contact_name', $data['manager_contact_name']);
          add_user_meta($user_id, 'manager_cell_number', $data['manager_cell_number']);
          add_user_meta($user_id, 'npi', $data['npi_api']);
          
          if (isset($data['sales_rep']) && !empty($data['sales_rep'])) {
            $added_by = $data['sales_rep'];
          } else {
            $added_by = $data['user_id'];
          }
          
          add_user_meta($user_id, 'added_by', $added_by);
          add_user_meta($user_id, '_assign_clinic', $data['assign_clinic']);
          add_user_meta($user_id, '_assign_partner', $data['assign_partner']);
          
          add_user_meta($user_id, 'assign_specialty', $data['assign_specialty']);
          add_user_meta($user_id, '_clinic_name', $data['clinic_name']);
          add_user_meta($user_id, 'state', $data['state']);
          
          if ($data['report_recive_way_fax'] != "" && $data['report_recive_way_email'] != "") {
            if ($data['report_recive_way_fax'] == 'Fax' || $data['report_recive_way_fax'] == true) {
              $fax = 'Fax';
            }
            if ($data['report_recive_way_email'] == 'Email' || $data['report_recive_way_email'] == true) {
              $email = 'Email';
            }
            $report_recive_way = $fax . ',' . $email;
          } elseif ($data['report_recive_way_fax'] != "") {
            if ($data['report_recive_way_fax'] == 'Fax' || $data['report_recive_way_fax'] == true) {
              $report_recive_way = 'Fax';
            }
          } elseif ($data['report_recive_way_email'] != "") {
            if ($data['report_recive_way_email'] == 'Email' || $data['report_recive_way_email'] == true) {
              $report_recive_way = 'Email';
            }
          } else {
            $report_recive_way = '';
          }
          add_user_meta($user_id, 'report_recive_way', $report_recive_way);
          
          $this->new_physician_added_notification($data['sales_rep'], $user_id);
          echo json_encode(array(
            "status" => "1",
            "message" => 'User Added successfully.'
          ));
        } else {
          echo json_encode(array(
            "status" => "0",
            "message" => 'Something is worng!. Please try again'
          ));
        }
      } else {
        echo json_encode(array(
          "status" => "0",
          "message" => $check_email . ' ' . $check_username
        ));
      }
      
    }
    
  }
  
  
  
  function new_physician_added_notification($sales_id, $physician_id)
  {
    $fname          = get_user_meta($physician_id, 'first_name', true);
    $lname          = get_user_meta($physician_id, 'last_name', true);
    $physician_name = $fname['meta_value'] . ' ' . $lname['meta_value'];
    if ($physician_name == "") {
      $physician_sql  = $this->db->query("SELECT `user_login` FROM wp_abd_users WHERE `ID` = '" . $physician_id . "'");
      $physician_name = $physician_sql['user_login'];
    }
    $notification_details = "New physician Dr. " . $physician_name . " added ";
    $assigened_sales_id   = get_user_meta($sales_id, 'assign_to', TRUE);
    $assigened_sales_id   = $assigened_sales_id['meta_value'];
    
    if ($assigened_sales_id) {
      
      $new_physician_added_notification_fr_abv_sales_array  = array(
        'users_id' => $assigened_sales_id,
        'physician_id' => $physician_id,
        'notification_details' => nl2br($notification_details),
        'notification_details_aft_date' => '5',
        'note_publish_date' => date('Y-m-d'),
        'note_read' => 'Unread',
        'note_for' => 'Sales',
        'status' => '0',
        'create_date' => date('Y-m-d H:i:s')
      );
      $insert_new_physician_added_notification_fr_abv_sales = $this->db->insert('wp_abd_notification', $new_physician_added_notification_fr_abv_sales_array);
    }
    
    $new_physician_added_notification_fr_sale_admin_array = array(
      'users_id' => '66',
      'physician_id' => $physician_id,
      'notification_details' => nl2br($notification_details),
      'notification_details_aft_date' => '5',
      'note_publish_date' => date('Y-m-d'),
      'note_read' => 'Unread',
      'note_for' => 'Sales',
      'status' => '0',
      'create_date' => date('Y-m-d H:i:s')
    );
    $new_physician_added_notification_fr_sale_admin       = $this->db->insert('wp_abd_notification', $new_physician_added_notification_fr_sale_admin_array);
    $new_physician_added_notification_array               = array(
      'users_id' => $sales_id,
      'physician_id' => $physician_id,
      'notification_details' => nl2br($notification_details),
      'notification_details_aft_date' => '5',
      'note_publish_date' => date('Y-m-d'),
      'note_read' => 'Unread',
      'note_for' => 'Sales',
      'status' => '0',
      'create_date' => date('Y-m-d H:i:s')
    );
    $insert__new_physician_added_notification             = $this->db->insert('wp_abd_notification', $new_physician_added_notification_array);
  }
  
  
  // delete physician 
  function delete_physician()
  {
    $data     = json_decode(file_get_contents('php://input'), true);
    $user_id  = $data['user_id'];
    $data     = "ID";
    $del_sql  = $this->BlankModel->delete_data_id('users', $data, $user_id);
    // delete data from usermeta table 
    $data     = "user_id";
    $del_sql2 = $this->BlankModel->delete_data_id('usermeta', $data, $user_id);
    
    if ($del_sql == 'Success') {
      echo json_encode(array(
        "status" => "1",
        "message" => 'User deleted successfully.'
      ));
    } else {
      echo json_encode(array(
        "status" => "0",
        "message" => 'Something is worng!. Please try again.'
      ));
    }
    
  }
  
  
  /**
   * search_physician_list
   */
  
  function search_physician_list()
  {
    
    $physicians = array();
    $data       = json_decode(file_get_contents('php://input'), true);
    $status = $data['status'];
    
    if (isset($data['fname'])) {
      $search_data = $data['fname'];
    }
    $state     = $data['state'];
    $from_date = $data['from_date'];
    $to_date   = $data['to_date'];
    $user_role  = $data['user_role'];
    $user_id    = $data['user_id'];
    $roles = 'physician';
    $sql   = '';
    
    // if user role is Aps Sales Manager and Data Entry Oparator
    
    
    $sql .= 'SELECT ID FROM  wp_abd_users 
                INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID  = t1.user_id ';
    if ($status != '') {
      $sql .= ' INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID  = t2.user_id AND t2.meta_key = \'_status\'';
    }
    if ($state != '') {
      $sql .= '	INNER JOIN wp_abd_usermeta as t3 ON wp_abd_users.ID  = t3.user_id AND t3.meta_key = \'state\'';
    }
  
    if ($search_data != '') {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
    }
  
     $sql .= ' INNER JOIN wp_abd_usermeta as t5 ON wp_abd_users.ID  = t5.user_id			
			   WHERE t1.meta_key = \'wp_abd_capabilities\' AND t1.meta_value LIKE "%' . $roles . '%"';
    
    if ($status != '') {
     $sql .= ' AND t2.meta_value =  \'' . $status . '\'';
    }
    
    if ($state != '') {
     $sql .= '  AND t3.meta_value =  \'' . $state . '\' ';
    }
    
    if ($search_data != '') {
     $sql .= "    
        	AND (b1.meta_value LIKE '%".strtok( $search_data, " ")."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
    }   
    
   
    //Sales Reps
    
    if ($data['user_role'] == 'sales')
    {      
     $sql .= ' AND t5.meta_key = \'added_by\' AND t5.meta_value = "' . $data['user_id'] . '" ';
    }
    
    //Partner
    
    elseif ($data['user_role'] == 'partner') {
      
     $sql .= ' AND t5.meta_key = \'_assign_partner\' AND t5.meta_value = "' . $data['user_id'] . '" ';
      
    }
    //Clinic
    elseif ($data['user_role'] == 'clinic') {
      
     $sql .= ' AND t5.meta_key = \'_assign_clinic\' AND t5.meta_value = "' . $data['user_id'] . '" ';
      
    }
    // Combine_physicians_accounts
    elseif ($data['user_role'] == 'combine_physicians_accounts') {
      
     $sql .= ' AND t5.meta_key = \'combined_by\' AND t5.meta_value = "' . $data['user_id'] . '" ';
    }
    elseif ($data['user_role'] == 'sales_regional_manager' || $data['user_role'] == 'regional_manager') {
      
     $user_id = get_assign_user($data['user_id']);
     $sql .= ' AND t5.meta_key = \'added_by\' AND t5.meta_value IN ( ' . $user_id . ') ';
    }
    
     $sql .= ' AND t5.meta_key = \'added_by\' ORDER BY user_registered DESC';

    
     $physicians_list = $this->BlankModel->customquery($sql);
    
    if ($physicians_list) {
      $physician_count = 0;
      foreach ($physicians_list as $physician) {
        $physician_id = $physician['ID'];
        $sales_id     = get_user_meta_value($physician_id, 'added_by', TRUE);
        if ($sales_id) {
          $sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE) . " " . get_user_meta_value($sales_id, 'last_name', TRUE);
        }
        
        if($user_id == "76389" && $physician_id == "137"){
          $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
       }
         
        //$last_spc_date = " SELECT MAX(create_date) as create_date FROM wp_abd_specimen WHERE physician_id =  $physician_id ";
        
      //   $specimen_sql = "SELECT MAX(`wp_abd_specimen`.`create_date`) as create_date, COUNT(`wp_abd_specimen`.`id`) AS total_orders, `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`collection_date`, `wp_abd_nail_pathology_report`.`nail_pdf`,`wp_abd_specimen`.`test_type`
      //  FROM `wp_abd_specimen`
      //  INNER JOIN `wp_abd_nail_pathology_report` ON `wp_abd_specimen`.`id`=`wp_abd_nail_pathology_report`.`specimen_id`
      //  WHERE ( `wp_abd_specimen`.`physician_id` = '" . $physician_id . "') AND `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`qc_check` = '0' GROUP BY `wp_abd_specimen`.`test_type`";
       $specimen_sql = "SELECT MAX(create_date) as create_date, COUNT(`wp_abd_specimen`.`id`) AS total_orders,`test_type`  FROM `wp_abd_specimen` WHERE physician_id = $physician_id AND status = '0' AND physician_accepct = '0' AND qc_check = '0' GROUP BY `test_type`";           
        
        if (!empty($from_date) && !empty($to_date)) {
          
          $specimen_sql = "SELECT MAX(create_date) as create_date, COUNT(`wp_abd_specimen`.`id`) AS total_orders,`test_type`  FROM `wp_abd_specimen` WHERE physician_id = $physician_id AND status = '0' AND physician_accepct = '0' AND qc_check = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."' GROUP BY `test_type`";           
        }
        
          
        $specimen_submitted = $this->BlankModel->customquery($specimen_sql);
    
        $specimen_submitted_date = '';
        $totalSpecimen  =0;
        $totalSpecimen_NF =0;
        $totalSpecimen_W = 0;
        $specimen_submitted_date_NF = '';
        $specimen_submitted_date_W = '';
        if(array_key_exists("0", $specimen_submitted)){
          if($specimen_submitted[0]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }else if($specimen_submitted[0]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }

          if(array_key_exists("1", $specimen_submitted)){
            if($specimen_submitted[1]['test_type'] == 'NF'){
              $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }else if($specimen_submitted[1]['test_type'] == 'W'){
              $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }
          }
          $specimen_submitted_date = ($specimen_submitted_date_NF > $specimen_submitted_date_W? $specimen_submitted_date_NF : $specimen_submitted_date_W);
          $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
        }else{
          $totalSpecimen  =0;
          $specimen_submitted_date = '';
        }
        
        // $specimen_submitted_date = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
        
        // $specimen_submitted_date = ($last_specimen_date[0]['create_date'] != "" ? date('F d, Y', strtotime($last_specimen_date[0]['create_date'])) : "");
        
        // $totalSpecimen  = $specimen_submitted[0]['total_orders'];
        $physician_data = array(
          'totalSpecimen_NF' => $totalSpecimen_NF,
          'totalSpecimen_W' => $totalSpecimen_W,
          'short_totalSpecimen'     => (int) $totalSpecimen,
          'specimen_submitted_date' => $specimen_submitted_date,
          'sales_id'                => $sales_id,
          'physician_id'            => $physician_id,
          'full_name'               => get_user_meta_value($physician_id, 'first_name', TRUE) . ' ' . get_user_meta_value($physician_id, 'last_name', TRUE),
          'user_status'             => get_user_meta_value($physician_id, '_status', TRUE),
          'sales_reps_name'         => $sales_reps_name
        );
        array_push($physicians, $physician_data);
        $physician_count++;
        
      }
      
      echo json_encode(array(
        'status' => '1',
        "physicians_details" => $physicians,
        'physicians_count' => $physician_count
      ));
    }
    
    else {
      echo json_encode(array(
        'status' => '0',
        "message" => 'no data found!'
      ));
    }
  }
  
  /**
   * Physician Details 
   */
  
  function physician_details()
  {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($data['id'] != "") {
      $user_id = $data['id'];
       
      $conditions      = " ( `ID` = '" . $user_id . "')";
      $select_fields   = '*';
      $is_multy_result = 1;
      $memDetails      = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);      
      $fname                 = get_user_meta_value($user_id, 'first_name', TRUE);
      $lname                 = get_user_meta_value($user_id, 'last_name', TRUE);
      $mob                   = get_user_meta_value($user_id, '_mobile', TRUE);
      $add                   = get_user_meta_value($user_id, '_address', TRUE);
      $clinic_add            = get_user_meta_value($user_id, 'clinic_addrs', TRUE);
      $fax                   = get_user_meta_value($user_id, 'fax', TRUE);
      $anthr_fax             = get_user_meta_value($user_id, 'anthr_fax_number', TRUE);
      $cell                  = get_user_meta_value($user_id, 'cell_phone', TRUE);
      $manager               = get_user_meta_value($user_id, 'manager_contact_name', TRUE);
      $manager_cell          = get_user_meta_value($user_id, 'manager_cell_number', TRUE);
      $offce_num             = get_user_meta_value($user_id, 'office_number', TRUE);
      $npi_api               = get_user_meta_value($user_id, 'npi', TRUE);
      $assign_clinic         = get_user_meta_value($user_id, '_assign_clinic', TRUE);
      $acc_cntrl             = get_user_meta_value($user_id, 'specimen_control_access', TRUE);
      $acc_cntrl_qc          = get_user_meta_value($user_id, 'specimen_control_access_qc', TRUE);
      $role                  = get_user_role($user_id);
      $bank_routing_num      = get_user_meta_value($user_id, '_brn', TRUE);
      $bank_account_num      = get_user_meta_value($user_id, '_ban', TRUE);
      $name_on_bank          = get_user_meta_value($user_id, '_nbaccount', TRUE);
      $user_status           = get_user_meta_value($user_id, '_status', TRUE);
      $commission_percentage = get_user_meta_value($user_id, '_commission_percentage', TRUE);
      $sales_rep             = get_user_meta_value($user_id, 'added_by', TRUE);
      $assign_partner        = get_user_meta_value($user_id, '_assign_partner', TRUE);
      $clinic_name           = get_user_meta_value($user_id, '_clinic_name', TRUE);
      $assign_specialty      = get_user_meta_value($user_id, 'assign_specialty', TRUE);
      $state                 = get_user_meta_value($user_id, 'state', TRUE);
      $combined_by           = get_user_meta_value($user_id, 'combined_by', TRUE);
      $report_recive_way     = get_user_meta_value($user_id, 'report_recive_way', TRUE);
      
      if ($sales_rep) {
        $sales_rep_fullname = get_user_meta_value($sales_rep, 'first_name', TRUE) . ' ' . get_user_meta_value($sales_rep, 'last_name', TRUE);
      } else {
        $sales_rep_fullname = '';
      }
      
      if ($report_recive_way) {
        $fax2  = '';
        $email = '';
        if ($report_recive_way == 'Fax') {
          $fax2 = 'Fax';
        } else if ($report_recive_way == 'Email') {
          $email = 'Email';
        } else {
          $report_recive_ways = $report_recive_way;
          $report_recive_ways = explode(",", $report_recive_ways);
          if ($report_recive_ways[0] == 'Fax') {
            $fax2  = 'Fax';
            $email = 'Email';
          } else {
            $fax2  = 'Fax';
            $email = 'Email';
          }
        }
        
      } else {
        $fax2  = '';
        $email = '';
      }
      
      // Assign Physician to Clinic Profile list
      $role_clinic_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%clinic%' ORDER BY u.user_nicename ASC ";
      $role_clinic_all     = $this->BlankModel->customquery($role_clinic_all_que);
      $assign_clinic_name  = array();
      foreach ($role_clinic_all as $clinic) {
        $user['name'] = htmlspecialchars_decode(get_user_meta_value($clinic['ID'], 'first_name', TRUE) . ' ' . get_user_meta_value($clinic['ID'], 'last_name', TRUE));
        $user['id']   = $clinic['ID'];
        array_push($assign_clinic_name, $user);
      }
      
      
      // sales rep list
      $role_sales_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND (um2.meta_value LIKE '%sales%' OR um2.meta_value LIKE '%sales_regional_manager%' OR um2.meta_value LIKE '%aps_sales_manager%') ORDER BY u.user_nicename ASC ";
      
      $role_sales_all    = $this->BlankModel->customquery($role_sales_all_que);
      $assign_sales_name = array();
      foreach ($role_sales_all as $sales) {
        $user['name'] = htmlspecialchars_decode(get_user_meta_value($sales['ID'], 'first_name', TRUE) . ' ' . get_user_meta_value($sales['ID'], 'last_name', TRUE));
        $user['id']   = $sales['ID'];
        array_push($assign_sales_name, $user);
      }
      
      
      // Partner list
      $all_partner_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%partner%' ORDER BY u.user_nicename ASC ";
      
      $all_partner      = $this->BlankModel->customquery($all_partner_que);
      $all_partner_name = array();
      foreach ($all_partner as $sales) {
        $user['name'] = get_user_meta_value($sales['ID'], 'first_name', TRUE) . ' ' . get_user_meta_value($sales['ID'], 'last_name', TRUE);
        $user['id']   = $sales['ID'];
        array_push($all_partner_name, $user);
      }
      
      // Combine Physicians list
      $all_combine_physicians_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%combine_physicians_accounts%' ORDER BY u.user_nicename ASC ";
      
      $all_physicians      = $this->BlankModel->customquery($all_combine_physicians_que);
      $all_physicians_name = array();
      foreach ($all_physicians as $physicians) {
        $user['name'] = get_user_meta_value($physicians['ID'], 'first_name', TRUE) . ' ' . get_user_meta_value($physicians['ID'], 'last_name', TRUE);
        $user['id']   = $physicians['ID'];
        array_push($all_physicians_name, $user);
      }
      
      $specimen_information = array(
        'fname' => $fname,
        'lname' => $lname,
        'acc_cntrl' => $acc_cntrl,
        'acc_cntrl_qc' => $acc_cntrl_qc,
        'email' => $memDetails['user_email'],
        'user_login' => $memDetails['user_login'],
        'url' => $memDetails['user_url'],
        'mob' => $mob,
        'add' => $add,
        'clinic_add' => $clinic_add,
        'fax' => $fax,
        'anthr_fax' => $anthr_fax,
        'cell' => $cell,
        'manager' => $manager,
        'manager_cell' => $manager_cell,
        'offce_num' => $offce_num,
        'npi_api' => $npi_api,
        'assign_clinic' => $assign_clinic,
        'role' => $role,
        'bank_routing_num' => $bank_routing_num,
        'bank_account_num' => $bank_account_num,
        'user_status' => $user_status,
        'user_registered_date' => $memDetails['user_registered'],
        'commission_percentage' => $commission_percentage,
        'name_on_bank' => $name_on_bank,
        'sales_rep' => $sales_rep,
        'assign_partner' => $assign_partner,
        'clinic_name' => $clinic_name,
        'assign_specialty' => $assign_specialty,
        'combined_by' => $combined_by,
        'report_recive_way_fax' => $fax2,
        'report_recive_way_email' => $email,
        'state' => $state,
        'sales_rep_fullname' => $sales_rep_fullname
      );
      
      if ($specimen_information) {
        
        echo json_encode(array(
          "status" => "1",
          "details" => $specimen_information,
          'assign_clinic' => $assign_clinic_name,
          'all_sales_rep' => $assign_sales_name,
          'all_partner_name' => $all_partner_name,
          'all_com_physicians_name' => $all_physicians_name
        ));
      } else {
        echo json_encode(array(
          "status" => "0"
        ));
      }
    }
  }
  
  ///////////////////// get all dropdown value for add new physician add /////////////////////
  
  public function get_all_dropdown_data()
  {
    $data = json_decode(file_get_contents('php://input'), true);
    
    $added_by  = $data['user_id'];
    $user_role = get_user_role($added_by);
    
    // Assign Physician to Clinic Profile list
    $role_clinic_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u 
    JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       
    JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
    WHERE um1.meta_key = '_status' 
    AND um1.meta_value = 'Active' 
    AND um2.meta_key = 'wp_abd_capabilities' 
    AND um2.meta_value LIKE '%clinic%'    
    ORDER BY u.user_nicename ASC ";
    
    $role_clinic_all     = $this->BlankModel->customquery($role_clinic_all_que);
    $assign_clinic_name  = array();
    
    foreach ($role_clinic_all as $clinic) {
      $user_id      = $clinic['ID'];
      $c_fname      = get_user_meta_value($user_id, 'first_name');
      $c_lname      = get_user_meta_value($user_id, 'last_name');     
      $user['name'] = htmlspecialchars_decode($c_fname . ' ' . $c_lname);
      $user['id']   = $user_id;
      array_push($assign_clinic_name, $user);
      
    }
    
    // sales rep list
    if ($user_role == 'aps_sales_manager' || $user_role == 'data_entry_oparator') {
        $role_sales_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u 
        JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id 
        JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
        WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
        AND um2.meta_key = 'wp_abd_capabilities' 
        AND (um2.meta_value LIKE '%sales%' OR um2.meta_value LIKE '%sales_regional_manager%' OR um2.meta_value LIKE '%aps_sales_manager%') 
        ORDER BY u.user_nicename ASC ";
    } 
    else{
       $role_sales_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u 
	    JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id 
	    JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
	    JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
	    WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
	    AND um3.meta_key   = 'assign_to' AND um3.meta_value = $added_by  
	    AND um2.meta_key   = 'wp_abd_capabilities' AND (um2.meta_value LIKE '%sales%' OR um2.meta_value LIKE '%sales_regional_manager%' OR um2.meta_value LIKE '%aps_sales_manager%') 
	    ORDER BY u.user_nicename ASC ";
    }
    
    $role_sales_all    = $this->BlankModel->customquery($role_sales_all_que);
    $assign_sales_name = array();
    foreach ($role_sales_all as $sales) {
      $user_id      = $sales['ID'];
      $s_fname = get_user_meta_value($user_id, 'first_name');
      $s_lname  = get_user_meta_value($user_id, 'last_name');    
      $user['name'] = $s_fname . ' ' . $s_lname;
      $user['id']   = $user_id;
      array_push($assign_sales_name, $user);
    }
    
    // Partner list
    $all_partner_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u 
    JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       
    JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
    WHERE um1.meta_key = '_status' 
    AND um1.meta_value = 'Active' 
    AND um2.meta_key = 'wp_abd_capabilities' 
    AND um2.meta_value LIKE '%partner%' 
    ORDER BY u.user_nicename ASC ";
    
    $all_partner      = $this->BlankModel->customquery($all_partner_que);
    $all_partner_name = array();
    foreach ($all_partner as $sales) {
      $user_id      = $sales['ID'];
      $user['name'] = get_user_meta_value($user_id, 'first_name', TRUE) . ' ' . get_user_meta_value($user_id, 'last_name', TRUE);
      
      $user['id'] = $user_id;
      array_push($all_partner_name, $user);
    }
    
    // Combine Physicians list
    $all_combine_physicians_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u 
    JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id       
    JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
    WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
    AND um2.meta_key   = 'wp_abd_capabilities' 
    AND um2.meta_value LIKE '%combine_physicians_accounts%' 
    ORDER BY u.user_nicename ASC ";
    
    $all_physicians      = $this->BlankModel->customquery($all_combine_physicians_que);
    $all_physicians_name = array();
    foreach ($all_physicians as $physicians) {
      $user_id = $physicians['ID'];
      
      $user['name'] = get_user_meta_value($user_id, 'first_name', TRUE) . ' ' . get_user_meta_value($user_id, 'last_name', TRUE);
      $user['id']   = $user_id;
      array_push($all_physicians_name, $user);
    }
       echo json_encode(array(
        "status"                  => "1",
        'assign_clinic'           => $assign_clinic_name,
        'all_sales_rep'           => $assign_sales_name,
        'all_partner_name'        => $all_partner_name,
        'all_com_physicians_name' => $all_physicians_name
      ));
  
  }
  
  /**
   * Update Physician Details
   */
  
  function update_physician_details()
  {
    $data      = json_decode(file_get_contents('php://input'), true);
    $user_id   = $data['user_id'];
    $status    = array();
    $otherdata = 'no';
    $otherdata = update_user_meta($user_id, 'first_name', $data['first_name']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'last_name', $data['last_name']);
    $otherdata = update_user_meta($user_id, '_mobile', $data['mobile']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_brn', $data['brn']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_ban', $data['ban']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_nbaccount', $data['nbaccount']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_commission_percentage', $data['commission_percentage']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_address', $data['address']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'clinic_addrs', $data['sec_address']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'fax', $data['fax']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'anthr_fax_number', $data['anthr_fax']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'cell_phone', $data['cell_phone']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'manager_contact_name', $data['manager_contact_name']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'manager_cell_number', $data['manager_cell_number']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'npi', $data['npi_api']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_assign_clinic', $data['assign_clinic']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'combined_by', $data['combine_phy_acc']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'added_by', $data['sales_rep']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_assign_partner', $data['assign_partner']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, '_clinic_name', $data['clinic_name']);
    array_push($status, $otherdata);
    $otherdata = update_user_meta($user_id, 'assign_specialty', $data['assign_specialty']);
    array_push($status, $otherdata);
    if ($data['report_recive_way_fax'] != "" && $data['report_recive_way_email'] != "") {
      if ($data['report_recive_way_fax'] == 'Fax' || $data['report_recive_way_fax'] == true) {
        $fax = 'Fax';
      }
      if ($data['report_recive_way_email'] == 'Email' || $data['report_recive_way_email'] == true) {
        $email = 'Email';
      }
      $way       = $fax . ',' . $email;
      $otherdata = update_user_meta($user_id, 'report_recive_way', $way);
      array_push($status, $otherdata);
    } else if ($data['report_recive_way_fax'] != "") {
      $otherdata = update_user_meta($user_id, 'report_recive_way', $data['report_recive_way_fax']);
      array_push($status, $otherdata);
    } else if ($data['report_recive_way_email'] != "") {
      $otherdata = update_user_meta($user_id, 'report_recive_way', $data['report_recive_way_email']);
      array_push($status, $otherdata);
    } else {
      $way       = '';
      $otherdata = update_user_meta($user_id, 'report_recive_way', $way);
      array_push($status, $otherdata);
    }
    
    $otherdata = update_user_meta($user_id, '_status', $data['user_status']);
    array_push($status, $otherdata);
    $userdata = array();
    if (!empty($data['user_url'])) {
      $url = $data['user_url'];
    } else {
      $url = '';
    }

    if (!empty($data['user_email'])) {
      $user_email = $data['user_email'];
      $this->db->where('ID !=',$user_id);
      $this->db->where('user_email',$user_email);
      $resData = $this->db->get('wp_abd_users');

      if($resData->num_rows() > 0){
      	die(json_encode(array(
        "status" => "1",
        "message" => 'Email exits.Please try with new one.'
      	)));
      }else{
      	$userdata['user_email'] = $user_email;
      }
      
    } 

    if (!empty($data['edit_con_pass']) && $data['edit_con_pass'] !== null) {
      $password = md5(SECURITY_SALT . $data['edit_con_pass']);
      $userdata['user_pass'] = $password;
      $userdata['user_url']  = $url;
    } else {
      $userdata['user_url']  = $url;
    }

    $conditions = "( `ID` = " . $user_id . ")";
    $update     = $this->BlankModel->editTableData('users', $userdata, $conditions);
    
    if ($update == 'no' || $update == 'yes' || in_array('yes', $status)) {
      echo json_encode(array(
        "status" => "1",
        "message" => 'User details has been successfully updated.'
      ));
    } else {
      echo json_encode(array(
        "status" => "0",
        "message" => 'Something is worng!. Please try again.'
      ));
    }
  }
  
  
  //////////// active and inactive///////////////
  function change_user_status()
  {
    $data    = json_decode(file_get_contents('php://input'), true);
    $user_id = $data['user_id'];
    $status  = $data['status'];
    
    if ($status == 'Inactive') {
      $update = update_user_meta($user_id, '_status', 'Active');
    } else {
      $update = update_user_meta($user_id, '_status', 'Inactive');
    }
    
    if ($update = 'yes') {
      echo json_encode(array(
        "status" => "1",
        "message" => ' User details has been successfully updated.'
      ));
    } else {
      echo json_encode(array(
        "status" => "0",
        "message" => 'Something is worng!. Please try again.'
      ));
    }
  }
  
  /**
   * Physicians List by Sales Reps
   */
  
  function physicians_list_by_sales_reps()
  {
    $post_data             = json_decode(file_get_contents('php://input'), true);
    $curr_month            = date('n');
    $curr_year             = date('Y');
    $total_paid_amount     = 0;
    $total_charged_amount  = 0;
    $total_charged_amt     = 0;
    $total_paid_amt        = 0;
    $physicians_data_array = array();
    $current_user_id       = $post_data['user_id'];
    $user_role             = $post_data['user_role'];
    $access_user_list      = array(
							      'sales',
							      'team_regional_Sales',
							      'team_sales_manager',
							      'sales_regional_manager',
							      'regional_manager'
								  );
    
    
    
    if (isset($post_data['sales_reps_id'])) {
      $sales_reps_id = $post_data['sales_reps_id'];
    }
    
    if (in_array($user_role, $access_user_list) && !isset($post_data['sales_reps_id'])) {
      $sales_reps_id = $current_user_id;
    }
    
    $sales_man_name  = get_user_meta_value($sales_reps_id, 'first_name') . ' ' . get_user_meta_value($sales_reps_id, 'last_name');
    $regional_man_id = get_user_meta_value($sales_reps_id, 'assign_to');
    if ($regional_man_id) {
      $regional_man_name = get_user_meta_value($regional_man_id, 'first_name') . ' ' . get_user_meta_value($regional_man_id, 'last_name');
    } else {
      $regional_man_name = '';
    }
    
    $salesID       = get_assign_user($sales_reps_id);
    $physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
        JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = "_status")
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 
		WHERE (m3.meta_value LIKE "%physician%" )
        AND m4.meta_value IN (' . $salesID . ') 
        AND m2.meta_value = "Active"';
    
    $physicians_list = $this->BlankModel->customquery($physician_sql);
    
    if ($physicians_list) {
      $physician_count = 0;
      foreach ($physicians_list as $physicians) {
        $physician      = $physicians['ID'];
        $charged_amount = get_user_meta_value($physician, 'month_' . $curr_month . '_' . $curr_year . '_chargedamt');
        if (!empty($charged_amount)) {
          $total_charged_amt += $charged_amount;
        }
        $paid_amount = get_user_meta_value($physician, 'month_' . $curr_month . '_' . $curr_year . '_paidamt', TRUE);
        if (!empty($paid_amount)) {
          $total_paid_amt += $paid_amount;
        }
        $total_paid_amount    = "$" . number_format($total_paid_amt, 2, '.', ',');
        $total_charged_amount = "$" . number_format($total_charged_amt, 2, '.', ',');
        
        $specimen_sql = "SELECT COUNT(id) AS totalSpecimen FROM `wp_abd_specimen` WHERE physician_id = '" . $physician . "' AND status = 0 AND qc_check = 0 AND physician_accepct = 0 AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
        $specimens    = $this->BlankModel->customquery($specimen_sql);
        if ($specimens) {
          $totalSpecimen = $specimens[0]['totalSpecimen'];
        } else {
          $totalSpecimen = 0;
        }
        
        $first_name     = get_user_meta_value($physician, 'first_name');
        $last_name      = get_user_meta_value($physician, 'last_name');
        $u_status       = get_user_meta_value($physician, '_status');
        $physician_data = array(
          'user_id'       => $physician,
          'totalSpecimen' => $totalSpecimen,
          'physician_name'=> $first_name . " " . $last_name,
          'u_status'      => $u_status,
          'charged_amount'=> ($charged_amount != "" ? "$" . number_format($charged_amount, 2, '.', ',') : "$00.00"),
          'paid_amount'   => ($paid_amount != "" ? "$" . number_format($paid_amount, 2, '.', ',') : "$00.00")
        );
        array_push($physicians_data_array, $physician_data);
        $physician_count++;
      }
      
      
      echo json_encode(array(
        'status' => '1',
        'total_physician' => $physician_count,
        'sales_man_name' => $sales_man_name,
        'regional_man_name' => $regional_man_name,
        'total_paid_amt' => $total_paid_amount,
        'total_charged_amt' => $total_charged_amount,
        'physicians_data' => $physicians_data_array
      ));
    } else {
      $no_data_found = "No Data Found";
      echo json_encode(array(
        'status' => '0',
        'sales_man_name' => $sales_man_name,
        'regional_man_name' => $regional_man_name,
        'no_physician_data' => $no_data_found,
        'total_users' => '0'
      ));
    }
  }
  
  /**
   * Sales Reps Physician Search 
   */
  
  function sales_reps_search_physician_list()
  {
    
    $physicians_data_array = array();
    $curr_month            = date('n');
    $curr_year             = date('Y');
    $total_paid_amount     = 0;
    $total_charged_amount  = 0;
    $total_charged_amt     = 0;
    $total_paid_amt        = 0;
    $post_data             = json_decode(file_get_contents('php://input'), true);
    
    if (($post_data['from_month'] != '') && ($post_data['from_year'] != '')) {
      $curr_month = $post_data['from_month'];
      $curr_year  = $post_data['from_year'];
    }
    $status          = $post_data['status'];
    $search_value    = $post_data['search_value'];
    $current_user_id = $post_data['user_id'];
    $user_role       = $post_data['user_role'];
    
    $access_user_list = array(
      'sales',
      'team_regional_Sales',
      'team_sales_manager',
      'sales_regional_manager',
      'regional_manager'
    );
    
    if (isset($post_data['sales_reps_id'])) {
      $sales_reps_id = $post_data['sales_reps_id'];
    }
    
    if (in_array($user_role, $access_user_list) && !isset($post_data['sales_reps_id'])) {
      $sales_reps_id = $current_user_id;
    }
      $salesID       = get_assign_user($sales_reps_id);
    if (!empty($status) && !empty($search_value)) {
      $sales_reps_physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 		
		JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = "_status")
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = "first_name")	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = "last_name")	
	
	
		WHERE (m3.meta_value LIKE "%physician%")        
        AND(m6.meta_value LIKE "%' . $search_value . '%"
        OR m7.meta_value LIKE  "%' . $search_value . '%"  
        OR u1.user_nicename LIKE "%'.strtolower(preg_replace('/\s*/', '',$search_value)).'%"
		OR u1.display_name LIKE "%'.trim($search_value).'%")        
       
        AND m5.meta_value = "' . $status . '"
        AND m4.meta_value IN (' . $salesID . ')';
        
    } 
    else if (!empty($search_value) && empty($status)) {
      $sales_reps_physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 	 	
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = "first_name")	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = "last_name")	
		
		WHERE (m3.meta_value LIKE "%physician%")        
        AND(m6.meta_value LIKE "%' . $search_value . '%"
        OR m7.meta_value LIKE  "%' . $search_value . '%"  
        OR u1.user_nicename LIKE "%'.strtolower(preg_replace('/\s*/', '',$search_value)).'%"
		OR u1.display_name LIKE "%'.trim($search_value).'%")   
        AND m4.meta_value IN (' . $salesID . ')';
    } else if (!empty($status)) {
      $sales_reps_physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
    JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
    JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by")    
    JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = "_status")
 
    WHERE (m3.meta_value LIKE "%physician%")      
        AND m5.meta_value = "' . $status . '"
        AND m4.meta_value IN (' . $salesID . ')';
    } else {
      $sales_reps_physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 	 	
		
		WHERE (m3.meta_value LIKE "%physician%")        
        AND m4.meta_value IN (' . $salesID . ')';
    }
    
 
    $physicians_list = $this->BlankModel->customquery($sales_reps_physician_sql);
    if ($physicians_list) {
        $physician_count = 0;
      foreach ($physicians_list as $physicians) {
        $physician      = $physicians['ID'];
        $charged_amount = get_user_meta_value($physician, 'month_' . $curr_month . '_' . $curr_year . '_chargedamt');
        if (!empty($charged_amount)) {
          $total_charged_amt += $charged_amount;
        }
        $paid_amount = get_user_meta_value($physician, 'month_' . $curr_month . '_' . $curr_year . '_paidamt', TRUE);
        if (!empty($paid_amount)) {
          $total_paid_amt += $paid_amount;
        }
        $total_paid_amount    = "$" . number_format($total_paid_amt, 2, '.', ',');
        $total_charged_amount = "$" . number_format($total_charged_amt, 2, '.', ',');
        
        $specimen_sql = "SELECT COUNT(id) AS totalSpecimen FROM `wp_abd_specimen` WHERE physician_id = '" . $physician . "' AND status = 0 AND qc_check = 0 AND physician_accepct = 0 AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
        $specimens    = $this->BlankModel->customquery($specimen_sql);
        if ($specimens) {
          $totalSpecimen = $specimens[0]['totalSpecimen'];
        } else {
          $totalSpecimen = 0;
        }
        $first_name     = get_user_meta_value($physician, 'first_name');
        $last_name      = get_user_meta_value($physician, 'last_name');
        $u_status       = get_user_meta_value($physician, '_status');
        $physician_data = array(
          'user_id'        => $physician,
          'totalSpecimen'  => $totalSpecimen,
          'physician_name' => $first_name . " " . $last_name,
          'u_status'       => $u_status,
          'charged_amount' => ($charged_amount != "" ? "$" . number_format($charged_amount, 2, '.', ',') : "$00.00"),
          'paid_amount'    => ($paid_amount != "" ? "$" . number_format($paid_amount, 2, '.', ',') : "$00.00")
        );
        array_push($physicians_data_array, $physician_data);
        $physician_count++;
      }
      
      echo json_encode(array(
        'status' => '1',
        'total_physician'   => $physician_count,
        'total_paid_amt'    => $total_paid_amount,
        'total_charged_amt' => $total_charged_amount,
        'physicians_data'   => $physicians_data_array
      ));
     } 
    else {
      $no_data_found = "No Data Found";
      echo json_encode(array(
        'status'            => '0',
        'no_physician_data' => $no_data_found,
        'total_users'       => '0'
      ));
     }
   }
  
  /**
   *  Physician Specimen Details 
   */
  
  function physician_specimen_details()
  {
    
    $specimen_data_array        = array();
    $total_specimen_count       = 0;
    $histo_pending_report_count = 0;
    $histo_report_added_count   = 0;
    $total_pcr_report_count     = 0;
    
    $histo_report_status = "";
    $pcr_report_status   = "";
    $patient_name        = "";
    $extra               = "";
    $extra_add_on        = "";
    
    $post_data       = json_decode(file_get_contents('php://input'), true);
    $physician_id    = $post_data['physician_id'];
    $current_user_id = $post_data['user_id'];
    $user_role       = $post_data['user_role'];
    $test_type       = $post_data['test_type'];
    
    if ($post_data['from'] != "" && $post_data['to'] != "") {
     
      $extra .= " AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$post_data['from']."' AND '".$post_data['to']."'";
     }

     if( !empty($test_type) && $test_type !='All' ){
      $extra .=" AND `test_type` LIKE '%".$test_type."%'";
      }
      
      
     if($current_user_id == "76389" && $physician_id == "137"){
	   $extra_add_on = " AND `create_date` > '2018-06-12 23:59:59' ";
	 } 
     else{
	   $extra_add_on = " AND `create_date` > '2017-03-27 23:59:59'";
	 } 
      
    
    $specimen_sql = "SELECT `id`, `collection_date`, `create_date`, `date_received`, `assessioning_num`, `p_firstname`, `p_lastname` FROM `wp_abd_specimen` WHERE `physician_id` = '" . $physician_id . "'  AND `status` = '0' AND `physician_accepct` = '0' and `qc_check` = '0'  ". $extra_add_on . $extra . "  ORDER BY `assessioning_num` DESC";
    
    $specimen_results = $this->BlankModel->customquery($specimen_sql);
    
    $physician_name          = get_user_meta_value($physician_id, 'first_name') . ' ' . get_user_meta_value($physician_id, 'last_name');
    $physician_fax           = get_user_meta_value($physician_id, 'fax');
    $physician_clinic        = get_user_meta_value($physician_id, '_clinic_name');
    $physician_office_number = get_user_meta_value($physician_id, 'office_number');
    
    if ($specimen_results) {
      foreach ($specimen_results as $specimen) {
        $report_type_histo = "";
        $report_type_pcr   = "";
        $histo_nail_pdf    = "";
        $pcr_report_pdf    = "";
        $nail_unit         = array();
        
       
  /*      $pending_report_count_sql   = "SELECT count(`id`) as number_of_incompleted_specimen FROM `wp_abd_specimen` WHERE `id` ='" . $specimen['id'] . "' AND `status` = '0' AND `physician_accepct` = '0' AND `qc_check`='0' AND `create_date` > '2017-03-27 23:59:59' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report`)";
        
        $pending_report_added_count = $this->BlankModel->customquery($pending_report_count_sql);
        $histo_pending_report_count += $pending_report_added_count[0]['number_of_incompleted_specimen'];*/
        
        
        $issued_report_count_sql   = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `specimen_id`=" . $specimen['id'];
        
        $issued_report_added_count = $this->BlankModel->customquery($issued_report_count_sql);
        $histo_report_added_count += $issued_report_added_count[0]['issued_report_count'];
        
        /**
		* 
		* @var Histo Data
		* 
		*/
        
		$pathology_sql = "SELECT `nail_pdf`, `nail_funagl_id`,`specimen_id`,`is_downloaded` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='" . $specimen['id'] . "'    ";
		$histo_pathology_results = $this->BlankModel->customquery($pathology_sql);
        
       /**
	   * 
	   * @var PCR Data
	   * 
	   */
        $pcr_sql     = "SELECT `id`, `accessioning_num`, `report_pdf_name`, `is_downloaded` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='" . $specimen['assessioning_num'] . "'";
        $pcr_results = $this->BlankModel->customquery($pcr_sql);
       
        $condition = "( `accessioning_num` ='" . $specimen['assessioning_num'] . "')";
        $num_of_rows  = get_nos_rows('wp_abd_generated_pcr_reports',$condition);
       
       
       if($pcr_results){
	   	  $total_pcr_report_count += $num_of_rows;
	   }
     
           
        $report_sql     = "SELECT `nail_unit` FROM `wp_abd_clinical_info`  WHERE `specimen_id` ='" . $specimen['id'] . "'";
        $report_results = $this->BlankModel->customquery($report_sql);
        
        if (!empty($report_results)) {
          $nail_unit = explode(",", $report_results[0]['nail_unit']);
        
        if ((in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) {
            $report_type_histo = "Histo";
          }
          
          if (in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit)) {
            $report_type_pcr = "PCR";
          }
        }   
        /**
         * 
         * @var / User Role Aps Sales Manager
         * 
         */
        
        if ($user_role == "aps_sales_manager" || $user_role == "data_entry_oparator") {
          
          /**
           * 
           * @var Histo Reports
           * 
           */
           
          if($histo_pathology_results) {
            $histo_report_status = "Histo Report Generated";            
          }
          
          else if(empty($histo_pathology_results) && (in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) {
            $histo_report_status = "Pending";
          }
          
          else if (empty($histo_pathology_results) && (!in_array("1", $nail_unit) || !in_array("2", $nail_unit) || !in_array("3", $nail_unit))) {
            $histo_report_status = "No Histo Ordered";
          }
          
          /**
           * 
           * @var PCR Reports
           * 
           */           
         
          if ($pcr_results) {
            $pcr_report_status = "PCR Report Generated";            
          } 
          else if (empty($pcr_results) && ( in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit) )) {
            $pcr_report_status = "Pending";
          }
          else if ( empty($pcr_results) && ( !in_array("4", $nail_unit) )) {
            $pcr_report_status = "No PCR Ordered";
          }
          
        }
        
        /**
         * 
         * @var / User Role Clinic and Partner
         * 
         */
        
        if ($user_role == "clinic" || $user_role == "partner") {
            $patient_name = $specimen['p_firstname'] . " " . $specimen['p_lastname'];
         
           /**
           * 
           * @var Histo Reports
           * 
           */
       
          if ($histo_pathology_results) {
            $histo_report_status = "Histo Report Generated";
            $histo_nail_pdf      = REPORT_PDF_URL . 'histo_report_pdf/' . $histo_pathology_results[0]['nail_pdf'];
          }
          
          else if (empty($histo_pathology_results) && (in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) {
            $histo_report_status = "Pending";
          }
          
          else {
            $histo_report_status = "No Histo Ordered";
          }
           /**
           * 
           * @var PCR Reports
           * 
           */           
                   
          if (!empty($pcr_results)) {
            $pcr_report_status = "PCR Report Generated";
            $pcr_report_pdf    = REPORT_PDF_URL . 'pcr_report_pdf/' . $pcr_results[0]['report_pdf_name'];
          }
          
          else if (empty($pcr_results) && (in_array("4", $nail_unit))) {
            $pcr_report_status = "Pending";
            
          }          
          else {
            $pcr_report_status = "No PCR Ordered";
          }
          
        }
        
        $specimen_data = array(
          'collection_date'    => date('m-d-Y', strtotime($specimen['collection_date'])),
          'create_date'        => date('m-d-Y', strtotime($specimen['date_received'])),
          'assessioning_num'   => $specimen['assessioning_num'],
          'report_type_histo'  => $report_type_histo,
          'report_type_pcr'    => $report_type_pcr,
          'histo_report_status'=> $histo_report_status,
          'pcr_report_status'  => $pcr_report_status,
          'histo_nail_pdf'     => $histo_nail_pdf,
          'pcr_report_pdf'     => $pcr_report_pdf,
          'patient_name'       => $patient_name
        );
        
        array_push($specimen_data_array, $specimen_data);
        $total_specimen_count++;
      }      
      
      /**
	  * 
	  * @var Histo Details
	  * 
	  */
     if($current_user_id == "76389" && $physician_id == "137"){
	   $extra_add_on = " AND `wp_abd_specimen`.`create_date` > '2018-06-12 23:59:59' ";
	 } 
     else{
	   $extra_add_on = " AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'";
	 }    
         
            
      $histo_clinic_info_sql = "SELECT count(`wp_abd_specimen`.`id`) AS `total_histo_reports`
								FROM wp_abd_specimen
								INNER JOIN wp_abd_clinical_info
								ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
								WHERE                                 
                                (`wp_abd_clinical_info`.`nail_unit` LIKE '%1%'
                                 OR `wp_abd_clinical_info`.`nail_unit` LIKE '%2%'
                                 OR `wp_abd_clinical_info`.`nail_unit` LIKE '%3%'
                                 OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%'
                                 )
								$extra_add_on
								AND `wp_abd_specimen`.`physician_id` = '" . $physician_id . "' 
								AND `wp_abd_specimen`.`status` = '0' 
								AND `wp_abd_specimen`.`physician_accepct` = '0' 
								AND `wp_abd_specimen`.`qc_check` = '0' ".$extra;
      
      $total_histo_count_result = $this->BlankModel->customquery($histo_clinic_info_sql);     
      $total_added_histo_count   = $total_histo_count_result[0]['total_histo_reports'];  
      $histo_pending_report_count= ($total_added_histo_count - $histo_report_added_count);
       
     /**
	  * 
	  * @var PCR Details
	  * 
	  */
      
     $pcr_clinic_info_sql = "SELECT count(`wp_abd_specimen`.`id`) AS `total_pcr_reports`
								FROM wp_abd_specimen
								INNER JOIN wp_abd_clinical_info
								ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
								WHERE 
								(   `wp_abd_clinical_info`.`nail_unit` LIKE '%4%'
								 OR `wp_abd_clinical_info`.`nail_unit` LIKE '%5%'
                                 OR `wp_abd_clinical_info`.`nail_unit` LIKE '%7%'
                                 )    
								$extra_add_on
								AND `wp_abd_specimen`.`physician_id` = '" . $physician_id . "' 
								AND `wp_abd_specimen`.`status` = '0' 
								AND `wp_abd_specimen`.`physician_accepct` = '0' 
								AND `wp_abd_specimen`.`qc_check` = '0' ".$extra;
      
      $total_pcr_count_result  = $this->BlankModel->customquery($pcr_clinic_info_sql);     
      $total_added_pcr_count   = $total_pcr_count_result[0]['total_pcr_reports'];  
      $pcr_pending_report      = ($total_added_pcr_count - $total_pcr_report_count);
      
      echo json_encode(array(
						        'status' => '1',
						        'physician_id' => $physician_id,
						        'physician_name' => $physician_name,
						        'physician_fax' => $physician_fax,
						        'physician_clinic' => $physician_clinic,
						        'physician_office_number' => $physician_office_number,
						        'total_specimen_count' => $total_specimen_count,
						        'total_histo_added' => $total_added_histo_count,
						        'histo_report_added_count' => $histo_report_added_count,
						        'histo_pending_report_count' => $histo_pending_report_count,
						        'pcr_pending_report' => $pcr_pending_report,
						        'total_pcr_reports_count' => $total_pcr_report_count,
						        'total_added_pcr_reports_count' => $total_added_pcr_count,
						        'specimens_data' => $specimen_data_array
      							));
      
    } 
    else {
      $no_data_found = "No Data Found";
      echo json_encode(array(
						        'status' => '0',
						        'physician_id' => $physician_id,
						        'physician_name' => $physician_name,
						        'physician_fax' => $physician_fax,
						        'physician_clinic' => $physician_clinic,
						        'physician_office_number' => $physician_office_number,
						        'no_specimen_data' => $no_data_found,
						        'total_specimen_count' => '0'
      						 	));
    }
  }




  function specimen_count_for_physicians(){
    $data = json_decode(file_get_contents('php://input'), true);
    $physician_specimen_data = array();
    if(!empty($data)){
      $physician_list = $data['physician_list'];
      $phy_from_date = $data['phy_from_date'];
      $phy_to_date = $data['phy_to_date'];

      $physician_specimen_count_sql = "SELECT date_format( str_to_date( `collection_date`,'%m/%d/%Y'),'%Y-%m-%d') as 'collection_date', COUNT(`id`) AS 'total_specimen' FROM `wp_abd_specimen` WHERE `physician_id` = '".$physician_list."' AND `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$phy_from_date."' AND '".$phy_to_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') ORDER BY COUNT(`wp_abd_specimen`.`id`) DESC";
  
    $physician_specimen_count_data = $this->BlankModel->customquery($physician_specimen_count_sql);

    if(!empty($physician_specimen_count_data)){
      $collection_date_arr = array();
      $spe_count_arr = array();
      foreach ($physician_specimen_count_data as $key => $value) {
        $collection_date = $value['collection_date'];
        $total_specimen = $value['total_specimen'];
        array_push($collection_date_arr,$collection_date);
        array_push($spe_count_arr,$total_specimen);
      }
    $cnt=0;
    while (strtotime($phy_from_date) <= strtotime($phy_to_date)) {
        if(in_array($phy_from_date,$collection_date_arr)){
          $new_collection_arr = array($phy_from_date,(Int)$spe_count_arr[array_search($phy_from_date,$collection_date_arr)]);
          array_push($physician_specimen_data,$new_collection_arr);
          $cnt++;
        }
        else{
          $blank_data = 0;
          $new_collection_arr = array($phy_from_date,(Int)$blank_data);
          array_push($physician_specimen_data,$new_collection_arr);
        }
      
      $phy_from_date = date ("Y-m-d", strtotime("+1 day", strtotime($phy_from_date)));
    }
      die(json_encode(array(
            'status' => '1',
            'physician_specimen_data' => $physician_specimen_data,
             )));
  }else{
    die(json_encode(array(
          'status' => '0',
          'physician_specimen_data' => "No data found.",
           )));
  } 
    }
    
  }
  
 }

?>