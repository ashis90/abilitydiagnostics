<?php
if(!defined('BASEPATH')) EXIT("No direct script access allowed");
$basepath = base_url("assets/");
?>
<html>
 <head>
 <script src="<?=$basepath?>js/fusioncharts.js"></script>
 <style>
 .raphael-group-23-creditgroup{
  display:none;	
 }
 </style>
 </head>

<body>
<div class="content-wrap">
  <div class="container">
  <div class="col-md-3 col-sm-4" style="background: linear-gradient(to bottom, #fefefe 0, #eee 100%);    box-shadow: 0 1px 3px 2px #dfdede;
    box-sizing: border-box;
    min-height: 55px;
    padding: 12px;">
    <div class="logo">
        <a href="javascript: void(0);" class="custom-logo-link" rel="home" itemprop="url">
            <img width="371" height="58" src="<?php echo $basepath; ?>frontend/main_images/logo.png" class="custom-logo" alt="Ability Diagnostics" itemprop="logo" srcset="<?php echo $basepath; ?>frontend/main_images/logo.png 371w, <?php echo $basepath; ?>frontend/main_images/logo.png 300w" sizes="(max-width: 371px) 85vw, 371px"></a></div>
</div> 
    <?php 

    $arrChartConfig = array(
        "chart" => array(
         "caption" => "Sample size Distribution",
         "exportEnabled" => "1",
         "xAxisName" => "Max Specimen Dimension",
         "yAxisName" => "No. Of Cases",
        )
    );

    $arrChartData = array(
        ["0-5", $first],
        ["6-10", $second],
        ["11-15", $third],
        ["16-20", $fourth],
        ["21-25", $fifth],
        ["26-30",  $sixth],
        ["31-n",  $seventh]
    );

    $arrLabelValueData = array();

    // Pushing labels and values
    for($i = 0; $i < count($arrChartData); $i++) {
        array_push($arrLabelValueData, array(
            "label" => $arrChartData[$i][0], "value" => $arrChartData[$i][1]
        ));
    }

    $arrChartConfig["data"] = $arrLabelValueData;

    // JSON Encode the data to retrieve the string containing the JSON representation of the data in the array.
    $jsonEncodedData = json_encode($arrChartConfig);

    // chart object
    $Chart = new FusionCharts("column2d", "MyFirstChart" , "400", "250", "chart-container", "json", $jsonEncodedData);

    // Render the chart
    $Chart->render();
    ?>

    <div style="text-align: center;margin-top: 25px;">
        <div id="chart-container"></div>
    </div>
    
  
  </div>
</div>
<link rel="stylesheet" href="<?php echo $basepath; ?>css/jquery-ui.min.css"/>
<script type="text/javascript" src="<?=$basepath?>js/jquery-ui.min.js">
</script>
</body>
</html>

