// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  //---------------- Live Url Start-------------------//
  // baseUrl: "https://www.abilitydiagnostics.com/abadmin/api/",
  // assetsUrl: "https://www.abilitydiagnostics.com/abadmin/"
  //---------------- Live Url End-------------------//

  //---------------- beta-2.0 Url Start-------------------//
   baseUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/api/",
   assetsUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/"
  //---------------- beta-2.0 Url End-------------------//
};
