<p style="padding-top:1px;"></p>

<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4; border-top:none;font-family:Arial, Helvetica, sans-serif;font-size: 12px;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;font-size: 12px;">
    <h4 style="font:700 12px Arial, Helvetica, sans-serif; color:#fff;margin:0;">Clinical Report</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 0 0;">
      <div style="width:31.3333%; float:left; margin:0; padding:1% 0 1% 1%;">
        <div style="max-width:80%; margin:0 auto;">
          <img src="{logo}" alt = "ability_logo"/>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:0;">
        <div class="comny_tag">
          <h4 style="font:700 15px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;">Ability Diagnostics, LLC</h4>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">858 South Auto Mall Road, Suite 102</p>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">American Fork, UT 84003</p>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:1%;">
        <div style="font:400 12px/14px Arial, Helvetica, sans-serif;color: #444;">
          <p style="margin-bottom:0;font-size: 12px;">Date of Report</p>
          <p style="margin:0;font-size: 12px;"><span style="display:block;">{date}</span></p>
        </div>
      </div>
      <div style="clear:both;"></div>
        <div style="border-bottom:1px dotted #8db3e2; margin-top:20px;"></div>
    </div>
   
  </div>
  <div style="padding:3px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
    <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif; color:#000;">Physician Name:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;text-transform:capitalize;">{pname}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Date Range:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{daterange}</div>
        </p>

      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
    <div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif;">Physician Address:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;">{paddress}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Total Specimens:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{count}</div>
          </p>
        </div>
      </div>
    </div>
  </div>
   <div class="clearfix"></div>             
              </div>           
            
            
                         
<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4;border-top:none;">        
                
 <div style="padding: 0 30px;padding-bottom:10px;"> 
 
<div style="margin-bottom:15px;width:50%;float:left;">
<h2 style="color: #000;font-size: 14px;margin-bottom:0;font-family:Arial, Helvetica, sans-serif;">
<span style="border-bottom: 2px solid #000;margin-top: 0;">Specimen List:</span>
</h2>
</div>
<div style="margin-bottom:15px;width:50%;float:right;">
<h2 style="font-size: 14px;margin-bottom:0;font-family:Arial, Helvetica, sans-serif;text-align:right;">
<span style="border-bottom: 2px solid #000;margin-top: 0;color:red;">Confidential</span>
</h2>
</div>

        <div style="width: 100%;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size: 12px; margin-top:5px;margin-bottom: 10px;">

        {table_data}     
        </div>

        <div>

          
                <div style="width:50%; float:left;text-align:center;"> <img src="{diagnosis}" alt = "ability_logo" style="display:block;margin:0 auto;height:300px;"/> </div>
         <div style="width:50%; float:left;text-align:center;"> <img src="{dimension}" alt = "ability_logo" style="display:block;margin:0 auto;height:280px; margin-left:20px;"/>

        <p style="font-size:10px;margin:0;margin-top:5px;font-family:Arial, Helvetica, sans-serif;text-align:left;margin-left:17px;">This graph shows the detection rate of fungus or yeast, based on the size of the nail specimen.</p>
         </div>
            

        </div>


  <div style="margin-left:0px;">
        <div>
        <h2 style="margin-bottom:15px;font-family:Arial, Helvetica, sans-serif;font-size: 14px;"><span style="border-bottom: 2px solid #000;margin-top: 0;">
       Comments:</span></h2>
        </div>
        <div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
          <p style="margin:0;margin-top:2px;font-size:11px;">
          {comment}
          </p>
        </div>
        </div> 
        
    
          



        </div>



        </div>
        

  
      

