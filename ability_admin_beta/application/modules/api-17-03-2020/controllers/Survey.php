<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');
class Survey extends MY_Controller {

    function __construct() {
        parent::__construct();
	date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {
    	
    }

    function send_survey_details(){
    	$data = json_decode(file_get_contents('php://input'), true);
    	if(!empty($data)){
    		$MailFormat = '<div class="cont_wrapperInner">
			<table class="table table-striped listing_table doodle display" cellspacing="0" width="100%">
			<thead>
			<tr>
			<th>Survey Question</th>
			<th>Rating </th>
			</tr> 
			</thead>

			<tbody> 
			<tr>
			<td align="left" valign="top"><strong> How satisfied are you with the support your received:</strong></td>
			<td align="center" valign="top">'.$data["option1"].'</td>
			</tr> 
			<tr>
			<td align="left" valign="top"><strong>How responsive were we to your inquiries?</strong></td>
			<td align="center" valign="top">'.$data["option2"].'</td>
			</tr>

			<tr>
			<td align="left" valign="top"><strong>How satisfied are you with the diagnosis reports (for physicians)?</strong></td>
			<td align="center" valign="top">'.$data["option3"].'</td>
			</tr> 
			<tr>
			<td align="left" valign="top"><strong>How satisfied are you with the turnaround time?</strong></td>
			<td align="center" valign="top">'.$data["option4"].'</td>
			</tr>

			<tr>
			<td align="left" valign="top"><strong> Overall, how was your experience?</strong></td>
			<td align="center" valign="top">'.$data["option5"].'</td>
			</tr> 
			</tbody>
			</table>

			<table>
				<tr>
				<td align="left" valign="top"><strong>Sender Email:</strong></td>
				<td align="center" valign="top">'.$data["email"].'</td>
				</tr>
				<tr>
				<td align="left" valign="top"><strong>Response:</strong></td>
				<td align="center" valign="top">'.$data["answer_survey"].'</td>
				</tr>
		    </table>
		     </div>';
			$message  = "Dear admin,<br>Someone filled out a survey. Results below:<br/><br/>";
			$message .= $MailFormat;
			$message .= "Thank You<br><br> ";
			$message .= "Ability Diagnostics Teams<br>";
			// $body     = mailBody($message);
			// //$to       = "support@abilitydiagnostics.com, trajanking@gmail.com";
			// $to       = "saptarshi@sleekinfosolutions.com";
			// $from     = "support@abilitydiagnostics.com";
			// $subject  = "Ability Diagnostics Survey Report-";
			
			// sendMail($to, $subject,$body,$from,"Ability Diagnostics");
			$to_email = $data["email"];
			$this->load->library('email');

			$this->email->from('support@abilitydiagnostics.com', 'Ability Diagnostics Teams');
	
			$this->email->to($to_email);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');
			$this->email->subject('Ability Diagnostics Survey Report-');
			$this->email->message($message);
			$this->email->set_mailtype("html");
			$this->email->send();
    		die( json_encode(array('status'=>'1')));
    	}
    }

 }
