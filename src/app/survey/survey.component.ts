import { Component, OnInit } from '@angular/core';
import {SurveyService } from '../survey.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  surveyForm: FormGroup;
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl+'assets/frontend/main_images/inner_banner.jpg';
  constructor(
    private http:HttpClient, 
    public itemsService:SurveyService,
    public route:Router,
    private toastr: ToastrService
  ) { 
    this.surveyForm = new FormGroup({        
      "option1" : new FormControl('4'),
      "option2" : new FormControl('4'), 
      "option3" : new FormControl('4'),  
      "option4" : new FormControl('4'), 
      "option5" : new FormControl('4'), 
      "email" : new FormControl(''), 
      "answer_survey" : new FormControl('',Validators.required),
      "submit" : new FormControl('Submit')
     });
  }

  ngOnInit() {
    // let shand = document.getElementsByClassName('middle_hdr') as HTMLCollectionOf<HTMLElement>;
                     
    // shand[0].style.display = "none";
    // let shand2 = document.getElementsByClassName('top_hdr') as HTMLCollectionOf<HTMLElement>;

    // shand2[0].style.display = "block"; 
    // let shand3 = document.getElementsByClassName('hdr_btm') as HTMLCollectionOf<HTMLElement>;

    // shand3[0].style.display = "block";    
    
    // let shand4 = document.getElementsByClassName('labdoctor_hdr') as HTMLCollectionOf<HTMLElement>;
    // shand4[0].style.display = "none";

    // let shand6 = document.getElementsByClassName('data_entry_hdr') as HTMLCollectionOf<HTMLElement>;                    
    // shand6[0].style.display = "none";
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.surveyForm.get(field).valid && (this.surveyForm.get(field).touched || this.surveyForm.get(field).dirty);
  }

  onSubmit()
    {
      let post_arr;
        if(this.surveyForm.status == "VALID")
        {   
          let fmData = Object.assign({}, this.surveyForm.value);
          this.itemsService.sendSurveyDetails(fmData).subscribe( data=> {
            post_arr = data;
            if(post_arr['status'] === '1'){
              this.toastr.info('Thank you for your feedback', 'Success', {
                timeOut: 3000
              });
            }
          });
        }
        else
        {
          this.validateAllFormFields(this.surveyForm);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {         //{1}
      Object.keys(formGroup.controls).forEach(field => {  //{2}
        const control = formGroup.get(field);             //{3}
        if (control instanceof FormControl) {             //{4}
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        //{5}
          this.validateAllFormFields(control);            //{6}
        }
      });
    }

}
