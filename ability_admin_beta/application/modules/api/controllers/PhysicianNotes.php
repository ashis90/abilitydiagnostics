<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class PhysicianNotes extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {
    }

   	function add_notes(){
   		$data=json_decode(file_get_contents('php://input'), true);

   		if(!empty($data)){
   			$insert = $this->db->insert('wp_abd_data_entry_notification', 
			array( 'dp_user_id' => $data['user_id'],'physician_id' => $data['physician_id'], 'notifications' => $data['notes']));

			$lastInsertId = $this->db->insert_id();

			if($lastInsertId){
				die(json_encode(array('status'=>'1')));
			}else{
				die(json_encode(array('status'=>'0')));
			}
   		}

   	}

   	function edit_notes(){
   		$data=json_decode(file_get_contents('php://input'), true);

   		if(!empty($data)){
   			$insert = $this->db->insert('wp_abd_data_entry_notification', 
			array( 'dp_user_id' => $data['user_id'],'physician_id' => $data['physician_id'], 'notifications' => $data['notes']));

			$lastInsertId = $this->db->insert_id();

			if($lastInsertId){
				die(json_encode(array('status'=>'1')));
			}else{
				die(json_encode(array('status'=>'0')));
			}
   		}

   	}

   	function notes_details(){
   		$data=json_decode(file_get_contents('php://input'), true);
   		$name ='';
//echo json_encode(array('status'=>$data));
   		if(!empty($data)){
   			$this->db->where('id',$data['note_id']);
   			$result = $this->db->get('wp_abd_data_entry_notification')->row_array();
   			if(!empty($result)){
   				$fname = get_user_meta( $result['physician_id'], 'first_name',true );
   				$lname = get_user_meta( $result['physician_id'],'last_name',true );
   				$name = $fname['meta_value'].' '.$lname['meta_value'];
   			}
   			
			if($result){
				die(json_encode(array('status'=>'1','note_details'=>$result,'physician_name'=>$name)));
			}else{
				die(json_encode(array('status'=>'0')));
			}
   		}

   	}

   	function update_notes(){
   		$data=json_decode(file_get_contents('php://input'), true);

   		if(!empty($data)){
   			// $this->db->where('id',$data['id']);
   			// $insert = $this->db->update('wp_abd_data_entry_notification', 
			// array( 'dp_user_id' => $data['user_id'],'physician_id' => $data['physician_id'], 'notifications' => $data['notes']));

			$up_data = array('dp_user_id' => $data['user_id'],'physician_id' => $data['physician_id'], 'notifications' => $data['notes']);
			$conditions1 = " ( `id` = '".$data['id']."')";	
			$usersedit = $this->BlankModel->editTableData('data_entry_notification', $up_data, $conditions1);

			if($usersedit){
				die(json_encode(array('status'=>'1')));
			}else{
				die(json_encode(array('status'=>'0')));
			}
   		}

   	}

   	function get_all_notes_details(){

      $this->db->order_by('id','DESC');
   		$notes_data = $this->db->get('wp_abd_data_entry_notification')->result_array();

   		if(!empty($notes_data)){
	   		for($i=0;$i<count($notes_data);$i++){
	   			$fname = get_user_meta( $notes_data[$i]['physician_id'], 'first_name',true );
   				$lname = get_user_meta( $notes_data[$i]['physician_id'],'last_name',true );
   				$name = $fname['meta_value'].' '.$lname['meta_value'];
   				$notes_data[$i]['physician_name'] = $name;
	   		}
				die(json_encode(array('status'=>'1','notes_data'=>$notes_data,'notes_count'=>count($notes_data))));
			}else{
				die(json_encode(array('status'=>'0')));
			}
   	}

   	function get_all_notes_data(){
   		$allNotesData = array(array());
   		$notification_det = "SELECT * FROM wp_abd_data_entry_notification GROUP BY `physician_id` ORDER BY `id` DESC";
   		$notes_data = $this->db->query($notification_det)->result_array();

   		if(!empty($notes_data)){
	   		for($i=0;$i<count($notes_data);$i++){
	   			$phy_id = $notes_data[$i]['physician_id'];
	   			$fname = get_user_meta( $phy_id, 'first_name',true );
				$lname = get_user_meta( $phy_id,'last_name',true );
				$name = $fname['meta_value'].' '.$lname['meta_value'];
	   			$notes_sql = "SELECT * FROM wp_abd_data_entry_notification WHERE `physician_id`=$phy_id ORDER BY `id` DESC";
	   			$notes_details = $this->db->query($notes_sql)->result_array();

	   			$allNotesData[$i]['physician_name'] = $name;
	   			$allNotesData[$i]['physician_id'] = $phy_id;

	   			for($j=0;$j<count($notes_details);$j++){
	   				$usr_id = $notes_details[$j]['dp_user_id'];
		   			$firstname = get_user_meta( $usr_id, 'first_name',true );
					$lastname = get_user_meta( $usr_id,'last_name',true );
					$usr_name = $firstname['meta_value'].' '.$lastname['meta_value'];
	   				$allNotesData[$i]['notes'][$j] = $notes_details[$j]['notifications'];
	   				$allNotesData[$i]['user'][$j] = $usr_name;
	   				$allNotesData[$i]['create_date'][$j] = date('jS F Y',strtotime( $notes_details[$j]['create_date']));
	   			}

	   		}
	   		die(json_encode(array('status'=>'1','all_notes_data'=>$allNotesData)));
	   	}else{
	   		die(json_encode(array('status'=>'0')));
	   	}

   	}

   	function delete_notes_data(){
   		$data=json_decode(file_get_contents('php://input'), true);

   		if(!empty($data)){
   			$note_id = $data['note_id'];
   			$note_sql = "DELETE FROM wp_abd_data_entry_notification WHERE `id`= $note_id";
   			$this->db->query($note_sql);

   			if($this->db->affected_rows() > 0){
   				die(json_encode(array('status'=>'1')));
   			}else{
   				die(json_encode(array('status'=>'0')));
   			}
   		}
   	}
}

?>