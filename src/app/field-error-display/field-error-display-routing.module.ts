import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FieldErrorDisplayComponent } from './field-error-display.component';

const routes: Routes = [
  { path: '', component: FieldErrorDisplayComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AboutRoutingModule {
  static components = [ FieldErrorDisplayComponent ];
}
