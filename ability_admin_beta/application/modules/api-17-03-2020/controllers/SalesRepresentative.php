<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class SalesRepresentative extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test login";
    }
	/**
	* 
	* @ Sales Representative
	*/

    function salesManagersList() {
    $data = json_decode(file_get_contents('php://input'), true);
    
    $current_user_id = $data['user_id'];
    $user_role = $data['user_role'];
   	$total_char_amt = 0;
   	$charged_amt_for_own =0;
    $curr_month = date('n');
    $curr_year  = date('Y');	
    $sales_reps_list_array = array();	
    $total_pa_amt = 0;	
    $paid_amt_for_own = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt = 0;
 	
	if( $user_role == "aps_sales_manager" ||  $user_role == "data_entry_oparator"){
		$sales_reps_sql = 'SELECT u1.ID FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
        OR m3.meta_value LIKE  "%team_regional_Sales%" ) LIMIT 0,10';
  	}
	elseif ( $user_role == "regional_manager" || $user_role == "sales_regional_manager"){
		$sales_reps_sql = 'SELECT u1.ID FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
		WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
        OR m3.meta_value LIKE  "%team_regional_Sales%")
        AND m4.meta_value = "'.$current_user_id.'" LIMIT 0,10';
	}
	
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);      
    $sales_reps_count = 0;  
   
    if($sales_reps){
    foreach($sales_reps as $sales_representative)
    {
	      $regional_manager_name = "";
	      $sales_id = $sales_representative['ID'];
     /**
	 * 
	 * @var Charged Amount 
	 * 
	 */
      $charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);
       if($charged_amount !="" )
       {
	        $amt_ch = "$".number_format($charged_amount, 2, '.', ',');
	        $short_amt_ch = $charged_amount;
       }
       else{
	        $amt_ch ="$00.00";
	        $short_amt_ch = 0;
        }
      /**
	  * 
	  * @var Paid Amount
	  * 
	  */
      $paid_amount = get_total_amt($sales_id, 'paidamt', $curr_month, $curr_year);
       if($paid_amount !="" )
       {
	       $amt_paid = "$".number_format($paid_amount, 2, '.', ',');
	       $short_amt_paid = $paid_amount;
       }
       else{
	       $amt_paid ="$00.00";
	       $short_amt_paid = 0;
       }       
                	
       $sales_reps_name = get_user_meta_value($sales_id, 'first_name').' '.get_user_meta_value($sales_id, 'last_name');      	
     
       $reg_mgr = get_user_meta_value($sales_id, 'assign_to');
     	
       if($reg_mgr != ""){
		  $team_mang_id = $reg_mgr; 
	      $regional_manager_name = get_user_meta_value($team_mang_id, 'first_name')." ".get_user_meta_value($team_mang_id, 'last_name');
	    }      

        $sales_reps_status =  get_user_meta_value($sales_id, '_status');
        $salesID = get_assign_user($sales_id);
        $physician_sql = "SELECT COUNT(*) AS total_physician FROM wp_abd_users AS u 
					        JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
					        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
					        LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
					        WHERE um2.meta_key = 'wp_abd_capabilities' 
					        AND um2.meta_value LIKE '%physician%' 
					        AND um3.meta_key = 'added_by' 
					        AND um3.meta_value IN($salesID)
					        AND (m1.meta_value = 'Active')";
					             
		$total_physician = $this->BlankModel->customquery($physician_sql);
		
	    $sales_reps_array_data = array(
								'user_id'           => $sales_id,
								'charged_amt'       => $amt_ch,
					            'paid_amt'          => $amt_paid,
					            'short_charged_amt' => (int)$short_amt_ch,
								'short_paid_amt'    => (int)$short_amt_paid,
								'tot_phy'           => $total_physician[0]['total_physician'],
								'short_tot_phy'     => (int)$total_physician[0]['total_physician'],
								'sales_reps_name'   => $sales_reps_name,
								'regional_name'     => $regional_manager_name,
								'u_status'          => $sales_reps_status );    
     $sales_reps_count ++;
     array_push($sales_reps_list_array,$sales_reps_array_data);
     }

     $last_data_entry_date = get_user_meta_value('66','last_commissions_entry_date',TRUE);

     if($sales_reps){
	  echo json_encode(array('status'=>'1', 'total_users' => $sales_reps_count, "sales_reps_data" => $sales_reps_list_array, 'last_data_entry_date' => $last_data_entry_date));
	 }
	 else{
	  echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
     }
    }
    else {
	  echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
    }
        
   }

  //////////////////////////// show total number of regional manager///////////////////////
  public function showNumOfSalesManager()
  {
    $data = json_decode(file_get_contents('php://input'), true); 
    $current_user_id = $data['user_id'];
    $user_role = $data['user_role'];
    $total_char_amt = 0;
   	$charged_amt_for_own = 0;
    $curr_month = date('n');
    $curr_year  = date('Y');	
    $total_pa_amt = 0;	
    $paid_amt_for_own = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt = 0;	

    if( $user_role == "aps_sales_manager" || $user_role == "data_entry_oparator"){
      $sales_reps_sql = "SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 		
          WHERE (m3.meta_value LIKE '%sales_regional_manager%'       
          OR m3.meta_value LIKE  '%sales%'  
          ) ";
      }
    elseif ( $user_role == "regional_manager" || $user_role == "sales_regional_manager"){
      $sales_reps_sql = "SELECT u1.ID FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
      WHERE (m3.meta_value LIKE '%sales_regional_manager%'         
            OR m3.meta_value LIKE  '%sales%'  
           )
          AND m4.meta_value = $current_user_id ";
      }
      $sales_reps = $this->BlankModel->customquery($sales_reps_sql);

      if($sales_reps) {
        foreach($sales_reps as $sales_representative)
        {
			$sales_id = $sales_representative['ID'];

			$charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);

			if($charged_amount)
			{
				$amt_ch = $charged_amount;
        $total_char_amt = $total_char_amt + $amt_ch;
			}
			else{
				$amt_ch = 0;
			}

			$paid_amount = get_total_amt($sales_id , 'paidamt', $curr_month, $curr_year);
			
      if($paid_amount)
			{
				$amt_paid = $paid_amount;
        $total_paid_amt = ($total_paid_amt + $amt_paid);
			}
			else{
				$amt_paid = 0;
			}

			$total_paid_amount = "$".number_format($total_paid_amt, 2, '.', ',');
			$total_charged_amount = "$".number_format($total_char_amt, 2, '.', ',');

        }
       
      }

      if(!empty($sales_reps) && isset($sales_reps)) {
        $total_sales = count($sales_reps);
        echo json_encode(array("status"=>'1', "total"=> $total_sales, 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount));
      } else {
        echo json_encode(array("status"=>'0', "total"=> '0'));
      }
  }

//////////////////////////// for load more ///////////////////////


  public function salesManagersListAll()
  {
    $data = json_decode(file_get_contents('php://input'), true);    
    $limit = 	$data['limit'];
    $current_user_id = $data['user_id'];
    $user_role = $data['user_role'];
   	$total_char_amt = 0;
   	$charged_amt_for_own = 0;
    $curr_month = date('n');
    $curr_year  = date('Y');   
    $sales_reps_list_array = array();	
    $total_pa_amt = 0;	
    $paid_amt_for_own = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt = 0;
    	
	$roles = array('sales_regional_manager', 'sales');
	
	if( $user_role == "aps_sales_manager" || $user_role == "data_entry_oparator"){
		$sales_reps_sql = "SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 		
        WHERE (m3.meta_value LIKE '%sales_regional_manager%'        
        OR m3.meta_value LIKE  '%sales%'  
        ) LIMIT $limit,10 ";
  	}
	elseif ( $user_role == "regional_manager" || $user_role == "sales_regional_manager"){
		$sales_reps_sql = "SELECT u1.ID FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
		WHERE (m3.meta_value LIKE '%sales_regional_manager%'         
        OR m3.meta_value LIKE  '%sales%'  
        )
        AND m4.meta_value = $current_user_id LIMIT $limit,10 ";
	}
	
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);     
    $sales_reps_count = 0;  
    foreach($sales_reps as $sales_representative)
    {
      $regional_manager_name = "";
      $sales_id = $sales_representative['ID'];
      $charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);
       if($charged_amount !="" )
       {
        $amt_ch = "$".number_format($charged_amount, 2, '.', ',');
        $short_amt_ch = $charged_amount;
       }
       else{
        $amt_ch ="$00.00";
        $short_amt_ch = 0;
        }
      $paid_amount = get_total_amt($sales_id , 'paidamt', $curr_month, $curr_year);
       if($paid_amount !="" )
       {
        $amt_paid = "$".number_format($paid_amount, 2, '.', ',');
        $short_amt_paid = $paid_amount;
       }
       else{
       $amt_paid ="$00.00";
       $short_amt_paid = 0;
       }
       
     	$sales_reps_fname = get_user_meta($sales_id, 'first_name');
     	$sales_reps_lname = get_user_meta($sales_id, 'last_name');     	
        $sales_reps_name = $sales_reps_fname['meta_value']." ".$sales_reps_lname['meta_value'];      	
     	$reg_mgr = get_user_meta($sales_id, 'assign_to');
     	
       if($reg_mgr['meta_value'] != ""){
		$team_mang_id = $reg_mgr['meta_value'];        
        $regional_manager_fname = get_user_meta($team_mang_id, 'first_name');
        $regional_manager_lname = get_user_meta($team_mang_id, 'last_name');        
        $regional_manager_name = $regional_manager_fname['meta_value']." ".$regional_manager_lname['meta_value'];
      	}      
        
        $sales_reps_status = get_user_meta_value($sales_id, '_status', TRUE);

        $salesID = get_assign_user($sales_id);

        $physician_sql = "SELECT COUNT(*) AS total_physician FROM wp_abd_users AS u 
        JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id 
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
        LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
        WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
        AND um2.meta_key = 'wp_abd_capabilities' 
        AND um2.meta_value LIKE '%physician%' 
        AND um3.meta_key = 'added_by' 
        AND um3.meta_value IN($salesID)  
        AND (m1.meta_value = 'Active')
        ORDER BY u.user_registered DESC";
     
        $total_physician = $this->BlankModel->customquery($physician_sql);
   		

	      $sales_reps_array_data = array(
			'user_id' => $sales_id,
			'charged_amt' => $amt_ch,
            'paid_amt' => $amt_paid,
            'short_charged_amt' => (int)$short_amt_ch,
			'short_paid_amt' => (int)$short_amt_paid,
			'tot_phy' => $total_physician[0]['total_physician'],
			'short_tot_phy' => (int)$total_physician[0]['total_physician'],
			'sales_reps_name' => $sales_reps_name,
			'regional_name' => $regional_manager_name,
			'u_status' => $sales_reps_status
			);
    
     $sales_reps_count ++;
     array_push($sales_reps_list_array,$sales_reps_array_data);
 }

     if($sales_reps){
	 echo json_encode(array('status'=>'1', 'total_users' => $sales_reps_count,"sales_reps_data" => $sales_reps_list_array));
	}
	else{
	 $no_data_found = "No Data Found";
	 echo json_encode(array('status'=>'0', "no_commission_data" => $no_data_found, 'total_users'=>'0'));
    }
  }

     
    /**
	* /
	* search sales reps list start
	*/
	
function get_monthly_sales_summary()
  {
   
  $data = json_decode(file_get_contents('php://input'), true);
  $curr_month = date('n');
  $curr_month =  $curr_month - 1;
  $curr_year  = date('Y');
  $prev_month_first_date = date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1));
  $prev_month_last_date = date("Y-m-d", mktime(0, 0, 0, date("m"), 0));
  $jd=gregoriantojd($curr_month,1,$curr_year);
  $current_month = jdmonthname($jd,1);
  $past_seven_days = date('Y-m-d', strtotime('-7 days'));
  $crnt_date = date('Y-m-d');
  $past_seven_days_specimen_arr = array();
	$user_id = $data['user_id'];
	$user_role = $data['user_role'];
  // $reg_user_Id = $data['reg_id'];
  $physician_specimen_count_data = array();
  $prev_month_total_specimen_count_data = '';
  $physician_ids_data='';

  if($user_role == 'aps_sales_manager' || $user_role == 'data_entry_oparator'){

    $physician_ids_sql = "SELECT `physician_id` AS 'physician_name', COUNT(`id`) AS 'total_specimen' FROM `wp_abd_specimen` WHERE status = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id` ORDER BY COUNT(`wp_abd_specimen`.`id`) DESC LIMIT 5";
    $physician_ids_data = $this->BlankModel->customquery($physician_ids_sql);
  
    if($physician_ids_data){
      for($i=0;$i<count($physician_ids_data);$i++){
       // echo(json_encode(array('status'=>$physician_ids_data[1]['physician_name'])));
       $total_specimen_NF = 0;
       $total_specimen_W = 0;
        $ph_id = $physician_ids_data[$i]['physician_name'];
        $physician_specimen_count_sql = "SELECT `physician_id` AS 'physician_name', COUNT(`id`) AS 'total_specimen',`test_type` FROM `wp_abd_specimen` WHERE `physician_id` =$ph_id AND  `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `test_type` ORDER BY COUNT(`wp_abd_specimen`.`id`) DESC LIMIT 5";
        $physician_spe_count_data = $this->BlankModel->customquery($physician_specimen_count_sql);
        $nw_spe_count_arr = array();
        if(array_key_exists("0", $physician_spe_count_data)){
          $nw_spe_count_arr['physician_name']=$physician_spe_count_data[0]['physician_name']; 
          if($physician_spe_count_data[0]['test_type'] == 'NF'){
            $total_specimen_NF =$physician_spe_count_data[0]['total_specimen'] != ''?$physician_spe_count_data[0]['total_specimen']:0;
           
          }else if($physician_spe_count_data[0]['test_type'] == 'W'){
            $total_specimen_W=$physician_spe_count_data[0]['total_specimen'] != ''?$physician_spe_count_data[0]['total_specimen']:0;
          }
          if(array_key_exists("1", $physician_spe_count_data)){
            if($physician_spe_count_data[1]['test_type'] == 'NF'){
              $total_specimen_NF=$physician_spe_count_data[1]['total_specimen'] != ''?$physician_spe_count_data[1]['total_specimen']:0;
            }else if($physician_spe_count_data[1]['test_type'] == 'W'){
              $total_specimen_W=$physician_spe_count_data[1]['total_specimen'] != ''?$physician_spe_count_data[1]['total_specimen']:0;
            }
          }

        }
        $nw_spe_count_arr['total_specimen_NF'] = $total_specimen_NF?$total_specimen_NF:0;
        $nw_spe_count_arr['total_specimen_W']= $total_specimen_W?$total_specimen_W:0;
         array_push($physician_specimen_count_data,$nw_spe_count_arr);
      }
      //die(json_encode(array('status'=>$physician_specimen_count_data)));
    }
    $v=0;
    foreach ($physician_specimen_count_data as $key => $value) {
      $physician_id_in_loop = $value['physician_name'];
      $physician_specimen_count_data[$v]['physician_name'] = get_user_meta_value($physician_id_in_loop, 'first_name', TRUE) . " " . get_user_meta_value($physician_id_in_loop, 'last_name', TRUE);
      $v++;
    }
  }else if($user_role == 'sales'){
    $sql = "SELECT spe.`physician_id` AS 'physician_name', COUNT(spe.`id`) AS 'total_specimen' FROM `wp_abd_specimen` spe

    INNER JOIN (SELECT ID FROM wp_abd_users 
    INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
    INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 

    WHERE t1.meta_key ='wp_abd_capabilities' AND t2.meta_key ='added_by' AND (
    t1.meta_value LIKE '%physician%' 
    AND  t2.meta_value IN ('".$user_id."') 
     )) phycians ON spe.`physician_id` = phycians.ID WHERE spe.status = '0' AND spe.`physician_accepct` = '0' AND spe.`qc_check` = '0' AND date_format( str_to_date( spe.`collection_date`,'%m\/%d\/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND spe.`create_date` > '2017-03-27 23:59:59' GROUP BY spe.`physician_id` ORDER BY COUNT(spe.`id`) DESC LIMIT 5";
      $physician_ids_data = $this->BlankModel->customquery($sql);

      if($physician_ids_data){
        for($i=0;$i<count($physician_ids_data);$i++){
         // echo(json_encode(array('status'=>$physician_ids_data[1]['physician_name'])));
          $ph_id = $physician_ids_data[$i]['physician_name'];
          $total_specimen_NF = 0;
          $total_specimen_W = 0;
          $physician_specimen_count_sql = "SELECT `physician_id` AS 'physician_name', COUNT(`id`) AS 'total_specimen',`test_type` FROM `wp_abd_specimen` WHERE `physician_id` =$ph_id AND  `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `test_type` ORDER BY COUNT(`wp_abd_specimen`.`id`) DESC LIMIT 5";
          $physician_spe_count_data = $this->BlankModel->customquery($physician_specimen_count_sql);
          //  $nw_spe_count_arr = array('physician_name'=>$physician_spe_count_data[0]['physician_name'],'total_specimen_NF'=>array_key_exists("0", $physician_spe_count_data)?$physician_spe_count_data[0]['total_specimen']:0,'total_specimen_W'=>array_key_exists("1", $physician_spe_count_data)?$physician_spe_count_data[1]['total_specimen']:0);
          $nw_spe_count_arr = array();
          if(array_key_exists("0", $physician_spe_count_data)){
            $nw_spe_count_arr['physician_name']=$physician_spe_count_data[0]['physician_name']; 
            if($physician_spe_count_data[0]['test_type'] == 'NF'){
              $total_specimen_NF =$physician_spe_count_data[0]['total_specimen'] != ''?$physician_spe_count_data[0]['total_specimen']:0;
             
            }else if($physician_spe_count_data[0]['test_type'] == 'W'){
              $total_specimen_W=$physician_spe_count_data[0]['total_specimen'] != ''?$physician_spe_count_data[0]['total_specimen']:0;
            }
            if(array_key_exists("1", $physician_spe_count_data)){
              if($physician_spe_count_data[1]['test_type'] == 'NF'){
                $total_specimen_NF=$physician_spe_count_data[1]['total_specimen'] != ''?$physician_spe_count_data[1]['total_specimen']:0;
              }else if($physician_spe_count_data[1]['test_type'] == 'W'){
                $total_specimen_W=$physician_spe_count_data[1]['total_specimen'] != ''?$physician_spe_count_data[1]['total_specimen']:0;
              }
            }
  
          }
          $nw_spe_count_arr['total_specimen_NF'] = $total_specimen_NF?$total_specimen_NF:0;
          $nw_spe_count_arr['total_specimen_W']= $total_specimen_W?$total_specimen_W:0;
           array_push($physician_specimen_count_data,$nw_spe_count_arr);
        }
        //die(json_encode(array('status'=>$physician_specimen_count_data)));
      }
      //die(json_encode(array('status'=>$physician_specimen_count_data)));
      $v=0;
      foreach ($physician_specimen_count_data as $key => $value) {
        $physician_id_in_loop = $value['physician_name'];
        $physician_specimen_count_data[$v]['physician_name'] = get_user_meta_value($physician_id_in_loop, 'first_name', TRUE) . " " . get_user_meta_value($physician_id_in_loop, 'last_name', TRUE);
        $v++;
      }
  }

  if($user_role == 'aps_sales_manager' || $user_role == 'data_entry_oparator'){
    $prev_month_total_specimen_count_sql = "SELECT `test_type`, COUNT(`id`) AS total_specimen FROM `wp_abd_specimen` WHERE status = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `test_type`";
    $prev_month_total_specimen_count_data = $this->BlankModel->customquery($prev_month_total_specimen_count_sql);
  }else if($user_role == 'sales'){
    $prev_month_total_specimen_count_sql = "SELECT COUNT(spe.`id`) AS 'total_specimen' FROM `wp_abd_specimen` spe
    INNER JOIN (SELECT ID FROM wp_abd_users 
    INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
    INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 

    WHERE t1.meta_key ='wp_abd_capabilities' AND t2.meta_key ='added_by' AND (
    t1.meta_value LIKE '%physician%' 
    AND  t2.meta_value IN ('".$user_id."') 
     )) phycians ON spe.`physician_id` = phycians.ID WHERE spe.status = '0' AND spe.`physician_accepct` = '0' AND spe.`qc_check` = '0' AND date_format( str_to_date( spe.`collection_date`,'%m\/%d\/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND spe.`create_date` > '2017-03-27 23:59:59' GROUP BY `test_type`";
    $prev_month_total_specimen_count_data = $this->BlankModel->customquery($prev_month_total_specimen_count_sql);

   
  }
  if($user_role == 'aps_sales_manager' || $user_role == 'data_entry_oparator'){
    $past_seven_days_specimen_sql = "SELECT  `qb`.dy as `collection_date` , count(`spe`.`id`) AS 'total_specimen' FROM `wp_abd_specimen` spe RIGHT join (
      select curdate() as dy union
      select DATE_SUB(curdate(), INTERVAL 1 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 2 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 3 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 4 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 5 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 6 day) as `dy` 
      ) as qb 
  ON 
    `qb`.`dy` = date_format( str_to_date( `spe`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    AND date_format( str_to_date( `spe`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') > DATE_SUB(curdate(), INTERVAL 7 day) 
    AND `spe`.`status` = '0'
    AND `spe`.`physician_accepct` = '0' 
    AND `spe`.`qc_check` = '0' 
    GROUP BY `qb`.`dy`
    ORDER BY `qb`.`dy` ASC";
    $past_seven_days_specimen_data = $this->BlankModel->customquery($past_seven_days_specimen_sql);
    foreach ($past_seven_days_specimen_data as $key => $value) {
      $collection_date = date('Y-m-d',strtotime($value['collection_date']));
      $new_collection_arr = array($collection_date,(Int)$value['total_specimen']);
      array_push($past_seven_days_specimen_arr,$new_collection_arr);
    }  
  }else if($user_role == 'sales'){

    $get_physician_by_sales_sql = "SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = 'wp_abd_capabilities' AND t2.meta_key = 'added_by' AND (
      t1.meta_value LIKE '%physician%'
      AND  t2.meta_value IN ('". $user_id ."')
      )";
   $get_physician_by_sales_data = $this->BlankModel->customquery($get_physician_by_sales_sql);
   $physician_ids = implode(',',array_column($get_physician_by_sales_data, 'ID'));
    if($physician_ids){
    $past_seven_days_specimen_sql = "SELECT  `qb`.dy as `collection_date` , count(`spe`.`id`) AS 'total_specimen' FROM `wp_abd_specimen` spe RIGHT join (
      select curdate() as dy union
      select DATE_SUB(curdate(), INTERVAL 1 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 2 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 3 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 4 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 5 day) as `dy` union
      select DATE_SUB(curdate(), INTERVAL 6 day) as `dy` 
      ) as qb 
  ON 
    `qb`.`dy` = date_format( str_to_date( `spe`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') 
    AND date_format( str_to_date( `spe`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') > DATE_SUB(curdate(), INTERVAL 7 day) 
    AND `spe`.`status` = '0'
    AND `spe`.`physician_accepct` = '0' 
    AND `spe`.`qc_check` = '0' 
    AND `spe`.`physician_id` IN ($physician_ids)
    GROUP BY `qb`.`dy`
    ORDER BY `qb`.`dy` ASC";
     
    $past_seven_days_specimen_data = $this->BlankModel->customquery($past_seven_days_specimen_sql);
    foreach ($past_seven_days_specimen_data as $key => $value) {
      $collection_date = date('Y-m-d',strtotime($value['collection_date']));
      $new_collection_arr = array($collection_date,(Int)$value['total_specimen']);
      array_push($past_seven_days_specimen_arr,$new_collection_arr);
    }
  }
}

  $sales_representative_data = $this->sales_representative_data();
  if($user_role == 'aps_sales_manager' || $user_role == 'data_entry_oparator'){
    $physician_data= $this->physician_data();
  }else if($user_role == 'sales'){
    $physician_data= $this->physician_data($user_id);
  }

  
  if(!empty($data['sales_reps_id']))
  {
    $sales_reps_id = $data['sales_reps_id'];
  }
  
	// if($data['from_month']!='') {
	//    $curr_month =  $data['from_month'];
	// }
	// if($data['from_year']!='') {
	//    $curr_year  =  $data['from_year'];
	// }

	// $status = $data['status'];
	// $search_value = $data['search_value'];
	// $state = $data['state'];
	$total_char_amt = 0;
	$charged_amt_for_own = 0;
	$sales_reps_list_array = array();	
	$new_data_set = array();	
	$total_pa_amt = 0;	
	$paid_amt_for_own = 0;	
	$total_charged_amt = 0;	
	$total_paid_amt = 0;	

    $firstJoin = "SELECT `ID` FROM wp_abd_users u1 
    JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') ";

    $whererole = 'WHERE (m3.meta_value LIKE "%sales_regional_manager%"
        OR m3.meta_value LIKE  "%team_sales_manager%"
        OR m3.meta_value LIKE  "%sales%"
        OR m3.meta_value LIKE  "%team_regional_Sales%")';


  if(( $user_role == "aps_sales_manager" || $user_role == "sales" || $user_role == 'data_entry_oparator')){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')
		JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
		LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')		
	    $whererole";    
  
  }
  if (isset($sales_reps_sql)) {
  
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);      
      
    $sales_reps_count = 0;  
    foreach($sales_reps as $sales_representative)
    {
    $regional_manager_name = "";
    $total_specimen_count_NF=0;
    $total_specimen_count_W=0;
		$sales_id = $sales_representative['ID'];
		$charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);
		if($charged_amount !="" )
		{
		$amt_ch = "$".number_format($charged_amount, 2, '.', ',');
		$short_amt_ch =$charged_amount;
		}
		else{
		$amt_ch ="$00.00";
		$short_amt_ch = 0;
		}
		$paid_amount = get_total_amt($sales_id , 'paidamt', $curr_month, $curr_year);
		if($paid_amount !="" )
		{
		$amt_paid = "$".number_format($paid_amount, 2, '.', ',');
		$short_amt_paid = $paid_amount;
		}
		else{
		$amt_paid ="$00.00";
		$short_amt_paid = 0;
		}
 	
		$sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE)." ".get_user_meta_value($sales_id, 'last_name', TRUE);
		$reg_mgr = get_user_meta_value($sales_id, 'assign_to', TRUE);

		if($reg_mgr != ""){      
		$regional_manager_name = get_user_meta_value($reg_mgr, 'first_name', TRUE)." ".get_user_meta_value($reg_mgr, 'last_name', TRUE);
		}      

		$sales_reps_status =  get_user_meta_value($sales_id, '_status', TRUE);

		$physician_sql = "SELECT u.ID AS ID FROM wp_abd_users AS u 
		JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
		LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id 
		LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
		LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
		WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
		AND um2.meta_key = 'wp_abd_capabilities' 
		AND um2.meta_value LIKE '%physician%' 
		AND um3.meta_key = 'added_by' 
		AND um3.meta_value LIKE '".$sales_id."' 
		AND (m1.meta_value = 'Active') 
        ORDER BY u.user_registered DESC";

    $total_physician = $this->BlankModel->customquery($physician_sql);
    foreach($total_physician as $key=>$value){
      $ph_id = $value['ID'];
      $specimen_sql = "SELECT COUNT(`wp_abd_specimen`.`id`) AS total_orders  FROM `wp_abd_specimen` WHERE physician_id = $ph_id AND status = '0' AND physician_accepct = '0' AND qc_check = '0' AND date_format( str_to_date( `collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$prev_month_first_date."' AND '".$prev_month_last_date."' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `test_type`";
      $total_specimen = $this->BlankModel->customquery($specimen_sql);
      if($total_specimen){
        $total_specimen_count_NF +=array_key_exists("0", $total_specimen)?$total_specimen[0]['total_orders']:0;
        $total_specimen_count_W +=array_key_exists("1", $total_specimen)?$total_specimen[1]['total_orders']:0;
      }
    }
		   if (is_numeric($charged_amount)) {
		      $total_char_amt += $charged_amount;
		    } 
		   if($charged_amt_for_own != 0 )
		    {
		      $total_charged_amt = $charged_amt_for_own+$total_char_amt;
		    }
		   else
		    {
		     $total_charged_amt = $total_char_amt;
		    }
		   
		   if(is_numeric($paid_amount)) {
			 $total_pa_amt += $paid_amount; 
			}
		   if($paid_amt_for_own !=0 )
			{
			 $total_paid_amt = $paid_amt_for_own+$total_pa_amt;
			}
		   else
			{
			 $total_paid_amt = $total_pa_amt; 
			}
		   
		    $total_paid_amount = "$".number_format($total_paid_amt, 2, '.', ',');
		    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');

      if($amt_ch !='$00.00'){
        $sales_reps_array_data = array(
          'user_id' => $sales_id,
          'charged_amt' => $amt_ch,
          'paid_amt' => $amt_paid,
          'short_charged_amt' => (int)$short_amt_ch,
          'short_paid_amt' => (int)$short_amt_paid,
          'tot_phy' => count($total_physician),
          'short_tot_phy' => count($total_physician),
          'sales_reps_name' => $sales_reps_name,
          'regional_name' => $regional_manager_name,
          'u_status' => $sales_reps_status,
          'total_specimen_NF'=>$total_specimen_count_NF,
          'total_specimen_W'=>$total_specimen_count_W
          );
    
        $sales_reps_count ++;
            array_push($sales_reps_list_array,$sales_reps_array_data);
         }
      }
      //die(json_encode(array('status'=>$sales_reps_list_array)));
      $sort_arr = $this->array_sort($sales_reps_list_array, 'short_charged_amt', SORT_DESC);
      //die(json_encode(array('status'=>$sort_arr)));
      $srt_count=0;
      foreach($sort_arr as $arr){
        if($srt_count<5){
          array_push($new_data_set,$arr);
          $srt_count++;
        }
        
      }
   if($sales_reps || $physician_specimen_count_data){
    
   echo json_encode(array('status'=> '1', 'total_users' => $sales_reps_count, 'total_paid_amt' => $total_paid_amount, 'total_charged_amt'=> $total_charged_amount,"sales_reps_data" => $new_data_set,'physician_specimen_count_data'=>$physician_specimen_count_data,'prev_month_total_specimen_count_data'=>$prev_month_total_specimen_count_data,'current_month'=>$current_month,'past_seven_days_specimen_data'=>$past_seven_days_specimen_arr,
  'sales_representative_data'=>$sales_representative_data,
  'physician_data'=>$physician_data));
	}
	else{
	 echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
    }
   
  } 

  else {
  
    echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
  
   }
}






  public function physician_data($user_id=''){
    $sql ='';
    if($user_id !=''){
      $sql = 'SELECT ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id
      INNER JOIN wp_abd_usermeta as t3 ON wp_abd_users.ID = t3.user_id  
      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'_status\' AND t3.meta_key = \'added_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%physician%\' ';
      $sql .= 'AND  t2.meta_value LIKE \'%Active%\' ';
      $sql .= 'AND  t3.meta_value IN (' . $user_id . ') ';
      $sql .= ') ORDER BY wp_abd_users.user_registered DESC';
    }else{
      $sql = 'SELECT ID FROM wp_abd_users 
      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'_status\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%physician%\' ';
      $sql .= 'AND  t2.meta_value LIKE \'%Active%\' ';
      $sql .= ') ORDER BY wp_abd_users.user_registered DESC';
    }
      $physicians_list = $this->BlankModel->customquery($sql);
      $assign_physician_name = array();
    foreach ($physicians_list as $phy) {
      $user_id      = $phy['ID'];
      $phy_first_name = get_user_meta($user_id, 'first_name');
      $phy_last_name  = get_user_meta($user_id, 'last_name');
      $phy_fname      = $phy_first_name['meta_value'];
      $phy_lname      = $phy_last_name['meta_value'];
      $user['physician_name'] = $phy_fname . ' ' . $phy_lname;
      $user['physician_id']   = $user_id;
      array_push($assign_physician_name, $user);
    }
      return $assign_physician_name;
  }


  public function sales_representative_data()
  {
    // sales rep list

      $role_sales_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND (um2.meta_value LIKE '%sales%' OR um2.meta_value LIKE '%sales_regional_manager%' OR um2.meta_value LIKE '%team_regional_Sales%' OR um2.meta_value LIKE '%team_sales_manager%' OR um2.meta_value LIKE '%aps_sales_manager%') ORDER BY u.user_nicename ASC ";
    
    $role_sales_all    = $this->BlankModel->customquery($role_sales_all_que);
    $assign_sales_name = array();
    foreach ($role_sales_all as $sales) {
      $user_id      = $sales['ID'];
      $s_first_name = get_user_meta($user_id, 'first_name');
      $s_last_name  = get_user_meta($user_id, 'last_name');
      $s_fname      = $s_first_name['meta_value'];
      $s_lname      = $s_last_name['meta_value'];
      $user['name'] = $s_fname . ' ' . $s_lname;
      $user['id']   = $user_id;
      array_push($assign_sales_name, $user);
    }
    return $assign_sales_name;
    
  }
  
  function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
            arsort($sortable_array);
            
                
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
  function search_sales_list()
  {
   
	$data = json_decode(file_get_contents('php://input'), true);
	$curr_month = date('n');
	$curr_year  = date('Y');
	$current_user_id = $data['user_id'];
	$user_role = $data['user_role'];
    $reg_user_Id = $data['reg_id'];
  
   if(!empty($data['sales_reps_id']))
    {
      $sales_reps_id = $data['sales_reps_id'];
    }
  
	if($data['from_month']!='') {
	   $curr_month =  $data['from_month'];
	}
	if($data['from_year']!='') {
	   $curr_year  =  $data['from_year'];
	}

	$status = $data['status'];
	$search_value = $data['search_value'];
	$state = $data['state'];
	$total_char_amt = 0;
	$charged_amt_for_own = 0;
	$sales_reps_list_array = array();	
	$total_pa_amt = 0;	
	$paid_amt_for_own = 0;	
	$total_charged_amt = 0;	
	$total_paid_amt = 0;	

   $firstJoin = "SELECT `ID` FROM wp_abd_users u1 
    JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') ";

   $whererole = 'WHERE (m3.meta_value LIKE "%sales_regional_manager%"
        OR m3.meta_value LIKE  "%team_sales_manager%"
        OR m3.meta_value LIKE  "%sales%"
        OR m3.meta_value LIKE  "%team_regional_Sales%")';


  if(( $user_role == "aps_sales_manager" || $user_role == "data_entry_oparator") && $reg_user_Id == ""){

  if(!empty($search_value) && !empty($status) && !empty($state) ){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')
		JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
		LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')		
		
	    $whererole        
        AND( m6.meta_value LIKE '%" . $search_value . "%'
			OR m7.meta_value  LIKE '%". $search_value . "%'  
			OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
			OR u1.display_name LIKE '%".trim($search_value)."%' )
	    AND m5.meta_value = '".$status."'
	    AND (t5.meta_value LIKE '%".$state."%'
	      OR t4.meta_value LIKE '%".$state."%')";
     
    } 
    
     elseif (!empty($state) && empty($status) ) {
      $sales_reps_sql = "$firstJoin	
	    JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
		LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')		
	    $whererole      
        AND (t5.meta_value LIKE '%".$state."%'
	      OR t4.meta_value LIKE '%".$state."%')";
    }
    
     elseif (!empty($status) && !empty($state) && empty($search_value) ){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status')
        JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
		LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
        $whererole 
        AND m4.meta_value ='".$status."'
         AND (t5.meta_value LIKE '%".$state."%'
	       OR t4.meta_value LIKE '%".$state."%')";
    } 
    
     elseif(!empty($search_value) && empty($status) && !empty($state)){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')
		JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
		LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')		
	    $whererole        
        AND( m6.meta_value LIKE '%" . $search_value . "%'
			OR m7.meta_value  LIKE '%". $search_value . "%'  
			OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
			OR u1.display_name LIKE '%".trim($search_value)."%' )
	    AND (t5.meta_value LIKE '%".$state."%'
	      OR t4.meta_value LIKE '%".$state."%')";     
    } 
               
     elseif (!empty($status) && !empty($sales_reps_id)){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status')
        JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = 'assign_to')		
        $whererole AND m4.meta_value ='".$status."' AND m5.meta_value = $sales_reps_id";

     } 
    
     elseif (!empty($status) && empty($sales_reps_id)){

      $sales_reps_sql = "$firstJoin	
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status')
  
        $whererole AND m4.meta_value ='".$status."'";
     } 
     
     elseif(!empty($search_value) && empty($status) && empty($state) ){
   
      $sales_reps_sql = "$firstJoin		
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')		
	    $whererole        
        AND(m6.meta_value LIKE '%" . $search_value . "%'
        OR m7.meta_value LIKE  '%" . $search_value . "%'  
        OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
		OR u1.display_name LIKE '%".trim($search_value)."%')";

     } 
    
     else {
       $sales_reps_sql = "$firstJoin $whererole ";
     }
  
  }
 
  elseif ( ($user_role == "regional_manager" || $user_role == "sales_regional_manager") && $reg_user_Id == "" ){

    if(!empty($status) && !empty($search_value)  && !empty($state) ){
      $sales_reps_sql = "$firstJoin 
							JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to')
							JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status') 
							JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
							JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')	
							JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
							LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
						 $whererole						
							AND( m6.meta_value LIKE '%" . $search_value . "%'
							OR m7.meta_value  LIKE '%". $search_value . "%'  
							OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
							OR u1.display_name LIKE '%".trim($search_value)."%' )
							AND m4.meta_value = '".$current_user_id."'  
							AND m5.meta_value = '".$status."'
							AND (t5.meta_value LIKE '%".$state."%'
							OR t4.meta_value LIKE '%".$state."%')";	
    }    
    
    elseif (!empty($status) && !empty($state) && empty($search_value) ){

	  $sales_reps_sql = "$firstJoin
							JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
							JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')
							JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
							LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
						 $whererole
							AND m4.meta_value = $current_user_id 
							AND m5.meta_value ='".$status."'
							AND (t5.meta_value LIKE '%".$state."%'
	                        OR t4.meta_value LIKE '%".$state."%')";

    } 
    
    else if(!empty($search_value) && empty($status) && empty($state) ){

      $sales_reps_sql = "$firstJoin 
							JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to')
							JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
							JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')						
						$whererole						
							AND( m6.meta_value LIKE '%" . $search_value ."%'
							OR m7.meta_value  LIKE '%". $search_value ."%'  
							OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
							OR u1.display_name LIKE '%".trim($search_value)."%' )
							AND m4.meta_value = '".$current_user_id."'"; 						

    }   
      elseif ($state != '' && empty($status) ) {

      $sales_reps_sql = "$firstJoin
							JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
					
							JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
							LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
						 $whererole
							AND m4.meta_value = $current_user_id 
						
							AND (t5.meta_value LIKE '%".$state."%'
	                        OR t4.meta_value LIKE '%".$state."%')";			

    }    
    else {
       $sales_reps_sql = "$firstJoin
						JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
						$whererole
						AND m4.meta_value = $current_user_id ";
     }

	}
  
  elseif ($reg_user_Id != ""){

    if (!empty($status) && !empty($search_value) && empty($state)){
		$sales_reps_sql = "$firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
			JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')		
			JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
			JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')			
		$whererole     
	        AND m4.meta_value = $reg_user_Id
	        AND m5.meta_value = '".$status."'              
			AND( m6.meta_value LIKE '%" . $search_value . "%'
			OR m7.meta_value  LIKE '%". $search_value . "%'  
			OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
			OR u1.display_name LIKE '%".trim($search_value)."%' )"; 
        
     } 
     elseif (!empty($status) && !empty($search_value) && !empty($state)){
		$sales_reps_sql = "$firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
			JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')		
			JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
			JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')
			JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
			LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')				
		$whererole     
	        AND m4.meta_value = $reg_user_Id
	        AND m5.meta_value = '".$status."'              
			AND( m6.meta_value LIKE '%" . $search_value . "%'
			OR m7.meta_value  LIKE '%". $search_value . "%'  
			OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
			OR u1.display_name LIKE '%".trim($search_value)."%' )
			AND (t5.meta_value LIKE '%".$state."%'
			OR t4.meta_value LIKE '%".$state."%')"; 
        
     }
      
    elseif (!empty($status) && empty($search_value) && !empty($state)){
		$sales_reps_sql = "$firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
			JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status')				
			JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
			LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
		$whererole     
	        AND m4.meta_value = $reg_user_Id
	        AND m5.meta_value = '".$status."' 
			AND (t5.meta_value LIKE '%".$state."%'
			OR t4.meta_value LIKE '%".$state."%')"; 
        
     }
         
     elseif (empty($status) && empty($search_value) && !empty($state)){
		$sales_reps_sql = "$firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 				
			JOIN wp_abd_usermeta t4 ON (t4.user_id = u1.ID AND t4.meta_key = '_address')		
			LEFT JOIN wp_abd_usermeta t5 ON (t5.user_id = u1.ID AND t5.meta_key = 'state')			
		$whererole     
	        AND m4.meta_value = $reg_user_Id
			AND (t5.meta_value LIKE '%".$state."%'
			OR t4.meta_value LIKE '%".$state."%')"; 
        
     }
        
    elseif(!empty($search_value) && empty($status)){
		$sales_reps_sql = "$firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
			JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'first_name')	
			JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = 'last_name')
		$whererole
			AND( m6.meta_value LIKE '%" . $search_value . "%'
			OR m7.meta_value  LIKE '%". $search_value . "%'  
			OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '',$search_value))."%'
			OR u1.display_name LIKE '%".trim($search_value)."%' )
			AND m4.meta_value = $reg_user_Id ";

    } 
    elseif (!empty($status) && empty($search_value) ) {
		$sales_reps_sql = " $firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to') 
			JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_status') 
		$whererole
			AND m4.meta_value = $reg_user_Id 
			AND m5.meta_value = '".$status."'";
    }
    else{
		$sales_reps_sql = " $firstJoin
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'assign_to')
		$whererole
			AND m4.meta_value = $reg_user_Id ";
    }
  
  }
    
  
  if (isset($sales_reps_sql)) {
  
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);      
       
    if($reg_user_Id != "")	
   	{
      $reg_user_role = get_user_role($reg_user_Id);
      if($reg_user_role == "team_regional_Sales"){
         $reg_Sales_reps = array( 'ID' => $reg_user_Id);
         array_push($sales_reps, $reg_Sales_reps);
        }
     if($reg_user_role == 'sales_regional_manager' ) {
        $reg_Sales_reps = array( 'ID' => $reg_user_Id);
        array_push($sales_reps, $reg_Sales_reps);
       }
 	 }
      
    $sales_reps_count = 0;  
    foreach($sales_reps as $sales_representative)
    {
		$regional_manager_name = "";
		$sales_id = $sales_representative['ID'];
		$charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);
		if($charged_amount !="" )
		{
		$amt_ch = "$".number_format($charged_amount, 2, '.', ',');
		$short_amt_ch =$charged_amount;
		}
		else{
		$amt_ch ="$00.00";
		$short_amt_ch = 0;
		}
		$paid_amount = get_total_amt($sales_id , 'paidamt', $curr_month, $curr_year);
		if($paid_amount !="" )
		{
		$amt_paid = "$".number_format($paid_amount, 2, '.', ',');
		$short_amt_paid = $paid_amount;
		}
		else{
		$amt_paid ="$00.00";
		$short_amt_paid = 0;
		}
 	
		$sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE)." ".get_user_meta_value($sales_id, 'last_name', TRUE);
		$reg_mgr = get_user_meta_value($sales_id, 'assign_to', TRUE);

		if($reg_mgr != ""){      
		$regional_manager_name = get_user_meta_value($reg_mgr, 'first_name', TRUE)." ".get_user_meta_value($reg_mgr, 'last_name', TRUE);
		}      

		$sales_reps_status =  get_user_meta_value($sales_id, '_status', TRUE);

		$physician_sql = "SELECT COUNT(*) AS total_physician FROM wp_abd_users AS u 
		JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
		LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id 
		LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
		LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
		WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' 
		AND um2.meta_key = 'wp_abd_capabilities' 
		AND um2.meta_value LIKE '%physician%' 
		AND um3.meta_key = 'added_by' 
		AND um3.meta_value LIKE '".$sales_id."' 
		AND (m1.meta_value = 'Active') 
        ORDER BY u.user_registered DESC";

		$total_physician = $this->BlankModel->customquery($physician_sql);
		   if (is_numeric($charged_amount) && is_numeric($charged_amount)) {
		      $total_char_amt += $charged_amount;
		    } 
		   if($charged_amt_for_own != 0 )
		    {
		      $total_charged_amt = $charged_amt_for_own+$total_char_amt;
		    }
		   else
		    {
		     $total_charged_amt = $total_char_amt;
		    }
		   
		   if(is_numeric($paid_amount) && is_numeric($paid_amount)) {
			 $total_pa_amt += $paid_amount; 
			}
		   if($paid_amt_for_own !=0 )
			{
			 $total_paid_amt = $paid_amt_for_own+$total_pa_amt;
			}
		   else
			{
			 $total_paid_amt = $total_pa_amt; 
			}
		   
		    $total_paid_amount = "$".number_format($total_paid_amt, 2, '.', ',');
		    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');

		  $sales_reps_array_data = array(
			'user_id' => $sales_id,
			'charged_amt' => $amt_ch,
            'paid_amt' => $amt_paid,
            'short_charged_amt' => (int)$short_amt_ch,
			'short_paid_amt' => (int)$short_amt_paid,
			'tot_phy' => $total_physician[0]['total_physician'],
			'short_tot_phy' => (int)$total_physician[0]['total_physician'],
			'sales_reps_name' => $sales_reps_name,
			'regional_name' => $regional_manager_name,
			'u_status' => $sales_reps_status
			);

		$sales_reps_count ++;
        array_push($sales_reps_list_array,$sales_reps_array_data);
     }

   if($sales_reps){
	 echo json_encode(array('status'=> '1', 'total_users' => $sales_reps_count, 'total_paid_amt' => $total_paid_amount, 'total_charged_amt'=> $total_charged_amount,"sales_reps_data" => $sales_reps_list_array));
	}
	else{
	 echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
    }
   
  } 

  else {
  
    echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
  
   }



  }
  
   /**
   * / search_regional_list End
   */
  
    function sales_man_details()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if($data['id']!=""){
        $user_id = $data['id'];
        // $user_id = '76067';
        
        $conditions = " ( `ID` = '".$user_id."')";		
        
        $select_fields = '*';
        $is_multy_result = 1;
        $memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
        


        $fname = get_user_meta_value($user_id, 'first_name',TRUE);
        $lname = get_user_meta_value($user_id, 'last_name',TRUE);
        $mob  = get_user_meta_value($user_id, '_mobile',TRUE);
        $add = get_user_meta_value($user_id, '_address',TRUE);
        $clinic = get_user_meta_value($user_id, 'clinic_addrs',TRUE);
        $fax = get_user_meta_value($user_id, 'fax',TRUE);
        $cell = get_user_meta_value($user_id, 'cell_phone',TRUE);
        $manager = get_user_meta_value($user_id, 'manager_contact_name',TRUE);
        $manager_cell = get_user_meta_value($user_id, 'manager_cell_number',TRUE);
        $offce_num = get_user_meta_value($user_id, 'office_number',TRUE);
        $npi_api = get_user_meta_value($user_id, 'npi',TRUE);
        $acc_cntrl = get_user_meta_value($user_id, 'specimen_control_access',TRUE);
        $acc_cntrl_qc =  get_user_meta_value($user_id, 'specimen_control_access_qc',TRUE);
        $role = get_user_role($user_id);
        $bank_routing_num = get_user_meta_value($user_id, '_brn',TRUE);
        $bank_account_num = get_user_meta_value($user_id, '_ban',TRUE);
        $name_on_bank = get_user_meta_value($user_id, '_nbaccount',TRUE);
        $user_status = get_user_meta_value($user_id, '_status',TRUE);
        $commission_percentage = get_user_meta_value($user_id, '_commission_percentage',TRUE);
        $llc = get_user_meta_value($user_id, '_llc',TRUE);
        $ein_no = get_user_meta_value($user_id, '_ein_no',TRUE);
        $assign_to =  get_user_meta_value($user_id, 'assign_to',TRUE);
        
      
        // regional manager data
        if($assign_to) {
        $re_ma_full_name = get_user_meta_value($assign_to, 'first_name',TRUE).' '. get_user_meta_value($assign_to, 'last_name',TRUE);
        } else { $re_ma_full_name = ''; }
        // regional manager list
        $role_regional_all_que = "SELECT  `u`.`ID` FROM wp_abd_users AS u LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id WHERE um1.meta_key = '_status' AND um1.meta_value = 'Active' AND um2.meta_key = 'wp_abd_capabilities' AND (um2.meta_value LIKE '%sales_regional_manager%' OR um2.meta_value LIKE '%regional_manager%' OR um2.meta_value LIKE '%team_regional_Sales%') ORDER BY u.display_name ASC";
        
        $role_regional_all = $this->BlankModel->customquery($role_regional_all_que);
        $assign_regional_name = array();
        foreach($role_regional_all as $regional){
          $user_id = $regional['ID'];
          $s_first_name = get_user_meta( $user_id, 'first_name');
          $s_last_name  = get_user_meta( $user_id, 'last_name');
          $s_fname = $s_first_name['meta_value'];
          $s_lname = $s_last_name['meta_value'];
          $user['name'] =$s_fname.' '.$s_lname;
          $user['id'] = $user_id;
          array_push($assign_regional_name, $user);
        }
      

    
        $specimen_information = array('fname' => $fname,'lname' => $lname,'acc_cntrl' => $acc_cntrl,'acc_cntrl_qc' => $acc_cntrl_qc,'email' => $memDetails['user_email'],'user_login'=>$memDetails['user_login'],'url' => $memDetails['user_url'], 'mob' => $mob,'add' => $add,'clinic' => $clinic,'fax' => $fax,'cell' => $cell,'manager' => $manager, 'manager_cell' => $manager_cell, 'offce_num' => $offce_num,'npi_api' => $npi_api,'role' => $role,'bank_routing_num' => $bank_routing_num,'bank_account_num' => $bank_account_num,'user_status' => $user_status, 'user_registered_date' => $memDetails['user_registered'], 'commission_percentage' => $commission_percentage, 'name_on_bank' => $name_on_bank,'llc'=>$llc,'ein_no'=>$ein_no,'assign_to'=>$assign_to,'re_ma_full_name'=>$re_ma_full_name);
      
      
        if($specimen_information)
        {
          echo json_encode(array("status" => "1","details" =>$specimen_information, "assign_regional_list"=>$assign_regional_name));
        }
        else
        {
          echo json_encode(array("status" => "0"));
        }
  
        }
    }



///////////////////////// update regional manager details/////////////////////////////
  function update_sales_man_details()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id = $data['user_id'];
      $status = array();
     
      $otherdata = 'no';
      
        $otherdata = update_user_meta ($user_id, 'first_name' , $data['first_name']);
        array_push($status,$otherdata);
    
        $otherdata = update_user_meta ($user_id, 'last_name' , $data['last_name']);
     
        $otherdata = update_user_meta ($user_id, '_mobile' , $data['mobile']);
        array_push($status,$otherdata);
     
        $otherdata = update_user_meta ($user_id, '_brn' , $data['brn']);
        array_push($status,$otherdata);
   
        $otherdata = update_user_meta ($user_id, '_ban' , $data['ban']);
        array_push($status,$otherdata);
      
        $otherdata = update_user_meta ($user_id, '_nbaccount' , $data['nbaccount']);
        array_push($status,$otherdata);

        $otherdata = update_user_meta ($user_id, '_commission_percentage' , $data['commission_percentage']);
        array_push($status,$otherdata);

        $otherdata = update_user_meta ($user_id, '_address' , $data['address']);
        array_push($status,$otherdata);

  
        $otherdata = update_user_meta ($user_id, '_status' , $data['user_status']);
        array_push($status,$otherdata);
  
        $otherdata = update_user_meta ($user_id, 'assign_to' , $data['regional_manager']);
        array_push($status,$otherdata);

        $llc_sql = "select * from `wp_abd_usermeta` where `meta_key` = '_llc' AND `user_id`= '".$user_id."'";
        $llc_result =  $this->BlankModel->customquery($llc_sql);
        if(empty($llc_result)) 
        {
          $llc = $data['llc'];
          $otherdata = add_user_meta ($user_id, '_llc' , $llc);
          array_push($status,$otherdata);
          if ($llc == 'llc') {
            $ein = $data['ein_no'];
          $otherdata = add_user_meta ($user_id, '_ein_no' , $ein);
          array_push($status,$otherdata);
          } else {
            $ein = '';
          $otherdata = add_user_meta ($user_id, '_ein_no' , $ein);
          array_push($status,$otherdata);
          }
        } 	
        else
        {
          $llc = $data['llc'];
          $otherdata = update_user_meta ($user_id, '_llc' , $llc);
          array_push($status,$otherdata);
          if ($llc == 'llc') {
            $ein = $data['ein_no'];
          $otherdata = update_user_meta ($user_id, '_ein_no' , $ein);
          array_push($status,$otherdata);
          } else {
            $ein = '';
          $otherdata = update_user_meta ($user_id, '_ein_no' , $ein);
          array_push($status,$otherdata);
          }
        }

       $userdata =array();
       if(!empty($data['user_url'])){
        $url = $data['user_url'];
       }else {
        $url ='';
       }
       
       if (!empty($data['user_email'])) {
          $user_email = $data['user_email'];
          $this->db->where('ID !=',$user_id);
          $this->db->where('user_email',$user_email);
          $resData = $this->db->get('wp_abd_users');

          if($resData->num_rows() > 0){
            die(json_encode(array(
            "status" => "1",
            "message" => 'Email exits.Please try with new one.'
            )));
          }else{
            $user_email = $data['user_email'];
            $userdata['user_email'] = $user_email;
          }
         
       }
       if (!empty($data['edit_con_pass']) && $data['edit_con_pass'] !== null) {
         $password = md5(SECURITY_SALT.$data['edit_con_pass']);
         $userdata['user_pass'] = $password;
         $userdata['user_url'] = $url;
       } else {
          $userdata['user_url'] = $url;
       }

    
      $conditions = "( `ID` = ".$user_id.")";
      $update = $this->BlankModel->editTableData('users',$userdata, $conditions);

      if($update == 'no' || $update == 'yes' || in_array('yes',$status)){
          echo json_encode(array("status" => "1","message" => 'User details has been successfully updated.'));
        }
     else{
          echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
        }
    }




    function change_user_status() {
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id = $data['user_id'];
      $status = $data['status'];

      if ($status =='Inactive') {
        $update = update_user_meta ($user_id, '_status','Active');
      } else {
        $update = update_user_meta ($user_id, '_status','Inactive');
      }

      if($update = 'yes'){
        echo json_encode(array("status" => "1","message" =>' User details has been successfully updated.'));
      }
      else{
        echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
      }

    }


//////////////////////////get commissions details//////////////////////////////

    public function commissions()
      {
      $physicians = array();
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id = $data['user_id'];
      $role = $data['user_role'];
      if (isset($data['keyword']) && !empty($data['keyword'])) {
      $search_data = $data['keyword'];
      }

      if (isset($data['sales_id']) && !empty($data['sales_id'])) {
         $user_id = $data['sales_id'];
      }
    
      $role = get_user_role($user_id);

      $sql='';

     if($role == 'aps_sales_manager'){
   
      $sql .= 'SELECT ID FROM wp_abd_users 
		    INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		    INNER JOIN wp_abd_usermeta as t3 ON wp_abd_users.ID = t3.user_id ';
  
      
       if (isset($search_data )) {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
       }
        
      $sql .=' WHERE t1.meta_key = \'wp_abd_capabilities\' ';
     

     if (isset($search_data )) {
      $sql .= "    
        	AND (b1.meta_value LIKE '%".trim($search_data)."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
       }  
       
       $sql .= ' AND t1.meta_value LIKE \'%physician%\' AND t3.meta_key = "_status" AND t3.meta_value = "Active"';
       
    } 
    
    elseif ( $role =='regional_manager' || $role =='sales_regional_manager' || $role =='sales') {

      $user_id = get_assign_user($user_id);
      $sql ='';
        
      $sql .= 'SELECT ID FROM wp_abd_users  
			    INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
			    INNER JOIN wp_abd_usermeta as t3 ON wp_abd_users.ID = t3.user_id ';
   
      if (isset($search_data )) {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
       }
        
      $sql .= ' INNER JOIN wp_abd_usermeta as m4 ON wp_abd_users.ID = m4.user_id ';       
    
      $sql .=' WHERE t1.meta_key = \'wp_abd_capabilities\' AND t1.meta_value LIKE \'%physician%\'';     
      
      $sql .= ' AND m4.meta_value IN ('.$user_id.')  AND m4.meta_key = "added_by"      
      AND t3.meta_key = "_status" AND t3.meta_value = "Active"';
      
      if (isset($search_data )) {
      $sql .= "    
        	AND (b1.meta_value LIKE '%".trim($search_data)."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
       }  
 
    
    } 
    
    elseif($role =='partner') {
      $sql = '';
      $sql .= 'SELECT `ID` FROM wp_abd_users u1 
      INNER JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      INNER JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "_assign_partner") 
      INNER JOIN wp_abd_usermeta as t3 ON u1.ID = t3.user_id ';
    

      
     if (isset($search_data )) {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
       }
        
      
      
      $sql .= ' WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value ="'.$user_id.'" AND t3.meta_key = "_status" AND t3.meta_value = "Active"';
       

      if (isset($search_data )) {
      $sql .= "    
        	AND (b1.meta_value LIKE '%".trim($search_data)."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
       }
      

    } 
    
   
   
    elseif($role =='clinic'){
     $sql = '';
       $sql .= 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "_assign_clinic") 
      JOIN wp_abd_usermeta as t3 ON u1.ID = t3.user_id ';
      

      
     if (isset($search_data )) {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
       }
      
      
      
       $sql .='  WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value ="'.$user_id.'" AND t3.meta_key = "_status" AND t3.meta_value = "Active"';
      

       
      if (isset($search_data )) {
      $sql .= "    
        	AND (b1.meta_value LIKE '%".trim($search_data)."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
       }
       
       
    
    
    else{
       $sql ='';
       $sql .= 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by")
      INNER JOIN wp_abd_usermeta as t3 ON u1.ID = t3.user_id ';
     

     if (isset($search_data )) {
      $sql .= '	INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
				INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
				';
       }
      
        
        $sql .= ' WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value IN ('.$user_id.') AND t3.meta_key = "_status" AND t3.meta_value = "Active"';
       

      
          if (isset($search_data )) {
      $sql .= "    
        	AND (b1.meta_value LIKE '%".trim($search_data)."%' 
			OR b2.meta_value LIKE '%".trim($search_data)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
			OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
       }
    
      }
    
    }
    

     
      $physicians_list = $this->BlankModel->customquery($sql);  	
     if($physicians_list){
      $physician_count = 0; 
      if(!empty($data['month']) && isset($data['month'])){
        $curr_month = $data['month'];
      } else {
        $curr_month = abs(date('m'));
      }

      if(!empty($data['year']) && isset($data['year'])){
        $curr_year  = $data['year'];
      } else {
        $curr_year  = date('Y');
      }
      
       
       $total_char = 0;
       $total_paid = 0;

    foreach($physicians_list as $physician){
   	  $physician_id = $physician['ID'];
      $sales_id = get_user_meta_value($physician_id, 'added_by',TRUE);
      if($sales_id){
         $sales_reps_name = get_user_meta_value($sales_id, 'first_name',TRUE)." ".get_user_meta_value($sales_id, 'last_name',TRUE);
        }
   
        $charged_amnt = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_chargedamt',TRUE);
        $paid_amnt = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_paidamt',TRUE);
        
        if(!empty($charged_amnt))
     		 {
          $char_amnt = $charged_amnt;
     			$short_char_amnt = $charged_amnt;
         } else {
          $char_amnt = "00.0";
          $short_char_amnt = 0;
         }
         $total_char = (floatval($total_char) + floatval($char_amnt));
        

        if(!empty($paid_amnt))
        {
         $paid_amount = $paid_amnt;
         $short_paid_amount = $paid_amnt;
        } else {
          $paid_amount = "00.0";
          $short_paid_amount = 0;
        }
              
                    
        $total_paid = (floatval($total_paid) + floatval($paid_amount));
      
 
	    $physician_data = array(
	      'sales_id'               => $sales_id,
	      'physician_id'           => $physician_id,
	      'physician_name'         => get_user_meta_value($physician_id, 'first_name',TRUE).' '.get_user_meta_value($physician_id, 'last_name',TRUE),
	      'sales_reps_name'        => $sales_reps_name,
	      'charged_amt'            => '$'.number_format($char_amnt, 2),
	      'paid_amt'               => '$'.number_format($paid_amount, 2),
	      'short_charged_amt'      => (int)$short_char_amnt,
	      'short_paid_amt'         => (int)$short_paid_amount,
	     
	     );
    	 array_push($physicians,$physician_data);
   		 $physician_count ++;
      }

		$last_data_entry_date = get_user_meta_value('66','last_commissions_entry_date',TRUE);
		$total_char = '$'.number_format($total_char, 2);
		$total_paid = '$'.number_format($total_paid, 2);

       echo json_encode(array('status'=>'1', "physicians_details" => $physicians, 'physicians_count'=> $physician_count, 'total_char'=> $total_char,
       'total_paid'=>$total_paid, 'last_data_entry_date'=>$last_data_entry_date));
    }
    else{
        echo json_encode(array('status'=>'0', "message" => 'no data found!'));
       }

    }




    //////////////////////// realData////////////////////////////
    public function realData()
    {
      $physicians = array();
      $data = json_decode(file_get_contents('php://input'), true);
    
     
       $role = $data['user_role'];
       $user_id = $data['user_id'];
       if (isset($data['sales_id']) && !empty($data['sales_id'])) {
         $user_id = $data['sales_id'];
       }
       $role = get_user_role($user_id);

     if( $role =='aps_sales_manager'){
     $sql = ' SELECT  ID FROM    wp_abd_users INNER JOIN wp_abd_usermeta as t1
    ON      wp_abd_users.ID          = t1.user_id 
    WHERE   t1.meta_key = \'wp_abd_capabilities\'
    AND     ( 
    ';  
      $sql .= 't1.meta_value LIKE \'%physician%\'';
      $sql .= ' ) ORDER BY display_name ASC';

    }elseif ( $role =='regional_manager' || $role =='sales_regional_manager') {
        $user_id = get_assign_user($user_id);

         $sql = 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by")  WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value IN ('.$user_id.')';
      
    } elseif($role =='partner') {
      
      $sql = 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "_assign_partner")  WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value ="'.$user_id.'"';

    } elseif($role =='clinic'){

       $sql = 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "_assign_clinic")  WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value ="'.$user_id.'"';
    }else{
       $sql = 'SELECT `ID` FROM wp_abd_users u1 
      JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")    
      JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by")  WHERE (m3.meta_value LIKE "%physician%" ) AND m4.meta_value IN ('.$user_id.')';
    }
     

      $physicians_list = $this->BlankModel->customquery($sql);  	
     if($physicians_list){
      $physician_count = 0; 
      if(!empty($data['month']) && isset($data['month'])){
        $curr_month = $data['month'];
      } else {
        $curr_month = abs(date('m'));
      }

      if(!empty($data['year']) && isset($data['year'])){
        $curr_year  = $data['year'];
      } else {
        $curr_year  = date('Y');
      }
      
       
       $total_char = 0;
       $total_paid = 0;

    foreach($physicians_list as $physician){
      $physician_id = $physician['ID'];
      $sales_id = get_user_meta_value($physician_id, 'added_by',TRUE);
      if($sales_id){
        $sales_reps_name = get_user_meta_value($sales_id, 'first_name',TRUE)." ".get_user_meta_value($sales_id, 'last_name',TRUE);
       }
   
        $charged_amnt = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_chargedamt',TRUE);
        $paid_amnt = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_paidamt',TRUE);
        
        if(!empty($charged_amnt)){
          $char_amnt = $charged_amnt;
     	  $short_charged_amt = $charged_amnt;
         }
        else {
          $char_amnt = "0.00";
          $short_charged_amt = 0;
         }
         $total_char = $total_char + $char_amnt;
        

        if(!empty($paid_amnt))
        {
         $paid_amount = $paid_amnt;
          $short_paid_amt = $paid_amnt;
        } else {
          $paid_amount = "0.00";
          $short_paid_amt = 0;
        }
        $total_paid = $total_paid + $paid_amount;
      
    if($paid_amount > 0 || $char_amnt > 0) {
    $physician_data = array(
      'sales_id'               => $sales_id,
      'physician_id'           => $physician_id,
      'physician_name'         => get_user_meta_value($physician_id, 'first_name',TRUE).' '.get_user_meta_value($physician_id, 'last_name',TRUE),
      'sales_reps_name'        => $sales_reps_name,
      'charged_amt'            => '$'.number_format($char_amnt, 2),
      'paid_amt'               => '$'.number_format($paid_amount,2),
      'short_charged_amt'      => (int)$short_charged_amt,
      'short_paid_amt'         => (int)$short_paid_amt,
     
    );
    array_push($physicians,$physician_data);
    $physician_count ++;
  }
      }
       echo json_encode(array('status'=>'1', "physicians_details" => $physicians, 'physicians_count'=> $physician_count, 'total_char'=>'$'.number_format($total_char,2),
       'total_paid'=>'$'.number_format($total_paid,2)));
    }
    else{
        echo json_encode(array('status'=>'0', "message" => 'no data found!'));
       }
    }
    
    
    ///////////////////////sales rep////////////////////////////

    public function salesRepList()
    {
    $data = json_decode(file_get_contents('php://input'), true);
    
    $current_user_id = $data['user_id'];
    $user_role = $data['user_role'];
  
    $sales_reps_list_array = array();
    $sales_reps_sql = "";
 	
	if( $user_role == "aps_sales_manager" || $user_role == "data_entry_oparator"){
		$sales_reps_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
        OR m3.meta_value LIKE  "%team_regional_Sales%" )';
  	}
	elseif ( $user_role == "regional_manager" || $user_role == "sales_regional_manager"){
		$sales_reps_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
		WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
        OR m3.meta_value LIKE  "%team_regional_Sales%")
        AND m4.meta_value = "'.$current_user_id.'"';
	}
	
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);      
    $sales_reps_count = 0;  
   if($sales_reps){
   	
   foreach($sales_reps as $sales_representative)
    {
      $regional_manager_name = "";
      $sales_id = $sales_representative['ID'];
      
       
     	$sales_reps_fname = get_user_meta($sales_id, 'first_name');
     	$sales_reps_lname = get_user_meta($sales_id, 'last_name');     	
        $sales_reps_name = $sales_reps_fname['meta_value']." ".$sales_reps_lname['meta_value'];      	
	    $sales_reps_array_data = array(
			'user_id' => $sales_id,
			'sales_reps_name' => $sales_reps_name,
      );
      
     $sales_reps_count ++;
     array_push($sales_reps_list_array,$sales_reps_array_data);
     }

    }
     if($sales_reps){
	 echo json_encode(array('status'=>'1', "sales_reps_data" => $sales_reps_list_array));
	}
	else {
	 echo json_encode(array('status'=>'0', "sales_reps_data" => "No Data Found"));
      }
    }

    public function specimen()
    {
     $data = json_decode(file_get_contents('php://input'), true);
     $phy_id = $data['phy_id'];
    //  $phy_id ='75035';

     $conditions = " ( `physician_id` = '".$phy_id."')";
     $select_fields = 'id,collection_date,date_received,assessioning_num,physician_id';
     $is_multy_result = 0;
     $memDetails  = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);

     if(!empty($memDetails)) {
      echo json_encode(array('status'=>'1', 'specimen_details'=> $memDetails,'total_specimen'=>count($memDetails)));
      } else {
        echo json_encode(array('status'=>'0'));
      }
     
    }
//////////////////////////get commissions history details//////////////////////////////


    public function add_sales_rep()
    { 
      $data = json_decode(file_get_contents('php://input'), true);

      if(!empty($data) && isset($data)){

      $check_email    = email_validation('users', 'user_email', $data['user_email']);
      $check_username = username_validation('users', 'user_login', $data['user_login']);

   if(!$check_email && !$check_username) {

        $password =   md5(SECURITY_SALT.$data['con_pass']);
        $display_name = $data['first_name'].' '. $data['last_name'];
        $created_date = date("Y-m-d h:i:sa");
        $table_data = array('user_login' =>$data['user_login'] ,'user_pass' => $password,'user_email' => $data['user_email'],'user_url' => $data['user_url'], 'display_name'=>$display_name ,'user_nicename'=> strtolower($data['first_name']), 'user_registered' => $created_date);
        $user_id = $this->BlankModel->addTableData('users', $table_data);
    if($user_id){
        $role_type_array = array('sales' => '1');
        $role = serialize( $role_type_array);

        add_user_meta($user_id,'first_name', $data['first_name'] );
        add_user_meta($user_id,'last_name', $data['last_name'] );
        add_user_meta($user_id,'_mobile', $data['mobile'] );
        add_user_meta($user_id,'wp_abd_capabilities', $role );
        add_user_meta($user_id,'_address', $data['address'] );
        add_user_meta($user_id,'_status', $data['user_status'] );
        add_user_meta($user_id,'_ban', $data['ban'] );
        add_user_meta($user_id,'_brn', $data['brn'] );
        add_user_meta($user_id,'_commission_percentage', $data['commission_percentage'] );
        if (isset($data['llc'])) {
        add_user_meta($user_id,'_llc', $data['llc'] );
        add_user_meta($user_id,'_ein_no', $data['ein_num'] );
         }
         if(isset($data['regional_manager']) && !empty($data['regional_manager'])) {
         $assign_to = $data['regional_manager'];
       }else {
         $assign_to = $data['user_id'];
       }

        add_user_meta($user_id,'assign_to', $assign_to );
        add_user_meta($user_id,'_nbaccount', $data['nbaccount']);

       
        echo json_encode(array("status" => "1","message" => 'User Added successfully.'));
      } else {
        echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again'));
        }
      } else {
          echo json_encode(array("status" => "0","message" => $check_email.' '.$check_username ));
        }
      }

  }

//////////////////////////get commissions history details//////////////////////////////

    public function commission_history()
    {
      
      $data = json_decode(file_get_contents('php://input'), true);
        if($data['user_id']!=""){
        $user_id = $data['user_id'];
        $history_array = array();       
        $curr_year  = date('Y');        
        $total_char = 0;
        $total_paid = 0;

        for($i=1; $i<=12; $i++)
        {
           if($i==1) $month = 'January';
           else if($i==2) $month = 'February';
           else if($i==3) $month = 'March';
           else if($i==4) $month = 'April';
           else if($i==5) $month = 'May';
           else if($i==6) $month = 'June';
           else if($i==7) $month = 'July';
           else if($i==8) $month = 'August';
           else if($i==9) $month = 'September';
           else if($i==10) $month = 'October';
           else if($i==11) $month = 'November';
           else if($i==12) $month = 'December';
   
        $charged_amnt = get_user_meta($user_id, 'month_'.$i.'_'.$curr_year.'_chargedamt');
        $paid_amnt = get_user_meta($user_id, 'month_'.$i.'_'.$curr_year.'_paidamt');
        $first_name = get_user_meta( $user_id, 'first_name');
        $last_name  = get_user_meta( $user_id, 'last_name');

        $fname = $first_name['meta_value'];
        $lname = $last_name['meta_value'];
        $full_name = $fname.' '.$lname;
        
        if(!empty($charged_amnt['meta_value']))
     		 {
     			$char_amnt = $charged_amnt['meta_value'];
         } else {
          $char_amnt = "0.00";
         }
         $total_char = $total_char + $char_amnt;
        
       

        if(!empty($paid_amnt['meta_value']))
        {
         $paid_amount = $paid_amnt['meta_value'];
        } else {
          $paid_amount = "0.00";
        }
        $total_paid = $total_paid + $paid_amount;

        $history_array_data = array(
            'month' => $month,
            'charged_amt'=>'$'.number_format($char_amnt, 2),
            'paid_amt'=>'$'.number_format($paid_amount,2),
            );
             array_push($history_array, $history_array_data);
      }

    if(!empty($history_array)) {
          echo json_encode(array('status'=>'1', 'total_paid_amt'=> '$'.number_format($total_paid, 2), 'total_charged_amt'=> '$'.number_format($total_char,2), 'search_year' => $curr_year,'full_name'=>$full_name,"history_view" => $history_array));
    } else {
      echo json_encode(array('status'=>'0'));
    }
  }
         
    }


    public function search_commission_history()
    {
      
        $data = json_decode(file_get_contents('php://input'), true);
        if($data['user_id']!=""){
        $user_id = $data['user_id'];
        $curr_year = $data['year'];
        $history_array = array();
        $total_char = 0;
        $total_paid = 0;

        for($i=1; $i<=12; $i++)
        {
           if($i==1) $month = 'January';
           else if($i==2) $month = 'February';
           else if($i==3) $month = 'March';
           else if($i==4) $month = 'April';
           else if($i==5) $month = 'May';
           else if($i==6) $month = 'June';
           else if($i==7) $month = 'July';
           else if($i==8) $month = 'August';
           else if($i==9) $month = 'September';
           else if($i==10) $month = 'October';
           else if($i==11) $month = 'November';
           else if($i==12) $month = 'December';
   
        $charged_amnt = get_user_meta($user_id, 'month_'.$i.'_'.$curr_year.'_chargedamt');
        $paid_amnt = get_user_meta($user_id, 'month_'.$i.'_'.$curr_year.'_paidamt');
        $first_name = get_user_meta( $user_id, 'first_name');
        $last_name  = get_user_meta( $user_id, 'last_name');

        $fname = $first_name['meta_value'];
        $lname = $last_name['meta_value'];
        $full_name = $fname.' '.$lname;
        
        if(!empty($charged_amnt['meta_value']))
     		 {
     			$char_amnt = $charged_amnt['meta_value'];
         } else {
          $char_amnt = "0.00";
         }
         $total_char = $total_char + $char_amnt;
        
       

        if(!empty($paid_amnt['meta_value']))
        {
         $paid_amount = $paid_amnt['meta_value'];
        } else {
          $paid_amount = "0.00";
        }
        $total_paid = $total_paid + $paid_amount;

        $history_array_data = array(
            'month' => $month,
            'charged_amt'=>'$'.number_format($char_amnt, 2),
            'paid_amt'=>'$'.number_format($paid_amount,2),
            );
             array_push($history_array, $history_array_data);
      }

    if(!empty($history_array)) {
          echo json_encode(array('status'=>'1', 'total_paid_amt'=> '$'.number_format($total_paid, 2), 'total_charged_amt'=> '$'.number_format($total_char,2), 'search_year' => $curr_year,'full_name'=>$full_name,"history_view" => $history_array));
    } else {
      echo json_encode(array('status'=>'0'));
    }
    }
         
    }

    
  /**
	* Sales Reps list with Regional Manager id
	*/
	function sale_reps_by_reg_mgr_id()
    {
    	$reg_data = json_decode(file_get_contents('php://input'), true);
		$total_char_amt = 0;
		$charged_amt_for_own = 0;
		$curr_month = date('n');
		$curr_year  = date('Y');
		$sales_reps_list_array = array();	
		$total_pa_amt = 0;	
		$paid_amt_for_own = 0;	
		$total_charged_amt = 0;	
		$total_paid_amt = 0;
		$reg_user_Id = $reg_data['reg_id'];
		$user_role = "aps_sales_manager";
    if(($reg_user_Id != "" && $user_role == "aps_sales_manager" ) || ($reg_user_Id != "" && $user_role == "data_entry_oparator"))
     {
     $sales_reps_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
		JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = "_status")
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
		WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
        OR m3.meta_value LIKE  "%team_regional_Sales%")
        AND m4.meta_value = "'.$reg_user_Id.'" 
        AND m2.meta_value = "Active"';
                
     $sales_reps = $this->BlankModel->customquery($sales_reps_sql);      
     $sales_reps_count = 0;  
  
     if($reg_user_Id != "")	
   	 {
      $reg_user_role = get_user_role($reg_user_Id);
      if($reg_user_role == "team_regional_Sales"){
         $reg_Sales_reps = array( 'ID' => $reg_user_Id);
         array_push($sales_reps, $reg_Sales_reps);
        }
     if($reg_user_role == 'sales_regional_manager' ){
        $reg_Sales_reps = array( 'ID' => $reg_user_Id);
        array_push($sales_reps, $reg_Sales_reps);
       }
 	 }
    
     foreach($sales_reps as $sales_representative)
     {
      $regional_manager_name = "";
      $sales_id = $sales_representative['ID'];
      /**
	  * 
	  * @var Charged Amount
	  * 
	  */
      $charged_amount = get_total_amt($sales_id, 'chargedamt', $curr_month, $curr_year);
     
       if($charged_amount != "" )
       {
        $amt_ch = "$".number_format($charged_amount, 2, '.', ',');
       }
       
       else{
        $amt_ch ="$00.00";
        }
      /**
	  * 
	  * @var Paid Amount
	  * 
	  */  
        
      $paid_amount = get_total_amt($sales_id , 'paidamt', $curr_month, $curr_year);
      
       if($paid_amount !="" )
       {
        $amt_paid = "$".number_format($paid_amount, 2, '.', ',');
       }
      
       else{
        $amt_paid ="$00.00";
       }
       
     	$sales_reps_fname = get_user_meta_value($sales_id, 'first_name');
     	$sales_reps_lname = get_user_meta_value($sales_id, 'last_name');     	
        $sales_reps_name = $sales_reps_fname." ".$sales_reps_lname;      	
     	$reg_mgr = get_user_meta_value($sales_id, 'assign_to');
     	
       if($reg_mgr != ""){
		$team_mang_id = $reg_mgr;        
        $regional_manager_fname = get_user_meta_value($team_mang_id, 'first_name');
        $regional_manager_lname = get_user_meta_value($team_mang_id, 'last_name');        
        $regional_manager_name = $regional_manager_fname." ".$regional_manager_lname;
      	}      
        
        $u_status = get_user_meta($sales_id, '_status');
        $sales_reps_status =  $u_status['meta_value'];
     
        $physician_sql = "SELECT COUNT(*) AS total_physician FROM wp_abd_users AS u 
					       	JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
					        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
					        LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
					        WHERE um2.meta_key = 'wp_abd_capabilities' 
					        AND um2.meta_value LIKE '%physician%' 
					        AND um3.meta_key = 'added_by' 
					        AND um3.meta_value LIKE '".$sales_id."' 
					        AND (m1.meta_value = 'Active')
					        ORDER BY u.user_registered DESC";
     
        $total_physician = $this->BlankModel->customquery($physician_sql);
        
           if(is_numeric($paid_amount) && is_numeric($paid_amount)) {
			 $total_pa_amt += $paid_amount; 
			}
        
   		   if (is_numeric($charged_amount) && is_numeric($charged_amount)) {
		      $total_char_amt += $charged_amount;
		    } 
		   if($charged_amt_for_own != 0 )
		   {
		    $total_charged_amt = $charged_amt_for_own+$total_char_amt;
		   }
		   else
		   {
		    $total_charged_amt = $total_char_amt;
		   }
		   
		   if($paid_amt_for_own !=0 )
			{
			 $total_paid_amt = $paid_amt_for_own+$total_pa_amt;
			}
		   else
			{
			 $total_paid_amt = $total_pa_amt; 
			}
		   
		    $total_paid_amount = "$".number_format($total_paid_amt, 2, '.', ',');
		    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');

	      $sales_reps_array_data = array(
			'user_id'         => $sales_id,
			'charged_amt'     => $amt_ch,
			'paid_amt'        => $amt_paid,
			'tot_phy'         => $total_physician[0]['total_physician'],
			'short_tot_phy'   => (int)$total_physician[0]['total_physician'],
			'sales_reps_name' => $sales_reps_name,
			'regional_name'   => $regional_manager_name,
			'u_status'        => $sales_reps_status,
			'short_charged_amt' => (int)$charged_amount,
            'short_paid_amt'    => (int)$paid_amount,
			);
    
      $sales_reps_count ++;
      array_push($sales_reps_list_array,$sales_reps_array_data);
     }
   
     $last_data_entry_date = get_user_meta_value('66','last_commissions_entry_date',TRUE);
	

     if($sales_reps){
	 echo json_encode(array( 'status'=>'1', 'total_users' => $sales_reps_count, 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount,"sales_reps_data" => $sales_reps_list_array, 'last_data_entry_date'=>$last_data_entry_date ));
	 }
	 else{
	 $no_data_found = "No Data Found";
	 echo json_encode(array( 'status'=>'0', "no_commission_data" => $no_data_found, 'total_users'=>'0' ));
     }
 
    }
 
  }   

function getSpecimenCountForSales(){
    $data = json_decode(file_get_contents('php://input'), true);
  if($data){
    // $curr_month = date('n');
    // $curr_month =  $curr_month - 1;
    // $curr_year  = date('Y');
    $phy_year = $data['phy_year'];
    $form_month = $data['form_month'];
    $to_month = $data['to_month'];
    $sales_comm_arr = array();
    // $jd=gregoriantojd($curr_month,1,$curr_year);
    // $current_month = jdmonthname($jd,1);
    // $crnt_date = date('Y-m-d');
    $user_role = $data['user_role'];
    
    $sales_id = $data['sales_rep'];
    if (isset($sales_id)) {   
    
      //$sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE)." ".get_user_meta_value($sales_id, 'last_name', TRUE);
      while($form_month<=$to_month){
        $charged_amount = get_total_amt($sales_id, 'chargedamt', $form_month, $phy_year);
        $month_name = date("Y-m-d", mktime(0, 0, 0, $form_month, 10)); 

        $sales_chart_arr = array($month_name,(Int)$charged_amount?$charged_amount:0);
        array_push($sales_comm_arr,$sales_chart_arr);
        $form_month++;
      }
      
  
     if(!empty($sales_comm_arr)){
      
     echo json_encode(array('status'=> '1', 'sales_comm_arr'=>$sales_comm_arr));
    }
    else{
     echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
      }
     
    } 
  
    else {
    
      echo json_encode(array('status'=>'0', "no_commission_data" => "No Data Found", 'total_users'=>'0'));
    
     }
  }
  
  }
 }


?>