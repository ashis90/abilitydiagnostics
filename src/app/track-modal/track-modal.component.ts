import { Component, OnInit, Renderer2 } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cm-track-modal',
  templateUrl: './track-modal.component.html',
  styleUrls: ['./track-modal.component.css']
})
export class TrackModalComponent implements OnInit {
  title;
  modalreference:any;
  mouseEnterVal:boolean=false;
  mouseEnterNewVal:boolean=false;
  showDropDown:boolean=false;
  shand2;
  constructor(
    public modalRef: BsModalRef,
    public route:Router, 
    private renderer: Renderer2
  ) { }

  ngOnInit() 
  {
    //this.shand2 = document.getElementsByClassName('modal-content');
    this.renderer.addClass(document.body,'new-track-class');
  }

  mouseEnter(){
    this.mouseEnterVal=true;
  }

  mouseLeave(){
    this.mouseEnterVal=false;
  }

  mouseEnterNew(){
    this.mouseEnterNewVal=true;
  }

  mouseLeaveNew(){
    this.mouseEnterNewVal=false;
  }

}
