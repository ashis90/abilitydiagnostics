import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }
  public home_ban = environment.assetsUrl+'assets/frontend/images/1490771733_PR06.JPG';
  ngOnInit() {
  }

}
