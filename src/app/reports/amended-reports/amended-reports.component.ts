import { Component, OnInit } from '@angular/core';
import {PendingSpecimensService } from '../../report.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';
import { environment } from '../../../environments/environment';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-amended-reports',
  templateUrl: './amended-reports.component.html',
  styleUrls: ['./amended-reports.component.css']
})
export class AmendedReportsComponent implements OnInit {

  public data: any;
  datePickerConfig: Partial<BsDatepickerConfig>;
  specimens_list: any = [];
  specimen_data: any = [];
  physicians_list: any = [];
  searchForm: FormGroup;
  public phy_data: any;
  load_status: boolean = true;
  public loading = false;
  sendData;
  postsArray: any = [] ;
  no_data: any;
  no_data_status = false;

  physicians_list_data: any = [];

  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  modalRef: BsModalRef;
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }];
 
public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }];
  filteredOptions: Observable<StateGroup[]>;
  count: any;
  public url = environment.assetsUrl+'assets/uploads/';
    constructor(
      private http: HttpClient, 
      public itemsService: PendingSpecimensService, 
      public route: Router,
      private spinner: NgxSpinnerService,
      private modalService: BsModalService,
      public analyticsService:AnlyticsService
      ) {
       this.searchForm = new FormGroup({
        "physician_id": new FormControl(),
        "physician_name": new FormControl(),
        "collected_date" : new FormControl(),
        "firstname" : new FormControl(),
        "lastname" : new FormControl(),
        "assessioning_num" : new FormControl(),
        "barcode" : new FormControl(),
       });

       this.datePickerConfig = Object.assign({}, {
        containerClass: 'theme-blue',
        dateInputFormat: 'MM/DD/YYYY'
      });
     }
     resetForm(){
      this.searchForm.reset();
      this.get_amended_report();
    }

     onSubmit() {
        if (this.searchForm.status == 'VALID') {
            this.loading = true;
            this.load_status = false;

            this.sendData = { "date": this.convertDate (this.searchForm.value.collected_date),"physician": this.searchForm.value.physician_id,
             "fname": this.searchForm.value.firstname,
            "lname": this.searchForm.value.lastname,
             "assessioning_num": this.searchForm.value.assessioning_num,
            "barcode": this.searchForm.value.barcode,
           };
         
            this.spinner.show();
            this.itemsService.search_amended_reports(this.sendData).subscribe( data => {
                this.loading = false;
                this.postsArray = data;
                this.data  =  this.postsArray.specimens_detail;
                this.phy_data  =  this.postsArray.physicians_data;
                this.count  =  this.postsArray.specimen_count;
                if(this.postsArray['status'] ==='1'){
                  this.spinner.hide();
                }
                if(this.postsArray['status'] ==='0'){
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
                  this.spinner.hide();
                }
             });
        } else {}

    }



  ngOnInit() {
    this.loading = true;
    this.get_amended_report();
    this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value?value.toLowerCase():''))
    );

  }

  get_amended_report() {
    this.spinner.show();
    this.itemsService.amendedReport().subscribe(data => {
    this.specimens_list = data;
    this.loading = false;
      if (this.specimens_list.status == '1') {

        this.no_data_status = false;
        this.data  =  this.specimens_list.specimen_results;
        this.count  =  this.specimens_list.specimen_count;
        this.physicians_list = this.specimens_list.physicians_data;
        this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
        this.physicians_list.map((val) => this.allIds.push ( val.id));
        this.physicians_list.map((data) => this.filterVal.push ( data));
        this.spinner.hide();

      }
      if (this.specimens_list.status == '0') {
        this.no_data_status = true;
        this.no_data = 'No Data Found.';
        this.spinner.hide();
      }

    });
    this.stateGroups = [{
      ids: this.allIds,
      names: this.allNames
    }];

  }


  private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount: number = this.filterVal.length;
        this.options = [];
        this.ids = [];
        for (let i = 0; i < itemCount; i++) {
            if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ) {
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids: this.ids,
          names: this.options
        }];

        return this.searchData;
    }

    return this.stateGroups;
}

callSomeFunction(phy_id: any) {
  this.searchForm.controls['physician_id'].setValue(phy_id);
}

convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {
      var date = new Date(str),
        mnth = ('0' + (date.getMonth() + 1)).slice(-2),
        day  = ('0' + date.getDate()).slice(-2),
        newDate = [ mnth, day, date.getFullYear() ].join('/');
    }
    return newDate;
  }
  
  displaytrackmodal(specimen_id:any){
    this.spinner.show();
    this.sendData = { "acc_no":specimen_id};
    this.analyticsService.specimen_track(this.sendData).subscribe(data => {
      this.postsArray = data;
      this.spinner.hide();
      if(this.postsArray.histo_status=='1')
      {
        this.acc_no=true;
        this.histo_modal =true;
      }
      else
      {
        this.histo_modal =false;
      }
      if(this.postsArray.pcr_status=='1')
      {
        this.acc_no=true;
        this.pcr_modal =true;
      }
      else
      {
        this.pcr_modal =false;
      }
      if(this.postsArray.status=='0')
      {
         this.acc_no=false;
         this.pcr_modal =false;
         this.histo_modal =false;
      }
      this.modalRef = this.modalService.show(TrackModalComponent,  {
        initialState: {
          title: 'track',
          acc_no:  this.acc_no,
          postsArray: this.postsArray,
          histo_modal: this.histo_modal,
          pcr_modal: this.pcr_modal
        }

      });

    });


  }

}
