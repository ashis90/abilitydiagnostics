import { Component, OnInit } from '@angular/core';
import { PcrService } from '../../pcr.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'cm-archive-pcr-submited-reports',
  templateUrl: './archive-pcr-submited-reports.component.html',
  styleUrls: ['./archive-pcr-submited-reports.component.css']
})
export class ArchivePcrSubmitedReportsComponent implements OnInit {

  public data: any;
  specimens_list: any = [];
  post_array: any;
  batch_review: any;
  specimen_count: any;
  loading: boolean;
  no_data: any;
  no_data_status: boolean = false;
  filterData: any;
  public url = environment.assetsUrl + 'assets/uploads/';
  constructor(
    private http: HttpClient,
    public itemsService: PcrService,
    public route: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {

  }

  ngOnInit() {
    this.spinner.show();
    this.getSubmitedReportsData();
  }
  getSubmitedReportsData() {
    let params = { 'dataType': 'archive' };
    this.itemsService.getSubmitedReports(params).subscribe(data => {
      this.post_array = data;
      this.data = this.post_array.submited_report_data;
      this.specimen_count = this.post_array.specimen_count;
      console.log(this.post_array);
      if (this.post_array.status === '1') {
        this.spinner.hide();
      }
      if (this.post_array.status === '0') {
        this.no_data_status = true;
        this.spinner.hide();
        this.no_data = "No Data Found.";
      }

    });
  }

  search(term: string) {
    let allData: any;
    allData = this.post_array.submited_report_data;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.accessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }

  }

}
