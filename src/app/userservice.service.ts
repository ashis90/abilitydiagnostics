import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl + 'Frontapi/';
  public login_url = environment.baseUrl + 'Login/';


  makeHttpGetRequest() {

    return this.http.get(this.url + 'home_page_details', this.options);
  }

  dashboardRequest(data) {

    return this.http.post(this.login_url + 'get_user_details', JSON.stringify(data), this.options);
  }
  chkdashboardRequest(data) {

    return this.http.post(this.login_url + 'user_exists', JSON.stringify(data), this.options);
  }

  login(data) {
    return this.http.post(this.login_url + 'login', JSON.stringify(data), this.options);
  }
  logout(data) {
    //console.log(localStorage.userid);
    return this.http.post(this.login_url + 'logout_user', JSON.stringify(data), this.options);

  }
  userDetailsRequest(id) {
    return this.http.post(this.login_url + 'user_details', JSON.stringify(id), this.options);
  }
  updateUserDetails(data) {
    return this.http.post(this.login_url + 'update_user_details', JSON.stringify(data), this.options);
  }
  getGoogleAnalytics() {
    return this.http.get(this.url + 'get_google_analytics', this.options);
  }
}
