		
		
		<p style="padding-top:1px;"></p>

<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4; border-top:none;font-family:Arial, Helvetica, sans-serif;font-size: 12px;">
  <div style="background:#8db3e2; border:1px solid #4285f4; border-left:none; border-right:none; padding:1px 8px; text-align:center; font:700 24px Arial, Helvetica, sans-serif;color:#000;font-size: 12px;">
    <h4 style="font:700 12px Arial, Helvetica, sans-serif; color:#fff;margin:0;">{report_heading}</h4>
  </div>
  <div class="n_hdr_sec">
    <div style="text-align:center; padding:2px 0 0;">
      <div style="width:31.3333%; float:left; margin:0; padding:1% 0 1% 1%;">
        <div style="max-width:80%; margin:0 auto;">
          <img src="{logo}" alt = "ability_logo"/>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:0;">
        <div class="comny_tag">
          <h4 style="font:700 15px Arial, Helvetica, sans-serif; margin:0; padding:0; color:#000;">Ability Diagnostics, LLC</h4>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">858 South Auto Mall Road, Suite 102</p>
          <p style="color:#444; margin:0; padding:0; font:400 12px Arial, Helvetica, sans-serif;">American Fork, UT 84003</p>
        </div>
      </div>
      <div style="width:31.3333%; float:left; margin:0; padding:1%;">
        <div style="font:400 12px/14px Arial, Helvetica, sans-serif;color: #444;">
          <p style="margin-bottom:0;font-size: 12px;">Date of Report</p>
          <p style="margin:0;font-size: 12px;"><span style="display:block;">{date}</span></p>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="border-bottom:1px dotted #8db3e2; text-align:center;color:#444; padding:0; margin:0;">
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%;">
        <div class="n_phn_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">801-899-3828</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%">
        <div class="fax_no">
          <p style="color:#444; margin:0;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax Number</p>
          <p style="margin-top:5px;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">801-855-7548</p>
        </div>
      </div>
      <div style=" width:31.3333%; float:left; margin:0; padding:1% 1% 0 1%;">
        <div class="assct_no">
          <h6 style="font:700 12px/14px Arial, Helvetica, sans-serif; color:#000;margin:0;">Accessioning Number</h6>
          <p style="margin-top:0;color:#000;font:bold 12px/14px Arial, Helvetica, sans-serif;">{acc_num}</p>
        </div>
      </div>
    </div>
  </div>
  <div style="padding:3px 5px 0 5px; margin:0; display:block;">
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style="padding:0;">
		<p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif; color:#000;">Patient Information:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;text-transform:capitalize;">{p_name}</div>
        </p>
        <p style="display:block;margin:5px 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_phone}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">DOB:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_dob}</div>
        </p>
        <p style="display:block;margin:0 0 5px">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Collection Date:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_col_date}</div>
        </p>
        <p style="display:block;margin:0 0 5px;">
        <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Date Received:</div>
        <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{p_rec_date}</div>
        </p>
      </div>
    </div>
    <div style=" width:48%; padding:1%; float:left; margin:0;">
      <div style=" padding:0;">
		<div class="n_ptnt_loctn">
          <p style="display:block;margin:0 0 0px;">
        <div style="width:40%;float:left;color:#444;font:400 12px Arial, Helvetica, sans-serif;">Referring Physician:</div>
        <div style="width:50%;float:left;color:#00;font:bold 12px Arial, Helvetica, sans-serif;margin:0 0 0px;">{phy_name}</div>
        </p></div>
        <div class="n_ptnt_loctn">
          <p style="display:block;margin:5px 0 0px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Address:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_address}</div>
          </p>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px;">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Phone:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_phone}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
        <div style="margin:0 0 3px 0;">
          <p style="display:block;margin:0 0 5px">
          <div style="width:40%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">Fax:</div>
          <div style="width:50%;float:left;color:#444;font:400 12px/14px Arial, Helvetica, sans-serif;">{phy_fax}</div>
          </p>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
   <div class="clearfix"></div>             
              </div>           
            
                
                
                
                
                
                
<div style="width:96%; margin:0px auto;display:block;border:1px solid #4285f4;border-top:none;">        
                
 <div style="padding: 0 30px;padding-bottom:10px;">             
<div style="margin-bottom:10px; float:none; width:100%;">
<h2 style="color: #000;font-size: 14px;margin-bottom:0;font-family:Arial, Helvetica, sans-serif;"><span style="border-bottom: 2px solid #000;margin-top: 0;">PCR Results:</span></h2>
<p style="margin:0;margin-top:3px;font-size:11px;">{cancel_reason} </p>
</div>
				<!--<div style=" float :left;width: 50%;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size: 12px; margin-top:5px;margin-bottom: 20px;">

                    
     {table_data}
          
                    
				</div>-->
          <div style="width:40%; float:left; margin: 0; padding:0;">
            <span style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 13px;font-weight: bold;border-bottom: 2px solid #000;">Gross Description:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{addi_desc}</span>
     </div>
     
       <div style="width:28%; float:left; margin: 0; padding:0;">
            <span style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 13px;font-weight: bold;margin-left: 5px;border-bottom: 2px solid #000;">Location:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{location}</span>
     </div>
     
     
     
       <div style="width:31.3333%; float:left; margin: 0; padding:0;">
            <span style="border-bottom: 2px solid #000;margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 13px;font-weight: bold;">Site Indicator:</span>
       <span style="margin:0;margin-top:3px;font-size:11px;font-family:Arial, Helvetica, sans-serif;">{site_indicator}</span>
     </div>
     
     
     
     
     
     
<div style="clear:both"></div>
        

            
            <div class="sign" style="margin-top:6px;">
            
         
            <div style="clear:both"></div>
            <span style="display:block;font-family:Arial, Helvetica, sans-serif;font-size:12px;">Report verified and autogenerated on <span style="display:block;">{date_generate}</span> at <span style="display:block;">{time_report}</span>
            
            </div>
            
				<div><h2 style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 14px;"><span style="border-bottom: 2px solid #000;
margin-top: 0;">Test Method Validation:</span></h2></div>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
					<p style="margin:0;margin-top:2px;font-size:11px;">The test method used to generate the above results is real-time quantitative polymerase chain reaction (QPCR). Analytical and clinical validation studies compliant with CLIA and the College of American Pathologists(CAP) were performed at Ability Diagnostics, LLC. The performance characteristics for this assay are
available upon request.</p>
				</div>
				<div><h2 style="margin-bottom:2px;font-family:Arial, Helvetica, sans-serif;font-size: 14px;"><span style="border-bottom: 2px solid #000;margin-top: 0;">Lab Developed Test (LDT) Disclaimer:</span></h2>
				</div>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
					<p style="margin:0;margin-top:2px;font-size:11px;">This test was developed and its performance characteristics determined by Ability Diagnostics, LLC. It has not been cleared or approved by the FDA. The laboratory is regulated under CLIA as qualified to perform high-complexity testing. This test is used for clinical purposes. It should not be regarded as investigational or for research.</p>
				</div>
                </div> 
				
			
		</div>  