import { TestBed } from '@angular/core/testing';

import { DataEntryPeopleService } from './data-entry-people.service';

describe('DataEntryPeopleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataEntryPeopleService = TestBed.get(DataEntryPeopleService);
    expect(service).toBeTruthy();
  });
});
