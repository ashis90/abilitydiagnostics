<?php if (!defined('BASEPATH')) EXIT("No direct script access allowed");
class Stateexport extends MY_Controller{
		public function __construct(){
			parent::__construct();
 			$this->session_checked($is_active_session = 1);			
		}
		
		public function index(){
				$state = array(
			    'AL'=>'Alabama',
			    'AK'=>'Alaska',
			    'AZ'=>'Arizona',
			    'AR'=>'Arkansas',
			    'CA'=>'California',
			    'CO'=>'Colorado',
			    'CT'=>'Connecticut',
			    'DE'=>'Delaware',
			    'DC'=>'District of Columbia',
			    'FL'=>'Florida',
			    'GA'=>'Georgia',
			    'HI'=>'Hawaii',
			    'ID'=>'Idaho',
			    'IL'=>'Illinois',
			    'IN'=>'Indiana',
			    'IA'=>'Iowa',
			    'KS'=>'Kansas',
			    'KY'=>'Kentucky',
			    'LA'=>'Louisiana',
			    'ME'=>'Maine',
			    'MD'=>'Maryland',
			    'MA'=>'Massachusetts',
			    'MI'=>'Michigan',
			    'MN'=>'Minnesota',
			    'MS'=>'Mississippi',
			    'MO'=>'Missouri',
			    'MT'=>'Montana',
			    'NE'=>'Nebraska',
			    'NV'=>'Nevada',
			    'NH'=>'New Hampshire',
			    'NJ'=>'New Jersey',
			    'NM'=>'New Mexico',
			    'NY'=>'New York',
			    'NC'=>'North Carolina',
			    'ND'=>'North Dakota',
			    'OH'=>'Ohio',
			    'OK'=>'Oklahoma',
			    'OR'=>'Oregon',
			    'PA'=>'Pennsylvania',
			    'RI'=>'Rhode Island',
			    'SC'=>'South Carolina',
			    'SD'=>'South Dakota',
			    'TN'=>'Tennessee',
			    'TX'=>'Texas',
			    'UT'=>'Utah',
			    'VT'=>'Vermont',
			    'VA'=>'Virginia',
			    'WA'=>'Washington',
			    'WV'=>'West Virginia',
			    'WI'=>'Wisconsin',
			    'WY'=>'Wyoming',
			);

		
			 $key_val = array(
			 	'p_firstname'=>'First Name',
			 	'p_lastname'=>'Last Name',
			 	'patient_address'=>'Street',
			 	'apt'=>'Unit Number',
			 	'patient_city'=>'City',
			 	'patient_state'=>'State',
			 	'patient_zip'=>'Zip Code',
			 	'patient_phn'=>'Phone Number',
			 	'patient_dob'=>'Date of Birth',
			 	'patient_age'=>'Patient Age',
			 	'patient_sex'=>'Birth Sex',
			 	'patient_ethnicity'=>'Ethnicity',
			 	'patient_race'=>'Patient Race',
			 	
			 	'performing_lab'=>'Performning Lab',			 	
			 	
			 	'source'=>'Test Type',
			 	
			 	//'test_result' => 'Test Result',
			 	'positive_negtaive' => 'Test Result',
			 
			 	'collection_date'=>'Collection Date',
			 	//'lab_test_date' => 'Lab Test Date',
			 	'assessioning_num'=>'Accessioning Number',
			 	
			 	'physician_name'=>'Physician Name',
			 	'physician_address'=>'Physician Address',			 	
			 	'physician_phone'=>'Physician Phone Number',
			 	'physician_fax'=>'Physician Fax',	
			 	
			 	
			 	'date_received'=>'Date Received',			 	
			 	'ordering_provider_name'=>'Ordering Provider Name',
			 	'npi'=>'NPI',
			 	'ordering_provider_zip_code'=>'Ordering Provider Zip Code',
			 	'clia_number'=>'CLIA Number',
			 	'performing_facility_zip'=>'Performing Facility Zip',
			 	'date_test_ordered'=>'Date Test Ordered',
			 	'test_result_date'=>'Test Result Date',
			 	'healthcare_worker'=>'Healthcare Worker',
			 	'symptomatic'=>'Symptomatic',
			 	'date_of_symptoms'=>'Date Of Symptoms',
			 	'icu'=>'ICU',			 	
			 	'resident_in_care_facility'=>'Diagnostic facility',
			 	'hospitalized'=>'Hospitalized',
			 	'wp_abd_specimen.sd_number'=>'Medical Record Number(Sd Number)',
			 	'pregnant'=>'Pregnant'
			 );
			$state_sql= "SELECT `state_name`FROM `wp_abd_stateexport`  ORDER BY `id` DESC";

			$state_details = $this->BlankModel->customquery($state_sql);
		    common_viewloader('StateExport/index', array('state'=> $state,'state_list'=> $state_details,
				'val'=> $key_val));
		}
					/*
    | -------------------------------------------------------------------
    | Add State Export List
    | -------------------------------------------------------------------
    */ 

		public function value(){
			
            if ($this->input->post("submit") && $this->input->post("state_name") != "" && $this->input->post("submit") != "" && $this->input->post("meta") != "") {

			    $state_list = $this->input->post('state_name');
			    foreach($state_list as  $value) {
			    $data= array(
			    	'state_name' => $value,
			        'column_name' => implode(",",$this->input->post('meta'))
			        
			    );
			    $sql_insert = $this->db->insert('stateexport', $data);
			    }


           if ($sql_insert) {
           	$this->session->set_flashdata('success','Sussessfully Inserted');
					//header('location:/'.base_url().'admin/Stateexport');
			     redirect('/admin/Stateexport/');
			 }

           	
           }
           else{
           	$this->session->set_flashdata('Err','Plese Enter Any Value');
					//header('location:/'.base_url().'admin/Stateexport');
			     redirect('/admin/Stateexport/');

           }

          
	    } 
			/*
    | -------------------------------------------------------------------
    | Show State Export List
    | -------------------------------------------------------------------
    */   
		public function StateExportList(){

			$state = array(
			    'AL'=>'Alabama',
			    'AK'=>'Alaska',
			    'AZ'=>'Arizona',
			    'AR'=>'Arkansas',
			    'CA'=>'California',
			    'CO'=>'Colorado',
			    'CT'=>'Connecticut',
			    'DE'=>'Delaware',
			    'DC'=>'District of Columbia',
			    'FL'=>'Florida',
			    'GA'=>'Georgia',
			    'HI'=>'Hawaii',
			    'ID'=>'Idaho',
			    'IL'=>'Illinois',
			    'IN'=>'Indiana',
			    'IA'=>'Iowa',
			    'KS'=>'Kansas',
			    'KY'=>'Kentucky',
			    'LA'=>'Louisiana',
			    'ME'=>'Maine',
			    'MD'=>'Maryland',
			    'MA'=>'Massachusetts',
			    'MI'=>'Michigan',
			    'MN'=>'Minnesota',
			    'MS'=>'Mississippi',
			    'MO'=>'Missouri',
			    'MT'=>'Montana',
			    'NE'=>'Nebraska',
			    'NV'=>'Nevada',
			    'NH'=>'New Hampshire',
			    'NJ'=>'New Jersey',
			    'NM'=>'New Mexico',
			    'NY'=>'New York',
			    'NC'=>'North Carolina',
			    'ND'=>'North Dakota',
			    'OH'=>'Ohio',
			    'OK'=>'Oklahoma',
			    'OR'=>'Oregon',
			    'PA'=>'Pennsylvania',
			    'RI'=>'Rhode Island',
			    'SC'=>'South Carolina',
			    'SD'=>'South Dakota',
			    'TN'=>'Tennessee',
			    'TX'=>'Texas',
			    'UT'=>'Utah',
			    'VT'=>'Vermont',
			    'VA'=>'Virginia',
			    'WA'=>'Washington',
			    'WV'=>'West Virginia',
			    'WI'=>'Wisconsin',
			    'WY'=>'Wyoming',
			);

			$state_sql= "SELECT * FROM `wp_abd_stateexport`  ORDER BY `id` DESC";

			$state_details = $this->BlankModel->customquery($state_sql);
		   common_viewloader('StateExport/StateExportList', array('state_list'=> $state_details,'state_array'=> $state));

		}
			        /*
    | -------------------------------------------------------------------
    | Delete State Export List
    | -------------------------------------------------------------------
    */
		public function StateExportListDelete($did=''){
		$delete_holiday = $this->BlankModel->delete_id('wp_abd_stateexport','id',$did);
		if ($delete_holiday) {
			$this->session->set_flashdata('delete','Successfully Deleted');
			redirect('/admin/Stateexport/StateExportList/');
		}
	    }

	        /*
    | -------------------------------------------------------------------
    | Edit View State Export List
    | -------------------------------------------------------------------
    */
    function StateExportListEdit($eid='') {

            $state = array(
			    'AL'=>'Alabama',
			    'AK'=>'Alaska',
			    'AZ'=>'Arizona',
			    'AR'=>'Arkansas',
			    'CA'=>'California',
			    'CO'=>'Colorado',
			    'CT'=>'Connecticut',
			    'DE'=>'Delaware',
			    'DC'=>'District of Columbia',
			    'FL'=>'Florida',
			    'GA'=>'Georgia',
			    'HI'=>'Hawaii',
			    'ID'=>'Idaho',
			    'IL'=>'Illinois',
			    'IN'=>'Indiana',
			    'IA'=>'Iowa',
			    'KS'=>'Kansas',
			    'KY'=>'Kentucky',
			    'LA'=>'Louisiana',
			    'ME'=>'Maine',
			    'MD'=>'Maryland',
			    'MA'=>'Massachusetts',
			    'MI'=>'Michigan',
			    'MN'=>'Minnesota',
			    'MS'=>'Mississippi',
			    'MO'=>'Missouri',
			    'MT'=>'Montana',
			    'NE'=>'Nebraska',
			    'NV'=>'Nevada',
			    'NH'=>'New Hampshire',
			    'NJ'=>'New Jersey',
			    'NM'=>'New Mexico',
			    'NY'=>'New York',
			    'NC'=>'North Carolina',
			    'ND'=>'North Dakota',
			    'OH'=>'Ohio',
			    'OK'=>'Oklahoma',
			    'OR'=>'Oregon',
			    'PA'=>'Pennsylvania',
			    'RI'=>'Rhode Island',
			    'SC'=>'South Carolina',
			    'SD'=>'South Dakota',
			    'TN'=>'Tennessee',
			    'TX'=>'Texas',
			    'UT'=>'Utah',
			    'VT'=>'Vermont',
			    'VA'=>'Virginia',
			    'WA'=>'Washington',
			    'WV'=>'West Virginia',
			    'WI'=>'Wisconsin',
			    'WY'=>'Wyoming',
			);
			 $key_val = array(
			 	'p_firstname'=>'First Name',
			 	'p_lastname'=>'Last Name',
			 	'patient_address'=>'Street',
			 	'apt'=>'Unit Number',
			 	'patient_city'=>'City',
			 	'patient_state'=>'State',
			 	'patient_zip'=>'Zip Code',
			 	'patient_phn'=>'Phone Number',
			 	'patient_dob'=>'Date of Birth',
			 	'patient_age'=>'Patient Age',
			 	'patient_sex'=>'Birth Sex',
			 	'patient_ethnicity'=>'Ethnicity',
			 	'patient_race'=>'Patient Race',
			 	
			 	'performing_lab'=>'Performning Lab',			 	
			 	
			 	'source'=>'Test Type',
			 	
			 	//'test_result' => 'Test Result',
			 	'positive_negtaive' => 'Test Result',
			 	
			 	'collection_date'=>'Collection Date',
			 	//'lab_test_date' => 'Lab Test Date',
			 	'assessioning_num'=>'Accessioning Number',
			 	
			 	'physician_name'=>'Physician Name',
			 	'physician_address'=>'Physician Address',			 	
			 	'physician_phone'=>'Physician Phone Number',
			 	'physician_fax'=>'Physician Fax',	
			 	
			 	
			 	'date_received'=>'Date Received',			 	
			 	'ordering_provider_name'=>'Ordering Provider Name',
			 	'npi'=>'NPI',
			 	'ordering_provider_zip_code'=>'Ordering Provider Zip Code',
			 	'clia_number'=>'CLIA Number',
			 	'performing_facility_zip'=>'Performing Facility Zip',
			 	'date_test_ordered'=>'Date Test Ordered',
			 	'test_result_date'=>'Test Result Date',
			 	'healthcare_worker'=>'Healthcare Worker',
			 	'symptomatic'=>'Symptomatic',
			 	'date_of_symptoms'=>'Date Of Symptoms',
			 	'icu'=>'ICU',			 	
			 	'resident_in_care_facility'=>'Diagnostic facility',
			 	'hospitalized'=>'Hospitalized',
			 	'wp_abd_specimen.sd_number'=>'Medical Record Number(Sd Number)',
			 	'pregnant'=>'Pregnant'
			 );
			 
				    $conditions = "(`id` = '".$eid."')";
				   	$select_fields = '*';
				   	$is_multy_result = 1;
				   	$state_data = $this->BlankModel->getTableData('wp_abd_stateexport', $conditions, $select_fields, $is_multy_result );
					   common_viewloader('StateExport/StateExportListEdit', array('statexpt_details'=> $state_data,'val'=> $key_val,'state_array'=> $state));
			        //}
			    }
		    	        /*
    | -------------------------------------------------------------------
    | Edit Update State Export List
    | -------------------------------------------------------------------
    */
		    public function StateExportListUpdate()
		    {  

		    	if ($this->input->post("submit") && $this->input->post("submit") != "") {

		    	$conditions = "(`id` = '".$this->input->post("state_export_id")."')";


					$export_data = array('column_name' => implode(",",$this->input->post('column_name'))
				         );

					$update_export = $this->BlankModel->editTableData('wp_abd_stateexport',$export_data,
									$conditions);
					if ($update_export) {
						$this->session->set_flashdata('success','Successfully Updated');
			            redirect('/admin/Stateexport/StateExportList/');
					}
					else{
           	       $this->session->set_flashdata('Err','Update failed');
			       redirect('/admin/Stateexport/StateExportList/');

                }
				}
    		}					

}

