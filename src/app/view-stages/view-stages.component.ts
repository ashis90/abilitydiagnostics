import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'cm-view-stages',
  templateUrl: './view-stages.component.html',
  styleUrls: ['./view-stages.component.css']
})
export class ViewStagesComponent implements OnInit {

  constructor(private http:HttpClient, public itemsService:SpecimenService,public route:Router,public activatedRoute : ActivatedRoute, private _location: Location) { }
  postsArray:any = [] ;
  load_status:boolean = true;
  ticked:any;

  ngOnInit() 
  {
    if(this.load_status==true)
    {
        var id=this.activatedRoute.snapshot.paramMap.get('id');
      var dataType = this.activatedRoute.snapshot.paramMap.get('dataType');
      let params = { 'id': id, 'dataType': dataType };
      this.itemsService.makeStageViewRequest(params).subscribe(data => {
        this.postsArray = data;
      });

    }
  }
  go_back()
  {
    this._location.back();
  }

}
