import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-batch-delete',
  templateUrl: './batch-delete.component.html',
  styleUrls: ['./batch-delete.component.css']
})
export class BatchDeleteComponent implements OnInit {

  public data : any;
  specimens_list:any = [];
  post_array:any;
  batch_review:any;
  specimen_count:any;
  loading:boolean;
  no_data:any;
  no_data_status:boolean=false;
  filterData:any;
  public popoverTitle: string = 'Do you want to delete?';
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
    ) { }

  ngOnInit() {
    this.spinner.show();
      this.getBatchDeletetData();  
  }

  getBatchDeletetData() {
    this.itemsService.getBatchDelete().subscribe(data => {          
    this.post_array = data;
    this.data = this.post_array.batch_delete;
    this.specimen_count = this.post_array.specimen_count;
    console.log(this.post_array);
      if(this.post_array.status === '1')
      {
        this.spinner.hide();
 
      }else if(this.post_array.status === '0')
      {
        this.no_data_status=true;
        this.spinner.hide();
        this.no_data="No Data Found.";
      }
               
    });
  }

  deleteBatchData(batch:any){
    let params;
    params= {batch_no:batch};
    this.itemsService.batchDelete(params).subscribe(data => { 
      this.spinner.show();
      this.getBatchDeletetData();  
      this.toastr.info('Batch data deleted successfully', 'Success', {
        timeOut: 3000
      });
    });
  }

  search(term: string) {
    let allData:any;
    allData = this.post_array.batch_delete;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.batch_no.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
    
  }

}


