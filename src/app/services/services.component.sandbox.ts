import { sandboxOf } from 'angular-playground';
import { ServicesComponent } from './services.component';

export default sandboxOf(ServicesComponent)
  .add('Services Component', {
    template: `<cm-services></cm-services>`
  });
