import { NgModule } from "@angular/core";
import {
  RouterModule,
  Routes,
  PreloadAllModules,
  NoPreloading
} from "@angular/router";
import { PreloadModulesStrategy } from "./core/strategies/preload-modules.strategy";
import { PrivacypolicyComponent } from "./privacypolicy/privacypolicy.component";
import { ServicedetComponent } from "./servicedet/servicedet.component";
import { WoundCareComponent } from "./wound-care/wound-care.component";
import { SuspendedComponent } from "./specimen/suspended/suspended.component";
import { SpecimensListComponent } from "./specimen/specimens-list/specimens-list.component";
import { SpecimenAddComponent } from "./specimen/specimen-add/specimen-add.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MenuComponent } from "./menu/menu.component";
import { MyaccountComponent } from "./myaccount/myaccount.component";
import { QcConfirmationComponent } from "./qc-confirmation/qc-confirmation.component";
import { CancelledComponent } from "./specimen/cancelled/cancelled.component";
import { StagesComponent } from "./specimen/stages/stages.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./auth.guard";
import { HomeComponent } from "./home/home.component";
import { ViewSpecimenComponent } from "./view-specimen/view-specimen.component";
import { ViewStagesComponent } from "./view-stages/view-stages.component";
import { DeliveryToExtractionComponent } from "./pcr/delivery-to-extraction/delivery-to-extraction.component";
import { DeliveryToPcrComponent } from "./pcr/delivery-to-pcr/delivery-to-pcr.component";
import { AssignWellStageComponent } from "./pcr/assign-well-stage/assign-well-stage.component";
// import { SpecimenNxtStageComponent } from './specimen-nxt-stage/specimen-nxt-stage.component';
import { SpecimensEditComponent } from "./specimen/specimens-edit/specimens-edit.component";
import { LabAnalyticsComponent } from "./lab-analytics/lab-analytics.component";
import { HistoPcrAnalyticsComponent } from "./histo-pcr-analytics/histo-pcr-analytics.component";
import { LabDataComponent } from "./lab-data/lab-data.component";
import { ShortCodeAnalyticsComponent } from "./short-code-analytics/short-code-analytics.component";
import { DataAnalysisComponent } from "./pcr/data-analysis/data-analysis.component";
import { DiagnosesAnalysisComponent } from "./diagnoses-analysis/diagnoses-analysis.component";
import { AvgTurnTimeComponent } from "./avg-turn-time/avg-turn-time.component";
import { ManualReviewComponent } from "./pcr/manual-review/manual-review.component";
import { SpecimenTrackComponent } from "./specimen-track/specimen-track.component";
import { PendingSpecimensListComponent } from "./reports/pending-specimens-list/pending-specimens-list.component";
import { PendingReportsComponent } from "./reports/pending-reports/pending-reports.component";
import { SubmittedReportsComponent } from "./reports/submitted-reports/submitted-reports.component";
import { AmendedReportsComponent } from "./reports/amended-reports/amended-reports.component";
import { SlideAnalyticsComponent } from "./slide-analytics/slide-analytics.component";
import { HistoAvgStageComponent } from "./histo-avg-stage/histo-avg-stage.component";
import { PcrAvgStageComponent } from "./pcr-avg-stage/pcr-avg-stage.component";
import { ReportGenerateComponent } from "./pcr/report-generate/report-generate.component";
import { BatchDeleteComponent } from "./pcr/batch-delete/batch-delete.component";
import { PcrSubmitedReportsComponent } from "./pcr/pcr-submited-reports/pcr-submited-reports.component";
import { ArchiveDocumentsComponent } from "./pcr/archive-documents/archive-documents.component";
import { ClinicalReportComponent } from "./clinical-report/clinical-report.component";
import { QualityReportsComponent } from "./analytics/quality-reports/quality-reports.component";
import { PhysicianNotificationAeComponent } from "./physician-notification-ae/physician-notification-ae.component";
import { PhysicianNotesComponent } from "./physician-notes/physician-notes.component";
import { PendingReportEditComponent } from "./reports/pending-reports/pending-report-edit/pending-report-edit.component";
import { ViewAmendedReportComponent } from "./reports/amended-reports/view-amended-report/view-amended-report.component";
import { AddDataEntryPeopleComponent } from "./data-entry-people/add-data-entry-people/add-data-entry-people.component";
import { EditDataEntryPeopleComponent } from "./data-entry-people/edit-data-entry-people/edit-data-entry-people.component";
import { ViewDataEntryPeopleComponent } from "./data-entry-people/view-data-entry-people/view-data-entry-people.component";
import { DataEntryPeopleListComponent } from "./data-entry-people/data-entry-people-list/data-entry-people-list.component";
import { NotificationListComponent } from "./notifications/notification-list/notification-list.component";
import { SurveyComponent } from "./survey/survey.component";
import { EditSubmittedReportComponent } from "./edit-submitted-report/edit-submitted-report.component";
import { ArchiveComponent } from "./archive/archive.component";
// import { AddSpecimenReportComponent } from './reports/add-specimen-report/add-specimen-report.component';
import { EditPendingReportComponent } from "./reports/edit-pending-report/edit-pending-report.component";
import { PcrStagesComponent } from "./pcr/pcr-stages/pcr-stages.component";
import { ContactComponent } from "./contact/contact.component";
import { MainComponent } from "./main/main.component";
import { DeliveryExtComponent } from "./delivery-ext/delivery-ext.component";
import { AssignWellComponent } from "./assign-well/assign-well.component";
import { PendingStageDetailsComponent } from "./specimen/pending-stage-details/pending-stage-details.component";
import { DeliveryToExtractionStageComponent } from "./pcr/delivery-to-extraction-stage/delivery-to-extraction-stage.component";
import { FullSpecimenTarckingComponent } from "./full-specimen-tarcking/full-specimen-tarcking.component";
import { DystrophicNailPresentmentComponent } from "./dystrophic-nail-presentment/dystrophic-nail-presentment.component";
import { ImportSpecimenDataComponent } from "./import-specimen-data/import-specimen-data.component";
import { ArchiveSpecimenListComponent } from "./specimen/archive-specimen-list/archive-specimen-list.component";
import { ArchiveSubmittedReportsComponent } from "./reports/archive-submitted-reports/archive-submitted-reports.component";
import { ArchivePcrSubmitedReportsComponent } from "./pcr/archive-pcr-submited-reports/archive-pcr-submited-reports.component";
import { CertificateComponent } from "./certificate/certificate.component";
import { ReferenceLabReportComponent } from "./analytics/reference-lab-report/reference-lab-report.component";
import { UtiComponent } from "./uti/uti.component";
import { RppComponent } from "./rpp/rpp.component";
const app_routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/home",
    canActivate: [AuthGuard]
  },
  { path: "about", loadChildren: "app/about/about.module#AboutModule" },
  {
    path: "certificate",
    component: CertificateComponent
    //loadChildren: "app/certificate/certificate.module#CertificateModule"
  },
  {
    path: "reference-lab-report",
    component: ReferenceLabReportComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  //{ path: 'home', loadChildren: 'app/home/home.module#HomeModule', canActivate:[AuthGuard] },
  { path: "home", component: HomeComponent },
  {
    path: "nails",
    loadChildren: "app/services/services.module#ServicesModule"
  },
  { path: "wound-care", component: WoundCareComponent },
  { path: "contact", component: ContactComponent },
  // { path: 'pay', loadChildren: 'app/pay/pay.module#PayModule' },
  { path: "privacy", component: PrivacypolicyComponent },
  { path: "login", component: LoginComponent },
  { path: "servicedetail/:id", component: ServicedetComponent },
  { path: "survey", component: SurveyComponent },
  { path: "white-papers", component: DystrophicNailPresentmentComponent },
  { path: "uti", component: UtiComponent },
  { path: "rpp", component: RppComponent },

  //Comon Section

  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "office_manager",
        "data_entry_oparator",
        "generic_account",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "myaccount",
    component: MyaccountComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "office_manager",
        "data_entry_oparator",
        "generic_account",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "physician-notification-ae/:id/:type",
    component: PhysicianNotificationAeComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "office_manager",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "physician-notes",
    component: PhysicianNotesComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "office_manager",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },

  //Lab Analytics
  {
    path: "lab-analytics",
    component: LabAnalyticsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "histo-pcr-analytics",
    component: HistoPcrAnalyticsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "short-code-analytics",
    component: ShortCodeAnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "lab-data-analytics",
    component: LabDataComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "diagnoses-analytics",
    component: DiagnosesAnalysisComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "avg-turn-around",
    component: AvgTurnTimeComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "slide-analytics",
    component: SlideAnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "histo-avg-stage-time",
    component: HistoAvgStageComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "labdoctor", "aps_sales_manager"] }
  },
  {
    path: "pcr-avg-stage-time",
    component: PcrAvgStageComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "specimen-tracking",
    component: SpecimenTrackComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "clinical-report",
    component: ClinicalReportComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "quality-reports",
    component: QualityReportsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },
  {
    path: "full-specimen-tracking",
    component: FullSpecimenTarckingComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "laboratory",
        "labdoctor",
        "data_entry_oparator",
        "aps_sales_manager"
      ]
    }
  },

  //Specimens Sections
  {
    path: "specimens-list",
    component: SpecimensListComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "archive-specimens-list",
    component: ArchiveSpecimenListComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "specimens-add",
    component: SpecimenAddComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "specimens-edit/:id/:type",
    component: SpecimensEditComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "suspended",
    component: SuspendedComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "qc-confirmation",
    component: QcConfirmationComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  {
    path: "cancelled-specimens",
    component: CancelledComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "listed-specimen-stages",
    component: StagesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "view-specimen/:id/:dataType",
    component: ViewSpecimenComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator", "aps_sales_manager"] }
  },
  // { path: 'specimen-next-page/:id/:stageid', component: SpecimenNxtStageComponent, canActivate:[AuthGuard], data: { roles: ['laboratory','aps_sales_manager'] } },
  {
    path: "specimen-next-page",
    loadChildren:
      "app/specimen-nxt-stage/specimen-nxt-stage.module#SpecimenNxtStageModule"
  },
  {
    path: "view-specimen-stages/:id/:dataType",
    component: ViewStagesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "pending-stage-details/:stageid",
    component: PendingStageDetailsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "upload-specimen",
    component: ImportSpecimenDataComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "data_entry_oparator"] }
  },

  //Pcr Sections
  {
    path: "pcr-stages",
    component: PcrStagesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "delivery-to-extraction",
    component: DeliveryToExtractionComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },

  {
    path: "delivery-to-ext",
    component: DeliveryExtComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "delivery-to-extraction-stage/:id/:stageid",
    component: DeliveryToExtractionStageComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "assign-well",
    component: AssignWellComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },

  {
    path: "assign-well-stage",
    component: AssignWellStageComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "delivery-to-pcr",
    component: DeliveryToPcrComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "data-analysis",
    component: DataAnalysisComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  // { path: 'manual-review', component: ManualReviewComponent, canActivate:[AuthGuard], data: { roles: ['laboratory'] } },
  {
    path: "report-generate",
    component: ReportGenerateComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "batch-delete",
    component: BatchDeleteComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "pcr-submitted-reports",
    component: PcrSubmitedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager", "labdoctor"] }
  },
  {
    path: "archive-pcr-submitted-reports",
    component: ArchivePcrSubmitedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },
  {
    path: "archive-documents",
    component: ArchiveDocumentsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["laboratory", "aps_sales_manager"] }
  },

  //Report Sections
  {
    path: "archive",
    component: ArchiveComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor"] }
  },
  // { path: 'add-specimen-report/:id', component: AddSpecimenReportComponent, canActivate:[AuthGuard], data: { roles: ['labdoctor','generic_account'] } },
  {
    path: "add-specimen-report",
    loadChildren:
      "app/reports/add-specimen-report/add-specimen-report.module#AddSpecimenReportModule",
    data: { roles: ["labdoctor", "generic_account"] }
  },
  {
    path: "pending-specimen-list",
    component: PendingSpecimensListComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account"] }
  },
  {
    path: "pending-reports",
    component: PendingReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account"] }
  },
  {
    path: "submitted-reports",
    component: SubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account", "aps_sales_manager"] }
  },
  {
    path: "archive-submitted-reports",
    component: ArchiveSubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account"] }
  },
  {
    path: "edit-submit-report/:id",
    component: EditSubmittedReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor"] }
  },
  {
    path: "amended-reports",
    component: AmendedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account"] }
  },
  {
    path: "view-amended-report/:id",
    component: ViewAmendedReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor"] }
  },
  {
    path: "pending-report-edit/:id",
    component: PendingReportEditComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor"] }
  },
  {
    path: "edit-pending-report/:id",
    component: EditPendingReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ["labdoctor", "generic_account"] }
  },

  //Data entry Section
  {
    path: "view-data-entry-people/:id",
    component: ViewDataEntryPeopleComponent,
    canActivate: [AuthGuard],
    data: { roles: ["office_manager"] }
  },
  {
    path: "data-entry-people-list",
    component: DataEntryPeopleListComponent,
    canActivate: [AuthGuard],
    data: { roles: ["office_manager"] }
  },
  {
    path: "add-data-entry-people",
    component: AddDataEntryPeopleComponent,
    canActivate: [AuthGuard],
    data: { roles: ["office_manager"] }
  },
  {
    path: "edit-data-entry-people/:id",
    component: EditDataEntryPeopleComponent,
    canActivate: [AuthGuard],
    data: { roles: ["office_manager"] }
  },
  {
    path: "notification-list",
    component: NotificationListComponent,
    canActivate: [AuthGuard],
    data: { roles: ["data_entry_oparator"] }
  },

  { path: "restricted", component: MainComponent },
  { path: "**", pathMatch: "full", redirectTo: "/home" } // catch any unfound routes and redirect to home page
];

@NgModule({
  imports: [
    RouterModule.forRoot(app_routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
