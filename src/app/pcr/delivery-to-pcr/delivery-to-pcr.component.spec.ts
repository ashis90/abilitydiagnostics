import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryToPcrComponent } from './delivery-to-pcr.component';

describe('DeliveryToPcrComponent', () => {
  let component: DeliveryToPcrComponent;
  let fixture: ComponentFixture<DeliveryToPcrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryToPcrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryToPcrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
