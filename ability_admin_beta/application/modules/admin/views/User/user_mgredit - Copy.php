<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  /*  if($service['service_status']=="Active"){
       $redirect_url=base_url().'admin/user/';
   }else{
       $redirect_url=base_url().'admin/user/'.$_REQUEST['user_id'];
   }  */
  
  if (!empty($user_data)) {
        // echo "<pre>";
        // print_r($user_data['user_nicename']);
        // exit();
        $user_name = $user_data['user_login'];
        $user_id =  $user_data['ID'];
        $user_email = $user_data['user_email'];
        // $nickname = $user_data['user_nicename'];
        $user_website = $user_data['user_url'];
        $fname='';$lname='';$wp_abd_capabilities='';$nickname='';$mobile='';$address='';$fax='';$report_recive_way='';$report_recive_way=''; $company_logo='';
       
    if (!empty($user_id)) {
        $user_role = get_user_role($user_id);
        $fname = get_user_meta_value($user_id,'first_name',TRUE);
        $lname = get_user_meta_value($user_id,'last_name',TRUE);
        $wp_abd_capabilities = get_user_meta_value($user_id,'wp_abd_capabilities',TRUE);
        $nickname = get_user_meta_value($user_id,'nickname',TRUE);
        $mobile = get_user_meta_value($user_id,'_mobile',TRUE);
        $address = get_user_meta_value($user_id,'_address',TRUE);
        $spe_stage = get_user_meta_value($user_id,'specimen_stages',TRUE);
       
       if($user_role == 'physician' || $user_role == 'partner'){
          $fax = get_user_meta_value($user_id,'fax',TRUE);
      		$anthr_fax_number = get_user_meta_value($user_id,'anthr_fax_number',TRUE);
        }
       
      if($user_role == 'physician'){
        $report_recive_way = get_user_meta_value($user_id,'report_recive_way',TRUE);
        $npi = get_user_meta_value($user_id,'npi',TRUE);
        $anthr_fax_number = get_user_meta_value($user_id,'anthr_fax_number',TRUE);
        $cell_phone = get_user_meta_value($user_id,'cell_phone',TRUE);
        $manager_contact_name = get_user_meta_value($user_id,'manager_contact_name',TRUE);
        $manager_cell_number = get_user_meta_value($user_id,'manager_cell_number',TRUE);
        $assign_specialty = get_user_meta_value($user_id,'assign_specialty',TRUE);
        $clinic_name = get_user_meta_value($user_id,'_clinic_name',TRUE);
        $added_by = get_user_meta_value($user_id,'added_by',TRUE);
        if (!empty($added_by)) {
          $added_by_value = get_user_meta_value($added_by,'first_name',TRUE).' '.
                          get_user_meta_value($added_by, 'last_name', TRUE);
        } else{
          $added_by_value = '';
        }
                         

        $combined_by = get_user_meta_value($user_id,'combined_by',TRUE);
        if (!empty($combined_by)) {
          $combined_by_value = get_user_meta_value($combined_by,'first_name',TRUE).' '.
                            get_user_meta_value($combined_by, 'last_name', TRUE);
        } else{
          $combined_by_value = '';
        }


        $assign_clinic = get_user_meta_value($user_id,'_assign_clinic',TRUE);
        if(!empty($assign_clinic)){
          $assign_clinic_value = get_user_meta_value($assign_clinic,'first_name',TRUE).' '.
                            get_user_meta_value($assign_clinic, 'last_name', TRUE);
        } else{
          $assign_clinic_value= '';
        }


        $assign_partner = get_user_meta_value($user_id,'_assign_partner',TRUE);
        if(!empty($assign_partner)){
          $assign_partner_value = get_user_meta_value($assign_partner,'first_name',TRUE).' '.
                            get_user_meta_value($assign_partner, 'last_name', TRUE);
        } else{
          $assign_partner_value= '';
        }

      }
       
      if ($user_role == 'partner') {
       	$company_logo = get_user_meta_value($user_id,'company_logo',TRUE);
      }
    }
  }
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1> Users Management </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Users List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <?php
            if ($this->session->flashdata('email')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('email');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>

            
            <h4 class="title">User Edit</h4><br>
            <?php
            if ($this->session->flashdata('wrong')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('wrong');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
          </div>
          <form action="<?php echo base_url().'admin/user/userEditing/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $_REQUEST['user_id'];?>" />
            <div class="form-group">
              <label for="exampleTextarea">User Role </label>
              <select name="user_role" id="user_role" class="form-control">
                <option value="">Select User Role</option>
                <?php 
                  foreach($roleList as $rolval)
                  {
                  ?>
                <option value="<?php echo $rolval['role_shortname'];?>" <?php if($rolval['role_shortname']==$user_role){?> selected="selected" <?php }?>><?php echo ucwords(str_replace('_', ' ', $rolval['role_name']));?></option>
                <?php 
                  }
                  ?>
              </select>
              <?php echo form_error('user_role','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> User Name </label>
              <input type="text" id="user_username" name="user_username" value="<?php echo $user_name; ?>">
              <?php echo form_error('user_username','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> First Name </label>
              <input type="text" id="user_firstname" name="user_firstname" placeholder="Enter User First name" value="<?php echo $fname; ?>">
              <?php echo form_error('user_firstname','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Last Name </label>
              <input type="text" id="user_lastname" name="user_lastname" placeholder="Enter User Lastname" value="<?php echo $lname; ?>">
              <?php echo form_error('user_lastname','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group col-md-12">
              <label for="exampleTextarea"> Nick Name </label>
              <input type="text" id="user_nickname" name="user_nickname" placeholder="Enter User Nickname" value="<?php echo $nickname; ?>">
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> New Password </label>
              <input type="password" id="new_password" class="form-control" name="new_password" placeholder="Enter Password">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>

            </div>

            <div class="form-group">
              <label for="exampleTextarea"> Confirm Password </label>
              <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="Enter Confirm Password">
              <?php echo form_error('confirm_password','<div class="text-danger">','</div>'); ?>
            </div>



            <b>Contact Info:</b> <br/><br/> 
            <div class="form-group">
              <label for="exampleTextarea"> Email </label>
              <input type="text" id="user_email" name="user_email" placeholder="Enter User Email" value="<?php echo $user_email;?>">
              <?php echo form_error('user_email','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Website </label>
              <input type="text" id="user_url" name="user_url" placeholder="Enter User Website" value="<?php echo $user_website; ?>">
            </div>


            <b> Extra Profile Information: </b> <br/><br/> 
            <div class="form-group">
              <label for="exampleTextarea"> Mobile </label>
              <input type="text" id="user_mobile" name="user_mobile" placeholder="Enter User Mobile" value="<?php echo $mobile; ?>">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>
            </div>


            <?php if($user_role =='physician' || $user_role =='partner'){ ?>
            <div class="form-group">
              <label for="exampleTextarea"> Fax </label>
              <input type="text" id="user_fax" name="user_fax" placeholder="Enter User Fax" value="<?php echo $fax; ?>">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>
            </div>
            <?php }?>


            <?php if($user_role =='partner'){ ?>
            <div class="form-group">
              <img src="<?php echo  $company_logo;?>" height="100px" width="100px">
              <label for="exampleTextarea"> Company logo </label>
              <!-- <input type="file" name="company_logo" id="company_logo" class="form-control"> -->
              <input type="file" name="company_logo" id="company_logo" class="form-control">
            </div>
            <?php } ?>


            <div class="form-group">
              <label for="exampleTextarea"> Address </label>
              <textarea name="user_address" rows="3" cols="80" placeholder="User address" class="form-control"><?php echo $address; ?></textarea>
            </div>


            <?php if($user_role == 'physician' ){ ?>
            <b> Additional ways to get reports: </b> <br/><br/>
            <div>
              <?php
                $receiveArr = explode(',',$report_recive_way);
                ?>
              <label>Note :***Physicians only.</label>
              <input type="checkbox" name="way_recive_fax" value="Fax" id="way_recive_fax" <?php if(count($receiveArr)>0){if(in_array('Fax',$receiveArr)){?> checked="checked" <?php }}?>> Fax
              <input type="checkbox" name="way_recive_email" value="Email" id="way_recive_email" <?php if(count($receiveArr)>0){if(in_array('Email',$receiveArr)){?> checked="checked" <?php }}?>> Email
              <input type="checkbox" name="way_recive_none" value="None" id="way_recive_none" <?php if(count($receiveArr)>0){if(in_array('None',$receiveArr)){?> checked="checked" <?php }}?>> None
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Another Fax number : </label>
              <input type="text" id="anthr_fax_number" name="anthr_fax_number" placeholder="Enter Another Fax" value="<?php echo $anthr_fax_number; ?>">
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Cell Phone : </label>
              <input type="text" id="cell_phone" name="cell_phone" placeholder="Enter Cell Phone" value="<?php echo $cell_phone; ?>">
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Office Manager Contact Name : </label>
              <input type="text" id="manager_contact_name" name="manager_contact_name" placeholder="Enter  Office Manager Contact Name" value="<?php echo $manager_contact_name; ?>">
            </div>  

            <div class="form-group">
              <label for="exampleTextarea"> Office Manager Cell Number : </label>
              <input type="text" id="manager_cell_number" name="manager_cell_number" placeholder="Enter  Office Manager Cell Number" value="<?php echo $manager_cell_number; ?>">
            </div>

            <div class="form-group">
              <label for="exampleTextarea"> National Provider Identifier : </label>
              <input type="text" id="npi" name="npi" placeholder="Enter National Identifier" value="<?php echo $npi; ?>">
            </div>

            
            <div class="form-group">
              <label for="exampleTextarea"> Assign Physician to Clinic Profile: </label>
              <select name="assign_clinic" id="assign_clinic" class="form-control">
                 <option value="">Select Clinic</option>
                <?php foreach($clinicList as $clinic) {
                  ?>
                 <option value="<?php echo $clinic['ID'];?>" <?php if($clinic['firstname']." ".$clinic['lastname']==$assign_clinic_value){?> selected="selected" <?php }?>><?php echo $clinic['firstname']." ".$clinic['lastname']?></option>
                <?php
                  }
                ?>
              </select>
            </div>


            <div class="form-group">
              <label for="exampleTextarea"> Combine Physicians Account: </label>
              <select name="combined_by" id="combined_by" class="form-control">
                 <option value="">Select Combine Physicians Account</option>
                <?php foreach($combinePhysicianList as $combine) {  ?>
                  <option value="<?php echo $combine['ID'];?>" <?php if($combine['firstname']." ".$combine['lastname']==$combined_by_value){?> selected="selected" <?php }?>><?php echo $combine['firstname']." ".$combine['lastname']?>
                  </option>
                <?php
                  }
                ?>
              </select>
            </div>


            <div class="form-group">
              <label for="exampleTextarea">Sales Representative : </label>
              <select name="added_by" id="added_by" class="form-control">
                 <option value="">Select Sales Representative</option>
                <?php foreach($salesList as $sales) {  ?>
                  <option value="<?php echo $sales['ID'];?>" <?php if($sales['firstname']." ".$sales['lastname']==$added_by_value){?> selected="selected" <?php }?>><?php echo $sales['firstname']." ".$sales['lastname']?>
                  </option>
                <?php
                  }
                ?>
              </select>
            </div> 


            <div class="form-group">
              <label for="exampleTextarea">Clinic Name : </label>
              <input type="text" id="clinic_name" name="clinic_name" placeholder="Enter Clinic Name" value="<?php echo $clinic_name; ?>">
            </div>  

            
            <div class="form-group">
              <label for="exampleTextarea">Assign Physician's Specialty: </label>
              <select name="assign_specialty" id="assign_specialty" class="form-control">
                 <option value="">Select Physician's Specialty</option>

                <?php foreach($assignSpecialityList as $speciality) {  ?>

                  <option value="<?php echo $speciality['meta_value'];?>" <?php if($speciality['meta_value']==$assign_specialty){?> selected="selected" <?php }?>><?php echo $speciality['meta_value']; ?>
                  </option>

                 <!-- <option value="<?php echo $speciality['meta_value']; ?>"><?php echo $speciality['meta_value']; ?></option> -->
                <?php
                  }
                ?>
              </select>
            </div> 


            <div class="form-group">
              <label for="exampleTextarea"> Assign with Partner Company: </label>
              <select name="assign_partner" id="assign_partner" class="form-control">
                 <option value="">Select Partner</option>
                <?php foreach($partnerList as $partner) {
                  ?>

                  <option value="<?php echo $partner['ID'];?>" <?php if($partner['firstname']." ".$partner['lastname']==$assign_partner_value){?> selected="selected" <?php }?>><?php echo $partner['firstname']." ".$partner['lastname']?></option>

                <?php
                  }
                ?>
              </select>
            </div>      
            <?php } ?>


            <!-- <div class="form-group">
              <label for="exampleTextarea"> Assign with Partner Company: </label>
              <select name="assign_partner" id="assign_partner" class="form-control">
                 <option value="">Select Partner</option>
                <?php foreach($partnerList as $partner) {
                  ?>

                  <option value="<?php echo $partner['ID'];?>" <?php if($partner['firstname']." ".$partner['lastname']==$assign_partner_value){?> selected="selected" <?php }?>><?php echo $partner['firstname']." ".$partner['lastname']?></option>

                <?php
                  }
                ?>
              </select>
            </div> -->


            <?php 
            if($user_role ==='laboratory' || $user_role ==='aps_sales_manager'){ ?>
            <div class="form-group">
              <label for="exampleTextarea">Access Control:</label><br> 
              <?php $stage= explode(",",$spe_stage); ?>
              <input type="checkbox" name="acc_cnt[]" value="1" <?php if(in_array("1",$stage)) {  echo "checked"; } ?>>Delivery To Lab<br>
              <input type="checkbox" name="acc_cnt[]" value="2" <?php if(in_array("2",$stage)) {  echo "checked"; } ?>>Gross<br>
              <input type="checkbox" name="acc_cnt[]" value="3" <?php if(in_array("3",$stage)) {  echo "checked"; } ?>>Microtomy<br>
              <input type="checkbox" name="acc_cnt[]" value="4" <?php if(in_array("4",$stage)) {  echo "checked"; } ?>>QC Check<br>
            </div>
            <?php } ?>
            

            

            <div class="form-group">
              <label for="exampleTextarea"> User status: </label>
              <select name="user_status" id="user_status" class="form-control">
                <option value="">Select Status</option>
                <option value="Active" <?php if( isset($user_data['status']) && $user_data['status']=='Active'){ echo "selected"; }?>>Active</option>
                <option value="Inactive" <?php if( isset($user_data['status']) && $user_data['status']=='Inactive'){ echo "selected"; }?>>Inactive</option>
              </select>
            </div>



            <button type="submit" class="btn btn-primary ban-sbmt">Update User</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
