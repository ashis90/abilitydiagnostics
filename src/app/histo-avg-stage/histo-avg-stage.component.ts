import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-histo-avg-stage',
  templateUrl: './histo-avg-stage.component.html',
  styleUrls: ['./histo-avg-stage.component.css']
})
export class HistoAvgStageComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  data;
  no_data = false;
  load_more_data: any = 0;
  isSearch: boolean = false;
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({

      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "acc": new FormControl(),
      "dataType": new FormControl('general', Validators.required)

    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'MM/DD/YYYY'

    });

  }

  ngOnInit() {
    if (this.load_status == true) {
      this.spinner.show();
      this.no_data = false;
      this.get_avg_data();
    }

  }

  get_avg_data() {
    this.isSearch = false;
    this.itemsService.get_histo_avg_time().subscribe(data => {
      this.spinner.hide();
      this.postsArray = data;
      this.data = this.postsArray.details;
    });
  }
  resetForm() {
    this.searchForm.reset();
    this.spinner.show();
    this.get_avg_data();
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.no_data = false;
      this.isSearch = true;
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), 'to_date': this.convertDate(this.searchForm.value.to_date), 'acc': this.searchForm.value.acc, "dataType": this.searchForm.value.dataType };
      this.itemsService.search_histo_avg_time(this.sendData).subscribe(data => {

        this.postsArray = data;
        this.data = this.postsArray.details;
        if (this.postsArray.status === '1') {
          this.spinner.hide();
        }
        if (this.postsArray.status === '0') {
          this.no_data = true;
          this.spinner.hide();
        }
      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }

  }

  load_more() {
    this.load_more_data = this.load_more_data + 10;
    this.isSearch = false;
    this.spinner.show();
    let params = { 'load': this.load_more_data };
    this.itemsService.get_more_histo_avg_time(params).subscribe(data => {
      this.spinner.hide();
      this.postsArray = data;
      if (this.postsArray) {
        this.postsArray.details.forEach(element => {
          this.data.push(element);
        });
      }
    });
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  load_last() {
    this.isSearch = false;
    this.spinner.show();
    this.itemsService.load_last_one_month().subscribe(data => {
      this.postsArray = data;
      if (this.postsArray.status == '1') {
        this.spinner.hide();
        this.no_data = false;
        this.data = this.postsArray.details;
        this.isSearch = true;
      }
      if (this.postsArray.status == '0') {
        this.data = "";
        this.spinner.hide();
        this.no_data = true;
      }

    });
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
