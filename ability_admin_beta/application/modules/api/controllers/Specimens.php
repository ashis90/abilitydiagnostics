<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');

class Specimens extends MY_Controller {
//	private $DB2 = '';
    function __construct() {
        parent::__construct();
        $this->load->library('phpqrcode/qrlib');
        $this->load->library('m_pdf');
	   date_default_timezone_set('MST7MDT');
//	   $this->DB2 = $this->load->database('aps_db', TRUE);
    }
  
    function index()
    {
	  		//Histo report folder archive
		
		$files_data=array();
		$dist_year = $this->db->query("SELECT DISTINCT YEAR(`create_date`) as 'dist_year' FROM `wp_abd_nail_pathology_report_archive`")->result_array();

		foreach ($dist_year as $yr) {
			$year = $yr['dist_year'];
			$files_data[$year] = $this->db->query("SELECT `nail_pdf`, YEAR(`create_date`) as 'dist_year' FROM `wp_abd_nail_pathology_report_archive` WHERE YEAR(`create_date`) LIKE '%".$year."%'")->result_array();
		}
		$nailPdfPath = FCPATH."assets/uploads/histo_report_pdf/";
		foreach ($files_data as $file_key => $file_val) {
			$archive_folder_path = $nailPdfPath.$file_key;
			if(!file_exists($archive_folder_path)) {
				mkdir($archive_folder_path, 0777);
			} 
			foreach ($file_val as $files_name) {
				$file_temp_name = $files_name['nail_pdf'];
				$old_file_path = $nailPdfPath.$file_temp_name;
				$new_file_path = $archive_folder_path.'/'.$file_temp_name;
	
				if(file_exists($old_file_path)) {
					chmod ($new_file_path, 0777);
					if (rename($old_file_path,$new_file_path)) {
						$this->db->query('SET SQL_BIG_SELECTS=1'); 
						$this->db->query('UPDATE `wp_abd_nail_pathology_report_archive` nail_patho INNER JOIN `wp_abd_specimen_archive` specimen ON `specimen`.`id` = `nail_patho`.`specimen_id`
						SET `nail_patho`.`nail_pdf` = CONCAT("'.$file_key.'/",`nail_patho`.`nail_pdf`)
						WHERE `nail_patho`.`status` ="Active" AND `nail_patho`.`nail_pdf`="'.$file_temp_name.'"');
					}
				}else{
					echo "File not exist ".$file_temp_name;
					echo "\n";
				}
			}

		}

		//PCR report pdf folder archive

		// $files_data=array();
		// $dist_year = $this->db->query("SELECT DISTINCT YEAR(`created_date`) as 'dist_year' FROM `wp_abd_generated_pcr_reports_archive`")->result_array();

		// foreach ($dist_year as $yr) {
		// 	$year = $yr['dist_year'];
		// 	$files_data[$year] = $this->db->query("SELECT `report_pdf_name`, YEAR(`created_date`) as 'dist_year' FROM `wp_abd_generated_pcr_reports_archive` WHERE YEAR(`created_date`) LIKE '%".$year."%'")->result_array();
		// }
		// //print_r($files_data); die();
		// $nailPdfPath = FCPATH."assets/uploads/pcr_report_pdf/";
		// foreach ($files_data as $file_key => $file_val) {
		// 	$archive_folder_path = $nailPdfPath.$file_key;
		// 	if(!file_exists($archive_folder_path)) {
		// 		mkdir($archive_folder_path, 0777);
		// 	} 
		// 	foreach ($file_val as $files_name) {
		// 		$file_temp_name = $files_name['report_pdf_name'];
		// 		$old_file_path = $nailPdfPath.$file_temp_name;
		// 		$new_file_path = $archive_folder_path.'/'.$file_temp_name;
	
		// 		if(file_exists($old_file_path)) {
		// 			chmod ($new_file_path, 0777);
		// 			if (rename($old_file_path,$new_file_path)) {
		// 				$this->db->query('SET SQL_BIG_SELECTS=1'); 
		// 				$this->db->query('UPDATE `wp_abd_generated_pcr_reports_archive` generated_pcr INNER JOIN `wp_abd_specimen_archive` specimen ON `specimen`.`assessioning_num` = `generated_pcr`.`accessioning_num`
		// 				SET `generated_pcr`.`report_pdf_name` = CONCAT("'.$file_key.'/",`generated_pcr`.`report_pdf_name`)
		// 				WHERE `generated_pcr`.`report_pdf_name`="'.$file_temp_name.'"');
		// 			}
		// 		}else{
		// 			echo "File not exist ".$file_temp_name;
		// 			echo "\n";
		// 		}
		// 	}

		// }
    }
    
    /**
	* /
	* 
	* @physicians_list
	*/
    
    function physicians_list()
     {
    	$physicians_data = array();
    	$physicians_det = array();
	    $physician_details = get_aps_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	    
	 if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta_value( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta_value( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det);
	     }
	     die( json_encode(array( "status" => "1","physicians_data" => $physicians_data )));
		 }
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Physician Found." )));
		 }		
	}
	   
   /**
	* /
	* Specimens List Start here
	*/
    function specimens_list()
    {
    	$specimen_details = array();
    	$physicians_data = array();
		$physicians_det = array();
		
	    $physician_sql = "SELECT `id`, `physician_id` FROM ".SPECIMEN_MAIN." WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta_value( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta_value( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	  /*  $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `modify_date` >= DATE(NOW()) - INTERVAL 30 DAY )";  */ 
	    $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`,`partners_company`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData(SPECIMEN_MAIN, $conditions, $select_fields, $is_multy_result, 'id', 'DESC', '100', '0');
		$conditions_all = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
		$select_fields_all = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
		$is_multy_result = 0;
			
		$specimens_details_all = $this->BlankModel->getTableData(SPECIMEN_MAIN, $conditions_all, $select_fields_all, $is_multy_result, 'id', 'DESC');
		$tot_count = count($specimens_details_all);
		
		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
			$sp_id = $specimen['id'];
			if($specimen['partners_company']){
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta_value( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta_value( $specimen['physician_id'], 'last_name');
				$physician_name = $physician_first_name.' '.$physician_last_name;
			}
		
		$specimen_information =  array('id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'],'physician_name' => $physician_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data, 'tot_count'=> $tot_count )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
	/**
	 * Archive Specimen
	 */

	function archive_specimens_list()
    {
    	$specimen_details = array();
    	$physicians_data = array();
		$physicians_det = array();
		
	    $physician_sql = "SELECT `id`, `physician_id` FROM ".SPECIMEN_ARCHIVE." WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta_value( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta_value( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	     /*  $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `modify_date` >= DATE(NOW()) - INTERVAL 30 DAY )";  */ 
	    $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`,`partners_company`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData(SPECIMEN_ARCHIVE, $conditions, $select_fields, $is_multy_result, 'id', 'DESC', '100', '0');
		$conditions_all = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
		$select_fields_all = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
		$is_multy_result = 0;
			
		$specimens_details_all = $this->BlankModel->getTableData(SPECIMEN_ARCHIVE, $conditions_all, $select_fields_all, $is_multy_result, 'id', 'DESC');
		$tot_count = count($specimens_details_all);

		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
			
			$sp_id = $specimen['id'];
			if($specimen['partners_company']){
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta_value( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta_value( $specimen['physician_id'], 'last_name');
				$physician_name = $physician_first_name.' '.$physician_last_name;
			}
		
		
		$specimen_information =  array('id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'],'physician_name' => $physician_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data, 'tot_count'=> $tot_count)));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}

	
	/**
	* 
	* Search Specimen
	*/
	
	function search_specimen()
	{	
		$search = "";
		$tbl_name = SPECIMEN_MAIN;
		$data = json_decode(file_get_contents('php://input'), true);
		if(count(array_filter($data)) != 0){
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".trim($data['fname'])."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".trim($data['lname'])."%'";	
		 }
		 if($data['dob']!=""){
			$search.=" AND `patient_dob` LIKE '%".trim($data['dob'])."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".trim($data['barcode'])."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` LIKE '%".trim($data['assessioning_num'])."%'";	
		 }
		 if($data['search_type']!="general"){
			$tbl_name = SPECIMEN_ARCHIVE;
		 }else{
			$tbl_name = SPECIMEN_MAIN;
		 }
		 $specimen_details = array();
		 $physicians_data = array();
		 $physicians_det = array();
	  
		 $physician_sql = "SELECT `id`, `physician_id` FROM ".$tbl_name." WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			$physician_first_name = get_aps_user_meta_value( $physician['physician_id'], 'first_name', '' );
			$physician_last_name  = get_aps_user_meta_value( $physician['physician_id'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name.' '.$physician_last_name;
		 	$physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		 $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59'".$search." )";
		 $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`,`partners_company`';
		 $is_multy_result = 0;
			 
		 $specimens_details = $this->BlankModel->getTableData($tbl_name, $conditions, $select_fields, $is_multy_result);
		 
		 if($specimens_details)
		 {
		 
		 $num_rows_count = 0;
		 foreach($specimens_details as $specimen){
			
			$sp_id = $specimen['id'];
			if($specimen['partners_company']){
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta_value( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta_value( $specimen['physician_id'], 'last_name');		 
				$physician_name = $physician_first_name.' '.$physician_last_name;		 
			}
			$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);

			$num_rows_count++;	
		   array_push($specimen_details, $specimen_information);
		 }
		 
		 die( json_encode(array('status' =>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
		}else{
			die( json_encode(array('status'=>'0')));
		}
	}
	
	/**
	* 
	* Load all Specimens
	*/

	function load_all_specimens()
    {
    	$specimen_details = array();
    	$physicians_data = array();
    	$physicians_det = array();
	 
	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta_value( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta_value( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name.' '.$physician_last_name;
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	    $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);

		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
		$physician_first_name = get_aps_user_meta( $specimen['physician_id'], 'first_name' );
		$physician_last_name  = get_aps_user_meta( $specimen['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
	
    
    /**
	*
	*  Specimens Suspended List
	*/
	function suspend_specimens_list()
	{
		$specimen_details = array();
		$physicians_data = array();
		$physicians_det = array();
		
		$conditions = " status = '1' AND (physician_accepct ='1' OR physician_accepct ='0') ORDER BY `modify_date` DESC";
		$select_fields = '`id`,`patient_address`,`physician_accepct`,`assessioning_num`,`physician_id` ,`p_lastname`,`p_firstname`, `collection_date`, `qc_check`,`partners_company`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		$count = count($specimens_details);

	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
		$physician_ids = $this->BlankModel->customquery($physician_sql);
		
		foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
		   }
		
		if($specimens_details)
		{
			foreach($specimens_details as $specimen){
			if($specimen['partners_company']){
				$sp_id = $specimen['id'];
				$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta( $specimen['physician_id'], 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			
			}
				$specimen_information =   array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'patient_address' => $specimen['patient_address'], 'physician_accepct' => $specimen['physician_accepct']);
	 
				array_push($specimen_details, $specimen_information);
			}
		}
	  
		if($specimens_details)
		{
			echo json_encode(array('status'=>'1', 'specimens_detail'=> $specimen_details, 'count'=>$count, "physicians_data" => $physicians_data));
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
	}
	/**
	* 
	* Search Suspended
	*  
	*/
	
	function search_sus()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".trim($data['fname'])."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".trim($data['lname'])."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` LIKE '%".$data['assessioning_num']."%'";	
		 }
		$specimen_details = array();
		$physicians_data = array();
		$physicians_det = array();
		
		$conditions = " status = '1' AND (physician_accepct ='1' OR physician_accepct ='0')".$search." ORDER BY `modify_date` DESC";
		$select_fields = '`id`,`patient_address`,`physician_accepct`,`assessioning_num`,`physician_id` ,`p_lastname`,`p_firstname`, `collection_date`, `qc_check`,`partners_company`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		$count = count($specimens_details);

	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
		$physician_ids = $this->BlankModel->customquery($physician_sql);
		
		foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
		   }
		
		if($specimens_details)
		{
			foreach($specimens_details as $specimen){
				if($specimen['partners_company']){
					$sp_id = $specimen['id'];
					$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
					$physician_name = $partners_specimen_details['physician_name'];
				}else{
					$physician_first_name = get_aps_user_meta( $specimen['physician_id'], 'first_name' );
					$physician_last_name  = get_aps_user_meta( $specimen['physician_id'], 'last_name');
					
					$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
					
				}
				$specimen_information =   array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'patient_address' => $specimen['patient_address'], 'physician_accepct' => $specimen['physician_accepct']);
	 
				array_push($specimen_details, $specimen_information);
			}
		}
	  
		if($specimens_details)
		{
			die (json_encode(array('status'=>'1', 'specimens_detail'=> $specimen_details, 'count'=>$count, "physicians_data" => $physicians_data)));

		}
		else
		{
			die (json_encode(array('status'=>'0')));
		}

	}

	/**
	* 
	* Specimens QC check
	*/
	function specimens_qclist()
    {
    	$specimen_details = array();
    	$physicians_data = array();
    	$physicians_det = array();
	 
	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id` ORDER BY `id` DESC";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	    $conditions = "( status = '0' AND physician_accepct ='0' AND qc_check = '1' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`,`lab_id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`, `patient_address`,`partners_company`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		
		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
		$physician_name = '';
			$sp_id = $specimen['id'];
			if($specimen['partners_company']){
				$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta( $specimen['physician_id'], 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			}

		$labtech_first_name = get_aps_user_meta( $specimen['lab_id'], 'first_name' );
		$labtech_last_name  = get_aps_user_meta( $specimen['lab_id'], 'last_name');
		
		$labtech_name = $labtech_first_name['meta_value'].' '.$labtech_last_name['meta_value'];
		
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'patient_address' => $specimen['patient_address'] ,'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'labtech_name' => $labtech_name,'lab_id'=>$specimen['lab_id']);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
/**
	* Search QC Specimen	
	* 
	*/
	
	function search_qcspecimen()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".trim($data['fname'])."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".trim($data['lname'])."%'";	
		 }
		 if($data['acc_no']!=""){
			$search.=" AND `assessioning_num` LIKE '%".$data['acc_no']."%'";	
		}
		 
		 $specimen_details = array();
		 $physicians_data = array();
		 $physicians_det = array();
	  
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			$physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
			$physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		 $conditions = "( status = '0' AND physician_accepct ='0' AND qc_check ='1'".$search." )";
		 $select_fields = '`id`,`lab_id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`,`patient_address`,`partners_company`';
		 $is_multy_result = 0;
			 
		 $specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		 
		 if($specimens_details)
		 {
		 
		$num_rows_count = 0;
		foreach($specimens_details as $specimen){
			if($specimen['partners_company']){
				$sp_id = $specimen['id'];
				$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
			}else{
				$physician_first_name = get_aps_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_aps_user_meta( $specimen['physician_id'], 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			}

		$labtech_first_name = get_aps_user_meta_value( $specimen['lab_id'], 'first_name' );
		$labtech_last_name  = get_aps_user_meta_value( $specimen['lab_id'], 'last_name');
		$labtech_name = $labtech_first_name.' '.$labtech_last_name;

		 
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'],'labtech_name'=> $labtech_name,'qc_check' => $specimen['qc_check'],'patient_address' => $specimen['patient_address'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name,'lab_id'=>$specimen['lab_id']);
		 
		   $num_rows_count++;	
		   array_push($specimen_details, $specimen_information);
		 }
		 
		 die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
	}
	
	/**
	* Add Specimens 
	*/
	function specimen_exits_check()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);

		if(!empty($apiData)){
			$p_lastname = $apiData['p_lastname'];
			$p_firstname = $apiData['p_firstname'];
			$patient_dob = $apiData['patient_dob'];
			$patient_state = $apiData['patient_state'];
			$msg ="";
			$this->db->where('p_lastname',$p_lastname);
			$this->db->where('p_firstname',$p_firstname);
			$this->db->where('patient_dob',$patient_dob);
			$result = $this->db->get('wp_abd_specimen');

			$this->db->where('p_lastname',$p_lastname);
			$this->db->where('p_firstname',$p_firstname);
			$this->db->where('patient_state',$patient_state);
			$result1 = $this->db->get('wp_abd_specimen');
			if($result1->num_rows() > 0){
				$msg ="Patient has already been tested previously";
			}else if($result->num_rows() > 0){
				$msg ="Specimen exits. Do you want to add again?";
			}
			if($result->num_rows() > 0 || $result1->num_rows() > 0){
				die( json_encode(array('status'=>'1','msg'=>$msg)));
			}else{
				die( json_encode(array('status'=>'0')));
			}
		}
	 }
	
	function add_specimen()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);
		$regex_string_negate = '/^((?!S)(?!L)(?!O)(?!I)(?!B)(?!Z)(?!s)(?!l)(?!o)(?!i)(?!b)(?!z).)*$/';
		$regex_string_format = '/^([1-9][a-zA-Z][a-zA-Z|0-9]\d[a-zA-Z][a-zA-Z0-9]\d[a-zA-Z][a-zA-Z]\d\d){1}$/'; 
		
		//pr($apiData);
		$field_data = array('collection_date','date_received','physician_id','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
		$error = false;
		$dataCount = count($field_data);
		$clinicalData = array();

		for($i=0;$i<$dataCount;$i++){
			if(empty($apiData[$field_data[$i]])){
				$error = true;
			}
		}

		if($error){
			$status = '1';
			$redirect_url = "/suspended";
		}
		elseif($apiData['physician_accepct'] == '0' && !$error && ($apiData['clinical_info'] || $apiData['specimen'][0]['nail_unit'])){
			$status = '0';
			$redirect_url = "/specimens-list";
		}
		elseif(!$error || $apiData['physician_accepct'] == '1' || !$apiData['clinical_info']){
			$status = '1';
			$redirect_url = "/suspended";
		}
		else{
			$status = '0';
			$redirect_url = "/specimens-list";
		}

		$bill = ""; $pri_subscribers = ""; $sec_subscribers = "";$insurance_type = "";
		if(count(array_filter($apiData['bill'])) != 0){
			$bill = implode(',',$apiData['bill']);
		}
		
		if(count(array_filter($apiData['pri_subscriber_relation'])) != 0){
			$pri_subscribers =  implode(',',$apiData['pri_subscriber_relation']);
		}
		 
		if(count(array_filter($apiData['sec_subscriber_relation'])) != 0){
			$sec_subscribers = implode(',',$apiData['sec_subscriber_relation']);
		  }
		

		$insurance_type = $apiData['insurance_type'];
		$insertData['lab_id'] = $apiData['user_id'];
		$insertData['physician_id'] = $apiData['physician_id'];
		$insertData['patient_id'] = $apiData['patient_id'];
		$insertData['collection_date'] = $apiData['collection_date'];
		$insertData['collection_time'] = str_replace($specialChars, $replaceChars, $apiData['time'])." ".$apiData['format'];
		$insertData['date_received'] = $apiData['date_received']; 
		$insertData['bill'] = $bill;
		$insertData['p_lastname'] = stripslashes($apiData['p_lastname']);
		$insertData['p_firstname'] = $apiData['p_firstname']; 
		$insertData['patient_city'] = $apiData['patient_city']; 
		$insertData['patient_address'] = $apiData['patient_address']; 
		$insertData['apt'] = $apiData['apt'];	
		$insertData['patient_phn'] = $apiData['patient_phn'];
		$insertData['patient_state'] = $apiData['patient_state'];
		$insertData['patient_zip'] = $apiData['patient_zip']; 
		$insertData['patient_dob'] = $apiData['patient_dob'];
		$insertData['patient_age'] = $apiData['patient_age'];	
		$insertData['patient_sex'] = $apiData['patient_sex']; 
		$insertData['pri_address'] = $apiData['pri_address']; 
		$insertData['pri_city'] = $apiData['pri_city'];
		$insertData['pri_state'] = $apiData['pri_state'];
		$insertData['pri_zip'] = $apiData['pri_zip']; 
		$insertData['pri_employee_name'] = $apiData['pri_employee_name'];	
		$insertData['pri_member_id'] = $apiData['pri_member_id'];
		$insertData['pri_subscriber_dob'] = $apiData['pri_subscriber_dob'];	
		$insertData['pri_group_contact'] = $apiData['pri_group_contact']; 
		$insertData['pri_sex'] = $apiData['pri_sex'];
		if(!empty($apiData['pri_medicare_id'])){
			$mbi = $apiData['pri_medicare_id'];
			if(preg_match($regex_string_negate, $mbi) && preg_match($regex_string_format, $mbi)) { 
				$insertData['pri_medicare_id'] = $mbi;
			} 	
		}else{
			$insertData['pri_medicare_id'] = '';
		}
		$insertData['pri_madicaid_id'] = $apiData['pri_madicaid_id'];
		$insertData['sec_insurance_name'] = $apiData['sec_insurance_name']; 
		$insertData['sec_address'] = $apiData['sec_address'];
		$insertData['sec_city'] =$apiData['sec_city'];	
		$insertData['sec_state'] = $apiData['sec_state'];
		$insertData['sec_zip'] = $apiData['sec_zip'];
		$insertData['sec_employee_name'] = $apiData['sec_employee_name'];
		$insertData['sec_member_id'] = $apiData['sec_member_id']; 
		$insertData['sec_subscriber_dob'] = $apiData['sec_subscriber_dob']; 
		$insertData['sec_group_contact'] = $apiData['sec_group_contact']; 
		$insertData['sec_sex'] = $apiData['sec_sex'];
		if(!empty($apiData['sec_medicare_id'])){
			$mbi_sec = $apiData['sec_medicare_id'];
			if(preg_match($regex_string_negate, $mbi_sec) && preg_match($regex_string_format, $mbi_sec)) { 
				$insertData['sec_medicare_id'] = $mbi_sec;
			} 	
		}else{
			$insertData['sec_medicare_id'] = '';
		}
		$insertData['sec_medicaid_id'] = $apiData['sec_medicaid_id'];
		$insertData['physician_accepct'] =$apiData['physician_accepct'];
		$insertData['status'] =$status;
		$insertData['modify_date'] = date('Y-m-d H:i:s');
		$insertData['create_date'] = date('Y-m-d H:i:s');
		$insertData['pri_insurance_name']= $apiData['pri_insurance_name'];
		$insertData['ins_type'] = $insurance_type; 
		$insertData['sec_subscriber'] =$sec_subscribers;
		$insertData['pri_subscriber'] = $pri_subscribers; 
		$insertData['test_type'] = $apiData['test_type'];
		if(!empty($apiData['process_done'])){
			$insertData['process_done'] = $apiData['process_done'];
		}
		$speCount = count($apiData['specimen']);

		for($i=0;$i<$speCount;$i++){
			$specimenData = $apiData['specimen'][$i];
			if(count(array_filter($specimenData['nail_unit'])) != 0){
				$digits = 4;
				$today_date = date('Y-m-d');
				$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where accessioning_cur_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
				$insert_day_data = $this->db->query($today_date)->result_array();
				$serise_no;
				foreach ($insert_day_data as $data) {
					$no = substr($data['assessioning_num'],7,4);
					$no = abs($no);
					if(empty($serise_no) || abs($serise_no) < $no){
						$serise_no = $no;
					}
				}
			
				$day_data = $serise_no+1;
				$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-'.$apiData['test_type'];
				$insertData['assessioning_num'] = $assessioning_num;
				$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");
				$clinicalData['assessioning_num']=$assessioning_num;
			}

				$insertData['site_indicator'] = $specimenData['site_indicator'];
				$this->db->insert('wp_abd_specimen',$insertData);
				$lastId = $this->db->insert_id();

				if(count(array_filter($specimenData['nail_unit'])) != 0){
				
					$clinicalData['specimen_id'] = $lastId;
					$clinicalData['clinical_specimen'] = implode(',',$specimenData['clinical_specimen']);
					$clinicalData['nail_unit']			= implode(',',$specimenData['nail_unit']);
					$clinicalData['create_date'] = date('Y-m-d H:i:s');

					$this->db->insert('wp_abd_clinical_info',$clinicalData);
					$clinicLastId = $this->db->insert_id();

					$folder = FCPATH."assets/uploads/qr_images/";
					$file_name = "qr_".$assessioning_num.".png";
					$file_path = $folder.$file_name;
					$info = $assessioning_num;
					QRcode::png($info, $file_path);
					array_push($qrCode, $file_name);
			 		array_push($assessioning, $assessioning_num);
					
				}
		}

		 if($lastId){
			 die( json_encode(array('returnUrl'=>$redirect_url,'qrCode'=>$qrCode,'base_url'=>base_url(),'assessioning_num'=>$assessioning)));
		 }
		
	  }

	function get_assessioning_num(){
		$digits = 4;
		$today_date = date('Y-m-d');
		$today_date = "SELECT count(*) as num_count FROM `wp_abd_temp_accessioning_num` where create_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `temp_accessioning_num` <> 0";
		$insert_day_data = $this->db->query($today_date)->row_array();
		$day_data = $insert_day_data['num_count']+1;
		$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-NF';

		echo( json_encode(array('assessioning_num'=>$assessioning_num)));
	  }

	function edit_specimen()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);
		$regex_string_negate = '/^((?!S)(?!L)(?!O)(?!I)(?!B)(?!Z)(?!s)(?!l)(?!o)(?!i)(?!b)(?!z).)*$/';
		$regex_string_format = '/^([1-9][a-zA-Z][a-zA-Z|0-9]\d[a-zA-Z][a-zA-Z0-9]\d[a-zA-Z][a-zA-Z]\d\d){1}$/'; 
		
		//pr($apiData);
		if(!empty($apiData['partners_company'])){
			$field_data = array('collection_date','date_received','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		}else{
			$field_data = array('collection_date','date_received','physician_id','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		}
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
		$error = false;
		$dataCount = count($field_data);
		$clinicLastId='';
		$specimenAfftectedRows=0;
		$clinicAfftectedRows=0;
		$exIdAffectedRows=0;
		for($i=0;$i<$dataCount;$i++){
			if(empty($apiData[$field_data[$i]])){
				$error = true;
			}
		}
		if($error){
			$status = '1';
			$redirect_url = "/suspended";
		}
		elseif($apiData['physician_accepct'] == '0' && !$error){
			$status = '0';
			$redirect_url = "/specimens-list";
		}
		elseif(!$error || $apiData['physician_accepct'] == '1'){
			$status = '1';
			$redirect_url = "/suspended";
		}
		else{
			$status = '0';
			$redirect_url = "/specimens-list";
		} 

		$bill = ""; $pri_subscribers = ""; $sec_subscribers = ""; $insurance_type = "";
		if(count(array_filter($apiData['bill'])) != 0){
			$bill = implode(',',$apiData['bill']);
		}
		
		if(count(array_filter($apiData['pri_subscriber_relation'])) != 0){
			$pri_subscribers =  implode(',',$apiData['pri_subscriber_relation']);
		}
		 
		if(count(array_filter($apiData['sec_subscriber_relation'])) != 0){
			$sec_subscribers = implode(',',$apiData['sec_subscriber_relation']);
		  }
		

		$insurance_type = $apiData['insurance_type'];
		// $insertData['lab_id'] = $apiData['user_id'];
		$insertData['physician_id'] = $apiData['physician_id'];
		$insertData['patient_id'] = $apiData['patient_id'];
		$insertData['collection_date'] = $apiData['collection_date'];
		$insertData['collection_time'] = str_replace($specialChars, $replaceChars, $apiData['time'])." ".$apiData['format'];
		$insertData['date_received'] = $apiData['date_received']; 
		$insertData['bill'] = $bill;
		$insertData['p_lastname'] = stripslashes($apiData['p_lastname']);
		$insertData['p_firstname'] = $apiData['p_firstname']; 
		$insertData['patient_city'] = $apiData['patient_city']; 
		$insertData['patient_address'] = $apiData['patient_address']; 
		$insertData['apt'] = $apiData['apt'];	
		$insertData['patient_phn'] = $apiData['patient_phn'];
		$insertData['patient_state'] = $apiData['patient_state'];
		$insertData['patient_zip'] = $apiData['patient_zip']; 
		$insertData['patient_dob'] = $apiData['patient_dob'];
		$insertData['patient_age'] = $apiData['patient_age'];	
		$insertData['patient_sex'] = $apiData['patient_sex']; 
		$insertData['pri_address'] = $apiData['pri_address']; 
		$insertData['pri_city'] = $apiData['pri_city'];
		$insertData['pri_state'] = $apiData['pri_state'];
		$insertData['pri_zip'] = $apiData['pri_zip']; 
		$insertData['pri_employee_name'] = $apiData['pri_employee_name'];	
		$insertData['pri_member_id'] = $apiData['pri_member_id'];
		$insertData['pri_subscriber_dob'] = $apiData['pri_subscriber_dob'];	
		$insertData['pri_group_contact'] = $apiData['pri_group_contact']; 
		$insertData['pri_sex'] = $apiData['pri_sex'];
		if(!empty($apiData['pri_medicare_id'])){
			$mbi = $apiData['pri_medicare_id'];
			if(preg_match($regex_string_negate, $mbi) && preg_match($regex_string_format, $mbi)) { 
				$insertData['pri_medicare_id'] = $mbi;
			} 	
		}else{
			$insertData['pri_medicare_id'] = '';
		}
		$insertData['pri_madicaid_id'] = $apiData['pri_madicaid_id'];
		$insertData['sec_insurance_name'] = $apiData['sec_insurance_name']; 
		$insertData['sec_address'] = $apiData['sec_address'];
		$insertData['sec_city'] =$apiData['sec_city'];	
		$insertData['sec_state'] = $apiData['sec_state'];
		$insertData['sec_zip'] = $apiData['sec_zip'];
		$insertData['sec_employee_name'] = $apiData['sec_employee_name'];
		$insertData['sec_member_id'] = $apiData['sec_member_id']; 
		$insertData['sec_subscriber_dob'] = $apiData['sec_subscriber_dob']; 
		$insertData['sec_group_contact'] = $apiData['sec_group_contact']; 
		$insertData['sec_sex'] = $apiData['sec_sex'];

		if(!empty($apiData['sec_medicare_id'])){
			$mbi_sec = $apiData['sec_medicare_id'];
			if(preg_match($regex_string_negate, $mbi_sec) && preg_match($regex_string_format, $mbi_sec)) { 
				$insertData['sec_medicare_id'] = $mbi_sec;
			} 	
		}else{
			$insertData['sec_medicare_id'] = '';
		}
		$insertData['sec_medicaid_id'] = $apiData['sec_medicaid_id'];
		$insertData['physician_accepct'] =$apiData['physician_accepct'];
		$insertData['status'] =$status;
		$insertData['modify_date'] = date('Y-m-d H:i:s');
		$insertData['pri_insurance_name']= $apiData['pri_insurance_name'];
		$insertData['ins_type'] = $insurance_type; 
		$insertData['sec_subscriber'] =$sec_subscribers;
		$insertData['pri_subscriber'] = $pri_subscribers; 
		$insertData['notes'] = $apiData['notes'];

		if(!empty($apiData['test_type'])){
			$insertData['test_type'] = $apiData['test_type'];
		}	

		if(!empty($apiData['specimen'])){
			$speCount = count($apiData['specimen']);
		
			for($i=0;$i<$speCount;$i++){
				$accessioning_no = '';
				$specimenData = $apiData['specimen'][$i];
				$accessioning_no = $this->generateAccessioning($apiData['test_type']);

				
				if($i==0){
					$specimenId= $apiData['specimen_id'];
					$insertData['site_indicator'] = $specimenData['site_indicator'];
					$external_id = $apiData['external_id'];

					if($external_id != ''){
						$partners_comp_data = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE external_id = '".$external_id."' AND specimen_id != $specimenId");
						if($partners_comp_data->num_rows() > 0){
							die( json_encode(array('external_id_err'=>'External ID exits. Please try with new ID.')));
						}else{
							$this->db->where('specimen_id',$specimenId);
							$this->db->update('wp_abd_partners_specimen',array('external_id'=>$external_id));
						}
					}
					
					$clinic_rows = $this->db->query("SELECT * FROM wp_abd_clinical_info WHERE specimen_id = $specimenId");
					
					if($clinic_rows->num_rows() > 0){
						$clinicalData = array(  
							'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
							'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
						);
						$this->db->where('specimen_id',$specimenId);
						$this->db->update('wp_abd_clinical_info',$clinicalData);
						$clinicAfftectedRows = $this->db->affected_rows();
					}else{
						$clinicalData = array( 
							'specimen_id' => $specimenId, 
							'assessioning_num'  => $accessioning_no, 
							'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
							'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
							'create_date' => date('Y-m-d H:i:s')
						);
						$this->db->insert('wp_abd_clinical_info',$clinicalData);
						$insertData['assessioning_num'] = $accessioning_no;
						$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");
					}

					$this->db->where('id',$specimenId);
					$this->db->update('wp_abd_specimen',$insertData);
					$specimenAfftectedRows = $this->db->affected_rows();
					
				}else{
					$insertData['site_indicator'] = $specimenData['site_indicator'];
					$insertData['create_date'] = date('Y-m-d H:i:s');
					$insertData['assessioning_num'] = $accessioning_no;
					$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");
					$this->db->insert('wp_abd_specimen',$insertData);
					$lastId = $this->db->insert_id();

					$clinicalData = array( 
						'specimen_id' => $lastId, 
						'assessioning_num'  => $accessioning_no, 
						'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
						'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
						'create_date' => date('Y-m-d H:i:s')
					);

					$this->db->insert('wp_abd_clinical_info',$clinicalData);

					$clinicLastId = $this->db->insert_id();
				}
					if(isset($insertData['assessioning_num'])){
						$folder = FCPATH."assets/uploads/qr_images/";
						$file_name = "qr_".$accessioning_no.".png";
						$file_path = $folder.$file_name;
						$info = $accessioning_no;
						QRcode::png($info, $file_path);
						array_push($qrCode, $file_name);
						array_push($assessioning, $accessioning_no);
					}
					
			}
		}
		 if(($clinicAfftectedRows || $specimenAfftectedRows) || $clinicLastId){
			 die( json_encode(array('returnUrl'=>$redirect_url,'qrCode'=>$qrCode,'base_url'=>base_url(),'assessioning_num'=>$assessioning,'update_success'=>'1')));
		 }
		
	  }

	function qc_checklist_edit()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);
		$regex_string_negate = '/^((?!S)(?!L)(?!O)(?!I)(?!B)(?!Z)(?!s)(?!l)(?!o)(?!i)(?!b)(?!z).)*$/';
		$regex_string_format = '/^([1-9][a-zA-Z][a-zA-Z|0-9]\d[a-zA-Z][a-zA-Z0-9]\d[a-zA-Z][a-zA-Z]\d\d){1}$/'; 
		
		if(!empty($apiData['partners_company'])){
			$field_data = array('collection_date','date_received','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		}else{
			$field_data = array('collection_date','date_received','physician_id','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		}
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
		$error = false;
		$dataCount = count($field_data);
		$clinicLastId='';
		$specimenAfftectedRows=0;
		$clinicAfftectedRows=0;
		for($i=0;$i<$dataCount;$i++){
			if(empty($apiData[$field_data[$i]])){
				$error = true;
			}
		}
		if($error){
			$status = '1';
			$redirect_url = "/suspended";
		}
		elseif($apiData['physician_accepct'] == '0' && !$error){
			$status = '0';
			$redirect_url = "/specimens-list";
		}
		elseif(!$error || $apiData['physician_accepct'] == '1'){
			$status = '1';
			$redirect_url = "/suspended";
		}
		else{
			$status = '0';
			$redirect_url = "/specimens-list";
		} 

		$bill = ""; $pri_subscribers = ""; $sec_subscribers = ""; $insurance_type = "";
		if(count(array_filter($apiData['bill'])) != 0){
			$bill = implode(',',$apiData['bill']);
		}
		
		if(count(array_filter($apiData['pri_subscriber_relation'])) != 0){
			$pri_subscribers =  implode(',',$apiData['pri_subscriber_relation']);
		}
		 
		if(count(array_filter($apiData['sec_subscriber_relation'])) != 0){
			$sec_subscribers = implode(',',$apiData['sec_subscriber_relation']);
		  }
		

		$insurance_type = $apiData['insurance_type'];
		$insertData['physician_id'] = $apiData['physician_id'];
		$insertData['qc_check_lab_id'] = $apiData['user_id'];
		
		$insertData['patient_id'] = $apiData['patient_id'];
		$insertData['collection_date'] = $apiData['collection_date'];
		$insertData['collection_time'] = str_replace($specialChars, $replaceChars, $apiData['time'])." ".$apiData['format'];
		$insertData['date_received'] = $apiData['date_received']; 
		$insertData['bill'] = $bill;
		$insertData['p_lastname'] = stripslashes($apiData['p_lastname']);
		$insertData['p_firstname'] = $apiData['p_firstname']; 
		$insertData['patient_city'] = $apiData['patient_city']; 
		$insertData['patient_address'] = $apiData['patient_address']; 
		$insertData['apt'] = $apiData['apt'];	
		$insertData['patient_phn'] = $apiData['patient_phn'];
		$insertData['patient_state'] = $apiData['patient_state'];
		$insertData['patient_zip'] = $apiData['patient_zip']; 
		$insertData['patient_dob'] = $apiData['patient_dob'];
		$insertData['patient_age'] = $apiData['patient_age'];	
		$insertData['patient_sex'] = $apiData['patient_sex']; 
		$insertData['pri_address'] = $apiData['pri_address']; 
		$insertData['pri_city'] = $apiData['pri_city'];
		$insertData['pri_state'] = $apiData['pri_state'];
		$insertData['pri_zip'] = $apiData['pri_zip']; 
		$insertData['pri_employee_name'] = $apiData['pri_employee_name'];	
		$insertData['pri_member_id'] = $apiData['pri_member_id'];
		$insertData['pri_subscriber_dob'] = $apiData['pri_subscriber_dob'];	
		$insertData['pri_group_contact'] = $apiData['pri_group_contact']; 
		$insertData['pri_sex'] = $apiData['pri_sex'];
		if(!empty($apiData['pri_medicare_id'])){
			$mbi = $apiData['pri_medicare_id'];
			if(preg_match($regex_string_negate, $mbi) && preg_match($regex_string_format, $mbi)) { 
				$insertData['pri_medicare_id'] = $mbi;
			} 	
		}else{
			$insertData['pri_medicare_id'] = '';
		}
		$insertData['pri_madicaid_id'] = $apiData['pri_madicaid_id'];
		$insertData['sec_insurance_name'] = $apiData['sec_insurance_name']; 
		$insertData['sec_address'] = $apiData['sec_address'];
		$insertData['sec_city'] =$apiData['sec_city'];	
		$insertData['sec_state'] = $apiData['sec_state'];
		$insertData['sec_zip'] = $apiData['sec_zip'];
		$insertData['sec_employee_name'] = $apiData['sec_employee_name'];
		$insertData['sec_member_id'] = $apiData['sec_member_id']; 
		$insertData['sec_subscriber_dob'] = $apiData['sec_subscriber_dob']; 
		$insertData['sec_group_contact'] = $apiData['sec_group_contact']; 
		$insertData['sec_sex'] = $apiData['sec_sex'];
		if(!empty($apiData['sec_medicare_id'])){
			$mbi_sec = $apiData['sec_medicare_id'];
			if(preg_match($regex_string_negate, $mbi_sec) && preg_match($regex_string_format, $mbi_sec)) { 
				$insertData['sec_medicare_id'] = $mbi_sec;
			} 	
		}else{
			$insertData['sec_medicare_id'] = '';
		}
		$insertData['sec_medicaid_id'] = $apiData['sec_medicaid_id'];
		$insertData['physician_accepct'] =$apiData['physician_accepct'];
		$insertData['status'] =$status;
		$insertData['modify_date'] = date('Y-m-d H:i:s');
		$insertData['pri_insurance_name']= $apiData['pri_insurance_name'];
		$insertData['ins_type'] = $insurance_type; 
		$insertData['sec_subscriber'] =$sec_subscribers;
		$insertData['pri_subscriber'] = $pri_subscribers; 
		$insertData['notes'] = $apiData['notes'];

		if(!empty($apiData['test_type'])){
			$insertData['test_type'] = $apiData['test_type'];
		}	
		
		if(!empty($apiData['specimen'])){
			$speCount = count($apiData['specimen']);
		
			for($i=0;$i<$speCount;$i++){
				$accessioning_no = '';
				$specimenData = $apiData['specimen'][$i];
				$accessioning_no = $this->generateAccessioning($apiData['test_type']);

				
				if($i==0){
					$specimenId= $apiData['specimen_id'];
					$insertData['site_indicator'] = $specimenData['site_indicator'];
					$external_id = $apiData['external_id'];

					if($external_id != ''){
						$partners_comp_data = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE external_id = '".$external_id."' AND specimen_id != $specimenId");
						if($partners_comp_data->num_rows() > 0){
							die( json_encode(array('external_id_err'=>'External ID exits. Please try with new ID.')));
						}else{
							$this->db->where('specimen_id',$specimenId);
							$this->db->update('wp_abd_partners_specimen',array('external_id'=>$external_id));
						}
					}
					
					$clinic_rows = $this->db->query("SELECT * FROM wp_abd_clinical_info WHERE specimen_id = $specimenId");
					
					if($clinic_rows->num_rows() > 0){
						$clinicalData = array(  
							'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
							'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
						);
						$this->db->where('specimen_id',$specimenId);
						$this->db->update('wp_abd_clinical_info',$clinicalData);
						$clinicAfftectedRows = $this->db->affected_rows();
					}else{
						$clinicalData = array( 
							'specimen_id' => $specimenId, 
							'assessioning_num'  => $accessioning_no, 
							'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
							'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
							'create_date' => date('Y-m-d H:i:s')
						);
						$this->db->insert('wp_abd_clinical_info',$clinicalData);
						$insertData['assessioning_num'] = $accessioning_no;
						$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");
					}

						$this->db->where('id',$specimenId);
						$spe_data = $this->db->get('wp_abd_specimen')->row_array();
						if($spe_data['qc_check'] == '1'){
							$insertData['qc_check'] = '0'; 
						}

					$this->db->where('id',$specimenId);
					$this->db->update('wp_abd_specimen',$insertData);
					$specimenAfftectedRows = $this->db->affected_rows();
					
				}
					if(isset($insertData['assessioning_num'])){
						$folder = FCPATH."assets/uploads/qr_images/";
						$file_name = "qr_".$accessioning_no.".png";
						$file_path = $folder.$file_name;
						$info = $accessioning_no;
						QRcode::png($info, $file_path);
						array_push($qrCode, $file_name);
						array_push($assessioning, $accessioning_no);
					}
					
			}
		}
		 if(($clinicAfftectedRows || $specimenAfftectedRows) || $clinicLastId){
			 die( json_encode(array('returnUrl'=>$redirect_url,'qrCode'=>$qrCode,'base_url'=>base_url(),'assessioning_num'=>$assessioning,'update_success'=>'1')));
		 }
		
	  }

	//   function qc_checklist_edit(){
	//   	$spe_id = json_decode(file_get_contents('php://input'), true);
	//   	$this->db->where('id',$spe_id);
	//   	$spe_data = $this->db->get('wp_abd_specimen')->row_array();

	//   	if($spe_data['qc_check'] == '1'){
	//   		$digits = 4;
	// 		$today_date = date('Y-m-d');
	// 		$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where create_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
	// 		$insert_day_data = $this->db->query($today_date)->result_array();
	// 		$serise_no;
	// 		foreach ($insert_day_data as $data) {
	// 			$no = substr($data['assessioning_num'],7,4);
	// 			$no = abs($no);
	// 			if(empty($serise_no) || abs($serise_no) < $no){
	// 				$serise_no = $no;
	// 			}
	// 		}
			
	// 		$day_data = $serise_no+1;
	// 		$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-'.$spe_data['test_type'];


	//   		$this->db->where('id',$spe_id);
	//   		$this->db->update('wp_abd_specimen',array('qc_check' => '0','assessioning_num'=>$assessioning_num));

	//   		$this->db->where('specimen_id',$spe_id);
	//   		$this->db->update('wp_abd_clinical_info',array('assessioning_num'=>$assessioning_num));

	//   		$folder = FCPATH."assets\uploads\qr_images\/";
	// 		 $file_name = "qr_".$assessioning_num.".png";
	// 		 $file_path = $folder.$file_name;

	// 		 $info = $assessioning_num;
			 
	// 		 QRcode::png($info, $file_path);
			 
	//   		die(json_encode(array('status'=>'1','qrCode'=>$file_name,'base_url'=>base_url(),'assessioning_num'=>$assessioning_num)));
	//   	}else{
	//   		die(json_encode(array('status'=>'0')));
	//   	}
	//   }

	/**
	* Cancelled Specimens 
	*/
	function cancelled_specimens_list()
	  {
		$search = "";  
		$specimens_details = $this->BlankModel->get_cancelled($search);
		$num_rows_count = count($specimens_details);
		$details = array();
		$physicians_det = array();
		$physicians_data = array();
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			 $physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
		  $physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
			 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		  $physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				if($specimen->partners_company){
					$sp_id = $specimen->id;
					$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
					$physician_name = $partners_specimen_details['physician_name'];
				}else{
					$physician_first_name = get_aps_user_meta( $specimen->physician_id, 'first_name' );
					$physician_last_name  = get_aps_user_meta( $specimen->physician_id, 'last_name');
					$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				}
				$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'physician_id' => $specimen->physician_id, 'p_lastname' => $specimen->p_lastname, 'p_firstname' => $specimen->p_firstname, 'qc_check' => $specimen->qc_check,'patient_address' => $specimen->patient_address, 'collection_date' => $specimen->collection_date, 'physician_name' => $physician_name,'stage_id'=>$specimen->stage_id);
				array_push($details, $specimen_information);
				
				
			}
			die( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $details , "physicians_data" => $physicians_data )));
			
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	  }
	/**
	* 
	*Search Cancelled Specimen
	* 
	*/
	function search_cancelled_specimen()
	  {
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			// $month_day = substr($collection_date , '0','6');
			// $year = substr($collection_date , '6','4');
			//$trimed_date = end(explode('20', $year)); 
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!="")
		 {
			$search.=" AND `p_firstname` LIKE '%".trim($data['fname'])."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".trim($data['lname'])."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` LIKE '%".$data['assessioning_num']."%'";	
		 }

		$specimens_details = $this->BlankModel->get_cancelled($search);
		$num_rows_count = count($specimens_details);
		$details = array();
		$physicians_det = array();
		$physicians_data = array();
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			 $physician_first_name = get_aps_user_meta( $physician['physician_id'], 'first_name', '' );
		  $physician_last_name  = get_aps_user_meta( $physician['physician_id'], 'last_name' , '' );
			 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		  $physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				if($specimen->partners_company){
					$sp_id = $specimen->id;
					$partners_specimen_details = $this->db->query("SELECT * FROM wp_abd_partners_specimen WHERE specimen_id = $sp_id")->row_array();
					$physician_name = $partners_specimen_details['physician_name'];
				}else{
					$physician_first_name = get_aps_user_meta( $specimen->physician_id, 'first_name' );
					$physician_last_name  = get_aps_user_meta( $specimen->physician_id, 'last_name');
					$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				}
				$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'physician_id' => $specimen->physician_id, 'p_lastname' => $specimen->p_lastname, 'p_firstname' => $specimen->p_firstname, 'qc_check' => $specimen->qc_check,'patient_address' => $specimen->patient_address, 'collection_date' => $specimen->collection_date, 'physician_name' => $physician_name,'stage_id'=>$specimen->stage_id);
				array_push($details, $specimen_information);
				
				
			}
			echo( json_encode(array('status'=>'1','specimen_count' => $num_rows_count, "specimens_detail" => $details , "physicians_data" => $physicians_data )));
			
		}
		else
		{
			echo( json_encode(array('status'=>'0')));
		}

	  }
	
	/**
	* Specimens Stage details
	*/
    function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict)))        {
            return true;
        }
    }

    return false;
    }
	
	
	
	/**
	* 
	* Specimen Stages TAT 
	* 
	*/
	function specimenStagesTatNotification()
    {  
             
    /**
	* 
	* @var Current Date and Time
	* 
	*/
   
    $current_datetime     = date('Y-m-d H:i:s');  
    $current_date         = date('Y-m-d'); 
	$current_timestamp    = strtotime($current_datetime);  
   
    /**
	* 
	* @var HISTO Specimen
	* 
	*/
	
    $details = array(); 
    $red_specimen_array = array(); 

    $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
    (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` >= DATE(NOW()) - INTERVAL 7 DAY AND `id` NOT IN 
    (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
    (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `id` DESC");
   
    if($specimen_results)
    {
	
	/**
	* 
	* Ability HISTO Process
	* 
	*/

    foreach ($specimen_results as $specimen_data)
    {    	
        $specimen_create_date  = strtotime($specimen_data['create_date']);  
        $specimen_date         = date('Y-m-d', $specimen_create_date);
		
		/**
		* 
		* @var Weekend Checking
		* 
		*/
		
		$weekend_days = Array ();

		for($i=$specimen_create_date; $i<=strtotime($current_date); $i=$i+86400) {

		if(date("w",$i) == 6) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		if(date("w",$i) == 0) {
		$weekend_days[] = date("Y-m-d ", $i);
		}
		}


		/**
		* 
		* @var Holiday List 
		* 
		*/
		$holiday_sql = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' AND `start_date` BETWEEN '".$specimen_date."' AND '".$current_date."'";
        $holiday_range = $this->BlankModel->customquery($holiday_sql);  
       
       
        $holiday_count = 0;
        if($holiday_range){
		
		foreach($holiday_range as $holidays ){
          $holiday_list[] = getDatesFromRange($holidays['start_date'], $holidays['end_date']);
        }
                
        $holidays_list = $this->array_flatten($holiday_list);
	
		if($holidays_list){
			
			foreach($holidays_list as $holiday){		
		    $holiday_data = check_in_range($specimen_date, $current_date, $holiday);  
		    
		    if($holiday_data == '1'){
			   $holiday_count += $holiday_data;
			 }
		    }
		   }
	    }
	
	    $total_exclude_days = count($weekend_days) + $holiday_count;
	    $total_seconds_calculation   = floor(abs($current_timestamp - $specimen_create_date));  
	    $exclude_second = 0;
	    if($total_exclude_days>0){
		   $exclude_second = $total_exclude_days*86400; 
		}
	 
	    $seconds_calculation   = ($total_seconds_calculation - $exclude_second);
    	
    	
    	/**
		* 
		* @var Delivary to Lab 24 hrs.
		* 
		*/
       
        $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
      
         $stage1 = '';      
        //Check Stage Completed or Not.
        if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
        {              
           $color1  = 'Green';
           $status1 = 'Process Done';                    
        }
        else
        {              
          if($seconds_calculation > 86400) // 24 hrs.
          {
            $color1  = 'Red';
            $status1 = 'Beyond TAT';        
          }
          else
          {
           $color1  = 'Green';
           $status1 = 'Not Pass TAT';                
          } 
        }
        
        /**
		* 
		* @var Gross Description 8hrs.
		* 
		*/
        
        $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
           //Check Stage Completed or Not.
          if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
          {
            $color2  = 'Green';
            $status2 = 'Process Done';       
          }
          else
          {                  
           if($seconds_calculation > 28800)  // 8 hrs.
           {
            $color2  = 'Red'; 
            $status2 = 'Beyond TAT';                
           }
           else
           {
            $color2  = 'Green';
            $status2 = 'Not Pass TAT';              
           }
          }

			/**
			* 
			* @var Microtomy  24 hrs.>86400) // 24 hrs.
			* 
			*/
          $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
          if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
          {
              $color3  = 'Green'; 
              $status3 = 'Process Done';     
          }  
          else
          {                 
            if($seconds_calculation > 86400) // 24 hrs.
            {
               $color3  = 'Red';
               $status3 = 'Beyond TAT';  
            }
            else 
            {
               $color3  = 'Green';
               $status3 = 'Not Pass TAT';
            } 
          }

          /**
		  * 
		  * @var QC Check  6 hrs.
		  * 
		  */
		  
          $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
          if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
          {
                $color4  = 'Green'; 
                $status4 = 'Process Done'; 
          }
          else
          {
            if($seconds_calculation > 21600) // 6 hrs.
            {
                $color4  = 'Red';
                $status4 = 'Beyond TAT';  
            }
            else 
            {
                $color4  = 'Green';
                $status4 = 'Not Pass TAT';
            } 
          }

         $specimen_information =  array( 'accessioning_num' => $specimen_data['assessioning_num'], 
        								'specimen_id' => $specimen_data['id'],
						                'color1'  => $color1, 
						                'status1' => $status1,
						                'color2'  => $color2,
						                'status2' => $status2,
						                'color3'  => $color3,
						                'status3' => $status3, 
						                'color4'  => $color4, 
						                'status4' => $status4);
        array_push($details, $specimen_information); 
      
      }

      if(!empty($details))
      {	 
       $data_row = '0'; 
       $count = 1;	
	   foreach($details as $specimnen)
	   {	   	
	    if(in_array("Red", $specimnen))
        {
           $data_row = '1';        
           $red_specimen_stages =  array( 'accessioning_num' => $specimnen['accessioning_num'], 
							           'color1'  => $specimnen['color1'], 
						               'status1' => $specimnen['status1'],
						               'color2'  => $specimnen['color2'],
						               'status2' => $specimnen['status2'],
						               'color3'  => $specimnen['color3'],
						               'status3' => $specimnen['status3'], 
						               'color4'  => $specimnen['color4'], 
						               'status4' => $specimnen['status4']);
           array_push($red_specimen_array, $red_specimen_stages);
           $note_cout = $count++;    
		 }
        }
       
       if(!empty($red_specimen_array)){
	   	
	   	  echo( json_encode(array('status' => '1', 'specimen_stages_details' => $red_specimen_array, 'note_count' => $note_cout ))); 
	   }
	   else{
	   	  echo( json_encode(array('status' => '0', 'message' => 'No Pending HISTO Stages.', 'note_count' => '0' )));
	   }
       
      }        
	  else{
	   	  echo( json_encode(array('status' => '0', 'message' => 'No Pending HISTO Stages.', 'note_count' =>'0' )));
	   }
	 
	  } 
	
	else{
		echo (json_encode( array('status' => '0', 'message' => 'No Data Found for Last Week', 'note_count' => '0' )));
	  }	
	 }
       
	
	function array_flatten($array_list) { 
      if (!is_array($array_list)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array_list as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, $this->array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return $result; 
    } 
	
	
	/**
	* Specimen List
	*
	*/
	
	function specimen_stage_list()
	{
		$details = array();
		$data = json_decode(file_get_contents('php://input'), true);
		
		if(!empty($data))
		{
		$load = $data['load'];
		$specimens_details = $this->get_specimen_stages($load);
		$num_rows_count = count($specimens_details);
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				
			$clinic_query = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `assessioning_num` = '".$specimen->assessioning_num."'";
			$clinic_res = $this->BlankModel->customquery($clinic_query);
			$nail_unit = explode(',' ,$clinic_res[0]['nail_unit']);
			
			if(!in_array(7, $nail_unit))
			{			
							$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='1'";
							$stage1_data = $this->BlankModel->customquery($stage1_sql);
							if(!empty($stage1_data))
							{
								$first_data = "Process Done";
								$first_msg = "Update Stage Details";
							}
							else
							{
								$first_data = "Process Not Done";
								$first_msg = "Add Stage Details";
							}
							
							$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='2'";
							$stage2_data = $this->BlankModel->customquery($stage2_sql);
							if(!empty($stage2_data))
							{
								$second_data = "Process Done";
								$second_msg = "Update Stage Details";
							}
							else
							{
								$second_data = "Process Not Done";
								$second_msg = "Add Stage Details";
							}

							$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='3'";
							$stage3_data = $this->BlankModel->customquery($stage3_sql);
							if(!empty($stage3_data))
							{
								$third_data = "Process Done";
								$third_msg = "Update Stage Details";
							}
							else
							{
								$third_data = "Process Not Done";
								$third_msg = "Add Stage Details";
							}

							$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='4'";
							$stage4_data = $this->BlankModel->customquery($stage4_sql);

							if(!empty($stage4_data))
							{
								$fourth_data = "Process Done";
								$fourth_msg = "Update Stage Details";
							}
							else
							{
								$fourth_data = "Process Not Done";
								$fourth_msg = "Add Stage Details";
							}

							$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
							array_push($details, $specimen_information);
			}		
			if(in_array(7, $nail_unit))
			{
				$conditions = " ( `accessioning_num` = '".$specimen->assessioning_num."')";
				$select_fields = 'positive_negtaive';
				$is_multy_result = 0;
				$imp_data = $this->BlankModel->getTableData('import_data', $conditions, $select_fields, $is_multy_result);	
		
				if(in_array('positive', array_column($imp_data, 'positive_negtaive')))
			   	  {
					$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='1'";
					$stage1_data = $this->BlankModel->customquery($stage1_sql);
					if(!empty($stage1_data))
					{
						$first_data = "Process Done";
						$first_msg = "Update Stage Details";
					}
					else
					{
						$first_data = "Process Not Done";
						$first_msg = "Add Stage Details";
					}
					
					$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='2'";
					$stage2_data = $this->BlankModel->customquery($stage2_sql);
					if(!empty($stage2_data))
					{
						$second_data = "Process Done";
						$second_msg = "Update Stage Details";
					}
					else
					{
						$second_data = "Process Not Done";
						$second_msg = "Add Stage Details";
					}
	
					$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='3'";
					$stage3_data = $this->BlankModel->customquery($stage3_sql);
					if(!empty($stage3_data))
					{
						$third_data = "Process Done";
						$third_msg = "Update Stage Details";
					}
					else
					{
						$third_data = "Process Not Done";
						$third_msg = "Add Stage Details";
					}
	
					$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='4'";
					$stage4_data = $this->BlankModel->customquery($stage4_sql);
	
					if(!empty($stage4_data))
					{
						$fourth_data = "Process Done";
						$fourth_msg = "Update Stage Details";
					}
					else
					{
						$fourth_data = "Process Not Done";
						$fourth_msg = "Add Stage Details";
					}
	
					$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
					array_push($details, $specimen_information);
				}

			}	
			}
			echo(json_encode(array('count' => $num_rows_count, "specimens_detail" => $details)));
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}
		}

	}

    function specimen_stage_list_by_id()
	{
		$details = array();
		$data = json_decode(file_get_contents('php://input'), true);
		
		if(!empty($data)){
			$load = $data['load'];
			$stage_id = $data['stage_id'];
		$specimens_details = $this->get_specimen_stages($load);
		$num_rows_count = count($specimens_details);
		$specimen_information =  array();
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
			$clinic_query = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `assessioning_num` = '".$specimen->assessioning_num."'";
			$clinic_res = $this->BlankModel->customquery($clinic_query);
			$nail_unit = explode(',' ,$clinic_res[0]['nail_unit']);
			
			if(!in_array(7, $nail_unit))
			{			
							if($stage_id == '1'){
								$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='1'";
								$stage1_data = $this->BlankModel->customquery($stage1_sql);
								
								if(empty($stage1_data))
								{
									$first_data = "Process Not Done";
									$first_msg = "Add Stage Details";
									$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'first_msg'=>$first_msg);
									array_push($details, $specimen_information);
								}
								
								}
							if($stage_id == '2'){
								$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='2'";
								$stage2_data = $this->BlankModel->customquery($stage2_sql);
								if(empty($stage2_data))
								{
									$second_data = "Process Not Done";
									$second_msg = "Add Stage Details";
									$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'second_data'=>$second_data,'second_msg'=>$second_msg);
									array_push($details, $specimen_information);
								}
								}

							if($stage_id == '3'){
								$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='3'";
								$stage3_data = $this->BlankModel->customquery($stage3_sql);
								if(empty($stage3_data))
								{
									$third_data = "Process Not Done";
									$third_msg = "Add Stage Details";
									$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'third_data'=>$third_data,'third_msg'=>$third_msg);
									array_push($details, $specimen_information);
								}
								}

							if($stage_id == '4'){
								$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='4'";
								$stage4_data = $this->BlankModel->customquery($stage4_sql);

								if(empty($stage4_data))
								{
									$fourth_data = "Process Not Done";
									$fourth_msg = "Add Stage Details";
									$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'fourth_data'=>$fourth_data, 'fourth_msg'=>$fourth_msg);
									array_push($details, $specimen_information);
								}
								}

							//$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
							
			}		
			if(in_array(7, $nail_unit))
			{
				$conditions = " ( `accessioning_num` = '".$specimen->assessioning_num."')";
				$select_fields = 'positive_negtaive';
				$is_multy_result = 0;
				$imp_data = $this->BlankModel->getTableData('import_data', $conditions, $select_fields, $is_multy_result);	
		
				if(in_array('positive', array_column($imp_data, 'positive_negtaive')))
			   	  {
					if($stage_id == '1'){		 
						$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='1'";
						$stage1_data = $this->BlankModel->customquery($stage1_sql);
						if(empty($stage1_data))
						{
							$first_data = "Process Not Done";
							$first_msg = "Add Stage Details";
							$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'first_msg'=>$first_msg);
							array_push($details, $specimen_information);
						}
						}
					if($stage_id == '2'){
						$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='2'";
						$stage2_data = $this->BlankModel->customquery($stage2_sql);
						if(empty($stage2_data))
						{
							$second_data = "Process Not Done";
							$second_msg = "Add Stage Details";
							$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'second_data'=>$second_data,'second_msg'=>$second_msg);
							array_push($details, $specimen_information);
						}
						}
					if($stage_id == '3'){
					$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='3'";
					$stage3_data = $this->BlankModel->customquery($stage3_sql);
					if(empty($stage3_data))
					{
						$third_data = "Process Not Done";
						$third_msg = "Add Stage Details";
						$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'third_data'=>$third_data,'third_msg'=>$third_msg);
						array_push($details, $specimen_information);
					}
					
					}
					if($stage_id == '4'){
						$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='4'";
						$stage4_data = $this->BlankModel->customquery($stage4_sql);
		
						if(empty($stage4_data))
						{
							$fourth_data = "Process Not Done";
							$fourth_msg = "Add Stage Details";
							$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'fourth_data'=>$fourth_data, 'fourth_msg'=>$fourth_msg);
							array_push($details, $specimen_information);
						}
						
						
						}
					//$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
					
				}

			}	
			}
			echo(json_encode(array('count' => $num_rows_count, "specimens_detail" => $details)));
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}
	}

	}
	
	function get_specimen_stages($start='')
	{	
		$sql = "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check` FROM 
		(SELECT `id`,`assessioning_num`,`qc_check`,`modify_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' 
		AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' OR `pass_fail` = '1' group by `specimen_id` HAVING COUNT( * ) > 3))specimen  
		INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `specimen`.`modify_date` DESC LIMIT $start,10";


   /*$sql = "SELECT `specimen`.`id`,`specimen`.`assessioning_num` FROM (SELECT `id`, `assessioning_num`, `modify_date` FROM `wp_abd_specimen` 
                                                                                 
    WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59'    
                                                                                   
    AND `id` NOT IN ( SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id` )                                              AND `id` NOT IN ( SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '1' group by `specimen_id` HAVING COUNT(*) > 3 )                          )specimen 
    
    INNER JOIN 
    
    ( SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%' )clinic_info 
    ON `specimen`.`id` = `clinic_info`.`specimen_id` 
      
    ORDER BY `specimen`.`modify_date` DESC LIMIT $start,10 ";*/


		$results = $this->db->query($sql);	

		$specimen_details = $results->result();
		return $specimen_details;

	}
	
	
	/**
	* 
	* Search Stages 
	* 
	*/
	
	function search_stages()
	{	
		$details = array();
		$search="";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['barcode']!="")
		{
		$sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check`, `specimen`.`modify_date` FROM (SELECT `id`,`assessioning_num`,`qc_check`, `modify_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `assessioning_num` ='".$data['barcode']."' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`   ORDER BY `specimen`.`modify_date` DESC";			
		}
		if($data['accessioning_number']!= "" ){	
	    $sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check`, `specimen`.`modify_date` FROM (SELECT `id`,`assessioning_num`,`qc_check`, `modify_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `assessioning_num` LIKE '%".$data['accessioning_number']."%' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen 
	    INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info 
	    ON `specimen`.`id` = `clinic_info`.`specimen_id`   
	   
	    ORDER BY `specimen`.`modify_date` DESC";
		}
		if($data['fromdate']!="" && $data['todate']!="" && ($data['fromdate'] > '2017-03-27 23:59:59') && ($data['todate'] > '2017-03-27 23:59:59'))
		{
		 $sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check`, `specimen`.`modify_date` 
		 FROM (SELECT `id`,`assessioning_num`,`qc_check`, `modify_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `modify_date` BETWEEN '".$data['fromdate']." 00:00:00' AND '".$data['todate']." 23:59:59' AND `id` 
		 NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen 
		 
		 INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info 
		 ON `specimen`.`id` = `clinic_info`.`specimen_id` 
		
		 
		 ORDER BY `specimen`.`modify_date` DESC";
		
		}
		 $specimens_details = $this->BlankModel->customquery($sql);	 
		 $num_rows_count = count($specimens_details);
		 if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				
			$clinic_query = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `assessioning_num` = '".$specimen['assessioning_num']."'";
			$clinic_res = $this->BlankModel->customquery($clinic_query);
			$nail_unit = explode(',' ,$clinic_res[0]['nail_unit']);
			if(!in_array(7, $nail_unit))
			{			
				$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
				$stage1_data = $this->BlankModel->customquery($stage1_sql);
				if(!empty($stage1_data))
				{
					$first_data = "Process Done";
					$first_msg = "Update Stage Details";
				}
				else
				{
					$first_data = "Process Not Done";
					$first_msg = "Add Stage Details";
				}
				
				$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
				$stage2_data = $this->BlankModel->customquery($stage2_sql);
				if(!empty($stage2_data))
				{
					$second_data = "Process Done";
					$second_msg = "Update Stage Details";
				}
				else
				{
					$second_data = "Process Not Done";
					$second_msg = "Add Stage Details";
				}

				$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
				$stage3_data = $this->BlankModel->customquery($stage3_sql);
				if(!empty($stage3_data))
				{
					$third_data = "Process Done";
					$third_msg = "Update Stage Details";
				}
				else
				{
					$third_data = "Process Not Done";
					$third_msg = "Add Stage Details";
				}

				$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
				$stage4_data = $this->BlankModel->customquery($stage4_sql);

				if(!empty($stage4_data))
				{
					$fourth_data = "Process Done";
					$fourth_msg = "Update Stage Details";
				}
				else
				{
					$fourth_data = "Process Not Done";
					$fourth_msg = "Add Stage Details";
				}

				$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
				array_push($details, $specimen_information);
							
			}
					
			if(in_array(7, $nail_unit))
			{ 
				$conditions = " ( `accessioning_num` = '".$specimen['assessioning_num']."')";
				$select_fields = 'positive_negtaive';
				$is_multy_result = 0;
				$imp_data = $this->BlankModel->getTableData('import_data', $conditions, $select_fields, $is_multy_result);	
				
				if(in_array('positive', array_column($imp_data, 'positive_negtaive')))
			   	  {
					$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
					$stage1_data = $this->BlankModel->customquery($stage1_sql);
					if(!empty($stage1_data))
					{
						$first_data = "Process Done";
						$first_msg = "Update Stage Details";
					}
					else
					{
						$first_data = "Process Not Done";
						$first_msg = "Add Stage Details";
					}
					
					$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
					$stage2_data = $this->BlankModel->customquery($stage2_sql);
					if(!empty($stage2_data))
					{
						$second_data = "Process Done";
						$second_msg = "Update Stage Details";
					}
					else
					{
						$second_data = "Process Not Done";
						$second_msg = "Add Stage Details";
					}
	
					$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
					$stage3_data = $this->BlankModel->customquery($stage3_sql);
					if(!empty($stage3_data))
					{
						$third_data = "Process Done";
						$third_msg = "Update Stage Details";
					}
					else
					{
						$third_data = "Process Not Done";
						$third_msg = "Add Stage Details";
					}
	
					$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
					$stage4_data = $this->BlankModel->customquery($stage4_sql);
	
					if(!empty($stage4_data))
					{
						$fourth_data = "Process Done";
						$fourth_msg = "Update Stage Details";
					}
					else
					{
						$fourth_data = "Process Not Done";
						$fourth_msg = "Add Stage Details";
					}
	
					$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
					array_push($details, $specimen_information);
					
				}
				

			}	
			}
		
			die(json_encode(array('status'=>'1','count' => $num_rows_count, "specimens_detail" => $details)));
		}
		else
		{
			die(json_encode(array('status'=>'0')));
		}
	}
	
	function search_stages_by_id()
	{	
		$details = array();
		$search="";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['accessioning_number']!=""){	
	    $sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check` FROM (SELECT `id`,`assessioning_num`,`qc_check` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `assessioning_num` ='".$data['accessioning_number']."' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `specimen`.`id` desc";
		}
		 $specimens_details = $this->BlankModel->customquery($sql);	 
		 $num_rows_count = count($specimens_details);
		 if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				
			$clinic_query = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `assessioning_num` = '".$specimen['assessioning_num']."'";
			$clinic_res = $this->BlankModel->customquery($clinic_query);
			$nail_unit = explode(',' ,$clinic_res[0]['nail_unit']);
			if(!in_array(7, $nail_unit))
			{			
							$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
							$stage1_data = $this->BlankModel->customquery($stage1_sql);
							if(!empty($stage1_data))
							{
								$first_data = "Process Done";
								$first_msg = "Update Stage Details";
							}
							else
							{
								$first_data = "Process Not Done";
								$first_msg = "Add Stage Details";
							}
							
							$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
							$stage2_data = $this->BlankModel->customquery($stage2_sql);
							if(!empty($stage2_data))
							{
								$second_data = "Process Done";
								$second_msg = "Update Stage Details";
							}
							else
							{
								$second_data = "Process Not Done";
								$second_msg = "Add Stage Details";
							}

							$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
							$stage3_data = $this->BlankModel->customquery($stage3_sql);
							if(!empty($stage3_data))
							{
								$third_data = "Process Done";
								$third_msg = "Update Stage Details";
							}
							else
							{
								$third_data = "Process Not Done";
								$third_msg = "Add Stage Details";
							}

							$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
							$stage4_data = $this->BlankModel->customquery($stage4_sql);

							if(!empty($stage4_data))
							{
								$fourth_data = "Process Done";
								$fourth_msg = "Update Stage Details";
							}
							else
							{
								$fourth_data = "Process Not Done";
								$fourth_msg = "Add Stage Details";
							}

							$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
							array_push($details, $specimen_information);
							
			}
					
			if(in_array(7, $nail_unit))
			{ 
				$conditions = " ( `accessioning_num` = '".$specimen['assessioning_num']."')";
				$select_fields = 'positive_negtaive';
				$is_multy_result = 0;
				$imp_data = $this->BlankModel->getTableData('import_data', $conditions, $select_fields, $is_multy_result);	
				
				if(in_array('positive', array_column($imp_data, 'positive_negtaive')))
			   	  {
					$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
					$stage1_data = $this->BlankModel->customquery($stage1_sql);
					if(!empty($stage1_data))
					{
						$first_data = "Process Done";
						$first_msg = "Update Stage Details";
					}
					else
					{
						$first_data = "Process Not Done";
						$first_msg = "Add Stage Details";
					}
					
					$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
					$stage2_data = $this->BlankModel->customquery($stage2_sql);
					if(!empty($stage2_data))
					{
						$second_data = "Process Done";
						$second_msg = "Update Stage Details";
					}
					else
					{
						$second_data = "Process Not Done";
						$second_msg = "Add Stage Details";
					}
	
					$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
					$stage3_data = $this->BlankModel->customquery($stage3_sql);
					if(!empty($stage3_data))
					{
						$third_data = "Process Done";
						$third_msg = "Update Stage Details";
					}
					else
					{
						$third_data = "Process Not Done";
						$third_msg = "Add Stage Details";
					}
	
					$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
					$stage4_data = $this->BlankModel->customquery($stage4_sql);
	
					if(!empty($stage4_data))
					{
						$fourth_data = "Process Done";
						$fourth_msg = "Update Stage Details";
					}
					else
					{
						$fourth_data = "Process Not Done";
						$fourth_msg = "Add Stage Details";
					}
	
					$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
					array_push($details, $specimen_information);
					
				}
				

			}	
			}
		
			die(json_encode(array('status'=>'1','count' => $num_rows_count, "specimens_detail" => $details)));
		}
		else
		{
			die(json_encode(array('status'=>'0')));
		}
	}
	function last_five_stage_list()
	{
		$details = array();
	 
	    $sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check` FROM (SELECT `id`,`assessioning_num`,`qc_check` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND modify_date >= DATE(NOW()) - INTERVAL 5 DAY AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' OR `pass_fail` = '1' group by `specimen_id` HAVING COUNT( * ) > 3))specimen INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%')clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `specimen`.`id` desc";
		
	// $sql = "SELECT `specimen`.`id`,`specimen`.`assessioning_num` FROM (SELECT `id`,`assessioning_num`,`qc_check` FROM `wp_abd_specimen` 
                                                                                 
 //    WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59'    
 //    AND modify_date >= DATE(NOW()) - INTERVAL 5 DAY                                                                                  
 //    AND `id` NOT IN ( SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id` )                                              AND `id` NOT IN ( SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '1' group by `specimen_id` HAVING COUNT(*) > 3 )                          )specimen 
    
 //    INNER JOIN 
    
 //    ( SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' OR `nail_unit` LIKE '%2%' OR `nail_unit` LIKE '%3%' OR `nail_unit` LIKE '%5%' OR `nail_unit` LIKE '%6%' )clinic_info 
 //    ON `specimen`.`id` = `clinic_info`.`specimen_id` 
      
 //    ORDER BY `specimen`.`id` desc";		
		
		$specimens_details = $this->BlankModel->customquery($sql);
		$num_rows_count = count($specimens_details);
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				
			$clinic_query = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '%".$specimen['assessioning_num']."%'";
			$clinic_res = $this->BlankModel->customquery($clinic_query);
			$nail_unit = explode(',' ,$clinic_res[0]['nail_unit']);
			if(!in_array(7, $nail_unit))
			{			
							$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
							$stage1_data = $this->BlankModel->customquery($stage1_sql);
							if(!empty($stage1_data))
							{
								$first_data = "Process Done";
								$first_msg = "Update Stage Details";
							}
							else
							{
								$first_data = "Process Not Done";
								$first_msg = "Add Stage Details";
							}
							
							$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
							$stage2_data = $this->BlankModel->customquery($stage2_sql);
							if(!empty($stage2_data))
							{
								$second_data = "Process Done";
								$second_msg = "Update Stage Details";
							}
							else
							{
								$second_data = "Process Not Done";
								$second_msg = "Add Stage Details";
							}

							$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
							$stage3_data = $this->BlankModel->customquery($stage3_sql);
							if(!empty($stage3_data))
							{
								$third_data = "Process Done";
								$third_msg = "Update Stage Details";
							}
							else
							{
								$third_data = "Process Not Done";
								$third_msg = "Add Stage Details";
							}

							$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
							$stage4_data = $this->BlankModel->customquery($stage4_sql);

							if(!empty($stage4_data))
							{
								$fourth_data = "Process Done";
								$fourth_msg = "Update Stage Details";
							}
							else
							{
								$fourth_data = "Process Not Done";
								$fourth_msg = "Add Stage Details";
							}

							$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
							array_push($details, $specimen_information);
							//echo(json_encode(array('count' => $num_rows_count, "specimens_detail" => $details)));
			}
					
			if(in_array(7, $nail_unit))
			{ 
				$conditions = " ( `accessioning_num` = '".$specimen['assessioning_num']."')";
				$select_fields = 'positive_negtaive';
				$is_multy_result = 0;
				$imp_data = $this->BlankModel->getTableData('import_data', $conditions, $select_fields, $is_multy_result);	
				
				if(in_array('positive', array_column($imp_data, 'positive_negtaive')))
			   	  {
					$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='1'";
					$stage1_data = $this->BlankModel->customquery($stage1_sql);
					if(!empty($stage1_data))
					{
						$first_data = "Process Done";
						$first_msg = "Update Stage Details";
					}
					else
					{
						$first_data = "Process Not Done";
						$first_msg = "Add Stage Details";
					}
					
					$stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='2'";
					$stage2_data = $this->BlankModel->customquery($stage2_sql);
					if(!empty($stage2_data))
					{
						$second_data = "Process Done";
						$second_msg = "Update Stage Details";
					}
					else
					{
						$second_data = "Process Not Done";
						$second_msg = "Add Stage Details";
					}
	
					$stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='3'";
					$stage3_data = $this->BlankModel->customquery($stage3_sql);
					if(!empty($stage3_data))
					{
						$third_data = "Process Done";
						$third_msg = "Update Stage Details";
					}
					else
					{
						$third_data = "Process Not Done";
						$third_msg = "Add Stage Details";
					}
	
					$stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen['id']."' AND `status` = 'Complete' AND `stage_id` ='4'";
					$stage4_data = $this->BlankModel->customquery($stage4_sql);
	
					if(!empty($stage4_data))
					{
						$fourth_data = "Process Done";
						$fourth_msg = "Update Stage Details";
					}
					else
					{
						$fourth_data = "Process Not Done";
						$fourth_msg = "Add Stage Details";
					}
	
					$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'],'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data, 'first_msg'=>$first_msg,'second_msg'=>$second_msg, 'third_msg'=>$third_msg,'fourth_msg'=>$fourth_msg);
					array_push($details, $specimen_information);
					//echo(json_encode(array('count' => $num_rows_count, "specimens_detail" => $details)));
				}
				

			}	
			}
			die(json_encode(array('status'=>'1','count' => $num_rows_count, "specimens_detail" => $details)));	
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}
	}
	function view_specimen() 
	{
		$details = array();
		$partners_specimen_details ='';
		$tbl1=SPECIMEN_MAIN;
		$tbl2 = CLINICAL_INFO_MAIN;
		$spe_id ='';
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$spe_id = $data['id'];
			$dataType = $data['dataType'];
			if($dataType == 'archive'){
				$tbl1=SPECIMEN_ARCHIVE;
				$tbl2 = CLINICAL_INFO_ARCHIVE;
			}else{
				$tbl1=SPECIMEN_MAIN;
				$tbl2 = CLINICAL_INFO_MAIN;
			}
			$clinic_rows = $this->db->query("SELECT * FROM ".$tbl2." WHERE specimen_id ='".$spe_id."'");
				if($clinic_rows->num_rows() > 0){
					$sql = "SELECT * FROM (SELECT * FROM ".$tbl1.")specimen INNER JOIN 
		(SELECT * FROM $tbl2)clinical_det ON specimen.id = clinical_det.specimen_id WHERE specimen.id ='".$spe_id."'";
				}else{
					$sql = "SELECT * FROM ".$tbl1." WHERE id =".$spe_id;
				}
			
		$specimens_details = $this->BlankModel->customquery($sql);
			//pr($specimens_details[0]);
		if($specimens_details)
		{
			$partners_company= $specimens_details[0]['partners_company'];
			$bill= $specimens_details[0]['bill'];
			$bils = explode(",",$bill);
			$pri_subscriber = $specimens_details[0]['pri_subscriber']; 
			$subscriber = explode(",",$pri_subscriber);
			$sec_subscriber = $specimens_details[0]['sec_subscriber']; 
			$subscriber_sec = explode(",",$sec_subscriber);
			$insurance_type = explode(",",$specimens_details[0]['ins_type']);
			$clinical_specimen_fst = $specimens_details[0]['clinical_specimen'];
			$clinical_specimen = explode(",",$clinical_specimen_fst);
			$nail_units = $specimens_details[0]['nail_unit'];
			$nail_unit = explode(",",$nail_units);

			$pid = $specimens_details[0]['physician_id'];
			$physicianId = "'".$pid."'";
			$sql_new = "select * from `wp_abd_users` where ID ='".$pid."'";
			$result =  $this->BlankModel->customqueryForAps($sql_new);
			$physician_name = '';
			if($partners_company){
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $spe_id")->row_array();
				$partners_company_details = $this->db->query("SELECT * FROM wp_abd_partners_company WHERE partner_id = $partners_company")->row_array();
				$physician_name = $partners_specimen_details['physician_name'];
				$add = $partners_specimen_details['physician_address'];
				$mob = $partners_specimen_details['physician_phone'];

				$specimens_details[0]['partners_company_name'] = $partners_company_details['partner_name'];
			}else{
				$physician_first_name = get_aps_user_meta( $physicianId, 'first_name' );
				$physician_last_name  = get_aps_user_meta( $physicianId, 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				$phy_email = $result[0]['user_email'];
				$Physician_add= get_aps_user_meta($physicianId, '_address'); 
				$add = $Physician_add['meta_value'];
				$Physician_mob= get_aps_user_meta($physicianId, '_mobile');
				$mob = $Physician_mob['meta_value'];
			}
			$phy_information =  array( 'name' => $physician_name, 'phy_email' => $phy_email, 'phy_add' => $add, 'mob' => $mob);
			array_push($details, $phy_information);
			if($partners_company){
				$partners_specimen_details = $this->db->query("SELECT * FROM ".PARTNERS_SPECIMEN." WHERE specimen_id = $spe_id")->row_array();
			
			}
			$this->db->where('type_status','Active');
			$test_types = $this->db->get('wp_abd_test_types')->result_array();
			
			echo(json_encode(array('spe_det'=>$specimens_details,'phy_det'=>$details,'bill'=>$bils,'subscriber'=>$subscriber, 'subscriber_sec'=>$subscriber_sec, 'clinical_specimen'=>$clinical_specimen,'nail_unit'=>$nail_unit,'insurance_type'=>$insurance_type,'partners_specimen_details'=>$partners_specimen_details,'test_types'=>$test_types,'status'=>'1')));
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}
		}
	}

	function get_physician_details(){
		$details = array();
		$pid = json_decode(file_get_contents('php://input'), true);
		$sql_new = "select * from `wp_abd_users` where ID ='".$pid."'";
		$result =  $this->BlankModel->customqueryForAps($sql_new);
		$physicianId = "'".$pid."'";
		if($result){
		$physician_first_name = get_aps_user_meta( $physicianId, 'first_name' );
		$physician_last_name  = get_aps_user_meta( $physicianId, 'last_name');
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$phy_email = $result[0]['user_email'];
		$Physician_add= get_aps_user_meta($physicianId, '_address'); 
		$add = $Physician_add['meta_value'];
		$Physician_mob= get_aps_user_meta($physicianId, '_mobile');
		$mob = $Physician_mob['meta_value'];
		$phy_information =  array( 'name' => $physician_name, 'phy_email' => $phy_email, 'phy_add' => $add, 'mob' => $mob);
		array_push($details, $phy_information);
			echo(json_encode(array('phy_det'=>$details,'status'=>'1')));
		}else{
			echo(json_encode(array('status'=>'0')));
		}
	}

	function print_add_specimen_pdf(){
	$filecontent = $this->load->view('add_specimen','',true);
	$pdfTotalBody = $filecontent;
	$find=array();
	$find[]="{logo}";
	$find[]="{foot_pic}";
	$replace=array();
	$replace[] = base_url()."assets/frontend/images/logo.png";
	$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
	for($data_int=0;$data_int<count($find);$data_int++){ 
		$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
	}
	$body = $pdfTotalBody;
	$message = $body;
	$this->m_pdf->pdf->WriteHTML($message);
	$ability_pdf = "ability_".rand().".pdf";
	$this->m_pdf->pdf->Output(FCPATH."assets/uploads/pdf/".$ability_pdf,"F");

	echo(json_encode(array('base_url'=>base_url().'assets/uploads/pdf/','pdf_name'=>$ability_pdf)));
}

	function get_all_test_types(){

		$this->db->where('type_status','Active');
		$test_types = $this->db->get('wp_abd_test_types')->result_array();

		if(!empty($test_types)){
			die(json_encode(array('status'=>'1','test_types'=>$test_types)));
		}else{
			die(json_encode(array('status'=>'0')));
		}
		
	}
	function generate_barcode()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$folder = FCPATH."assets/uploads/qr_images/";
		$file_name = "qr_".$data.".png";
		$file_path = $folder.$file_name;
		$info = $data;	
		QRcode::png($info, $file_path);
		die(json_encode(array('status'=>'1','qrCode'=>$file_name,'base_url'=>base_url(),'acc_no'=>$data)));
	}

	function generateAccessioning($test_type){
		$digits = 4;
		$today_date = date('Y-m-d');
		$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where accessioning_cur_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
		$insert_day_data = $this->db->query($today_date)->result_array();
		$serise_no;
		foreach ($insert_day_data as $data) {
			$no = substr($data['assessioning_num'],7,4);
			$no = abs($no);
			if(empty($serise_no) || abs($serise_no) < $no){
				$serise_no = $no;
			}
		}
		$day_data = $serise_no+1;
		$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-'.$test_type;
		return $assessioning_num;
	}

/**
* 
* Specimen execl export data
*/
	function get_execl_export_data()
	{
        $data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$from_date = date('Y-m-d',strtotime($data['from_date']));
		$to_date = date('Y-m-d',strtotime($data['to_date']));
		$export_type = $data['export_type'];

     if($export_type == 'report_date'){
	
		/** 
		* @var Specimen and Histo Data
		*/	
		$sql_query_histo = "SELECT 
		`wp_abd_specimen`.`id` as`specimenid`,
		`wp_abd_specimen`.`lab_id`, 
		`wp_abd_specimen`.`qc_check_lab_id`, 
		`wp_abd_specimen`.`site_indicator`, 
		`wp_abd_specimen`.`test_type`, 
		`wp_abd_specimen`.`process_done`, 
		`wp_abd_specimen`.`physician_id`, 
		`wp_abd_specimen`.`patient_id`, 
		`wp_abd_specimen`.`collection_date`, 
		`wp_abd_specimen`.`collection_time`, 
		`wp_abd_specimen`.`date_received`, 
		`wp_abd_specimen`.`bill`, 
		`wp_abd_specimen`.`p_lastname`, 
		`wp_abd_specimen`.`p_firstname`, 
		`wp_abd_specimen`.`patient_city`, 
		`wp_abd_specimen`.`patient_address`, 
		`wp_abd_specimen`.`apt`, 
		`wp_abd_specimen`.`patient_phn`, 
		`wp_abd_specimen`.`patient_state`, 
		`wp_abd_specimen`.`patient_zip`, 
		`wp_abd_specimen`.`patient_dob`, 
		`wp_abd_specimen`.`patient_age`, 
		`wp_abd_specimen`.`patient_sex`, 
		`wp_abd_specimen`.`pri_subscriber`, 
		`wp_abd_specimen`.`pri_insurance_name`, 
		`wp_abd_specimen`.`pri_address`, 
		`wp_abd_specimen`.`pri_city`, 
		`wp_abd_specimen`.`pri_state`, 
		`wp_abd_specimen`.`pri_zip`, 
		`wp_abd_specimen`.`pri_employee_name`, 
		`wp_abd_specimen`.`pri_member_id`, 
		`wp_abd_specimen`.`pri_subscriber_dob`, 
		`wp_abd_specimen`.`pri_group_contact`, 
		`wp_abd_specimen`.`pri_sex`, 
		`wp_abd_specimen`.`pri_medicare_id`, 
		`wp_abd_specimen`.`pri_madicaid_id`, 
		`wp_abd_specimen`.`sec_subscriber`, 
		`wp_abd_specimen`.`sec_insurance_name`, 
		`wp_abd_specimen`.`sec_address`, 
		`wp_abd_specimen`.`sec_city`, 
		`wp_abd_specimen`.`sec_state`, 
		`wp_abd_specimen`.`sec_zip`, 
		`wp_abd_specimen`.`sec_employee_name`, 
		`wp_abd_specimen`.`sec_member_id`, 
		`wp_abd_specimen`.`sec_subscriber_dob`, 
		`wp_abd_specimen`.`sec_group_contact`, 
		`wp_abd_specimen`.`sec_sex`, 
		`wp_abd_specimen`.`sec_medicare_id`, 
		`wp_abd_specimen`.`sec_medicaid_id`, 
		`wp_abd_specimen`.`assessioning_num`,
		`wp_abd_specimen`.`qc_check`, 
		`wp_abd_specimen`.`ins_type`, 
		`wp_abd_specimen`.`create_date`
		
		FROM `wp_abd_specimen`

		LEFT JOIN  `wp_abd_nail_pathology_report`
		ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
			
		WHERE `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59' 

		AND (`wp_abd_nail_pathology_report`.`create_date` > '$to_date 00:00:00' AND `wp_abd_nail_pathology_report`.`create_date` < '$from_date 23:59:59' AND `wp_abd_nail_pathology_report`.`status` = 'Active') ORDER BY `wp_abd_specimen`.`id` DESC"; 
		$this->db->query('SET SQL_BIG_SELECTS=1'); 	
		$sql_result_histo = $this->BlankModel->customquery($sql_query_histo);
         
         /**
		 * PCR Specimen
		 */
          
		$sql_query_PCR = "SELECT 
		`wp_abd_specimen`.`id` as`specimenid`,
		`wp_abd_specimen`.`lab_id`, 
		`wp_abd_specimen`.`qc_check_lab_id`, 
		`wp_abd_specimen`.`site_indicator`, 
		`wp_abd_specimen`.`test_type`, 
		`wp_abd_specimen`.`process_done`, 
		`wp_abd_specimen`.`physician_id`, 
		`wp_abd_specimen`.`patient_id`, 
		`wp_abd_specimen`.`collection_date`, 
		`wp_abd_specimen`.`collection_time`, 
		`wp_abd_specimen`.`date_received` , 
		`wp_abd_specimen`.`bill`, 
		`wp_abd_specimen`.`p_lastname`, 
		`wp_abd_specimen`.`p_firstname`, 
		`wp_abd_specimen`.`patient_city`, 
		`wp_abd_specimen`.`patient_address`, 
		`wp_abd_specimen`.`apt` , 
		`wp_abd_specimen`.`patient_phn`, 
		`wp_abd_specimen`.`patient_state`, 
		`wp_abd_specimen`.`patient_zip`, 
		`wp_abd_specimen`.`patient_dob`, 
		`wp_abd_specimen`.`patient_age`, 
		`wp_abd_specimen`.`patient_sex`, 
		`wp_abd_specimen`.`pri_subscriber`, 
		`wp_abd_specimen`.`pri_insurance_name`, 
		`wp_abd_specimen`.`pri_address`, 
		`wp_abd_specimen`.`pri_city`, 
		`wp_abd_specimen`.`pri_state`, 
		`wp_abd_specimen`.`pri_zip`, 
		`wp_abd_specimen`.`pri_employee_name`, 
		`wp_abd_specimen`.`pri_member_id`, 
		`wp_abd_specimen`.`pri_subscriber_dob`, 
		`wp_abd_specimen`.`pri_group_contact`, 
		`wp_abd_specimen`.`pri_sex`, 
		`wp_abd_specimen`.`pri_medicare_id`, 
		`wp_abd_specimen`.`pri_madicaid_id`, 
		`wp_abd_specimen`.`sec_subscriber`, 
		`wp_abd_specimen`.`sec_insurance_name`, 
		`wp_abd_specimen`.`sec_address`, 
		`wp_abd_specimen`.`sec_city`, 
		`wp_abd_specimen`.`sec_state`, 
		`wp_abd_specimen`.`sec_zip`, 
		`wp_abd_specimen`.`sec_employee_name`, 
		`wp_abd_specimen`.`sec_member_id`, 
		`wp_abd_specimen`.`sec_subscriber_dob`, 
		`wp_abd_specimen`.`sec_group_contact`, 
		`wp_abd_specimen`.`sec_sex`, 
		`wp_abd_specimen`.`sec_medicare_id`, 
		`wp_abd_specimen`.`sec_medicaid_id`, 
		`wp_abd_specimen`.`assessioning_num`,
		`wp_abd_specimen`.`qc_check`, 
		`wp_abd_specimen`.`ins_type`, 
		`wp_abd_specimen`.`create_date`
		
		FROM `wp_abd_specimen`
		       
		LEFT JOIN `wp_abd_generated_pcr_reports` 
		ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num` WHERE `wp_abd_specimen`.`status` = '0' AND `wp_abd_specimen`.`physician_accepct` = '0' AND `wp_abd_specimen`.`create_date` > '2017-03-27 23:59:59'
		AND (`wp_abd_generated_pcr_reports`.`created_date` > '$from_date 00:00:00' AND `wp_abd_generated_pcr_reports`.`created_date` < '$to_date 23:59:59' AND `wp_abd_generated_pcr_reports`.`report_pdf_name` != '' ) ORDER BY `wp_abd_specimen`.`id` DESC ";
		$this->db->query('SET SQL_BIG_SELECTS=1'); 	
		$sql_result_PCR = $this->BlankModel->customquery($sql_query_PCR);
	
		$specimens_data   = array_unique( array_merge($sql_result_histo, $sql_result_PCR), SORT_REGULAR );
		$specimen_reports = array();	
		$specimen_reports_data = array();	
	
        foreach($specimens_data as $specimen_data){
	
		$specimen_id         = $specimen_data['specimenid'];		
		$accessioning_number = $specimen_data['assessioning_num'];	
		
		    $specimen_reports_data['Specimen Id']          = $specimen_id ; 
			$specimen_reports_data['Accessioning Number']  = $accessioning_number;
			$specimen_reports_data['Lab Tech']             = $specimen_data['lab_id'];
			$specimen_reports_data['QC Check']             = $specimen_data['qc_check'];
			$specimen_reports_data['QC Check Lab Tech']    = $specimen_data['qc_check_lab_id'];
			$specimen_reports_data['Site Indicator']       = $specimen_data['site_indicator'];
			$specimen_reports_data['Test Type']            = $specimen_data['test_type'];
			$specimen_reports_data['Wounds Process']       = $specimen_data['process_done'];
			$specimen_reports_data['Physician']            = $specimen_data['physician_id'];
			$specimen_reports_data['Patient Id']           = $specimen_data['patient_id'];
			$specimen_reports_data['Collection Date']      = $specimen_data['collection_date'];
			$specimen_reports_data['Collection Time']      = $specimen_data['collection_time'];
			$specimen_reports_data['Date Received']        = $specimen_data['date_received'];
			$specimen_reports_data['Bill']                 = $specimen_data['bill'];
			$specimen_reports_data['Patient Lastname']     = $specimen_data['p_lastname'];
			$specimen_reports_data['Patient Firstname']    = $specimen_data['p_firstname'];
			$specimen_reports_data['Patient City']         = $specimen_data['patient_city'];
			$specimen_reports_data['Patient Address']      = $specimen_data['patient_address'];
			$specimen_reports_data['Apt']                  = $specimen_data['apt'];
			$specimen_reports_data['Patient Phone']        = $specimen_data['patient_phn'];
			$specimen_reports_data['Patient State']        = $specimen_data['patient_state'];
			$specimen_reports_data['Patient Zip']          = $specimen_data['patient_zip'];
			$specimen_reports_data['Patient DOB']          = $specimen_data['patient_dob'];
			$specimen_reports_data['Patient Age']          = $specimen_data['patient_age'];
			$specimen_reports_data['Patient Sex']          = $specimen_data['patient_sex'];
			$specimen_reports_data['Primary Subscriber']   = $specimen_data['pri_subscriber'];
			$specimen_reports_data['Primary Insurance Name'] = $specimen_data['pri_insurance_name'];
			$specimen_reports_data['Primary Address']        = $specimen_data['pri_address'];
			$specimen_reports_data['Primary City']           = $specimen_data['pri_city'];
			$specimen_reports_data['Primary State']          = $specimen_data['pri_state'];
			$specimen_reports_data['Primary Zip']            = $specimen_data['pri_zip'];
			$specimen_reports_data['Primary Employee Name']  = $specimen_data['pri_employee_name'];
			$specimen_reports_data['Primary Member Id']      = $specimen_data['pri_member_id'];
			$specimen_reports_data['Primary Subscriber DOB'] = $specimen_data['pri_subscriber_dob'];
			$specimen_reports_data['Primary Group Contact']  = $specimen_data['pri_group_contact'];
			$specimen_reports_data['Primary Sex']            = $specimen_data['pri_sex'];
			$specimen_reports_data['Primary Medicare Id']    = $specimen_data['pri_medicare_id'];
			$specimen_reports_data['Primary Madicaid Id']    = $specimen_data['pri_madicaid_id'];
			$specimen_reports_data['Secondary Subscriber']   = $specimen_data['sec_subscriber'];
			$specimen_reports_data['Secondary Insurance Name'] = $specimen_data['sec_insurance_name'];
			$specimen_reports_data['Secondary Address']        = $specimen_data['sec_address'];
			$specimen_reports_data['Secondary City']           = $specimen_data['sec_city'];
			$specimen_reports_data['Secondary State']          = $specimen_data['sec_state'];
			$specimen_reports_data['Secondary Zip']            = $specimen_data['sec_zip'];
			$specimen_reports_data['Secondary Employee Name']  = $specimen_data['sec_employee_name'];
			$specimen_reports_data['Secondary Member Id']      = $specimen_data['sec_member_id'];
			$specimen_reports_data['Secondary Subscriber DOB'] = $specimen_data['sec_subscriber_dob'];
			$specimen_reports_data['Secondary Group Contact']  = $specimen_data['sec_group_contact'];
			$specimen_reports_data['Secondary Sex']            = $specimen_data['sec_sex'];
			$specimen_reports_data['Secondary Medicare']       = $specimen_data['sec_medicare_id'];
			$specimen_reports_data['Secondary Medicaid Id']    = $specimen_data['sec_medicaid_id'];			
			$specimen_reports_data['Insurance Type']           = $specimen_data['ins_type'];
			$specimen_reports_data['Specimen Create Date']     = $specimen_data['create_date'];
			
		$histo_condition   = " ( `specimen_id` = '$specimen_id' )";		
		$histo_field = "specimen_id, status, nail_pdf, create_date";
		$histo_data = $this->BlankModel->getTableData('wp_abd_nail_pathology_report', $histo_condition, $histo_field ,$get_data = 1 );
		
			$specimen_reports_data['HISTO PDF Link']           =  $histo_data['nail_pdf'];
		  
		$pcr_condition   = " ( `accessioning_num` = '$accessioning_number' )";		
		$field     = "accessioning_num, report_pdf_name, created_date";
		$pcr_data  = $this->BlankModel->getTableData('wp_abd_generated_pcr_reports', $pcr_condition, $field ,$get_data = 1 );
			$specimen_reports_data['PCR PDF']           =  $pcr_data['report_pdf_name'];
		    array_push($specimen_reports, $specimen_reports_data);	
	    }

    	$sql_result =  $specimen_reports;
    }  
    
     //------------- Specimen Data-------------//
	 else if($export_type == 'collection_date'){
			$sql_query_specimen = "SELECT 
			`wp_abd_specimen`.`id` as `Specimen Id`,
			`wp_abd_specimen`.`test_type` as 'Test Type', 
			`wp_abd_specimen`.`process_done` as 'Wounds Process', 
			`wp_abd_specimen`.`physician_id` as 'Physician', 
			`wp_abd_specimen`.`collection_date` as 'Collection Date', 
			`wp_abd_specimen`.`date_received` as 'Date Received', 
			`wp_abd_specimen`.`p_lastname` as 'Patient Lastname', 
			`wp_abd_specimen`.`p_firstname` as 'Patient Firstname', 
			`wp_abd_specimen`.`assessioning_num` as 'AccessioningNumber',
			`wp_abd_specimen`.`create_date` as 'Specimen Create Date', 
			`wp_abd_nail_pathology_report`.`nail_pdf` as 'HISTO PDF Link',	
			`wp_abd_generated_pcr_reports`.`report_pdf_name` as 'PCR PDF',
			`wp_abd_nail_pathology_report`.`dor` as 'Date of Histo Report',	
			`wp_abd_generated_pcr_reports`.`created_date` as 'Date of PCR Report'
		
			FROM `wp_abd_specimen`

			LEFT JOIN  `wp_abd_nail_pathology_report`
			ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
				   
			LEFT JOIN `wp_abd_generated_pcr_reports` 
			ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num` 

			WHERE date_format( str_to_date(`wp_abd_specimen`.`collection_date`,'%m/%d/%Y') ,'%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."' 
			ORDER BY `wp_abd_specimen`.`id` DESC";
		$this->db->query('SET SQL_BIG_SELECTS=1'); 	
		$new_sql_result = $this->BlankModel->customquery($sql_query_specimen);
	 }
      else {
	  $sql_query_specimen = "SELECT 
							`wp_abd_specimen`.`id` as `Specimen Id`,
							`wp_abd_specimen`.`lab_id` as 'Lab Tech', 
							`wp_abd_specimen`.`qc_check_lab_id` as 'QC Check Lab Tech', 
							`wp_abd_specimen`.`site_indicator` as 'Site Indicator', 
							`wp_abd_specimen`.`test_type` as 'Test Type', 
							`wp_abd_specimen`.`process_done` as 'Wounds Process', 
							`wp_abd_specimen`.`physician_id` as 'Physician', 
							`wp_abd_specimen`.`patient_id` as 'Patient Id', 
							`wp_abd_specimen`.`collection_date` as 'Collection Date', 
							`wp_abd_specimen`.`collection_time` as 'Collection Time', 
							`wp_abd_specimen`.`date_received` as 'Date Received', 
							`wp_abd_specimen`.`bill` as 'Bill', 
							`wp_abd_specimen`.`p_lastname` as 'Patient Lastname', 
							`wp_abd_specimen`.`p_firstname` as 'Patient Firstname', 
							`wp_abd_specimen`.`patient_city` as 'Patient City', 
							`wp_abd_specimen`.`patient_address` as 'Patient Address', 
							`wp_abd_specimen`.`apt` as 'Apt', 
							`wp_abd_specimen`.`patient_phn` as 'Patient Phone', 
							`wp_abd_specimen`.`patient_state` as 'Patient State', 
							`wp_abd_specimen`.`patient_zip` as 'Patient Zip', 
							`wp_abd_specimen`.`patient_dob` as 'Patient DOB', 
							`wp_abd_specimen`.`patient_age` as 'Patient Age', 
							`wp_abd_specimen`.`patient_sex` as 'Patient Sex', 
							`wp_abd_specimen`.`pri_subscriber` as 'Primary Subscriber', 
							`wp_abd_specimen`.`pri_insurance_name` as 'Primary Insurance Name', 
							`wp_abd_specimen`.`pri_address` as 'Primary Address', 
							`wp_abd_specimen`.`pri_city` as 'Primary City', 
							`wp_abd_specimen`.`pri_state` as 'Primary State', 
							`wp_abd_specimen`.`pri_zip` as 'Primary Zip', 
							`wp_abd_specimen`.`pri_employee_name` as 'Primary Employee Name', 
							`wp_abd_specimen`.`pri_member_id` as 'Primary Member Id', 
							`wp_abd_specimen`.`pri_subscriber_dob` as 'Primary Subscriber DOB', 
							`wp_abd_specimen`.`pri_group_contact` as 'Primary Group Contact', 
							`wp_abd_specimen`.`pri_sex` as 'Primary Sex', 
							`wp_abd_specimen`.`pri_medicare_id` as 'Primary Medicare Id', 
							`wp_abd_specimen`.`pri_madicaid_id` as 'Primary Madicaid Id', 
							`wp_abd_specimen`.`sec_subscriber` as 'Secondary Subscriber', 
							`wp_abd_specimen`.`sec_insurance_name` as 'Secondary Insurance Name', 
							`wp_abd_specimen`.`sec_address` as 'Secondary Address', 
							`wp_abd_specimen`.`sec_city` as 'Secondary City', 
							`wp_abd_specimen`.`sec_state` as 'Secondary State', 
							`wp_abd_specimen`.`sec_zip` as 'Secondary Zip', 
							`wp_abd_specimen`.`sec_employee_name` as 'Secondary Employee Name', 
							`wp_abd_specimen`.`sec_member_id` as 'Secondary Member Id', 
							`wp_abd_specimen`.`sec_subscriber_dob` as 'Secondary Subscriber DOB', 
							`wp_abd_specimen`.`sec_group_contact` as 'Secondary Group Contact', 
							`wp_abd_specimen`.`sec_sex` as 'Secondary Sex', 
							`wp_abd_specimen`.`sec_medicare_id` as 'Secondary Medicare', 
							`wp_abd_specimen`.`sec_medicaid_id` as 'Secondary Medicaid Id', 
							`wp_abd_specimen`.`assessioning_num` as AccessioningNumber,
							`wp_abd_specimen`.`qc_check` as 'QC Check', 
							`wp_abd_specimen`.`ins_type` as 'Insurance Type', 
							`wp_abd_specimen`.`create_date` as 'Specimen Create Date', 
							`wp_abd_nail_pathology_report`.`nail_pdf` as 'HISTO PDF Link',	
							`wp_abd_generated_pcr_reports`.`report_pdf_name` as 'PCR PDF'
						
							FROM `wp_abd_specimen`

							LEFT JOIN  `wp_abd_nail_pathology_report`
							ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
							       
							LEFT JOIN `wp_abd_generated_pcr_reports` 
							ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num` 

                            WHERE `wp_abd_specimen`.`create_date` > '".$from_date." 00:00:00' AND `wp_abd_specimen`.`create_date` < '".$to_date." 23:59:59' ORDER BY `wp_abd_specimen`.`id` DESC";
		$this->db->query('SET SQL_BIG_SELECTS=1'); 	
		$sql_result = $this->BlankModel->customquery($sql_query_specimen);
       }
	
	   if($export_type == 'collection_date'){
		$specimen_reports = array();	
    	for($i=0; $i <count($new_sql_result); $i++) {	
		$spe_id = $new_sql_result[$i]['Specimen Id'];	
		$specimen_details_sql = 'SELECT *  FROM `wp_abd_specimen` WHERE `id` = '.$spe_id.'';
		$specimen_details_data = $this->db->query($specimen_details_sql)->row_array();	
		$report_sql     = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `specimen_id` = '".$spe_id."'";
        $report_results = $this->BlankModel->customquery($report_sql);
        
        // if (!empty($report_results)) {
        //     $nail_unit = explode(",", $report_results[0]['nail_unit']);
        
        //   if ((in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) {
        //     $report_type_histo = "Histo";
        //   }
          
        //   if (in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit)) {
        //     $report_type_pcr = "PCR";
        //    }
        //   }
		
		if($specimen_details_data['partners_company']){
			$prtnr_id = $specimen_details_data['partners_company'];
			$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
			$partners_company_data = $this->db->query($partners_company_sql)->row_array();
			$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
			$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();

			$new_sql_result[$i]['Physician']= $partners_specimen_data['physician_name'];
			$new_sql_result[$i]['Sales Rep']= '';
		}else{
			$physician_first_name       = get_aps_user_meta_value( $new_sql_result[$i]['Physician'], 'first_name' );
			$physician_last_name        = get_aps_user_meta_value( $new_sql_result[$i]['Physician'], 'last_name');
			$physician_added_by        = get_aps_user_meta_value( $new_sql_result[$i]['Physician'], 'added_by');

			$new_sql_result[$i]['Physician']= $physician_first_name.' '.$physician_last_name;
			if(!empty($physician_added_by)){
				$sales_first_name       = get_aps_user_meta_value( $physician_added_by, 'first_name' );
				$sales_last_name        = get_aps_user_meta_value( $physician_added_by, 'last_name');
				$new_sql_result[$i]['Sales Rep']= $sales_first_name.' '.$sales_last_name;
			}else{
				$new_sql_result[$i]['Sales Rep']= '';
			}
		}
		 /**
		 * 
		 * @var HISTO
		 * 
		 */			 
		
	     if( ($new_sql_result[$i]['HISTO PDF Link'] != "") )
	     {
		 $new_sql_result[$i]['HISTO PDF Link'] = base_url().'assets/uploads/histo_report_pdf/'.$new_sql_result[$i]['HISTO PDF Link'];
		 }

		 else if(empty($new_sql_result[$i]['HISTO PDF Link']) && (in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) 
		 {
         $new_sql_result[$i]['HISTO PDF Link'] = "Pending";
         }
		
         else if (empty($new_sql_result[$i]['HISTO PDF Link']) && (!in_array("1", $nail_unit) || !in_array("2", $nail_unit) || !in_array("3", $nail_unit))) 
         {
         $new_sql_result[$i]['HISTO PDF Link'] = "No Histo Ordered";
         } 
		 			 
		 /**
		 * 
		 * @var PCR
		 * 
		 */
		 
		 if($new_sql_result[$i]['PCR PDF'] != "")
		 {
		 $new_sql_result[$i]['PCR PDF'] = base_url().'assets/uploads/pcr_report_pdf/'.$new_sql_result[$i]['PCR PDF']; 
		 }
		 
	     else if (empty($new_sql_result[$i]['PCR PDF']) && ( in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit) )) 
	     {
         $new_sql_result[$i]['PCR PDF'] = "Pending";
         }
         
         else if ( empty($new_sql_result[$i]['PCR PDF']) && ( !in_array("4", $nail_unit) )) 
         {
         $new_sql_result[$i]['PCR PDF'] = "No PCR Ordered";
		 } 

		 if(empty($new_sql_result[$i]['Date of Histo Report']) && empty($new_sql_result[$i]['Date of PCR Report'])){
			$new_sql_result[$i]['Most Recent Report Date'] = '';
		 }else if(empty($new_sql_result[$i]['Date of Histo Report'])){
			$new_sql_result[$i]['Most Recent Report Date'] = date('m/d/Y',strtotime($new_sql_result[$i]['Date of PCR Report']));
		 }else if(empty($new_sql_result[$i]['Date of PCR Report'])){
			$new_sql_result[$i]['Most Recent Report Date'] = date('m/d/Y',strtotime($new_sql_result[$i]['Date of Histo Report']));
		 }else if(!empty($new_sql_result[$i]['Date of Histo Report']) && !empty($new_sql_result[$i]['Date of PCR Report'])){
			 if(strtotime($new_sql_result[$i]['Date of PCR Report']) < strtotime($new_sql_result[$i]['Date of Histo Report'])){
				$new_sql_result[$i]['Most Recent Report Date'] = date('m/d/Y',strtotime($new_sql_result[$i]['Date of Histo Report']));
			 }else{
				$new_sql_result[$i]['Most Recent Report Date'] = date('m/d/Y',strtotime($new_sql_result[$i]['Date of PCR Report']));
			 }
			
		 }
		 

		 $specimen_res_based_on_collection_date = array(
			"Sales Rep"=> $new_sql_result[$i]['Sales Rep'],
			"Refer Provider"=> $new_sql_result[$i]['Physician'],
			"Accesssioning number"=> $new_sql_result[$i]['AccessioningNumber'],
			"Collection Date"=>$new_sql_result[$i]['Collection Date'],
			"Received Date"=>$new_sql_result[$i]['Date Received'],
			"Most Recent Report Date"=> $new_sql_result[$i]['Most Recent Report Date'],
			"Patient Last Name"=> $new_sql_result[$i]['Patient Lastname'],
			"Patient First Name"=> $new_sql_result[$i]['Patient Firstname'],
			"Histo Report Link"=> $new_sql_result[$i]['HISTO PDF Link'],
			"PCR Report Links"=> $new_sql_result[$i]['PCR PDF']
		 );

		 array_push($specimen_reports,$specimen_res_based_on_collection_date);
		}
		$sql_result =  $specimen_reports;

	}else{
		for($i=0; $i <count($sql_result); $i++) {	
			$spe_id = $sql_result[$i]['Specimen Id'];	
			$specimen_details_sql = 'SELECT *  FROM `wp_abd_specimen` WHERE `id` = '.$spe_id.'';
			$specimen_details_data = $this->db->query($specimen_details_sql)->row_array();	
			$report_sql     = "SELECT `nail_unit` FROM `wp_abd_clinical_info` WHERE `specimen_id` = '".$spe_id."'";
			$report_results = $this->BlankModel->customquery($report_sql);
			
			if (!empty($report_results)) {
				$nail_unit = explode(",", $report_results[0]['nail_unit']);
			
			  if ((in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) {
				$report_type_histo = "Histo";
			  }
			  
			  if (in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit)) {
				$report_type_pcr = "PCR";
			   }
			  }
			
			if($specimen_details_data['partners_company']){
				$prtnr_id = $specimen_details_data['partners_company'];
				$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
				$partners_company_data = $this->db->query($partners_company_sql)->row_array();
				$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
				$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();
	
				$sql_result[$i]['Physician']= $partners_specimen_data['physician_name'];
			}else{
				$physician_first_name       = get_aps_user_meta_value( $sql_result[$i]['Physician'], 'first_name' );
				$physician_last_name        = get_aps_user_meta_value( $sql_result[$i]['Physician'], 'last_name');
				$sql_result[$i]['Physician']= $physician_first_name.' '.$physician_last_name;
			}
			
	
	
			$lab_tech_first_name        = get_user_meta_value( $sql_result[$i]['Lab Tech'], 'first_name' );
			$lab_tech_last_name         = get_user_meta_value( $sql_result[$i]['Lab Tech'], 'last_name');	
			$sql_result[$i]['Lab Tech'] = $lab_tech_first_name.' '.$lab_tech_last_name;			
			$qc_check_lab_first_name    = get_user_meta_value( $sql_result[$i]['QC Check Lab Tech'], 'first_name' );
			$qc_check_lab_last_name     = get_user_meta_value( $sql_result[$i]['QC Check Lab Tech'], 'last_name');
			$sql_result[$i]['QC Check Lab Tech'] = $qc_check_lab_first_name.' '.$qc_check_lab_last_name;
			$sql_result[$i]['QC Check'] = $sql_result[$i]['QC Check'] == '0' ? 'Yes':'No';
			$bill_data = $sql_result[$i]['Bill'];
			$bils = explode(",",$bill_data);
			
			 foreach($bils as $bil){ 
			 if($bil == "1" ) { $sql_result[$i]['Bill'] = 'Insurance'; }
			 if($bil == "2" ) { $sql_result[$i]['Bill'] = 'Patient'; } 
			 }
				 
			 /**
			 * 
			 * @var HISTO
			 * 
			 */			 
			
			 if( ($sql_result[$i]['HISTO PDF Link'] != "") )
			 {
			 $sql_result[$i]['HISTO PDF Link'] = base_url().'assets/uploads/histo_report_pdf/'.$sql_result[$i]['HISTO PDF Link'];
			 }
	
			 else if(empty($sql_result[$i]['HISTO PDF Link']) && (in_array("1", $nail_unit) || in_array("2", $nail_unit) || in_array("3", $nail_unit))) 
			 {
			 $sql_result[$i]['HISTO PDF Link'] = "Pending";
			 }
			
			 else if (empty($sql_result[$i]['HISTO PDF Link']) && (!in_array("1", $nail_unit) || !in_array("2", $nail_unit) || !in_array("3", $nail_unit))) 
			 {
			 $sql_result[$i]['HISTO PDF Link'] = "No Histo Ordered";
			 } 
						  
			 /**
			 * 
			 * @var PCR
			 * 
			 */
			 
			 if($sql_result[$i]['PCR PDF'] != "")
			 {
			 $sql_result[$i]['PCR PDF'] = base_url().'assets/uploads/pcr_report_pdf/'.$sql_result[$i]['PCR PDF']; 
			 }
			 
			 else if (empty($sql_result[$i]['PCR PDF']) && ( in_array("4", $nail_unit) || in_array("5", $nail_unit) || in_array("7", $nail_unit) )) 
			 {
			 $sql_result[$i]['PCR PDF'] = "Pending";
			 }
			 
			 else if ( empty($sql_result[$i]['PCR PDF']) && ( !in_array("4", $nail_unit) )) 
			 {
			 $sql_result[$i]['PCR PDF'] = "No PCR Ordered";
			 } 
			}
	}
		
    	if(!empty($sql_result)){
			die(json_encode(array('status' => '1', 'excel_data' => $sql_result )));
		}
		else{
			die(json_encode(array('status' => '0', 'err_msg' => 'Data not found.' )));
		}
  	 
  	  } 
  	  	
      else{
	  die(json_encode(array('status' => '0')));
      }	
	}
	function split_name($name) {
		$name = trim($name);
	    $first_name = (strpos($name, ' ') === false) ? '' : preg_replace('/(\s*)([^\s]*)(.*)/', '$2', $name);
	    $last_name = trim( preg_replace('#'.$first_name.'#', '', $name ) );
	    return array($first_name, $last_name);
	}

	function import_specimen_data(){
		$data = json_decode(file_get_contents('php://input'), true);
		$regex_string_negate = '/^((?!S)(?!L)(?!O)(?!I)(?!B)(?!Z)(?!s)(?!l)(?!o)(?!i)(?!b)(?!z).)*$/';
		$regex_string_format = '/^([1-9][a-zA-Z][a-zA-Z|0-9]\d[a-zA-Z][a-zA-Z0-9]\d[a-zA-Z][a-zA-Z]\d\d){1}$/'; 
		if(!empty($data)){
			$this->db->trans_begin();
			$import_data = $data['import_data'];
			$user_id = $data['user_id'];
			$partners_company_id = $data['partners_company'];
			$fieldData = array();
			$clinical_specimen = array();
			$specialChars = array(" ", "\r", "\n");
			$replaceChars = array("", "", "");
			$errfields = array();
			$field_data = array('collection_date','date_received','physician_name','patient_lastname','patient_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_sex', 'primary_insurance_name','primary_address','primary_city','primary_state','primary_subscriber_dob','primary_group_contract','primary_sex');
			
			if($partners_company_id == 'ability-diagnostics'){
				foreach ($import_data as $apiData) {
					$error = false;
					$clinical_loc = array('','','','');
					$bill = array('','');
					$pri_subscriber = array('','','');
					$sec_subscriber = array('','','');
					$clinical_type = array('','','','','','');
					$insertData = array();
					$clinicalData = array();
					$physician_data ='';

					if(isset($apiData['physician_name'])){
						$name_split_arr = $this->split_name($apiData['physician_name']);
						$physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
						JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
						JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = "_status")
						JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = "first_name")	
						JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = "last_name")	
						WHERE (m3.meta_value LIKE "%physician%")        
						AND(m6.meta_value LIKE "%'.$name_split_arr[0].'%"
						AND m7.meta_value LIKE "%'.$name_split_arr[1].'%"  
						AND m5.meta_value LIKE "%Active%")';
//						$physician_data = $this->DB2->query($physician_sql)->row_array();
						$physician_data =  $this->BlankModel->customqueryForAps($sql_new);;
					}
					
						$dataCount = count($field_data);

						for($j=0;$j<$dataCount;$j++){
							if(empty($apiData[$field_data[$j]])){
								$error = true;
							}
						}
						$insertData['lab_id'] = $user_id;
	
						if(!empty($apiData['patient_id'])){
							$insertData['patient_id'] = $apiData['patient_id'];
						}
						
					if(isset($physician_data['ID']) && !empty($physician_data['ID'])){
							$insertData['physician_id'] = $physician_data['ID'];
						}else{
							$error = true;
						}
						if(!empty($apiData['collection_date'])){
							$insertData['collection_date'] = date('m/d/Y',strtotime($apiData['collection_date']));
						}
						if(!empty($apiData['date_received'])){
							$insertData['date_received'] = date('m/d/Y',strtotime($apiData['date_received'])); 
						}	
						
						$insertData['collection_time'] = !empty($apiData['collection_time'])?$apiData['collection_time']:'';

						$insertData['p_lastname'] = !empty($apiData['patient_lastname'])?stripslashes($apiData['patient_lastname']):'N/A';

						$insertData['p_firstname'] = !empty($apiData['patient_firstname'])?stripslashes($apiData['patient_firstname']):''; 

						$insertData['patient_phn'] = !empty($apiData['patient_phone'])?$apiData['patient_phone']:'N/A';

						if(!empty($apiData['patient_sex'])){
							if(preg_match("/female/i", $apiData['patient_sex'])){
								$insertData['patient_sex'] = 'Female';
							}else if(preg_match("/male/i", $apiData['patient_sex'])){
								$insertData['patient_sex'] = 'Male';
							}
						}
						
						$patient_dob = date('m/d/Y',strtotime($apiData['patient_dob']));	
					    $insertData['patient_dob'] = !empty($patient_dob) ? $patient_dob: 'N/A';
						$insertData['apt'] = !empty($apiData['apt'])?$apiData['apt']:'';	
						$insertData['patient_address'] = !empty($apiData['patient_address'])?$apiData['patient_address']:'N/A';
						$insertData['patient_city'] = !empty($apiData['patient_city'])?$apiData['patient_city']:'N/A';
						$insertData['patient_zip'] = !empty($apiData['patient_zip'])?$apiData['patient_zip']:'N/A';
						$insertData['patient_state'] = !empty($apiData['patient_state'])?$apiData['patient_state']:'N/A';
						//Primary Insurance
						$insertData['pri_insurance_name'] = !empty($apiData['primary_insurance_name'])?$apiData['primary_insurance_name']:'N/A';
						$insertData['pri_address'] = !empty($apiData['primary_address'])?$apiData['primary_address']:'N/A';
						$insertData['pri_city'] = !empty($apiData['primary_city'])?$apiData['primary_city']:'N/A';
						$insertData['pri_state'] = !empty($apiData['primary_state'])?$apiData['primary_state']:'N/A';
						$insertData['pri_zip'] = !empty($apiData['primary_zip'])?$apiData['primary_zip']:'N/A';
						$insertData['pri_member_id'] = !empty($apiData['primary_member_id'])? $apiData['primary_member_id']:'N/A';
						$insertData['pri_group_contact'] = !empty($apiData['primary_group_contact'])?$apiData['primary_group_contact']:'N/A';

						if(!empty($apiData['primary_subscriber'])){
							if(preg_match("/self/i", $apiData['primary_subscriber'])){
								unset($pri_subscriber[0]);  
								array_splice( $pri_subscriber, 0, 0, 'Self' );
							}
							if(preg_match("/spouse/i", $apiData['primary_subscriber'])){
								unset($pri_subscriber[1]);  
								array_splice( $pri_subscriber, 1, 0, 'Spouse' );
							}
							if(preg_match("/dependent/i", $apiData['primary_subscriber'])){
								unset($pri_subscriber[2]);  
								array_splice( $pri_subscriber, 2, 0, 'Dependent' );
							}
							$insertData['pri_subscriber'] = implode(',',$pri_subscriber);
						}
						

						
						if(!empty($apiData['primary_subscriber_dob'])){
							$insertData['pri_subscriber_dob'] = date('m/d/Y',strtotime($apiData['primary_subscriber_dob']));
						}
						if(!empty($apiData['primary_sex'])){
							if(preg_match("/female/i", $apiData['primary_sex'])){
								$insertData['pri_sex'] = 'Female';
							}else if(preg_match("/male/i", $apiData['primary_sex'])){
								$insertData['pri_sex'] = 'Male';
							}
						}
						
					if(!empty($apiData['primary_medicare_id'])){
						$mbi = $apiData['primary_medicare_id'];
						if(preg_match($regex_string_negate, $mbi) && preg_match($regex_string_format, $mbi)) { 
							$insertData['pri_medicare_id'] = $mbi;
						} 	
					}else{
						$insertData['pri_medicare_id'] = '';
						$error = true;
					}

//end primary insurance

					// Secondary Insurance

					$insertData['sec_insurance_name'] = !empty($apiData['secondary_insurance_name'])?$apiData['secondary_insurance_name']:'N/A';
					$insertData['sec_address'] = !empty($apiData['secondary_address'])?$apiData['secondary_address']:'N/A';
					$insertData['sec_city'] = !empty($apiData['secondary_city'])?$apiData['secondary_city']:'N/A';
					$insertData['sec_state'] = !empty($apiData['secondary_state'])?$apiData['secondary_state']:'N/A';
					$insertData['sec_zip'] = !empty($apiData['secondary_zip'])?$apiData['secondary_zip']:'N/A';
					$insertData['sec_member_id'] = !empty($apiData['secondary_member_id'])? $apiData['secondary_member_id']:'N/A';
					$insertData['sec_group_contact'] = !empty($apiData['secondary_group_contact'])?$apiData['secondary_group_contact']:'N/A';
if(!empty($apiData['secondary_subscriber'])){
						if(preg_match("/self/i", $apiData['secondary_subscriber'])){
							unset($sec_subscriber[0]);  
							array_splice( $sec_subscriber, 0, 0, 'Self' );
						}
						if(preg_match("/spouse/i", $apiData['secondary_subscriber'])){
							unset($sec_subscriber[1]);  
							array_splice( $sec_subscriber, 1, 0, 'Spouse' );
						}
						if(preg_match("/dependent/i", $apiData['secondary_subscriber'])){
							unset($sec_subscriber[2]);  
							array_splice( $sec_subscriber, 2, 0, 'Dependent' );
						}
						$insertData['sec_subscriber'] = implode(',',$sec_subscriber);
					}
	
if(!empty($apiData['secondary_subscriber_dob'])){
						$insertData['sec_subscriber_dob'] = date('m/d/Y',strtotime($apiData['secondary_subscriber_dob']));
					}
					if(!empty($apiData['secondary_sex'])){
						if(preg_match("/female/i", $apiData['secondary_sex'])){
							$insertData['sec_sex'] = 'Female';
						}
						else if(preg_match("/male/i", $apiData['secondary_sex'])){
							$insertData['sec_sex'] = 'Male';
						}
					}
					
					if(!empty($apiData['secondary_medicare_id'])){
						$mbi_sec = $apiData['secondary_medicare_id'];
						if(preg_match($regex_string_negate, $mbi_sec) && preg_match($regex_string_format, $mbi_sec)) { 
							$insertData['sec_medicare_id'] = $mbi_sec;
						} 	
					}else{
						$insertData['sec_medicare_id'] = '';
						$error = true;
					}

					//end secondary insurance 
						$insertData['physician_accepct'] =0;
						$insertData['status'] =0;
						$insertData['modify_date'] = date('Y-m-d H:i:s');
						$insertData['create_date'] = date('Y-m-d H:i:s');
						if($apiData['test_type'] == 'Nails'){
							$insertData['test_type'] = 'NF';
						}else if($apiData['test_type'] == 'Wounds'){
							$insertData['test_type'] = 'W';
							$insertData['process_done'] = 'Partner company';
						}
	
						$digits = 4;
						$today_date = date('Y-m-d');
						$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where accessioning_cur_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
						$insert_day_data = $this->db->query($today_date)->result_array();
						$serise_no;
						foreach ($insert_day_data as $data) {
							$no = substr($data['assessioning_num'],7,4);
							$no = abs($no);
							if(empty($serise_no) || abs($serise_no) < $no){
								$serise_no = $no;
							}
						}
					
						$day_data = $serise_no+1;
						$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-'.$insertData['test_type'];
	

						$insertData['assessioning_num'] = $assessioning_num;
						$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");
	
					if(!empty($apiData['insurance_type'])){
						$insertData['ins_type'] = $apiData['insurance_type'];
					}
						$clinicalData['assessioning_num']=$assessioning_num;
						if(preg_match("/histo/i", $apiData['clinical_type'])){
							if(preg_match("/pas/i",$apiData['histo_option']) && preg_match("/gms/i",$apiData['histo_option']) && preg_match("/fm/i",$apiData['histo_option'])){
								unset($clinical_type[1]);  
								array_splice( $clinical_type, 1, 0, '1' );
							}else if(preg_match("/pas/i",strtolower($apiData['histo_option'])) && preg_match("/gms/i",strtolower($apiData['histo_option']))){
								unset($clinical_type[2]);  
								array_splice( $clinical_type, 2, 0, '2' );
							}
						}
						if(preg_match("/pcr/i", $apiData['clinical_type'])){
							unset($clinical_type[4]);  
							array_splice( $clinical_type, 4, 0, '4' );
						}
						if(preg_match("/combination/i", $apiData['clinical_type'])){
								unset($clinical_type[0]);  
								array_splice( $clinical_type, 0, 0, '5' );
							if(preg_match("/pas/i",$apiData['histo_option']) && preg_match("/gms/i",$apiData['histo_option']) && preg_match("/fm/i",$apiData['histo_option'])){
								unset($clinical_type[1]);  
								array_splice( $clinical_type, 1, 0, '1' );
							}else if(preg_match("/pas/i",strtolower($apiData['histo_option'])) && preg_match("/gms/i",strtolower($apiData['histo_option']))){
								unset($clinical_type[2]);  
								array_splice( $clinical_type, 2, 0, '2' );
							}
						}
					
						$clinicalData['nail_unit'] = implode(',',$clinical_type);
						$clinicalData['create_date'] = date('Y-m-d H:i:s');
	
						if(preg_match("/right/i", $apiData['clinical_location'])){
							unset($clinical_loc[0]);  
							array_splice( $clinical_loc, 0, 0, 'Right' );
						}
						if(preg_match("/left/i", $apiData['clinical_location'])){
							unset($clinical_loc[1]);  
							array_splice( $clinical_loc, 1, 0, 'Left' );
						}
						if(preg_match("/biopsy/i", $apiData['clinical_location'])){
							unset($clinical_loc[2]);  
							array_splice( $clinical_loc, 2, 0, 'Biopsy' );
						}
						if(preg_match("/excision/i", $apiData['clinical_location'])){
							unset($clinical_loc[3]);  
							array_splice( $clinical_loc, 3, 0, 'Excision' );
						}
						
						$clinicalData['clinical_specimen'] = implode(',',$clinical_loc);
	
						if(preg_match("/insurance/i", $apiData['bill'])){
							unset($bill[0]);  
							array_splice( $bill, 0, 0, '1' );
						}
						if(preg_match("/patient/i", $apiData['bill'])){
							unset($bill[1]);  
							array_splice( $bill, 1, 0, '2' );
						}
	
						// $insertData['partners_company'] = $partners_company_id;
						 $insertData['bill'] = implode(',',$bill);
	
						if(!empty($apiData['site_indicator'])){
							$insertData['site_indicator'] = $apiData['site_indicator'];
						}
						$this->db->insert('wp_abd_specimen',$insertData);
						$lastId = $this->db->insert_id();
						$clinicalData['specimen_id'] = $lastId;
						$this->db->insert('wp_abd_clinical_info',$clinicalData);
						$clinicLastId = $this->db->insert_id();
	
						$folder = FCPATH."assets/uploads/qr_images/";
						$file_name = "qr_".$assessioning_num.".png";
						$file_path = $folder.$file_name;
						$info = $assessioning_num;
						QRcode::png($info, $file_path);
						if($error){
							array_push($errfields,$assessioning_num);
						}
					
				}
		}

          /**
			* 
			* @var Other Partner Company
			* 
			*/
			
			else{
				
			foreach ($import_data as $apiData) {
				$clinical_loc = array('','','','');
				$insertData   = array();
				$clinicalData = array();
				$bill         = array('','');
				$pri_subscriber = array('','','');
				$external_id    = $apiData['external_id'];				
			   
			    if($external_id){
				   $res_count = $this->db->query("SELECT `external_id` FROM `wp_abd_partners_specimen` WHERE `external_id` = '".$external_id."' ")->num_rows();	
				
				if($res_count > 0){	
				   $exits_external_id .= $external_id.', ';
				   $errfields_message = 'Note: Data has been Imported successfully but External ID '.$exits_external_id.'  is already exits on the system.'; 
				   $external_id = "";
				 }
			    }
			    else {
				   $external_id = "";
				}
			    
				$insertData['lab_id'] = $user_id;
				if(!empty($apiData['patient_id'])){
				$insertData['patient_id'] = $apiData['patient_id'];
				}
				$insertData['collection_date'] = date('m/d/Y',strtotime($apiData['collection_date']));
				$insertData['date_received'] = date('m/d/Y',strtotime($apiData['date_received'])); 
				$insertData['p_lastname'] = stripslashes($apiData['patient_lastname']);
				$insertData['p_firstname'] = $apiData['patient_firstname']; 
				if(!empty($apiData['patient_phone'])){
					$insertData['patient_phn'] = $apiData['patient_phone'];
				}
				$insertData['patient_sex'] = $apiData['patient_sex']; 
				$insertData['patient_dob'] = date('m/d/Y',strtotime($apiData['patient_dob'])); 
				$insertData['patient_address'] = 'N/A';
				$insertData['patient_city'] = 'N/A';
				$insertData['patient_zip'] = 'N/A';
				$insertData['patient_state'] = 'N/A';
				$insertData['pri_insurance_name'] = 'N/A';
				$insertData['pri_address'] = 'N/A';
				$insertData['pri_city'] = 'N/A';
				$insertData['pri_state'] = 'N/A';
				$insertData['pri_member_id'] = 'N/A';
				$insertData['pri_group_contact'] = 'N/A';
				$insertData['pri_subscriber'] = 'self,,';
				$insertData['pri_subscriber_dob'] = date('m/d/Y',strtotime($apiData['patient_dob']));
				$insertData['pri_sex'] = $apiData['patient_sex'];					
				$insertData['physician_accepct'] = 0;
				$insertData['status'] = 0;
				$insertData['modify_date'] = date('Y-m-d H:i:s');
				$insertData['create_date'] = date('Y-m-d H:i:s');
				if($apiData['test_type'] == 'Nails'){
					$insertData['test_type'] = 'NF';
				}else if($apiData['test_type'] == 'Wounds'){
					$insertData['test_type'] = 'W';
					$insertData['process_done'] = 'Partner company';
				}

				$digits = 4;
				$today_date = date('Y-m-d');
				$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where accessioning_cur_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
				$insert_day_data = $this->db->query($today_date)->result_array();
				$serise_no;
				foreach ($insert_day_data as $data) {
					$no = substr($data['assessioning_num'],7,4);
					$no = abs($no);
					if(empty($serise_no) || abs($serise_no) < $no){
						$serise_no = $no;
					}
				}
			
				$day_data = $serise_no+1;
				$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-'.$insertData['test_type'];
	
				$insertData['assessioning_num'] = $assessioning_num;
				$insertData['accessioning_cur_date'] = date("Y-m-d, H:i:s");

				// $insertData['ins_type'] = $apiData['insurance_type'];
				$clinicalData['assessioning_num']=$assessioning_num;

				if(strtolower($apiData['clinical_type']) =='histo'){
					$clinicalData['nail_unit'] = implode(',',array('','1','','','',''));
				}else if(strtolower($apiData['clinical_type']) =='pcr'){
					$clinicalData['nail_unit'] = implode(',',array('','','','','4',''));
				}else if(strtolower($apiData['clinical_type']) =='combination'){
					$clinicalData['nail_unit'] = implode(',',array('5','1','','','',''));
				}else{
					$clinicalData['nail_unit'] = implode(',',array('','','','','',''));
				}
				
				$clinicalData['create_date'] = date('Y-m-d H:i:s');

				if(preg_match("/right/i", $apiData['clinical_location'])){
					unset($clinical_loc[0]);  
					array_splice( $clinical_loc, 0, 0, 'Right' );
				}
				if(preg_match("/left/i", $apiData['clinical_location'])){
					unset($clinical_loc[1]);  
					array_splice( $clinical_loc, 1, 0, 'Left' );
				}
				if(preg_match("/biopsy/i", $apiData['clinical_location'])){
					unset($clinical_loc[2]);  
					array_splice( $clinical_loc, 2, 0, 'Biopsy' );
				}
				if(preg_match("/excision/i", $apiData['clinical_location'])){
					unset($clinical_loc[3]);  
					array_splice( $clinical_loc, 3, 0, 'Excision' );
				}
				
				$clinicalData['clinical_specimen'] = implode(',',$clinical_loc);
	
				$insertData['partners_company'] = $partners_company_id;
				
				if(!empty($apiData['site_indicator'])){
					$insertData['site_indicator'] = $apiData['site_indicator'];
				}
	
				$this->db->insert('wp_abd_specimen', $insertData);
				$lastId = $this->db->insert_id();
				$partners_specimen_arr = array();
				if(!empty($lastId)){
					$partners_specimen_arr['physician_name']    = ''.$apiData['physician_name'].'';
					$partners_specimen_arr['physician_address'] = ''.$apiData['physician_address'].'';
					$partners_specimen_arr['physician_phone']   = ''.$apiData['physician_phone'].'';
					$partners_specimen_arr['external_id']       = $external_id;
					$partners_specimen_arr['specimen_id']       = $lastId;
					$partners_specimen_arr['modified_at']       = date('Y-m-d H:i:s');
					$partners_specimen_arr['created_at']        = date('Y-m-d H:i:s');

					$this->db->insert('wp_abd_partners_specimen',$partners_specimen_arr);
				}

				$clinicalData['specimen_id'] = $lastId;
				$this->db->insert('wp_abd_clinical_info',$clinicalData);
				$clinicLastId = $this->db->insert_id();
				$folder = FCPATH."assets/uploads/qr_images/";
				$file_name = "qr_".$assessioning_num.".png";
				$file_path = $folder.$file_name;
				$info = $assessioning_num;
				QRcode::png($info, $file_path);
			
			  }
		    }

			if($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				die(json_encode(array('status'=>'0')));
			}
			else{
				$this->db->trans_commit();
			    die(json_encode(array('status' => '1', 'error_fields' => $errfields, 'message'=> $errfields_message)));	
			}
		}
	}


	function get_partners_company_details(){
		$parners_detail_data = $this->db->query("SELECT * FROM wp_abd_partners_company WHERE status='1'")->result_array();

		if(!empty($parners_detail_data)){
			die(json_encode(array('status'=>'1','parners_detail'=>$parners_detail_data)));
		}else{
			die(json_encode(array('status'=>'0')));
		}
	}
}
?>