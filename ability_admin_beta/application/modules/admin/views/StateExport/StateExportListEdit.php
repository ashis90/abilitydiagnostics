<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      State Export Edit Section

    </h1>
            <?php
          if ($this->session->flashdata('success')) {
        ?>
        <div class="alert alert-success alert-dismissable" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
            $message = $this->session->flashdata('success');
            echo ($message);
            ?>
        </div>
        <?php
          }
        ?>
        <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">State Export Edit Section</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <!-------------------------------- Show Roles ------------------------------------>

          
          <form action="<?php echo base_url().'admin/Stateexport/StateExportListUpdate/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
          <div class="lidot">
              <ul>

              <h3>State Name : <?php
                      $state_real_name = $statexpt_details['state_name']; 
                      echo  $state_array[$state_real_name]; ?> </h3>
              </ul>
          </div></n>
          <!-------------------------------- Show Roles ------------------------------------>


          <!-------------------------------- Show User value ------------------------------------>
          <div class="box-header"></div>
          <h3>Select Column For Export</h3>
          <div class="lidot">
            <ul>
              

              <?php

                $column_name = explode(",",$statexpt_details['column_name']);
               foreach($val as $key=> $stateval) { 
                
                ?>
              <li>
               
                <label><input type="checkbox" 
                  <?php
                  if (in_array($key, $column_name)) {
                    echo "checked";
                   } 
                  ?>
                 name="column_name[]" value="<?php echo $key; ?>" class="checkSingle" /> 
                <?php echo $stateval; ?> </label>
                  <p></p>
              </li>
              <?php 
              }
               ?>

            </ul>
            <input type="hidden" name="state_export_id" value="<?php echo $this->uri->segment(4);?>">

            <h4><input type="checkbox"  id="checkall" class="btn btn-primary ban lidot" />Check All</h4> 

          </div>
            <input type="submit" name="submit" class="btn btn-primary ban-sbmt" value="Update">
            <!-- <button type="submit" name='submit' class="btn btn-primary ban-sbmt">Update</button> -->

          <!-------------------------------- Show Meta Key ------------------------------------>
        </form>

          
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>
      <script type="text/javascript">
        $(document).ready(function() {
        $('#checkall').on('click',function(){
            if(this.checked){
                $('.checkSingle').each(function(){
                    this.checked = true;
                });
            }else{
                 $('.checkSingle').each(function(){
                    this.checked = false;
                });
            }
        });
    
          $('.checkSingle').on('click',function(){
              if($('.checkSingle:checked').length == $('.checkSingle').length){
                  $('#checkall').prop('checked',true);
              }else{
                  $('#checkall').prop('checked',false);
              }
          });
 
        });


      </script>
