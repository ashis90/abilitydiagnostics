import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryExtComponent } from './delivery-ext.component';

describe('DeliveryExtComponent', () => {
  let component: DeliveryExtComponent;
  let fixture: ComponentFixture<DeliveryExtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryExtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
