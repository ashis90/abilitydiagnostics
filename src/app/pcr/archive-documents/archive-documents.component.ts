import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'cm-archive-documents',
  templateUrl: './archive-documents.component.html',
  styleUrls: ['./archive-documents.component.css']
})



export class ArchiveDocumentsComponent implements OnInit {
  @ViewChild('uploadFile') uploadFileInput: ElementRef;

  public data : any;
  specimens_list:any = [];
  post_array:any;
  batch_review:any;
  specimen_count:any;
  loading:boolean;
  no_data:any;
  no_data_status:boolean=false;
  file:any;
  filterData:any;
  public url = environment.assetsUrl+'assets/uploads/';
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.getArchiveDocumentData();
  }

  incomingfile(event) 
  {
  this.file= <File>event.target.files[0]; 
    
  }

  fileUpload(){
    if(this.file){
      this.spinner.show();
      var fileSplit = this.file['name'].split('.');
      var fileExt = '';
      if (fileSplit.length > 1) {
      fileExt = fileSplit[fileSplit.length - 1];
      } 
      if(fileExt ==='xls' || fileExt ==='xlsx' || fileExt ==='doc' || fileExt ==='docx' || fileExt ==='pdf' || fileExt ==='eds'){
        const formData = new FormData();
        formData.append('name',this.file['name']);
        formData.append('file_data',this.file);
        this.itemsService.uploadArchiveFile(formData).subscribe(data => {
          let response = data;
          if(response['status'] === '1'){
            this.spinner.hide();
            this.uploadFileInput.nativeElement.value = "";
            this.toastr.info('The file is successfully uploaded.', 'Success', {
              timeOut: 3000
            });
            this.loading = true;
            this.getArchiveDocumentData();
          }else {
            this.toastr.info('Sorry, Faild to upload the file!', 'Error', {
              timeOut: 3000
            });
          }
         });
      }else{
        this.toastr.info('Sorry, File type is not allowed. Only doc, excel and pdf!', 'Error', {
          timeOut: 3000
        });
      }
    }else{
      this.toastr.info('Sorry, no file selected!', 'Error', {
        timeOut: 3000
      });
    }
  }

  getArchiveDocumentData() {
    this.itemsService.getArchiveDocument().subscribe(data => {          
    this.post_array = data;
    this.spinner.hide();
    this.data = this.post_array.archive_doc_data;
  
    this.specimen_count = this.post_array.specimen_count;
    
      if(this.post_array['status'] === '1')
      {
        this.no_data_status=false;
      this.data.forEach(item => {
        let fileSplit = item.file_name.split('.');
        let fileExt = '';
        if (fileSplit.length > 1) {
        fileExt = fileSplit[fileSplit.length - 1];
        item.ext = fileExt;
        } 
      });
      }
      if(this.post_array['status'] === '0')
      {
        this.no_data_status=true;
        this.loading = false;
        this.no_data="No Data Found.";
      }
               
    });
  }

  deleteArchiveFile(dataId:any){
    let res = confirm("Want to delete File?");
    if(res){
      let params;
      params = {'file_id':dataId};
      this.itemsService.deleteArchive(params).subscribe(data => { 
        let response = data;
        if(response['status'] === '1'){
          this.toastr.info('File deleted successfully', 'Success', {
            timeOut: 3000
          });
          this.spinner.show();
          this.getArchiveDocumentData();
        }else{
          this.toastr.info('Failed to deleted the file!', 'Error', {
            timeOut: 3000
          });
          this.spinner.show();
          this.getArchiveDocumentData();
        }
      });
    }
  }

  search(term: string) {
    let allData:any;
    allData = this.post_array.archive_doc_data;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.file_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
    
  }

}
