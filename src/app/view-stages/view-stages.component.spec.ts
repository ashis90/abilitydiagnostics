import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStagesComponent } from './view-stages.component';

describe('ViewStagesComponent', () => {
  let component: ViewStagesComponent;
  let fixture: ComponentFixture<ViewStagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
