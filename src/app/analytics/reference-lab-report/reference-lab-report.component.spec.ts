import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceLabReportComponent } from './reference-lab-report.component';

describe('ReferenceLabReportComponent', () => {
  let component: ReferenceLabReportComponent;
  let fixture: ComponentFixture<ReferenceLabReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceLabReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceLabReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
