<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
?>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
       Contact Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Contact List</h3>
            </div>     
          <div class="box-body">
            <!--<div id="alert-success" style="display: none;" class="alert alert-success" role="alert" ></div>-->
         <div class="svt">
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="basicDataTable">
            <thead>
              <tr>
                <th class="sort-alpha">Sr No</th>
                <th class="sort-alpha">User Name</th>   
                <th class="sort-alpha">Subject</th>
                <th class="sort-alpha">User Message</th>            
              	<th class="sort-alpha">Date & Time</th>                 
              </tr>
            </thead>
            <tbody>
            <?php $i=1; if($contactList){ 
              foreach($contactList as $list){                
              ?>             
              <tr class="odd gradeX">
                    <td><?php echo $i;?></td>                
                    <td>
						Name : <?php echo $list['contact_name']."<br/>";?>
                        Email : <?php echo $list['contact_email'];?>
                    </td>
                    <td><?php echo $list['contact_subject'];?></td>  
                    <td><?php echo $list['contact_msg'];?></td>  
                    <td><?php echo $list['contact_add_date'];?></td>                            
              </tr>
              <?php $i++; } } ?>             
            </tbody>
           </table>
    		 </div>
         </div>
           </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
 </div>
  